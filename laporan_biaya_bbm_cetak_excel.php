<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 713;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_KASIR,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$cari  					= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$spbu						= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];


//INISIALISASI
$kondisi	= "WHERE (TglVoucher BETWEEN CONVERT(datetime,'$tanggal_mulai',105) AND CONVERT(datetime,'$tanggal_akhir',105))";

$kondisi_spbu	=($spbu==0 || $spbu=="")?"":
	" AND IdSPBU = '$spbu' ";

$kondisi_cari	=($cari=="")?"":
	" AND (NoSPJ LIKE '%$cari'
		OR NoPolisi LIKE '%$cari%')";
	
$kondisi	= $kondisi.$kondisi_spbu.$kondisi_jb.$kondisi_cari;

		
//QUERY

$sql="SELECT dbo.f_SPBUGetNameByKode('$spbu') AS SPBU";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$nama_spbu	= $row['SPBU'];
}


$sql=
	"SELECT 
		IdVoucherBBM,KodeVoucher,NoSPJ,CONVERT(varchar(12),TglVoucher,106) as TanggalVoucher,NoPolisi,
		dbo.f_SopirGetNamaByKode(KodeSopir) AS NamaSopir,Kilometer,
		JenisBBM,CONVERT(DECIMAL,JumlahLiter) AS JumlahLiter,CONVERT(DECIMAL,JumlahBiaya) AS JumlahBiaya,
		dbo.f_CabangGetNameByKode(KodeCabang) AS Cabang,
		dbo.f_SPBUGetNameByKode(IdSPBU) AS SPBU,
		dbo.f_UserGetNameById(IdPetugas) AS Kasir,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangAsalByIdJurusan(IdJurusan)) Asal,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangTujuanByIdJurusan(IdJurusan)) Tujuan
		
	FROM 
		TbVoucherBBM
	$kondisi
	ORDER BY TglVoucher,NoSPJ,IdVoucherBBM";	

//EXPORT KE MS-EXCEL
	
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Biaya BBM per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
		$objPHPExcel->getActiveSheet()->setCellValue('A2', 'SPBU: '.$nama_spbu);
	  $objPHPExcel->getActiveSheet()->setCellValue('A4', 'No');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Tanggal');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Kode Voucher');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Jurusan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E4', 'No. Polisi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Sopir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G4', 'BBM');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Jumlah');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Liter');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J4', 'SPBU');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Kasir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			$idx++;
			$idx_row=$idx+4;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['TanggalVoucher']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeVoucher']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Asal']."-".$row['Tujuan']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NoPolisi']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NamaSopir']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row,$LIST_BBM[$row['JenisBBM']]);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['JumlahBiaya']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['JumlahLiter']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['SPBU']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Kasir']);
			
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':G'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
			
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Laporan Biaya BBM '.$nama_spbu.' per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
