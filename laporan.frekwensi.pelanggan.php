<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 309;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tahun  		= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

// LIST
$template->set_filenames(array('body' => 'laporan.frekwensi.pelanggan/index.tpl')); 

$tahun_now	= date("Y");
$tahun			= $tahun==""?$tahun_now:$tahun;

//CREATE VIEW
$sql	= 
	"CREATE OR REPLACE VIEW v_group_book AS 
		SELECT Telp,MONTH(TglBerangkat) AS Bulan,COUNT(DISTINCT(TglBerangkat)) AS JumlahBerangkat
		FROM tbl_reservasi 
		WHERE (tglberangkat BETWEEN '$tahun-01-01' AND '$tahun-12-31') AND FlagBatal!=1 AND CetakTiket=1
		GROUP BY Telp,MONTH(TglBerangkat)
		ORDER BY Telp,Bulan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$sql=
	"SELECT Bulan,JumlahBerangkat,COUNT(DISTINCT(Telp)) AS JumlahPax FROM v_group_book
	GROUP BY JumlahBerangkat,Bulan
	ORDER BY JumlahBerangkat";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$max_grup	= 0;

//PLOT DATA
while($row = $db->sql_fetchrow($result)){
	
	$data[$row["Bulan"]][$row["JumlahBerangkat"]]	= $row["JumlahPax"];
	
	$max_grup	= $row["JumlahBerangkat"]<=$max_grup?$max_grup:$row["JumlahBerangkat"];
		
}

for($i=1;$i<=$max_grup;$i++){
	
	$odd	= $i%2==0?"even":"odd";
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'grup'=>$i,
				'jan'=>number_format($data[1][$i],0,",","."),
				'feb'=>number_format($data[2][$i],0,",","."),
				'mar'=>number_format($data[3][$i],0,",","."),
				'apr'=>number_format($data[4][$i],0,",","."),
				'mei'=>number_format($data[5][$i],0,",","."),
				'jun'=>number_format($data[6][$i],0,",","."),
				'jul'=>number_format($data[7][$i],0,",","."),
				'agu'=>number_format($data[8][$i],0,",","."),
				'sep'=>number_format($data[9][$i],0,",","."),
				'okt'=>number_format($data[10][$i],0,",","."),
				'nov'=>number_format($data[11][$i],0,",","."),
				'des'=>number_format($data[12][$i],0,",",".")
		)	
	);
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tahun=".$tahun;
													
$script_cetak_excel="Start('laporan.frekwensi.pelanggan.cetakexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$temp_var		= "sel_tahun".($tahun-$tahun_now+3);
$$temp_var	= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan.frekwensi.pelanggan.'.$phpEx),
	'TAHUN_1'				=> $tahun_now-2,
	'TAHUN_2'				=> $tahun_now-1,
	'TAHUN_3'				=> $tahun_now,
	'SEL_TAHUN1'		=> $sel_tahun1,
	'SEL_TAHUN2'		=> $sel_tahun2,
	'SEL_TAHUN3'		=> $sel_tahun3,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>