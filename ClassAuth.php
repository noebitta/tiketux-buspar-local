<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
//#############################################################################

class Auth{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Auth(){
		$this->ID_FILE="C-AUTH";
	}
	
	//BODY

  function verifikasiMacAddress($mac_address){

    //kamus
    global $db;

    $sql =
      "SELECT Id,KodeCabang,NamaKomputer,IsAktif
			FROM tbl_mac_address
			WHERE MacAddress='$mac_address';";

    if (!$result = $db->sql_query($sql,TRUE)){
      die_error("Err:$this->ID_FILE ".__LINE__);
    }

    $row=$db->sql_fetchrow($result);

    return $row;

  }//  END verifikasiMacAddress

  function login($username,$user_password){

		//kamus
		global $db;

    $sql =
      "SELECT user_id, user_password, user_level,user_active
			FROM  tbl_user
			WHERE username = '" . str_replace("\\'", "''", $username) . "'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);

    if($row["user_id"]>0 && $row["user_password"]==md5($user_password) && $row["user_active"]){
      //username dan password cocok

      $ret_val["berhasil"]      = true;
      $ret_val["user_id"]       = $row["user_id"];
      $ret_val["user_level"]    = $row["user_level"];
    }
    else{
      //user tidak ditemukan atau password salah atau user di nonaktifkan
      $ret_val["berhasil"]  = false;
    }
		
		return $ret_val;
		
	}//  END login

  function logout($userdata){

    //kamus
    global $db;

    //MENGAMBIL LOG TERAKHIR DARI USER INI
    $sql =
      "SELECT IdLog
			FROM  tbl_log_user
			WHERE UserId = '$userdata[user_id]'
			ORDER BY WaktuLogin DESC
			LIMIT 0,1";

    if (!$result = $db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    $row = $db->sql_fetchrow($result);

    $id_log = $row["IdLog"];

    //UPDATE WAKTU LOGOUT
    $sql=
      "UPDATE tbl_log_user SET
        WaktuLogout=NOW()
      WHERE IdLog=$id_log";

    if (!$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    //MENGHANCURKAN SESSION
    session_end($userdata['session_id'], $userdata['user_id']);

    return true;

  }//  END logout

  function createToken($username,$user_ip){
    global $db;

    $sql =
      "SELECT user_id, user_password
			FROM  tbl_user
			WHERE username = '" . str_replace("\\'", "''", $username) . "'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    $row=  $db->sql_fetchrow($result);

    $date = date("Ymd");

    return md5($username."#".$row["user_id"]."$".$row["user_password"]."%bhinekatunggalika"."^".$user_ip."&".$date);

  } //createToken

  function updateCabangTugas($user_id,$kode_cabang){

    //kamus
    global $db;

    $sql =
      "UPDATE tbl_user SET
        KodeCabang='$kode_cabang'
		  WHERE user_id='$user_id'";

    if (!$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    return true;

  }// updateCabangTugas

  function updateLogUser($user_id,$user_ip,$mac_address){

    //kamus
    global $db;

    //mencatat log login user
    $sql =
      "INSERT INTO  tbl_log_user SET
          UserId='$user_id',
          WaktuLogin=NOW(),
          UserIp='$user_ip',
          MacAddress='$mac_address'";

    if (!$db->sql_query($sql)){
      die_error("Err $sql:$this->ID_FILE".__LINE__);
    }

    return true;

  }// updateLogUser
	
	function tambah($mac_address,$kode_cabang,$nama_komputer){

		//kamus
		global $db;
		global $userdata;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
				"INSERT INTO tbl_mac_address(MacAddress,KodeCabang,NamaKomputer,AddBy,AddByName,WaktuTambah) 
				VALUES('$mac_address','$kode_cabang','$nama_komputer',$userdata[user_id],'$userdata[username]',NOW());";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah($id,$mac_address,$kode_cabang,$nama_komputer,$is_aktif,$allow_cso){
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		$sql =
			"UPDATE tbl_mac_address 
			SET MacAddress='$mac_address',KodeCabang='$kode_cabang',NamaKomputer='$nama_komputer',
			  IsAktif='$is_aktif',AllowCSO='$allow_cso'
			WHERE Id=$id;";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list){
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_mac_address
			WHERE Id IN($list);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($id){

		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_mac_address
			WHERE Id=$id;";
		
		if (!$result = $db->sql_query($sql,TRUE)){
			die_error("Err:$this->ID_FILE ".__LINE__);
		}

		$row=$db->sql_fetchrow($result);
		return $row;
		
	}//  END ambilDataDetail

  function setComboListCabang($cari="",$kota=""){

    //kamus
    global $db;

    if($kota!=""){
      $kondisi	= " WHERE Kota='$kota' AND (Nama LIKE '%$cari%' OR KodeCabang LIKE '$cari%')";
      $order_by	= " Nama ";
    }
    else{
      $kondisi	= " WHERE (Nama LIKE '%$cari%' OR KodeCabang LIKE '$cari%')";
      $order_by	= " Kota,Nama ";
    }

    $sql =
      "SELECT KodeCabang,Nama,Kota
			FROM tbl_md_cabang
			$kondisi AND FlagAgen!=1
			ORDER BY $order_by;";

    if ($result = $db->sql_query($sql)){
      return $result;
    }
    else{
      //die_error("Gagal $this->ID_FILE 003");
      echo("Err: $this->ID_FILE". __LINE__);
    }

  }//  END setComboListCabang

  function daftarkanKomputer($mac_address,$kode_cabang,$nama_komputer,$username){

    //kamus
    global $db;

    //MEMERIKSA APAKAH KOMPUTER SUDAH PERNAH TERDAFTAR ATAU BELUM
    $sql = "SELECT Id FROM tbl_mac_address WHERE MacAddress='$mac_address';";

    if (!$result=$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    $row = $db->sql_fetchrow($result);

    if($row["Id"]!=""){
      //KOMPUTER SUDAH PERNAH DIDAFTARKAN
      return false;
    }

    //MENGAMBIL NAMA USER YANG REQUEST
    $sql  = "SELECT user_id,nama FROM tbl_user WHERE username='$username'";


    if (!$result=$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    $row = $db->sql_fetchrow($result);

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql =
      "INSERT INTO tbl_mac_address(MacAddress,KodeCabang,NamaKomputer,AddBy,AddByName,WaktuTambah,IsAktif)
				VALUES('$mac_address','$kode_cabang','$nama_komputer','$row[user_id]','$row[nama]',NOW(),2);";

    if (!$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    return true;
  } //daftarkanKomputer

  function ubahPassword($user_id,$password_lama,$password_baru){

    //kamus
    global $db;

    //Mengambil password lama
    $sql= "SELECT user_password FROM tbl_user WHERE user_id='$user_id'";

    if ($result=!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    if($row["user_password"]!=md5($password_lama)){
      //Password lama tidak benar
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Password lama tidak benar";
      return $ret_val;
    }

    //Mengubah password
    $sql =
      "UPDATE tbl_user SET
        user_password='".md5($password_baru)."'
		  WHERE user_id='$user_id'";

    if (!$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    $ret_val["status"]  = "OK";
    return $ret_val;

  }// ubahPassword

}
?>