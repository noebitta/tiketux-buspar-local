<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 219;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';


//PARAMETER
$bulan          = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun          = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];
$cari   		= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$bulan          = $bulan==""?date("m"):$bulan;
$bulan          = (int)$bulan;
$tahun          = $tahun==""?date("Y"):$tahun;

$dateObj   = DateTime::createFromFormat('!m', $bulan);
$monthName = $dateObj->format('F');

$kondisi	=($cari=="")?"":" WHERE (tbl_md_kendaraan.KodeKendaraan LIKE '%$cari%' OR tbl_md_kendaraan.NoPolisi LIKE '%$cari' OR tbl_md_kendaraan.Merek LIKE '%$cari' OR tbl_md_kendaraan.Jenis LIKE '%$cari' OR tbl_md_kendaraan.JumlahKursi LIKE '%$cari%')";

//QUERY VOUCHER BBM
$sql = "SELECT NoBody, SUM(Kilometer) AS KM, SUM(JumlahLiter) AS LITER, SUM(JumlahBiaya) AS BIAYA
        FROM tbl_voucher_bbm
        WHERE MONTH(TglDicatat) = $bulan AND YEAR(TglDicatat) = $tahun 
        GROUP BY NoBody;";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
    echo("Err:".__LINE__);exit;
}
while($row=$db->sql_fetchrow($result)){
    $data_laporan[$row["NoBody"]]["NoBody"]	= $row["NoBody"];
    $data_laporan[$row["NoBody"]]["KM"] = $row["KM"];
    $data_laporan[$row["NoBody"]]["LITER"] = $row["LITER"];
    $data_laporan[$row["NoBody"]]["BIAYA"] = $row["BIAYA"];
}

//QUERY AMBIL DATA MOBIL
$sql = "SELECT tbl_md_kendaraan.KodeKendaraan, tbl_md_kendaraan.NoPolisi, tbl_md_kendaraan.JumlahKursi
        FROM tbl_md_kendaraan $kondisi";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
    echo("Err:".__LINE__);exit;
}


//PLOT KE ARRAY PENAMPUNGAN
$temp_array=array();

$idx=0;
while ($row = $db->sql_fetchrow($result)){
    $temp_array[$idx]['KodeKendaraan']	= $row['KodeKendaraan'];
    $temp_array[$idx]['NoPolisi']		= $row['NoPolisi'];
    $temp_array[$idx]['LayoutKursi']	= $row['JumlahKursi'];
    $temp_array[$idx]['KM']            = $data_laporan[$row["KodeKendaraan"]]["KM"];
    $temp_array[$idx]['LITER']          = $data_laporan[$row["KodeKendaraan"]]["LITER"];
    $temp_array[$idx]['BIAYA']          = $data_laporan[$row["KodeKendaraan"]]["BIAYA"];
    $idx++;
}

$i=1;

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Operasional Kendaraan '.$monthName.' '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'No. Polisi');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kode Kendaraan');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Kilometer');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'BBM');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'BIAYA');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

$idx=0;
$idx=0;

while ($idx<count($temp_array)){

    $idx_row=$idx+4;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['NoPolisi']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['KodeKendaraan']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['LayoutKursi']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($temp_array[$idx]['KM'],0,".",""));
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($temp_array[$idx]['LITER'],0,".",""));
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($temp_array[$idx]['BIAYA'],0,".",""));
    $idx++;

}

$temp_idx=$idx_row;

$idx_row++;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Operasional Kendaraan '.$monthName.' '.$tahun.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>