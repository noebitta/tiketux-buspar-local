<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);

$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 706;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//###############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Mobil	= new Mobil();
$Sopir	= new Sopir();
$Cabang	= new Cabang();

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $Sopir;
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	$opt_sopir="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
}

function setComboCabang($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

	
	if ($mode=='add'){
		// add 
		
		if($userdata['user_level']==$USER_LEVEL_INDEX['MEKANIK']){
			exit;
		}
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'mobil/add_body.tpl'));

		$template->assign_vars(array(
		 'BCRUMP'	=>setBcrump($id_page),
		 'JUDUL'	=>'Tambah Data Mobil',
		 'MODE'   => 'save',
		 'SUB'    => '0',
		 'SOPIR1'  => setComboSopir(""),
		 'SOPIR2'  => setComboSopir(""),
		 'SOPIR3'  => setComboSopir(""),
		 'OPT_CABANG' 			=> setComboCabang(""),
		 'KURSI' 						=> setComboLayoutKursi(8),
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_MOBIL_ADD_ACT'	=> append_sid('pengaturan_mobil.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		if($userdata['user_level']==$USER_LEVEL_INDEX['MEKANIK']){
			exit;
		}
		
		// aksi menambah mobil
		$kode_kendaraan  	= str_replace(" ","",$HTTP_POST_VARS['kode_kendaraan']);
		$kode_kendaraan_old = str_replace(" ","",$HTTP_POST_VARS['kode_kendaraan_old']);
		$no_polisi  		= str_replace(" ","",$HTTP_POST_VARS['no_polisi']);
		$no_polisi_old		= str_replace(" ","",$HTTP_POST_VARS['no_polisi_old']);
		$jenis   			= $HTTP_POST_VARS['jenis'];
		$merek   			= $HTTP_POST_VARS['merek'];
		$tahun_pembuatan	= $HTTP_POST_VARS['tahun_pembuatan'];
		$warna   			= $HTTP_POST_VARS['warna'];
		$jumlah_kursi		= $HTTP_POST_VARS['jumlah_kursi'];
		$sopir1   			= $HTTP_POST_VARS['sopir1'];
		$sopir2   			= $HTTP_POST_VARS['sopir2'];
		$sopir3   			= $HTTP_POST_VARS['sopir3'];
		$no_mesin   		= $HTTP_POST_VARS['no_mesin'];
		$no_stnk   			= $HTTP_POST_VARS['no_stnk'];
		$no_rangka 			= $HTTP_POST_VARS['no_rangka'];
		$no_bpkb   			= $HTTP_POST_VARS['no_bpkb'];
		$km   				= $HTTP_POST_VARS['km'];
		$cabang   			= $HTTP_POST_VARS['cabang'];
		$status_aktif   	= $HTTP_POST_VARS['aktif'];
        $status_dt         	= $HTTP_POST_VARS['DT'];
		$dongkrak			= $HTTP_POST_VARS['dongkrak'];
		$ban_serep			= $HTTP_POST_VARS['ban_serep'];
        $kunci_roda         = $HTTP_POST_VARS['kunci_roda'];
        $p3k                = $HTTP_POST_VARS['p3k'];
        $apar               = $HTTP_POST_VARS['apar'];
		$tempo_pajak		= $HTTP_POST_VARS['tempo_pajak'];
		$nokir				= $HTTP_POST_VARS['nokir'];
		$kir				= $HTTP_POST_VARS['kir'];
		$nosipa				= $HTTP_POST_VARS['nosipa'];
		$sipa				= $HTTP_POST_VARS['sipa'];
		$nokp				= $HTTP_POST_VARS['nokp'];
		$kp					= $HTTP_POST_VARS['kp'];

		if($tempo_pajak != null){
			$tempo_pajak = date_format(date_create($tempo_pajak),'Y-m-d');
		}else{
			$tempo_pajak ="";
		}
		$tempo_stnk			= $HTTP_POST_VARS['tempo_stnk'];
		if($tempo_stnk != null){
			$tempo_stnk = date_format(date_create($tempo_stnk),'Y-m-d');
		}else{
			$tempo_stnk = "";
		}

		if($kir != null){
			$kir = date_format(date_create($kir),'Y-m-d');
		}else{
			$kir="";
		}

		if($sipa != null){
			$sipa = date_format(date_create($sipa),'Y-m-d');
		}else{
			$sipa = "";
		}

		if($kp != null){
			$kp = date_format(date_create($kp),'Y-m-d');
		}else{
			$kp = "";
		}
		
		$terjadi_error=false;
		
		if($Mobil->periksaDuplikasi($kode_kendaraan) && $kode_kendaraan!=$kode_kendaraan_old){
			$pesan="<font color='white' size=3>Kode Kendaraan yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else if($Mobil->periksaDuplikasiNoPol($no_polisi) && $no_polisi!=$no_polisi_old){
			$pesan="<font color='white' size=3>No Polisi yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Mobil";

//				$tempo_pajak = date_format(date_create($tempo_pajak),'Y-m-d');
//				$tempo_stnk  = date_format(date_create($tempo_stnk),'Y-m-d');
				$kelengkapan = $dongkrak.",".$ban_serep.",".$kunci_roda.",".$p3k.",".$apar;

				if($Mobil->tambah(
					$kode_kendaraan, $cabang, $no_polisi,
				  $jenis,$merek, $tahun_pembuatan, $warna,
				  $jumlah_kursi, $sopir1, $sopir2,$sopir3,
				  $no_stnk, $no_bpkb, $no_rangka,
				  $no_mesin, $km, $status_aktif, $kelengkapan, $tempo_pajak, $tempo_stnk,$nokir,$kir,$nosipa,$sipa,$nokp,$kp,$status_dt)){
					
					redirect(append_sid('pengaturan_mobil.'.$phpEx.'?mode=add&pesan=1',true));
					
				}
			}
			else{
				
				$judul="Ubah Data Mobil";

        $kelengkapan = $dongkrak.",".$ban_serep.",".$kunci_roda.",".$p3k.",".$apar;

				if($Mobil->ubah(
					$kode_kendaraan_old,
					$kode_kendaraan, $cabang, $no_polisi,
				  $jenis,$merek, $tahun_pembuatan, $warna,
				  $jumlah_kursi, $sopir1, $sopir2,$sopir3,
				  $no_stnk, $no_bpkb, $no_rangka,
				  $no_mesin, $km, $status_aktif,$kelengkapan, $tempo_pajak, $tempo_stnk,$nokir,$kir,$nosipa,$sipa,$nokp,$kp,$status_dt)){
						
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}
			
		}
		
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";

        $temp_var_dt = "DT_".$status_dt;
        $$temp_var_dt="selected";

		if($dongkrak != null){
			$DONGKRAK = "checked";
		}
		if($ban_serep != null){
			$BANSEREP = "checked";
		}
		if($kunci_roda != null){
		    $KUNCIRODA = "checked";
        }
        if($p3k != null){
            $P3K = "checked";
        }
        if($apar != null){
            $APAR = "checked";
        }
		
		$template->set_filenames(array('body' => 'mobil/add_body.tpl'));

		$template->assign_vars(array(
			 'BCRUMP'	=>setBcrump($id_page),
			 'JUDUL'	=>$judul,
			 'MODE'   => 'save',
			 'SUB'    => $submode,
			 'KODE_KENDARAAN_OLD'=> $kode_kendaraan_old,
			 'KODE_KENDARAAN'    => $kode_kendaraan,
			 'NO_POLISI_OLD'     => $no_polisi_old,
			 'NO_POLISI'    	 => $no_polisi,
			 'JENIS'    		 => $jenis,
			 'MEREK'    		 => $merek,
			 'TAHUN_PEMBUATAN'	 => $tahun_pembuatan,
			 'WARNA'			 => $warna,
			 'DONGKRAK'			 => $DONGKRAK,
			 'BANSEREP'			 => $BANSEREP,
             'KUNCIRODA'         => $KUNCIRODA,
             'P3K'               => $P3K,
             'APAR'              => $APAR,
			 'KURSI'			 => setComboLayoutKursi($jumlah_kursi),
			 'SOPIR1'  			 => setComboSopir($sopir1),
			 'SOPIR2'  			 => setComboSopir($sopir2),
			 'SOPIR3'			 => setComboSopir($sopir3),
			 'NO_MESIN'  		 => $no_mesin,
			 'NO_RANGKA'  		 => $no_rangka,
			 'NO_STNK' 			 => $no_stnk,
			 'NO_BPKB' 			 => $no_bpkb,
			 'STNK'				 => date_format(date_create($tempo_stnk),'j-n-Y'),
			 'PAJAK'			 => date_format(date_create($tempo_pajak),'j-n-Y'),
			 'KIR'				 => date_format(date_create($kir),'j-n-Y'),
			 'NOKIR'			 => $nokir,
			 'NOSIPA'			 => $nosipa,
			 'SIPA'				 => $sipa,
			 'NOKP'				 => $nokp,
			 'KP'				 => $kp,
			 'KM'				 => $km,
			 'AKTIF_1'			 => $status_aktif_1,
			 'AKTIF_0'			 => $status_aktif_0,
			 'OPT_CABANG'		 => setComboCabang($cabang),
			 'PESAN'			 => $pesan,
			 'BGCOLOR_PESAN'	 => $bgcolor_pesan,
			 'U_MOBIL_ADD_ACT'	 =>append_sid('pengaturan_mobil.'.$phpEx)
			)
		);
		
	} 
	else if ($mode=='edit'){
		// edit
		if($userdata['user_level']==$USER_LEVEL_INDEX['MEKANIK']){
			exit;
		}
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Mobil->ambilDataDetail($id);
	
		
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";

        $temp_var_dt="DT_".$row['IsWH'];
        $$temp_var_dt="selected";

		$kelengkapan = explode(',',$row['Kelengkapan']);
		if(in_array("Dongkrak",$kelengkapan)){
			$DONGKRAK = "checked";
		}
		if(in_array("Ban Serep",$kelengkapan)){
			$BANSEREP = "checked";
		}
        if(in_array("Kunci Roda",$kelengkapan)){
            $KUNCIRODA = "checked";
        }
        if(in_array("P3K",$kelengkapan)){
            $P3K = "checked";
        }
        if(in_array("Apar",$kelengkapan)){
            $APAR = "checked";
        }


		if($row['TglStnk'] != null){
			$tglStnk = date_format(date_create($row['TglStnk']),'j-n-Y');
		}else{
			$tglStnk = "";
		}
		if($row['TglPajak'] != null){
			$tglPajak = date_format(date_create($row['TglPajak']),'j-n-Y');
		}else{
			$tglPajak = "";
		}
		if($row['Kir'] != null) {
			$tglkir = date_format(date_create($row['Kir']), 'j-n-Y');
		}else{
				$tglkir = "";
		}
		if($row['TglSipa'] != null){
			$tglSipa = date_format(date_create($row['TglSipa']),'j-n-Y');
		}else{
			$tglSipa = "";
		}
		if($row['TglKp'] != null){
			$tglKp = date_format(date_create($row['TglKp']),'j-n-Y');
		}else{
			$tglKp = "";
		}


		$template->set_filenames(array('body' => 'mobil/add_body.tpl'));

		$template->assign_vars(array(
			 'BCRUMP'	=>setBcrump($id_page),
			 'JUDUL'	=> 'Ubah Data Mobil',
			 'MODE'   => 'save',
			 'SUB'    => '1',
			 'KODE_KENDARAAN_OLD'=> $row['KodeKendaraan'],
			 'KODE_KENDARAAN'   => $row['KodeKendaraan'],
			 'NO_POLISI_OLD'    => $row['NoPolisi'],
			 'NO_POLISI'    	=> $row['NoPolisi'],
			 'JENIS'    		=> $row['Jenis'],
			 'MEREK'    		=> $row['Merek'],
			 'TAHUN_PEMBUATAN'	=> $row['Tahun'],
			 'WARNA'			=> $row['Warna'],
			 'DONGKRAK'			=> $DONGKRAK,
			 'BANSEREP'			=> $BANSEREP,
        'KUNCIRODA'        => $KUNCIRODA,
        'P3K'              => $P3K,
        'APAR'             => $APAR,
			 'KURSI'			=> setComboLayoutKursi($row['JumlahKursi']),
			 'SOPIR1'  			=> setComboSopir($row['KodeSopir1']),
			 'SOPIR2'  			=> setComboSopir($row['KodeSopir2']),
			 'SOPIR3'  			=> setComboSopir($row['KodeSopir3']),
			 'NO_MESIN'  		=> $row['NoMesin'],
			 'NO_RANGKA'  		=> $row['NoRangka'],
			 'NO_STNK' 			=> $row['NoSTNK'],
			 'NO_BPKB' 			=> $row['NoBPKB'],
			 'KIR'				=> $tglkir,
			 'SIPA'				=> $tglSipa,
			 'KP'				=> $tglKp,
			 'NOKIR'			=> $row['NoKir'],
			 'NOSIPA'			=> $row['Sipa'],
			 'NOKP'				=> $row['Kp'],
			 'STNK'				=> $tglStnk,
			 'PAJAK'			=> $tglPajak,
			 'KM'				=> $row['KilometerAkhir'],
			 'AKTIF_1'			=> $status_aktif_1,
			 'AKTIF_0'			=> $status_aktif_0,
             'DT_1'             => $DT_1,
             'DT_0'             => $DT_0,
			 'OPT_CABANG'		=> setComboCabang($row['KodeCabang']),
			 'PESAN'			=> $pesan,
			 'BGCOLOR_PESAN'	=> $bgcolor_pesan,
			 'U_MOBIL_ADD_ACT'	=>append_sid('pengaturan_mobil.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus mobil
		if($userdata['user_level']==$USER_LEVEL_INDEX['MEKANIK']){
			exit;
		}
		
		$list_mobil = str_replace("\'","'",$HTTP_GET_VARS['list_mobil']);
		//echo($list_mobil. " asli :".$HTTP_GET_VARS['list_mobil']);
		$Mobil->hapus($list_mobil);
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		$kode_kendaraan = str_replace("\'","'",$HTTP_GET_VARS['kode_kendaraan']);
		$remark 				= $HTTP_GET_VARS['remark'];
		$status 				= $HTTP_GET_VARS['status']==1?"Dinonaktifkan pada<br>".dateparse(FormatMySQLDateToTglWithTime(date("Y-m-d H:i:s")))."<br>Karena ":"Diaktifkan pada<br>".dateparse(FormatMySQLDateToTglWithTime(date("Y-m-d H:i:s")));
	
		$Mobil->ubahStatusAktif($kode_kendaraan,$status.$remark."<br>By:".$userdata['nama']);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'mobil/mobil_body.tpl'));
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":" WHERE KodeKendaraan LIKE '%$cari%' OR Merek LIKE '%$cari%' OR NoSTNK LIKE '%$cari%' OR NoPolisi LIKE '%$cari%' ";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"KodeKendaraan","tbl_md_kendaraan","&cari=$cari",$kondisi,"pengaturan_mobil.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT KodeKendaraan,NoPolisi,Merek,FlagAktif,TglPajak,TglStnk,Kir,TglSipa,TglKp,
			Jenis,JumlahKursi,(SELECT Nama FROM tbl_md_sopir WHERE KodeSopir=KodeSopir1) AS Sopir,
			(SELECT Nama FROM tbl_md_sopir WHERE KodeSopir=KodeSopir2) AS Sopir2,
			(SELECT Nama FROM tbl_md_sopir WHERE KodeSopir=KodeSopir3) AS Sopir3,
			Remark
			FROM tbl_md_kendaraan $kondisi 
			ORDER BY KodeKendaraan LIMIT $idx_awal_record,$VIEW_PER_PAGE";

		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
			$no=1;
		  while ($row = $db->sql_fetchrow($result)){

				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
			  $today = new DateTime(date('Y-m-d'));
			  $jatuh_tempo_stnk = new DateTime($row['TglStnk']);
			  $jatuh_tempo_pajak= new DateTime($row['TglPajak']);
			  $jatuh_tempo_kir	= new DateTime($row['Kir']);
			  $jatuh_tempo_sipa = new DateTime($row['TglSipa']);
			  $jatuh_tempo_kp	= new DateTime($row['TglKp']);

			  $interval_stnk = $jatuh_tempo_stnk->diff($today)->format('%a');
			  $interval_pajak= $jatuh_tempo_pajak->diff($today)->format('%a');
			  $interval_kir	 = $jatuh_tempo_kir->diff($today)->format('%a');
			  $interval_sipa = $jatuh_tempo_sipa->diff($today)->format('%a');
			  $interval_kp	 = $jatuh_tempo_kp->diff($today)->format('%a');

			  if(($interval_stnk <= 7 && (strtotime($row['TglStnk']) >= strtotime(date('Y-m-d')))) ||
					  $interval_pajak <= 7 && (strtotime($row['TglPajak']) >= strtotime(date('Y-m-d'))) ||
					  $interval_kir <= 7 && (strtotime($row['Kir']) >= strtotime(date('Y-m-d'))) ||
					  $interval_sipa <= 7 && (strtotime($row['TglSipa']) >= strtotime(date('Y-m-d')))	||
					  $interval_kp <= 7 && (strtotime($row['TglKp']) >= strtotime(date('Y-m-d')))
			  )
			 {
				 if($interval_stnk <= 7 && (strtotime($row['TglStnk']) >= strtotime(date('Y-m-d')))){
					 $stnk = '<td align="right" valign="top" bgcolor="red"><font size="4" color="yellow"><b>'.date_format($jatuh_tempo_stnk,"d-m-Y").'</b></font></td>';
				 }else{
					 $stnk = '<td align="right" valign="top"><font size="4"><b>'.date_format($jatuh_tempo_stnk,"d-m-Y").'</b></font></td>';
				 }

				 if($interval_pajak <= 7 && (strtotime($row['TglPajak']) >= strtotime(date('Y-m-d')))){
					 $pajak = '<td align="right" valign="top" bgcolor="red"><font size="4" color="yellow"><b>'.date_format($jatuh_tempo_pajak,"d-m-Y").'</b></font></td>';
				 }else{
					 $pajak = '<td align="right" valign="top" ><font size="4"><b>'.date_format($jatuh_tempo_pajak,"d-m-Y").'</b></font></td>';
				 }

				 if($interval_kir <= 7  && (strtotime($row['Kir']) >= strtotime(date('Y-m-d')))){
					 $kir = '<td align="right" valign="top" bgcolor="red"><font size="4"  color="yellow"><b>'.date_format($jatuh_tempo_kir,"d-m-Y").'</b></font></td>';
				 }else{
					 $kir = '<td align="right" valign="top" ><font size="4"><b>'.date_format($jatuh_tempo_kir,"d-m-Y").'</b></font></td>';
				 }

				 if($interval_sipa <= 7  && (strtotime($row['TglSipa']) >= strtotime(date('Y-m-d')))){
					 $sipa = '<td align="right" valign="top" bgcolor="red"><font size="4"  color="yellow"><b>'.date_format($jatuh_tempo_sipa,"d-m-Y").'</b></font></td>';
				 }else{
					 $sipa = '<td align="right" valign="top" ><font size="4"><b>'.date_format($jatuh_tempo_sipa,"d-m-Y").'</b></font></td>';
				 }

				 if($interval_kp <= 7  && (strtotime($row['TglKp']) >= strtotime(date('Y-m-d')))){
					 $kp = '<td align="right" valign="top" bgcolor="red"><font size="4"  color="yellow"><b>'.date_format(date_create($row['TglKp']),"d-m-Y").'</b></font></td>';
				 }else{
					 $kp = '<td align="right" valign="top" ><font size="4"><b>'.date_format(date_create($row['TglKp']),"d-m-Y").'</b></font></td>';
				 }

				  $template->
				  assign_block_vars(
						  'ROWALERT',
						  array(
							      'no'=>$no,
							      'body'=>$row['KodeKendaraan'],
								  'kendaraan'=>$row['NoPolisi'],
								  'stnk'=>$stnk,
								  'pajak'=>$pajak,
								  'kir'=>$kir,
								  'kp'=>$kp,
								  'sipa'=>$sipa
						  )
				  );
				 $no++;
			  }
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[KodeKendaraan]'\"/>";
				
				if($userdata['user_level']!=$USER_LEVEL_INDEX['MEKANIK']){
					$act 	="<a href='".append_sid('pengaturan_mobil.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
					$act .="<a  href='' onclick='return hapusData(\"$row[KodeKendaraan]\");'>Delete</a>";
				}
				else{
					$act="NONE";
				}
				
				if($row['FlagAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeKendaraan]\",1)'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeKendaraan]\",0)'>Tidak Aktif</a>";
				}

				$rowstnk = ($row['TglStnk'] != "")?date_format(date_create($row['TglStnk']),'d-m-Y'):$rowstnk="";
			    $rowpajak= ($row['TglPajak'] != "")?date_format(date_create($row['TglPajak']),'d-m-Y'):$rowpajak="";
			    $rowkir  = ($row['Kir'] != "")?date_format(date_create($row['Kir']),'d-m-Y'):$rowkir="";
				$rowsipa = ($row['TglSipa'] != "")?date_format(date_create($row['TglSipa']),'d-m-Y'):$rowsipa="";
			    $rowkp = ($row['TglKp'] != "" && $row['TglKp'] != "0000-00-00")?date_format(date_create($row['TglKp']),'d-m-Y'):$rowkp="";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'kode'=>$row['KodeKendaraan'],
							'plat'=>$row['NoPolisi'],
							'stnk'=>$rowstnk,
							'pajak'=>$rowpajak,
							'kir'=>$rowkir,
							'sipa'=>$rowsipa,
							'kp'=>$rowkp,
							'merek'=>$row['Merek']." ".$row['Jenis'],
							'kapasitas'=>$row['JumlahKursi'],
							'sopir'=>$row['Sopir']."<br>".$row['Sopir2']."<br>".$row['Sopir3'],
							'aktif'=>$status,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
			
		} 
		else{
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$page_title	 ="Pengaturan Mobil";
		
		if($userdata['user_level']!=$USER_LEVEL_INDEX['MEKANIK']){

			$template->assign_vars(array(
				'BCRUMP'    		=>setBcrump($id_page),
				'U_MOBIL_ADD'		=> append_sid('pengaturan_mobil.'.$phpEx.'?mode=add'),
				'ACTION_CARI'		=> append_sid('pengaturan_mobil.'.$phpEx),
				'TXT_CARI'			=> $cari,
				'NO_DATA'				=> $no_data,
				'PAGING'				=> $paging
				)
			);
			
			$template->assign_block_vars('operasi',array());
		}
		else{
			//JIKA USER ADALAH MEKANIK

			$template->assign_vars(array(
				'BCRUMP'    		=>setBcrump($id_page),
				'U_MOBIL_ADD'		=> "",
				'ACTION_CARI'		=> append_sid('pengaturan_mobil.'.$phpEx),
				'TXT_CARI'			=> $cari,
				'NO_DATA'				=> $no_data,
				'PAGING'				=> $paging
				)
			);
		
		}
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>