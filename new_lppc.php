<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 409;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################
//PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$cabang         = isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$awal           = isset($HTTP_GET_VARS['awal'])? $HTTP_GET_VARS['awal'] : $HTTP_POST_VARS['awal'];

$template->set_filenames(array('body' => 'laporan_penerimaan_pengeluaran/laporan_penerimaan_pengeluaran_body.tpl'));
if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"]))){
    $cabang         = $userdata['KodeCabang'];
    $secret = "display : none";
}else{
}

//inisialisasi
$today = strtotime('now');
$yesterday = date('Y-m-d',strtotime('-1 day', $today));
$tomorrow = date('Y-m-d',strtotime('+1 day', $today));
$USER = $userdata['nama'];
$Cabang	= new Cabang();
switch($mode){
    case 'validasi':
        $ID = $HTTP_GET_VARS['id'];
        $CABANG = $HTTP_GET_VARS['cabang'];
        $MODAL = str_replace(',','.',$HTTP_GET_VARS['modal']);
        $SALDO = str_replace('.','',$HTTP_GET_VARS['saldo']);
        $SETORAN = str_replace('.','',$HTTP_GET_VARS['setoran']);
        //UPDATE MODAL AKHIR HARI INI
        $sql = "UPDATE tbl_md_cabang_modal SET ModalAkhir = '$SALDO', JumlahTransaksi='$SETORAN', TglTransaksiModal=NOW(),
                Keterangan = 'DATA TELAH DIVALIDASI USER $USER' WHERE IDModal ='$ID'";
        if(!$data = $db->sql_query($sql)){
            echo("alert('GAGAL VALIDASI');");
            exit;
        }

        $sql = "SELECT * FROM tbl_md_cabang_modal WHERE KodeCabang = '$CABANG' AND TglTransaksiModal = '$tomorrow'";

        if(!$data = $db->sql_query($sql)){
            echo("alert('GAGAL CEK MODAL AWAL BESOK');");
            exit;
        }else{
            $ModalCABANG = $db->sql_fetchrow($data);
        }

        if($ModalCABANG['KodeCabang'] == null){
            //SET MODAL AWAL BESOK
            $sql = "INSERT INTO tbl_md_cabang_modal(KodeCabang,ModalAwal,Keterangan,TglTransaksiModal)VALUES('$CABANG',$SALDO,'MODAL AWAL BESOK DISET USER $USER',NOW() + INTERVAL 1 DAY)";
            if(!$data = $db->sql_query($sql)){
                echo("alert('GAGAL CLOSE DATA');");
                exit;
            }
        }else{
            //UPDATE MODAL AWAL BESOK KARENA TELAH DISET SEBELUMNYA
            $sql = "UPDATE tbl_md_cabang_modal SET ModalAwal = $SALDO, Keterangan ='MODAL  AWAL BESOK DIUPDATE USER $USER'WHERE KodeCabang = '$CABANG' AND TglTransaksiModal ='$tomorrow'";
            if(!$data = $db->sql_query($sql)){
                echo("alert('GAGAL UPDATE CLOSE DATA');");
                exit;
            }
        }

        echo ("alert('DATA TELAH DIVALIDASI');");
        exit;
    case 'manifest_realisasi':
        $biaya          = $HTTP_POST_VARS['biaya'];
        $data_manifest  = $HTTP_POST_VARS['data_manifest'];
        $sql = "UPDATE tbl_biaya_op SET Realisasi = $biaya WHERE NoSPJ = '$data_manifest' AND FlagJenisBiaya = 1";
        if(!$result = $db->sql_query($sql)){
            echo mysql_error();
            exit;
        }
        echo 1;
        exit;
}

$saldo_akhir = 0;
$awal = date_format(date_create($awal),"Y-m-d");

$sum_tunai = 0;
$sum_elevania = 0;
$sum_edc = 0;
$sum_member = 0;
$sum_voucher = 0;
$sum_transfer = 0;
$sum_tiketux = 0;
$sum_lpoc = 0;
$sum_biaya_tambahan = 0;

if($cabang != '' && $awal != '') {

    //QUERY AMBIL MODAL CABANG
    $sql = "SELECT IDModal, IS_NULL(ModalAwal,0) as ModalAwal FROM tbl_md_cabang_modal WHERE KodeCabang='$cabang' AND TglTransaksiModal = '$awal' ";

    if(!$data = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }else{
        $dataModal = $db->sql_fetchrow($modal);
        $modalcabang = ($dataModal['ModalAwal'] == null)?0:$dataModal['ModalAwal'];
    }

    //QUERY AMBIL MODAL FIX DI TABEL CABANG
    $sql = "SELECT ModalCabang FROM tbl_md_cabang WHERE KodeCabang='$cabang'";
    if(!$data = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }else{
        $result = $db->sql_fetchrow($modal);
        $MODALFIX = ($result['ModalCabang'] == null)?0:$result['ModalCabang'];
    }
//===========================================================================================================
    /**
     * Update 6-6-2016
     */
//===========================================================================================================
    //AMBIL DATA PAKET TUNAI BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql_paket_tunai = "SELECT sum(HargaPaket) AS paket_tunai, COUNT(HargaPaket) AS jml_paket_tunai, NoSPJ
                        FROM tbl_paket
                        WHERE TglBerangkat = '$awal' AND JenisPembayaran = 0 AND KodeCabang = '$cabang'
                        AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
                        GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql_paket_tunai)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_paket_tunai [$row['NoSPJ']]['TUNAI'] = $row['paket_tunai'];
        $data_paket_tunai [$row['NoSPJ']]['JML_TUNAI'] = $row['jml_paket_tunai'];
    }

    //AMBIL DATA PAKET EDC BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql_paket_edc = "SELECT sum(HargaPaket) AS paket_edc, COUNT(HargaPaket) AS jml_paket_edc, NoSPJ
                        FROM tbl_paket
                        WHERE TglBerangkat = '$awal' AND JenisPembayaran = 5 AND KodeCabang = '$cabang'
                        AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
                        GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql_paket_edc)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_paket_edc [$row['NoSPJ']]['EDC'] = $row['paket_edc'];
        $data_paket_edc [$row['NoSPJ']]['JML_EDC'] = $row['jml_paket_edc'];
    }
//===========================================================================================================
    //AMBIL DATA TIKET TUNAI BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(Total) AS tiket_tunai, COUNT(Total) AS jml_tiket_tunai, NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal' AND JenisPembayaran = 0 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
            GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_tunai [$row['NoSPJ']]['TUNAI'] = $row['tiket_tunai'];
        $data_tiket_tunai [$row['NoSPJ']]['JML_TUNAI'] = $row['jml_tiket_tunai'];
    }

    //AMBIL DATA TIKET EDC BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(Total) AS tiket_edc, COUNT(Total) AS jml_tiket_edc,NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal' AND JenisPembayaran = 5 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
            GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_edc [$row['NoSPJ']]['EDC'] = $row['tiket_edc'];
        $data_tiket_edc [$row['NoSPJ']]['JML_EDC'] = $row['jml_tiket_edc'];
    }

    //AMBIL DATA TIKET TRANSFER BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(Total) AS tiket_transfer, COUNT(Total) AS jml_tiket_transfer, NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal'  AND JenisPembayaran = 6 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_transafer [$row['NoSPJ']]['TRANSFER'] = $row['tiket_transfer'];
        $data_tiket_transafer [$row['NoSPJ']]['JML_TRANSFER'] = $row['jml_tiket_transfer'];
    }

    //AMBIL DATA TIKET MEMBER BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(Total) AS tiket_member, COUNT(Total) AS jml_tiket_member, NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal'  AND JenisPembayaran = 4 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_member [$row['NoSPJ']]['MEMBER'] = $row['tiket_member'];
        $data_tiket_member [$row['NoSPJ']]['JML_MEMBER'] = $row['jml_tiket_member'];
    }

    //AMBIL DATA TIKET VOUCHER BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(Total) AS tiket_voucher, COUNT(Total) AS jml_tiket_voucher, NoSPJ
            FROM tbl_reservasi
            JOIN tbl_voucher ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
            WHERE KodeVoucher NOT LIKE 'ELDAY%' AND Total > 0 AND TglBerangkat = '$awal'  
            AND JenisPembayaran = 3 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";

    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_voucher [$row['NoSPJ']]['VOUCHER'] = $row['tiket_voucher'];
        $data_tiket_vocuher [$row['NoSPJ']]['JML_VOUCHER'] = $row['jml_tiket_voucher'];
    }

    //AMBIL DATA TIKET VOUCHER ELEVANIA BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(HargaTiket) AS tiket_elevania, COUNT(Total) AS jml_tiket_elevania, NoSPJ
            FROM tbl_reservasi
            JOIN tbl_voucher ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
            WHERE KodeVoucher LIKE 'ELDAY%' AND IsReturn != 1 AND TglBerangkat = '$awal'  AND JenisPembayaran = 3 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";

    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_elevania [$row['NoSPJ']]['ELEVANIA'] = $row['tiket_elevania'];
        $data_tiket_elevania [$row['NoSPJ']]['JML_ELEVANIA'] = $row['jml_tiket_elevania'];
    }

    //AMBIL DATA TIKET TIKETUX BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
    $sql = "SELECT sum(Total) AS tiket_tiketux, COUNT(Total) AS jml_tiket_tiketux,NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal' AND JenisPembayaran = 99 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";
    if(!$tiket = $db->sql_query($sql)){
        die("Err:".__LINE__."<br>".mysql_error());
    }
    while($row = $db->sql_fetchrow($tiket)){
        $data_tiket_tiketux [$row['NoSPJ']]['TIKETUX'] = $row['tiket_tiketux'];
        $data_tiket_tiketux [$row['NoSPJ']]['JML_TIKETUX'] = $row['jml_tiket_tiketux'];
    }

    // END TIKET =======================================================================================

    //SET JURUSAN BERANGKAT DARI CABANG DIPILIH
        $sql  ="SELECT IdJurusan,KodeCabangTujuan 
                FROM tbl_md_jurusan 
                WHERE KodeCabangAsal ='$cabang' AND FlagAktif=1 
                ORDER BY KodeCabangTujuan";

    if(!$result=$db->sql_query($sql)){
        echo("Err:".__LINE__);exit;
    }

    $id_jurusan = "";
    while ($row = $db->sql_fetchrow($result)){
        $id_jurusan .= $row['IdJurusan'].',';
    }

    $id_jurusan = rtrim($id_jurusan,',');

    $id_jurusan = $id_jurusan != ""?$id_jurusan:0;

    //AMBIL KODE JADWAL UTAMA DARI LIST ID JURUSAN
    $sql = "SELECT IF(FlagSubJadwal = 1,KodeJadwalUtama,KodeJadwal) AS Jadwal
            FROM tbl_md_jadwal
            WHERE IdJurusan IN ($id_jurusan) 
            AND FlagAktif = 1
            ORDER BY Jadwal";

    if(!$result=$db->sql_query($sql)){
        echo("Err:".__LINE__);exit;
    }

    $kode_jadwal = "";
    while ($row = $db->sql_fetchrow($result)){
        $kode_jadwal .= "'".$row['Jadwal']."',";
    }

    $kode_jadwal = $kode_jadwal != ""?$kode_jadwal:"'KOSONG'";

    //BIAYA OPERASIONAL DARI SPJ
    $sql = "SELECT tbl_biaya_op.NoSPJ, SUM( Jumlah ) AS Biaya
            FROM tbl_biaya_op
            JOIN tbl_spj ON tbl_spj.NoSPJ = tbl_biaya_op.NoSPJ
            WHERE DATE(TglBerangkat) =  '$awal' 
            AND KodeJadwal IN (".rtrim($kode_jadwal,',').")
            AND tbl_biaya_op.FlagJenisBiaya = 1
            GROUP BY NoSPJ
            ORDER BY tbl_spj.IdJurusan, JamBerangkat";
    if(!$op = $db->sql_query($sql)){
        die("Err:".__LINE__);
    }
    while ($row = $db->sql_fetchrow($op)){
        $data_op[$row['NoSPJ']]['OP'] = $row['Biaya'];
    }

    //AMBIL DAFTAR SPJ DARI CABANG TERTENTU DAN TANGGALNYA
    $sql = "SELECT NoSPJ, KodeCabangAsal, TglBerangkat, 
                    JamBerangkat, Driver,tbl_md_jurusan.KodeJurusan, 
                    f_kendaraan_ambil_nopol_by_kode(NoPolisi) AS Mobil,
                    CONCAT(f_cabang_get_name_by_kode(KodeCabangAsal),'-',f_cabang_get_name_by_kode(KodeCabangTujuan)) AS trip
            FROM tbl_spj 
            JOIN tbl_md_jurusan ON tbl_spj.IdJurusan = tbl_md_jurusan.IdJurusan
            WHERE DATE(TglBerangkat) = '$awal' 
            AND KodeJadwal IN (".rtrim($kode_jadwal,',').")
            ORDER BY tbl_spj.KodeJadwal, JamBerangkat;";
    if(!$spj = $db->sql_query($sql)){
        die("Err:".mysql_error());
    }

    $paket_sql_spj = $sql;

    $list_no_spj = "";



    $idx=0;
    while($row = $db->sql_fetchrow($spj)){
        $data_spj [$idx]['NoSPJ']      = $row['NoSPJ'];
        $data_spj [$idx]['Jurusan']    = $row['KodeJurusan'];
        $data_spj [$idx]['rute']       = $row['trip'];
        $data_spj [$idx]['Sopir']      = $row['Driver'];
        $data_spj [$idx]['Mobil']      = $row['Mobil'];
        $data_spj [$idx]['Jam']        = $row['JamBerangkat'];
        $data_spj [$idx]['TUNAI']      = $data_tiket_tunai[$row['NoSPJ']]['TUNAI'];
        $data_spj [$idx]['EDC']        = $data_tiket_edc[$row['NoSPJ']]['EDC'];
        $data_spj [$idx]['VOUCHER']    = $data_tiket_voucher[$row['NoSPJ']]['VOUCHER'];
        $data_spj [$idx]['MEMBER']     = $data_tiket_member[$row['NoSPJ']]['MEMBER'];
        $data_spj [$idx]['TRANSFER']   = $data_tiket_transafer[$row['NoSPJ']]['TRANSFER'];
        $data_spj [$idx]['TIKETUX']    = $data_tiket_tiketux[$row['NoSPJ']]['TIKETUX'];
        $data_spj [$idx]['ELEVANIA']   = $data_tiket_elevania[$row['NoSPJ']]['ELEVANIA'];
        $data_spj [$idx]['OP']         = $data_op [$row['NoSPJ']]['OP'];
        $data_spj [$idx]['QTY']        = $data_tiket_tunai[$row['NoSPJ']]['JML_TUNAI']+$data_tiket_edc[$row['NoSPJ']]['JML_EDC']+
                                        $data_tiket_transafer[$row['NoSPJ']]['JML_TRANSFER']+$data_tiket_tiketux[$row['NoSPJ']]['JML_TIKETUX']+
                                        $data_tiket_voucher[$row['NoSPJ']]['JML_VOUCHER']+$data_tiket_member[$row['NoSPJ']]['JML_MEMBER']+
                                        $data_tiket_elevania[$row['NoSPJ']]['JML_ELEVANIA'];

        $sum_tunai      += $data_spj [$idx]['TUNAI'];
        $sum_edc        += $data_spj [$idx]['EDC'];
        $sum_voucher    += $data_spj [$idx]['VOUCHER'];
        $sum_member     += $data_spj [$idx]['MEMBER'];
        $sum_transfer   += $data_spj [$idx]['TRANSFER'];
        $sum_tiketux    += $data_spj [$idx]['TIKETUX'];
        $sum_elevania   += $data_spj [$idx]['ELEVANIA'];
        $sum_lpoc       += $data_spj [$idx]['OP'];

        $list_no_spj .= "'".$row['NoSPJ']."',";

        $idx++;
    }

        $list_no_spj = $list_no_spj != "" ? $list_no_spj : "'KOSONG'";

    // ambil list cabang bandung
    $cbg_bandung = $Cabang->ambilCabang("WHERE Kota = 'BANDUNG'");

    $list_cbg = array();
    while ($row = $db->sql_fetchrow($cbg_bandung)){
        array_push($list_cbg,$row['KodeCabang']);
    }

    $i = 0;
    while ($i<count($data_spj)){
        $odd ='odd';

        if (($i % 2)==0){
            $odd = 'even';
        }

        if(in_array($cabang,$list_cbg)){
            $x = ($data_spj[$i]['OP'] != "")?number_format($data_spj[$i]['OP'],0,',','.'):"";
            $realisasi = "<a onclick='dialog_realisasi.show(); getData(\"".$data_spj[$i]['NoSPJ']."\",\"".$data_spj [$i]['rute']."\",\"".$data_spj [$i]['Jam']."\")'><font color='red'>$x</font></a>";
        }else{
            $realisasi = ($data_spj[$i]['OP'] != "")?number_format($data_spj[$i]['OP'],0,',','.'):"";
        }

        $template->
        assign_block_vars(
            'TIKET',
            array(
                'no' => $i+1,
                'odd'=> $odd,
                'nospj'=>$data_spj[$i]['NoSPJ'],
                'jurusan'=>$data_spj[$i]['Jurusan'],
                'jam'=>date_format(date_create($data_spj[$i]['Jam']),'H:i'),
                'sopir'=>$data_spj[$i]['Sopir'],
                'mobil'=>$data_spj [$i]['Mobil'],
                'tunai'=>($data_spj[$i]['TUNAI'] != "")?number_format($data_spj[$i]['TUNAI'],0,',','.'):"",
                'edc'=>($data_spj[$i]['EDC'] != "")?number_format($data_spj[$i]['EDC'],0,',','.'):"",
                'voucher'=>($data_spj[$i]['VOUCHER'] != "")?number_format($data_spj[$i]['VOUCHER'],0,',','.'):"",
                'elevania'=>($data_spj [$i]['ELEVANIA'] != "")?number_format($data_spj [$i]['ELEVANIA'],0,',','.'):"",
                'member'=>($data_spj[$i]['MEMBER'] != "")?number_format($data_spj[$i]['MEMBER'],0,',','.'):"",
                'transfer'=>($data_spj[$i]['TRANSFER'] != "")?number_format($data_spj[$i]['TRANSFER'],0,',','.'):"",
                'tiketux'=>($data_spj[$i]['TIKETUX'] != "")?number_format($data_spj[$i]['TIKETUX'],0,',','.'):"",
                'lpoc'=>$realisasi,
                'jml_tiket'=>$data_spj [$i]['QTY']." Pax",
            )
        );
        
        $i++;
    }


//===========================================================================================================
    // tiket yang dibayar hari ini dan berangkat hari ini tapi counter berangkatnya beda dengan counter bayarnya
    // jadi pembayaranya di akui di counter bayarnya.

    $sql_tiket_ex = "SELECT tbl_reservasi.NoTiket, NoSPJ, tbl_reservasi.KodeJadwal, JamBerangkat, TglBerangkat, WaktuCetakTiket, Nama, JenisPembayaran,
                            IF(JenisPembayaran = 0,Total,0) AS tunai,
                            IF(JenisPembayaran = 5,Total,0) AS edc,
                            IF(JenisPembayaran = 3 AND LEFT(KodeVoucher,5) != 'ELDAY',Total,0) AS voucher,
                            IF(JenisPembayaran = 4,Total,0) AS member,
                            IF(JenisPembayaran = 6,Total,0) AS transfer,
                            IF(JenisPembayaran = 99,Total,0) AS tiketux,
                            IF(JenisPembayaran = 3 AND LEFT(KodeVoucher,5) = 'ELDAY',Total,0) AS elevania
                    FROM tbl_reservasi
                    LEFT JOIN tbl_voucher 
                    ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
                      WHERE Total > 0 AND FlagBatal !=1 AND CetakTiket =1
                      AND JenisPembayaran IN ( 0,3,4,5,6,99) 
                      AND DATE( WaktuCetakTiket ) =  '$awal'
                      AND DATE( WaktuCetakTiket ) = DATE( TglBerangkat )
                      AND tbl_reservasi.KodeCabang = '$cabang'
                      AND tbl_reservasi.NoSPJ NOT IN (".rtrim($list_no_spj,',').")
                    ORDER BY KodeJadwal;";

    if(!$data = $db->sql_query($sql_tiket_ex)){
        echo("Err:".__LINE__.mysql_error());
        exit();
    }

    while($row = $db->sql_fetchrow($data)) {
        $odd = 'odd';
        if (($i % 2) == 0) {
            $odd = 'even';
        }

        $sum_tunai += $row['tunai'];
        $sum_edc += $row['edc'];
        $sum_voucher += $row['voucher'];
        $sum_member += $row['member'];
        $sum_transfer += $row['transfer'];
        $sum_tiketux += $row['tiketux'];
        $sum_elevania += $row['elevania'];

        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd' => $odd,
                'no' => $i + 1,
                'tiket' => $row['NoTiket'],
                'penumpang' => $row['Nama'],
                'KodeJurusan' => $row['KodeJadwal'],
                'JamBerangkat' => date_format(date_create($row['JamBerangkat']), 'H:i'),
                'TglBerangkat' => date_format(date_create($row['TglBerangkat']), 'd-m-Y'),
                'Tunai' => ($row['tunai'] != 0) ? number_format($row['tunai'], 0, ',', '.') : "",
                'EDC' => ($row['edc'] != 0) ? number_format($row['edc'], 0, ',', '.') : "",
                'VOUCHER' => ($row['voucher'] != 0) ? number_format($row['voucher'], 0, ',', '.') : "",
                'ELEVANIA' => ($row['elevania'] != 0) ? number_format($row['elevania'], 0, ',', '.') : "",
                'MEMBER' => ($row['member'] != 0) ? number_format($row['member'], 0, ',', '.') : "",
                'TRANSFER' => ($row['transfer'] != 0) ? number_format($row['transfer'] + $row['elevania'], 0, ',', '.') : "",
                'Sistem' => ($row['tiketux'] != 0) ? number_format($row['tiketux'], 0, ',', '.') : "",
            )
        );
        $i++;
    }

//===========================================================================================================
    //tiket yang dibayar hari ini tapi keberangkatannya bukan hari ini di list satu2
    //ambil tiket percabang


    $sql_tiket = "SELECT tbl_reservasi.NoTiket, NoSPJ, tbl_reservasi.KodeJadwal, JamBerangkat, TglBerangkat, WaktuCetakTiket, Nama, JenisPembayaran,
                            IF(JenisPembayaran = 0,Total,0) AS tunai,
                            IF(JenisPembayaran = 5,Total,0) AS edc
                    FROM tbl_reservasi
                    LEFT JOIN tbl_voucher 
                    ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
                      WHERE Total > 0 AND FlagBatal !=1 AND CetakTiket =1
                      AND JenisPembayaran IN ( 0,5) 
                      AND DATE( WaktuCetakTiket ) =  '$awal'
                      AND DATE( WaktuCetakTiket ) < DATE( TglBerangkat )
                      AND tbl_reservasi.KodeCabang = '$cabang'
                    ORDER BY KodeJadwal;";

    if(!$data = $db->sql_query($sql_tiket)){
        echo("Err:".__LINE__.mysql_error());
    }

    while($row = $db->sql_fetchrow($data)){
        $odd ='odd';
        if (($i % 2)==0){
            $odd = 'even';
        }

        $sum_tunai += $row['tunai'];
        $sum_edc += $row['edc'];
        $sum_voucher += $row['voucher'];
        $sum_member += $row['member'];
        $sum_transfer += $row['transfer'];
        $sum_tiketux += $row['tiketux'];
        $sum_elevania += $row['elevania'];

        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'           =>$odd,
                'no'            =>$i+1,
                'tiket'         => $row['NoTiket'],
                'penumpang'     =>$row['Nama'],
                'KodeJurusan'   =>$row['KodeJadwal'],
                'JamBerangkat'  =>date_format(date_create($row['JamBerangkat']),'H:i'),
                'TglBerangkat'  =>date_format(date_create($row['TglBerangkat']),'d-m-Y'),
                'Tunai'         =>($row['tunai'] != 0)?number_format($row['tunai'],0,',','.'):"",
                'EDC'           =>($row['edc'] != 0)?number_format($row['edc'],0,',','.'):"",
                'VOUCHER'       =>($row['voucher'] != 0)?number_format($row['voucher'],0,',','.'):"",
                'ELEVANIA'      =>($row['elevania'] != 0)?number_format($row['elevania'],0,',','.'):"",
                'MEMBER'        =>($row['member'] != 0)?number_format($row['member'],0,',','.'):"",
                'TRANSFER'      =>($row['transfer'] != 0)?number_format($row['transfer']+$row['elevania'],0,',','.'):"",
                'Sistem'        =>($row['tiketux'] != 0)?number_format($row['tiketux'],0,',','.'):"",
            )
        );
        $i++;
    }
//===================================================================================================================
//TOP UP DEPOSIT MEMBER PERCABANG

    $sql_topup = "SELECT * FROM tbl_member_deposit_topup_log
                  JOIN tbl_md_member ON tbl_md_member.IdMember = tbl_member_deposit_topup_log.IdMember
                  WHERE KodeCabang = '$cabang' AND DATE(WaktuTransaksi) = '$awal' AND IsVerified = 1";
    if(!$topup = $db->sql_query($sql_topup)){
        die("error : ".__LINE__);
    }

    $index = 0;
    while ($row = $db->sql_fetchrow($topup)){
        $odd ='odd';

        if (($index % 2)==0){
            $odd = 'even';
        }

        $sum_member += $row['JumlahRupiah'];

        $template->
        assign_block_vars(
            'MEMBER',
            array(
                'no' => $i+1,
                'odd'=> $odd,
                'keterangan'=> $row['NoSeriKartu']." / ".$row['Nama'],
                'jumlah'=> number_format($row['JumlahRupiah'],0,',','.')
            )
        );

        $i++;
        $index++;
    }


//===========================================================================================================
    // paket yang dibayar hari ini dan berangkat hari ini
    // di group by manifest

        if(!$spj = $db->sql_query($paket_sql_spj)){
            die("Err:".mysql_error());
        }

        $idx=0;
        while($row = $db->sql_fetchrow($spj)){
            $data_spj_paket [$idx]['NoSPJ']      = $row['NoSPJ'];
            $data_spj_paket [$idx]['Jurusan']    = $row['KodeJurusan'];
            $data_spj_paket [$idx]['Sopir']      = $row['Driver'];
            $data_spj_paket [$idx]['Mobil']      = $row['Mobil'];
            $data_spj_paket [$idx]['Jam']        = $row['JamBerangkat'];
            $data_spj_paket [$idx]['PAKETTUNAI'] = $data_paket_tunai[$row['NoSPJ']]['TUNAI'];
            $data_spj_paket [$idx]['PAKETEDC']   = $data_paket_edc[$row['NoSPJ']]['EDC'];
            $data_spj_paket [$idx]['QTY']        = $data_paket_tunai[$row['NoSPJ']]['JML_TUNAI']+$data_paket_edc[$row['NoSPJ']]['JML_EDC'];

            $sum_tunai      += $data_spj_paket [$idx]['PAKETTUNAI'];
            $sum_edc        += $data_spj_paket [$idx]['PAKETEDC'];
            $idx++;
        }

        $index = 0;
        while ($index<count($data_spj_paket)){
            $odd ='odd';

            if (($index % 2)==0){
                $odd = 'even';
            }

            if($data_spj_paket [$index]['QTY'] > 0){
                $template->
                assign_block_vars(
                    'PAKET',
                    array(
                        'no' => $i+1,
                        'odd'=> $odd,
                        'nospj'=>$data_spj_paket[$index]['NoSPJ'],
                        'jurusan'=>$data_spj_paket[$index]['Jurusan'],
                        'jam'=>date_format(date_create($data_spj_paket[$index]['Jam']),'H:i'),
                        'sopir'=>$data_spj_paket[$index]['Sopir'],
                        'mobil'=>$data_spj_paket [$index]['Mobil'],
                        'paket_tunai'=>($data_spj_paket[$index]['PAKETTUNAI'] != "")?number_format($data_spj_paket[$index]['PAKETTUNAI'],0,',','.'):"",
                        'paket_edc'=>($data_spj_paket[$index]['PAKETEDC'] != "")?number_format($data_spj_paket[$index]['PAKETEDC'],0,',','.'):"",
                        'jml_paket'=>$data_spj_paket [$index]['QTY']." Paket",
                    )
                );
                $i++;
            }

            $index++;

        }


//===========================================================================================================
    //paket yang dibayar hari ini tapi keberangkatanya untuk hari berikutnya di list satu2
    //ambil paket percabang
    $sql_paket = "SELECT *, IF(JenisPembayaran = 0,HargaPaket,0) AS tunai, IF(JenisPembayaran = 5,HargaPaket,0) AS edc 
                FROM tbl_paket
                JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
                WHERE FlagBatal != 1 AND CetakTiket = 1 
                AND JenisPembayaran IN (0,5) 
                AND tbl_paket.KodeCabang = '$cabang'
                AND DATE(WaktuCetakTiket) = '$awal' 
                AND DATE(WaktuCetakTiket) < DATE(TglBerangkat);";

    if(!$data = $db->sql_query($sql_paket)){
        die("Err:".__LINE__);
    }

    while($row = $db->sql_fetchrow($data)){
        $odd ='odd';


        if (($i % 2)==0){
            $odd = 'even';
        }

        $sum_tunai += $row['tunai'];
        $sum_edc += $row['edc'];

        $template->
        assign_block_vars(
            'PAKET_PLUS',
            array(
                'odd'           =>$odd,
                'no'            =>$i+1,
                'paket'         => $row['NoTiket'],
                'pengirim'      =>$row['NamaPengirim'],
                'penerima'      =>$row['NamaPenerima'],
                'KodeJurusan'   =>$row['KodeJadwal'],
                'JamBerangkat'  =>date_format(date_create($row['JamBerangkat']),'H:i'),
                'TglBerangkat'  =>date_format(date_create($row['TglBerangkat']),'d-m-Y'),
                'Tunai'         =>($row['tunai'] != 0)?number_format($row['tunai'],0,',','.'):"",
                'EDC'           =>($row['edc'] != 0)?number_format($row['edc'],0,',','.'):"",
            )
        );
        $i++;
    }

//==========================================================================================================
    //QUERY BIAYA TAMBAHAN
    $sql = "SELECT KodeCabang,SUM(Jumlah) AS Jumlah,KodeJurusan, Keterangan, NamaPenerima, JenisBiaya, f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) AS mobil
            FROM tbl_biaya_tambahan
            WHERE DATE(TglCetak)='$awal' AND KodeCabang = '$cabang'
            GROUP BY NamaPenerima, TglCetak ";
    if (!$other = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }


    if($db->sql_numrows($other)){
        $sum_other = 0;
        $index = $i+1;
        while($dataother = $db->sql_fetchrow($other)){
            $sum_biaya_tambahan += $dataother['Jumlah'];

            if (($index % 2)==0){
                $odd = 'even';
            }

            $template->
            assign_block_vars(
                'ROWBIAYA',
                array(
                    'odd'           =>$odd,
                    'no'            =>$index,
                    'Penerima'      =>$dataother['NamaPenerima'],
                    'Jurusan'       =>$dataother['KodeJurusan']." ".$dataother['mobil'],
                    'Keterangan'    =>$dataother['Keterangan'],
                    'Jumlah'        =>number_format($dataother['Jumlah'],0,',','.'),
                )
            );
            $index++;
        }
    }
    //END PENANGANAN BIAYA TAMBAHAN

    // jika pendapatan lebih besar dari pengeluaran maka modal jangan dipake
    if(($sum_tunai+$sum_member) >= ($sum_lpoc+$sum_biaya_tambahan)){
        $saldo_akhir = $modalcabang;
    }else{
        $saldo_akhir = $modalcabang + ($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan);
    }

    //jika modal terakhir kurang dari MODALFIX maka harus digenapkan lagi jadi MODALFIX
    if($modalcabang < $MODALFIX){
        if((($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan)) > ($MODALFIX- $modalcabang)){
            $saldo_akhir = $MODALFIX;
        }else{
            $saldo_akhir = $modalcabang + ($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan);
        }
    }


    $setoran = ($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan);
    $interface_menu_utama=false;
    $parameter_cetak = "&p1=".$cabang."&p2=".$awal;
    $script_excel ="Start('laporan_penerimaan_pengeluaran_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
}


$page_title	= "LPPC";
// Dropdown Cabang
function setDropdownCabang($cabang_dipilih){
    //SET COMBO cabang
    global $db;
    global $Cabang;

    $result=$Cabang->ambilData("","Nama,Kota","ASC");
    $opt_cabang="<option value=''>none</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
            $opt_cabang .='<option value="'.$row['KodeCabang'].'" '.$selected.'>'.$row['Nama'].' ('.$row['Kota'].') ('.$row['KodeCabang'].')</option>';
        }
    }
    else{
        echo("Error :".__LINE__);exit;
    }
    return $opt_cabang;
}

$template->assign_vars(array(
    'BCRUMP'    		    =>setBcrump($id_page),
    'SELECT'            => setDropdownCabang($cabang),
    'AWAL'				      => date_format(date_create($awal),'d-m-Y'),
    'CABANG'            => $cabang,
    'IDMODAL'           => $dataModal['IDModal'],
    'MODAL'             => number_format($modalcabang,0,',','.'),
    'BLAIN'             => number_format($sum_biaya_tambahan,0,',','.'),
    'SALDO'             => number_format($saldo_akhir,0,',','.'),
    'SETORAN'           => number_format($setoran,0,',','.'),
    'TOTAL_CASH'        => number_format($sum_tunai,0,',','.'),
    'TOTAL_EDC'         => number_format($sum_edc,0,',','.'),
    'TOTAL_VOUCHER'     => number_format($sum_voucher,0,',','.'),
    'TOTAL_ELEVANIA'    => number_format($sum_elevania,0,',','.'),
    'TOTAL_MEMBER'      => number_format($sum_member,0,',','.'),
    'TOTAL_TRANSFER'    => number_format($sum_transfer,0,',','.'),
    'TOTAL_SISTEM'      => number_format($sum_tiketux,0,',','.'),
    'TOTAL_LPOC'        => number_format($sum_lpoc,0,',','.'),
    'ACTION_CARI'       => append_sid('new_lppc.'.$phpEx),
    'EXCEL'             => $script_excel,
    'SECRET'            => $secret
));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>