<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 219;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//################################################################################

//PARAMETER
$bulan          = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun          = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];
$cari   		= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$bulan          = $bulan==""?date("m"):$bulan;
$bulan          = (int)$bulan;
$tahun          = $tahun==""?date("Y"):$tahun;

$kondisi	=($cari=="")?"":" WHERE (tbl_md_kendaraan.KodeKendaraan LIKE '%$cari%' OR tbl_md_kendaraan.NoPolisi LIKE '%$cari' OR tbl_md_kendaraan.Merek LIKE '%$cari' OR tbl_md_kendaraan.Jenis LIKE '%$cari' OR tbl_md_kendaraan.JumlahKursi LIKE '%$cari%')";

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tbl_md_kendaraan.KodeKendaraan","tbl_md_kendaraan",
    "&cari=$cari&tahun=$tahun&bulan=$bulan",
    $kondisi,"operasional_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//QUERY VOUCHER BBM
$sql = "SELECT NoBody, SUM(Kilometer) AS KM, SUM(JumlahLiter) AS LITER, SUM(JumlahBiaya) AS BIAYA
        FROM tbl_voucher_bbm
        WHERE MONTH(TglDicatat) = $bulan AND YEAR(TglDicatat) = $tahun 
        GROUP BY NoBody;";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
    echo("Err:".__LINE__);exit;
}
while($row=$db->sql_fetchrow($result)){
    $data_laporan[$row["NoBody"]]["NoBody"]	= $row["NoBody"];
    $data_laporan[$row["NoBody"]]["KM"] = $row["KM"];
    $data_laporan[$row["NoBody"]]["LITER"] = $row["LITER"];
    $data_laporan[$row["NoBody"]]["BIAYA"] = $row["BIAYA"];
}

//QUERY AMBIL DATA MOBIL
$sql = "SELECT tbl_md_kendaraan.KodeKendaraan, tbl_md_kendaraan.NoPolisi, tbl_md_kendaraan.JumlahKursi
        FROM tbl_md_kendaraan $kondisi";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
    echo("Err:".__LINE__);exit;
}


//PLOT KE ARRAY PENAMPUNGAN
$temp_array=array();

$idx=0;
while ($row = $db->sql_fetchrow($result)){
    $temp_array[$idx]['KodeKendaraan']	= $row['KodeKendaraan'];
    $temp_array[$idx]['NoPolisi']		= $row['NoPolisi'];
    $temp_array[$idx]['LayoutKursi']	= $row['JumlahKursi'];
    $temp_array[$idx]['KM']            = $data_laporan[$row["KodeKendaraan"]]["KM"];
    $temp_array[$idx]['LITER']          = $data_laporan[$row["KodeKendaraan"]]["LITER"];
    $temp_array[$idx]['BIAYA']          = $data_laporan[$row["KodeKendaraan"]]["BIAYA"];
    $idx++;
}


$idx=$idx_awal_record;

while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)) {
    $odd = 'odd';

    if (($idx % 2) == 0) {
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'					=>$odd,
            'no'					=>$idx+1,
            'kode_kendaraan'		=>$temp_array[$idx]['KodeKendaraan'],
            'no_polisi'				=>$temp_array[$idx]['NoPolisi'],
            'kursi'                 =>$temp_array[$idx]['LayoutKursi'],
            'KM'                   =>number_format($temp_array[$idx]['KM'],0,',','.'),
            'LITER'                 =>number_format($temp_array[$idx]['LITER'],0,',','.'),
            'BIAYA'                 =>number_format($temp_array[$idx]['BIAYA'],0,',','.')
        )
    );

    $idx++;
}


//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&cari=".$cari."&bulan=".$bulan."&tahun=".$tahun;
$cetak = "Start('operasional_kendaraan_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//END KOMPONEN EXPORT

$template->assign_vars(array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('operasional_kendaraan.'.$phpEx),
        'BULAN'             => $bulan,
        'TAHUN'             => $tahun,
        'PAGE'              => $page,
        'CARI'			    => $cari,
        'PAGING'            =>$paging,
        'CETAK'             =>$cetak,
    )
);

$template->set_filenames(array('body' => 'operasional_kendaraan.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>

