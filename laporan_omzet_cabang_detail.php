<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 302;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);



// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO_PAKET'],$USER_LEVEL_INDEX['KEUANGAN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$filter_by 			= isset($HTTP_GET_VARS['filterby'])? $HTTP_GET_VARS['filterby'] : $HTTP_POST_VARS['filterby'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];


// LIST
$template->set_filenames(array('body' => 'laporan_rekap_tiket_detail/laporan_rekap_tiket_detail_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

//QUERY TIKET
$sql=
	"SELECT 
		NoTiket,tr.TglBerangkat,tr.KodeJadwal,
		tr.JamBerangkat,WaktuPesan,Nama,Telp,
		Alamat,Telp,NomorKursi,
		IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,HargaTiket,0),SubTotal) AS HargaTiket,SubTotal,
		IF(JenisPembayaran!=3,Discount,0) AS Discount,IF(JenisPembayaran!=3,Total,0) AS Total,
		IF(JenisPenumpang!='R',JenisDiscount,'RETURN') AS JenisDiscount,JenisPembayaran,
		tr.FlagBatal,CetakTiket,WaktuCetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan,
		tr.NoSPJ,ts.NoPolisi
	FROM 
		tbl_reservasi tr LEFT JOIN tbl_spj ts ON tr.NoSPJ=ts.NoSPJ
	WHERE (".($filter_by==0?"DATE(tr.TglBerangkat)":"DATE(WaktuCetakTiket)")." BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(tr.IdJurusan)='$kode_cabang' AND FlagBatal!=1
	ORDER BY $sort_by $order";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo(mysql_error());exit;
}

$i = 1;//$idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$pesanan	= (!$row['FlagPesanan'])?"Go Show":"Pesanan";
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
				'no_tiket'=>$row['NoTiket'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'telp'=>$row['Telp'],
				'no_kursi'=>$row['NomorKursi'],
				'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
				'discount'=>number_format($row['Discount'],0,",","."),
				'manifest'=>$row['NoSPJ'],
				'body_mobil'=>$row['NoPolisi'],
				'total'=>number_format($row['Total'],0,",","."),
				'tipe_discount'=>$row['JenisDiscount'],
				'cso'=>$row['NamaCSO'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
	
	$i++;
}


//QUERY PAKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket
	WHERE (".($filter_by==0?"DATE(TglBerangkat)":"DATE(WaktuPesan)")." BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' AND FlagBatal!=1
	ORDER BY $sort_by $order";	


if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	$template->
		assign_block_vars(
			'ROWPAKET',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
				'no_tiket'=>$row['NoTiket'],
				'kode_jadwal'=>$row['KodeJadwal'],
				'dari'=>$row['NamaPengirim'],
				'untuk'=>$row['NamaPenerima'],
				'harga_paket'=>number_format($row['HargaPaket'],0,",","."),
				'cso'=>$row['NamaCSO'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
	
	$i++;
}

//QUERY TAMBAHAN
$sql=
	"SELECT
		KodeCabang,NamaReleaser,NamaPembuat,
		NamaPenerima,TglBuat,JenisBiaya,
		Jumlah,Keterangan
	FROM
		tbl_biaya_tambahan
	WHERE StatusCetak=1 AND KodeCabang='$kode_cabang'AND
	DATE(TglCetak) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'";

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
	while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';

		if (($i % 2)==0){
			$odd = 'even';
		}

		$template->
		assign_block_vars(
			'ROWTAMBAHAN',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'cabang'=>$row['KodeCabang'],
				'releaser'=>$row['NamaReleaser'],
				'pembuat'=>$row['NamaPembuat'],
				'penerima'=>$row['NamaPenerima'],
				'tgl_buat'=>$row['TglBuat'],
				'status'=>$status,
				'jenis_biaya'=>$row['JenisBiaya'],
				'jumlah'=>$row['Jumlah'],
				'ket'=>$row['Keterangan']
			)
		);

		$i++;
	}
}
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}


//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p0=".$tanggal_mulai."&p1=".$tanggal_akhir."&p2=".$pencari."&p3=".$jenis_laporan."";
													
//$script_cetak_excel="Start('laporan_rekap_tiket_detail_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'PENCARI'				=> $pencari,
	'JENIS_LAPORAN'	=> $jenis_laporan,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>