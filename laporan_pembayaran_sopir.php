<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 404;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$page_title	= "Laporan Biaya Sopir";
$interface_menu_utama=false;

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kota_dipilih 	= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$start	= $tanggal_mulai==''?true:false;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$kota = $kota_dipilih;
$kota_dipilih	= ($kota_dipilih!='')?"AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang=f_jurusan_get_kode_cabang_asal_by_jurusan(ts.idjurusan))='$kota_dipilih'":"";
		
$kondisi_cari	=($cari=="")?
	" WHERE tms.KodeSopir LIKE '%' ":
	" WHERE (tms.KodeSopir LIKE '$cari%' OR tms.Nama LIKE '%$cari%')";
	

	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tms.KodeSopir":$sort_by;

// LIST
$template->set_filenames(array('body' => 'laporan_pembayaran_sopir/laporan_pembayaran_sopir_body.tpl')); 

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"tms.KodeSopir","tbl_md_sopir tms",
"&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih."&cari=".$cari."&sort_by=".$sort_by."&order=".$order,
$kondisi_cari,"laporan_pembayaran_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//mengambil query biaya op

if($mode=='0' || $start){

	//hanya dijalankan ketika user menekan tombol cari
	$sql_biaya_op	=
		"CREATE OR REPLACE VIEW view_biaya_sopir AS 
		SELECT 
			KodeSopir,COUNT(DISTINCT(ts.NoSPJ)) AS TotalJalan,IS_NULL(SUM(Jumlah),0) AS Jumlah
		FROM tbl_biaya_op tbo INNER JOIN tbl_spj ts ON tbo.nospj=ts.nospj
		WHERE tbo.NoSPJ IN(
			SELECT NoSPJ
			FROM tbl_spj
			WHERE tglberangkat
				BETWEEN '$tanggal_mulai_mysql'
				AND '$tanggal_akhir_mysql')
		AND FlagJenisBiaya IN ($FLAG_BIAYA_SOPIR_KUMULATIF,$FLAG_BIAYA_SOPIR)
		$kota_dipilih
		GROUP BY KodeSopir";

	if (!$result = $db->sql_query($sql_biaya_op)){
		//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	}
	
	//mengambil total kasbon
	$sql_biaya_op	=
		"CREATE OR REPLACE VIEW view_kasbon_sopir AS 
		SELECT 
			KodeSopir,IS_NULL(SUM(Jumlah),0) AS Jumlah
		FROM tbl_kasbon_sopir 
		WHERE 
			(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
			AND IsBatal!=1
		GROUP BY KodeSopir";
		
	if (!$result = $db->sql_query($sql_biaya_op)){
		//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	}
	
}

$sql	=
	"SELECT tms.KodeSopir,tms.Nama,TotalJalan,vbs.Jumlah,vks.Jumlah AS JumlahKasbon,(IS_NULL(vbs.Jumlah,0)-IS_NULL(vks.Jumlah,0)) AS JumlahTerima
	FROM (tbl_md_sopir tms LEFT JOIN view_biaya_sopir vbs ON tms.KodeSopir=vbs.KodeSopir) 
		LEFT JOIN view_kasbon_sopir vks ON tms.KodeSopir=vks.KodeSopir
	$kondisi_cari
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'nama'=>$row['Nama'].$test,
					'nrp'=>$row['KodeSopir'],
					'total_jalan'=>number_format($row['TotalJalan'],0,",","."),
					'total_biaya'=>number_format($row['Jumlah'],0,",","."),
					'total_kasbon'=>number_format($row['JumlahKasbon'],0,",","."),
					'total_terima'=>number_format($row['JumlahTerima'],0,",",".")
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota.
										"&cari=".$cari."&sort_by=".$sort_by."&order=".$order."";
	
$script_cetak_pdf="Start('laporan_pembayaran_sopir_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_pembayaran_sopir_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//BEGIN KOMPONEN-KOMPONEN SORTING
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= 
	"&page=".$idx_page."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih.
	"&cari=".$cari."&order=".$order_invert."";
//END KOMPONEN-KOMPONEN SORTING

$page_title	= "Pembayaran Sopir";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_pembayaran_sopir.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'OPT_KOTA'			=> ComboKota($kota),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'OPT_SORT'			=> $opt_sort_by,
	'OPT_ORDER'			=> $opt_order,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_pembayaran_sopir.'.$phpEx.'?sort_by=nama'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Nama ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_pembayaran_sopir.'.$phpEx.'?sort_by=kodesopir'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan NRP ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_pembayaran_sopir.'.$phpEx.'?sort_by=totaljalan'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Total Rit ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_pembayaran_sopir.'.$phpEx.'?sort_by=jumlah'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan Total Biaya ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_pembayaran_sopir.'.$phpEx.'?sort_by=jumlahkasbon'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Total Kasbon ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_pembayaran_sopir.'.$phpEx.'?sort_by=jumlahterima'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Total Terima ($order_invert)",
	)
);

function ComboKota($kota_dipilih){
    //SET COMBO kota

    global $LIST_KOTA;

    $opt_kota="<option value=''>--PILIH--</option>";

    for($idx=0;$idx<count($LIST_KOTA);$idx++){
        $selected	=($kota_dipilih!=$LIST_KOTA[$idx])?"":"selected";
        $opt_kota .="<option value='$LIST_KOTA[$idx]' $selected>$LIST_KOTA[$idx]</option>";
    }

    return $opt_kota;
    //END SET COMBO KOTA
}
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>