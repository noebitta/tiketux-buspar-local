<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 0;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari  			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$kondisi_cari	= " AND (Nama LIKE '%$cari%'
									OR Telp LIKE '%$cari%'
									OR HP LIKE '%$cari%'
									OR KodeJadwal LIKE '%$cari%'
									OR NoSPJ LIKE '%$cari%'
									OR KodeBooking LIKE '%$cari%'
									OR JamBerangkat LIKE '$cari'
									OR NoTiket LIKE '%$cari%')";
	
if($kota!=""){
	$kondisi_cabang	.= " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'";
}
	
if($asal!=""){
	$kondisi_cabang	.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'";
}

if($asal!="" && $tujuan!=""){
	$kondisi_cabang	.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'";
}

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_tanggal	= " AND (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')";
			

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"WaktuCetakTiket":$sort_by;

//QUERY
$sql	= 
	"SELECT * FROM tbl_reservasi
	WHERE PetugasPenjual=0 AND SUBSTRING(PaymentCode,-3)='SEL' 
	$kondisi_cabang $kondisi_tanggal $kondisi_cari
	ORDER BY $sort_by $order";

if (!$result = $db->sql_query($sql)){
	die_error("Err:",__LINE__);
}   

		
$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Seven Eleven per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Filter Kota: '.$kota.', Asal: '.$asal.', Tujuan: '.$tujuan.',Jenis: '.$status);
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Bayar');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C4', '#Booking');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D4', '#Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F4', '#Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'HP');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I4', '#Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Harga Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Discount');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L4', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M4', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+4;

	$keterangan=$row['FlagBatal']!=1?"":$row['NamaCSOPembatalan'];

	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeBooking']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['Telp']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['NomorKursi']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['HargaTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['HargaTiket']-$row['HargaTiketux']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['HargaTiketux']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $keterangan);
	
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':G'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row,'=SUM(J4:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row,'=SUM(K4:K'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row,'=SUM(L4:L'.$temp_idx.')');

	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Seven Eleven per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}


?>
