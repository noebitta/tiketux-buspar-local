<?php
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');


// SESSION
$id_page = 313;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

$bulan = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$laporan = array();


//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');

$border = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);


$styleheader = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font'  => array(
        'bold'  => true,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '#48616c')
    )
);

$fill = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '#48616c')
    )
);


//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Total Trip Bulan '.BulanString($bulan).' Tahun '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Total Trip DT');
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'HARI');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('10');
$objPHPExcel->getActiveSheet()->mergeCells('C3:C4');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'TANGGAL');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');




$sql_dt = "SELECT
                DATE(TglBerangkat) AS Tgl,
                IdJurusan,
                f_cabang_get_name_by_kode (
                    f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
                ) Cabang,
                COUNT(NoSPJ) AS TRIP
            FROM
                tbl_spj
            JOIN tbl_md_kendaraan
               ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
            WHERE
                MONTH (TglBerangkat) = $bulan
            AND YEAR (TglBerangkat) = $tahun
            AND tbl_md_kendaraan.IsWH != 1
            GROUP BY
                TglBerangkat, IdJurusan";

if(!$result_dt = $db->sql_query($sql_dt)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_dt)){
    $laporan_dt[$row['Tgl']][$row['IdJurusan']] = $row['TRIP'];
}

$sql_sum_dt = "SELECT
                    IdJurusan,
                    f_cabang_get_name_by_kode (
                        f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
                    ) Cabang,
                    COUNT(NoSPJ) AS TRIP
                FROM
                    tbl_spj
                JOIN tbl_md_kendaraan
                   ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
                WHERE
                    MONTH (TglBerangkat) = $bulan
                AND YEAR (TglBerangkat) = $tahun
                AND tbl_md_kendaraan.IsWH != 1
                GROUP BY
                    IdJurusan";

if(!$result_sum_dt = $db->sql_query($sql_sum_dt)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_sum_dt)){
    $laporan_sum_dt[$row['IdJurusan']] = $row['TRIP'];
}

// mengambil total trip pertanggal dan perjurusan mobil wh
$sql_wh = "SELECT
            DATE(TglBerangkat) AS Tgl,
            IdJurusan,
            f_cabang_get_name_by_kode (
                f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
            ) Cabang,
            COUNT(NoSPJ) AS TRIP
        FROM
            tbl_spj
        JOIN tbl_md_kendaraan
           ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
        WHERE
            MONTH (TglBerangkat) = $bulan
        AND YEAR (TglBerangkat) = $tahun
        AND tbl_md_kendaraan.IsWH != 0
        GROUP BY
            TglBerangkat, IdJurusan";

if(!$result_wh = $db->sql_query($sql_wh)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_wh)){
    $laporan_wh[$row['Tgl']][$row['IdJurusan']] = $row['TRIP'];
}

// mengambil total trip perjurusan mobil wh

$sql_sum_wh = "SELECT
                    IdJurusan,
                    f_cabang_get_name_by_kode (
                        f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
                    ) Cabang,
                    COUNT(NoSPJ) AS TRIP
                FROM
                    tbl_spj
                JOIN tbl_md_kendaraan
                   ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
                WHERE
                    MONTH (TglBerangkat) = $bulan
                AND YEAR (TglBerangkat) = $tahun
                AND tbl_md_kendaraan.IsWH != 0
                GROUP BY
                    IdJurusan";

if(!$result_sum_wh = $db->sql_query($sql_sum_wh)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_sum_wh)){
    $laporan_sum_wh[$row['IdJurusan']] = $row['TRIP'];
}

//====================== JAKARTA BANDUNG

// jakarta bandung
$sql_cabang_jakarta = 'SELECT
                            IdJurusan,
                            KodeJurusan,
                            f_cabang_get_name_by_kode (KodeCabangAsal) AS Nama,
                            f_cabang_get_name_by_kode (KodeCabangTujuan) AS Tujuan
                        FROM
                            tbl_md_jurusan j
                        WHERE
                            (
                                SELECT
                                    Kota
                                FROM
                                    tbl_md_cabang
                                WHERE
                                    KodeCabang = KodeCabangAsal
                            ) = "JAKARTA" 
                        ORDER BY
                            IdJurusan';

if(!$list_cabang_jakarta = $db->sql_query($sql_cabang_jakarta)){
    die("Error : ".__LINE__);
}

$kolom = 3;
while($cabang = $db->sql_fetchrow($list_cabang_jakarta)){

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).'4', $cabang['KodeJurusan']);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $kolom++;
}

    $objPHPExcel->getActiveSheet()->mergeCells('D3:'.position($kolom).'3');
    $objPHPExcel->getActiveSheet()->setCellValue('D3','JAKARTA BANDUNG');

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).'4', 'Total');
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);

    $objPHPExcel->getActiveSheet()->getStyle('A3:'.position($kolom).'4')->applyFromArray($styleheader);

// END HEADER

$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;
$idx=0;
while($currentdate <= $end) {

    $cur_date = date('Y-m-d', $currentdate);
    $idx_row=$idx+5;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, translateDay(date_format(date_create($cur_date),'l')));
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, date_format(date_create($cur_date),'d-m-Y'));

    if(!$list_cabang_jakarta = $db->sql_query($sql_cabang_jakarta)){
        die("Error : ".__LINE__);
    }

    $kolom = 3;
    while($cabang = $db->sql_fetchrow($list_cabang_jakarta)){

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $laporan_dt[$cur_date][$cabang['IdJurusan']]);
        $kolom++;

    }

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, '=SUM('.position(3).$idx_row.':'.position($kolom-1).$idx_row.')');
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).$idx_row)->applyFromArray($fill);


    $idx++;
    $currentdate = strtotime('+1 days', $currentdate);

}

    $idx_row = $idx+5;

    $objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'GRAND TOTAL');

    $kolom_awal = 3;
    while($kolom_awal <= $kolom){

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom_awal).$idx_row, '=SUM('.position($kolom_awal).'5:'.position($kolom_awal).($idx_row-1).')');
        $kolom_awal++;

    }

    $objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':'.position($kolom_awal-1).$idx_row)->applyFromArray($fill);

    for($col = 'A'; $col !== position($kolom_awal); $col++) {
            $objPHPExcel->getActiveSheet()->getStyle($col.'3:'.$col.$idx_row)->applyFromArray($border);
    }


$idx_row = $idx_row+4;

//======================  BANDUNG JAKARTA


$idx_style = $idx_row;
$objPHPExcel->getActiveSheet()->setCellValue('A'.($idx_row-1), 'Total Trip DT');
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':A'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'No');
$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':B'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, 'HARI');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('10');
$objPHPExcel->getActiveSheet()->mergeCells('C'.$idx_row.':C'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, 'TANGGAL');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');



// bandung jakarta
$sql_cabang_bandung = 'SELECT
                            IdJurusan AS IdJurusanJakarta,
                            KodeJurusan,
                            f_cabang_get_name_by_kode (KodeCabangAsal) AS DariJakarta,
                            f_cabang_get_name_by_kode (KodeCabangTujuan) AS KeBandung,
                            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanBandung,
                            (SELECT KodeJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KodeJurusanBandung,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS DariBandung,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakarta,
                            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanPasteur,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS DariPasteur,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakartaDariPasteur
                            
                        FROM
                            tbl_md_jurusan j
                        WHERE
                            (
                                SELECT
                                    Kota
                                FROM
                                    tbl_md_cabang
                                WHERE
                                    KodeCabang = KodeCabangAsal
                            ) = "JAKARTA"
                        ORDER BY
                            IdJurusan';

if(!$list_cabang_bandung = $db->sql_query($sql_cabang_bandung)){
    die("Error : ".__LINE__);
}

$idx_row++;
$kolom = 3;
while($cabang = $db->sql_fetchrow($list_cabang_bandung)){

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $cabang['KodeJurusanBandung']);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $kolom++;
}

    $objPHPExcel->getActiveSheet()->mergeCells('D'.($idx_row-1).':'.position($kolom).($idx_row-1));
    $objPHPExcel->getActiveSheet()->setCellValue('D'.($idx_row-1),'BANDUNG JAKARTA');

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, 'Total');
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);

    $objPHPExcel->getActiveSheet()->getStyle('A'.($idx_row-1).':'.position($kolom).$idx_row)->applyFromArray($styleheader);



$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;
$idx=0;
$index = $idx_row+1;
$index_sum = $index;
while($currentdate <= $end) {

    $cur_date = date('Y-m-d', $currentdate);
    $idx_row=$idx+$index;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, translateDay(date_format(date_create($cur_date),'l')));
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, date_format(date_create($cur_date),'d-m-Y'));

    if(!$list_cabang_bandung = $db->sql_query($sql_cabang_bandung)){
        die("Error : ".__LINE__);
    }

    $kolom = 3;
    while($cabang = $db->sql_fetchrow($list_cabang_bandung)){

        if($cabang['IdJurusanBandung'] != $cabang['IdJurusanPasteur']){
            $jml = $laporan_dt[$cur_date][$cabang['IdJurusanBandung']] + $laporan_dt[$cur_date][$cabang['IdJurusanPasteur']];
        }else{
            $jml = $laporan_dt[$cur_date][$cabang['IdJurusanBandung']];
        }

        $jml = ($jml != 0)?$jml:'';

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $jml);
        $kolom++;

    }

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, '=SUM('.position(3).$idx_row.':'.position($kolom-1).$idx_row.')');
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).$idx_row)->applyFromArray($fill);

    $idx++;
    $currentdate = strtotime('+1 days', $currentdate);
}

    $idx_row++;
    $objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'GRAND TOTAL');

    $kolom_awal = 3;
    while($kolom_awal <= $kolom){

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom_awal).$idx_row, '=SUM('.position($kolom_awal).$index_sum.':'.position($kolom_awal).($idx_row-1).')');
        $kolom_awal++;

    }

    $objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':'.position($kolom_awal-1).$idx_row)->applyFromArray($fill);

    for($col = 'A'; $col !== position($kolom_awal); $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col.$idx_style.':'.$col.$idx_row)->applyFromArray($border);
    }

//=================== WH

//HEADER
$idx_row = $idx_row+4;

$objPHPExcel->getActiveSheet()->setCellValue('A'.($idx_row-1), 'Total Trip WH');
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':A'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':B'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, 'HARI');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('10');
$objPHPExcel->getActiveSheet()->mergeCells('C'.$idx_row.':C'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, 'TANGGAL');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');

if(!$list_cabang_jakarta = $db->sql_query($sql_cabang_jakarta)){
    die("Error : ".__LINE__);
}

$kolom = 3;
while($cabang = $db->sql_fetchrow($list_cabang_jakarta)){

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).($idx_row+1), $cabang['KodeJurusan']);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $kolom++;
}

$objPHPExcel->getActiveSheet()->mergeCells('D'.$idx_row.':'.position($kolom).$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row,'JAKARTA BANDUNG');

$objPHPExcel->getActiveSheet()->setCellValue(position($kolom).($idx_row+1), 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getStyle('A'.($idx_row).':'.position($kolom).($idx_row+1))->applyFromArray($styleheader);

// END HEADER

$index = $idx_row+2;
$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;
$idx=0;
$idx_sum_wh = $idx_row;
while($currentdate <= $end) {

    $cur_date = date('Y-m-d', $currentdate);
    $idx_row=$idx+$index;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, translateDay(date_format(date_create($cur_date),'l')));
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, date_format(date_create($cur_date),'d-m-Y'));

    if(!$list_cabang_jakarta = $db->sql_query($sql_cabang_jakarta)){
        die("Error : ".__LINE__);
    }

    $kolom = 3;
    while($cabang = $db->sql_fetchrow($list_cabang_jakarta)){

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $laporan_wh[$cur_date][$cabang['IdJurusan']]);
        $kolom++;

    }

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, '=SUM('.position(3).$idx_row.':'.position($kolom-1).$idx_row.')');
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).$idx_row)->applyFromArray($fill);

    $idx++;
    $currentdate = strtotime('+1 days', $currentdate);

}

    $objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'GRAND TOTAL');

    $kolom_awal = 3;
    while($kolom_awal <= $kolom){

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom_awal).$idx_row, '=SUM('.position($kolom_awal).$idx_sum_wh.':'.position($kolom_awal).($idx_row-1).')');
        $kolom_awal++;

    }

    $objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':'.position($kolom_awal-1).$idx_row)->applyFromArray($fill);

    for($col = 'A'; $col !== position($kolom_awal); $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col.'3:'.$col.$idx_row)->applyFromArray($border);
    }


// WH BANDUNG JAKARTA
$idx_row = $idx_row+5;

// HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A'.($idx_row-1), 'Total Trip WH');
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':A'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
$objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':B'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, 'HARI');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('10');
$objPHPExcel->getActiveSheet()->mergeCells('C'.$idx_row.':C'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, 'TANGGAL');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');

if(!$list_cabang_bandung = $db->sql_query($sql_cabang_bandung)){
    die("Error : ".__LINE__);
}

$idx_row++;
$kolom = 3;
while($cabang = $db->sql_fetchrow($list_cabang_bandung)){

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $cabang['KodeJurusanBandung']);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $kolom++;
}

$objPHPExcel->getActiveSheet()->mergeCells('D'.($idx_row-1).':'.position($kolom).($idx_row-1));
$objPHPExcel->getActiveSheet()->setCellValue('D'.($idx_row-1),'BANDUNG JAKARTA');

$objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getStyle('A'.($idx_row-1).':'.position($kolom).$idx_row)->applyFromArray($styleheader);


$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;
$idx=0;
$index = $idx_row+1;
$index_sum = $index;
while($currentdate <= $end) {

    $cur_date = date('Y-m-d', $currentdate);
    $idx_row=$idx+$index;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, translateDay(date_format(date_create($cur_date),'l')));
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, date_format(date_create($cur_date),'d-m-Y'));

    if(!$list_cabang_bandung = $db->sql_query($sql_cabang_bandung)){
        die("Error : ".__LINE__);
    }

    $kolom = 3;
    while($cabang = $db->sql_fetchrow($list_cabang_bandung)){

        if($cabang['IdJurusanBandung'] != $cabang['IdJurusanPasteur']){
            $jml = $laporan_wh[$cur_date][$cabang['IdJurusanBandung']] + $laporan_wh[$cur_date][$cabang['IdJurusanPasteur']];
        }else{
            $jml = $laporan_wh[$cur_date][$cabang['IdJurusanBandung']];
        }

        $jml = ($jml != 0)?$jml:'';

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $jml);
        $kolom++;

    }

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, '=SUM('.position(3).$idx_row.':'.position($kolom-1).$idx_row.')');
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).$idx_row)->applyFromArray($fill);

    $idx++;
    $currentdate = strtotime('+1 days', $currentdate);
}

$idx_row++;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'GRAND TOTAL');

$kolom_awal = 3;
while($kolom_awal <= $kolom){

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom_awal).$idx_row, '=SUM('.position($kolom_awal).$index_sum.':'.position($kolom_awal).($idx_row-1).')');
    $kolom_awal++;

}

$objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':'.position($kolom_awal-1).$idx_row)->applyFromArray($fill);

for($col = 'A'; $col !== position($kolom_awal); $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.$idx_style.':'.$col.$idx_row)->applyFromArray($border);
}



//=================== SUMERY

$idx_row = $idx_row+5;
$idx_border = $idx_row;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'COUNTER');
$objPHPExcel->getActiveSheet()->mergeCells('D'.$idx_row.':I'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, 'TRIP DAYTRANS');
$objPHPExcel->getActiveSheet()->mergeCells('D'.($idx_row+1).':F'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('D'.($idx_row+1), 'JAKARTA - BANDUNG');
$objPHPExcel->getActiveSheet()->mergeCells('G'.($idx_row+1).':I'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+1), 'BANDUNG - JAKARTA');
$objPHPExcel->getActiveSheet()->mergeCells('J'.$idx_row.':J'.($idx_row+1));
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, 'TOTAL');

$objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':J'.($idx_row+1))->applyFromArray($fill);
$objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':J'.($idx_row+1))->applyFromArray($styleheader);

$sql = 'SELECT
            IdJurusan AS IdJurusanJakarta,
            KodeJurusan,
            f_cabang_get_name_by_kode (KodeCabangAsal) AS DariJakarta,
            f_cabang_get_name_by_kode (KodeCabangTujuan) AS KeBandung,
            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanBandung,
            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS DariBandung,
            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakarta,
            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanPasteur,
            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS DariPasteur,
            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakartaDariPasteur
            
        FROM
            tbl_md_jurusan j
        WHERE
            (
                SELECT
                    Kota
                FROM
                    tbl_md_cabang
                WHERE
                    KodeCabang = KodeCabangAsal
            ) = "JAKARTA"
        ORDER BY
            IdJurusan';

if (!$jurusan_jakarta = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

$idx_row++;
$idx_sum = $idx_row;
while ($row = $db->sql_fetchrow($jurusan_jakarta)){

    if($row['IdJurusanBandung'] != $row['IdJurusanPasteur']){
        $jml_dt_bdg = $laporan_sum_dt[$row['IdJurusanBandung']] + $laporan_sum_dt[$row['IdJurusanPasteur']];
        $jml_wh_bdg = $laporan_sum_wh[$row['IdJurusanBandung']] + $laporan_sum_wh[$row['IdJurusanPasteur']];
    }else{
        $jml_dt_bdg = $laporan_sum_dt[$row['IdJurusanBandung']];
        $jml_wh_bdg = $laporan_sum_wh[$row['IdJurusanBandung']];
    }

    $objPHPExcel->getActiveSheet()->mergeCells('A'.($idx_row+1).':C'.($idx_row+1));
    $objPHPExcel->getActiveSheet()->mergeCells('D'.($idx_row+1).':F'.($idx_row+1));
    $objPHPExcel->getActiveSheet()->mergeCells('G'.($idx_row+1).':I'.($idx_row+1));
    $objPHPExcel->getActiveSheet()->setCellValue('A'.($idx_row+1), $row['DariJakarta']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.($idx_row+1), $laporan_sum_dt[$row['IdJurusanJakarta']]+$laporan_sum_wh[$row['IdJurusanJakarta']]);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+1), $jml_dt_bdg+$jml_wh_bdg);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.($idx_row+1), '=SUM(D'.($idx_row+1).':G'.($idx_row+1).')');
    $idx_row++;
}

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Total');
$objPHPExcel->getActiveSheet()->mergeCells('D'.$idx_row.':F'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D'.($idx_sum+1).':'.'D'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->mergeCells('G'.$idx_row.':I'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G'.($idx_sum+1).':'.'G'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J'.($idx_sum+1).':'.'J'.($idx_row-1).')');

$objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':J'.$idx_row)->applyFromArray($fill);
for($col = 'A'; $col !== 'K'; $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.$idx_border.':'.$col.$idx_row)->applyFromArray($border);
}
//END
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Total Aktual Trip Bulan '.BulanString($bulan).' Tahun '.$tahun.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

function position($posisi){
    $alpa = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    return $alpa[$posisi];
}
?>