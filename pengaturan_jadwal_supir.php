<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassPenjadwalanKendaraan.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassMobil.php');
// SESSION
$id_page = 705;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$cabang         = isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$status			= isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status'];
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];
$periode        = isset($HTTP_GET_VARS['periode'])? $HTTP_GET_VARS['periode'] : $HTTP_POST_VARS['periode'];

$bulan	= ($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");
$periode = ($periode != '')?$periode:1;

if($periode == 1){
    $periode_select_1 = "selected";
    $periode_select_2 = "";
    $tgl_max = 15;
    $tgl_awal = 0;
    $temp = 0;
    $har = 1;
}
else{
    $temp = 15;
    $har = 2;
    $tgl_awal = 15;
    $tgl_max = getMaxDate($bulan,$tahun);
    $periode_select_1 = "";
    $periode_select_2 = "selected";

}

$PenjadwalanKendaraan = new PenjadwalanKendaraan();
//PILIH MODE
switch($mode){
    case 'Hapus':
        $idjadwal   = $HTTP_GET_VARS['IdJadwalSopir'];
        $sql = "DELETE FROM tbl_penjadwalan_kendaraan WHERE IdPenjadwalan = $idjadwal";
        if(!$result = $db->sql_query($sql)){
            echo ("Error : ".__LINE__.mysql_error());
            exit;
        }
        echo("alert('Jadwal Telah Dihapus')");
    exit;
    case 'get_asal':

        $opt = setCabangAwal();
        echo ($opt);
    exit;
    case 'get_tujuan':
        $cabang_asal		= $HTTP_GET_VARS['asal'];
        $opt_cabang_tujuan=
            "<select id='opt_tujuan' name='opt_tujuan' onChange='getUpdateJam(this.value)'>".
            setComboCabangTujuan($cabang_asal)
            ."</select>";

        echo($opt_cabang_tujuan);
    exit;
    case 'get_jam':
        $id_jurusan = $HTTP_GET_VARS['id_jurusan'];
        $opt_jam_berangkat=
            "<select id='jam' nama='jam' onChange='ambiljam(this.value)'>".
            setComboJam($id_jurusan)
            ."</select>";
        echo($opt_jam_berangkat);
    exit;
    case 'set_hidden_jam':
        $KodeJadwal     = $HTTP_GET_VARS['KodeJadwal'];
        //QUERY AMBIL JAM BERANGKAT BERDASARKAN KODEJADWAL
        $sql = "SELECT JamBerangkat FROM tbl_md_jadwal WHERE KodeJadwal = '$KodeJadwal'";
        if(!$result = $db->sql_query($sql)){
            echo ("Error : ".__LINE__);
            die(mysql_error());
        }
        $row = $db->sql_fetchrow($result);
        $input_hidden = "<input type='hidden' name='JamBerangkat' id='JamBerangkat' value='$row[0]'>";
        echo($input_hidden);
    exit;
    case 'get_list_mobil':
        $KodeSopir      = $HTTP_GET_VARS['KodeSopir'];

        $sql = "SELECT * FROM tbl_md_kendaraan WHERE KodeSopir1 = '$KodeSopir' OR KodeSopir2 = '$KodeSopir' OR KodeSopir3 = '$KodeSopir'";
        if(!$sql_mobil = $db->sql_query($sql)){
            echo ("alert('Error : ".__LINE__."');");
        }else{

            $mobil_list = "";

            while ($row = $db->sql_fetchrow($sql_mobil)){

                $mobil_list .= "<option value='$row[KodeKendaraan]'>$row[NoPolisi] ( $row[JumlahKursi] Kursi )</option>";
            }

            $select_list = "<select id='mobil' name='mobil'><option value=''>none</option>$mobil_list</select>";

            echo ($select_list);
        }
    exit;
    case 'InsertData':
        $KodeSopir      = $HTTP_GET_VARS['KodeSopir'];
        $Tgl            = $HTTP_GET_VARS['Tgl'];
        $Bln            = $HTTP_GET_VARS['Bln'];
        $Thn            = $HTTP_GET_VARS['Thn'];
        $KodeJadwal     = $HTTP_GET_VARS['KodeJadwal'];
        $IdJurusan      = $HTTP_GET_VARS['IdJurusan'];
        $JamBerangkat   = $HTTP_GET_VARS['JamBerangkat'];
        $NamaSupir      = $HTTP_GET_VARS['NamaSupir'];
        $KodeMobil      = $HTTP_GET_VARS['Mobil'];
        $StatusKerja    = $HTTP_GET_VARS['StatusKerja'];
        $TglBerangkat   = $Thn."-".$Bln."-".$Tgl;
        $remark         = "User : ".$userdata['nama']." Mengaktifkan Pada ".date('d-m-Y H:i:s');

        // data mobil
        $Mobil  = new Mobil();
        $data_mobil = $Mobil->ambilDataDetail($KodeMobil);
        $kode_body  = $data_mobil['KodeKendaraan'];
        $nopol      = $data_mobil['NoPolisi'];
        $jml_kursi  = $data_mobil['JumlahKursi'];

        $sql_penjadawalan = "SELECT IdPenjadwalan
                               FROM
                                  tbl_penjadwalan_kendaraan
                               WHERE
                                  TglBerangkat='$TglBerangkat'
                               AND KodeJadwal='$KodeJadwal'";

        if ($hasil = $db->sql_query($sql_penjadawalan)){
            $checkJadwal = $db->sql_fetchrow($hasil);

            if($checkJadwal[0]!=""){
                $update = "UPDATE tbl_penjadwalan_kendaraan
                                    SET KodeDriver = '$KodeSopir', NamaDriver = '$NamaSupir' , KodeKendaraan = $kode_body, NoPolisi = '$nopol', LayoutKursi = $jml_kursi
                                    WHERE KodeJadwal = '$KodeJadwal'
                                    AND TglBerangkat = '$TglBerangkat'
                                    AND JamBerangkat = '$JamBerangkat'";
                if(!$db->sql_query($update)){
                    echo ("Error : ".__LINE__);
                    exit;
                }
            }else{
                $insert = "INSERT INTO tbl_penjadwalan_kendaraan
                                    (TglBerangkat,JamBerangkat,
                                    KodeJadwal,IdJurusan,LayoutKursi,NoPolisi,KodeKendaraan,
                                    KodeDriver,NamaDriver,StatusAktif,Remark
                                    )
                                    VALUES(
                                    '$TglBerangkat','$JamBerangkat',
                                    '$KodeJadwal',$IdJurusan,$jml_kursi,'$nopol',$kode_body,
                                    '$KodeSopir','$NamaSupir',1,'$remark')";
                if(!$db->sql_query($insert)){
                    echo ("Error : ".__LINE__);
                    exit;
                }
            }

        }else{
            echo ("alert('Error : ".__LINE__."');");
        }

    exit;
    case 'setJadwal':
        $KodeSopir      = $HTTP_GET_VARS['KodeSopir'];
        $Tgl            = $HTTP_GET_VARS['Tgl'];
        $Bln            = $HTTP_GET_VARS['Bln'];
        $Thn            = $HTTP_GET_VARS['Thn'];
        $NamaSupir      = $HTTP_GET_VARS['NamaSupir'];
        $KodeMobil      = $HTTP_GET_VARS['Mobil'];
        $kode_group     = $HTTP_GET_VARS['kode_group'];

        $TglBerangkat   = $Thn."-".$Bln."-".$Tgl;
        $remark         = "User : ".$userdata['nama']." Mengaktifkan Pada ".date('d-m-Y H:i:s');

        $Jurusan= new Jurusan();
        $Jadwal = new Jadwal();
        $Mobil  = new Mobil();

        $data_mobil = $Mobil->ambilDataDetail($KodeMobil);

        $kode_body  = $data_mobil['KodeKendaraan'];
        $nopol      = $data_mobil['NoPolisi'];
        $jml_kursi  = $data_mobil['JumlahKursi'];

        $sql = "SELECT KodeJadwal, IdJurusan FROM tbl_group_jadwal WHERE kode_group = $kode_group";

        if(!$result = $db->sql_query($sql)){
            echo ("alert('terjadi kesalahan');");
        }else{
            while($row = $db->sql_fetchrow($result)){

                $KodeJadwal     = $row['KodeJadwal'];
                $data_jadwal    = $Jadwal->ambilDataDetail($KodeJadwal);
                $JamBerangkat   = date_format(date_create($data_jadwal['JamBerangkat']),'H:i:s');
                $LayoutKursi    = ($jml_kursi == "") ? $data_jadwal['JumlahKursi'] : $jml_kursi;

                $IdJurusan      = $row['IdJurusan'];
                $data_jurusan   = $Jurusan->ambilDataDetail($IdJurusan);

                $sql_penjadawalan ="SELECT IdPenjadwalan
                       FROM
                          tbl_penjadwalan_kendaraan
                       WHERE
                          TglBerangkat='$TglBerangkat'
                       AND KodeJadwal='$KodeJadwal'";

                if ($hasil = $db->sql_query($sql_penjadawalan)){
                    $checkJadwal = $db->sql_fetchrow($hasil);

                    if($checkJadwal[0]!=""){
                        $update = "UPDATE tbl_penjadwalan_kendaraan
                                    SET KodeDriver = '$KodeSopir', NamaDriver = '$NamaSupir'
                                    WHERE KodeJadwal = '$KodeJadwal'
                                    AND TglBerangkat = '$TglBerangkat'
                                    AND JamBerangkat = '$JamBerangkat'";
                        if(!$db->sql_query($update)){
                            echo ("Error : ".__LINE__);
                            exit;
                        }
                    }else{
                        $insert = "INSERT INTO tbl_penjadwalan_kendaraan
                                    (TglBerangkat,JamBerangkat,
                                    KodeJadwal,IdJurusan,LayoutKursi,NoPolisi,KodeKendaraan,
                                    KodeDriver,NamaDriver,StatusAktif,Remark
                                    )
                                    VALUES(
                                    '$TglBerangkat','$JamBerangkat',
                                    '$KodeJadwal',$IdJurusan,$LayoutKursi,'$nopol',$kode_body,
                                    '$KodeSopir','$NamaSupir',1,'$remark')";
                        if(!$db->sql_query($insert)){
                            echo ("Error : ".__LINE__);
                            exit;
                        }
                    }


                }else{
                    echo ("alert('terjadi kesalahan');");
                    exit;
                }

            }

        }
        
    exit;
    case 'Edit':
        $idjadwal   = $HTTP_GET_VARS['idjadwal'];

        $sql = "SELECT tbl_penjadwalan_kendaraan.*,KodeCabangAsal,KodeCabangTujuan
                FROM tbl_penjadwalan_kendaraan
                JOIN tbl_md_jadwal ON tbl_penjadwalan_kendaraan.KodeJadwal = tbl_md_jadwal.KodeJadwal
                JOIN tbl_md_jurusan ON tbl_penjadwalan_kendaraan.IdJurusan = tbl_md_jurusan.IdJurusan
                WHERE IdPenjadwalan = '$idjadwal'";
        if(!$result = $db->sql_query($sql)){
            echo ("Error : ".__LINE__);
            exit;
        }

        $row = $db->sql_fetchrow($result);
        if($row['StatusKerja'] == 1){
            $opt = setOptCabang1($row['KodeCabangAsal']);
        }else{
            $opt = setDropdownCabang($row['KodeCabangAsal']);
        }


        // list mobil
        $sql = "SELECT * FROM tbl_md_kendaraan WHERE KodeSopir1 = '$row[KodeDriver]' OR KodeSopir2 = '$row[KodeDriver]' OR KodeSopir3 = '$row[KodeDriver]'";
        if(!$sql_mobil = $db->sql_query($sql)){
            echo ("Error : ".__LINE__);
        }else{

            $mobil_list = "<option value=''>none</option>";

            while ($mobil = $db->sql_fetchrow($sql_mobil)){
                $selected = $mobil['KodeKendaraan'] == $row['KodeKendaraan'] ? "selected" : "";
                $mobil_list .= "<option value='$mobil[KodeKendaraan]' $selected>$mobil[NoPolisi] ( $mobil[JumlahKursi] Kursi )</option>";
            }
        }

        // table edit
        $table = ' <table id="rewriteEdit">
                        <tr><td colspan="2" align="center"><h2>Penjadwalan Sopir</h2></td></tr>

                        <tr><td align="right" width="200">Supir :</td><td>'.$row['NamaDriver'].'</b></td></tr>
                        <tr><td align="right" width="200">Outlet Berangkat : </td>
                            <td width="300">
                                <select id="editCabangAWAL" name="editCabangAWAL" onChange="getUpdateTujuanEdit(this.value)">
                                '.$opt.'
                                </select>
                              </td>
                        </tr>
                        <tr>
                            <td align="right">Outlet Tujuan : </td>
                            <td width="300">
                                <select id="editCabangTujuan" name="editCabangTujuan" onChange="getUpdateJamEdit(this.value)">
                                '.setOptTujuan($row['KodeCabangAsal'],$row['KodeCabangTujuan']).'
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Jam Berangkat : </td>
                            <td>
                                <select id="KodeJAdwal" name="KodeJAdwal" onChange="getHidenJam(this.value)">
                                '.setOptJam($row['IdJurusan'],$row['JamBerangkat']).'
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Mobil : </td>
                            <td>
                                <select id="editmobil">
                                    '.$mobil_list.'
                                </select>
                            </td>
                        </tr>
                    </table>';
        echo($table);
    exit;
    case 'UpdateData':
        $IdJadwal       = $HTTP_GET_VARS['IdJadwal'];
        $KodeJadwal     = $HTTP_GET_VARS['KodeJadwal'];
        $IdJurusan      = $HTTP_GET_VARS['IdJurusan'];
        $KodeMobil      = $HTTP_GET_VARS['Mobil'];

        // data mobil
        $Mobil  = new Mobil();
        $data_mobil = $Mobil->ambilDataDetail($KodeMobil);
        $kode_body  = $data_mobil['KodeKendaraan'];
        $nopol      = $data_mobil['NoPolisi'];
        $jml_kursi  = $data_mobil['JumlahKursi'];

        $sql = "SELECT JamBerangkat FROM tbl_md_jadwal WHERE KodeJadwal = '$KodeJadwal'";
        if(!$result = $db->sql_query($sql)){
           echo ("Error : ".__LINE__);
        }
        $row = $db->sql_fetchrow($result);
        $jam = $row['JamBerangkat'];

        $sql = "UPDATE tbl_penjadwalan_kendaraan SET 
                KodeJadwal = '$KodeJadwal', 
                IdJurusan='$IdJurusan',
                JamBerangkat='$jam',
                KodeKendaraan=$kode_body,
                NoPolisi='$nopol',
                LayoutKursi=$jml_kursi
                WHERE IdPenjadwalan='$IdJadwal'";
        if(!$result = $db->sql_query($sql)){
            //echo ("Error : ".__LINE__);
            echo ("alert('gagal update');");
            exit;
        }
        echo ("alert('Jadwal Telah Di Update');");
    exit;
    default :



//AMBIL HARI
        $sql= "SELECT WEEKDAY('$tahun-$bulan-01') AS Hari";
        if ($result = $db->sql_query($sql)){
            $row = $db->sql_fetchrow($result);
            $temp_hari	= $row['Hari']+$har;
        }
        for($tgl_awal;$tgl_awal<$tgl_max;$tgl_awal++){
            $idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
            $tgl	= $tgl_awal+1 ."-".HariStringShort($idx_str_hari)."";
            $template->
            assign_block_vars(
                'HARI',
                array(
                    'tgl'=>$tgl,
                )
            );
            $temp_hari++;
        }
//END AMBIL HARI

        if($cabang == ""){
            $no_data = "<tr><td bgcolor='yellow' ALIGN='center'><font SIZE='3'><b>SILAHKAN PILIH TERLEBIH DAHULU CABANG TUGAS SUPIR</b></font></td></tr>";
        }
        else{
            $no_data = "";
            //AMBIL DATA SUPIR BERDASARKAN KODE CABANG
            $sql =
                "SELECT tbl_md_cabang.Nama AS Cabang, tbl_md_sopir.Nama, Norek, KodeSopir, HP, tbl_md_sopir.Alamat, NoSIM, FlagAktif, tbl_md_sopir.KodeCabang,
                      f_kendaraan_ambil_nopol_by_kode(Mobil1) AS Mobil1, f_kendaraan_ambil_nopol_by_kode(Mobil2) AS Mobil2, IsReguler
                 FROM tbl_md_sopir
                 JOIN tbl_md_cabang ON tbl_md_sopir.KodeCabang = tbl_md_cabang.KodeCabang
                 WHERE tbl_md_sopir.KodeCabang = '$cabang'";
            if(!$result = $db->sql_query($sql)){
                echo ("Error : ".__LINE__);
                die(mysql_error());
            }

            //AMBIL DATA JADWAL SUPIR
            $first = $tahun."-".$bulan."-".($temp+1);
            $end   = $tahun."-".$bulan."-".$tgl_max;
            $sql = "SELECT *
                    FROM tbl_penjadwalan_kendaraan
                    WHERE TglBerangkat BETWEEN '$first' AND '$end'
                    ORDER BY KodeDriver";
            if(!$jadwal_query = $db->sql_query($sql)){
                echo ("Error : ".__LINE__);
                die(mysql_error());
            }else{
                while($jadwal = $db->sql_fetchrow($jadwal_query)){
                    $data_jadwal [$jadwal['KodeSopir']]= $jadwal;
                }
            }

            $i=1;
            while ($row = $db->sql_fetchrow($result)){
                $sopir = $row['KodeSopir'];
                //ROW DATA
                $x = 0;
                for($temp;$temp<$tgl_max;$temp++){
                    $temp_tgl = "'".$tahun."-".$bulan."-".($temp+1)."'";
                    $temp_tgl = rtrim($temp_tgl,"'");
                    $temp_tgl = ltrim($temp_tgl,"'");

                    $sql = "SELECT *
                            FROM tbl_penjadwalan_kendaraan
                            WHERE TglBerangkat = '$temp_tgl' AND KodeDriver = '$sopir'";

                    if(!$jadwal = $db->sql_query($sql)){
                        echo ("Error : ".__LINE__);
                        die(mysql_error());
                    }else{
                        $array_temp = array();
                        $array_id   = array();
                        while($data_jadwal = $db->sql_fetchrow($jadwal)){
                            $kode = substr($data_jadwal['KodeJadwal'],0,7);
                            $nopolisi = $data_jadwal['NoPolisi'];
                            $jamBerangkat = date_format(date_create($data_jadwal['JamBerangkat']),'H:i');
                            $isi = '<a onclick="ShowEditDialog('."'".$data_jadwal['IdPenjadwalan']."'".')">'.$jamBerangkat.'('.$kode.')'.'</a>';
                            array_push($array_temp,$isi);
                            array_push($array_id,$data_jadwal['IdJadwalSopir']);
                        }

                    }

                    if($row['IsReguler'] == 1){
                        $data .= '<td align="center">'.implode("<br>",$array_temp).'
                                <br>
                                <input class="tombol" value="..." id="tambah_biaya" type="button" onclick="setJadwal('."'".$row['KodeSopir']."','".$row['Nama']."','".($temp+1)."','".$bulan."','".$tahun."'".')">
                              </td>';
                    }else{
                        $data .= '<td align="center">'.implode("<br>",$array_temp).'
                                <br>
                                <input class="tombol" value="..." id="jadwal_bakcup" type="button" onclick="setJadwalBackup('."'".$row['KodeSopir']."','".$row['Nama']."','".($temp+1)."','".$bulan."','".$tahun."','".$row['IsReguler']."'".')">
                              </td>';
                    }

                    $x++;
                }

                $temp = $temp - $x;

                $odd ='odd';
                if (($i % 2)==0){
                    $odd = 'even';
                }
                $i++;

                if($row['IsReguler'] != 1){
                    $backup = "<font color='green'>(BACKUP)</font>";
                }   else{
                    $backup = "";
                }

                $template->
                assign_block_vars(
                    'ROW',
                    array(
                        'odd'       =>$odd,
                        'Nama'      =>$row['Nama']." ".$backup,
                        'DATA'      =>$data,
                    )
                );
                $data = "";
            }
        }



        $template->set_filenames(array('body' => 'pengaturan_jadwal_supir_body.tpl'));

        $page_title	= "Penjadwalan Sopir";
        $template->assign_vars(array(
                'BCRUMP'    	=>setBcrump($id_page),
                'ACTION_CARI'	=> append_sid('pengaturan_jadwal_supir.'.$phpEx),
                'NODATA'			=> $no_data,
                'CABANG'      => setDropdownCabang($cabang),
                'URL'         => append_sid('pengaturan_jadwal_supir.'.$phpEx),
                'LIST_BULAN'  => list_bulan($bulan),
                'TAHUN'				=> $tahun,
                'BULAN'				=> $bulan,
                'PERIODE1'    => $periode_select_1,
                'PERIODE2'    => $periode_select_2,
                'BERANGKAT'   => setDropdownBerangkat($cabang),
                'list_jadwal' => setListGroupJadwal()

            )
        );
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');

}

//FUNGSI-FUNGSI
function setCabangAwal(){
    global $db;
    $opt_cabang_asal = " <select id='outlet' name='outlet' onChange='getUpdateTujuan(this.value)'>
                                   ".setDropdownCabang('')."
                                    </select>";
    return $opt_cabang_asal;
}
function setDropdownCabang($cabang){
    global $db;

    $Cabang	= new Cabang();

    $result	= $Cabang->setComboCabang();

    $opt = "";

    if ($result){
        $opt = "<option value=''>(none)</option>" . $opt;

        while ($row = $db->sql_fetchrow($result)){
            $selected = ($cabang == $row[0])? "selected" : "";
            $opt .= "<option id='$row[0]' value='$row[0]' $selected>$row[1] ($row[0]) $cabang</option>";
        }
    }
    else{
        $opt .="<option selected=selected>Error</option>";
    }

    return $opt;
}
function setDropdownBerangkat($cabang){
    global $db;

    $sql = "SELECT KodeCabang, Nama FROM tbl_md_cabang WHERE KodeCabang = '$cabang'";
    if(!$namacabang = $db->sql_query($sql)){
        die(mysql_error());
    }
    $namcab = $db->sql_fetchrow($namacabang);
    $sql = "SELECT  KodeCabangAsal, f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaAsal
            FROM tbl_md_jurusan
            WHERE tbl_md_jurusan.KodeCabangTujuan = '$cabang' ";
    if(!$result = $db->sql_query($sql)){
        die(mysql_error());
    }

    $opt = "<option value=''>(none)</option>";
    if ($result){

        $opt .= "<option id='".$namcab['KodeCabang']."' value='".$namcab['KodeCabang']."' >".$namcab['Nama']." (".$namcab['KodeCabang'].")</option>";

        while ($row = $db->sql_fetchrow($result)){

            $opt .= "<option id='$row[0]' value='$row[0]' >$row[1] ($row[0])</option>";
        }
    }
    else{
        $opt .="<option selected=selected>Error</option>";
    }

    return $opt;
}
function setComboCabangTujuan($cabang_asal){
    //SET COMBO cabang
    global $db;
    $sql =
        "SELECT IdJurusan, KodeJurusan, KodeCabangTujuan, f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan
			WHERE KodeCabangAsal LIKE '$cabang_asal' ORDER BY NamaCabangTujuan";

    if (!$result = $db->sql_query($sql)){
        //die_error("Gagal $this->ID_FILE 003");
        echo(mysql_error());
    }
    $opt_cabang="<option value=''>(none)</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){

            $opt_cabang .="<option value='$row[IdJurusan]' >$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
        }
    }
    else{
        echo(mysql_error());exit;
    }
    return $opt_cabang;
    //END SET COMBO CABANG
}
function setComboJam($id_jurusan){
    //SET COMBO JAM KEBERANGKATAN
    global $db;
    $sql = "SELECT KodeJadwal, JamBerangkat, IdJurusan FROM tbl_md_jadwal WHERE IdJurusan = $id_jurusan";
    if(!$result = $db->sql_query($sql)){
        echo(mysql_error());
    }
    $opt_cabang="<option value=''>(none)</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){

            $opt_cabang .="<option value='$row[KodeJadwal]' >$row[JamBerangkat] ($row[KodeJadwal])</option>";
        }
    }
    else{
        echo(mysql_error());exit;
    }
    return $opt_cabang;
}
function list_bulan($bulan){
    //LIST BULAN
    $list_bulan="";
    for($idx_bln=1;$idx_bln<=12;$idx_bln++){

        $selected = ($bulan==$idx_bln)?"selected":"";
        $list_bulan .= "<option value='".$idx_bln."' ".$selected.">".BulanString($idx_bln)."</option>";
    }
    return $list_bulan;
//END LIST BULAN
}
// KHUSUS UNTUK FUNGSI EDIT
function setOptCabang1($cabang){
    global $db;

    $sql = "SELECT KodeCabang, Nama FROM tbl_md_cabang WHERE KodeCabang = '$cabang'";
    if(!$namacabang = $db->sql_query($sql)){
        die("Error : ".__LINE__);
    }
    $namcab = $db->sql_fetchrow($namacabang);
    $sql = "SELECT  KodeCabangAsal, f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaAsal
            FROM tbl_md_jurusan
            WHERE tbl_md_jurusan.KodeCabangTujuan = '$cabang' ";
    if(!$result = $db->sql_query($sql)){
        die("Error : ".__LINE__);
    }

    $opt = "<option value=''>(none)</option>";
    if ($result){
        $selected = ($namcab['KodeCabang'] == $cabang)?"selected":"";
        $opt .= "<option id='".$namcab['KodeCabang']."' value='".$namcab['KodeCabang']."'".$selected." >".$namcab['Nama']." (".$namcab['KodeCabang'].")</option>";

        while ($row = $db->sql_fetchrow($result)){
            $selected = ($row[0] == $cabang)?"selected":"";
            $opt .= "<option id='$row[0]' value='$row[0]' $selected>$row[1] ($row[0])</option>";
        }
    }
    else{
        $opt .="<option selected=selected>Error</option>";
    }

    return $opt;
}
function setOptTujuan($cabang_asal,$pilih){
    //SET COMBO cabang
    global $db;
    $sql =
        "SELECT IdJurusan, KodeJurusan, KodeCabangTujuan, f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan
			WHERE KodeCabangAsal LIKE '$cabang_asal' ORDER BY NamaCabangTujuan";

    if (!$result = $db->sql_query($sql)){
        //die_error("Gagal $this->ID_FILE 003");
        echo("ERROR : ".__LINE__);
    }
    $opt_cabang="<option value=''>(none)</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ($pilih == $row['KodeCabangTujuan'])?"selected":"";
            $opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
        }
    }
    else{
        echo("ERROR : ".__LINE__);exit;
    }
    return $opt_cabang;
    //END SET COMBO CABANG
}
function setOptJam($id_jurusan,$jam){
//SET COMBO JAM KEBERANGKATAN
    global $db;
    $sql = "SELECT KodeJadwal, JamBerangkat, IdJurusan FROM tbl_md_jadwal WHERE IdJurusan = $id_jurusan";
    if(!$result = $db->sql_query($sql)){
        echo("ERROR : ".__LINE__);
    }
    $opt_cabang="<option value=''>(none)</option>";
    $jam = date_format(date_create($jam),'H:i');
    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ( $jam == $row['JamBerangkat'])?"selected":"";
            $opt_cabang .="<option value='$row[KodeJadwal]' $selected>$row[JamBerangkat] ($row[KodeJadwal])</option>";
        }
    }
    else{
        echo("ERROR : ".__LINE__);exit;
    }
    return $opt_cabang;
}
function setListGroupJadwal(){
    global $db;

    $opt_jadwal="<option value=''>- silahkan pilih judul group jadwal  -</option>";

    $Jurusan= new Jurusan();
    $Jadwal = new Jadwal();

    $sql = "SELECT kode_group,
                    GROUP_CONCAT(
                        tbl_group_jadwal.KodeJadwal
                        ORDER BY
                            id_jadwal
                    ) AS Jadwal, title_group
                FROM
                    tbl_group_jadwal
                GROUP BY
                    kode_group";

    if(!$result = $db->sql_query($sql)){
        die("Error : ".__LINE__);
    };

    while ($row = $db->sql_fetchrow($result)){

        $list_jadwal = "";

        $arrayJadwal = explode(',',$row['Jadwal']);

        foreach ($arrayJadwal as $item){

            $data_jadwal = $Jadwal->ambilDataDetail($item);
            $jamberangkat = $data_jadwal['JamBerangkat'];
            $idjurusan = $data_jadwal['IdJurusan'];

            $data_jurusan = $Jurusan->ambilNamaJurusanByIdJurusan($idjurusan);

            $asal = $data_jurusan['Asal'];
            $tujuan = $data_jurusan['Tujuan'];

            $list_jadwal .= $asal." - ".$tujuan." : ".$jamberangkat.",";
        }

        $list_jadwal = str_replace(',',' =>',rtrim($list_jadwal,','));

        $title = ($row['title_group'] != "")?$row['title_group']:$list_jadwal;

        $opt_jadwal .="<option value='$row[kode_group]'>$title</option>";

    }

    return $opt_jadwal;

}
?>
