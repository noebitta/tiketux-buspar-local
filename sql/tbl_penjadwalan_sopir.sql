CREATE TABLE `tbl_penjadwalan_sopir` (
  `IdJadwal`  int(11) NOT NULL AUTO_INCREMENT ,
  `KodeJadwal`  varchar(20) NULL ,
  `IdJurusan`  int(10) NULL ,
  `TglBerangkat`  date NULL ,
  `KodeSopir`  varchar(20) NULL ,
  `NoSPJ`  varchar(50) NULL ,
  `KodeJadwalSPJ`  varchar(20) NULL ,
  `IdJurusanSPJ`  int(10) NULL ,
  `TglBerangkatSPJ`  date NULL ,
  `Keterangan` TEXT NULL,
  `StatusKehadiran` int(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (`IdJadwal`)
)
;

