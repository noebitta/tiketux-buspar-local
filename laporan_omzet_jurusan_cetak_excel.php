<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 303;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"]))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%' 
		OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%' 
		OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Jurusan":$sort_by;
		
$sql=
	"SELECT 
		IdJurusan,KodeJurusan,f_cabang_get_name_by_kode(KodeCabangAsal) AS CabangAsal,
		f_cabang_get_name_by_kode(KodeCabangTujuan) AS CabangTujuan,
		KodeCabangAsal
	FROM tbl_md_jurusan
	$kondisi_cari";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  tmj.IdJurusan,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		tmj.IdJurusan IN (SELECT IdJurusan
		FROM tbl_md_jurusan
		$kondisi_cari)
	GROUP BY IdJurusan
	ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['IdJurusan']]	= $row['JadwalDibuka'];
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF(CetakTiket!=1,NoTiket,NULL)),0) AS TotalPenumpangB,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='' OR JenisPenumpang='14' OR JenisPenumpang='14_20K' OR JenisPenumpang='14_5K') AND CetakTiket=1  AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='GV' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS TotalTiket,
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,SubTotal,0),0)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['IdJurusan']]	= $row;
}

//DATA KEBERANGKATAN BY MANIFEST
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(1),0) AS TotalBerangkat
	FROM tbl_spj
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['IdJurusan']]	= $row['TotalBerangkat'];
}

//DATA KEBERANGKATAN MANIFEST PICKUP/TRANSIT
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(DISTINCT(IF(CetakTiket=1,NoSPJ,NULL))),0) AS TotalBerangkat
	FROM tbl_reservasi tr
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 AND (SELECT FlagSubJadwal FROM tbl_md_jadwal tmj WHERE tmj.KodeJadwal=tr.KodeJadwal)=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['IdJurusan']]	= $jumlah_berangkat[$row['IdJurusan']]+$row['TotalBerangkat'];
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['IdJurusan']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']					= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']				= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']						= $row['CabangAsal']."->".$row['CabangTujuan'];
	$temp_array[$idx]['KodeCabangAsal']			= $row['KodeCabangAsal'];
	$temp_array[$idx]['TotalOpenTrip']			= $data_jadwal_tersedia[$row['IdJurusan']];
	$temp_array[$idx]['TotalPenumpangB']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangB']+0;
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangU']+0;
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangM']+0;
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangK']+0;
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangKK']+0;
	$temp_array[$idx]['TotalPenumpangV']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangV']+0;
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangG']+0;
	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangR']+0;
	$temp_array[$idx]['TotalPenumpangVR']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangVR']+0;
	$temp_array[$idx]['TotalBerangkat']			= $jumlah_berangkat[$row['IdJurusan']]+0;
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['IdJurusan']]['TotalTiket']+0;
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['IdJurusan']]['TotalPenjualanTiket']+0;
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['IdJurusan']]['TotalDiscount']+0;
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['IdJurusan']]['TotalPenjualanPaket']+0;
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['IdJurusan']]['TotalPaket']+0;
	$temp_array[$idx]['TotalBiaya']					= $data_biaya_total[$row['IdJurusan']]['TotalBiaya']+0;
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}


$idx=$idx_awal_record;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$idx_ascii=65;
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Jurusan per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Jurusan');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Kode');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Open Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Booking');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Umum');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Anak/Lansia');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Member');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Special Ticket Free Staff');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Gift Voucher');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Delay');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp.Return');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Pnp. Voucher Return');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Penumpang/Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Omzet Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Total Omzet Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'Disc');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'OP Langsung');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;
$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).'3', 'L/R Kotor');
$objPHPExcel->getActiveSheet()->getColumnDimension(chr($idx_ascii))->setAutoSize(true);$idx_ascii++;

$idx=0;
$idx_row=4;

while ($idx<count($temp_array)){
	
	$idx_ascii=65;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $idx+1);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['Jurusan']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['KodeJurusan']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalOpenTrip']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalBerangkat']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangB']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangU']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangM']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangK']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangKK']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangV']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangG']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangR']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangVR']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalTiket']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenumpangPerTrip']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenjualanTiket']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPaket']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalPenjualanPaket']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalDiscount']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['TotalBiaya']);$idx_ascii++;
	$objPHPExcel->getActiveSheet()->setCellValue(chr($idx_ascii).$idx_row, $temp_array[$idx]['Total']);$idx_ascii++;
	
	$idx_row++;
	$idx++;
}
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, '=SUM(L4:L'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, '=SUM(M4:M'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, '=SUM(N4:N'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, '=SUM(O4:O'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, '=SUM(P4:P'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, '=SUM(Q4:Q'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, '=SUM(R4:R'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, '=SUM(S4:S'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('T'.$idx_row, '=SUM(T4:T'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('U'.$idx_row, '=SUM(U4:U'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('V'.$idx_row, '=SUM(V4:V'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Jurusan per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
