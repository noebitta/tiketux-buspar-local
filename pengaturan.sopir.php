<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 705;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#############################################################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//######################################################################################################################

//INCLUDE
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');

// PARAMETER
$mode 			= getVariabel("mode");

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

//ROUTER================================================================================================================
switch($mode){
    case 0:
        //VIEW LIST
        $cari         = getVariabel("cari");
        $idx_page     = getVariabel("idxpage");
        $order_by     = getVariabel("orderby");
        $sort         = getVariabel("sort");
        $scroll_value = getVariabel("scrollvalue");

        $template->assign_vars(array(
            'BCRUMP'	  =>setBcrump($id_page)
        ));
        include($adp_root_path . 'includes/page_header.php');
        showData();
        include($adp_root_path . 'includes/page_tail.php');

        exit;

    case 1:
        //SHOW DIALOG TAMBAH DATA
        showDialogTambah();

        break;

    case 1.1:
        //SIMPAN DATA
        $nrp_old        = getVariabel("nrpold");
        $id_sopir       = getVariabel("idsopir");
        $nrp            = getVariabel("nrp");
        $nama           = getVariabel("nama");
        $alamat         = getVariabel("alamat");
        $no_hp          = getVariabel("nohp");
        $no_sim         = getVariabel("nosim");
        $jenis_sim      = getVariabel("jenissim");
        $sim_expired    = getVariabel("simexpired");
        $kota_lahir     = getVariabel("kotalahir");
        $tgl_lahir      = getVariabel("tgllahir");
        $rekening_bank  = getVariabel("rekeningbank");
        $nama_bank      = getVariabel("namabank");
        $cabang_bank    = getVariabel("cabangbank");
        $outlet_kerja   = getVariabel("outletkerja");
        $is_reguler     = getVariabel("isreguler");
        $is_aktif       = getVariabel("isaktif");

        echo(prosesSimpan(strtoupper($nrp),(strtoupper($nama)),(strtoupper($alamat)),$no_hp,$no_sim,(strtoupper($jenis_sim)),$sim_expired,(strtoupper($kota_lahir)),$tgl_lahir, $rekening_bank,(strtoupper($nama_bank)),(strtoupper($cabang_bank)),$outlet_kerja,$is_reguler,$is_aktif,$id_sopir));

        exit;

    case 2:
        //SET COMBO CABANG

        $cari         = getVariabel('filter');
        $cabang_asal  = getVariabel('cabangasal');

        setListCabang($cari,$cabang_asal);

        break;

    case 3:
        //HAPUS
        $id_sopir       = getVariabel("idsopir");
        echo $id_sopir;
        echo(hapusData($id_sopir));

        break;

    case 4:
        //SHOW DATA
        $id_sopir       = getVariabel("idsopir");
        showDialogUbah($id_sopir);

        break;

    case 5:
        //UBAH STATUS AKTIF
        $id_sopir       = getVariabel("idsopir");

        ubahStatusAktif($id_sopir);

        break;

    case 6:
        //UBAH STATUS REGULER
        $id_sopir       = getVariabel("idsopir");

        ubahStatusReguler($id_sopir);

        break;


}

//METHODES & PROCESSES==================================================================================================

function showData(){

    global $db,$cari,$order_by,$sort,$idx_page,$template,$VIEW_PER_PAGE,$userdata,$id_page,$scroll_value;

    $Sopir = new Sopir();

    $template->set_filenames(array('body' => 'pengaturan.sopir/index.tpl'));


    //cek permission untuk CRUD
    $Permission = new Permission();

    if($Permission->isPermitted($userdata["user_level"],$id_page.".1")){
        $template->assign_block_vars("CRUD_ADD",array());
        $template->assign_block_vars("CRUD_DEL",array());
        $template->assign_block_vars("EXPORT",array());
    }

    $result = $Sopir->ambilData($order_by,$sort,$idx_page,$cari);

    //PAGING==============================================================================================================
    $paging=setPaging($idx_page,"formdata");
    //END PAGING==========================================================================================================

    $no = 0;

    while($row=$db->sql_fetchrow($result)){
        $no++;

        $odd = ($no%2)==0?"even":"odd";

        if($row["StatusAktif"]=="AKTIF"){
            $class_crud_aktif = "crudgreen";
        }
        else{
            $odd  = "red";
            $class_crud_aktif = "crud";
        }

        $class_crud_reguler = ($row["StatusReguler"]=="BACKUP")?"crud":"crudgreen";

        $template->assign_block_vars(
            'ROW',array(
                'odd'             => $odd,
                'idx'             => $no,
                'no'              => $idx_page*$VIEW_PER_PAGE+$no,
                'idsopir'         => $row['IdSopir'],
                'nama'            => $row["Nama"],
                'nrp'             => $row["KodeSopir"],
                'nohp'            => $row["HP"],
                'alamat'          => $row["Alamat"],
                'nosim'           => $row["NoSIM"],
                'jenissim'        => $row["JenisSIM"],
                'simexpired'      => FormatMySQLDateToTgl($row["SIMExpired"]),
                'kotalahir'       => $row["KotaLahir"],
                'tgllahir'        => FormatMySQLDateToTgl($row["TglLahir"]),
                'rekeningbank'    => $row["Norek"],
                'namabank'        => $row["NamaBank"],
                'cabangbank'      => $row["CabangBank"],
                'outletkerja'     => $row["KodeCabang"],
                'reguler'         => $row["StatusReguler"],
                'aktif'           => $row["StatusAktif"],
                'classcrudreguler'=> $class_crud_reguler,
                'classcrudaktif'  => $class_crud_aktif
            )
        );

        //ACTION
        $template->assign_block_vars("ROW.ACT_EDIT", array());
        $template->assign_block_vars("ROW.ACT_DEL", array());

    }

    if($no>0){
        $template->assign_block_vars("TABLE_HEADER",array());
    }
    else{
        $template->assign_block_vars("NO_DATA",array());
    }

    $template->assign_vars(array(
            'URL_CRUD'	    => basename(__FILE__),
            'CARI'          => $cari,
            'ORDER'         => $order_by,
            'SORT'          => $sort,
            'IDX_PAGE'      => $idx_page,
            'PAGING'        => $paging,
            'U_EXPORT_EXCEL'=> substr(basename(__FILE__),0,-4).".export.excel.php",
            'SCROLL_VALUE'  => ($scroll_value==""?0:$scroll_value),
            'JUDUL_HALAMAN' => "pengaturan sopir",
            'JUDUL_TABEL'   => "data sopir"
        )
    );

    $template->pparse('body');

} //showData

function showDialogTambah(){
    global $template;

    $template->set_filenames(array('body' => 'pengaturan.sopir/detail.tpl'));

    $template->assign_vars(array(
            "JUDUL"	             => "tambah data sopir",
            "STATUS_AKTIF"       => 0,
            "STATUS_REGULER"     => 0,
        )
    );

    $template->pparse('body');
}//showDialogTambah

function showDialogUbah($id_sopir){
    global $template;

    $Sopir = new Sopir();

    $template->set_filenames(array('body' => 'pengaturan.sopir/detail.tpl'));

    $data = $Sopir->ambilDetailData($id_sopir);

    $template->assign_vars(array(
            "JUDUL"	          => "ubah data sopir",
            'ID_SOPIR'        => $data["IdSopir"],
            'NAMA'            => $data["Nama"],
            'NRP'             => $data["KodeSopir"],
            'NO_HP'           => $data["HP"],
            'ALAMAT'          => $data["Alamat"],
            'NO_SIM'          => $data["NoSIM"],
            'JENIS_SIM'       => $data["JenisSIM"],
            'SIM_EXPIRED'     => FormatMySQLDateToTgl($data["SIMExpired"]),
            'KOTA_LAHIR'      => $data["KotaLahir"],
            'TGL_LAHIR'       => FormatMySQLDateToTgl($data["TglLahir"]),
            'NO_REK'          => $data["Norek"],
            'NAMA_BANK'       => $data["NamaBank"],
            'CABANG_BANK'     => $data["CabangBank"],
            'CABANG_ASAL'     => $data["KodeCabang"],
            "STATUS_REGULER"  => $data["IsReguler"],
            "STATUS_AKTIF"    => $data["IsAktif"]

        )
    );
    $template->pparse('body');
}//showDialogUbah

function prosesSimpan($nrp,$nama,$alamat,$no_hp,$no_sim,$jenis_sim,$sim_expired,$kota_lahir,$tgl_lahir,$rekening_bank,$nama_bank,$cabang_bank,$outlet_kerja,$is_reguler,$is_aktif,$id_sopir=""){

    $Sopir = new Sopir();

    if($id_sopir=="") {
        //proses tambah
        $id_sopir=$Sopir->tambah($nrp,$nama,$alamat,$no_hp,$no_sim,$jenis_sim,FormatTglToMySQLDate($sim_expired),$kota_lahir,FormatTglToMySQLDate($tgl_lahir),$rekening_bank,$nama_bank,$cabang_bank,$outlet_kerja,$is_reguler,$is_aktif);
    }
    else {
        $Sopir->ubah($id_sopir,$nrp,$nama,$alamat,$no_hp,$no_sim,$jenis_sim,FormatTglToMySQLDate($sim_expired),$kota_lahir,FormatTglToMySQLDate($tgl_lahir),$rekening_bank,$nama_bank,$cabang_bank,$outlet_kerja,$is_reguler,$is_aktif);
    }
    $return = array("status" => "OK", "idsopir" => $id_sopir);
    return json_encode($return);
}//prosesSimpan

function setListCabang($cari="",$cabang_asal=""){
    global $db;

    $Cabang = new Cabang();

    $result = $Cabang->setComboListCabang($cari,$cabang_asal);

    $id = $cabang_asal==""?"KodeCabang":"IdJurusan";

    while($row=$db->sql_fetchrow($result)){
        $list_cabang[]=array("group"=>$row["Kota"],"id"=>$row[$id],"text"=>$row["Nama"].($row["KodeJurusan"]==""?"":" ($row[KodeJurusan])"));
    }

    $ret_val  = json_encode($list_cabang);

    echo($ret_val);
}//setListCabang

function hapusData($id_sopir){
    $Sopir = new Sopir();

    $Sopir->hapus($id_sopir);

    $return = array("status" => "OK","id_sopir"=>$id_sopir);

    return json_encode($return);
}

function ubahStatusAktif($id_sopir){
    $Sopir = new Sopir();

    if($Sopir->ubahStatusAktif($id_sopir)){
        $ret_val["status"]  = "OK";
    }
    else{
        $ret_val["status"]  = "GAGAL";
    }

    echo(json_encode($ret_val));
}//ubahStatusAktif

function ubahStatusReguler($id_sopir){
    $Sopir = new Sopir();

    if($Sopir->ubahStatusReguler($id_sopir)){
        $ret_val["status"]  = "OK";
    }
    else{
        $ret_val["status"]  = "GAGAL";
    }

    echo(json_encode($ret_val));
}//ubahStatusReguler

?>