<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 705;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassRole.php');

// PARAMETER
$mode 			= isset($HTTP_POST_VARS['mode'])? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
$id_role 		= isset($HTTP_POST_VARS['id'])? $HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];

//INIT
$mode       = $mode==""?0:$mode;

//ROUTER
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    exit;

  case 1:
    //SIMPAN
    $json_list_page = isset($HTTP_POST_VARS['listpage'])? $HTTP_POST_VARS['listpage'] : $HTTP_GET_VARS['listpage'];

    $json_list_page = preg_replace('/[^A-Za-z0-9\-\.\,\[\]]/','',$json_list_page);

    $list_page      = json_decode($json_list_page);

    echo(simpan($id_role,$list_page));

    exit;

}

//METHODES & PROCESS

function showData(){

  global $db,$template,$Permission,$id_role;

  $Role = new Role();

  $template->set_filenames(array('body' => 'pengaturan.permission/index.tpl'));

  $result = $Role->ambilData();

  while($row=$db->sql_fetchrow($result)){

    $id_role  = $id_role==""?$row["IdRole"]:$id_role;

    $template->assign_block_vars("OPT_ROLE",array(
      "id"        => $row["IdRole"],
      "nama"      => $row["NamaRole"],
      "selected"  => ($row["IdRole"]!=$id_role?"":"selected")
    ));
  }

  //MENGAMBIL LIST HALAMAN YANG BOLEH DIAKSES BERDASARKAN ROLE
  $list_permission  = $Permission->listPermission($id_role);

  $result_group = $Permission->ambilGroupPage();

  $no = 0;

  while($data_group=$db->sql_fetchrow($result_group)) {
    $no++;

     if(in_array($data_group["PageId"],$list_permission)){
       //JIKA DIIJINKAN
       $is_group_permitted  = true;
       $group_checked       = "checked";
     }
    else{
      //JIKA TIDAK DIIJINKAN
      $is_group_permitted   = false;
      $group_checked        = "";
    }

    $template->assign_block_vars(
      'ROW',array(
        'odd'       => "yellow",
        'no'        => $no,
        'idgroup'   => $data_group["PageId"],
        'id'        => $data_group["PageId"],
        'checked'   => $group_checked,
        'groupmenu' => $data_group["NamaTampil"],
        'onclick'   => "selDeselCheckbox(this.checked,$data_group[PageId])"
      )
    );

    $result_menu  = $Permission->ambilMenuPage($data_group["PageId"]);

    while($data_menu=$db->sql_fetchrow($result_menu)){

      $no++;

      $odd =($no%2)==0?"even":"odd";

      if(in_array($data_menu["PageId"],$list_permission)){
        //JIKA DIIJINKAN
        $is_menu_permitted  = true;
        $menu_checked       = "checked";
      }
      else{
        //JIKA TIDAK DIIJINKAN
        $is_menu_permitted    = false;
        $menu_checked         = "";
      }

      $template->assign_block_vars(
        'ROW',array(
          'odd'       =>$odd,
          'no'        => $no,
          'idgroup'   => $data_group["PageId"],
          'id'        => $data_menu["PageId"],
          'checked'   => ($is_group_permitted?$menu_checked:""),
          'menu'      => $data_menu["NamaTampil"],
          'onclick'   => "selDeselCheckbox(this.checked,$data_menu[PageId])"
        )
      );

      $result_submenu  = $Permission->ambilMenuPage($data_menu["PageId"]);

      while($data_submenu=$db->sql_fetchrow($result_submenu)) {

        $no++;

        $odd = ($no % 2) == 0 ? "even" : "odd";

        $template->assign_block_vars(
          'ROW', array(
            'odd'     => $odd,
            'no'      => $no,
            'idgroup' => $data_menu["PageId"],
            'id'      => $data_submenu["PageId"],
            'checked' => ($is_menu_permitted?(in_array($data_submenu["PageId"],$list_permission)?"checked":""):""),
            'submenu' => $data_submenu["NamaTampil"]
          )
        );

      }
    }

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	=> basename(__FILE__),
      'ID'	      => $id_role
    )
  );


  $template->pparse('body');

}

function simpan($id_role,$list_page){

  global $Permission;

  if($Permission->simpan($id_role,$list_page)){
    $return = array("status" => "OK","id"=>$id_role);
  }
  else{
    $return = array("status" => "GAGAL","error"=>"kegagalan query!");
  }

  return json_encode($return);
}

?>