<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);

$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');
// SESSION
$id_page = 607;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################
// HEADER

$page_title	= "Menu Daftar Transaksi";
$interface_menu_utama=true;

$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$sort_by		= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order			= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$cari           = isset($HTTP_GET_VARS["txt_cari"])?$HTTP_GET_VARS["txt_cari"]:$HTTP_POST_VARS["txt_cari"];
$Member	= new Member();
$Cabang	= new Cabang();

// LIST
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= date_format(date_create($tanggal_mulai),'Y-m-d');
$tanggal_akhir_mysql	= date_format(date_create($tanggal_akhir),'Y-m-d');

$template->set_filenames(array('body' => 'member/daftar_reedem_ponta.tpl'));


$kondisi	=($cari=="")?"":
            " AND (r.Nama LIKE '%$cari%'
				OR r.Telp LIKE '%$cari%'
				OR m.IdMember LIKE '%$cari%'
				OR p.IdCard LIKE '%$cari%'
				OR p.KodeBooking LIKE '%$cari%')";

$kondisi_paging = "WHERE (DATE(WaktuReedem) BETWEEN '".$tanggal_mulai_mysql."' AND '".$tanggal_akhir_mysql."')".$kondisi;
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"IdPontaReedem","tbl_ponta_reedem","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",$kondisi_paging,"pengaturan_member_daftar_reedem.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql =" SELECT p . * , r.Nama AS Nama, r.Telp AS Telp
        FROM tbl_ponta_reedem AS p
        LEFT JOIN tbl_md_member AS m ON m.NoSeriKartu = p.IdCard
        LEFT JOIN tbl_reservasi AS r ON r.NoTiket = p.NoTiket
        WHERE (DATE(p.WaktuReedem) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	    $kondisi ORDER BY KodeBooking LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if ($result = $db->sql_query($sql)){
    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
            $odd = 'even';
        }

        if($row['TipeReedem'] == 0){
            $odd = 'red';
        }

        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'=>$odd,
                'no'=>$i,
                'nama'=>$row['Nama'],
                'hp'=>$row['Telp'],
                'IdMember'=>$row['IdMember'],
                'PontaID'=>$row['IdCard'],
                'tgl'=>date_format(date_create($row['WaktuReedem']),'d-m-Y H:i:s'),
                'kodebooking'=>$row['KodeBooking'],
                'notiket'=>$row['NoTiket'],
                'jmlReedem'=>number_format($row['JumlahReedem'],0,',','.'),
                'jmlPoin'=>number_format($row['JumlahPoin'],0,',','.'),
                'jmlTagihan'=>number_format($row['JumlahTagihan'],0,',','.')
            )
        );
        $i++;
    }
    if($i-1<=0){
        $no_data	=	"<div class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></div>";
    }
}else{
    die(mysql_error());
    echo("Error :".mysql_error);exit;
}

$page_title	= "Reedem Ponta";
$order_invert	= ($order=='asc' || $order=='')?'desc':'asc';

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&cari=".$cari."&sort_by=".$sort_by."&order=".$order_invert."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir;

//$script_cetak_pdf="Start('pengaturan_member_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";

$script_cetak_excel="Start('daftar_redeem_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('pengaturan_member_daftar_transaksi_ponta.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'TGL_AWAL'			=> $tanggal_mulai,
        'TGL_AKHIR'			=> $tanggal_akhir,
        'NO_DATA'			=> $no_data,
        'PAGING'			=> $paging,
        'CETAK_XL'			=> $script_cetak_excel
    )
);
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>