<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassPaketEkspedisi.php');

//SESSION
$id_page = 202;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tgl_berangkat		= $HTTP_GET_VARS['tgl_berangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal'];
$sopir_sekarang		= $HTTP_GET_VARS['sopir_sekarang'];
$mobil_sekarang		= $HTTP_GET_VARS['mobil_sekarang'];

//tombol OK di klik untuk mencetak SPJ
$Reservasi	= new Reservasi();
$Jadwal			= new Jadwal();
$Sopir			= new Sopir();
$Mobil			= new Mobil();
$BiayaOperasional	= new BiayaOperasional();
$Paket			= new Paket();

$sopir_dipilih= $HTTP_GET_VARS['sopir_dipilih'];
$mobil_dipilih= $HTTP_GET_VARS['mobil_dipilih'];
$no_spj				= $HTTP_GET_VARS['no_spj'];

$nourut = rand(1000,9999);
$useraktif=$userdata['user_id'];

if ($tgl_berangkat=='' || $kode_jadwal==''){
	exit;
}
	
#mengambil data jadwal
$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);

if($data_jadwal['FlagSubJadwal']!=1){
	$kode_jadwal_utama	= $kode_jadwal;
}
else{
	$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
}

$jam_berangkat_show	= $data_jadwal['JamBerangkat'];
$list_kode_jadwal		= "'$kode_jadwal'";

$row	= $Sopir->ambilDataDetail($sopir_dipilih);
$nama_sopir = $row['Nama'];

$row				= $Mobil->ambilDataDetail($mobil_dipilih);
$no_polisi 	= $row['NoPolisi'];

$list_field_diupdate="";

//Mengambil jumlah paket dan omzet
$data_total_paket	=$Paket->hitungTotalOmzetdanJumlahPaketPerSPJ($tgl_berangkat,$list_kode_jadwal);

$total_omzet_paket=($data_total_paket['TotalOmzet']!='')?$data_total_paket['TotalOmzet']:0;
$jumlah_paket			=($data_total_paket['JumlahPaket']!='')?$data_total_paket['JumlahPaket']:0;	

//Mengambil pembiayaan berdasarkan jurusan
$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);

$data_spj	= $Paket->ambilDataHeaderLayout($tgl_berangkat,$kode_jadwal_utama);

if($data_spj['NoSPJ']==""){
	//jika spj belum pernah dicetak, maka akan menambahkan biaya ke database
	
	$no_spj= "MNF-X".substr($kode_jadwal_utama,0,3).dateYMD().$nourut;
	
	$Paket->tambahSPJ(
		$no_spj, $kode_jadwal_utama, $tgl_berangkat, 
		$jam_berangkat_show, $mobil_dipilih, $useraktif, $sopir_dipilih,
		$nama_sopir, $jumlah_paket,$total_omzet_paket);
	
	//biaya sopir
	if($data_biaya['BiayaSopir']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaSopir'],$FLAG_BIAYA_SOPIR,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaSopir'],
			$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
	}
	
	//biaya tol
	if($data_biaya['BiayaTol']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaTol'],$FLAG_BIAYA_TOL,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaTol'],
			$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
	}
	
	//biaya parkir
	if($data_biaya['BiayaParkir']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaParkir'],$FLAG_BIAYA_PARKIR,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaParkir'],
			$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
	}
	
	//biaya bbm
	if($data_biaya['BiayaBBM']>0){
		$BiayaOperasional->tambah(
			$no_spj,$data_biaya['KodeAkunBiayaBBM'],$FLAG_BIAYA_BBM,
			$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaBBM'],
			$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
	}
	
	$duplikat	= ""; 
	
}
else{

	$Paket->ubahSPJ(
		$data_spj['NoSPJ'], $mobil_dipilih, $useraktif, 
		$sopir_dipilih, $nama_sopir, $jumlah_paket,
		$total_omzet_paket);
		
	
	$no_spj	= $data_spj['NoSPJ'];
	
	$duplikat	= "<br>****DUPLIKAT****";
}
	
//update data paket di tbl_paket
$Reservasi->ubahDataReservasiCetakSPJ(
	$list_kode_jadwal, $tgl_berangkat,$sopir_dipilih,
	$mobil_dipilih,$no_spj);
	
//update tblspj untuk insentif sopir
$Paket->updateInsentifSopir(
	$no_spj, $INSENTIF_SOPIR_JUMLAH_PAKET_MAKSIMUM, $INSENTIF_SOPIR_JUMLAH_PAKET_MINIMUM, $data_biaya['KomisiPaketSopir']);

if($jumlah_paket>0){
	$result_paket = $Paket->ambilDataPaketUntukSPJ($tgl_berangkat,$list_kode_jadwal);
	$idx_no=0;
	
	while($row_paket=$db->sql_fetchrow($result_paket)){
		$idx_no++;
		
		$template ->assign_block_vars(
			'ROW_PAKET',
			array(
				'IDX_PAKET_NO'=>"(".$idx_no.")",
				'NO_TIKET_PAKET'=>$row_paket['NoTiket'],
				'TUJUAN'=>$row_paket['Tujuan'],
				'NAMA_PENGIRIM'=>$row_paket['NamaPengirim'],
				'TELP_PENGIRIM'=>$row_paket['TelpPengirim'],
				'NAMA_PENERIMA'=>$row_paket['NamaPenerima'],
				'TELP_PENERIMA'=>$row_paket['TelpPenerima']
				)
		);
	}
}
else{
	$tidak_ada_paket	= '<br>TIDAK ADA PAKET<br><br>';
}

$data_perusahaan	= $Reservasi->ambilDataPerusahaan();

$template->set_filenames(array('body' => 'SPJ_paket_body.tpl')); 
$template->assign_vars(array(
   'DUPLIKAT'    				=>$duplikat,
   'NAMA_PERUSAHAAN'    =>$data_perusahaan['NamaPerusahaan'],
   'ALAMAT_PERUSAHAAN'  =>$data_perusahaan['AlamatPerusahaan'],
   'TELP_PERUSAHAAN' 		=>$data_perusahaan['TelpPerusahaan'],
   'NO_SPJ' 						=>$no_spj,
   'TGL_BERANGKAT' 			=>dateparse(FormatMySQLDateToTgl($tgl_berangkat)),
   'JURUSAN' 						=>$kode_jadwal." ".$jam_berangkat_show,
   'TGL_CETAK' 					=>FormatMySQLDateToTglWithTime(dateNow(true)),
   'NO_POLISI' 					=>$mobil_dipilih." (".$no_polisi.")",
   'SOPIR' 							=>$nama_sopir." (".$sopir_dipilih.")",
   'TIDAK_ADA_PAKET' 		=>$tidak_ada_paket,
   'JUMLAH_PAKET' 			=>$jumlah_paket,
   'OMZET_PAKET' 				=>number_format($total_omzet_paket,0,",","."),
	 'CSO' 								=>$userdata['nama'],
   'EMAIL_PERUSAHAAN' 	=>$data_perusahaan['EmailPerusahaan'],
   'WEBSITE_PERUSAHAAN' =>$data_perusahaan['WebSitePerusahaan'],
	 'SID'								=>$userdata['session_id']
   )
);

$template->pparse('body');	
	
?>