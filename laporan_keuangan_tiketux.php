<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 501;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$fil_status = isset($HTTP_GET_VARS['filterstatus'])? $HTTP_GET_VARS['filterstatus'] : $HTTP_POST_VARS['filterstatus'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_keuangan_tiketux/laporan_keuangan_tiketux_body.tpl')); 

$Cabang	= new Cabang();

switch($mode){
	case 'getasal':
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
}

if($HTTP_POST_VARS['btn_cari']=='cari' || $tanggal_mulai==''){
	
	$cari	= $HTTP_POST_VARS["txt_cari"];
	
	$kondisi_cari	= $cari==""?"":" AND (Nama LIKE '%$cari%'
															OR Telp LIKE '%$cari%'
															OR HP LIKE '%$cari%'
															OR KodeJadwal LIKE '%$cari%'
															OR NoSPJ LIKE '%$cari%'
															OR KodeBooking LIKE '%$cari%'
															OR JamBerangkat LIKE '$cari'
															OR NoTiket LIKE '%$cari%')";
	
	if($kota!=""){
		$kondisi_cabang	.= " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'";
	}
	
	if($asal!=""){
		$kondisi_cabang	.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'";
	}
	
	if($asal!="" && $tujuan!=""){
		$kondisi_cabang	.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'";
	}
	
	if($fil_status!=""){
		switch($fil_status){
			case "0"	:
				//deposit
				$kondisi_status	= " AND IsSettlement=0 ";
				break;
			case "1"	:
				//settlemet
				$kondisi_status	= " AND IsSettlement=1 ";
				break;
		}
	}

	
	$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
	$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
	$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
	$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
	
	$kondisi_tanggal	= " AND (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')";
			
	//CREATE VIEW
	$sql	= "DROP VIEW IF EXISTS v_laporan_keuangan_tiketux".$userdata['user_id'].";";
	
	if (!$result_laporan = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	
	$sql	= "CREATE VIEW v_laporan_keuangan_tiketux".$userdata['user_id']." AS SELECT * FROM tbl_reservasi WHERE PetugasPenjual=0 $kondisi_cabang $kondisi_tanggal $kondisi_status $kondisi_cari;";
	
	if (!$result_laporan = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}
	
	$sql=
		"INSERT IGNORE INTO v_laporan_keuangan_tiketux".$userdata['user_id']." (SELECT * FROM tbl_reservasi_olap WHERE PetugasPenjual=0 $kondisi_cabang $kondisi_tanggal $kondisi_status $kondisi_cari);";
	
	/*if (!$result_laporan = $db->sql_query($sql)){
		//echo("Err:".__LINE__);exit;
		die(mysql_error());
	}*/
}

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?"WaktuCetakTiket":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoTiket","v_laporan_keuangan_tiketux".$userdata['user_id'],
"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order","","laporan_keuangan_tiketux.php",
$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================


//DATA PENJUALAN TIKET
$sql	= 
	"SELECT *,(HargaTiketux-Komisi) AS TotalJual,f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM v_laporan_keuangan_tiketux".$userdata['user_id']."
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=0;

//PLOT DATA
while($row = $db->sql_fetchrow($result)){
	
	$i++;
	
	if($row['FlagBatal']!=1){
		
		if($row['CetakTiket']!=1){
			
			$odd = $i % 2==0?'odd':'even';
			$status	= "Book";
		}
		else{
			$odd	= "green";
			$status	= "OK";
		}
		$keterangan="";
		
	}
	else{
		
		$odd	= $row['CetakTiket']!=1?'yellow':'red';
		
		$status	= "BATAL ".$row['CetakTiket'];
		
		$keterangan	= $row['NamaCSOPembatalan'];
		
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'waktu_cetak_tiket'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
				'kode_booking'=>$row['KodeBooking'],
				'no_tiket'=>$row['NoTiket'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'no_kursi'=>$row['NomorKursi'],
				'harga_tiket'=>number_format($row['HargaTiketux'],0,",","."),
				'discount'=>number_format($row['Discount'],0,",","."),
				'komisi'=>number_format($row['Komisi'],0,",","."),
				'total'=>number_format($row['TotalJual'],0,",","."),
				'tipe_discount'=>$row['JenisDiscount'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
		
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota."&asal=".$asal."&status=".$fil_status.
										"&tujuan=".$tujuan."&sort_by=".$sort_by."&order=".$order."";
	
$script_cetak_pdf="Start('laporan_keuangan_tiketux_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_keuangan_tiketux_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&status=$fil_status&asal=$asal&tujuan=$tujuan&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "opt_status".$fil_status;
$$temp_var	= "selected";

$sql	= 
	"SELECT
		COUNT(DISTINCT(KodeBooking)) AS TotalPesanan,
		COUNT(NoTiket) AS TotalTiket,
		SUM(HargaTiketux) AS TotalPenjualan,
		SUM(Discount) AS TotalDiscount,
		SUM(HargaTiketux-Komisi) AS TotalPenerimaan,
		SUM(Komisi) AS TotalKomisi
	FROM v_laporan_keuangan_tiketux".$userdata['user_id'];
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_total = $db->sql_fetchrow($result);

//$kota	= $kota!=""?$kota:"JAKARTA";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_keuangan_tiketux.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> setComboKota($kota),
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'OPT_STATUS'		=> $opt_status,
	'OPT_STATUS0'		=> $opt_status0,
	'OPT_STATUS1'		=> $opt_status1,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=WaktuCetakTiket'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan waktu cetak tiket ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=NoTiket'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan no tiket ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=TglBerangkat,JamBerangkat'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan waktu berangkat ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan kode jadwal ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan nama ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=NomorKursi'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan nomor kursi ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=HargaTiket'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Total harga tiket ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=Komisi'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Total komisi ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=TotalJual'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan Total ($order_invert)",
	'A_SORT_10'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=JenisDiscount'.$parameter_sorting),
	'TIPS_SORT_10'	=> "Urutkan Jenis Diskon ($order_invert)",
	'A_SORT_11'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=CetakTiket'.$parameter_sorting),
	'TIPS_SORT_11'	=> "Urutkan Total ($order_invert)",
	'A_SORT_12'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=NamaCSOPembatalan'.$parameter_sorting),
	'TIPS_SORT_12'	=> "Urutkan Cso Pembatal ($order_invert)",
	'A_SORT_13'			=> append_sid('laporan_keuangan_tiketux.'.$phpEx.'?sort_by=KodeBooking'.$parameter_sorting),
	'TIPS_SORT_13'	=> "Urutkan Kode Booking ($order_invert)",
	'TOTAL_TIKET'		=> number_format($data_total['TotalTiket'],0,",","."),
	'TOTAL_PESANAN'	=> number_format($data_total['TotalPesanan'],0,",","."),
	'TOTAL_PENJUALAN'	=> number_format($data_total['TotalPenjualan'],0,",","."),
	'TOTAL_DISCOUNT'=> number_format($data_total['TotalDiscount'],0,",","."),
	'TOTAL_KOMISI'	=> number_format($data_total['TotalKomisi'],0,",","."),
	'TOTAL_PENERIMAAN'=> number_format($data_total['TotalPenerimaan'],0,",",".")
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>