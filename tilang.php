<?php
/**
 * Created by PhpStorm.
 * User: assus
 * Date: 1/21/2016
 * Time: 14:15 PM
 */

define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSuratJalan.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');

$id_page = 216;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$asal 			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan 		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$Jurusan		= new Jurusan();
$Cabang			= new Cabang();
$SuratJalan	= new SuratJalan();
$Mobil			= new Mobil();
$Sopir			= new Sopir();

switch($mode){
  case "":
    $sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
    $order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];


    $order	=($order=='')?"ASC":$order;

    $sort_by =($sort_by=='')?"NoST":$sort_by;

    // LIST
    $template->set_filenames(array('body' => 'tilang/index_body.tpl'));

    if($HTTP_POST_VARS["txt_cari"]!=""){
        $cari=$HTTP_POST_VARS["txt_cari"];
    }
    else{
        $cari=$HTTP_GET_VARS["cari"];
    }

    $kondisi	=($cari=="")?"": " AND(NoST LIKE '%$cari'
	    OR NamaSopir LIKE '%$cari%'
	    OR NoPolisi LIKE '%$cari%')";

    //PAGING======================================================
    $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
    $paging		= pagingData($idx_page,"NoST","tbl_tilang","&cari=$cari&sort_by=$sort_by&order=$order",$kondisi,"tiket.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
    //END PAGING======================================================

    $sql =
      "SELECT *
			FROM tbl_tilang
			WHERE 1 $kondisi
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

    $idx_check=0;

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load jurusan',__FILE__,__LINE__,$sql);
      echo("Err:".__LINE__);exit;
    }

    $i = $idx_page*$VIEW_PER_PAGE+1;

    while ($row = $db->sql_fetchrow($result)){
      $odd ='odd';

      if (($i % 2)==0){
        $odd = 'even';
      }

      $tgl_jatuh_tempo = new DateTime($row['JatuhTempo']);
      $today = new DateTime(date('Y-m-d'));
      $interval = $tgl_jatuh_tempo->diff($today)->format("%a");
      //$jt = explode(" ",$row['JatuhTempo']);
      if($interval <= 7 && (strtotime($row['JatuhTempo']) >= strtotime(date('Y-m-d')))){
        $template->assign_block_vars(
          'ROWALERT',array(
            'odd'=>$odd,
            'no'=>$i,
            'nost'=>$row['NoST'],
            'sopir'=>$row['NamaSopir'],
            'kendaraan'=>$row['NoPolisi'],
            'tgl_tilang'=>date_format(date_create($row['TanggalTilang']),'d-m-Y'),
            'tempo'=>date_format(date_create($row['JatuhTempo']),'d-m-Y'),
            'petugas'=>$row['NamaPetugas'],
            'denda'=>number_format($row['Denda'],0,',','.'),
            'act'=>$act
          )
        );
      }

      $idx_check++;

      $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";

      $act 	="<a href='".append_sid('tilang.'.$phpEx.'?mode=edit&id='.$row['Id'])."'>Edit</a> + ";
      $act .="<a  href='' onclick='return hapusData(\"$row[Id]\");'>Delete</a>";

      $template->assign_block_vars(
        'ROW', array(
          'odd'=>$odd,
          'check'=>$check,
          'no'=>$i,
          'nost'=>$row['NoST'],
          'sopir'=>$row['NamaSopir'],
          'kendaraan'=>$row['NoPolisi'],
          'tgl_tilang'=>date_format(date_create($row['TanggalTilang']),'d-m-Y'),
          'tempo'=>date_format(date_create($row['JatuhTempo']),'d-m-Y'),
          'petugas'=>$row['NamaPetugas'],
          'denda'=>number_format($row['Denda'],0,',','.'),
          'act'=>$act
        )
      );

      $i++;
    }

    if($i-1<=0){
      $no_data	=	"<tr><td colspan=15 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
    }

    $page_title	= "Surat Tilang";

    $temp_var	= "pool_".$pool;
    $$temp_var = "selected";

    $template->assign_vars(array(
      'BCRUMP'    			=>setBcrump($id_page),
      'U_ADD'						=> append_sid('tilang.'.$phpEx.'?mode=add'),
      'ACTION_CARI'			=> append_sid('tilang.'.$phpEx),
      'TGL_AWAL'				=> $tanggal_mulai,
      'TGL_AKHIR'				=> $tanggal_akhir,
      'OPT_KOTA'				=> setComboKota($kota),
      'POOL_0'					=> $pool_0,
      'POOL_1'					=> $pool_1,
      'TXT_CARI'				=> $cari,
      'NO_DATA'					=> $no_data,
      'KETERANGAN_SORT'	=> "sort by: ".$sort_by." ".$order,
      'PAGING'					=> $paging
      )
    );

    break;

  case "getasal":
    $act_idx	= $HTTP_GET_VARS['idx']==""?"":",".$HTTP_GET_VARS['idx'];

    echo "
			<select name='asal$idx' id='asal$idx' onChange='getUpdateTujuan(this.value,$act_idx);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";

    exit;

  case "gettujuan":
    $idx	= $HTTP_GET_VARS['idx'];

    $next_idx		= $idx+1;

    $asals	= explode("|",$asal);

    echo "
			<select name='tujuan$idx' id='tujuan$idx' onChange='getUpdateTujuan(this.value,$next_idx);getBiaya(this.value,$idx);'>
				".setComboCabangTujuan($asals[0],$asals[1])."
			</select>";
    exit;

  case "setdetailbiaya":
    $no_sj= $HTTP_GET_VARS["no_sj"];
    $trip	= $HTTP_GET_VARS["trip"];

    $asal		= array();
    $tujuan	= array();

    if($no_sj!=""){
      //JIKA NO SURAT JALAN TIDAK KOSONG, BERARTI MENGAMBIL DATA DARI DATABASE
    }

    $output	=
      "<table width='500'>
				<tr><td colspan=3><h3>Detail Biaya:</h3></td></tr>";

    for($idx=0;$idx<$trip;$idx++){
      $no_trip	= $idx+1;

      $output .=($no_trip!=1)?"":"
				<tr>
					<td valign='top'><b>ASAL</b></td><td  valign='top'>:</td>
					<td valign='top'>
						<select name='asal$idx' id='asal$idx' onChange='getUpdateTujuan(this.value,$idx);'>
							".$Cabang->setInterfaceComboCabangByKota("",$asal[$idx],"")."
						</select>
					</td>
				</tr>";

      $output	.=
        "<tr><td valign='top' colspan='3'><b>TRIP $no_trip</b></td></tr>
				<tr>
					<td valign='top'>Tujuan</td><td  valign='top'>:</td>
					<td valign='top'><span id='rewrite_tujuan$idx'></span></td>
				</tr>
				<tr><td valign='top' colspan='3'><br><b>Biaya-biaya</b></td></tr>
				<tr><td colspan='3'><span id='rewrite_biaya$idx'></span></td></tr>
				<tr><td colspan='3'><hr></td></tr>";
    }

    $output.="</table>";

    echo($output);
    exit;

  case "getbiaya":
    $idx	= $HTTP_GET_VARS['idx'];

    if($no_sj!=""){
      //JIKA NO SURAT JALAN TIDAK KOSONG, BERARTI MENGAMBIL DATA DARI DATABASE

    }

    $asals	= explode("|",$asal);
    $data_biaya	= $Jurusan->ambilBiaya($asals[1]);

    $output	= "<table padding=0 spacing=0>";

    //BBM
    $output	.="<tr><td>Voucher BBM</td><td width='5'>:</td><td align='right'>Rp. ".number_format($data_biaya['BiayaBBM'],0,",",".")."<input type='hidden' name='biaya_bbm$idx' value='".$data_biaya['BiayaBBM']."'><input type='hidden' name='coa_bbm$idx' value='".$data_biaya['KodeAkunBiayaBBM']."'></td></tr>";

    //TOL
    $output	.="<tr><td>Biaya TOL</td><td width='5'>:</td><td align='right'>Rp. ".number_format($data_biaya['BiayaTol'],0,",",".")."<input type='hidden' name='biaya_tol$idx' value='".$data_biaya['BiayaTol']."'><input type='hidden' name='coa_tol$idx' value='".$data_biaya['KodeAkunBiayaTol']."'></td></tr>";

    //UANG ORDER
    $output	.="<tr><td>Biaya ORDER</td><td width='5'>:</td><td align='right'>Rp. ".number_format($data_biaya['BiayaSopir'],0,",",".")."<input type='hidden' name='biaya_sopir$idx' value='".$data_biaya['BiayaSopir']."'><input type='hidden' name='coa_sopir$idx' value='".$data_biaya['KodeAkunBiayaSopir']."'></td></tr>";


    $output .="<table>";

    echo($output);

    exit;

  case "add":
    // add

    $pesan = $HTTP_GET_VARS['pesan'];

    if($pesan==1){
      $pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
      $bgcolor_pesan="98e46f";
    }elseif($pesan==2){
      $pesan="<font color='#f5f5f5' size=3>Gagal Menambahkan Data!</font>";
      $bgcolor_pesan="red";
    }

    $template->set_filenames(array('body' => 'tilang/add_body.tpl'));

    $template->assign_vars(array(
      'BCRUMP'		=>setBcrump($id_page),
      'JUDUL'			=> 'Tambah Surat Tilang',
      'MODE'   		=> 'save',
      'SUB'    		=> '0',
      'TGL_SJ'		=> dateD_M_Y(),
      'OPT_POOL'	=> $SuratJalan->setComboPool(0),
      'OPT_KENDARAAN'=> $SuratJalan->setComboMobil(""),
      'OPT_SOPIR'	=> $SuratJalan->setComboSopir(""),
      'PESAN'						=> $pesan,
      'BGCOLOR_PESAN'		=> $bgcolor_pesan,
      'U_JURUSAN_ADD_ACT'	=> append_sid('tilang.'.$phpEx)
      )
    );

    break;

  case "edit":
    // add

    $pesan = $HTTP_GET_VARS['pesan'];

    if($pesan==1){
      $pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
      $bgcolor_pesan="98e46f";
    }elseif($pesan==2){
      $pesan="<font color='#f5f5f5' size=3>Gagal Menambahkan Data!</font>";
      $bgcolor_pesan="red";
    }

    $id = $HTTP_GET_VARS['id'];

    $sql = "SELECT *
      FROM tbl_tilang
      WHERE Id=$id";

    $result = $db->sql_query($sql);
    $row=$db->sql_fetchrow($result);

    $template->set_filenames(array('body' => 'tilang/add_body.tpl'));

    $template->assign_vars(array(
      'BCRUMP'		=>setBcrump($id_page),
      'JUDUL'			=> 'Edit Surat Tilang',
      'MODE'   		=> 'save',
      'SUB'    		=> '1',
      'ID'            => $row['Id'],
      'NOST'          => $row['NoST'],
      'TGL_ST'		=> dateD_M_Y($row['TanggalTilang']),
      'TGL_JT'		=> dateD_M_Y($row['JatuhTempo']),
      'OPT_KENDARAAN'=> $SuratJalan->setComboMobil($row['KodeKendaraan']),
      'OPT_SOPIR'	=> $SuratJalan->setComboSopir($row['KodeSopir']),
      'PESAN'						=> $pesan,
      'BGCOLOR_PESAN'		=> $bgcolor_pesan,
      'DENDA'             => $row['Denda'],
      'U_JURUSAN_ADD_ACT'	=> append_sid('tilang.'.$phpEx)
      )
    );

    break;

  case "save":
    // aksi membuat surat jalan

    $judul="Tambah Surat Tilang";
    $path	='<a href="'.append_sid('tilang.'.$phpEx."?mode=add").'">Tambah Surat Tilang</a> ';

    $nost              = $HTTP_POST_VARS['nost'];
    $tgl_st				= FormatTglToMySQLDate($_POST['tgl_st']);
    $tgl_jt				= FormatTglToMySQLDate($_POST['tgl_jt']);
    $nama_petugas	= $userdata['nama'];
    $kendaraan		= explode("|",$_POST['kendaraan']);
    $no_body			= $kendaraan[0];
    $no_polisi		= $kendaraan[1];
    $sopir				= explode("|",$_POST['sopir']);
    $kode_sopir		= $sopir[0];
    $nama_sopir		= $sopir[1];
    $denda          = $_POST['denda'];
    if($nost != null && $tgl_st != null && $tgl_jt != null && $kendaraan != null && $sopir != null){
      if($submode == 0){
        $sql	=
          "INSERT INTO tbl_tilang(
				    NoST,KodeSopir,NamaSopir,
				    KodeKendaraan,NoPolisi,TanggalTilang,
				    JatuhTempo,NamaPetugas,Denda)
			    VALUES(
				    '$nost','$kode_sopir','$nama_sopir',
				  '$no_body','$no_polisi','$tgl_st',
				  '$tgl_jt','$nama_petugas','$denda')";
      }elseif($submode == 1){
        $id              = $HTTP_POST_VARS['Id'];
        $sql =
          "UPDATE tbl_tilang
          SET NoST='$nost',KodeSopir='$kode_sopir',NamaSopir='$nama_sopir',
            KodeKendaraan='$no_body',NoPolisi='$no_polisi',TanggalTilang='$tgl_st',
				    JatuhTempo='$tgl_jt',NamaPetugas='$nama_petugas',Denda='$denda'
          WHERE Id=$id";
      }

      if (!$db->sql_query($sql)){
        die_error("Kesalahan mengeksekusi query");
      }else{
        //ambil id yang terbaru dimasukan

      $sql = "SELECT MAX(Id) AS LASTID FROM tbl_tilang";

        if (!$result = $db->sql_query($sql)){
          die_error("Kesalahan mengeksekusi query");
        }else{
          $row = $db->sql_fetchrow($result);
          $id  = $row['LASTID'];
        }
      }

    }else{
      redirect(append_sid('tilang.'.$phpEx.'?mode=edit&pesan=2&id='.$id,true));
    }

    redirect(append_sid('tilang.'.$phpEx.'?mode=edit&pesan=1&id='.$id,true));
    exit;

  case "batal":
    $no_sj	= $_GET['no_sj'];
    $SuratJalan->batalkanSuratJalan($no_sj,$userdata['user_id'],$userdata['nama']);
    exit;

  case "delete":
    global $db;
    $list_page = str_replace("\\'","'",$HTTP_GET_VARS['list_page']);

    $sql =
      "DELETE FROM tbl_tilang
      WHERE Id IN($list_page);";

    if ($db->sql_query($sql)){
      echo 1;
    }else{
      echo 2;
    }
    exit;
}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>