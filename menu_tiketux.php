<?php
// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
//var_dump($userdata);exit;
$id_page = 500;
$userdata = session_pagestart($user_ip,$id_page);
//init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//INIT

$page_title	= "Menu Operasional";

include($adp_root_path . 'includes/page_header.php');

setHalamanMenu($id_page);

include($adp_root_path . 'alert.adminmobil.php');
include($adp_root_path . 'includes/page_tail.php');
?>