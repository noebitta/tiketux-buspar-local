<?php
// HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 412;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassVoucherPaket.php');
include($adp_root_path . 'ClassUser.php');

// PARAMETER
$mode 			= getVariabel('mode');
$fil_status = getVariabel('filterstatus');
$cari       = getVariabel('cari');
$idx_page   = getVariabel('idxpage');
$order_by   = getVariabel('orderby');
$sort       = getVariabel('sort');

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

$VoucherPaket      = new VoucherPaket();

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    exit;

  case 1:
    //TAMBAH DATA
    showInputData();

    exit;

  case 2:
    //SHOW DETAIL DATA
    $kode_voucher = getVariabel('kodevoucher');

    showDetailData($kode_voucher);

    exit;

  case 3:
    //SIMPAN DATA
    $kode_voucher             = getVariabel("kodevoucher");
    $kota_berlaku             = getVariabel('kotaberlaku');
    $cabang_berangkat_berlaku = getVariabel('cabangasal');
    $cabang_tujuan_berlaku    = getVariabel('cabangtujuan');
    $berlaku_mulai            = getVariabel('mulaiberlaku');
    $expired_date             = getVariabel('expireddate');
    $nilai_voucher            = getVariabel('nilaivoucher');
    $is_harga_tetap           = getVariabel('ishargatetap');
    $is_boleh_weekend         = getVariabel('isbolehweekend');
    $jumlah_cetak             = getVariabel('jumlahcetak');
    $keterangan               = getVariabel('keterangan');

    echo(prosesSimpan($kode_voucher,$kota_berlaku,$cabang_berangkat_berlaku,$cabang_tujuan_berlaku,$berlaku_mulai,$expired_date,$nilai_voucher,$is_harga_tetap,$is_boleh_weekend,$jumlah_cetak,$keterangan));

    exit;

  case 4:
    //SHOW DIALOG BATALKAN VOUCHER
    $kode_voucher = getVariabel('kodevoucher');

    showDialogBatal($kode_voucher);

    break;

  case 5:
    //SET COMBO CABANG ASAL
    $kota        = getVariabel('kota');

    echo(setListCabangAsal($kota));
    break;

  case 6:
    //SET COMBO CABANG TUJUAN
    $cabang_asal        = getVariabel('cabangasal');

    echo(setListCabangTujuan($cabang_asal));
    break;

  case 7:
    //BATALKAN VOUCHER
    $kode_voucher = getVariabel('kodevoucher');
    $alasan       = getVariabel('alasan');

    echo(batalkanVoucher($kode_voucher,$alasan));

    break;

  case 8:
    //HAPUS DATA
    $kode_voucher = getVariabel('kodevoucher');

    echo(hapusVoucher($kode_voucher));

    break;

}

//METHODES & PROCESS ========================================================================================================================================================================

function showData(){

  global $db,$fil_status,$cari,$order_by,$sort,$idx_page,$template,$Permission,$VoucherPaket,$userdata,$id_page,$VIEW_PER_PAGE;

  $template->set_filenames(array('body' => 'voucher.paket/index.tpl'));


  $template->assign_block_vars("CRUD_ADD",array());
  $template->assign_block_vars("CRUD_BATAL",array());

  if($Permission->isPermitted($userdata["user_level"],$id_page.".1")){
    $template->assign_block_vars("CRUD_DEL",array());
  }

  switch($fil_status){
    case "": $filter="";break;
    case 0: $filter=" NoResiDigunakan='' AND IsBatal!=1";break;
    case 1: $filter=" NoResiDigunakan!='' AND IsBatal!=1";break;
    case 2: $filter=" IsBatal=1";break;
  }

  $result = $VoucherPaket->ambilData($order_by,$sort,$idx_page,$cari,$filter);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  //GET DATA USER
  $User   = new User();

  $res_user = $User->ambilData("","nama","ASC");

  $data_user  = array();

  while($row_user=$db->sql_fetchrow($res_user)){
    $data_user[$row_user["user_id"]]=$row_user["nama"];
  }

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd =($no%2)==0?"even":"odd";

    if(!$row["IsBatal"]) {
      $batal_oleh       = "";
      $waktu_batal       = "";
      $alasan_batal       = "";
    }
    else{
      $batal_oleh       = $data_user[$row["DibatalkanOleh"]];
      $waktu_batal      = dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuPembatalan"]));
      $alasan_batal     = $row["AlasanPembatalan"];
      $odd              = "red";
    }

    $template->assign_block_vars(
      'ROW',array(
        'odd'                 => $odd,
        'idx'                 => $no,
        'no'                  => $idx_page*$VIEW_PER_PAGE+$no,
        'kodevoucher'         => $row["KodeVoucher"],
        'jenis'               => $row["IsHargaTetap"]?"Harga Tetap":"Diskon",
        'nilaivoucher'        => $row["NilaiVoucher"]>1?/*Rupiah*/number_format($row["NilaiVoucher"]):/*persen*/($row["NilaiVoucher"]*100)."%",
        'keterangan'          => $row["Keterangan"],
        'hariberlaku'         => $row["IsBolehWeekEnd"]?/*boleh semua hari*/"semua hari":/*hanya weekday*/"week day saja",
        'kotaberlaku'         => $row["KotaBerlaku"]==""?"semua kota":$row["KotaBerlaku"],
        'cabangasalberlaku'   => $row["CabangBerangkatBerlaku"]==""?"semua keberangkatan":$row["CabangBerangkatBerlaku"],
        'cabangtujuanberlaku' => $row["CabangTujuanBerlaku"]==""?"semua tujuan":$row["CabangTujuanBerlaku"],
        'berlakumulai'        => dateparse(FormatMySQLDateToTgl($row["BerlakuMulai"])),
        'expired'             => dateparse(FormatMySQLDateToTgl($row["ExpiredDate"])),
        'dicetakoleh'         => $data_user[$row["PetugasPencetak"]],
        'waktucetak'          => dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuCetak"])),
        'tglberangkat'        => ($row["TglBerangkatDigunakan"]==""?"":dateparse(FormatMySQLDateToTgl($row["TglBerangkatDigunakan"]))),
        'jadwalberangkat'     => $row["KodeJadwalDigunakan"],
        'noresi'              => $row["NoResiDigunakan"],
        'digunakanpada'       => ($row["WaktuDigunakan"]==""?"":dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuDigunakan"]))),
        'csopengguna'         => $data_user[$row["PetugasPengguna"]],
        'dibatalkanoleh'      => $batal_oleh,
        'waktubatal'          => $waktu_batal,
        'alasan'              => $alasan_batal
      )
    );

    //ACTION
    if(!$row["IsBatal"]){
      $template->assign_block_vars("ROW.ACT_EDIT", array());

      if($row["NoResiDigunakan"]=="") {
        $template->assign_block_vars("ROW.ACT_BATAL", array());
      }
    }

    if($Permission->isPermitted($userdata["user_level"],$id_page.".1") && $row["NoResiDigunakan"]==""){
      $template->assign_block_vars("ROW.ACT_DEL",array());
    }

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	=> basename(__FILE__),
      "FILSTS$fil_status" => "selected",
      'CARI'      => $cari,
      'ORDER'     => $order_by,
      'SORT'      => $sort,
      'IDX_PAGE'  => $idx_page,
      'PAGING'    => $paging
    )
  );

  $template->pparse('body');

}

function showInputData(){
  global $template,$db,$VoucherPaket;

  $template->set_filenames(array('body' => 'voucher.paket/edit.tpl'));

  $masa_berlaku = 60; //hari
  $date_expired = new DateTime(date("Y-m-d"));
  $date_expired->add(new DateInterval("P".$masa_berlaku."D"));

  $template->assign_vars(array(
      "OPERATION"	        => "tambah",
      "TGL_MULAI_BERLAKU" => date("d-m-Y"),
      "EXPIRED_DATE"      => $date_expired->format("d-m-Y")
    )
  );

  //MENGAMBIL KOTA
  $result = $VoucherPaket->getListKota();

  while($row=$db->sql_fetchrow($result)){
    $template->assign_block_vars("OPT_KOTA_BERLAKU",array(
      "value" => $row["NamaKota"],
      "text"  => $row["NamaKota"]
    ));
  }

  //MENAMPILKAN INPUT JUMLAH CETAK VOUCHER
  $template->assign_block_vars("SHOW_JUMLAH_CETAK",array());

  //MENAMPILKAN DEFAULT CABANG ASAL DAN TUJUAN
  $template->assign_block_vars("CABANG_BERANGKAT_DEFAULT",array());
  $template->assign_block_vars("CABANG_TUJUAN_DEFAULT",array());

  $template->pparse('body');
}

function showDetailData($kode_voucher){
  global $db,$template,$VoucherPaket;

  $template->set_filenames(array('body' => 'voucher.paket/edit.tpl'));

  $data = $VoucherPaket->getDataVoucher($kode_voucher);

  $template->assign_block_vars("SHOW_KODE_VOUCHER",array());

  //MENGAMBIL KOTA
  $result = $VoucherPaket->getListKota();

  while($row=$db->sql_fetchrow($result)){
    $template->assign_block_vars("OPT_KOTA_BERLAKU",array(
      "value"     => $row["NamaKota"],
      "text"      => $row["NamaKota"],
      "selected"  => ($row["NamaKota"]!=$data["KotaBerlaku"]?"":"selected")
    ));
  }

  $template->assign_vars(array(
    "OPERATION"	        => "Ubah data",
    "KODE_VOUCHER"      => $data["KodeVoucher"],
    "LIST_CABANG_ASAL"  => setListCabangAsal($data["KotaBerlaku"],$data["CabangBerangkatBerlaku"]),
    "LIST_CABANG_TUJUAN"=> setListCabangTujuan($data["CabangBerangkatBerlaku"],$data["CabangTujuanBerlaku"]),
    "TGL_MULAI_BERLAKU" => FormatMySQLDateToTgl($data["BerlakuMulai"]),
    "EXPIRED_DATE"      => FormatMySQLDateToTgl($data["ExpiredDate"]),
    "IS_HARGA_TETAP".$data["IsHargaTetap"]        => "selected",
    "IS_PERSEN".($data["NilaiVoucher"]>1?"0":"1") => "selected",
    "IS_BOLEH_WEEKEND".$data["IsBolehWeekEnd"]    => "selected",
    "NILAI_VOUCHER"     => $data["NilaiVoucher"],
    "KETERANGAN"        => $data["Keterangan"]
  ));

  $template->pparse('body');
}
function prosesSimpan($kode_voucher,$kota_berlaku,$cabang_berangkat_berlaku,
                      $cabang_tujuan_berlaku,$berlaku_mulai,$expired_date,
                      $nilai_voucher,$is_harga_tetap,$is_boleh_weekend,
                      $jumlah_cetak,$keterangan){

  global $VoucherPaket,$userdata;

  //cek duplikasi

  if($kode_voucher==""){
    //proses tambah
    for($i=0;$i<$jumlah_cetak;$i++) {
      $kode_voucher = $VoucherPaket->setVoucher($kota_berlaku, $cabang_berangkat_berlaku,
        $cabang_tujuan_berlaku, $userdata["user_id"], FormatTglToMySQLDate($berlaku_mulai),
        FormatTglToMySQLDate($expired_date), $nilai_voucher, $is_harga_tetap, $is_boleh_weekend,
        $keterangan);
    }
  }
  else{
    //proses ubah
    $VoucherPaket->ubahVoucher($kode_voucher,$kota_berlaku,$cabang_berangkat_berlaku,
      $cabang_tujuan_berlaku,FormatTglToMySQLDate($berlaku_mulai),FormatTglToMySQLDate($expired_date),
      $nilai_voucher,$is_harga_tetap,$is_boleh_weekend,
      $keterangan);
  }

  $return = array("status" => "OK","kodevoucher"=>$kode_voucher);

  return json_encode($return);
}

function showDialogBatal($kode_voucher){
  global $template;

  $template->set_filenames(array('body' => 'voucher.paket/dialogbatal.tpl'));

  $template->assign_vars(array(
    "OPERATION"	        => "Pembatalan",
    "KODE_VOUCHER"      => str_replace("\\'","",$kode_voucher)
  ));

  $template->pparse('body');
}

function hapusVoucher($kode_voucher){
  global $VoucherPaket;

  $VoucherPaket->hapus($kode_voucher);

  $return = array("status" => "OK");

  return json_encode($return);
}

function batalkanVoucher($kode_voucher,$alasan_batal){
  global $VoucherPaket,$userdata;

  $VoucherPaket->batal($kode_voucher,$userdata["user_id"],$alasan_batal);

  $return = array("status" => "OK");

  return json_encode($return);
}

function setListCabangAsal($kota,$cabang_asal=""){
  global $VoucherPaket,$db;

  $result = $VoucherPaket->getListCabang($kota);

  $str_return = "<option value=''>-semua cabang-</option>";

  while($row=$db->sql_fetchrow($result)){
    $str_return .="<option value='$row[KodeCabang]' ".($row["KodeCabang"]==$cabang_asal?"selected":"").">$row[NamaCabang]</option>";
  }

  return $str_return;
}

function setListCabangTujuan($cabang_asal,$cabang_tujuan=""){
  global $VoucherPaket,$db;

  $result = $VoucherPaket->getListCabang("",$cabang_asal);

  $str_return = "<option value=''>-semua cabang-</option>";

  while($row=$db->sql_fetchrow($result)){
    $str_return .="<option value='$row[KodeCabang]' ".($row["KodeCabang"]==$cabang_tujuan?"selected":"").">$row[NamaCabang]</option>";
  }

  return $str_return;
}
?>