<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script src="{TPL}js/jquery.min.js" type="text/javascript"></script>
<script src="{TPL}js/chartphp.js" type="text/javascript"></script>
<script src="{TPL}js/chartphp.css" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
 <td class="whiter" valign="middle" align="center">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul" width="450">&nbsp;General Total</td>
				<td align='left' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal' colspan=2>&nbsp;Tanggal:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp;s/d:&nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal' colspan=2>&nbsp;Tanggal Acuan:&nbsp;<select name="tgl_acuan" id="tgl_acuan"><option value="0" {SELECT1}>Tanggal Berangkat</option><option value="1" {SELECT2}>Cetak Tiket</option></select></td>
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
	</form>
	</br>
	<!--BODY-->
	<table width="90%">
		<tbody>
		<!--
		<tr>
			<td colspan="2" align="center"><a href="{SCRIPT}" target="_self"><img src="{TPL}images/icon_grafik.png">&nbsp;&nbsp;<font size="3">GRAFIK TARGET CABANG</font></a></td>
		</tr>
		-->
		<tr>
			<td width="40%">
				<table>
					<tbody>
					<tr class="generaltotalrow"><td class="generaltotalkolom">Tanggal</td><td class="generaltotaldata"><b>{TANGGAL}</b></td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">open trip</td><td class="generaltotaldata">{OPEN_TRIP}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">trip isi</td><td class="generaltotaldata">{TRIP_ISI}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">booking</td><td class="generaltotaldata">{BOOKING}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">cancel/noshow</td><td class="generaltotaldata">{CANCEL}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">unit aktif</td><td class="generaltotaldata">{UNIT_AKTIF}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">unit tidak aktif</td><td class="generaltotaldata">{UNIT_NONAKTIF}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">supir aktif</td><td class="generaltotaldata">{SUPIR_AKTIF}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">supir tidak aktif</td><td class="generaltotaldata">{SUPIR_NONAKTIF}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">member aktif</td><td class="generaltotaldata">{MEMBER_AKTIF}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">member nonaktif</td><td class="generaltotaldata">{MEMBER_NONAKTIF}</td></tr>
					<tr><td colspan="2"><hr></td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">penumpang jakarta</td><td class="generaltotaldata">{PNP_JKT}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">penumpang bandung</td><td class="generaltotaldata">{PNP_BDG}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">pnp.online jakarta</td><td class="generaltotaldata">{PNP_OL_JKT}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">pnp.online bandung</td><td class="generaltotaldata">{PNP_OL_BDG}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">paket jakarta</td><td class="generaltotaldata">{PKT_JKT}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">paket bandung</td><td class="generaltotaldata">{PKT_BDG}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">omset pnp.jakarta</td><td class="generaltotaldata">Rp. {OMZ_PNP_JKT}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">omset pnp.bandung</td><td class="generaltotaldata">Rp. {OMZ_PNP_BDG}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">omset pnp.online jakarta</td><td class="generaltotaldata">Rp. {OMZ_OL_JKT}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">omset pnp.online bandung</td><td class="generaltotaldata">Rp. {OMZ_OL_BDG}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">omset paket jakarta</td><td class="generaltotaldata">Rp. {OMZ_PKT_JKT}</td></tr>
					<tr class="generaltotalrow"><td class="generaltotalkolom">omset paket bandung</td><td class="generaltotaldata">Rp. {OMZ_PKT_BDG}</td></tr>
					</tbody>
				</table>
			</td>
			<td width="5%"></td>
			<td width="45%" valign="top">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tbody>
					<tr class="generaltotalrow">
						<td class="generaltotalkolom"><div align="center">Cabang</div></td>
						<td class="generaltotalkolom"><div align="center">Target Tiket</div></td>
						<td class="generaltotalkolom"><div align="center">Real Tiket</div></td>
						<td class="generaltotalkolom"><div align="center">Capaian Paket</div></td>
						<td class="generaltotalkolom"><div align="center">Target Paket</div></td>
						<td class="generaltotalkolom"><div align="center">Real Paket</div></td>
						<td class="generaltotalkolom"><div align="center">Capaian Paket</div></td>
						<td class="generaltotalkolom"><div align="center">Action</div></td>
					</tr>
					<!-- BEGIN ROW -->
					<tr class="generaltotalrow">
						<td class="generaltotalkolom">{ROW.Cabang}</td>
						<td class="generaltotaldata"><div  align="center">{ROW.targetpax}</div></td>
						<td class="generaltotaldata"><div  align="center">{ROW.realpax}</div></td>
						<td class="generaltotaldata"><div  align="center">{ROW.capaianpax} %</div></td>
						<td class="generaltotaldata"><div  align="center">{ROW.targetpaket}</div></td>
						<td class="generaltotaldata"><div  align="center">{ROW.realpaket}</div></td>
						<td class="generaltotaldata"><div  align="center">{ROW.capaianpaket} %</div></td>
						<td class="generaltotaldata"><div  align="center"><a href="{ROW.SCRIPT}">Grafik</a></div></td>
					</tr>
					<!-- END ROW -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
 </td>
</tr>
</table>