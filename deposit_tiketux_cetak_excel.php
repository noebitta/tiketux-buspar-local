<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 502;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];

$fil_status 		= $HTTP_GET_VARS['status'];
$tanggal_mulai  = $HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = $HTTP_GET_VARS['tanggal_akhir'];
$cari						= $HTTP_GET_VARS["cari"];

$kondisi_cari	= $cari==""?"":" AND (KodeReferensi LIKE '%$cari%')";
	
if($filstatus!=""){
	switch($filstatus){
		case "0"	:
			//Belum diverifikasi
			$kondisi_status	= " AND IsVerified=0 ";
			break;
		case "1"	:
			//sudah diverifikasi
			$kondisi_status	= " AND IsVerified=1 ";
			break;
	}
}

$kondisi_tanggal	= " AND (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai' AND '$tanggal_akhir')";
 

//QUERY
	$sql	= 
		"SELECT *,
			f_user_get_nama_by_userid(PetugasTopUp) AS NamaPetugasTopUp,
			f_user_get_nama_by_userid(PetugasVerifikasi) AS NamaPetugasVerifikasi
		FROM tbl_deposit_log_topup
		WHERE 1 $kondisi_cari $kondisi_tanggal $kondisi_status
		ORDER BY ID";
	
if ($result = $db->sql_query($sql)){
		
	$i=1;
	
	$objPHPExcel = new PHPExcel();          
  $objPHPExcel->setActiveSheetIndex(0);  
  $objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
  $objPHPExcel->getActiveSheet()->mergeCells('A2:M2');
  
	//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Top Up Deposit Tiketux per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Filter Status: '.$status);
	$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Waktu Transaksi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('C4', '#Referensi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Jumlah');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('E4', 'User');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Status');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Verifikator');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Waktu Verifikasi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$idx=0;
	
	while ($row = $db->sql_fetchrow($result)){
		$idx++;
		$idx_row=$idx+4;
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])));
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeReferensi']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Jumlah']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NamaPetugasTopUp']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['IsVerified']==1?"VERIFIED":"NOT VERIFIED");
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NamaPetugasVerifikasi']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuVerifikasi'])));
		
	}
		
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
  
	if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Top Up Deposit Tiketux per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output'); 
	}
}
else{
	die_error('Err:',__LINE__);
}   


?>
