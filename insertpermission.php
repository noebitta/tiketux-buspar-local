<?php
    define('FRAMEWORK', true);
    $adp_root_path = './';
    include($adp_root_path . 'common.php');
    global $db;
    global $USER_LEVEL_INDEX;
    global $LEVEL_ADMIN;
	global $LEVEL_MANAJEMEN;
	global $LEVEL_MANAJER;
	global $LEVEL_SUPERVISOR;
	global $LEVEL_SUPERVISOR_OPS;
	global $LEVEL_CSO;
	global $LEVEL_CSO_PAKET;
	global $LEVEL_SCHEDULER;
	global $LEVEL_KASIR;
	global $LEVEL_KEUANGAN;
	global $LEVEL_CCARE;

    /*$page = [
        700 => 'Menu Pengaturan',
        701 => 'Cabang',
        702 => 'Jurusan',
        703 => 'Jadwal',
        704 => 'Pengaturan Umum',
        705 => 'Sopir',
        706 => 'Mobil',
        707 => 'Daftar Komputer',
        708 => 'User',
        709 => 'Harga Dinamis',
        710 => 'Target CSO',
        711 => 'Target Counter',
        712 => 'COA',
    ];

    foreach($page as $k=>$p){
        $sql = "INSERT INTO tbl_page(page_id,nama_page)
            VALUES ($k,'$p')";

        $db->sql_query($sql);
    }*/
    function insert($thispage_id,$HaveAccess){
        global $db;

        foreach($HaveAccess as $access){
            $sql = "INSERT INTO tbl_permissions(page_id,user_level)
                            VALUES ($thispage_id,$access)";
            
            $db->sql_query($sql);
        }
    }

    /*insert('300',array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["KEUANGAN"]));
    insert('400',array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SUPERVISOR"],$USER_LEVEL_INDEX["KEUANGAN"]));
    insert('500',array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]));
    insert('600',array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SUPERVISOR'],$USER_LEVEL_INDEX["CUSTOMER_CARE"]));
    insert('700',array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"]));*/

    /*for($thispage_id=201;$thispage_id<216;$thispage_id++) {
        if ($thispage_id == 201) {
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 202){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 202){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 203){
            $HaveAccess = [
                $USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["SCHEDULER"]
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 204){
            $HaveAccess = [
                $USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 205){
            $HaveAccess = [

            ];
        }elseif($thispage_id == 206){
            $HaveAccess = [
                $USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 207){
            $HaveAccess = [
                $LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 208){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 209){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 210){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 211){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 212){
            $HaveAccess = [
                $USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX['KEUANGAN']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 213){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 214){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']
            ];

            insert($thispage_id,$HaveAccess);
        }elseif($thispage_id == 215){
            $HaveAccess = [
                $USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN']
            ];

            insert($thispage_id,$HaveAccess);
        }
    }

    for($thispage_id=301;$thispage_id<=311;$thispage_id++) {
        if($thispage_id == 301){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 302){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 303){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SUPERVISOR"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 304){

        }elseif($thispage_id == 305){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 306){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SUPERVISOR"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 307){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SUPERVISOR"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 308){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN));

        }elseif($thispage_id == 309){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]));

        }elseif($thispage_id == 310){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 311){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }
    }

    for($thispage_id=401;$thispage_id<=409;$thispage_id++) {
        if($thispage_id == 401){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN));

        }elseif($thispage_id == 402){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN,$LEVEL_SUPERVISOR));

        }elseif($thispage_id == 403){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN));

        }elseif($thispage_id == 404){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 405){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 406){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 407){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN));

        }elseif($thispage_id == 408){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 409){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }
    }

    for($thispage_id=501;$thispage_id<=503;$thispage_id++) {
        if($thispage_id == 501){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 502){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 503){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }
    }

    for($thispage_id=601;$thispage_id<=605;$thispage_id++) {
        if($thispage_id == 601){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['SUPERVISOR'],$USER_LEVEL_INDEX['CUSTOMER_CARE']));

        }elseif($thispage_id == 602){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE));

        }elseif($thispage_id == 603){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE));

        }elseif($thispage_id == 604){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE));

        }elseif($thispage_id == 605){
            insert($thispage_id,array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_CCARE));

        }
    }

    for($thispage_id=701;$thispage_id<=713;$thispage_id++) {
        if($thispage_id == 701){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['SPV_OPERASIONAL']));

        }elseif($thispage_id == 702){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_OPERASIONAL']));

        }elseif($thispage_id == 703){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['SPV_OPERASIONAL']));

        }elseif($thispage_id == 704){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"]));

        }elseif($thispage_id == 705){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_OPERASIONAL']));

        }elseif($thispage_id == 706){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX["MEKANIK"]));

        }elseif($thispage_id == 707){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]));

        }elseif($thispage_id == 708){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"]));

        }elseif($thispage_id == 709){


        }elseif($thispage_id == 710){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]));

        }elseif($thispage_id == 711){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]));

        }elseif($thispage_id == 712){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]));

        }elseif($thispage_id == 713){
            insert($thispage_id,array($USER_LEVEL_INDEX["ADMIN"]));

        }
    }*/