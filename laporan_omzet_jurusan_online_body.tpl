<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }
    dojo.addOnLoad(init);

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr class='banner' height=40>
                        <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penjualan Online Jurusan </td>
                        <td align='right' valign='middle'>
                            <table>
                                <tr><td class='bannernormal'>
                                        Berdasarkan : <select id="berdasarkan" name="berdasarkan"> <!-- <option value="1">Tgl.Berangkat</option>--> <option value="2">Tgl.Pembelian</option></select>
                                        &nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                                        &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                                        &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                                        <input type="submit" value="cari" />&nbsp;
                                    </td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=2 align='center' valign='middle'>
                            <table>
                                <tr>
                                    <td>
                                        <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=2>
                            <table width='100%'>

                                <td align='right' valign='bottom'>
                                    {PAGING}
                                </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- END HEADER-->
                <table class="border" width='100%' >
                    <tr>
                        <th width=30 >No</th>
                        <th >Jurusan</th>
                        <th >Cabang Asal</th>
                        <th >Cabang Tujuan</th>
                        <th >Total Penumpang</th>
                        <th >Total Penjualan Tiket</th>
                        <th >Total Setor</th>
                    </tr>
                    <!-- BEGIN ROW -->
                    <tr class="{ROW.odd}">
                        <td ><div align="right">{ROW.no}</div></td>
                        <td ><div align="left">{ROW.jurusan}</div></td>
                        <td ><div align="left">{ROW.cabangasal}</div></td>
                        <td ><div align="left">{ROW.cabangtujuan}</div></td>
                        <td style="background: yellow"><div align="right" >{ROW.total_penumpang}</div></td>
                        <td ><div align="right" >{ROW.total_penjualan_tiket}</div></td>
                        <td style="background: yellow;"><div align="right" >{ROW.total_setor}</div></td>
                    </tr>
                    <!-- END ROW -->
                </table>
                <table width='100%'>
                    <tr>
                        <td align='right' width='100%'>
                            {PAGING}
                        </td>
                    </tr>

                    <tr>
                        <td align='left' valign='bottom' colspan=3>
                            <!-- <table>
                                <tr><td>B</td><td>=</td><td>Penumpang Booking</td></tr>
                                <tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
                                <tr><td>MH</td><td>=</td><td>Mahasiswa</td></tr>
                                <tr><td>M</td><td>=</td><td>Member</td></tr>
                                <!-- <tr><td>S</td><td>=</td><td>Special Ticket Staff</td></tr>
                                <tr><td>G</td><td>=</td><td>Tiket Gratis</td></tr>
                                <tr><td>PP</td><td>=</td><td>Promo PP</td></tr>
                                <tr><td>Or</td><td>=</td><td>Online Reguler</td></tr>
                                <tr><td>Om</td><td>=</td><td>Online Mahasiswa</td></tr>
                                <tr><td>Opp</td><td>=</td><td>Online Promo PP</td></tr>
                                <tr><td>O</td><td>=</td><td>Total Online</td></tr>
                                <tr><td>T</td><td>=</td><td>Total</td></tr>
                            </table>-->
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>