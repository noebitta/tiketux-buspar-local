<?php
//
// SPJ
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassBiayaOperasional.php');


include($adp_root_path . 'phpqrcode/BarcodeQR.php');
/// set BarcodeQR object
$qr = new BarcodeQR();

//SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tgl_berangkat		= $HTTP_GET_VARS['tgl_berangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal'];
$sopir_sekarang		= $HTTP_GET_VARS['sopir_sekarang'];
$mobil_sekarang		= $HTTP_GET_VARS['mobil_sekarang'];
$aksi							= $HTTP_GET_VARS['aksi'];

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	$Sopir = new Sopir();
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	$opt_sopir="<option value=''>- silahkan pilih sopir  -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
}

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;
	$Mobil = new Mobil();
			
	$result=$Mobil->ambilDataForComboBox();
	$opt_mobil="<option value=''>- silahkan pilih kendaraan  -</option>";
	
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[Merek] $row[Jenis]</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	
	return $opt_mobil;
	//END SET COMBO MOBIL
}

if($aksi==0){
	$Jadwal						= new Jadwal();
	$BiayaOperasional	= new BiayaOperasional();
	$no_spj						= $HTTP_GET_VARS['no_spj'];
	
	//load data
	$str_option_sopir=setComboSopir($sopir_sekarang);
	$str_option_mobil=setComboMobil($mobil_sekarang);
	
	#mengambil data jadwal
	$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
	if($data_jadwal['FlagSubJadwal']!=1){
		$kode_jadwal_utama	= $kode_jadwal;
	}
	else{
		$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
	}
	
	$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);
	
	
	/*if($no_spj==""){
		//BELUM CETAK SPJ
		$biaya_bbm_value	= $data_biaya['BiayaBBM'];
		
		if($data_biaya['IsVoucherBBM']!=1){
			//$show_biaya_bbm				="<input style='text-align: right' type='text' onBlur='hitungTotalBiayaOperasional();' name='biaya_bbm' id='biaya_bbm' onkeypress='validasiAngka2(event);' value='$data_biaya[BiayaBBM]' />";
			$show_biaya_bbm					= number_format($biaya_bbm_value,0,",",".")."<input type='hidden' name='biaya_bbm' id='biaya_bbm' value='$biaya_bbm_value' />";
			$keterangan_voucher_bbm = "";
			$total_biaya	= $biaya_bbm_value;
			$biaya_bbm_color	= "black";
		}
		else{
			$show_biaya_bbm				= number_format($biaya_bbm_value,0,",",".")."<input type='hidden' name='biaya_bbm' id='biaya_bbm' value='0' />";
			$keterangan_voucher_bbm	= "<tr><td class='noticeMessage' align='center' colspan='3'>----| VOUCHER BBM |----</td></tr>";
			$total_biaya	= 0;
			$biaya_bbm_color	= "red";
		}
		
	}
	else{
		//SUDAH CETAK SPJ		
		if($data_biaya['IsVoucherBBM']!=1){
			$biaya_bbm_value				= $BiayaOperasional->ambilBiayaBBMByNoSPJ($no_spj)*1;
			$show_biaya_bbm					= number_format($biaya_bbm_value,0,",",".")."<input type='hidden' name='biaya_bbm' id='biaya_bbm' value='$biaya_bbm_value' />";
			$keterangan_voucher_bbm	= "";
			$total_biaya						= $biaya_bbm_value;
			$biaya_bbm_color				= "black";
		}
		else{
			$biaya_bbm_value				= $BiayaOperasional->ambilBiayaVoucherBBMByNoSPJ($no_spj)*1;
			$show_biaya_bbm					= number_format($biaya_bbm_value,0,",",".")."<input type='hidden' name='biaya_bbm' id='biaya_bbm' value='0' />";
			$keterangan_voucher_bbm	= "<tr><td class='noticeMessage' align='center' colspan='3'>----| VOUCHER BBM |----</td></tr>";
			$total_biaya						= 0;
			$biaya_bbm_color				= "red";
		}
	}*/
	
	$biaya_sopir			= $data_biaya['BiayaSopir'];
	$biaya_tol				= $data_biaya['BiayaTol'];
	//$biaya_parkir			= $data_biaya['BiayaParkir'];
	
	if($data_biaya['IsBiayaSopirKumulatif']!=1){
		$keterangan_bayar_biaya_sopir = "<tr><td class='noticeMessage' align='center' colspan='3'>----| Biaya Sopir dibayarkan pada saat pencetakkan SPJ |----</td></tr>";
		$total_biaya	+= $biaya_sopir+$biaya_tol+$biaya_parkir;
		$biaya_sopir_color	= "black";
		$biaya_sopir_value	= $biaya_sopir;
	}
	else{
		$keterangan_bayar_biaya_sopir	= "<tr><td class='noticeMessage' align='center' colspan='3'>----| Biaya sopir diakumulasikan |----</td></tr>";
		$total_biaya	+= $biaya_tol+$biaya_parkir;
		$biaya_sopir_color	= "red";
		$biaya_sopir_value	= 0;
	}
	
	/*
	echo("
		<table bgcolor='white' width='100%'>
		  <tr>
				<td>Silahkan pilih mobil</td>
				<td  width='1'>:</td>
				<td>
					<select name='list_mobil' id='list_mobil'>$str_option_mobil</select>
					<span id='list_mobil_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
		  <tr>
				<td>Silahkan pilih Sopir</td>
				<td>:</td>
				<td>
					<select name='list_sopir' id='list_sopir'>$str_option_sopir</select>
					<span id='list_sopir_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
			<tr><td colspan=3><br><h2>Biaya-biaya</h2></td></tr>
			$keterangan_bayar_biaya_sopir
			<tr>
				<td>Biaya Sopir</td>
				<td>:</td>
				<td align='right' style='color:$biaya_sopir_color;'>Rp. ".number_format($biaya_sopir,0,",",".")."<input type='hidden' id='biaya_sopir' value='$biaya_sopir_value'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td>Biaya Tol</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($biaya_tol,0,",",".")."<input type='hidden' id='biaya_tol' value='$biaya_tol'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td>Biaya Parkir</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($biaya_parkir,0,",",".")."<input type='hidden' id='biaya_parkir' value='$biaya_parkir'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr>
			$keterangan_voucher_bbm
			<tr>
				<td>Biaya BBM</td>
				<td>: </td>
				<td align='right' style='color:$biaya_bbm_color;'>Rp. $show_biaya_bbm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr><td colspan=3 height=1 bgcolor='red'></td></tr>
			<tr>
				<td>Total Biaya</td>
				<td>:</td>
				<td align='right'>Rp. <span id='biaya_total' name='biaya_total'> ".number_format($total_biaya,0,",",".")."</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
	  </table>");*/  
	
	//BIAYA TOL & Sopir SAJA
	echo("
		<table bgcolor='white' width='100%'>
		  <tr>
				<td>Silahkan pilih mobil</td>
				<td  width='1'>:</td>
				<td>
					<select name='list_mobil' id='list_mobil'>$str_option_mobil</select>
					<span id='list_mobil_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
		  <tr>
				<td>Silahkan pilih Sopir</td>
				<td>:</td>
				<td>
					<select name='list_sopir' id='list_sopir'>$str_option_sopir</select>
					<span id='list_sopir_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
			<tr><td colspan=3><br><h2>Biaya-biaya</h2></td></tr>
			$keterangan_bayar_biaya_sopir
			<tr>
				<td>Biaya Sopir</td>
				<td>:</td>
				<td align='right' style='color:$biaya_sopir_color;'>Rp. ".number_format($biaya_sopir,0,",",".")."<input type='hidden' id='biaya_sopir' value='$biaya_sopir_value'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
			<tr>
				<td>Biaya Tol</td>
				<td>:</td>
				<td align='right'>Rp. ".number_format($biaya_tol,0,",",".")."<input type='hidden' id='biaya_tol' value='$biaya_tol'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			</tr> 
	  </table>");  
	
	//SHOW TANPA BIAYA
	/*echo("
		<table bgcolor='white' width='100%'>
		  <tr>
				<td>Silahkan pilih mobil</td>
				<td  width='1'>:</td>
				<td>
					<select name='list_mobil' id='list_mobil'>$str_option_mobil</select>
					<span id='list_mobil_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
		  <tr>
				<td>Silahkan pilih Sopir</td>
				<td>:</td>
				<td>
					<select name='list_sopir' id='list_sopir'>$str_option_sopir</select>
					<span id='list_sopir_load' style='display:none;'><img src='./images/progress.gif' /></span>
				</td>
			</tr>
	  </table>");  */ //SHOW TANPA BIAYA
	exit;		
}
else 
if($aksi==1){
	//tombol OK di klik untuk mencetak SPJ
	$Reservasi	= new Reservasi();
	$Jadwal			= new Jadwal();
	$Sopir			= new Sopir();
	$Mobil			= new Mobil();
	$BiayaOperasional	= new BiayaOperasional();
	
	$sopir_dipilih= $HTTP_GET_VARS['sopir_dipilih'];
	$mobil_dipilih= $HTTP_GET_VARS['mobil_dipilih'];
	$no_spj				= $HTTP_GET_VARS['no_spj'];
	
	$nourut = rand(1000,9999);
	$useraktif=$userdata['user_id'];
	
	if ($tgl_berangkat!='' && $kode_jadwal!=''){
		
		#mengambil data jadwal
		$data_jadwal	= $Jadwal->ambilDataDetail($kode_jadwal);
		
		if($data_jadwal['FlagSubJadwal']!=1){
			//Mengambil semua jadwal yang memiliki jadwal utama = kode_jadwal
			$result_sub_jadwal	= $Jadwal->ambilListSubKodeJadwalByKodeJadwal($kode_jadwal);
			
			$list_kode_jadwal		= "'$kode_jadwal'";
			
			while($row_sub_jadwal=$db->sql_fetchrow($result_sub_jadwal)){
				$list_kode_jadwal .=",'$row_sub_jadwal[KodeJadwal]'";
			}
			
			$kode_jadwal_utama			= $kode_jadwal;
		}
		else{
			$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
			$list_kode_jadwal		= "'$kode_jadwal'";
		}
		
		$jam_berangkat_show	= $data_jadwal['JamBerangkat'];
		
		//mengambil layout kursi
		
	  //$layout_kursi = $Reservasi->ambilLayoutKursiByKodeJadwal($kode_jadwal_utama);
		
		$row	= $Sopir->ambilDataDetail($sopir_dipilih);
	  	$nama_sopir = $row['Nama'];
		
		$row		= $Mobil->ambilDataDetail($mobil_dipilih);
	  	$no_polisi 	= $row['KodeKendaraan'];
		$id_layout	= $row['IdLayout'];
		
		
		//mendirect pencetakan spj sesuai dengan layout kursi
		
		//mengambil data tiket
		
		$row	= $Reservasi->ambilDataPosisiUntukSPJ($tgl_berangkat,$kode_jadwal_utama);
		
		$tgl_berangkat				= $row['TglBerangkat'];
		$jam_berangkat				= $row['JamBerangkat'];
		$no_polisi						= ($row['KodeKendaraan']=="")?$no_polisi:$row['KodeKendaraan'];
		
		
		$list_field_diupdate="";
		
		//Mengambil jumlah penumpang dan omzet
		$data_total	=$Reservasi->hitungTotalOmzetdanJumlahPenumpangPerSPJ($tgl_berangkat,$list_kode_jadwal);
						
		$total_omzet			=($data_total['TotalOmzet']!='')?$data_total['TotalOmzet']:0;
		$jumlah_penumpang	=($data_total['JumlahPenumpang']!='')?$data_total['JumlahPenumpang']:0;	
		
		//Mengambil jumlah paket dan omzet
		$data_total_paket	=$Reservasi->hitungTotalOmzetdanJumlahPaketPerSPJ($tgl_berangkat,$list_kode_jadwal);

		$total_omzet_paket=($data_total_paket['TotalOmzet']!='')?$data_total_paket['TotalOmzet']:0;
		$jumlah_paket			=($data_total_paket['JumlahPaket']!='')?$data_total_paket['JumlahPaket']:0;	
		
		
		//Mengambil pembiayaan berdasarkan jurusan
		$data_biaya	= $BiayaOperasional->ambilBiayaOpByKodeJadwal($kode_jadwal_utama);
		
		//mengupdate field utk SPJ di tbl posisi
		
		if($row['NoSPJ']==""){
			$no_spj= "MNF".substr($kode_jadwal_utama,0,3).dateYMD().$nourut;
			$data_layout	= $Mobil->getArrayLayout();
			
			$Reservasi->tambahSPJ(
				$no_spj, $kode_jadwal_utama, $tgl_berangkat, 
				$jam_berangkat, $id_layout,$data_layout["$id_layout"], $jumlah_penumpang,
				$no_polisi, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,
				$jumlah_paket,$total_omzet_paket,$data_jadwal['IdJurusan']);
			
			//jika spj belum pernah dicetak, maka akan menambahkan biaya ke database
			
			//memeriksa apakah biaya supir diakumulasikan, jika iya,akan diberi flag biaya sopir kumulatif
			$flag_biaya_sopir_dipilih	= $data_biaya['IsBiayaSopirKumulatif']!=1?$FLAG_BIAYA_SOPIR:$FLAG_BIAYA_SOPIR_KUMULATIF;
			
			//biaya sopir
			if($data_biaya['BiayaSopir']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaSopir'],$flag_biaya_sopir_dipilih,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaSopir'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya tol
			if($data_biaya['BiayaTol']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaTol'],$FLAG_BIAYA_TOL,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaTol'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya parkir
			/*if($data_biaya['BiayaParkir']>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaParkir'],$FLAG_BIAYA_PARKIR,
					$mobil_dipilih,$sopir_dipilih,$data_biaya['BiayaParkir'],
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}
			
			//biaya bbm
			if($data_biaya['IsVoucherBBM']!=1){
				$biaya_bbm	= $HTTP_GET_VARS['biaya_bbm'];
				$flag_biaya_voucher_bbm	= $FLAG_BIAYA_BBM;
			}
			else{
				$biaya_bbm	= $data_biaya['BiayaBBM'];
				$flag_biaya_voucher_bbm	= $FLAG_BIAYA_VOUCHER_BBM;
			}
			
			if($biaya_bbm>0){
				$BiayaOperasional->tambah(
					$no_spj,$data_biaya['KodeAkunBiayaBBM'],$flag_biaya_voucher_bbm,
					$mobil_dipilih,$sopir_dipilih,$biaya_bbm,
					$useraktif,$kode_jadwal_utama,$userdata['KodeCabang']);
			}*/
			
			$duplikat	= ""; 
			
		}
		else{
			$Reservasi->ubahSPJ(
				$row['NoSPJ'], $jumlah_penumpang, 
				$mobil_dipilih, $useraktif, $sopir_dipilih,
				$nama_sopir,$total_omzet,
				$jumlah_paket,$total_omzet_paket);
				
			
			$no_spj	= $row['NoSPJ'];
			
			$duplikat	= "<br>****DUPLIKAT****";
		}
			
		//update data pada tbl posisi
		$Reservasi->ubahPosisiCetakSPJ(
			$kode_jadwal_utama, $tgl_berangkat,$list_field_diupdate, 
			$sopir_dipilih,$mobil_dipilih,$no_spj,$useraktif);
			
		//update tblReservasi
		$Reservasi->ubahDataReservasiCetakSPJ(
			$list_kode_jadwal, $tgl_berangkat,$sopir_dipilih,
			$mobil_dipilih,$no_spj);
			
		//update tblspj untuk insentif sopir
		/*$Reservasi->updateInsentifSopir(
			$no_spj,$layout_kursi, $INSENTIF_SOPIR_LAYOUT_MAKSIMUM, 
			$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM, $data_biaya['KomisiPenumpangSopir']);*/
		
		
		if($jumlah_paket>0){
			$result_paket = $Reservasi->ambilDataPaketUntukSPJ($tgl_berangkat,$list_kode_jadwal);
			$idx_no=0;
			while($row_paket=$db->sql_fetchrow($result_paket)){
				$idx_no++;
				
				$template ->assign_block_vars(
					'ROW_PAKET',
					array(
						'IDX_PAKET_NO'=>"(".$idx_no.")",
						'NO_TIKET_PAKET'=>$row_paket['NoTiket'],
						'TUJUAN'=>$row_paket['Tujuan'],
						'NAMA_PENGIRIM'=>$row_paket['NamaPengirim'],
						'TELP_PENGIRIM'=>$row_paket['TelpPengirim'],
						'NAMA_PENERIMA'=>$row_paket['NamaPenerima'],
						'TELP_PENERIMA'=>$row_paket['TelpPenerima']
						)
				);
			}
		}
		else{
			$tidak_ada_paket	= '<br>TIDAK ADA PAKET<br><br>';
		}
		
		//list penumpang
		
		$result_penumpang = $Reservasi->ambilDataPenumpangUntukSPJ($tgl_berangkat,$list_kode_jadwal);
		
		$total_jenis_penumpang	= array();
		
		if($jumlah_penumpang>0){
			while($row_penumpang=$db->sql_fetchrow($result_penumpang)){
				$nama_index	= $row_penumpang['JenisPenumpang'];
				$total_jenis_penumpang[$nama_index]++;
				
				$template ->assign_block_vars(
					'ROW_PENUMPANG',
					array(
						'NOMOR_KURSI'=>substr("0".$row_penumpang['NomorKursi'],-2),
						'NAMA'=>"(".$row_penumpang['JenisPenumpang'].") ".$row_penumpang['Nama'],
						'NO_TIKET'=>$row_penumpang['NoTiket'],
						'TELP'=>$row_penumpang['Telp'],
						'ASAL'=>$row_penumpang['Asal'],
						'TUJUAN'=>$row_penumpang['Tujuan']
						)
				);
			}
		}
		else{
			$tidak_ada_penumpang	= '<br>TIDAK ADA PENUMPANG<br>';
		}
			
		$list_total_by_jenis_penumpang = "";
		foreach($total_jenis_penumpang as $index=>$value){
			$list_total_by_jenis_penumpang .= $index."=".$value."|";
		}
		
		$list_total_by_jenis_penumpang	= substr($list_total_by_jenis_penumpang,0,-1);
		
		//VOUCHER BBM
		/*
		if($data_biaya['IsVoucherBBM']!=1){
			$show_voucher_bbm	= "";
		}
		else{
			if($row['NoSPJ']==""){
				$nominal_voucher_bbm	= $biaya_bbm;
			}
			else{
				$nominal_voucher_bbm	= $BiayaOperasional->ambilBiayaVoucherBBMByNoSPJ($no_spj);
			}
			
			$show_voucher_bbm	=
				"-------potong disini------<br>
				=======DAYTRANS=======<br>
				*****VOUCHER BBM*******<br>
				BBM-$no_spj
				$duplikat
				<br>Kendaraan:<b>$no_polisi</b><br>
				Driver:$nama_sopir
				<br>mohon untuk mengisi <br>
				bahan bakar kendaraan <br>
				tersebut Sejumlah:<br>
				<b>Rp.".number_format($nominal_voucher_bbm,0,",",".")."</b><br><br>
				Petugas DAYTRANS<br>".$userdata['nama']."<br><br>
				Petugas SPBU<br><br><br><br>
				........................<br>
				-Terima kasih-";
		}*/
		
		$data_perusahaan	= $Reservasi->ambilDataPerusahaan();

        // create qr code no tiket
        $qr->text($no_spj);

        //save qr to image
        $qr->draw(150, "phpqrcode/imageQR/".$no_spj.".png");
        $qrcode = "phpqrcode/imageQR/".$no_spj.".png";

		$template->set_filenames(array('body' => 'SPJ_body.tpl')); 
		$template->assign_vars(array(
		   'DUPLIKAT'    				=>$duplikat,
		   'NAMA_PERUSAHAAN'    =>strtoupper($data_perusahaan['NamaPerusahaan']),
		   'ALAMAT_PERUSAHAAN'  =>$data_perusahaan['AlamatPerusahaan'],
		   'TELP_PERUSAHAAN' 		=>$data_perusahaan['TelpPerusahaan'],
		   'NO_SPJ' 						=>$no_spj,
           'QR_CODE'                => $qrcode,
		   'TGL_BERANGKAT' 			=>dateparse(FormatMySQLDateToTgl($tgl_berangkat)),
		   'JURUSAN' 						=>$kode_jadwal." ".$jam_berangkat_show,
		   'TGL_CETAK' 					=>FormatMySQLDateToTglWithTime(dateNow(true)),
		   'NO_POLISI' 					=>$mobil_dipilih." (".$no_polisi.")",
		   'SOPIR' 							=>$nama_sopir." (".$sopir_dipilih.")",
		   'TIDAK_ADA_PAKET' 		=>$tidak_ada_paket,
		   'JUMLAH_PAKET' 			=>$jumlah_paket,
		   'OMZET_PAKET' 				=>number_format($total_omzet_paket,0,",","."),
		   'TIDAK_ADA_PENUMPANG'=>$tidak_ada_penumpang,
		   'JUMLAH_PENUMPANG' 	=>$jumlah_penumpang,
		   'OMZET_PENUMPANG' 		=>number_format($total_omzet,0,",","."),
		   'BIAYA_TOL' 		=>number_format($data_biaya['BiayaTol'],0,",","."),
		   'TOTAL_OMZET' 				=>number_format($total_omzet+$total_omzet_paket,0,",","."),
		   'JENIS_PENUMPANG'		=>$list_total_by_jenis_penumpang,						
			 'CSO' 								=>$userdata['nama'],
		   'EMAIL_PERUSAHAAN' 	=>$data_perusahaan['EmailPerusahaan'],
		   'WEBSITE_PERUSAHAAN' =>$data_perusahaan['WebSitePerusahaan'],
			 'VOUCHER_BBM'				=>"<br>".$show_voucher_bbm,
			 'SID'								=>$userdata['session_id']
		   )
		);

		$template->pparse('body');	
		
	}
}
?>