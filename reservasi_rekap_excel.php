<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$bulan   = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tahun   = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$calendar= CAL_GREGORIAN;

//INISIALISASI
$jumlah_hari = cal_days_in_month($calendar,$bulan,$tahun);
			
$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Rekap Reservasi '.BulanString($bulan).' '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,3,"Route");
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,3,"Wilayah");
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
for($i=1;$i<=$jumlah_hari;$i++)
{
	$colString = PHPExcel_Cell::stringFromColumnIndex($i+1);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i+1, 3, $i);
	$objPHPExcel->getActiveSheet()->getColumnDimension($colString)->setAutoSize(true);
}
$colString = PHPExcel_Cell::stringFromColumnIndex($jumlah_hari+1);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($jumlah_hari+1,3,"Total Counter");
$objPHPExcel->getActiveSheet()->getColumnDimension($colString)->setAutoSize(true);

$sql_jurusan = 
	"SELECT 
		IdJurusan, 
		CONCAT(f_cabang_get_name_by_kode(KodeCabangAsal),' - ',f_cabang_get_name_by_kode(KodeCabangTujuan)) as Jurusan,
		f_cabang_get_kota_by_kode_cabang(KodeCabangAsal) as Kota  
	FROM 
		tbl_md_jurusan
	ORDER BY KOTA asc, Jurusan asc";

if($result_jurusan = $db->sql_query($sql_jurusan))
{
	$idx_row = 4;
	while($data_jurusan = $db->sql_fetchrow($result_jurusan))
	{
		$total = 0;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx_row , $data_jurusan['Jurusan']);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $idx_row , $data_jurusan['Kota']);

		for($i=1;$i<=$jumlah_hari;$i++)
		{
			$jumlah_reservasi = "";

			$sql = "
				SELECT 
					IdJurusan,
					WEEKDAY(WaktuCetakTiket)+1 AS Hari,
					DAY(WaktuCetakTiket) AS Tanggal,
					IS_NULL(COUNT(NoTiket),0) AS JumlahReservasi
				FROM 
					tbl_reservasi
				WHERE 
					MONTH(WaktuCetakTiket)= $bulan
				AND 
					YEAR(WaktuCetakTiket)= $tahun
				AND
					CetakTiket=1 
				AND 
					FlagBatal!=1 
				AND 
					JenisPembayaran = 0
				AND
					IdJurusan = $data_jurusan[IdJurusan]
				Group by Date(WaktuCetakTiket)
				order by (WaktuCetakTiket) ";
	
			if($result = $db->sql_query($sql))
			{
				while($row = $db->sql_fetchrow($result))
				{
					$jumlah_reservasi .= ($row['Tanggal'] == $i ? $row['JumlahReservasi'] : "");
				}
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($i+1, $idx_row, ($jumlah_reservasi != "" ? $jumlah_reservasi  : 0 ));
			}
			else
			{
				echo("Error:".__LINE__);exit;
			}

			$total = $total + $jumlah_reservasi;
		}
		$col = PHPExcel_Cell::stringFromColumnIndex($jumlah_hari);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($jumlah_hari+1, $idx_row, "=SUM(C".$idx_row.":".$col.$idx_row.")");
		$idx_row++;
	}
} 
else
{
	echo("Error:".__LINE__);exit;
}

$objPHPExcel->getActiveSheet()->mergeCells("A".$idx_row.":B".$idx_row);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $idx_row , "Total");

for($j=1;$j<=$jumlah_hari;$j++)
{
	$col2 = PHPExcel_Cell::stringFromColumnIndex($j+1);
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j+1, $idx_row , "=SUM(".$col2."4:".$col2.($idx_row-1).")");
}


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);


$styleArray = array(
		'borders' => array(
				'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
				)
		)
);
/*
for($col = 'A'; $col !== 'M'; $col++) {
	$objPHPExcel->getActiveSheet()->getStyle($col.'3:'.$col.$idx_row)->applyFromArray($styleArray);
}
 */

	header('Content-Type: application/vnd.ms-excel');
  	header('Content-Disposition: attachment;filename="Rekap Reservasi periode '.BulanString($bulan).' '.$tahun.'.xls"');
  	header('Cache-Control: max-age=0');

  	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  	$objWriter->save('php://output'); 

  
?>
