<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 402;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_uang_cabang/laporan_uang_cabang_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang	= " AND KodeCabang='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeCabang","tbl_md_cabang",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_uang_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		KodeCabang,
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket,
		IS_NULL(SUM(SubTotal),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang
	GROUP BY KodeCabang ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		KodeCabang,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND (CaraPembayaran<$PAKET_CARA_BAYAR_DI_TUJUAN OR (CaraPembayaran=$PAKET_CARA_BAYAR_DI_TUJUAN AND StatusDiambil=1))   AND FlagBatal!=1 $kondisi_cabang
	GROUP BY KodeCabang ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		KodeCabang,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang
	GROUP BY KodeCabang ORDER BY KodeCabang";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['KodeCabang']]	= $row;
}

//QUERY BIAYA TAMBAHAN
$sql = "SELECT KodeCabang,
		IS_NULL(SUM(Jumlah),0) as TotalBiayaTambahan
		FROM tbl_biaya_tambahan
		WHERE (DATE(TglCetak) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		GROUP BY KodeCabang ORDER BY KodeCabang";

if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
	//die(mysql_error());
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_tambahan_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']							= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['Fax']								= $row['Fax'];
	$temp_array[$idx]['hp']									= $row['hp'];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['KodeCabang']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']					= $data_biaya_total[$row['KodeCabang']]['TotalBiaya'];
	$temp_array[$idx]['TotalBiayaTambahan']			= $data_biaya_tambahan_total[$row['KodeCabang']]['TotalBiayaTambahan'];
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'] - $temp_array[$idx]['TotalBiayaTambahan'];
	
	$idx++;
}

if($order=='ASC'){
	//PHP Versi 5
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	
	//PHP Versi 4
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//PHP Versi 5
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	
	//PHP Versi 4
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_rekap_tiket_detail.'.$phpEx.'?p0='.$tanggal_mulai.'&p1='.$tanggal_akhir.'&p2='.$temp_array[$idx]['KodeCabang'].'&p3=4')."\");return false'>Detail<a/>&nbsp;+&nbsp;";		
	
	$act 	.="<a href='".append_sid('laporan_uang_cabang_grafik.php?kode_cabang='.$temp_array[$idx]['KodeCabang'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order)."'>Grafik<a/>";		
	
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	$total_biaya_tambahan				= $temp_array[$idx]['TotalBiayaTambahan'];
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'kode_cabang'=>$temp_array[$idx]['KodeCabang'],
				'cabang'=>$temp_array[$idx]['Nama'],
				'alamat'=>$temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Kota'],
				'total_penumpang'=>number_format($total_tiket,0,",","."),
				'total_omzet'=>number_format($total_penjualan_tiket,0,",","."),
				'total_paket'=>number_format($total_paket,0,",","."),
				'total_omzet_paket'=>number_format($total_penjualan_paket,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
				'total_biaya'=>number_format($total_biaya,0,",","."),
				'total_biaya_tambahan'=>number_format($total_biaya_tambahan,0,',','.'),
				'total_profit'=>number_format($total,0,",","."),
				'act'=>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$temp_array[$idx]['KodeCabang'].
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
	
$script_cetak_pdf="Start('laporan_uang_cabang_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_uang_cabang_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
	
$page_title	= "Keuangan Cabang";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_uang_cabang.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Nama cabang ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=KodeCabang'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Kode cabang ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=Alamat'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Alamat cabang ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=TotalTiket'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan Total tiket ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=TotalPenjualanTiket'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Total penjualan tiket ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=TotalPaket'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Total paket ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=TotalPenjualanPaket'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Total penjualan paket ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=TotalDiscount'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Total discount ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=TotalBiaya'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan Total biaya ($order_invert)",
	'A_SORT_10'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by=Total'.$parameter_sorting),
	'TIPS_SORT_10'		=> "Urutkan Total ($order_invert)",
	'A_SORT_11'			=> append_sid('laporan_uang_cabang.'.$phpEx.'?sort_by_TotalBiayaTambahan'.$parameter_sorting),
	'TIPS_SORT_11'		=> "Urutkan Total Biaya Tambahan ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>