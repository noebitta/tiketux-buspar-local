<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 702;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassRole.php');

// PARAMETER
$mode 			= isset($HTTP_POST_VARS['mode'])? $HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
$cari       = isset($HTTP_POST_VARS['cari'])?$HTTP_POST_VARS['cari'] : $HTTP_GET_VARS['cari'];
$idx_page   = isset($HTTP_POST_VARS['idxpage'])?$HTTP_POST_VARS['idxpage']:$HTTP_GET_VARS['idxpage'];
$order_by   = isset($HTTP_POST_VARS['orderby'])?$HTTP_POST_VARS['orderby']:$HTTP_GET_VARS['orderby'];
$sort       = isset($HTTP_POST_VARS['sort'])?$HTTP_POST_VARS['sort']:$HTTP_GET_VARS['sort'];

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;
$Role       = new Role();

//ROUTER
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    exit;

  case 1:
    //TAMBAH DATA
    showInputData();

    exit;

  case 2:
    //SHOW DETAIL DATA
    $id = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];

    showDetailData($id);

    exit;

  case 3:
    //SIMPAN DATA
    $id        = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
    $nama_role = isset($HTTP_POST_VARS['namarole'])?$HTTP_POST_VARS['namarole'] : $HTTP_GET_VARS['namarole'];

    echo(prosesSimpan($id,$nama_role));

    exit;

  case 4:
    //HAPUS DATA
    $id        = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];

    echo(hapusData($id));

    break;
}

//METHODES & PROCESS

function showData(){

  global $db,$cari,$order_by,$sort,$idx_page,$template,$Permission,$userdata,$id_page,$Role,$VIEW_PER_PAGE;

  $template->set_filenames(array('body' => 'pengaturan.role/index.tpl'));

  $is_permited_crud =  $Permission->isPermitted($userdata["user_level"],$id_page+0.1); //0.1 utk permission crud

  if($is_permited_crud){
    $template->assign_block_vars("CRUD_ADD",array());
    $template->assign_block_vars("CRUD_DEL",array());
  }

  $result = $Role->ambilData($order_by,$sort,$idx_page,$cari);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd =($no%2)==0?"even":"odd";

    $template->assign_block_vars(
      'ROW',array(
        'odd'       =>$odd,
        'no'        =>$idx_page*$VIEW_PER_PAGE+$no,
        'id'        =>$row["IdRole"],
        'namarole'  =>$row["NamaRole"]
      )
    );

    //ACTION
    if ($is_permited_crud){
      $template->assign_block_vars("ROW.ACT_EDIT", array());
      $template->assign_block_vars("ROW.ACT_DEL", array());
    }

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	=> basename(__FILE__),
      'CARI'      => $cari,
      'ORDER'     => $order_by,
      'SORT'      => $sort,
      'IDX_PAGE'  => $idx_page,
      'PAGING'    => $paging
    )
  );


  $template->pparse('body');

}

function showInputData(){
  global $template;

  $template->set_filenames(array('body' => 'pengaturan.role/edit.tpl'));

  $template->assign_block_vars("ACT_SAVE",array());

  $template->assign_vars(array(
      'OPERATION'	=> "tambah",
      'NAMA_ROLE' => ""
    )
  );

  $template->pparse('body');
}

function showDetailData($id){
  global $template,$Permission,$Role,$userdata,$id_page;

  $template->set_filenames(array('body' => 'pengaturan.role/edit.tpl'));

  $is_permited_crud =  $Permission->isPermitted($userdata["user_level"],$id_page+0.1); //0.1 utk permission crud

  if($is_permited_crud){
    $template->assign_block_vars("ACT_SAVE",array());
    $readonly = "";
  }
  else{
    $readonly = "readonly";
  }

  $data = $Role->ambilDataDetail($id);

  $template->assign_vars(array(
    "OPERATION"	=> "Data",
    "ID"        => $data["IdRole"],
    "NAMA_ROLE" => $data["NamaRole"],
    "READONLY"  => $readonly
  ));

  $template->pparse('body');
}

function prosesSimpan($id,$nama_role){
  global $userdata,$Role,$Permission,$id_page;

  $is_permited_crud =  $Permission->isPermitted($userdata["user_level"],$id_page+0.1); //0.1 utk permission crud

  if($is_permited_crud) {
    $nama_role  = strtoupper(cleansingString($nama_role));

    //cek duplikasi
    if(!$Role->periksaDuplikasi($nama_role,$id)){
      if($id==""){
        //proses tambah
        $id=$Role->tambah($nama_role);
      }
      else{
        //proses ubah
        $Role->ubah($id,$nama_role);
      }

      $return = array("status" => "OK","id"=>$id);

    }
    else{
      $return = array("status" => "GAGAL","error"=>"data dengan nama role ini sudah terdaftar dalam sistem!");
    }

  }
  else{
    $return = array("status" => "GAGAL","error"=>"anda tidak memiliki akses untuk proses ini!");
  }

  return json_encode($return);
}

function hapusData($id){
  global $userdata,$Role,$Permission,$id_page;

  $is_permited_crud =  $Permission->isPermitted($userdata["user_level"],$id_page+0.1); //0.1 utk permission crud

  if($is_permited_crud){

    $Role->hapus($id);

    $return = array("status" => "OK","id"=>$id);
  }
  else{
    $return = array("status" => "GAGAL","error"=>"anda tidak memiliki akses untuk proses ini!");
  }

  return json_encode($return);
}
?>