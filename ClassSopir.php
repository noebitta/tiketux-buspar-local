<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    //redirect('index.'.$phpEx,true);
    exit;
}
//#############################################################################

class Sopir{

    //KAMUS GLOBAL
    var $ID_FILE; //ID Kelas

    //CONSTRUCTOR
    function Sopir(){
        $this->ID_FILE="C-SPR";
    }

    //BODY

    function isDuplikasi($nrp){

        //kamus
        global $db;

        $sql = "SELECT COUNT(1)  FROM tbl_md_sopir WHERE KodeSopir='$nrp'";

        if(!$result=$db->sql_query($sql)){
            echo("Err: $this->ID_FILE ".__LINE__);
            exit;
        }

        $row  = $db->sql_fetchrow($result);

        return ($row[0]>0?true:false);

    }//  END isDuplikasi

    function ambilData($order_by,$sort_by=0,$idx_page="",$cari=""){

        //kamus
        global $db, $VIEW_PER_PAGE;

        //UNTUK SORTING
        $koloms = array("Nama","KodeSopir","HP","Alamat","NoSIM","JenisSIM","SIMExpired","KotaLahir","TglLahir","Norek","NamaBank","CabangBank","KodeCabang","Reguler","Aktif");

        $sort     = ($sort_by==0)?"ASC":"DESC";

        $order		= ($order_by!=="" && $order_by!==null)?" ORDER BY $koloms[$order_by] $sort":'';
        $set_limit= ($idx_page!=="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

        $set_kondisi  = "WHERE 1".($cari==""?"":" AND (Nama LIKE '%$cari%' OR KodeSopir LIKE '$cari%' OR KodeCabang LIKE '%$cari%' OR HP LIKE '$cari%'  OR Alamat LIKE '%$cari%' OR NoSIM LIKE '%$cari%' '%$cari%' OR JenisSIM LIKE '%$cari%' OR NamaBank LIKE '%$cari%')");

        $sql  =
            "SELECT SQL_CALC_FOUND_ROWS
        IdSopir,
        KodeSopir,
        Nama,
        Alamat,
        HP,
        NoSIM,
        JenisSIM,
        SIMExpired,
        KotaLahir,
        TglLahir,
        Norek,
        NamaBank,
        CabangBank,
        KodeCabang,
        IF(IsAktif=1,'AKTIF','NON-AKTIF') AS StatusAktif, IF(IsReguler,'REGULER','BACKUP') AS StatusReguler
      FROM tbl_md_sopir
      $set_kondisi
      $order
	    $set_limit";

        if (!$result = $db->sql_query($sql)){
            echo("Err: $this->ID_FILE:".__LINE__);exit;
        }

        return $result;

    }//  END ambilData

    function ambilDetailData($id_sopir){
        global $db;

        $sql  = "SELECT IdSopir,KodeSopir,Nama,
                HP,Alamat,NoSIM,JenisSIM,SIMExpired,
                KotaLahir,TglLahir,Norek,
                NamaBank,CabangBank,KodeCabang,
                IsReguler,IsAktif
                FROM tbl_md_sopir WHERE IdSopir='$id_sopir'";

        if (!$result = $db->sql_query($sql,TRUE)){
            echo("Err:$this->ID_FILE ".__LINE__);exit;
        }

        $row=$db->sql_fetchrow($result);

        return $row;
    } //ambilDetailData

    function tambah($KodeSopir,$Nama,$Alamat,$HP,$NoSIM,$JenisSIM,$SIMExpired,$KotaLahir,$TglLahir,$Norek,$NamaBank,$CabangBank,$KodeCabang,$IsReguler,$IsAktif){

        //kamus
        global $db;

        $sql  =
            "INSERT INTO tbl_md_sopir SET
        KodeSopir='$KodeSopir',
        Nama='$Nama',
        Alamat='$Alamat',
        HP='$HP',
        NoSIM='$NoSIM',
        JenisSIM='$JenisSIM',
        SIMExpired='$SIMExpired',
        KotaLahir='$KotaLahir',
        TglLahir='$TglLahir',
        Norek='$Norek',
        NamaBank='$NamaBank',
        CabangBank='$CabangBank',
        KodeCabang='$KodeCabang',
        IsReguler='$IsReguler',
        IsAktif='$IsAktif'";

        if(!$result=$db->sql_query($sql)){
            $ret_val["status"]  = "GAGAL";
            $ret_val["pesan"]   = "SQL ERROR";

            return $ret_val;
        }

        $ret_val["status"]  = "OK";

        return $ret_val;
    }

    function ubah($IdSopir,$KodeSopir,$Nama,$Alamat,$HP,$NoSIM,$JenisSIM,$SIMExpired,$KotaLahir,$TglLahir,$Norek,$NamaBank,$CabangBank,$KodeCabang,$IsReguler,$IsAktif){
        global $db;

        $sql  =
            "UPDATE tbl_md_sopir SET
        KodeSopir='$KodeSopir',
        Nama='$Nama',
        Alamat='$Alamat',
        HP='$HP',
        NoSIM='$NoSIM',
        JenisSIM='$JenisSIM',
        SIMExpired='$SIMExpired',
        KotaLahir='$KotaLahir',
        TglLahir='$TglLahir',
        Norek='$Norek',
        NamaBank='$NamaBank',
        CabangBank='$CabangBank',
        KodeCabang='$KodeCabang',
        IsReguler='$IsReguler',
        IsAktif='$IsAktif'
      WHERE IdSopir=$IdSopir";

        if(!$result=$db->sql_query($sql)){
            $ret_val["status"]  = "GAGAL";
            $ret_val["pesan"]   = "SQL ERROR ".__LINE__;

            return $ret_val;
        }
    } //ubah

//    function ubah($kode_sopir_old,$kode_sopir,$nama,$hp,$alamat,$no_sim,$flag_aktif){
//
//        /*
//        IS	: data kendaraan sudah ada dalam database
//        FS	:Data kendaraan diubah
//        */
//
//        //kamus
//        global $db;
//
//        //MENAMBAHKAN DATA KEDALAM DATABASE
//        $sql = "CALL sp_sopir_ubah('$kode_sopir_old','$kode_sopir','$nama','$hp','$alamat','$no_sim',$flag_aktif)";
//
//        if (!$db->sql_query($sql)){
//            die_error("Err: $this->ID_FILE".__LINE__);
//        }
//
//        return true;
//    }

    function hapus($id_sopir){

        //kamus

        global $db,$userdata;

        //MEMBACKUP JADWAL YANG DIHAPUS

        $sql="SELECT * FROM tbl_md_sopir WHERE IdSopir IN ($id_sopir)";

        if (!$result=$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        //MENGHAPUS DATA DARI DATABASE
        $sql =
            "DELETE FROM tbl_md_sopir
			WHERE IdSopir IN($id_sopir);";

        if (!$db->sql_query($sql)){
            return false;
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        $ret_val["status"]  = "OK";

        return $ret_val;
    }//end hapus

    function ubahStatusAktif($id_sopir){

        //kamus
        global $db;

        $sql =
            "UPDATE tbl_md_sopir SET
        IsAktif=1-IsAktif
      WHERE IdSopir='$id_sopir';";

        if (!$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        return true;
    }//end ubahStatusAktif

    function ubahStatusReguler($id_sopir){

        //kamus
        global $db;

        $sql =
            "UPDATE tbl_md_sopir SET
        IsReguler=1-IsReguler
        
      WHERE IdSopir='$id_sopir';";

        if (!$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        return true;
    }//end ubahStatusReguler

    function ambilDataDetail($kode_sopir){

        /*
        Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
        */

        //kamus
        global $db;

        $sql =
            "SELECT *, f_kendaraan_ambil_nopol_by_kode(Mobil1) AS NOPOL1, f_kendaraan_ambil_nopol_by_kode(Mobil2) AS NOPOL2
			FROM tbl_md_sopir
			WHERE KodeSopir='$kode_sopir';";

        if ($result = $db->sql_query($sql)){
            $row=$db->sql_fetchrow($result);
            return $row;
        }
        else{
            die_error("Err: $this->ID_FILE".__LINE__);
        }

    }//  END ambilDataDetail

}
?>