<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

//PERSIAPAN
$mode   = isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];

$today = date('Y-m-d');
$result = array();
$Petugas = 0;
$NamaPetugas = "TIKETUK";
//PILIHAN MODE
switch ($mode){
    case 'login':
        // LOGIN
        $email = $HTTP_POST_VARS['email'];
        $upass = $HTTP_POST_VARS['password'];
        $sandi = md5($upass);
        $sql = "SELECT * FROM tbl_md_member WHERE Email = '$email' AND KataSandi = '$sandi'";
        if(!$query = $db->sql_query($sql)){
            $result = ['status'=>0,'pesan'=> mysql_error()];
            echo json_encode($result);
            exit;
        }
        $row = $db->sql_fetchrow($query);
        if($row == null){
            $result = ['status'=>0,'pesan'=>'Email atau Password Salah'];
            echo json_encode($result);
            exit;
        }
        else{
            $result =['status'      => 1,
                    'pesan'         => 'success',
                    'idmember'      => $row['IdMember'],
                    'nama'          => $row['Nama'],
                    'email'         => $row['Email'],
                    'tempat_lahir'  => $row['TempatLahir'],
                    'tanggal_lahir' => $row['TglLahir'],
                    'alamat'        => $row['Alamat'],
                    'hp'            => $row['Handphone'],
                    'saldo'         => $row['SaldoDeposit'],
            ];
            echo json_encode($result);
            exit;
        }
    case 'register':
        //AMBIL DATA POST
        $nama   	    = strtoupper($HTTP_POST_VARS['nama']);
        $hp				= $HTTP_POST_VARS['hp'];
        $email			= $HTTP_POST_VARS['email'];
        $tempat_lahir   = $HTTP_POST_VARS['tempat_lahir'];
        $tgl_lahir   	= date_format(date_create($HTTP_POST_VARS['tanggal_lahir']),'Y-m-d');
        $alamat   		= $HTTP_POST_VARS['alamat'];
        $password		= $HTTP_POST_VARS['password'];

        //CEK ID MEMBER PERNAH DIGENERTE ATAU BELUM
        $i=1;
        do{
            $id_member      = generateIdMember();
            //JIKA ID MEMBER TELAH TERDAFTAR MAKA GENERATE LAGI, JIKA TIDAK TERDAFTAR MAKA KELUAR DARI LOOPING
            if(periksaDuplikasiID($id_member) == 0){
                $i=2;
            }
        }while($i<2);
        //END CEK ID MEMBER PERNAH DIGENERTE ATAU BELUM
        //PERIKSA DUPLIKASI MEMBER
        if(periksaDuplikasi($email,$hp) > 0){
            $result = ['status'=>0,"pesan"=>"Member sudah terdaftar dalam sistem!"];
            echo json_encode($result);
            exit;
        }
        //SIMPAN DATA KE DATABASE
        $sql = "INSERT INTO tbl_md_member (IdMember, KategoriMember, Point, Nama, TempatLahir, TglLahir, TglRegistrasi, Alamat,
                                          Handphone, Email, KataSandi, FlagAktif, PetugasPendaftar, NamaPetugasDaftar, WaktuPendaftaran)
			    VALUES('$id_member',0,0,'$nama','$tempat_lahir','$tgl_lahir','$today','$alamat','$hp','$email',md5('$password'),1,'$petugas','$NamaPetugas',NOW())";

        if(!$query = $db->sql_query($sql)){
            $result = ['status'=>0,'pesan'=> mysql_error()];
            echo json_encode($result);
            exit;
        }

        //AMBIL DATA MEMBER UNTUK RETURN
        $sql = "SELECT * FROM tbl_md_member WHERE IdMember = '$id_member'";

        if(!$query = $db->sql_query($sql)){
            $result = ['status'=>0,'pesan'=> mysql_error()];
            echo json_encode($result);
            exit;
        }
        $row = $db->sql_fetchrow($query);
        $result =[
            'status'        => 1,
            'pesan'         => 'success',
            'idmember'      => $row['IdMember'],
            'nama'          => $row['Nama'],
            'email'         => $row['Email'],
            'tempat_lahir'  => $row['TempatLahir'],
            'tanggal_lahir' => $row['TglLahir'],
            'alamat'        => $row['Alamat'],
            'hp'            => $row['Handphone'],
            'saldo'         => $row['SaldoDeposit'],
        ];
        echo json_encode($result);
    exit;
    case 'profile':
        //AMBIL DATA POST
        $IDMember   = $HTTP_POST_VARS['id_member'];

        //AMBIL DATA MEMBER BY ID
        $sql = "SELECT * FROM tbl_md_member WHERE IdMember='$IDMember'";
        if(!$result = $db->sql_query($sql)){
            $result = ['status'=>0,'pesan'=> mysql_error()];
            echo json_encode($result);
            exit;
        }
        $row = $db->sql_fetchrow($query);
        if($row == null){
            $result = ['status'=>0,'pesan'=>'ID MEMBER TIDAK TERDAFTAR'];
            echo json_encode($result);
            exit;
        }else{
            $result =['status'      => 1,
                'pesan'         => 'success',
                'idmember'      => $row['IdMember'],
                'nama'          => $row['Nama'],
                'email'         => $row['Email'],
                'tempat_lahir'  => $row['TempatLahir'],
                'tanggal_lahir' => $row['TglLahir'],
                'alamat'        => $row['Alamat'],
                'telepon'       => $row['Telp'],
                'hp'            => $row['Handphone'],
                'saldo'         => $row['SaldoDeposit'],
            ];
            echo json_encode($result);
            exit;
        }
    case 'topup':
        //AMBIL DATA DARI POST
        $id_member       = $HTTP_POST_VARS['id_member'];
        $koderef         = $HTTP_POST_VARS['kode_referensi'];
        $jumlah_topup    = $HTTP_POST_VARS['jml_deposit'];
        $kode_token      = $HTTP_POST_VARS['kodepembayaran'];

        $harga_point      = ambilHargaPoin();
        $jumlah_poin      = $jumlah_topup>$harga_point?$jumlah_topup/$harga_point:0;

        // CEK ID MEMBER EXIST OR NOT
        $sql = "SELECT * FROM tbl_md_member WHERE IdMember = '$id_member'";

        if(!$query = $db->sql_query($sql)){
            $result = ['status'=>0,'pesan'=> mysql_error()];
            echo json_encode($result);
            exit;
        }
        $row = $db->sql_fetchrow($query);
        if($row == null){
            $result = ['status'=>0,'pesan'=> 'MEMBER TIDAK TERDAFTAR'];
            echo json_encode($result);
            exit;
        }
        //END PENGECEKAN ID MEMBER
        //==========TOP UP============//
        if(topUpMember($id_member,$koderef,$jumlah_topup, $jumlah_poin,$Petugas,$NamaPetugas, $kode_token)){
            //UPDATE SALDO DEPOSIT POINT MEMBER
            $sql = "UPDATE tbl_md_member SET SaldoDeposit = SaldoDeposit+$jumlah_poin, WaktuTransaksiTerakhir = NOW(), Signature = '$kode_token' WHERE IdMember ='$id_member'";
            if(!$query = $db->sql_query($sql)){
                $result = ['status'=>0,'pesan' => mysql_error()];
                echo json_encode($result);
                exit;
            }
            //GET SALDO UFTER TOPUP
            $sql = "SELECT SaldoDeposit FROM tbl_md_member WHERE IdMember = '$id_member'";
            if(!$query = $db->sql_query($sql)){
                $result = ['status'=>0,'pesan'=> mysql_error()];
                echo json_encode($result);
                exit;
            }
            $row = $db->sql_fetchrow($query);
            $SALDO = $row['SaldoDeposit'];
            //CATAT LOG TRANSASKSI
            $ket = "TOP UP DEPOSIT Rp.".$jumlah_topup."Point".$jumlah_poin;
            $sql="INSERT INTO tbl_member_deposit_trx_log (
                  IdMember,KodeReferensi,WaktuTransaksi,
                  Jumlah,Saldo,Signature,IsDebit,Keterangan)
                  VALUES('$id_member','$koderef',NOW(),
                         '$jumlah_topup','$SALDO','$kode_token',0,'$ket');";
            if (!$db->sql_query($sql)){
                $result = ['status'=>0,'pesan'=> mysql_error(),'function'=>'topUpMember lOG TRANSASKSI TOPUP'];
                echo json_encode($result);
                exit;
            }
            $result = ['status'=>1,'pesan' => 'success'];
            echo json_encode($result);
        }else{
            $result = ['status'=>0,'pesan' => 'gagal'];
            echo json_encode($result);
        }
    exit;
    case 'debit':
        //AMBIL DATA DARI POST
        $id_member       = $HTTP_POST_VARS['id_member'];
        $koderef         = $HTTP_POST_VARS['kode_referensi'];
        $harga_tiket     = $HTTP_POST_VARS['harga_tiket'];
        $kode_token      = $HTTP_POST_VARS['kode_booking'];

        //CONVERT HARGA TIKET KE POINT
        $harga_point     = ambilHargaPoin();
        $point           = $harga_tiket/$harga_point;
        //END CONVER HAGA TIKET KE POINT

        //BANDINGKAN SALDO POINT DENGAN POINT DARI TIKET
        $sql = "SELECT SaldoDeposit FROM tbl_md_member WHERE IdMember = '$id_member'";
        if(!$query = $db->sql_query($sql)){
            $result = ['status'=>0,'pesan'=> mysql_error()];
            echo json_encode($result);
            exit;
        }
        $row = $db->sql_fetchrow($query);
        if($row['SaldoDeposit'] < $point){
            $result = ['status'=>0,'pesan'=> 'SALDO POINT LEBIH KECIL DARI HARGA TIKET'];
            echo json_encode($result);
            exit;
        }
        //=======DEBIT POINT======//
        if(debitMember($id_member,$koderef,$harga_tiket, $point,$Petugas,$NamaPetugas, $kode_token)) {
            //UPDATE SALDO POIN MEMBER
            $sql = "UPDATE tbl_md_member SET SaldoDeposit = SaldoDeposit-$point, WaktuTransaksiTerakhir = NOW(), Signature = '$kode_token' WHERE IdMember = '$id_member'";
            if (!$query = $db->sql_query($sql)) {
                $result = ['status' => 0, 'pesan' => 'gagal update saldo untuk debit'];
                echo json_encode($result);
                exit;
            }
            //GET SALDO UFTER DEPOSIT
            $sql = "SELECT SaldoDeposit FROM tbl_md_member WHERE IdMember = '$id_member'";
            if(!$query = $db->sql_query($sql)){
                $result = ['status'=>0,'pesan'=> mysql_error()];
                echo json_encode($result);
                exit;
            }
            $row = $db->sql_fetchrow($query);
            $SALDO = $row['SaldoDeposit'];
            //CATAT LOG TRANSASKSI
            $ket = "PEMBELIAN.".$kode_token." ".date('d-m-Y');
            $sql="INSERT INTO tbl_member_deposit_trx_log (
                  IdMember,KodeReferensi,WaktuTransaksi,
                  Jumlah,Saldo,Signature,IsDebit,Keterangan)
                  VALUES('$id_member','$koderef',NOW(),
                         '$harga_tiket','$SALDO','$kode_token',1,'$ket');";
            if (!$db->sql_query($sql)){
                $result = ['status'=>0,'pesan'=> mysql_error(),'function'=>'topUpMember lOG TRANSASKSI DEBIT'];
                echo json_encode($result);
                exit;
            }
            $result = ['status' => 1, 'pesan' => 'success'];
            echo json_encode($result);
        }else{
            $result = ['status'=>0,'pesan' => 'gagal'];
            echo json_encode($result);
        }
    exit;
}

//FUNCTION-FUNCTION YANG DIBUTUHKAN
function generateIdMember(){
    $temp	= array("1","2","3","4","5","6","7","8","9",
        "A","B","C","D","E","F","G","H","I","J",
        "K","L","M","N","O","P","Q","R","S","T",
        "U","V","W","X","Y","Z",
        "A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
        "K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
        "U1","V1","W1","X1","Y1","Z1");

    $y		= $temp[date("y")*1];
    $m		= $temp[date("m")*1];
    $d		=	$temp[date("d")*1];
    $j		= $temp[date("j")*1];
    $mn		= $temp[date("i")*1];
    $rnd1	= $temp[rand(1,61)];
    $rnd2	= $temp[rand(1,61)];

    return $y.$rnd1.$m.$rnd2.$d.$j.$mn;
}
function periksaDuplikasiID($id_member){

    //kamus
    global $db;

    $sql = "SELECT COUNT(IdMember) AS jumlah_data FROM tbl_md_member WHERE IdMember = '$id_member'";

    if ($result = $db->sql_query($sql)){
        $row = $db->sql_fetchrow($result);
    }
    else{
        $result = ['status'=>0,'pesan'=> mysql_error(), 'function'=>'periksaDuplikasiID'];
        echo json_encode($result);
        exit;
    }

    return $row['jumlah_data'];
}
function periksaDuplikasi($email,$handphone){


    //kamus
    global $db;

    $sql =
        "SELECT COUNT(Email) AS jumlah_data FROM tbl_md_member WHERE Email = '$email' OR Handphone = '$handphone'";

    if ($result = $db->sql_query($sql)){
        $row = $db->sql_fetchrow($result);
    }
    else{
        $result = ['status'=>0,'pesan'=> mysql_error(), 'function'=>'periksaDuplikasi'];
        echo json_encode($result);
        exit;
    }

    return $row['jumlah_data'];

}//  END periksaDuplikasi
function generateKodeReferensi(){

    global $db;

    $temp	= array("0",
        "1","2","3","4","5","6","7","8","9",
        "A","B","C","D","E","F","G","H","I","J",
        "K","L","M","N","O","P","Q","R","S","T",
        "U","V","W","X","Y","Z",
        "A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
        "K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
        "U1","V1","W1","X1","Y1","Z1");

    $y		= $temp[date("y")*1];
    $m		= $temp[date("m")*1];
    $d		=	$temp[date("d")*1];
    $j		= $temp[date("j")*1];
    $mn		= $temp[date("i")*1];
    $s		= $temp[date("s")*1];

    $is_duplikasi = false;

    do{
        $rnd1	= $temp[rand(1,61)];
        $rnd2	= $temp[rand(1,61)];
        $rnd3	= $temp[rand(1,61)];

        $kode_referensi	= "DTA".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s.$rnd3;

        $is_duplikasi = $this->isDuplikasiKodeRef($kode_referensi);

    }while($is_duplikasi);



    return $kode_referensi;
}
function ambilHargaPoin(){

    global $db;

    $sql =
        "SELECT NilaiParameter
      FROM tbl_pengaturan_parameter
      WHERE NamaParameter LIKE 'MEMBER_RP2POINT'";

    if (!$result = $db->sql_query($sql)){
        $result = ['status'=>0,'pesan'=> mysql_error()];
        echo json_encode($result);
        exit;
    }

    $row=$db->sql_fetchrow($result);
    return $row[0];

}//  END ambilHargaPoin
function topUpMember($IdMember,$KodeReferensi,$JumlahRupiah, $JumlahPoin,$PetugasTopUp,$NamaPetugasTopUp, $kode_token){

    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
        "INSERT INTO tbl_member_deposit_topup_log (
        IdMember,KodeReferensi,WaktuTransaksi,
        JumlahRupiah,JumlahPoin,PetugasTopUp,
        NamaPetugasTopUp,KodeToken,IsVerified,WaktuVerified)
      VALUES('$IdMember','$KodeReferensi',NOW(),
        '$JumlahRupiah','$JumlahPoin','$PetugasTopUp',
        '$NamaPetugasTopUp','$kode_token',1,NOW());";

    if (!$db->sql_query($sql)){
        $result = ['status'=>0,'pesan'=> mysql_error(),'function'=>'topUpMember'];
        echo json_encode($result);
    }

    return true;
}
function debitMember($IdMember,$KodeReferensi,$JumlahRupiah, $JumlahPoin,$PetugasDebit,$NamaPetugasDebit, $kode_token){
    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
        "INSERT INTO tbl_member_deposit_debit_log (
        IdMember,KodeReferensi,WaktuTransaksi,
        JumlahRupiah,JumlahPoin,PetugasDebit,
        NamaPetugasDebit,KodeToken,IsVerified,WaktuVerified)
      VALUES('$IdMember','$KodeReferensi',NOW(),
        '$JumlahRupiah','$JumlahPoin','$PetugasDebit',
        '$NamaPetugasDebit','$kode_token',1,NOW());";

    if (!$db->sql_query($sql)){
        $result = ['status'=>0,'pesan'=> mysql_error(),'function'=>'debitMember'];
        echo json_encode($result);
        exit;
    }
    return true;
}

?>