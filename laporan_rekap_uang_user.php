<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];


$bulan	=($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}

// LIST
$template->set_filenames(array('body' => 'laporan_rekap_uang_user/laporan_rekap_uang_user_body.tpl'));


//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}

//memilih tabel yang akan diambil

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];
$tgl_hari_sekarang= $temp_tanggal_sekarang[2];

if($tahun==$tahun_sekarang && $bulan==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi";
}
	
//QUERY PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM $tbl_reservasi
	WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun 
		AND CetakTiket=1 
		AND FlagBatal!=1 
		AND PetugasCetakTiket=$userdata[user_id]
		AND JenisPembayaran = 0
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket) ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//QUERY TOP UP MEMBER
$sql=
	"SELECT 
		WEEKDAY(WaktuTransaksi)+1 AS Hari,DAY(WaktuTransaksi) AS Tanggal,
		IS_NULL(COUNT(ID),0) AS JMLTopUp,
		IS_NULL(SUM(JumlahRupiah),0) AS TotalTopUp
	FROM tbl_member_deposit_topup_log
	WHERE MONTH(WaktuTransaksi)=$bulan AND YEAR(WaktuTransaksi)=$tahun 
		AND PetugasTopUp=$userdata[user_id]
	GROUP BY DATE(WaktuTransaksi)
	ORDER BY DATE(WaktuTransaksi) ";

if (!$result_topup = $db->sql_query($sql)){
	echo("Error: ".__LINE__);exit;
}

$data_topup = $db->sql_fetchrow($result_topup);
	
//QUERY PAKET
$sql=
	"SELECT 
		WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzet
	FROM tbl_paket
	WHERE MONTH(WaktuPesan)=$bulan 
		AND YEAR(WaktuPesan)=$tahun 
		AND CetakTiket=1 
		AND FlagBatal!=1
		AND JenisPembayaran = 0
		AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual=$userdata[user_id],PetugasPemberi=$userdata[user_id])
	GROUP BY DATE(WaktuPesan)
	ORDER BY DATE(WaktuPesan) ";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//QUERY BIAYA
$sql="SELECT
		WEEKDAY(TglTransaksi)+1 AS Hari,DAY(TglTransaksi) AS Tanggal,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE MONTH(TglTransaksi)=$bulan
		AND YEAR(TglTransaksi)=$tahun
		AND IdPetugas=$userdata[user_id]
		AND FlagJenisBiaya IN (1,2)
	GROUP BY TglTransaksi
	ORDER BY TglTransaksi ";

if ($result_biaya = $db->sql_query($sql)){
	$data_biaya = $db->sql_fetchrow($result_biaya);
}
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}


$sql = "SELECT WEEKDAY(TglCetak)+1 AS Hari,DAY(TglCetak) AS Tanggal, SUM(Jumlah) as biayatambahan
	  	FROM tbl_biaya_tambahan
		WHERE MONTH(TglCetak)='$bulan' AND YEAR(TglCetak)='$tahun' AND IdPencetak='$userdata[user_id]'
		GROUP BY Tanggal";

if ($result_tambahan = $db->sql_query($sql)){
	$data_tambahan = $db->sql_fetchrow($result_tambahan);
}
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

/*$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount,
		$sql_total_biaya AS TotalBiaya,
		IS_NULL(SUM(SubTotal),0)-$sql_total_biaya AS TotalProfit
	FROM tbl_reservasi
	WHERE MONTH(WaktuCetakTiket)=$bulan AND YEAR(WaktuCetakTiket)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket)";*/

	
$sum_penumpang						= 0;
$sum_paket								= 0;
$sum_omzet_penumpang			= 0;
$sum_omzet_paket					= 0;
$sum_topup							= 0;
$sum_member							= 0;
$sum_discount							= 0;
$sum_pendapatan_penumpang	= 0;
$sum_biaya								= 0;
$sum_setor								= 0;

for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$odd ='odd';
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	if (($idx_tgl % 2)==0){
			$odd = 'even';
	}
	
	if($idx_str_hari!=7){
		$font_color	= "000000";
	}
	else{
		$font_color = "ffffff";
		$odd	='red';
	}
	
	if($idx_tgl+1==$tgl_hari_sekarang){
		$font_color = "ffffff";
		$odd	='blue';
	}

	$tanggal_mulai = ($idx_tgl+1)."-".$bulan."-".$tahun;
	
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_penumpang				= $data_penumpang['TotalPenumpang']; 
		$total_omzet_penumpang	= $data_penumpang['TotalOmzet']; 
		$total_discount					= $data_penumpang['TotalDiscount'];
		$pendapatan_penumpang		= $total_omzet_penumpang-$total_discount;
		$total_setor						= $pendapatan_penumpang;
		$data_penumpang 				= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_penumpang				= 0;
		$total_omzet_penumpang	= 0;
		$total_discount					= 0;
		$total_setor						= 0;
		$pendapatan_penumpang		= 0;
	}
	
	//OMZET TOPUP
	if($data_topup['Tanggal']==$idx_tgl+1){
		$total_topup	= $data_topup['TotalTopUp'];
		$total_member 	= $data_topup['JMLTopUp'];
		$total_setor	+= $total_topup;
		$data_topup 	= $db->sql_fetchrow($result_topup);
	}
	else{
		$total_topup				= 0;
		$total_member		= 0;
	}
	
	//OMZET PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_omzet_paket	= $data_paket['TotalOmzet'];
		$total_setor				+= $total_omzet_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket				= 0;
		$total_omzet_paket	= 0;
	}

	//OMZET BIAYA
	if($data_biaya['Tanggal']==$idx_tgl+1){
		$total_biaya		= $data_biaya['TotalBiaya'];
		$data_biaya 		= $db->sql_fetchrow($result_biaya);
		$total_setor		-= $total_biaya;
	}
	else{
		$total_biaya		= 0;
	}

	if($data_tambahan['Tanggal']==$idx_tgl+1){
		$total_tambahan 		= $data_tambahan['biayatambahan'];
		$data_tambahan =		 $db->sql_fetchrow($result_tambahan);
		$total_setor		-= $total_tambahan;
	}else{
		$total_tambahan = 0;
	}
	//echo "tanggal ".$data_tambahan['Tanggal']." idx tgl ".($idx_tgl+1)."biaya_tambahan ".$total_tambahan."<br>";
	$sum_penumpang				+= $total_penumpang;
	$sum_paket						+= $total_paket;
	$sum_omzet_penumpang	+= $total_omzet_penumpang;
	$sum_omzet_paket			+= $total_omzet_paket;
	$sum_topup						+= $total_topup;
	$sum_member					+= $total_member;
	$sum_biaya						+= $total_biaya;
	$sum_total_biaya						+= $total_biaya;
	$sum_total_tambahan						+= $total_tambahan;
	$sum_discount					+= $total_discount;
	$sum_pendapatan_penumpang	+= $pendapatan_penumpang ;
	$sum_setor						+= $total_setor;

	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_rekap_tiket_detail_user.php?p0='.$tanggal_mulai.'&p1='.$tanggal_akhir.'&p2='.$userdata['user_id'].'&p3=3')."\");return false'>Detail<a/>&nbsp;+&nbsp;";		
	
	$act 	.="<a href='#' onClick='Start(\"".append_sid("laporan_rekap_uang_user_cetak_struk.php?p0=$tahun-$bulan-".($idx_tgl+1))."\");return false'>Cetak<a/>";		

	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'font_color'=>$font_color,
				'tgl'=>$tgl_transaksi,
				'total_penumpang'=>number_format($total_penumpang,0,",","."),
				'total_omzet_penumpang'=>number_format($total_omzet_penumpang,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
				'pendapatan_penumpang'=>number_format($pendapatan_penumpang,0,",","."),
				'total_asuransi'=>number_format($total_member,0,",","."),
				'total_paket'=>number_format($total_paket,0,",","."),
				'total_omzet_paket'=>number_format($total_omzet_paket,0,",","."),
				'total_omzet_asuransi'=>number_format($total_topup,0,",","."),
				'total_biaya'=>number_format($total_biaya,0,",","."),
				'total_biaya_tambahan'=>number_format($total_tambahan,0,",","."),
				'total_profit'=>number_format($total_setor,0,",","."),
				'act'=>$act
			)
		);
		
	$temp_hari++;
}
			


//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$bulan."&p2=".$tahun;
	
$script_cetak_pdf="Start('laporan_rekap_uang_user_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_rekap_uang_user_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('laporan_rekap_uang_user.'.$phpEx).'">Laporan Rekap Uang</a>',
	'URL'						=> append_sid('laporan_rekap_uang_user.php'.$parameter),
	'LIST_BULAN'		=> "| ".$list_bulan,
	'BULAN'					=> $bulan,
	'TAHUN'					=> $tahun,
	'SUM_PENUMPANG'	=>number_format($sum_penumpang,0,",","."),
	'SUM_OMZET_PENUMPANG'=>number_format($sum_omzet_penumpang,0,",","."),
	'SUM_DISCOUNT'	=>number_format($sum_discount,0,",","."),
	'SUM_PENDAPATAN_PENUMPANG'=>number_format($sum_pendapatan_penumpang,0,",","."),
	'SUM_ASURANSI'=>number_format($sum_member,0,",","."),
	'SUM_PAKET'			=>number_format($sum_paket,0,",","."),
	'SUM_OMZET_PAKET'=>number_format($sum_omzet_paket,0,",","."),
	'SUM_OMZET_ASURANSI'=>number_format($sum_topup,0,",","."),
	'SUM_BIAYA'			=>number_format($sum_biaya,0,",","."),
	'SUM_BIAYA_TAMBAHAN'			=>number_format($sum_total_tambahan,0,",","."),
	'SUM_PROFIT'			=>number_format($sum_setor,0,",","."),
	'U_laporan_rekap_uang_user_GRAFIK'	=>append_sid('laporan_rekap_uang_user_grafik.'.$phpEx).'&bulan='.$bulan.'&tahun='.$tahun,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>