<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 306;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$tgl_acuan		= isset($HTTP_GET_VARS['p7'])? $HTTP_GET_VARS['p7'] : $HTTP_POST_VARS['p7'];
$username				= $userdata['username'];

//INISIALISASI

if($tgl_acuan == 0){
	$label = "Berdasarkan Tanggal Berangkat";
	$tgl_acuan = 'TglBerangkat';
}else{
	$label = "Berdasarkan Waktu Cetak Tiket";
	$tgl_acuan = 'DATE(WaktuCetakTiket)';
}
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cabang	=($kode_cabang=="")?"":" AND tmk.KodeCabang='$kode_cabang' ";
		
$kondisi_cabang_spv	= $userdata['user_level']!=$USER_LEVEL_INDEX["SUPERVISOR"]?"":" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";
		
$kondisi_cari	=($cari=="")?
	" WHERE KodeKendaraan LIKE '%'":
	" WHERE (KodeKendaraan LIKE '$cari%')";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tmk.KodeKendaraan":$sort_by;

//MENGAMBIL BIAYA-BIAYA DARI TABEL BIAYA
$sql =
	"SELECT NoPolisi,IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE (TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	GROUP BY NoPolisi";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalBiaya"]	= $row["TotalBiaya"];
}

//MANGAMBIL BIAYA TAMBAHAN
$sql = "SELECT KodeKendaraan,IS_NULL(SUM(Jumlah),0) AS TotalBiayaTambahan
		FROM tbl_biaya_tambahan
		WHERE (DATE (TglCetak)BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		GROUP BY KodeKendaraan";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}
while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalBiayaTambahan"]	= $row["TotalBiayaTambahan"];
}

//MENGAMBIL JUMLAH JALAN DARI SPJ
$sql =
	"SELECT
		NoPolisi,IS_NULL(COUNT(1),0) AS TotalJalan,
		COUNT(DISTINCT(TglBerangkat)) AS TotalHariKerja,
		JumlahKursiDisediakan, JumlahPenumpang
	FROM tbl_spj
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	GROUP BY NoPolisi";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalJalan"]			= $row["TotalJalan"];
	$data_laporan[$row[0]]["TotalHariKerja"]	= $row["TotalHariKerja"];
	$data_laporan[$row[0]]["JumlahKursiDisediakan"]	= $row["JumlahKursiDisediakan"];
	$data_laporan[$row[0]]["JumlahPenumpang"]	= $row["JumlahPenumpang"];
}

//MENGAMBIL JUMLAH PENUMPANG
$sql =
	"SELECT KodeKendaraan,
		IS_NULL(COUNT(1),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet
	FROM tbl_reservasi
	WHERE ($tgl_acuan BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND FlagBatal!=1
	GROUP BY KodeKendaraan";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalPenumpang"]	= $row["TotalPenumpang"];
	$data_laporan[$row[0]]["TotalOmzet"]			= $row["TotalOmzet"];
}
//MENGAMBIL JUMLAH PAKET
$sql ="SELECT KodeKendaraan,
		IS_NULL(COUNT(1),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
		FROM tbl_paket
		WHERE ($tgl_acuan BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		 AND FlagBatal!=1
		GROUP BY KodeKendaraan";

if(!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while($row=$db->sql_fetchrow($result)){
	$data_laporan[$row[0]]["TotalPaket"]	= $row["TotalPaket"];
	$data_laporan[$row[0]]["TotalOmzetPaket"]	= $row["TotalOmzetPaket"];
}

//MENGAMBIL DATA KENDARAAN
$sql	=
	"SELECT
		KodeKendaraan,f_cabang_get_name_by_kode(KodeCabang) AS Cabang,NoPolisi,
		JumlahKursi AS LayoutKursi
	FROM tbl_md_kendaraan $kondisi_cari AND FlagAktif = 1";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//PLOT KE ARRAY PENAMPUNGAN
$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result)){

	$temp_array[$idx]['KodeKendaraan']	= $row['KodeKendaraan'];
	$temp_array[$idx]['NoPolisi']		= $row['NoPolisi'];
	$temp_array[$idx]['LayoutKursi']	= $row['LayoutKursi'];
	$temp_array[$idx]['Okupansi']		= @($data_laporan[$row['KodeKendaraan']]['JumlahPenumpang']/$data_laporan[$row['KodeKendaraan']]['JumlahKursiDisediakan'])*100;
	$temp_array[$idx]['Cabang']			= $row['Cabang'];
	$temp_array[$idx]['TotalJalan']		= $data_laporan[$row['KodeKendaraan']]['TotalJalan'];
	$temp_array[$idx]['TotalPenumpang']	= $data_laporan[$row['KodeKendaraan']]['TotalPenumpang'];
	$temp_array[$idx]['TotalOmzet']		= $data_laporan[$row['KodeKendaraan']]['TotalOmzet'];
	$temp_array[$idx]['TotalPaket']		= $data_laporan[$row['KodeKendaraan']]['TotalPaket'];
	$temp_array[$idx]['TotalOmzetPaket']= $data_laporan[$row['KodeKendaraan']]['TotalOmzetPaket'];
	$temp_array[$idx]['TotalBiaya']		= $data_laporan[$row['KodeKendaraan']]['TotalBiaya'];
	$temp_array[$idx]['TotalBiayaTambahan'] = $data_laporan[$row['KodeKendaraan']]['TotalBiayaTambahan'];
	$temp_array[$idx]['TotalProfit']	= $data_laporan[$row['KodeKendaraan']]['TotalOmzet']+$data_laporan[$row['KodeKendaraan']]['TotalOmzetPaket']-$data_laporan[$row['KodeKendaraan']]['TotalBiaya']-$data_laporan[$row['KodeKendaraan']]['TotalBiayaTambahan'];

	$idx++;
}



//EXPORT KE MS-EXCEL
	
	//if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Kendaraan per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir." ".$label);
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'No. Polisi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Seat');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Cabang');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Jalan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jum.Pnp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Omzet Tiket');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jum. Paket');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Omzet Paket');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J3', 'OP Langsung');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Biaya Tambahan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('L3', 'L/R Kotor');
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Okupansi (%)');
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
		$idx=0;
		
		while ($idx<count($temp_array)){

			$idx_row=$idx+4;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['NoPolisi']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, substr($temp_array[$idx]['LayoutKursi'],0,2));
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['Cabang']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($temp_array[$idx]['TotalJalan'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($temp_array[$idx]['TotalPenumpang'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($temp_array[$idx]['TotalOmzet'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($temp_array[$idx]['TotalPaket'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($temp_array[$idx]['TotalOmzetPaket'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($temp_array[$idx]['TotalBiaya'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($temp_array[$idx]['TotalBiayaTambahan'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($temp_array[$idx]['TotalProfit'],0,".",""));
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($temp_array[$idx]['Okupansi'],0,".",""));
			$idx++;
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K4:K'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, '=SUM(L4:L'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		//if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Laporan Omzet Kendaraan per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		//}
	/*}
	else{
		die_error('Err:',__LINE__);
	}   */
  
  
?>
