<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'chartphp_dist.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassTargetCabang.php');

// SESSION
$id_page = 215;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$cabang		    = isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tgl1'])? $HTTP_GET_VARS['tgl1'] : $HTTP_POST_VARS['tgl1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tgl2'])? $HTTP_GET_VARS['tgl2'] : $HTTP_POST_VARS['tgl2'];
$nama           = isset($HTTP_GET_VARS['nama'])? $HTTP_GET_VARS['nama'] : $HTTP_POST_VARS['nama'];

$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$page_title = "General Total Target Cabang";
/*MENGAMBIL DATA TARGET CABANG DARI TABEL TARGET*/
$bulan1 = date_format(date_create($tanggal_mulai),'n');
$bulan2 = date_format(date_create($tanggal_akhir),'n');
$tahun1 = date_format(date_create($tanggal_mulai),'Y');
$tahun2 = date_format(date_create($tanggal_akhir),'Y');
$sql ="SELECT tbl_md_cabang_target.KodeCabang,IS_NULL(SUM(TargetPax),0) AS TargetPax,IS_NULL(SUM(TargetPaket),0) AS TargetPaket
       FROM tbl_md_cabang_target
       WHERE BulanTarget BETWEEN $bulan1 AND $bulan2 AND TahunTarget BETWEEN $tahun1 AND $tahun2 AND KodeCabang='$cabang'
       ORDER BY KodeCabang ASC";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".__LINE__."<br>");
    die(mysql_error());
}
$row=$db->sql_fetchrow($result);

$temp = $row['TargetPax'];
$coba = $row['TargetPaket'];
$coba_z = $coba/6;
$temp_z = $temp/6;
$temp_array_tiket = "0,";
$temp_array_paket = "0,";
$i=1;
while($i<=6){
    $temp_x = $temp_x+$temp_z;
    $temp_array_tiket = $temp_array_tiket .= ceil($temp_x).",";

    $coba_x = $coba_x + $coba_z;
    $temp_array_paket = $temp_array_paket .= ceil($coba_x).",";
    $i++;
}

/*MENGAMBIL JUMLAH TIKET YANG DIBELI DI COUNTER DIJAKARTA */
$sql="SELECT tbl_reservasi.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta
      FROM tbl_reservasi
      WHERE (DATE (TglBerangkat) BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
      AND CetakTiket = 1 AND FlagBatal != 1 AND KodeCabang = '$cabang'
      GROUP BY KodeCabang";

if (!$result = $db->sql_query($sql)){
    echo("Error:".__LINE__."<br>");
    die(mysql_error());
}
while($row = $db->sql_fetchrow($result)){
    $jmlFromJakarta = $row['JmlFromJakarta'];
}

/*MENGAMBIL JUMLAH TIKET YANG TUJUANYA KE JAKARTA*/
$sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan
        FROM tbl_reservasi
        JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
        WHERE (DATE (TglBerangkat) BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
        AND FlagBatal != 1 AND CetakTiket = 1 AND KodeCabangTujuan = '$cabang'
        GROUP BY KodeCabangTujuan";
if (!$result = $db->sql_query($sql)){
    echo("Error:".__LINE__."<br>");
    die(mysql_error());
}
while($row = $db->sql_fetchrow($result)){
    $jmlToJakarta = $row['JmlFromBandungToJakarta'];
}

/*MENGAMBIL JUMLAH PAKET DARI COUNTER JAKARTA*/
$sql="SELECT tbl_paket.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta
      FROM tbl_paket
      WHERE (DATE (TglBerangkat) BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
      AND CetakTiket = 1 AND FlagBatal != 1 AND KodeCabang = '$cabang'
      GROUP BY KodeCabang";

if (!$result = $db->sql_query($sql)){
    echo("Error:".__LINE__."<br>");
    die(mysql_error());
}
while($row = $db->sql_fetchrow($result)){
    $TiketFromJakarta = $row['JmlFromJakarta'];
}


/*MENGAMBIL JUMLAH PAKET YANG TUJUANYA KE JAKARTA*/
$sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan
        FROM tbl_paket
        JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
        WHERE (DATE (TglBerangkat) BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
        AND FlagBatal != 1 AND CetakTiket = 1 AND KodeCabangTujuan = '$cabang'
        GROUP BY KodeCabangTujuan";
if (!$result = $db->sql_query($sql)){
    echo("Error:".__LINE__."<br>");
    die(mysql_error());
}
while($row = $db->sql_fetchrow($result)){
    $TiketToJakarta = $row['JmlFromBandungToJakarta'];
}
// LIST
$template->set_filenames(array('body' => 'generaltotal/grafik.tpl'));

$template->assign_vars(array(
    'BCRUMP'=>setBcrump($id_page),
    'CAPAIANTIKET' =>$jmlFromJakarta+$jmlToJakarta,
    'CAPAIANPAKET'=>$TiketFromJakarta+$TiketToJakartai,
    'INTERVALTIKET'=>rtrim($temp_array_tiket,","),
    'INTERVALPAKET'=>rtrim($temp_array_paket,","),
    'Cabang'=>$nama,
    'Tanggal'=>dateparse($tanggal_mulai)." s/d ".dateparse($tanggal_akhir)
    )
);
    include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
