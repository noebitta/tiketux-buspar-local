<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 307;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//################################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$username				= $userdata['username'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_sopir/laporan_omzet_sopir_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cabang	= $userdata['user_level']!=$USER_LEVEL_INDEX["SUPERVISOR"]?"":" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";

$kondisi_cari	=($cari=="")?
	" WHERE tms.KodeSopir LIKE '%' $kondisi_cabang":
	" WHERE (tms.KodeSopir LIKE '$cari%' OR tms.Nama LIKE '%$cari%') $kondisi_cabang";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tms.KodeSopir":$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData(
		$idx_page,"tms.KodeSopir","tbl_md_sopir tms",
		"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
		$kondisi_cari,"laporan_omzet_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	= 
	"SELECT 
		tms.KodeSopir,tms.Nama,
		COUNT(DISTINCT(NoSPJ)) AS Jalan,
		COUNT(DISTINCT(TglBerangkat)) AS TotalHariKerja,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		f_sopir_get_insentif(tms.KodeSopir,'$tanggal_mulai_mysql','$tanggal_akhir_mysql') AS TotalInsentif
	FROM tbl_md_sopir tms  LEFT JOIN tbl_reservasi tro ON tms.KodeSopir=tro.KodeSopir
		AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 AND CetakSPJ=1 ".
		$kondisi_cari."
	GROUP BY tms.KodeSopir
	ORDER BY $sort_by $order
	LIMIT $idx_page,$VIEW_PER_PAGE";
	
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__ );exit;
}

$i = $idx_page*$VIEW_PER_PAGE+1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
		
	$act	="<a href='".append_sid('laporan_omzet_sopir_grafik.php?kode_driver='.$row['KodeSopir'].'&bulan='.$bulan.'&tahun='.$tahun.
		'&tanggal_mulai='.$tanggal_mulai.'&tanggal_mulai='.$tanggal_akhir.'&cabang='.$kode_cabang.'&sort_by='.$sort_by.'&order='.$order)."'>Grafik<a/>";
		
	$template->assign_block_vars(
		'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'nama'=>$row['Nama'],
				'nrp'=>$row['KodeSopir'],
				'total_hari_kerja'=>number_format($row['TotalHariKerja'],0,",","."),
				'total_jalan'=>number_format($row['Jalan'],0,",","."),
				'total_penumpang'=>number_format($row['TotalPenumpang'],0,",","."),
				'total_omzet'=>number_format($row['TotalOmzet'],0,",","."),
				'total_insentif'=>number_format($row['TotalInsentif'],0,",","."),
				'act'=>$act
			)
	);
		
	$i++;
}


//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$kode_cabang.
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
	
$script_cetak_pdf="Start('laporan_omzet_sopir_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_sopir_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
	

$page_title = "Penjualan/Sopir";

$template->assign_vars(array(
	'BCRUMP'    	=>setBcrump($id_page),
	'ACTION_CARI'	=> append_sid('laporan_omzet_sopir.'.$phpEx),
	'TXT_CARI'	=> $cari,
	'TGL_AWAL'	=> $tanggal_mulai,
	'TGL_AKHIR'	=> $tanggal_akhir,
	'NAMA'		=> $userdata['nama'],
	'SUMMARY'	=> $summary,
	'PAGING'	=> $paging,
	'CETAK_PDF'	=> $script_cetak_pdf,
	'CETAK_XL'	=> $script_cetak_excel,
	'A_SORT_1'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_1'	=> "Urutkan Nama Sopir ($order_invert)",
	'A_SORT_2'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=KodeSopir'.$parameter_sorting),
	'TIPS_SORT_2'	=> "Urutkan NRP ($order_invert)",
	'A_SORT_3'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=TotalHariKerja'.$parameter_sorting),
	'TIPS_SORT_3'	=> "Urutkan Total Hari Kerja ($order_invert)",
	'A_SORT_4'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=Jalan'.$parameter_sorting),
	'TIPS_SORT_4'	=> "Urutkan Total trip ($order_invert)",
	'A_SORT_5'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=TotalPenumpang'.$parameter_sorting),
	'TIPS_SORT_5'	=> "Urutkan Total penumpang ($order_invert)",
	'A_SORT_6'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=TotalOmzet'.$parameter_sorting),
	'TIPS_SORT_6'	=> "Urutkan Total Omzet ($order_invert)",
	'A_SORT_7'	=> append_sid('laporan_omzet_sopir.'.$phpEx.'?sort_by=TotalInsentif'.$parameter_sorting),
	'TIPS_SORT_7'	=> "Urutkan Total Insentif ($order_invert)"

	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>