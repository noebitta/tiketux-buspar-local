<?php

define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 312;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);


require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';


// mengambil jurusan yang berasal dari jakarta
$sql = 'SELECT
            IdJurusan AS IdJurusanJakarta,
            KodeJurusan,
            f_cabang_get_name_by_kode (KodeCabangAsal) AS DariJakarta,
            f_cabang_get_name_by_kode (KodeCabangTujuan) AS KeBandung,
            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanBandung,
            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS DariBandung,
            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakarta,
            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanPasteur,
            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS DariPasteur,
            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakartaDariPasteur
            
        FROM
            tbl_md_jurusan j
        WHERE
            (
                SELECT
                    Kota
                FROM
                    tbl_md_cabang
                WHERE
                    KodeCabang = KodeCabangAsal
            ) = "JAKARTA"
        ORDER BY
            IdJurusan';

if (!$jurusan_jakarta = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

//=======================================TIKET==============================================
//DATA PENJUALAN TIKET
$sql	= "SELECT 
                IdJurusan,
                IF(COUNT(NoTiket) < 0,0,COUNT(NoTiket)) AS PAX 
            FROM tbl_reservasi
            WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
                AND FlagBatal!=1 AND CetakTiket = 1
            GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $data_tiket_total[$row['IdJurusan']]	= $row;
}

//=======================================PAKET==============================================
//DATA PENJUALAN PAKET
$sql	= "SELECT 
                IdJurusan,
                IF(COUNT(NoTiket) < 0,0,COUNT(NoTiket)) AS PAKET 
            FROM tbl_paket
            WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
                AND FlagBatal!=1 AND CetakTiket = 1
            GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $data_paket_total[$row['IdJurusan']]	= $row;
}

//=========================================MANIFEST==============================
//DATA KEBERANGKATAN BY MANIFEST
$sql	=
    "SELECT 
		IdJurusan,
		IF(COUNT(NoSPJ) < 0 ,0 , COUNT(NoSPJ)) AS TRIP
	FROM tbl_spj
	WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
	GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $jumlah_berangkat[$row['IdJurusan']]	= $row;
}

//=======================================UNIT=====================================
//DATA UNIT YANG BERANGKAT BY MANIFEST
$sql	=
    "SELECT 
		IdJurusan,
		IF(COUNT(DISTINCT(NoPolisi)) < 0 , 0 ,COUNT(DISTINCT(NoPolisi))) AS UNIT
	FROM tbl_spj
	WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
	GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}
while ($row = $db->sql_fetchrow($result)){
    $jumlah_unit[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$sumpax=0;
$sumpaket=0;
$sumtrip=0;
$sumunit=0;
$idx=0;

while ($row = $db->sql_fetchrow($jurusan_jakarta)){

    if($row['IdJurusanBandung'] == $row['IdJurusanPasteur']){
        $counter_pasteur = "";
        $id_pasteur      = 0;
        $nama_pasteur    = "";
    }else{
        $counter_pasteur = $row['DariPasteur'];
        $id_pasteur      = $row['IdJurusanPasteur'];
        $nama_pasteur    = $row['DariPasteur']."->".$row['KeJakartaDariPasteur'];
    }

    $temp_array[$idx]['Counter']		= $row['DariJakarta'];
    $temp_array[$idx]['jakarta']		= $row['DariJakarta']."->".$row['KeBandung'];
    $temp_array[$idx]['bandung']        = $row['DariBandung']."->".$row['KeJakarta'];
    $temp_array[$idx]['pasteur']        = $nama_pasteur;
    $temp_array[$idx]['shortname']      = $row['DariJakarta']."-".$row['DariBandung']."-".$counter_pasteur;
    $temp_array[$idx]['pax']            = $data_tiket_total[$row['IdJurusanJakarta']]['PAX']+$data_tiket_total[$row['IdJurusanBandung']]['PAX']+$data_tiket_total[$id_pasteur]['PAX'];
    $temp_array[$idx]['paket']          = $data_paket_total[$row['IdJurusanJakarta']]['PAKET']+$data_paket_total[$row['IdJurusanBandung']]['PAKET']+$data_paket_total[$id_pasteur]['PAKET'];
    $temp_array[$idx]['trip']           = $jumlah_berangkat[$row['IdJurusanJakarta']]['TRIP']+$jumlah_berangkat[$row['IdJurusanBandung']]['TRIP']+$jumlah_berangkat[$id_pasteur]['TRIP'];
    $temp_array[$idx]['unit']           = $jumlah_unit[$row['IdJurusanJakarta']]['UNIT']+$jumlah_unit[$row['IdJurusanBandung']]['UNIT']+$jumlah_unit[$id_pasteur]['UNIT'];

    $sumpax += $temp_array[$idx]['pax'];
    $sumpaket += $temp_array[$idx]['paket'];
    $sumtrip += $temp_array[$idx]['trip'];
    $sumunit += $temp_array[$idx]['unit'];

    $counter_pasteur = "";
    $id_pasteur = 0;
    $nama_pasteur = "";

    $idx++;
}

//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Route Tanggal '.date_format(date_create($tanggal_mulai),'d F Y').' s/d '.date_format(date_create($tanggal_akhir),'d F Y'));
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'COUNTER');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
$objPHPExcel->getActiveSheet()->mergeCells('C3:F3');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'ROUTE');
$objPHPExcel->getActiveSheet()->mergeCells('G3:J3');
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'RINCIAN');
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Jakarta');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('40');
$objPHPExcel->getActiveSheet()->mergeCells('D4:E4');
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Bandung');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('40');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'SHORT NAME');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('50');
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Pax');
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Paket');
$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Trip');
$objPHPExcel->getActiveSheet()->setCellValue('J4', 'Unit');

$idx=0;

while ($idx<count($temp_array)){

    $idx_row=$idx+5;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Counter']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['jakarta']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['bandung']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['pasteur']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['shortname']);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['pax']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $temp_array[$idx]['paket']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $temp_array[$idx]['trip']);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $temp_array[$idx]['unit']);

    $idx++;
}


$temp_idx=$idx_row;

$idx_row++;

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':F'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Total');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');


$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);

for($col = 'A'; $col !== 'K'; $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.'3:'.$col.$idx_row)->applyFromArray($styleArray);
}

$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font'  => array(
        'bold'  => true,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '#48616c')
    )
);

for($col = 'A'; $col !== 'K'; $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.'3:'.$col.'4')->applyFromArray($style);
}

$style = array(
    'font'  => array(
        'bold'  => true,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '#48616c')
    )
);

for($col = 'A'; $col !== 'K'; $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.$idx_row.':'.$col.$idx_row)->applyFromArray($style);
}


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Rekap Route Tanggal '.date_format(date_create($tanggal_mulai),'d F Y').' s/d '.date_format(date_create($tanggal_akhir),'d F Y').'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>