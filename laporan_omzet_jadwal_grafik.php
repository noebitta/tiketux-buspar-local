<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include_once ($adp_root_path. 'chart/php-ofc-library/open_flash_chart_object.php');

// SESSION
$id_page = 304;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	$list_bulan	.="<a href='#' onClick='setGrafik($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}


// LIST
$parameter	= "%26tanggal_mulai=".$tanggal_mulai."%26tanggal_akhir=".$tanggal_akhir."%26asal=".$asal."%26tujuan=".$tujuan;

$template->set_filenames(array('body' => 'laporan_omzet_jadwal/laporan_omzet_jadwal_grafik_body.tpl')); 

$page_title	= "Grafik Penjualan/Jam";

$template->assign_vars(array(
	'BCRUMP'    						=>setBcrump($id_page),
	'LIST_BULAN'						=> "| ".$list_bulan,
	'TAHUN'									=> $tahun,
	'URL'										=> append_sid('laporan_omzet_jadwal_grafik.'.$phpEx),
	'DATA_GRAFIK_PER_JAM'		=> append_sid('laporan_omzet_jadwal_grafik_data.php').$parameter
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>