<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tanggal		= $HTTP_GET_VARS['p0'];
	
//INISIALISASI
$PengaturanUmum	= new PengaturanUmum();
$User						= new User();
$useraktif			= $userdata['user_id'];
$data_user			= $User->ambilDataDetail($useraktif);


$kondisi_tiket	=
	"DATE(WaktuCetakTiket)='$tanggal'
	AND CetakTiket=1 
	AND FlagBatal!=1 
	AND PetugasCetakTiket=$useraktif";

$kondisi_paket	=
	"DATE(WaktuPesan)='$tanggal'
	AND CetakTiket=1 
	AND FlagBatal!=1
	AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual=$useraktif,PetugasPemberi=$useraktif)";

	
$temp_tanggal_cari	= explode(" ",$tanggal);
$temp_tanggal_cari	= explode("-",$temp_tanggal_cari[0]);
$tahun_cari		= $temp_tanggal_cari[0];
$bulan_cari		= $temp_tanggal_cari[1];

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];

if($tahun_cari==$tahun_sekarang && $bulan_cari==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi";
}
	
//QUERY PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(WaktuCetakTiket)+1 AS Hari,DAY(WaktuCetakTiket) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM $tbl_reservasi
	WHERE $kondisi_tiket
	GROUP BY DATE(WaktuCetakTiket)
	ORDER BY DATE(WaktuCetakTiket) ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$total_penumpang				= $data_penumpang['TotalPenumpang']; 
$total_omzet_penumpang	= $data_penumpang['TotalOmzet']; 
$total_discount					= $data_penumpang['TotalDiscount'];
$pendapatan_penumpang		= $total_omzet_penumpang-$total_discount;
$total_profit						= $pendapatan_penumpang;
	
//QUERY PAKET
$sql=
	"SELECT 
		WEEKDAY(WaktuPesan)+1 AS Hari,DAY(WaktuPesan) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzet
	FROM tbl_paket
	WHERE $kondisi_paket
	GROUP BY DATE(WaktuPesan)
	ORDER BY DATE(WaktuPesan) ";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$total_paket				= $data_paket['TotalPaket']; 
$total_omzet_paket	= $data_paket['TotalOmzet'];
$total_profit				+= $total_omzet_paket;

//QUERY BIAYA
$sql=
	"SELECT 
		WEEKDAY(TglTransaksi)+1 AS Hari,DAY(TglTransaksi) AS Tanggal,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		DATE(TglTransaksi)='$tanggal'
		AND IdPetugas=$useraktif
	GROUP BY TglTransaksi
	ORDER BY TglTransaksi ";

if ($result_biaya = $db->sql_query($sql)){
	$data_biaya = $db->sql_fetchrow($result_biaya);
} 
else{
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

$total_biaya		= $data_biaya['TotalBiaya']; 
$total_profit		-= $total_biaya;

//mengambil total pendapatan dari asuransi
$Asuransi	= new Asuransi();
$total_pendapatan_asuransi	= $Asuransi->ambilTotalPendapatanAsuransi($tanggal,$tanggal,$useraktif);

$data_perusahaan	= $PengaturanUmum->ambilDataPerusahaan();
$line_space	=0.3;

//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
	
	var $javascript;
	var $n_js;

	function IncludeJS($script) {
	    $this->javascript=$script;
	}

	function _putjavascript() {
	    $this->_newobj();
	    $this->n_js=$this->n;
	    $this->_out('<<');
	    $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
	    $this->_out('>>');
	    $this->_out('endobj');
	    $this->_newobj();
	    $this->_out('<<');
	    $this->_out('/S /JavaScript');
	    $this->_out('/JS '.$this->_textstring($this->javascript));
	    $this->_out('>>');
	    $this->_out('endobj');
	}

	function _putresources() {
	    parent::_putresources();
	    if (!empty($this->javascript)) {
	        $this->_putjavascript();
	    }
	}

	function _putcatalog() {
	    parent::_putcatalog();
	    if (isset($this->javascript)) {
	        $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
	    }
	}
	
	function AutoPrint($dialog=false)
	{
	    //Embed some JavaScript to show the print dialog or start printing immediately
	    $param=($dialog ? 'true' : 'false');
	    $script="print($param);";
	    $this->IncludeJS($script);
	}
}
			
//set kertas & file
$pdf=new PDF('P','cm','spjkecil');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(0,0,0,0);
$pdf->SetFont('courier','',10);
		
// Header
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('courier','',13);
$pdf->Cell(6.4,$line_space,"REKAP SETORAN",'',0,'C');$pdf->Ln();$pdf->Ln();
$pdf->SetFont('courier','',11);
$pdf->Cell(6.4,$line_space,$data_perusahaan['NamaPerusahaan'],'',0,'');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(6.4,$line_space,$data_perusahaan['AlamatPerusahaan'],'',0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,$data_perusahaan['TelpPerusahaan'],'',0,'');$pdf->Ln();

$pdf->SetFont('courier','',10);
//content
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,"CSO    :".$data_user['nama'],'',0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,"Tgl.Trx:".dateparse(FormatMySQLDateToTgl($tanggal)),'',0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();

//QUERY DATA TIKET
$sql=
	"SELECT 
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal(KodeJadwal))) AS Asal,
		f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(f_jadwal_ambil_id_jurusan_by_kode_jadwal(KodeJadwal))) AS Tujuan,
		JamBerangkat,
		COUNT(NoTiket) AS JumlahPenumpang,
		SUM(Total) AS Total,
		IdJurusan
	FROM tbl_reservasi
	WHERE $kondisi_tiket
	GROUP BY KodeJadwal,JamBerangkat
	ORDER BY Asal,Tujuan,JamBerangkat ";

if (!$result_penumpang_detail = $db->sql_query($sql)){
	echo("Error:".__LINE__);exit;
}

//DETAIL DATA TIKET
$pdf->Ln();
$pdf->Cell(6.4,$line_space,'DETAIL TIKET',0,0,'');$pdf->Ln();

$jurusan_temp	="";

$sub_total_per_jurusan	= 0;

while ($data_penumpang_detail = $db->sql_fetchrow($result_penumpang_detail)){
	
	if($jurusan_temp!="" && $jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		$pdf->Cell(1,$line_space,"Sub.Tot: Rp.",'',0,'');$pdf->Cell(5.4,$line_space,number_format($sub_total_per_jurusan,0,",",".").'  ','',0,'R');$pdf->Ln();$pdf->Ln();
		$sub_total_per_jurusan	= 0;
	}
	
	if($jurusan_temp!=$data_penumpang_detail['IdJurusan']){
		$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
		$pdf->Cell(1,$line_space,"Asl:",'',0,'');$pdf->Cell(5,$line_space,$data_penumpang_detail['Asal'],'',0,'L');$pdf->Ln();
		$pdf->Cell(1,$line_space,"Tjn:",'',0,'');$pdf->Cell(5,$line_space,$data_penumpang_detail['Tujuan'],'',0,'L');$pdf->Ln();
		$jurusan_temp	= $data_penumpang_detail['IdJurusan'];
	}
	
	$pdf->Cell(1,$line_space,"Jam:",'',0,'');$pdf->Cell(5.4,$line_space,$data_penumpang_detail['JamBerangkat'],'',0,'L');$pdf->Ln();
	$pdf->Cell(1,$line_space,"Pnp:",'',0,'');$pdf->Cell(5.4,$line_space,$data_penumpang_detail['JumlahPenumpang'],'',0,'L');$pdf->Ln();
	$pdf->Cell(1,$line_space,"Tot: Rp.",'',0,'');$pdf->Cell(5.4,$line_space,number_format($data_penumpang_detail['Total'],0,",",".").'  ','',0,'R');$pdf->Ln();
	$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
	
	$sub_total_per_jurusan += $data_penumpang_detail['Total'];
	
}

if($sub_total_per_jurusan >0){
	$pdf->Cell(1,$line_space,"Sub.Tot: Rp.",'',0,'');$pdf->Cell(5.4,$line_space,number_format($sub_total_per_jurusan,0,",",".").'  ','',0,'R');$pdf->Ln();
}

//QUERY PAKET
$sql=
	"SELECT 
		NoTiket,LEFT(NamaPengirim,20) AS Nama,KodeJadwal,HargaPaket
	FROM tbl_paket
	WHERE $kondisi_paket
	ORDER BY DATE(WaktuPesan) ";

if (!$result_paket_detail = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_rekap_uang_user_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//DETAIL DATA TIKET
$pdf->Ln();
$pdf->Cell(6.4,$line_space,'DETAIL PAKET',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();

while ($data_paket_detail = $db->sql_fetchrow($result_paket_detail)){
	$pdf->Cell(2.4,$line_space,"No.Tiket :",'',0,'');$pdf->Cell(4,$line_space,$data_paket_detail['NoTiket'],'',0,'L');$pdf->Ln();
	$pdf->Cell(2.4,$line_space,"Nama     :",'',0,'');$pdf->Cell(4,$line_space,$data_paket_detail['Nama'],'',0,'L');$pdf->Ln();
	$pdf->Cell(2.4,$line_space,"Kode Jdwl:",'',0,'');$pdf->Cell(4,$line_space,$data_paket_detail['KodeJadwal'],'',0,'L');$pdf->Ln();
	$pdf->Cell(2.4,$line_space,"Total    : Rp.",'',0,'');$pdf->Cell(4,$line_space,number_format($data_paket_detail['HargaPaket'],0,",",".").'  ','',0,'R');$pdf->Ln();
	$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
}

$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'REKAP UANG TIKET',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Jum. pnp :",'',0,'');$pdf->Cell(3,$line_space,number_format($total_penumpang,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Penj. pnp:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_omzet_penumpang,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Jum. disc:",'',0,'');$pdf->Cell(3,$line_space,"-".number_format($total_discount,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Tot. Penj:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_profit,0,",",".").'  ','',0,'R');$pdf->Ln();

$pdf->Ln();
$pdf->Cell(6.4,$line_space,'REKAP UANG PAKET',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Jum. Pkt:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_paket,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Uang Pkt:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_omzet_paket,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Ln();
$pdf->Cell(6.4,$line_space,'REKAP BIAYA',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Total Biaya:",'',0,'');$pdf->Cell(3,$line_space,"-".number_format($total_biaya,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Ln();

/*$pdf->Ln();
$pdf->Cell(6.4,$line_space,'REKAP UANG ASURANSI',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(3.4,$line_space,"Uang Asuransi:",'',0,'');$pdf->Cell(3,$line_space,number_format($total_pendapatan_asuransi,0,",",".").'  ','',0,'R');$pdf->Ln();
$pdf->Ln();
*/

$pdf->Cell(3.4,$line_space,"TOTAL SETORAN  :",'',0,'');$pdf->Cell(3,$line_space,number_format($pendapatan_penumpang+$total_omzet_paket-$total_biaya+$total_pendapatan_asuransi,0,",",".").'  ','',0,'R');$pdf->Ln();

$pdf->Cell(6.4,$line_space,'-----------------------------------------',0,0,'');$pdf->Ln();
$pdf->Cell(6.4,$line_space,"Tgl. Cetak:".dateparseWithTime(FormatMySQLDateToTglWithTime(dateNow(true))),'',0,'');$pdf->Ln();
$pdf->Ln();

//PESAN SPONSOR

$pesan_sponsor	= $PengaturanUmum->ambilPesanUntukDiTiket();

if(strlen($pesan_sponsor)>30){
	$arr_kata	= explode(" ",$pesan_sponsor);
	
	$temp_pesan_sponsor="";
	$jumlah_kata	= count($arr_kata);
	
	$idx	= 0;
	
	while($idx<$jumlah_kata){
		
		if(strlen($temp_pesan_sponsor." ".$arr_kata[$idx])<30){
			$temp_pesan_sponsor	= $temp_pesan_sponsor." ".$arr_kata[$idx];
			$idx++;
		}
		else{
			$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
			$temp_pesan_sponsor	= "";
			
		}
	}
	
	$pdf->Cell(6.4,$line_space,$temp_pesan_sponsor,0,0,'C');$pdf->Ln();
}
else{
	$pdf->Cell(6.4,$line_space,$pesan_sponsor,0,0,'C');$pdf->Ln();
}

$pdf->Cell(6.4,$line_space,"-- Terima Kasih --",0,0,'C');$pdf->Ln();
$pdf->SetFont('courier','',8);
$pdf->Cell(6.4,$line_space,$data_perusahaan['EmailPerusahaan'],0,0,'C');$pdf->Ln();
$pdf->SetFont('courier','',8);
$pdf->Cell(6.4,$line_space,$data_perusahaan['WebSitePerusahaan'],0,0,'C');$pdf->Ln();
$pdf->AutoPrint(true);
$pdf->Output();


?>