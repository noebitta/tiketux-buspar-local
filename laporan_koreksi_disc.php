<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 211;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Jurusan= new Jurusan();
$Cabang	= new Cabang();

function setComboCabangAsal($kota,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->setComboCabang($kota);
	$opt_cabang="<option value='0'>-tampilkan semua-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="<option value='0'>-tampilkan semua-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		//echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboJam($id_jurusan,$jam_dipilih){
	//SET COMBO JAM
	
	global $db;
		
	$sql = 
		"SELECT KodeJadwal,JamBerangkat
		  FROM tbl_md_jadwal tmj
		  WHERE IdJurusan=$id_jurusan 
		  ORDER BY JamBerangkat;";
			
	if (!$result = $db->sql_query($sql)){
		//die_error("Gagal $this->ID_FILE 003");
		#echo("Err: $this->ID_FILE $sql". __LINE__);
	}
	
	$opt_jam="<option value='0'>-tampilkan semua-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($jam_dipilih!=$row['KodeJadwal'])?"":"selected";
			$opt_jam .="<option value='$row[KodeJadwal]' $selected>$row[KodeJadwal] ($row[JamBerangkat])</option>";
		}
	}
	else{
		//echo("Error :".__LINE__);exit;
	}		
	return $opt_jam;
	//END SET COMBO JAM
}

//BODY 
if ($mode=='get_asal'){

	$kota				= $HTTP_GET_VARS['kota'];
	$cabang_asal= $HTTP_GET_VARS['asal'];
	
	$opt_cabang_tujuan=
		"<select id='opt_cabang_asal' name='opt_cabang_asal' onChange='getUpdateTujuan(this.value)'>".
		setComboCabangAsal($kota,$cabang_asal)
		."</select>";
	
	echo($opt_cabang_tujuan);
	
	exit;
}
else if ($mode=='get_tujuan'){
	$cabang_asal		= $HTTP_GET_VARS['asal'];
	$id_jurusan			= $HTTP_GET_VARS['jurusan'];
	
	$opt_cabang_tujuan=
		"<select id='opt_tujuan' name='opt_tujuan' onChange='getUpdateJam(this.value)'>".
		setComboCabangTujuan($cabang_asal,$id_jurusan)
		."</select>";
	
	echo($opt_cabang_tujuan);
	
	exit;
}
else if ($mode=='get_jam'){
	$id_jurusan	= $HTTP_GET_VARS['jurusan'];
	$jam_dipilih= $HTTP_GET_VARS['jam'];
	
	$opt_cabang_jam=
		"<select id='opt_jam' name='opt_jam'>".
		setComboJam($id_jurusan,$jam_dipilih)
		."</select>";
	
	echo($opt_cabang_jam);
	
	exit;
}
else {
	// LIST
	$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
	$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
	$bulan					= isset($HTTP_GET_VARS['opt_bulan'])? $HTTP_GET_VARS['opt_bulan'] : $HTTP_POST_VARS['opt_bulan']; 
	$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun']; 
	$kota_asal			= isset($HTTP_GET_VARS['kota_asal'])? $HTTP_GET_VARS['kota_asal'] : $HTTP_POST_VARS['kota_asal']; 
	$cabang_asal		= isset($HTTP_GET_VARS['opt_cabang_asal'])? $HTTP_GET_VARS['opt_cabang_asal'] : $HTTP_POST_VARS['opt_cabang_asal']; 
	$cabang_tujuan	= isset($HTTP_GET_VARS['opt_tujuan'])? $HTTP_GET_VARS['opt_tujuan'] : $HTTP_POST_VARS['opt_tujuan']; 
	$kode_jadwal		= isset($HTTP_GET_VARS['opt_jam'])? $HTTP_GET_VARS['opt_jam'] : $HTTP_POST_VARS['opt_jam']; 
	$status					= isset($HTTP_GET_VARS['opt_status'])? $HTTP_GET_VARS['opt_status'] : $HTTP_POST_VARS['opt_status']; 
	$is_cari				= isset($HTTP_GET_VARS['is_cari'])? $HTTP_GET_VARS['is_cari'] : $HTTP_POST_VARS['is_cari']; 
	$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 
	
	//pengaturan periode
	if($tgl_awal>$tgl_akhir){
		$temp_tgl		= $tgl_akhir;
		$tgl_akhir	= $tgl_awal;
		$tgl_awal		= $temp_tgl;
	}
	
	$kota_asal	= ($kota_asal!="")?$kota_asal:"BANDUNG";
	
	$is_cari= ($tahun=="")?"1":$is_cari;
	$tahun	= ($tahun=="")?date("y"):$tahun;
	$bulan	= ($bulan=="")?substr('0'.date("m"),-2):$bulan;

	$tgl_maximum	= getMaxDate($bulan,'20'.$tahun);

	if($tgl_awal==""){
		$tgl_awal	= date("d");
	}
	else if($tgl_awal>$tgl_maximum){
		$tgl_awal	= $tgl_maximum;
	}

	if($tgl_akhir==""){
		$tgl_akhir	= date("d");
	}
	else if($tgl_akhir>$tgl_maximum){
		$tgl_akhir	= $tgl_maximum;
	}

	$tgl_awal_mysql	= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_awal,-2);
	$tgl_akhir_mysql= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_akhir,-2);
	//end pemgaturan periode

	//pengaturan pemilihan tabel pencarian
	$tbl_pencarian	= "tbl_log_koreksi_disc tr";
	//end pengatran pemilihan tabel pencarian
	
	$template->set_filenames(array('body' => 'laporan_koreksi_disc/laporan_koreksi_disc_body.tpl')); 
	
	//membuat view untuk cabang-cabang per kota sesuai dengan kota yang dikehendaki
	if($is_cari=="1"){
			
		//create view
		$sql = 
			"CREATE OR REPLACE VIEW view_list_jurusan_by_kota_".str_replace("/","_",$kota_asal)." AS 
			SELECT tmj.IdJurusan 
			FROM tbl_md_jurusan tmj 
			INNER JOIN tbl_md_cabang tmc ON tmj.KodeCabangAsal=tmc.KodeCabang 
			WHERE Kota='$kota_asal'";
		
		if (!$result = $db->sql_query($sql)){
			echo("Error :".__LINE__);exit;
		}
	}
	
	//mengambil list id jurusan by kota
	$sql	= "SELECT IdJurusan FROM view_list_jurusan_by_kota_".str_replace("/","_",$kota_asal)." ORDER BY IdJurusan";
	
	if (!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	} 
	
	$list_id_jurusan ="";
	
	while ($row = $db->sql_fetchrow($result)){
		$list_id_jurusan .=$row[0].",";
	}
	$list_id_jurusan	= substr($list_id_jurusan,0,-1);
	//END mengambil list id jurusan by kota
	
	$kondisi_status	= $status==''?'%':$status;
	
	$kondisi	= " WHERE (DATE(WaktuKoreksi) BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND IdJurusan IN($list_id_jurusan) ";
	
	$kondisi	.= ($cari=="")?"":" AND (Nama LIKE '%$cari%' OR Telp LIKE '%$cari' OR NoTiket LIKE '%$cari' OR KodeBooking LIKE '%$cari')";
	
	if($cabang_asal=='0'){
		$kondisi	.= "";
	}
	else{
		
		if($cabang_tujuan=='0'){
			$kondisi	.= " AND (SELECT KodeCabangAsal FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tr.IdJurusan) LIKE '$cabang_asal' ";
		}
		else{
			if($kode_jadwal=='0'){
				$kondisi	.= " AND IdJurusan LIKE '$cabang_tujuan' ";
			}
			else{
				$kondisi	.= " AND KodeJadwal LIKE '$kode_jadwal' ";
			}
		}
	}	
	
	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging		= pagingData($idx_page,"NoTiket",$tbl_pencarian,
							"&tgl_awal=$tgl_awal&tgl_akhir=$tgl_akhir&opt_bulan=$bulan&tahun=$tahun&kota_asal=$kota_asal&opt_cabang_asal=$cabang_asal&opt_tujuan=$cabang_tujuan&opt_jam=$kode_jadwal&cari=$cari",
							$kondisi,"laporan_koreksi_disc.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================

	$sql = 
		"SELECT *
		FROM $tbl_pencarian
		$kondisi
		ORDER BY TglBerangkat,JamBerangkat,KodeJadwal,NomorKursi LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
	$idx_check=0;
	
	
	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	} 
	
	$i = $idx_page*$VIEW_PER_PAGE+1;
		
	//mengambil data User
	$sql_user = 
		"SELECT user_id,nama,nrp FROM tbl_user";
	
	if ($result_user = $db->sql_query($sql_user)){
	  while ($row_user = $db->sql_fetchrow($result_user)){
			$data_user[$row_user['user_id']]	= $row_user['nama'];
	  }
	} 
	//end mengambil data User
	
	while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
						
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'						=>$odd,
					'no'						=>$i,
					'no_tiket'			=>$row['NoTiket'],
					'tgl_berangkat'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),
					'jam'						=>$row['JamBerangkat'],
					'kode_jadwal'		=>$row['KodeJadwal'],
					'no_kursi'			=>$row['NomorKursi'],
					'nama'					=>$row['Nama'],
					'telp'					=>$row['Telp'],
					'waktu_pesan'		=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
					'penjual'				=>$data_user[$row['PetugasPenjual']],
					'waktu_koreksi'	=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuKoreksi'])),
					'pengkoreksi'		=>$data_user[$row['PetugasPengkoreksi']],
					'otorisasi'			=>$data_user[$row['PetugasOtorisasi']],
					'harga_tiket'		=>number_format($row['HargaTiket'],0,",","."),
					'disc_lama'			=>number_format($row['DiscountMula'],0,",","."),
					'disc_baru'			=>number_format($row['DiscountBaru'],0,",","."),
					'manifest'			=>dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuCetakSPJ'])),
				)
			);
		
		$i++;
	}
	
	
	//KOMPONEN UNTUK EXPORT
	$parameter_cetak	= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir."&opt_bulan=".$bulan."&tahun=".$tahun."&kota_asal=".$kota_asal."&opt_cabang_asal=".$cabang_asal."&opt_tujuan=".$cabang_tujuan."&opt_jam=".$kode_jadwal."&status=".$status."&cari=".$cari;
	$script_cetak_pdf="Start('laporan_koreksi_disc_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
	$script_cetak_excel="Start('laporan_koreksi_disc_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
	//END KOMPONEN UNTUK EXPORT	

	$template->assign_vars(array(
		'BCRUMP'    		=>setBcrump($id_page),
		'CETAK_XL'    	=> $script_cetak_excel,
		'CETAK_PDF'    	=> $script_cetak_pdf,
		'ACTION_CARI'		=> append_sid('laporan_koreksi_disc.'.$phpEx),
		'KOTA'					=> $kota_asal,
		'OPT_KOTA'			=> setComboKota($kota_asal),
		'ASAL'					=> $cabang_asal,
		'ID_JURUSAN'		=> $cabang_tujuan,
		'KODE_JADWAL'		=> $kode_jadwal,
		'OPT_STATUS_'.$status		=> "selected",
		'CABANG_TUJUAN'	=> setComboCabangTujuan($cabang_asal,$cabang_tujuan),
		'TGL_AWAL'			=> $tgl_awal,
		'TGL_AKHIR'			=> $tgl_akhir,
		'BULAN_'.$bulan	=> "selected",
		'TAHUN'					=> $tahun,
		'CARI'					=> $cari,
		'PAGING'				=> $paging
		)
	);
	
}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>