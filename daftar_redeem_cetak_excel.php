<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 607;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN,$LEVEL_CCARE))){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$username				= $userdata['username'];
// LIST
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= date_format(date_create($tanggal_mulai),'Y-m-d');
$tanggal_akhir_mysql	= date_format(date_create($tanggal_akhir),'Y-m-d');
//INISIALISASI

$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";

$kondisi	=($cari=="")?"":
    " AND (r.Nama LIKE '%$cari%'
				OR r.Telp LIKE '%$cari%'
				OR m.IdMember LIKE '%$cari%'
				OR p.IdCard LIKE '%$cari%'
				OR p.KodeBooking LIKE '%$cari%')";

$sql =" SELECT p . * , r.Nama AS Nama, r.Telp AS Telp
        FROM tbl_ponta_reedem AS p
        LEFT JOIN tbl_md_member AS m ON m.NoSeriKartu = p.IdCard
        LEFT JOIN tbl_reservasi AS r ON r.NoTiket = p.NoTiket
        WHERE (DATE(p.WaktuReedem) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	    $kondis";

if ($result = $db->sql_query($sql)){

    $i=1;

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
    $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

    //HEADER
    $objPHPExcel->getActiveSheet()->setCellValue('A1','Daftar Reedem Ponta Per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Handphone');
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Ponta ID');
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Tanggal Transaksi');
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Kode Booking');
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jml Reedem');
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jml Poin');
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('I3', 'Jml Tagihan');
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

    $idx=0;

    while ($row = $db->sql_fetchrow($result)){
        $idx++;
        $idx_row=$idx+3;

        $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['HP'].'/'.$row['Telp']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$idx_row, $row['IdCard'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, date_format(date_create($row['WaktuReedem']),'d-m-Y H:i:s'));
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['KodeBooking']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['JumlahReedem']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['JumlahPoin']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['JumlahTagihan']);


    }


    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );
    for($col = 'A'; $col !== 'J'; $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col.'3:'.$col.$idx_row)->applyFromArray($styleArray);
    }

    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Daftar Reedem Ponta Per Tanggal '.$tanggal_mulai.'-'.$tanggal_akhir.'.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

}
else{
    die_error('Err:',__LINE__);
}
?>