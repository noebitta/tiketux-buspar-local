<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassPromo.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in']){
    if(!isset($HTTP_GET_VARS['mode'])){
        redirect(append_sid('index.'.$phpEx),true);
    }
    else{
        echo("loading...");exit;
    }
}

$Reservasi		= new Reservasi();
$Cabang         = new Cabang();

$kode_booking	= isset($HTTP_GET_VARS['kodebooking'])?$HTTP_GET_VARS['kodebooking']:$HTTP_POST_VARS['kodebooking'];
$no_tiket   	= isset($HTTP_GET_VARS['notiket'])?$HTTP_GET_VARS['notiket']:$HTTP_POST_VARS['notiket'];
$user_otp   	= isset($HTTP_GET_VARS['otp'])?$HTTP_GET_VARS['otp']:$HTTP_POST_VARS['otp'];
$kodecabang     = isset($HTTP_GET_VARS['kodecabang'])?$HTTP_GET_VARS['kodecabang'] : $HTTP_POST_VARS['kodecabang'];
$mode           = isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];

function CURL($params,$url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

    // receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec ($ch);

    curl_close ($ch);

    $json = json_decode($server_output);

    return $json;
}
function gagalkanReedem($id_ponta,$kode_booking,$kodecabang,$nama_cabang,$token){

    global $db;
    global $config;

    $sql = "SELECT * FROM tbl_ponta_reedem WHERE KodeBooking = '$kode_booking' AND TipeReedem = 1";
    if(!$result = $db->sql_query($sql)){
        return "alert('FATAL ERROR');";
    }

    while($row = $sql->sql_fetchrow($result)){
        $params = ['kode_cabang' => $kodecabang,
            'nama_cabang' => $nama_cabang,
            'access_token' => $token,
            'card_id' => $id_ponta,
            'kode_booking'=>$kode_booking,
            'no_tiket'=>$row['NoTiket']
        ];

        $void = CURL($params,$config['url_voidreedem']);

        if($void->daytrans->status != "OK"){
           return "alert('GAGAL MEMBATALKAN POINT PONTA!!'); ";

        }
    }
}

//Mengambil data total bayar
$data_total = $Reservasi->ambilTotalPesananByKodeBooking($kode_booking);
$total_bayar = $data_total['TotalBayar'];

//Mengambil detail cabang
$data_cabang = $Cabang->ambilDataDetail($kodecabang);
$nama_cabang = $data_cabang['Nama'];

//mengambil id ponta
$datapesanan = $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
$pesanan = $db->sql_fetchrow($datapesanan);
$id_ponta= $pesanan['IDPONTA'];
$telp    = $pesanan['Telp'];

$data_tiket = $Reservasi->ambilDataKursiByNoTiket($no_tiket);
if($id_ponta == ""){
    $id_ponta= $pesanan['IDPONTA'];
}

$data_pembayaran = "SELECT JenisPembayaran FROM tbl_reservasi WHERE KodeBooking = '$kode_booking' LIMIT 1";
if(!$hasil  = $db->sql_query($data_pembayaran)){
    echo ("Terjadi Kesalahan ");
    exit;
}
$data = $db->sql_fetchrow($hasil);
$jenis_pembayaran = $data[0];

//LOGIN
$params = ['kode_cabang' => $kodecabang, 'nama_cabang' => $nama_cabang];

$getToken = CURL($params,$config['url_login']);
if($getToken->daytrans->status != "OK"){
    echo ("alert('terjadi kegagalan!');");
    exit;
}
$token = $getToken->daytrans->results->accessToken;
switch ($mode){
    case "reedem":

        $sql = "SELECT OTPreedem FROM tbl_reservasi WHERE KodeBooking = '$kode_booking' GROUP BY KodeBooking;";
        $data_otp = $db->sql_fetchrow($db->sql_query($sql));

        if($user_otp != $data_otp['OTPreedem']){
            echo("alert('OTP TIDAK COCOK');dialog_ponta.hide();");
        }

        $result = $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
            while ($data_kursi = $db->sql_fetchrow($result)){
                //REEDEM
                $params = ['kode_booking'   => $kode_booking,
                            'no_tiket'      => $data_kursi['NoTiket'],
                            'kode_cabang' 	=> $kodecabang,
                            'nama_cabang' 	=> $nama_cabang,
                            'access_token'  => $token,
                            'card_id'       => $id_ponta,
                            'total'         => $data_kursi['Total'],
                            'poin'          => $data_kursi['Total'],
                ];
                $reedem = CURL($params,$config['url_reedem']);
                if($reedem->daytrans->status != "OK"){
                    $failed = gagalkanReedem($id_ponta,$kode_booking,$kodecabang,$nama_cabang,$token);
                    echo ("alert('Terjadi Kesalahan');");
                    exit;
                }
            }
        
            echo("CetakTiket('8');dialog_ponta.hide();");
        exit;
    case "enquery":
        //ENQUERY CHECK POINT PONTA MEMBER
        $params = ['kode_cabang' => $kodecabang,
                    'nama_cabang' => $nama_cabang,
                    'access_token' => $token,
                    'card_id' => $id_ponta,
                    'poin_reedem'=>$total_bayar,
                    'req_reedem'=>1,
                    'telp_reedem'=>$telp];
        $enQuery = CURL($params,$config['url_enquiry']);

        if($enQuery->daytrans->status != "OK"){
            echo ("alert('Jumlah poin tidak cukup!!'); ");
            exit;
        }

        $otp = $enQuery->daytrans->results->otp;

        $sql = "UPDATE tbl_reservasi SET OTPreedem = '$otp' WHERE KodeBooking = '$kode_booking';";
        if(!$db->sql_query($sql)){
            echo ("alert('Terjadi Kesalahan!!'); ");
            exit;
        }

        echo ("otp = ".$enQuery->daytrans->results->otp.";dialog_ponta.show();");
        exit;
    case 'voidreedem':
        //VOID REEDEM
        $sql = "SELECT StatusBatch FROM tbl_ponta_transaksi WHERE KodeBooking = '$kode_booking'";
        if(!$result = $db->sql_query($sql)){
            echo ("alert('GAGAL MEMDAPATKAN STATUS BATCH POINT PONTA!!'); ");
            exit;
        }

        $row = $db->sql_fetchrow($result);
        $batch = $row['StatusBatch'];

        if($id_ponta != ""){
            if($batch == 1 || $jenis_pembayaran == 8){
                $params = ['kode_cabang' => $kodecabang,
                            'nama_cabang' => $nama_cabang,
                            'access_token' => $token,
                            'card_id' => $id_ponta,
                            'kode_booking'=>$kode_booking,
                            'no_tiket'=>$no_tiket
                            ];

                $void = CURL($params,$config['url_voidreedem']);

                if($void->daytrans->status != "OK"){
                    echo ("alert('GAGAL MEMBATALKAN POINT PONTA!!'); ");
                    exit;
                }


                $sql = "UPDATE tbl_reservasi SET FlagVoidReedem = 1 WHERE KodeBooking = '$kode_booking'";
                $db->sql_query($sql);

                echo ("console.log('BERHASIL MEMBATALKAN POINT');");
                exit;
            }

            echo ("console.log('POINT BELUM DI BATCH DAN PEMBAYARAN BUKAN REEDEM');");
        }else{
            echo ("console.log('TIKET TIDAK MEMAKAI PONTA');");
        }

    exit;
}

?>