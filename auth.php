<?php
//
// AUTENTIFIKASI (login n logout)
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

//SESSION
$id_page = 0;
$userdata = session_pagestart($user_ip,$id_page);  // Login n Out
init_userprefs($userdata);

//INCLUDE
include($adp_root_path . 'ClassAuth.php');

//PARAMETER
$mode     = getVariabel("mode");

//INSTANCE UMUM
$Auth     = new Auth();

//INIT

//ROUTER========================================================================================================================================================================

switch($mode){
  case "0":
    //LOGIN

    $username = getVariabel('username');
    $password = getVariabel('password');
    $hostuuid = getVariabel('hostuuid');

    prosesLogin($username,$password,$hostuuid);

    break;

  case "1":
    //LOGOUT

    prosesLogout();

    break;

  case "2":
    //SHOW DIALOG UBAH PASSWORD

    showDialogUbahPassword();

    break;

  case "3":
    //UBAH PASSWORD
    $password_lama = getVariabel('passlama');
    $password_baru = getVariabel('passbaru');

    ubahPassword($password_lama,$password_baru);

    break;
}

//METHODES & PROCESS ========================================================================================================================================================================

function prosesLogin($username,$password,$hostuuid){
  global $Auth,$user_ip,$userdata;

  //PROSES LOGIN
  $result_login = $Auth->login($username,$password);

  if($result_login["berhasil"]){
    //Login berhasil

    //Memeriksa komputer apakah sudah terdaftar atau belum ---
    /*$data_macaddress  = $Auth->verifikasiMacAddress($hostuuid);

    if($data_macaddress["Id"]==""){
      //Komputer belum terdaftar
      $token  = $Auth->createToken($username,$user_ip);

      redirect("login.daftarkankomputer.php?user=$username&token=$token");

    }elseif($data_macaddress["IsAktif"]!=1){
      //Komputer di suspend

      die_error("Komputer ini sedang di suspend!");

    }*/

    $kode_cabang  = $data_macaddress["KodeCabang"];

    //mengupdate cabang tugas sesuai komputer yang terdaftar
    $Auth->updateCabangTugas($result_login["user_id"],$kode_cabang);

    //menulis ke log user
    $Auth->updateLogUser($result_login["user_id"],$user_ip,$hostuuid);

    $session_id = session_begin($result_login['user_id'], $user_ip,0);
    $userdata["user_level"] = $result_login["user_level"];

    if(!$session_id){
      die_error("GAGAL memulai session login!");
    }

    $url = gotoMainpage($userdata["user_level"]);

    redirect($url);
  }
  else{
    showHalamanGagalLogin("Username atau Password anda tidak benar!");
  }


} //prosesLogin

function prosesLogout(){
  global $Auth,$userdata;

  if($userdata['session_logged_in']){
    if($Auth->logout($userdata)){
      redirect("index.php");
    }
    else{
      die_error("Gagal melakukan proses logout!");
    }
  }
  else{
    redirect("index.php");
  }
} //prosesLogout

function showHalamanGagalLogin($pesan){
  global $template,$config;

  $template->set_filenames(array('body' => 'login.error.tpl'));

  $template->assign_vars(array(
    "SITENAME"  => $config["site_name"],
    "PESAN"     => $pesan));

  $template->pparse('body');

} //showHalamanGagalLogin

function showDialogUbahPassword(){
  global $template;

  $template->set_filenames(array("body"=>"ubahpassword.tpl"));


  $template->pparse("body");
}

function ubahPassword($password_lama,$password_baru){
  global $Auth,$userdata;

  echo(json_encode($Auth->ubahPassword($userdata["user_id"],$password_lama,$password_baru)));

} //ubahPassword

?>