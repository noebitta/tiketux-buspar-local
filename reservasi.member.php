<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSMSGateway.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>");
}
//#############################################################################
// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$order			= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$Member	= new Member();

$mode	= $mode==""?"tambah":$mode;

switch($mode){
	//tambah
	case "tambah":
		
		$tgl_berlaku	= date("d-m-").(date("Y")+5);
		$tgl_lahir		= date("d-m-").(date("Y")-30);
		
		$template->assign_block_vars('tombolsubmit',array());
		
		$template->assign_vars(array(
		 'ID_MEMBER'			=> $Member->generateIdMember(),
		 'MODE'   				=> 'simpan',
		 'U_ADD_ACT'			=> append_sid('reservasi.member.'.$phpEx),
		 'TGL_LAHIR'			=> $tgl_lahir,
		 'TGL_BERLAKU'		=> $tgl_berlaku,
		 'TGL_REGISTRASI'	=> date("d-m-Y"),
		 'TOMBOL_BATAL_VAL'=> "BATAL"
		 )
		);
		
		$template->set_filenames(array('body' => 'reservasi/reservasi.member.reg.tpl')); 
		
		include($adp_root_path . 'includes/page_header_detail.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
	exit;
	
	case "simpan":
		// aksi menambah member
		
		$id_member  		= str_replace(" ","",$HTTP_POST_VARS['id_member']);
		$no_seri_kartu 	= $HTTP_POST_VARS['kartuponta'];
		$nama   				= $HTTP_POST_VARS['nama'];
		$hp							= $HTTP_POST_VARS['hp'];
		$hp2						= $HTTP_POST_VARS['hp2'];
		$email					= $HTTP_POST_VARS['email'];
		$jenis_kelamin  = $HTTP_POST_VARS['jenis_kelamin'];
		$tempat_lahir   = $HTTP_POST_VARS['tempat_lahir'];
		$tgl_lahir   		= $HTTP_POST_VARS['tgl_lahir'];
		$alamat   			= $HTTP_POST_VARS['alamat'];
		$kota   				= $HTTP_POST_VARS['kota'];
		$pekerjaan			= $HTTP_POST_VARS['pekerjaan'];
		$tgl_berlaku		= $HTTP_POST_VARS['tgl_berlaku'];
		$password				= rand(100000,999999);
		
		$tombol_batal_val	= "BATAL";
		
		if($Member->periksaDuplikasi($id_member,$email,$hp)){
			$pesan="Member yang dimasukkan sudah terdaftar dalam sistem!";
			$bgcolor_pesan="red";
			$color_pesan	= "white";
			$template->assign_block_vars('tombolsubmit',array());
		}
		else{	
			if($Member->tambah(
				strtoupper($id_member),strtoupper($nama), $jenis_kelamin,
				0,$tempat_lahir,FormatTglToMySQLDate($tgl_lahir),
				"",date("Y-m-d"),$alamat,
				strtoupper($kota),"",$hp2,
				$hp,strtolower($email),strtoupper($pekerjaan),
				$point,FormatTglToMySQLDate($tgl_berlaku),"",
				$no_seri_kartu,$password,1,$userdata['KodeCabang'])){
				
				$pesan="Data Berhasil Disimpan!";
				$bgcolor_pesan="#98e46f";
				$color_pesan	= "green";
				
				$tombol_batal_val	= "TUTUP";

        //KIRIM SMS PASSWORD
        $SMSGateway = new SMSGateway();
        $isi_pesan  = "Terima kasih Tn/Ny ".strtoupper(substr($nama,0,1)).substr($nama,1,14)." sudah menjadi member Daytrans Addict,Nomor Member:".strtoupper($id_member)." Kartu Ponta : ".$no_seri_kartu.", password:".$password;
        $SMSGateway->sendSMS($hp,$isi_pesan);
			}
			else{
				$pesan="Gagal menyimpan data member!";
				$bgcolor_pesan="red";
				$color_pesan	= "white";
				$template->assign_block_vars('tombolsubmit',array());
			}
		}
		
		$template->assign_block_vars('pesan',array());
		
		$temp_var_aktif="jk_".$jenis_kelamin;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'member/add_body.tpl')); 
		$template->assign_vars(array(
			'MODE'   				=> 'save',
			'ID_MEMBER'			=> $id_member,
			'NOMOR_KARTU'		=> $no_seri_kartu,
			'NAMA'    			=> $nama,
			'HP'						=> $hp,
			'HP2'						=> $hp2,
			'EMAIL'					=> $email,
			'JK_0'    			=> $jk_0,
			'JK_1'    			=> $jk_1,
			'TEMPAT_LAHIR'	=> $tempat_lahir,
			'TGL_LAHIR' 		=> $tgl_lahir,
			'ALAMAT'   			=> $alamat,
			'KOTA'   				=> $kota,
			'PEKERJAAN'			=> $pekerjaan,
			'TGL_BERLAKU'		=> $tgl_berlaku,
			'PESAN'					=> $pesan,
			'BGCOLOR_PESAN'	=> $bgcolor_pesan,
			'COLOR_PESAN'		=> $color_pesan,
			'U_ADD_ACT'			=> append_sid('reservasi.member.'.$phpEx),
			'MODE'   				=> 'simpan',
			'TOMBOL_BATAL_VAL'=> $tombol_batal_val
			)
		);
		
		$template->set_filenames(array('body' => 'reservasi/reservasi.member.reg.tpl')); 
			
		include($adp_root_path . 'includes/page_header_detail.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
	exit;
}

?>