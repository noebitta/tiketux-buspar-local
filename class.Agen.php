<?php
$adp_root_path = './';
include($adp_root_path . 'class.RequestSignature.php');
//include($adp_root_path . '/includes/functions.php');

// Dev
//DEFINE('CLIENTID', 		'TEST');
//DEFINE('CLIENTSECRET', 	'b014dcc2a3a984ebaea090dd96e6d0fc');
//DEFINE('BASEURL',		'https://api.tiketux.com/dev1/');

// Live
/*DEFINE('CLIENTID', 		'BRTPAYMENT');
DEFINE('CLIENTSECRET', 	'5ffc0b24b3fab807836e6b4c92217513054f01671');
DEFINE('BASEURL',		'https://api.tiketux.com/v1/');*/
class Agen{

	//KAMUS GLOBAL
	var $id_file;
	var $url_class;

	//CONSTRUCTOR
	public function __construct(){

		$this->id_file = "AGEN";
	}

	//FINPAY
	public function payIndomaret($booking_code, $booking_name, $booking_price, $user_name, $user_phone, $user_email, $merchant, $gensignature, $time, $otp){

        global $config;

		$reqSignature	= new RequestSignature();
		$params 		= array();

		//Api Parameter
		$params['auth_nonce']	  	= $reqSignature->createNonce(true);
		$params['auth_timestamp'] 	= time();
		$params['auth_client_id']	= $config['client_id'];//CLIENTID;

		$params['merchant'] 		= $merchant;
		$params['order_id'] 		= $booking_code;
		$params['order_name'] 		= $booking_name;
		$params['order_user_name'] 	= $user_name;
		$params['order_user_phone'] = $user_phone;
		$params['order_user_email'] = $user_email;
		$params['order_time']	 	= $time;
        $params['order_price']      = $booking_price;
        $params['order_discount']   = 0;
        $params['order_admin_fee']  = 2500;
		$params['order_amount']	 	= $booking_price+2500;
		$params['return_url'] 		= $config['return_url_indomaret'];
		$params['signature'] 		= $gensignature;
        $params['otp']              = $otp;
		
		$accessToken				= $config['client_id'];//CLIENTID;
		$accessTokenSecret			= $config['client_secret'];//CLIENTSECRET;

		if (!empty($accessToken)) {
			$params['auth_access_token'] = $accessToken;
			$key 	= $accessToken;
			$secret = $accessTokenSecret;
		} else {
			$key 	= $config['client_id'];
			$secret = $config['client_secret'];
		}

		$baseSignature = $reqSignature->createSignatureBase("POST", $config['api_url']."paymentmc/indomaret.json", $params);
		$signature     = $reqSignature->createSignature($baseSignature, $key, $secret);

		return sendHttpPost($config['api_url'] ."paymentmc/indomaret.json",$reqSignature->normalizeParams($params).'&auth_signature=' .$signature);
		
	}

}
?>