<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPromo.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 207;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Promo	= new Promo();
$Cabang	= new Cabang();

function setComboCabang($cabang_dipilih=""){
	//SET COMBO cabang
	global $db;
	
	$opt_cabang ="<option value=''>-Semua-</option>";
	
	$sql = 
			"SELECT *
			FROM tbl_md_cabang ORDER BY Kota,Nama ASC;";
	
	
	if($result = $db->sql_query($sql)){
		
		$kota="";
		
		while ($row = $db->sql_fetchrow($result)){
			
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			
			if($kota!=$row['Kota']){
				
				if($kota!=""){
					$opt_cabang .= "</optgroup>";
				}
				
				$kota=$row['Kota'];
				$opt_cabang .="<optgroup label='$kota'>";
			}
			
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama]  ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	
	return $opt_cabang."</optgroup>";
	//END SET COMBO CABANG
}


	if ($mode=='add'){
		// add
		if($userdata['user_level'] != 0) {
			$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '105'
						AND user_level='$userdata[user_level]'";
			$result = $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
			}
		}

		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Ditambah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'promo/add_body.tpl'));

		$template->assign_vars(array(
		 'BCRUMP'		=>setBcrump($id_page),
		 'JUDUL'		=>'Tambah Data Promo',
		 'MODE'   	=> 'save',
		 'ASAL'   	=> setComboCabang(),
		 'TUJUAN'   => setComboCabang(),
		 'SUB'    	=> '0',
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_ADD_ACT'	=> append_sid('pengaturan_promo.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		if($userdata['user_level'] != 0) {
			$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '105'
						AND user_level='$userdata[user_level]'";
			$result = $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
			}
		}

		// aksi menambah promo
		$kode_promo  		= str_replace(" ","",$HTTP_POST_VARS['kode_promo']);
		$kode_promo_old	= str_replace(" ","",$HTTP_POST_VARS['kode_promo_old']);
		$nama_promo   			= $HTTP_POST_VARS['nama_promo'];
		$priority   				= $HTTP_POST_VARS['priority'];
		$asal								= $HTTP_POST_VARS['asal'];
		$tujuan							= $HTTP_POST_VARS['tujuan'];
		$mulai_jam					= $HTTP_POST_VARS['mulai_jam'];
		$akhir_jam					= $HTTP_POST_VARS['akhir_jam'];
		$tanggal_mulai_promo= $HTTP_POST_VARS['tanggal_mulai_promo'];
		$tanggal_akhir_promo= $HTTP_POST_VARS['tanggal_akhir_promo'];
		$flag_discount			= $HTTP_POST_VARS['flagdiscount'];
		$discount						= $HTTP_POST_VARS['discount'];
		$target_discount		= $HTTP_POST_VARS['target_discount'];
		$jumlah_minimum			= $HTTP_POST_VARS['jumlah_minimum'];
		$status_aktif 			= $HTTP_POST_VARS['aktif'];

		$terjadi_error=false;
		
		if($Promo->periksaDuplikasi($kode_promo,$asal,$tujuan,$tanggal_mulai_promo." ".$mulai_jam,$tanggal_akhir_promo." ".$akhir_jam,$target_discount,$priority) && $kode_promo!=$kode_promo_old){
			$pesan="<font color='white' size=3>Kode promo yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				if($userdata['user_level'] != 0) {
					$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '105'
						AND user_level='$userdata[user_level]'";
					$result = $db->sql_fetchrow($db->sql_query($sql));

					if($result == false){
						die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
					}
				}

				$judul="Tambah Data promo";

				if($Promo->tambah(
					$kode_promo, $nama_promo, $priority, $asal, $tujuan, 
					$mulai_jam, $akhir_jam, FormatTglToMySQLDate($tanggal_mulai_promo), FormatTglToMySQLDate($tanggal_akhir_promo), 
					$jumlah_minimum, $flag_discount, $discount, $target_discount,$status_aktif)){
					
					redirect(append_sid('pengaturan_promo.'.$phpEx.'?mode=add&pesan=1',true));
					
				}
			}
			else{
				if($userdata['user_level'] != 0) {
					$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '106'
						AND user_level='$userdata[user_level]'";
					$result = $db->sql_fetchrow($db->sql_query($sql));

					if($result == false){
						die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
					}
				}

				$judul="Ubah Data promo";

				if($Promo->ubah(
					$kode_promo_old,$kode_promo, $nama_promo, $priority, $asal, $tujuan, 
					$mulai_jam, $akhir_jam, FormatTglToMySQLDate($tanggal_mulai_promo), FormatTglToMySQLDate($tanggal_akhir_promo), 
					$jumlah_minimum, $flag_discount, $discount, $target_discount,$status_aktif)){
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
					
				}
			}
			
		}
		
		$temp_var_aktif="prior_".$priority;
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="target_".$target_discount;
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'promo/add_body.tpl'));

		$template->assign_vars(array(
			 'BCRUMP'		                    =>setBcrump($id_page),
			 'JUDUL'		                    => $judul,
			 'MODE'   	                    => 'save',
			 'SUB'    	                    => $submode,
			 'KODE_PROMO_OLD'               => $kode_promo_old_old,
			 'KODE_PROMO'                   => $kode_promo_old,
			 'NAMA_PROMO'                   => $nama_promo,
			 'PRIOR_0'   		                => $prior_0,
			 'PRIOR_1'			                => $prior_1,
			 'PRIOR_2'			                => $prior_2,
			 'ASAL'					                => setComboCabang($asal),
			 'TUJUAN'				                => setComboCabang($tujuan),
			 'JAM_MULAI'		                => $mulai_jam,
			 'JAM_AKHIR'		                => $akhir_jam,
			 'TGL_MULAI'		                => $tanggal_mulai_promo,
			 'TGL_AKHIR'		                => $tanggal_akhir_promo,
       'FLAGDISCOUNT'.$flag_discount  =>"selected",
			 'DISCOUNT'			                => $discount,
			 'TARGET_0'			                => $target_0,
			 'TARGET_1'			                => $target_1,
			 'TARGET_2'			                => $target_2,
			 'JUM_MINIMUM'	                => $jumlah_minimum,
			 'AKTIF_1'			                => $status_aktif_1,
			 'AKTIF_0'			                => $status_aktif_0,
			 'PESAN'				                => $pesan,
			 'BGCOLOR_PESAN'                => $bgcolor_pesan,
			 'U_ADD_ACT'		                =>append_sid('pengaturan_promo.'.$phpEx)
			)
		);
	
	} 
	else if ($mode=='edit'){
		// edit
		if($userdata['user_level'] != 0) {
			$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '106'
						AND user_level='$userdata[user_level]'";
			$result = $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
			}
		}

		$id = $HTTP_GET_VARS['id'];
		
		$row=$Promo->ambilDataDetail($id);
		
		$temp_var_aktif="prior_".$row['LevelPromo'];
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="target_".$row['FlagTargetPromo'];
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'promo/add_body.tpl'));

		$template->assign_vars(array(
			 'BCRUMP'		                            =>setBcrump($id_page),
			 'JUDUL'		                            => 'Ubah Data promo',
			 'MODE'   	                            => 'save',
			 'SUB'    	                            => '1',
			 'KODE_PROMO_OLD'                       => $row['KodePromo'],
			 'KODE_PROMO'                           => $row['KodePromo'],
			 'NAMA_PROMO'                           => $row['NamaPromo'],
			 'PRIOR_0'   		                        => $prior_0,
			 'PRIOR_1'			                        => $prior_1,
			 'PRIOR_2'			                        => $prior_2,
			 'ASAL'					                        => setComboCabang($row['KodeCabangAsal']),
			 'TUJUAN'				                        => setComboCabang($row['KodeCabangTujuan']),
			 'JAM_MULAI'		                        => $row['JamMulai'],
			 'JAM_AKHIR'		                        => $row['JamAkhir'],
			 'TGL_MULAI'		                        => FormatMySQLDateToTgl($row['BerlakuMula']),
			 'TGL_AKHIR'		                        => FormatMySQLDateToTgl($row['BerlakuAkhir']),
        'FLAGDISCOUNT'.$row["FlagDiscount"]   =>"selected",
			 'DISCOUNT'			                        => $row['JumlahDiscount'],
			 'TARGET_0'			                        => $target_0,
			 'TARGET_1'			                        => $target_1,
			 'TARGET_2'			                        => $target_2,
			 'JUM_MINIMUM'	                        => $row['JumlahPenumpangMinimum'],
			 'AKTIF_1'			                        => $status_aktif_1,
			 'AKTIF_0'			                        => $status_aktif_0,
			 'PESAN'				                        => $pesan,
			 'U_ADD_ACT'                            =>append_sid('pengaturan_promo.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		if($userdata['user_level'] != 0) {
			$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '107'
						AND user_level='$userdata[user_level]'";
			$result = $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				echo 0;
				exit;
			}
		}

		// aksi hapus promo
		$list_promo = str_replace("\'","'",$HTTP_GET_VARS['list_promo']);
		//echo($list_promo. " asli :".$HTTP_GET_VARS['list_promo']);
		echo($Promo->hapus($list_promo));
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		if($userdata['user_level'] != 0) {
			$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '106'
						AND user_level='$userdata[user_level]'";
			$result = $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				echo 0;
				exit;
			}
		}

		// aksi hapus jadwal
		$kode_promo_old = str_replace("\'","'",$HTTP_GET_VARS['kode_promo']);
	
		$Promo->ubahStatusAktif($kode_promo_old);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'promo/promo_body.tpl')); 
		
		$target_promo	= array(0=>"Semua",1=>"Member",2=>"Umum");
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE KodePromo LIKE '%$cari%' 
				OR NamaPromo LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"KodePromo","tbl_md_promo","",$kondisi,"pengaturan_promo.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *,f_cabang_get_name_by_kode(KodeCabangAsal) Asal, f_cabang_get_name_by_kode(KodeCabangTujuan) Tujuan
			FROM tbl_md_promo $kondisi 
			ORDER BY BerlakuMula, BerlakuAkhir LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['FlagAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[KodePromo]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[KodePromo]\")'>Tidak Aktif</a>";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_promo.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['NamaPromo'],
							'kode_promo'=>$row['KodePromo'],
							'berlaku_mulai'=>FormatMySQLDateToTgl($row['BerlakuMula']),
							'berlaku_hingga'=>FormatMySQLDateToTgl($row['BerlakuAkhir']),
							'asal'=>$row['Asal'],
							'tujuan'=>$row['Tujuan'],
							'jam_mulai'=>$row['JamMulai'],
							'jam_hingga'=>$row['JamAkhir'],
							'target'=>$target_promo[$row['FlagTargetPromo']],
							'jenis'=>($row['FlagDiscount']==0?"Discount":"Harga"),
							'jumlah'=>$row['JumlahDiscount']>1?number_format($row['JumlahDiscount'],0,",","."):($row['JumlahDiscount']*100)."%",
							'aktif'	=>$status,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=14 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load promo',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		}

		$template->assign_vars(array(
			'BCRUMP'    		=>setBcrump($id_page),
			'U_ADD'					=> append_sid('pengaturan_promo.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_promo.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>