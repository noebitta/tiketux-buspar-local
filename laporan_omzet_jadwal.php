<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 304;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$template->set_filenames(array('body' => 'laporan_omzet_jadwal/laporan_omzet_jadwal_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Cabang		= new Cabang();

if($Cabang->isCabangPusat($userdata['KodeCabang']) || in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]))){
	$kondisi_cabang	= ($kode_cabang=="")?"":" AND KodeCabang='$kode_cabang'";
	$cabang_default	= "";
}
else{
	$kondisi_cabang	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]' ";
	$cabang_default	= $userdata['KodeCabang'];
}

switch($mode){
	case 'gettujuan':
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
}

if($asal!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'";
}

if($asal!="" && $tujuan!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'";
}


$sql=
	"SELECT 
		IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(IF(JenisPenumpang='U',1,0)),0) AS TotalPenumpangU,
		IS_NULL(SUM(IF(JenisPenumpang='M',1,0)),0) AS TotalPenumpangM,
		IS_NULL(SUM(IF(JenisPenumpang='K',1,0)),0) AS TotalPenumpangK,
		IS_NULL(SUM(IF(JenisPenumpang='KK',1,0)),0) AS TotalPenumpangKK,
		IS_NULL(SUM(IF(JenisPenumpang='G',1,0)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE  (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY Jam
	ORDER BY Jam ";

	
if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	
	$sum_berangkat			= 0;
	$sum_penumpang_u		= 0;
	$sum_penumpang_m		= 0;
	$sum_penumpang_k		= 0;
	$sum_penumpang_kk		= 0;
	$sum_penumpang_g		= 0;
	$sum_penumpang			= 0;
	$sum_omzet					= 0;
	$sum_discount				= 0;
	
	for($idx_jam=0;$idx_jam<24;$idx_jam++){
		
		$odd ='odd';
		
		if (($idx_jam % 2)==0){
			$odd = 'even';
		}
		
		if($row['Jam']==$idx_jam){
			$total_berangkat		= $row['TotalBerangkat']; 
			$total_penumpang_u	= $row['TotalPenumpangU']; 
			$total_penumpang_m	= $row['TotalPenumpangM']; 
			$total_penumpang_k	= $row['TotalPenumpangK']; 
			$total_penumpang_kk	= $row['TotalPenumpangKK']; 
			$total_penumpang_g	= $row['TotalPenumpangG']; 
			$total_penumpang		= $row['TotalPenumpang']; 
			$total_omzet				= $row['TotalOmzet']; 
			$total_discount			= $row['TotalDiscount']; 
			$row = $db->sql_fetchrow($result);
		}
		else{
			$total_berangkat		= 0; 
			$total_penumpang_u	= 0; 
			$total_penumpang_m	= 0; 
			$total_penumpang_k	= 0; 
			$total_penumpang_kk	= 0; 
			$total_penumpang_g	= 0; 
			$total_penumpang		= 0; 
			$total_omzet				= 0; 
			$total_discount			= 0; 
		}
		
		$sum_berangkat			+= $total_berangkat;
		$sum_penumpang_u		+= $total_penumpang_u;
		$sum_penumpang_m		+= $total_penumpang_m;
		$sum_penumpang_k		+= $total_penumpang_k;
		$sum_penumpang_kk		+= $total_penumpang_kk;
		$sum_penumpang_g		+= $total_penumpang_g;
		$sum_penumpang			+= $total_penumpang;
		$sum_omzet					+= $total_omzet;
		$sum_discount				+= $total_discount;
		
		$rata_penumpang_per_trip	=($total_berangkat>0)?$total_penumpang/$total_berangkat:0;
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'jam'=>$idx_jam,
					'total_keberangkatan'=>number_format($total_berangkat,0,",","."),
					'total_penumpang_u'=>number_format($total_penumpang_u,0,",","."),
					'total_penumpang_m'=>number_format($total_penumpang_m,0,",","."),
					'total_penumpang_k'=>number_format($total_penumpang_k,0,",","."),
					'total_penumpang_kk'=>number_format($total_penumpang_kk,0,",","."),
					'total_penumpang_g'=>number_format($total_penumpang_g,0,",","."),
					'total_penumpang'=>number_format($total_penumpang,0,",","."),
					'rata_pnp_per_trip'=>number_format($rata_penumpang_per_trip,0,",","."),
					'total_omzet'=>number_format($total_omzet,0,",","."),
					'total_discount'=>number_format($total_discount,0,",","."),
				)
			);
			
	}
			
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&asal=".$asal."&tujuan=".$tujuan;
	
$script_cetak_pdf="Start('laporan_omzet_jadwal_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_jadwal_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$rata_penumpang_per_trip	=($sum_berangkat>0)?$sum_penumpang/$sum_berangkat:0;

$page_title	= "Penjualan/Jam";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'URL'						=> append_sid('laporan_omzet.php'.$parameter),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_ASAL'			=> $Cabang->setInterfaceComboCabang($asal,$cabang_default),
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'SUM_KEBERANGKATAN'	=>number_format($sum_berangkat,0,",","."),
	'SUM_PENUMPANG_U'		=>number_format($sum_penumpang_u,0,",","."),
	'SUM_PENUMPANG_M'		=>number_format($sum_penumpang_m,0,",","."),
	'SUM_PENUMPANG_K'		=>number_format($sum_penumpang_k,0,",","."),
	'SUM_PENUMPANG_KK'	=>number_format($sum_penumpang_kk,0,",","."),
	'SUM_PENUMPANG_G'		=>number_format($sum_penumpang_g,0,",","."),
	'SUM_PENUMPANG'			=>number_format($sum_penumpang,0,",","."),
	'RATA_PNP_PER_TRIP'	=>number_format($rata_penumpang_per_trip,0,",","."),
	'SUM_OMZET'			=> number_format($sum_omzet,0,",","."),
	'SUM_DISCOUNT'	=> number_format($sum_discount,0,",","."),
	'SUM_BIAYA'			=> number_format($sum_biaya,0,",","."),
	'SUM_PROFIT'		=> number_format($sum_profit,0,",","."),
	'U_GRAFIK'			=> append_sid('laporan_omzet_jadwal_grafik.'.$phpEx.'?1').$parameter_cetak,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>