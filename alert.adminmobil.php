<?php
// STANDAR

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

$sql =
  "SELECT JatuhTempo, CURDATE() as Hari_ini, DATEDIFF(JatuhTempo, CURDATE()) as selisih
  FROM tbl_tilang
  HAVING selisih <= 7 AND selisih >= 0";

if (!$result = $db->sql_query($sql)){
  echo("Err:".__LINE__);exit;
}

if($db->sql_numrows($result) > 0){
  $template->assign_block_vars("alert_tilang",array("url"=>append_sid("tilang.php")));
}

// NOTIF JATUH TEMPO SURAT-SURAT KENDARAAAN
$sql =
  "SELECT TglKp, TglPajak, TglStnk, Kir,TglSipa,
    DATEDIFF(TglPajak, CURDATE()) as selisih_pajak,
    DATEDIFF(TglStnk, CURDATE()) as selisih_stnk,
    DATEDIFF(Kir, CURDATE()) as selisih_kir,
    DATEDIFF(TglSipa, CURDATE()) as selisih_sipa,
    DATEDIFF(TglKp, CURDATE()) as selisih_kp
  FROM tbl_md_kendaraan
  HAVING (selisih_pajak <= 7 AND selisih_pajak >= 0)
  OR (selisih_stnk <= 7 AND selisih_stnk >= 0)
  OR (selisih_kir <= 7 AND selisih_kir >= 0)
  OR (selisih_sipa <= 7 AND selisih_sipa >= 0)
  OR (selisih_kp <= 7 AND selisih_kp >= 0)";

if (!$result = $db->sql_query($sql)){
  echo("Err:".__LINE__);exit;
}

if($db->sql_numrows($result) > 0){
  $template->assign_block_vars("alert_pajak",array("url"=>append_sid("pengaturan_mobil.php")));
}

$template->set_filenames(array("alert" => "alert.adminmobil.tpl"));
$template->pparse("alert")
?>