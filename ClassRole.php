<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Role{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL;

	//CONSTRUCTOR
	function Role(){
		$this->ID_FILE="C-ROLE";
    $this->TABEL="tbl_md_role";
	}
	
	//BODY
	
	function periksaDuplikasi($NamaRole,$id=""){
		
		//kamus
		global $db;

    $NamaRole = trim($NamaRole);

    $kondisi_id = $id==""?"":" AND IdRole!=$id";

		$sql = 
			"SELECT COUNT(1) AS Ditemukan FROM $this->TABEL  WHERE NamaRole='$NamaRole' $kondisi_id";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}

    $row  = $db->sql_fetchrow($result);
    $ditemukan  = $row["Ditemukan"]?true:false;
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($NamaRole){
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql ="INSERT INTO $this->TABEL SET NamaRole='$NamaRole'";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
		}

    $sql  = "SELECT LAST_INSERT_ID()";

    if (!$result=$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    return $row[0];
	}

  function ambilData($order_by=0,$sort_by=0,$idx_page="",$cari=""){

    //kamus
    global $db,$VIEW_PER_PAGE,$config,$userdata;

    //UNTUK SORTING
    $koloms = array("NamaRole");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1 ".($cari==""?"":" AND NamaRole LIKE '%$cari%'");

    $set_kondisi_admin  = $userdata["user_level"]!=$config["role_admin"]?" AND IdRole!=".$config["role_admin"]:"";

    $sql =
      "SELECT SQL_CALC_FOUND_ROWS *
			FROM $this->TABEL
			$set_kondisi $set_kondisi_admin
			$order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);
    }

    return $result;

  }//  END ambilData
	
	function ubah($IdRole,$NamaRole){

		//kamus
		global $db;
		global $userdata;

		$sql =
      "UPDATE $this->TABEL SET NamaRole='$NamaRole'
				WHERE IdRole=$IdRole";

		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);
		}

		return true;
	}
	
	function hapus($list_id_role){
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA DARI DATABASE
		$sql =
			"DELETE FROM $this->TABEL
			WHERE IdRole IN($list_id_role);";
								
		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($IdRole){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM $this->TABEL
			WHERE IdRole='$IdRole';";
		
		if (!$result = $db->sql_query($sql,TRUE)){
			echo("Err:$this->ID_FILE ".__LINE__);
		}

    $row=$db->sql_fetchrow($result);

    return $row;
		
	}//  END ambilDataDetail

}
?>