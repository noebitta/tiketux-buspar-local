<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 411;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

//PARAMETER
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$cari   		= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$mode           = $mode==""?"exp":$mode;
switch ($mode){
    case 'InsertData':
        $id  = $HTTP_GET_VARS['id'];
        //ambil detail biaya petty cash dari
        $sql = "SELECT * FROM tbl_biaya_petty_cash WHERE IdBiayaPettyCash = '$id'";
        if (!$result = $db->sql_query($sql)){
            echo("Error:".__LINE__);exit;
            //die(mysql_error());
        }
        $row = $db->sql_fetchrow($result);
        //update status biaya petty cash menjadi sudah di toptup
        $sql = "UPDATE tbl_biaya_petty_cash SET TopUP = 1 WHERE IdBiayaPettyCash = '$id'";
        if (!$result = $db->sql_query($sql)){
            echo("Error:".__LINE__);exit;
            //die(mysql_error());
        }
        // update saldo petty cash cabang
        $kas = $row['Jumlah'];
        $kodecabang = $row['KodeCabang'];
        $sql = "UPDATE tbl_md_cabang SET SaldoPettyCash = SaldoPettyCash+$kas WHERE KodeCabang = '$kodecabang'";
        if (!$result = $db->sql_query($sql)){
            echo("Error:".__LINE__);exit;
            //die(mysql_error());
        }
        $notif ="<tr><td colspan=3 bgcolor='98e46f' align='center'><font size='4'>SALDO TELAH DI TOPUP</font></td></tr>";
    case 'topup':
      $cabang = $HTTP_GET_VARS['cabang'];
      $sql =
        "SELECT *
        FROM tbl_biaya_petty_cash
        JOIN tbl_md_cabang ON tbl_biaya_petty_cash.KodeCabang = tbl_md_cabang.KodeCabang
        WHERE TglCetak IS NOT NULL AND tbl_biaya_petty_cash.KodeCabang = '$cabang' AND TopUP = 0
        ORDER BY TglBuat DESC  ";

      if (!$result = $db->sql_query($sql)){
        echo("Error:".__LINE__);exit;
        //die(mysql_error());
      }

      $i=1;

      while($row = $db->sql_fetchrow(($result))){
        $template->assign_block_vars(
          'ROW',array(
            'No'        =>$i,
            'KodeCabang'=>$row['Nama'],
            'Pembuat'   =>$row['NamaPembuat'],
            'Releaser'  =>$row['NamaReleaser'],
            'Penerima'  =>$row['NamaPenerima'],
            'Pencetak'  =>$row['NamaPencetak'],
            'JenisBiaya'=>$row['JenisBiaya'],
            'Keterangan'=>$row['Keterangan'],
            'Jml'       =>number_format($row['Jumlah'],0,',','.'),
            'TglBuat'   =>date_format(date_create($row['TglBuat']),'d-m-Y H:i:s'),
            'TglCetak'  =>date_format(date_create($row['TglCetak']),'d-m-Y H:i:s'),
            'TOPUP'     =>'<a href="'.append_sid('petty_cash.'.$phpEx.'?mode=InsertData&id='.$row['IdBiayaPettyCash'].'&cabang='.$row['KodeCabang']).'"><input type="button" class="tombol" value="TOP UP"></a>'
          )
        );

        $i++;
      }

      $page_title = "Top Up Petty Cash";
      $template->set_filenames(array('body' => 'top_up_petty_cash_body.tpl'));

      $template->assign_vars(array(
        'BCRUMP'    =>setBcrump($id_page),
        'PESAN'     => $notif
        )
      );

      include($adp_root_path . 'includes/page_header.php');
      $template->pparse('body');
      include($adp_root_path . 'includes/page_tail.php');
      exit;
    case 'exp':
      /*VIEW MODE*/
      $kondisi	=($cari=="")?"":" WHERE (KodeCabang LIKE '%$cari%' OR Nama LIKE '%$cari' OR Alamat LIKE '%$cari' OR Kota LIKE '%$cari' OR Telp LIKE '%$cari%')";

      $sql = "SELECT * FROM tbl_md_cabang $kondisi ORDER BY Nama ASC";

      if (!$result = $db->sql_query($sql)){
        echo("Error:".__LINE__);exit;
        //die(mysql_error());
      }

      $i=1;

      while ($row = $db->sql_fetchrow($result)){
        $template->
        assign_block_vars(
            'ROW',
            array(
                'no'          =>$i,
                'KodeCabang'  =>$row['KodeCabang'],
                'Cabang'      =>$row['Nama'],
                'Kota'        =>$row['Kota'],
                'Saldo'       => number_format($row['SaldoPettyCash'],0,',','.'),
                'topup'        => '<a href="'.append_sid('petty_cash.'.$phpEx.'?mode=topup&cabang='.$row['KodeCabang']).'"><u>Detail Pengeluaran</u></a>'
            )
        );
        $i++;
      }
      $page_title = "Top Up Petty Cash";

      $template->set_filenames(array('body' => 'petty_cash_body.tpl'));

      $template->assign_vars(array(
            'BCRUMP'    		=>setBcrump($id_page),
            'ACTION_CARI'   => append_sid('petty_cash.'.$phpEx),
        )
    );
    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');
    exit;
}
?>