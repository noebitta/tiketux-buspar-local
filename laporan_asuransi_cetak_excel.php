<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$id_page = 0;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari  					= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Asuransi	= new Asuransi();
$Jurusan	= new Jurusan();
$User			= new User();

$kondisi_sort	= ($sort_by=='') ?"ORDER BY WaktuTransaksi $order" : "ORDER BY $sort_by $order";
		
$temp_cari_jurusan	= explode('-',$cari);

$cari_asal		= $temp_cari_jurusan[0];
$cari_tujuan	= $temp_cari_jurusan[1];

$kondisi	=($cari=="")?"WHERE 1 ":
	" WHERE (Nama LIKE '%$cari%'  
		OR Telp LIKE '%$cari%'
		OR HP LIKE '%$cari%'
		OR (f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) LIKE '%$cari_asal%' AND 
			 (f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) LIKE '%$cari_tujuan%'))
		OR f_user_get_nama_by_userid(PetugasTransaksi) LIKE '%$cari%')";

$kondisi.=
	" AND (WaktuTransaksi BETWEEN '$tanggal_mulai_mysql 00:00:00' AND '$tanggal_akhir_mysql 23:59:59') AND FlagBatal!=1";
		
		
//QUERY
$sql = 
	"SELECT *
	FROM tbl_asuransi $kondisi 
	$kondisi_sort ";
						
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Peserta Asuransi per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Waktu Transaksi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Plan Asuransi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Besar Premi');
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'No. Tiket');
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Tanggal Berangkat');
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jurusan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Nama');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Telp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Tanggal Lahir');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Penutup');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		
		$idx=0;
		
		while ($row = $db->sql_fetchrow($result)){
			$idx++;
			$idx_row=$idx+3;
			
			//mengambil jurusan
			$data_jurusan	= $Jurusan->ambilDataDetail($row['IdJurusan']);
			
			//mengambil data user
			$data_user		= $User->ambilDataDetail($row['PetugasTransaksi']);
			
			//mengambil data plan asuransi
			$data_plan_asuransi	= $Asuransi->ambilDataDetail($row['PlanAsuransi']);
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparse(FormatMySQLDateToTgl($row['WaktuTransaksi'])));
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $data_plan_asuransi['NamaPlan']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['BesarPremi']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NoTiket']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])));
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $data_jurusan['NamaCabangAsal']."-".$data_jurusan['NamaCabangTujuan']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row,$row['Nama']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['Telp']."/".$row['HP']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglLahir'])));
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $data_user['nama']);
			
		}
		$temp_idx=$idx_row;
		
		$idx_row++;		
		
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row,'=SUM(D4:D'.$temp_idx.')');
		
			
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Laporan Peserta Asuransi per Tanggal '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
