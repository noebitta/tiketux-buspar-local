<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","laporan_keuangan_tiketux.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		/*if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }*/
		
		new Ajax.Updater("rewrite_tujuan","laporan_keuangan_tiketux.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Keuangan Tiketux</td></tr>
			<tr class='banner' height=40>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'><select id='filterstatus' name='filterstatus'><option value='' {OPT_STATUS}>-semua jenis trx-</option><option value=0 {OPT_STATUS0}>Deposit</option><option value=1 {OPT_STATUS1}>Settlement</option></select></td>
							<td class='bannernormal'><select onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select></td>
							<td class='bannernormal'>Asal:&nbsp;</td><td><div id='rewrite_asal'></div></td>
							<td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<!--<td>
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td bgcolor='D0D0D0'></td>-->
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<table class="border">
									<tr>
										<td><b>Total Pesanan</b></td><td width="1">:</td><td align="right">{TOTAL_PESANAN}</td>
									</tr>
									<tr>
										<td><b>Total Tiket</b></td><td width="1">:</td><td align="right">{TOTAL_TIKET} tiket</td>
									</tr>
									<tr>
										<td><b>Total Penjualan</b></td><td width="1">:</td><td align="right">Rp. {TOTAL_PENJUALAN}</td>
									</tr>
									<tr>
										<td><b>Total Komisi</b></td><td width="1">:</td><td align="right" style="color: red;">Rp. {TOTAL_KOMISI}</td>
									</tr>
									<tr><td height=1 bgcolor=red colspan=3></td></tr>
									<tr>
										<td><b>Total Penerimaan</b></td><td>:</td><td align="right">Rp. {TOTAL_PENERIMAAN}</td>
									</tr>
								</table>
							</td>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
			 <th width=30	><a class="th" >No</th>
			 <th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'	>Bayar</th>
			 <th width=150><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>#Booking</th>
			 <th width=200><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'	>#Tiket</th>
			 <th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'	>Berangkat</th>
			 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'	>#Jadwal</th>
			 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'	>Nama</th>
			 <th width=50	><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'	>Kursi</th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'	>Harga Tiket</th>
			 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'	>Komisi</th>
			 <th width=100><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'	>Total</th>
			 <!--<th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'	>Tipe Disc.</th>-->
			 <th width=100><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'	>Status</th>
			 <th width=100><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'	>Ket.</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.waktu_cetak_tiket}</div></td>
       <td><div align="left">{ROW.kode_booking}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
       <td><div align="left">{ROW.waktu_berangkat}</div></td>
       <td><div align="left">{ROW.kode_jadwal}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			 <td><div align="center">{ROW.no_kursi}</div></td>
			 <td><div align="right">{ROW.harga_tiket}</div></td>
			 <td><div align="right">{ROW.komisi}</div></td>
			 <td><div align="right">{ROW.total}</div></td>
			 <!--<td><div align="left">{ROW.tipe_discount}</div></td>-->
       <td><div align="center">{ROW.status}</div></td>
       <td><div align="left">{ROW.ket}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>