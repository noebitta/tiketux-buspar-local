<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }

    function setData(bulan){
        tahun	=document.getElementById('tahun').value;

        window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun+'&asal='+asal.value+'&tujuan='+tujuan.value;
    }

    function getUpdateAsal(kota){

        new Ajax.Updater("rewrite_asal","laporan_rekap_jurusan.php?sid={SID}", {
            asynchronous: true,
            method: "get",

            parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
            onLoading: function(request){
            },
            onComplete: function(request){
                Element.show('rewrite_asal');
            },
            onFailure: function(request){
                assignError(request.responseText);
            }
        });
    }

    function getUpdateTujuan(asal){
        // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]

        new Ajax.Updater("rewrite_tujuan","laporan_rekap_jurusan.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
                    onLoading: function(request)
                    {
                    },
                    onComplete: function(request)
                    {
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    getUpdateAsal("{KOTA}");
    getUpdateTujuan("{ASAL}");


</script>

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Jurusan</td>
                    <td class="bannerjudul" align="right">
                        <form action="{URL}" method="post">
                            <table>
                                <tr>
                                    <td class='bannernormal'>Asal:&nbsp;</td><td><div id='rewrite_asal'></div></td>
                                    <td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div></td><td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <table width='100%'>
                            <tr>
                                <td colspan="2" align='right' valign='bottom'>
                                    <form action="{ACTION_CARI}" method="get">
                                        {LIST_BULAN}
                                        &nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table width="100%" class="border">
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Jurusan</th>
                    <th colspan="{JML_HARI}">TIKET</th>
                    <td width="10" bgcolor="red"></td>
                    <th colspan="{JML_HARI}">PAKET</th>
                </tr>
                <tr>
                    <!-- BEGIN HARI -->
                    <th>{HARI.hari}</th>
                    <!-- END HARI -->
                    <td width="10" bgcolor="red"></td>
                    <!-- BEGIN HARI -->
                    <th>{HARI.hari}</th>
                    <!-- END HARI -->
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td>{ROW.no}</td>
                    <td>{ROW.jurusan}</td>
                    {ROW.jml_tiket}
                    <td width="10" bgcolor="red"></td>
                    {ROW.jml_paket}
                </tr>
                <!-- END ROW -->
                <tr>
                    <th colspan="2">
                       TOTAL
                    </th>
                    <!-- BEGIN HARI -->
                    <th>{HARI.sum_tiket}</th>
                    <!-- END HARI -->
                    <td width="10" bgcolor="red"></td>
                    <!-- BEGIN HARI -->
                    <th>{HARI.sum_paket}</th>
                    <!-- END HARI -->
                </tr>
            </table>
        </td>
    </tr>
</table>