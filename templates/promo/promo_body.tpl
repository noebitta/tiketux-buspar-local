<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_promo.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_promo="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			
			if(request.responseText==1){
				window.location.reload();
				deselectAll();
			}
			else if(request.responseText==0){
				alert("Anda tidak memiliki akses untuk menghapus data ini!");
			}
			else{
				alert("Terjadi kegagalan!");
			}
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_promo.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&kode_promo="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Promo</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
					</form>
				</td>
			</tr>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_ADD}">[+]Tambah Promo</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Promo</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
		<table width='100%' class="border">
    <tr>
       <th width=30></th>
       <th width=30>No</th>
			 <th width=200>Nama</th>
			 <th width=100>Kode</th>
			 <th width=100>Mulai</th>
			 <th width=100>Hingga</th>
			 <th width=200>Asal</th>
			 <th width=200>Tujuan</th>
			 <th width=100>Jam Mulai</th>
			 <th width=100>Jam Hingga</th>
			 <th width=100>Target</th>
			 <th width=100>Jenis</th>
			 <th width=100>Discount</th>
			 <th width=100>Aktif</th>
			 <th width=150>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="center">{ROW.check}</div></td>
       <td><div align="center">{ROW.no}</div></td>
       <td><div align="center">{ROW.nama}</div></td>
       <td><div align="center">{ROW.kode_promo}</div></td>
       <td><div align="center">{ROW.berlaku_mulai}</div></td>
       <td><div align="center">{ROW.berlaku_hingga}</div></td>
       <td><div align="center">{ROW.asal}</div></td>
       <td><div align="center">{ROW.tujuan}</div></td>
       <td><div align="center">{ROW.jam_mulai}</div></td>
       <td><div align="center">{ROW.jam_hingga}</div></td>
       <td><div align="center">{ROW.target}</div></td>
       <td><div align="center">{ROW.jenis}</div></td>
       <td><div align="right">{ROW.jumlah}</div></td>
       <td><div align="center">{ROW.aktif}</div></td>
       <td><div align="center">{ROW.action}</div></td>
     </tr>  
     <!-- END ROW -->
		 {NO_DATA}
    </table>
    <table width='100%'>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_ADD}">[+]Tambah Promo</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Promo</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>