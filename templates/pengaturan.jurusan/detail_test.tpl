<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 52px;
        height: 24px;
        border-bottom: 1px solid transparent;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 19px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: red;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

<span class="popuptitle">Master Jurusan</span>


    <input type="hidden" name="id_jurusan" id="id_jurusan" value="{ID_JURUSAN}">
    <input type="hidden" name="kode_jurusan_old" value="{KODE_JURUSAN_OLD}">
    <table width=100%" cellspacing="0" cellpadding="0" border="0" style="margin-top: -30px; padding-left: 20px;">
        <input type="hidden" id="dlgcabangasal" value="{CABANG_ASAL}"/>
        <input type="hidden" id="dlgcabangtujuan" value="{CABANG_TUJUAN}"/>
        <input type="hidden" id="dlgstatusaktif" value="{STATUS_AKTIF}">
        <tr>
            <td>
                <table>
                    <tr>
                        <td><h3>{JUDUL}</h3></td>
                    </tr>

                    <tr>
                        <input type="hidden" name="id_jurusan" value="{ID_JURUSAN}">
                        <input type="hidden" name="kode_jurusan_old" value="{KODE_JURUSAN_OLD}">

                        <td>
                            <u>Kode Jurusan</u>
                        </td>
                        <td>:</td>
                        <td>
                            <input type="text" id="dlgkode" name="kode_jurusan" value="{KODE_JURUSAN}" size="30"><br>
                            <span id='pesan' style='display:none'>
								<font color=red>&nbsp;
									<b>(X)</b>
								</font>
							</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Asal
                        </td>
                        <td>:</td>
                        <td>
                            <span class="popupinput" width="30px">
								<span id="dlgselcabangasal" class="combolistselection" onClick="setComboList(this,'dlgcabangasal','pengaturan.jurusan.php','mode=7',false,true,true); dlgselcabangtujuan.innerHTML='';">&nbsp;
								</span>
                                </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tujuan
                        </td>
                        <td>:</td>
                        <td>
                            <span class="popupinput" size="30">
								<span id="dlgselcabangtujuan" class="combolistselection" onClick="if(dlgcabangasal.value!='') setComboList(this,'dlgcabangtujuan','pengaturan.jurusan.php','mode=7&cabangasal='+dlgcabangasal.value,false,true,true);">&nbsp;
								</span>
							</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status Aktif
                        </td>
                        <td>:</td>
                        <td>
                            <!--<label class="switch">
                                <input type="checkbox" id="is_aktif" name="is_aktif" {IS_AKTIF}>
                                <div class="slider"></div>
                            </label>-->

                            <span class="popupinput"><a href="" id="dlgbtnstatusaktif" onclick="toggleStatusAktif();return false;">&nbsp;AKTIF&nbsp;</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Jenis Jurusan
                        </td>
                        <td>:</td>
                        <td>
                            <select id="flag_jenis" name="flag_jenis">
                                <option value=1 {JENIS_1}>LUAR KOTA</option>
                                <option value=0 {JENIS_0}>DALAM KOTA</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Operasional untuk
                        </td>
                        <td>:</td>
                        <td>
                            <select id="flag_op_jurusan" name="flag_op_jurusan" onChange="lockUnlockHarga(this.value);">
                                <option value=0 {OP_0}>Travel</option>
                                <option value=1 {OP_1}>Paket</option>
                                <option value=2 {OP_2}>Travel & Paket</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Estimasi waktu tempuh
                        </td>
                        <td>:</td>
                        <td>
                            <input type="text" id="estimasi_waktu_tempuh" name="estimasi_waktu_tempuh"> jam
                        </td>
                    </tr>
                    <tr>
                        <td colspan='10'>
                            <hr width="450px">
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan=3>
                            <h3>Biaya-biaya:</h3>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3>
                            <i style="font-size: 9px; color: red;">** kolom bbm rupiah dan extra di isi nilai rupiah</i><br>
                            <i style="font-size: 9px; color: red;">** kolom bbm liter dan extra di isi angka</i>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="2" width="80px">Layout</th>
                        <th rowspan="2" width="80px">Harga Tiket</th>
                        <th colspan="4" width="40px">BBM</th>
                    <tr>
                        <th width="40px">Liter</th>
                        <th width="40px">Ext</th>
                        <th width="40px">Rp.</th>
                        <th width="40px">Ext</th>
                    </tr>
                    </tr>

                        <!-- BEGIN DATA_LAYOUT -->
                        <tr align="center">
                            <td>{DATA_LAYOUT.kodelayout}
                                <input type="hidden" name="idxlayout[]" value="{DATA_LAYOUT.idlayout}">
                            </td>
                            <td><input type="text" name="hargatiket[]" placeholder="Rp.."></td>
                            <td><input type="text" name="bbmLiter[]" size="5" placeholder="Rp.."></td>
                            <td><input type="text" name="bbmLiterExt[]" size="5" placeholder="Rp.."></td>
                            <td><input type="text" name="bbmRupiah[]" size="5" placeholder="Rp.."></td>
                            <td><input type="text" name="bbmRupiahExt[]" size="5" placeholder="Rp.."></td>
                        </tr>
                        <!-- END DATA_LAYOUT -->

                </table>
                <br>
                <table>
                    <tr>
                        <td>Pakai Voucher</td>
                        <td>:</td>
                        <td>
                            <input type='checkbox' id='flag_biaya_voucher_bbm' name='flag_biaya_voucher_bbm' {FLAG_BIAYA_VOUCHER_BBM} />
                            <span class='noticeMessage'>Voucher BBM</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Biaya Sopir
                        </td>
                        <td>:</td>
                        <td>
                            <input type="text" id="biaya_sopir" name="biaya_sopir" value="{BIAYA_SOPIR}" placeholder="Rp..">
                            <span id='biaya_sopir_invalid' style='display:none;'>
								<font color=red>&nbsp;
									<b>(X)</b>
								</font>
							</span>
                            <input type='checkbox' id='flag_biaya_sopir_kumulatif' name='flag_biaya_sopir_kumulatif' {FLAG_BIAYA_SOPIR_KUMULATIF} />
                            <span class='noticeMessage'>Dibayar kumulatif</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Biaya Tol
                        </td>
                        <td>:</td>
                        <td>
                            <input type="text" id="biaya_tol" name="biaya_tol" value="{BIAYA_TOL}" maxlength=8 onChange="Element.hide('biaya_tol_invalid');" placeholder="Rp..">
                            <span id='biaya_tol_invalid' style='display:none;'>
								<font color=red>&nbsp;
									<b>(X)</b>
								</font>
							</span>
                            <input type="text" id="biaya_tol_extra" name="biaya_tol_extra" value="{BIAYA_TOL_EXTRA}" placeholder="tol extra Rp.." size="20px" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Biaya Parkir
                        </td>
                        <td>:</td>
                        <td>
                            <input type="text" id="biaya_parkir" name="biaya_parkir" value="{BIAYA_PARKIR}" maxlength=8 onChange="Element.hide('biaya_parkir_invalid');" placeholder="Rp..">
                            <span id='biaya_parkir_invalid' style='display:none;'>
								<font color=red>&nbsp;
									<b>(X)</b>
								</font>
							</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10">
                            <hr width="450px">
                        </td>
                    </tr>
                    <tr>
                        <td>Lokasi Pengisian BBM</td><td>:</td>
                        <td>
                            <select id="spbu" name="spbu">
                                <option value="0">-----PILIH SPBU-----</option>
                                <!-- BEGIN DATA_SPBU -->
                                <option value={DATA_SPBU.idpombensin}>{DATA_SPBU.pombensin}</option>
                                <!-- END DATA_SPBU -->
                            </select>

                        </td>
                    </tr>
                </table>
            </td>
            <td width=1 bgcolor='D0D0D0'>q</td>
            <td align='center' valign='top' width='500' style=" padding-left: 30px; padding-right: 20px;">
                <table width='500'>
                    <tr>

                        <td colspan=3>
                            <h3>Harga Kirim Paket:</h3>
                        </td>
                    </tr>

                    <!-- BEGIN PAKET_INPUT -->
                    <tr>
                        <td valign='top'>Paket <b>{PAKET_INPUT.paketname}</b>
                            <input type="hidden" name="idxpaketlayout[]" value="{PAKET_INPUT.paketnamelayout}">
                        </td>
                        <td  valign='top'>:</td>
                        <td valign='top'>
                            Rp.&nbsp;
                            <input type="text" id="harga_paket_kilo_pertama[]" name="harga_paket_kilo_pertama[]" value="{HARGA_PAKET_1_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama
                            <br>
                            Rp.&nbsp;
                            <input type="text" id="harga_paket_kilo_berikut[]" name="harga_paket_kilo_berikut[]" value="{HARGA_PAKET_1_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya
                            <hr>
                        </td>
                    </tr>
                    <!-- END PAKET_INPUT -->

                    <tr>
                        <td colspan='3'>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <h3>Komisi:</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>Komisi Sopir</td>
                        <td>:</td>
                        <td>
                            Rp.&nbsp;
                            <input type="text" size="12" id="komisi_sopir_per_paket" name="komisi_sopir_per_paket">
                            /Paket
                        </td>
                    </tr>
                    <!--<tr><td colspan=3><br><h3>Kode-kode Akun:</h3></td></tr>
					<tr>
						<td width='200'>Kode Akun Pendapatan Penumpang</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_pendapatan_penumpang" name="kode_akun_pendapatan_penumpang" value="{KODE_AKUN_PENDAPATAN_PENUMPANG}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Pendapatan Paket</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_pendapatan_paket" name="kode_akun_pendapatan_paket" value="{KODE_AKUN_PENDAPATAN_PAKET}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Biaya Sopir</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_sopir" name="kode_akun_biaya_sopir" value="{KODE_AKUN_BIAYA_SOPIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Biaya Tol</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_tol" name="kode_akun_biaya_tol" value="{KODE_AKUN_BIAYA_TOL}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='300'>Kode Akun Biaya Parkir</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_parkir" name="kode_akun_biaya_parkir" value="{KODE_AKUN_BIAYA_PARKIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='300'>Kode Akun Biaya BBM</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_bbm" name="kode_akun_biaya_bbm" value="{KODE_AKUN_BIAYA_BBM}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi Sopir/Penumpang</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_penumpang_sopir" name="kode_akun_komisi_penumpang_sopir" value="{KODE_AKUN_KOMISI_PENUMPANG_SOPIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi CSO/Penumpang</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_penumpang_cso" name="kode_akun_komisi_penumpang_cso" value="{KODE_AKUN_KOMISI_PENUMPANG_CSO}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi Sopir/Paket</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_paket_sopir" name="kode_akun_komisi_paket_sopir" value="{KODE_AKUN_KOMISI_PAKET_SOPIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi CSO/Paket</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_paket_cso" name="kode_akun_komisi_paket_cso" value="{KODE_AKUN_KOMISI_PAKET_CSO}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Charge</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_charge" name="kode_akun_charge" value="{KODE_AKUN_CHARGE}" maxlength=20>
						</td>
					</tr>-->
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="mode" value="{MODE}">
                <input type="hidden" name="submode" value="{SUB}">
            </td>
        </tr>
    </table>
<BR>

<center>
    <div class="wrapperpopup">
        <span style="float:left; width: 95%; display: inline-block;">
            <span class="flatbutton" onClick="javascript: history.back();">KEMBALI</span>
            <span class="flatbutton" onclick="simpans();" id="buttonproses">SIMPAN</span>
        </span>
    </div>

</center>

