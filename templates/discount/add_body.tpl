<script language="JavaScript">

var kode;

function validateInput(){

	valid=true;

	Element.hide('kode_kendaraan_invalid');
	Element.hide('no_polisi_invalid');
	Element.hide('km_invalid');
	Element.hide('tahun_pembuatan_invalid');

	kode_kendaraan		= document.getElementById('kode_kendaraan');
	no_polisi					= document.getElementById('no_polisi');
	no_stnk						= document.getElementById('no_stnk');
	km								= document.getElementById('km');
	tahun_pembuatan		= document.getElementById('tahun_pembuatan');

	if(kode_kendaraan.value==''){
		valid=false;
		Element.show('kode_kendaraan_invalid');
	}

	if(no_polisi.value==''){
		valid=false;
		Element.show('no_polisi_invalid');
	}

	if(!cekValTahun(tahun_pembuatan.value)){
		valid=false;
		Element.show('tahun_pembuatan_invalid');
	}

	cek_value=km.value*0;

	if(cek_value!=0){
		valid=false;
		Element.show('km_invalid');
	}

	if(km.value==''){
		km.value=0;
	}

	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>
<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script><table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">

<form name="frm_data_mobil" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Promo</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='800'>
				<table width='800'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
						<td width="200">Kode Discount</td><td width='10'>:</td>
						<td width="450">
							<input type="hidden" name="kode_diskon_old" id="kode_diskon_old" value='{KODE_DISKON_OLD}'/>
							<input type="text" name="kode_diskon" id="kode_diskon" value='{KODE_DISKON}'/>
						</td>
					</tr>
					<tr>
						<td>Nama Discount</td><td>:</td><td><input type="text" name="nama_diskon" id="nama_diskon" value='{NAMA_DISKON}'/></td>
					</tr>
					<tr>
						<td><select id="flagdiscount" name="flagdiscount"><option value="0" {FLAGDISCOUNT0}>Discount</option><option value="1" {FLAGDISCOUNT1}>Harga</option></select></td><td>:</td>
						<td><input type="text" name="discount" id="discount" onkeypress='validasiAngka(event);' value='{DISCOUNT}'/>(>1:rupiah; <=1:persen)</td>
					</tr>
					<tr>
			      		<td>Status Aktif</td><td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
			    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>