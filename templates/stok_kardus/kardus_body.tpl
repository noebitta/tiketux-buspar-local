<script language="JavaScript"  src="{TPL}/js/stok_kardus.js"></script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
		 <td class="whiter" valign="middle" align="center">		
				<table width='100%' cellspacing="0">
					<tr class='banner' height=40>
						<td align='center' valign='middle' class="bannerjudul">&nbsp;Stok Kardus</td>
						<td colspan=2 align='right' class="bannernormal" valign='middle'>
							<br>
							<form action="{ACTION_CARI}" method="post">
								Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />&nbsp;<input type="submit" value="cari" />&nbsp;
							</form>
						</td>
					</tr>
					<tr>
						<td width='30%' align='left'>
							<a href="#" onclick='ShowJenisKardus()'>[+]Tambah Jenis Kardus</a>
						</td>
					</tr>
				</table>
				<table width='100%' class="border">
		    <tr style="background:grey; color:white;" align=center>
				<td width="20px" rowspan="2">No</td>
				<td width="30px" rowspan="2">Kode Cabang</td>
				<td width=100 rowspan="2">Nama Cabang</td>
				<td width=100 rowspan="2"><center>Kota<center></td>
				<td colspan="4">Stok</td>
				<td width=100 rowspan="2">Action</td>
		     </tr>
		     <tr style="background:grey;color:white;" align=center>
		     	<td width="10">S</td>
		     	<td width="10">R</td></td>
		     	<td width="10">M</td>
		     	<td width="10">H</td>

		     </tr>
		     <!-- BEGIN ROW -->
		     <tr class="{ROW.odd}">
		       	<td><div align="right">{ROW.no}</div></td>
				<td><div align="left">{ROW.kodecabang}</div></td>
				<td><div align="left">{ROW.namacabang}</div></td>
				<td><div align="left">{ROW.kota}</div></td>
				{ROW.stok}
		       <td><div align="center">{ROW.action}</div></td>
		     </tr>  
		     <!-- END ROW -->
		    </table>
		    <table width='100%'>
					<tr>
						<td width='30%' align='left'>
							<a href="#" onclick='ShowJenisKardus()'>[+]Tambah Jenis Kardus</a>
						</td>
					</tr>
				</table>
		 </td>
		</tr>
</table>


<!--dialog jenis kardus-->
<div dojoType="dialog" id="dialog_JenisKardus" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='500'>
		<tr>
			<td><h1>Master Jenis Kardus</h1></td>
		</tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="jenis_kardus"></div>
				<span id='progress_jenis_kardus' style='display:none;'><img src='{TPL}images/loading.gif' />
					<font size=2 color='ffffff'>sedang memproses...</font>
				</span>
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">  
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;"> &nbsp;
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog jenis kardus-->

<!--dialog tambah stok kardus-->
<div dojoType="dialog" id="dialog_TambahStok" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='500'>
		<tr><td><h1>Tambah Stok Kardus</h1></td></tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="tambah_stok_kardus"></div>
				<!--span id='progress_stok_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span-->
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">  
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog tambah stok kardus-->

<!--dialog stok opname kardus -->
<div dojoType="dialog" id="dialog_StokOpname" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='500'>
		<tr><td><h1>Stok Opname Kardus</h1></td></tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="stok_opname_kardus"></div>
				<span id='progress_opname_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">  
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog stok opname kardus -->

<!--dialog log history kardus -->
<div dojoType="dialog" id="dialog_LogHistory" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='800'>
		<tr><td><h1>Log History</h1></td></tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="log_history_kardus"></div>
				<span id='progress_log_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">  
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog log history kardus -->


