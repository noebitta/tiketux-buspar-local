<script type="text/javascript">
  filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">

  function selectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;

      if(chk=document.getElementById(str_var)){
        chk.checked=true;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function deselectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;
      if(chk=document.getElementById(str_var)){
        chk.checked=false;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function simpan(){

    var inputvalid=true;

    inputvalid=validasiInput(nilaivoucher) && inputvalid;

    if(document.getElementById("jumlahcetak")){
      inputvalid= validasiInput(document.getElementById("jumlahcetak")) && inputvalid;
      sendjumcetak    = document.getElementById("jumlahcetak").value;
    }
    else{
      sendjumcetak    = 0;
    }

    inputvalid = validasiInput(keterangan) && inputvalid;

    if(!inputvalid){
      return;
    }

    sendkodevoucher = (document.getElementById("kodevoucher"))?document.getElementById("kodevoucher").value:"";

    sendnilaivoucher= (ispersen.value==0)?nilaivoucher.value:nilaivoucher.value/100;

    new Ajax.Request("voucher.paket.php", {
        asynchronous: true,
        method: "post",
        parameters: "mode=3&kodevoucher="+sendkodevoucher+
          "&kotaberlaku="+kotaberlaku.value+
          "&cabangasal="+cabangberangkat.value+
          "&cabangtujuan="+cabangtujuan.value+
          "&mulaiberlaku="+mulaiberlaku.value+
          "&expireddate="+expireddate.value+
          "&nilaivoucher="+sendnilaivoucher+
          "&ishargatetap="+jenisvoucher.value+
          "&isbolehweekend="+isbolehweekend.value+
          "&jumlahcetak="+sendjumcetak+
          "&keterangan="+keterangan.value,
        onLoading: function(request){

        },
        onComplete: function(request){

        },
        onSuccess: function(request){
          var result;

          result = JSON.parse(request.responseText);

          if(result["status"]=="OK") {
            formdata.submit();
          }
          else{
            alert("Gagal:"+result["error"]);
          }

        },
        onFailure: function(request){
          alert('Error');
          assignError(request.responseText);
        }
      });
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }


  function tambahData(){
    setPopup(500,420);

    new Ajax.Updater("popupcontent","voucher.paket.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1",
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function ubahData(kodevoucher){
    setPopup(500,420);

    new Ajax.Updater("popupcontent","voucher.paket.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2&kodevoucher="+kodevoucher,
      onLoading: function(request) {
      },
      onComplete: function(request) {
        if(jenisvoucher.value==1){ispersen.selectedIndex=0;ispersen.disabled=true;}else{ispersen.disabled=false}
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function hapusData(inkodevoucher=''){

    if(inkodevoucher!=''){
      list_dipilih=inkodevoucher;
    }
    else{
      i=1;
      loop=true;
      list_dipilih="";
      do{
        str_var='checked_'+i;

        if(chk=document.getElementById(str_var)){
          if(chk.checked){
            if(list_dipilih==""){
              list_dipilih +=chk.value;
            }
            else{
              list_dipilih +=","+chk.value;
            }
          }
        }
        else{
          loop=false;
        }
        i++;
      }while(loop);
    }

    if(list_dipilih==""){
      alert("Anda belum memilih voucher yang akan dihapus!");
      return false;
    }

    if(confirm("Apakah anda yakin akan menghapus voucher ini?")){

      new Ajax.Request("voucher.paket.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=8&kodevoucher="+list_dipilih,
        onLoading: function(request){
        },
        onComplete: function(request){
        },
        onSuccess: function(request){
          window.location.reload();
          deselectAll();
        },
        onFailure: function(request)
        {
        }
      })
    }

    return false;

  }

  function batalkan(){

    if(!validasiInput(document.getElementById("alasan"))){
      return false;
    }

    if(confirm("Apakah anda yakin akan membatalkan voucher ini?")){

      new Ajax.Request("voucher.paket.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=7&kodevoucher="+kodevoucher.value+"&alasan="+alasan.value,
        onLoading: function(request){
        },
        onComplete: function(request){
        },
        onSuccess: function(request){
          window.location.reload();
          deselectAll();
        },
        onFailure: function(request){
        }
      })
    }

    return false;

  }

  function setCabangAsal(kota){

    new Ajax.Updater("cabangberangkat","voucher.paket.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=5&kota="+kota,
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

  }

  function setCabangTujuan(cabangasal){

    new Ajax.Updater("cabangtujuan","voucher.paket.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=6&cabangasal="+cabangasal,
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

  }

  function showDialogBatal(inkodevoucher=""){

    if(inkodevoucher!=''){
      list_dipilih=inkodevoucher;
    }
    else{
      i=1;
      loop=true;
      list_dipilih="";
      do{
        str_var='checked_'+i;

        if(chk=document.getElementById(str_var)){
          if(chk.checked){
            if(list_dipilih==""){
              list_dipilih +="'"+chk.value+"'";
            }
            else{
              list_dipilih +=","+"'"+chk.value+"'";
            }
          }
        }
        else{
          loop=false;
        }
        i++;
      }while(loop);
    }

    if(list_dipilih==""){
      alert("Anda belum memilih voucher yang akan dibatalkan!");
      return false;
    }

    setPopup(500,210);

    new Ajax.Updater("popupcontent","voucher.paket.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=4&kodevoucher="+list_dipilih,
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
        popupwrapper.show();
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

  }

  function init(e){
     popupwrapper	= dojo.widget.byId("popupcontainer");
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER_BY}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Voucher Paket</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Filter Status <select id="filterstatus" name="filterstatus"><option value="" {FILSTS}>-semua-</option><option value="0" {FILSTS0}>belum digunakan</option><option value="1" {FILSTS1}>sudah digunakan</option><option value="2" {FILSTS2}>batal</option></select>
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="tambahData();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->

    <!-- BEGIN CRUD_BATAL -->
    <a href="" onClick="showDialogBatal();return false;" class="crud">batal</a>
    <!-- END CRUD_BATAL -->

    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="hapusData();return false" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  Data Voucher<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th rowspan="2"><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th rowspan="2"></th>
      <th rowspan="2">No</th>
      <th colspan="12">Data Voucher</th>
      <th colspan="5">Penggunaan</th>
      <th colspan="3">Pembatalan</th>
    </tr>
    <tr>
      <th onclick="setOrder(0)">Kode Voucher</th>
      <th onclick="setOrder(1)">Jenis</th>
      <th onclick="setOrder(2)">Nilai Voucher</th>
      <th onclick="setOrder(3)">Keterangan</th>
      <th onclick="setOrder(4)">Hari Berlaku</th>
      <th onclick="setOrder(5)">Kota Berlaku</th>
      <th onclick="setOrder(6)">Cabang Asal Berlaku</th>
      <th onclick="setOrder(7)">Cabang Tujuan Berlaku</th>
      <th onclick="setOrder(8)">Berlaku Mulai</th>
      <th onclick="setOrder(9)">Expired</th>
      <th >Dicetak Oleh</th>
      <th onclick="setOrder(10)">Waktu Cetak</th>
      <th onclick="setOrder(11)">Tgl.Berangkat</th>
      <th onclick="setOrder(12)">Jadwal</th>
      <th onclick="setOrder(13)">No.Resi</th>
      <th onclick="setOrder(14)">Digunakan Pada</th>
      <th >CSO</th>
      <th >Oleh</th>
      <th onclick="setOrder(15)">Waktu Batal</th>
      <th onclick="setOrder(16)">Alasan</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}" style="cursor: pointer;" onclick="checked_{ROW.idx}.checked=1-checked_{ROW.idx}.checked;">
        <td align="center"><input type="checkbox" id="checked_{ROW.idx}" name="checked_{ROW.kodevoucher}" value="{ROW.kodevoucher}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="ubahData('{ROW.kodevoucher}');" title="ubah data" ></span>&nbsp;
          <!-- END ACT_EDIT -->

          <!-- BEGIN ACT_BATAL -->
          <span class="b_delete" onclick="showDialogBatal('{ROW.kodevoucher}');" title="batalkan data" ></span>
          <!-- END ACT_BATAL -->

          <!-- BEGIN ACT_DEL -->
          <span class="b_bin" onclick="hapusData('{ROW.kodevoucher}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.kodevoucher}</td>
        <td align="center">{ROW.jenis}</td>
        <td align="center">{ROW.nilaivoucher}</td>
        <td align="center">{ROW.keterangan}</td>
        <td align="center">{ROW.hariberlaku}</td>
        <td align="center">{ROW.kotaberlaku}</td>
        <td align="center">{ROW.cabangasalberlaku}</td>
        <td align="center">{ROW.cabangtujuanberlaku}</td>
        <td align="center">{ROW.berlakumulai}</td>
        <td align="center">{ROW.expired}</td>
        <td align="center">{ROW.dicetakoleh}</td>
        <td align="center">{ROW.waktucetak}</td>
        <td align="center">{ROW.tglberangkat}</td>
        <td align="center">{ROW.jadwalberangkat}</td>
        <td align="center">{ROW.noresi}</td>
        <td align="center">{ROW.digunakanpada}</td>
        <td align="center">{ROW.csopengguna}</td>
        <td align="center">{ROW.dibatalkanoleh}</td>
        <td align="center">{ROW.waktubatal}</td>
        <td align="center">{ROW.alasan}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>
<script language="javascript">
  setClassArrowSort(orderby.value*1,sort.value,document.getElementById("tableheader"),1)
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>