<span class="popuptitle">{OPERATION} VOUCHER</span>
<center>
  <div class="wrapperpopup" style="height: 420px;width: 500px;">
    <!-- BEGIN SHOW_KODE_VOUCHER -->
    <span class="popuplabel">Kode Voucher</span><span class="popupinput"><input type="text" id="kodevoucher" name="kodevoucher" value="{KODE_VOUCHER}" readonly /></span><br><br>
    <!-- END SHOW_KODE_VOUCHER -->
    <span class="popuplabel">Kota Berlaku</span><span class="popupinput">
                                                                  <select id="kotaberlaku" name="kotaberlaku" onfocus="this.style.background='white';" onChange="setCabangAsal(this.value);">
                                                                    <option value="">-semua kota-</option>
                                                                    <!-- BEGIN OPT_KOTA_BERLAKU -->
                                                                    <option value="{OPT_KOTA_BERLAKU.value}" {OPT_KOTA_BERLAKU.selected}>{OPT_KOTA_BERLAKU.text}</option>
                                                                    <!-- END OPT_KOTA_BERLAKU -->
                                                                  </select>
                                                                </span><br><br>
    <span class="popuplabel">Berlaku Cabang Keberangkatan</span><span class="popupinput">
                                                                  <select id="cabangberangkat" name="cabangberangkat" onfocus="this.style.background='white';" onchange="setCabangTujuan(this.value);">
                                                                    <!-- BEGIN CABANG_BERANGKAT_DEFAULT -->
                                                                    <option value="">-semua kota-</option>
                                                                    <!-- END CABANG_BERANGKAT_DEFAULT -->
                                                                    {LIST_CABANG_ASAL}
                                                                  </select>
                                                                </span><br><br>
    <span class="popuplabel">Berlaku Cabang Tujuan</span><span class="popupinput">
                                                          <select id="cabangtujuan" name="cabangtujuan" onfocus="this.style.background='white';">
                                                            <!-- BEGIN CABANG_TUJUAN_DEFAULT -->
                                                            <option value="">-semua kota-</option>
                                                            <!-- END CABANG_TUJUAN_DEFAULT -->
                                                            {LIST_CABANG_TUJUAN}
                                                          </select>
                                                        </span><br><br>
    <span class="popuplabel">Berlaku Mulai</span><span class="popupinput"><input readonly="yes" class="drop_calendar" id="mulaiberlaku" name="mulaiberlaku" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_MULAI_BERLAKU}"></span><br><br>
    <span class="popuplabel">Expired</span><span class="popupinput"><input readonly="yes" class="drop_calendar" id="expireddate" name="expireddate" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{EXPIRED_DATE}"></span><br><br>
    <span class="popuplabel">Jenis Voucher</span><span class="popupinput"><select id="jenisvoucher" name="jenisvoucher" onfocus="this.style.background='white';" onchange="if(this.value==1){ispersen.selectedIndex=0;ispersen.disabled=true;}else{ispersen.disabled=false}" ><option value="0" {IS_HARGA_TETAP0}>Diskon</option><option value="1" {IS_HARGA_TETAP1}>Harga Tetap</option></select></span><br><br>
    <span class="popuplabel">Nilai Voucher</span><span class="popupinput"><select id="ispersen" name="ispersen"><option value="0" {IS_PERSEN0}>Rp.</option><option value="1" {IS_PERSEN1}>%</option></select><input type="text" id="nilaivoucher" name="nilaivoucher" value="{NILAI_VOUCHER}" onfocus="this.style.background='white';" onkeypress="validasiInputanAngka(event);"/></span><br><br>
    <span class="popuplabel">Hari Berlaku</span><span class="popupinput"><select id="isbolehweekend" name="isbolehweekend"><option value="1" {IS_BOLEH_WEEKEND1}>semua hari</option><option value="0" {IS_BOLEH_WEEKEND0}>weekday saja</option></select></span><br><br>
    <!-- BEGIN SHOW_JUMLAH_CETAK -->
    <span class="popuplabel">Jumlah Cetak</span><span class="popupinput"><input type="text" id="jumlahcetak" name="jumlahcetak" onfocus="this.style.background='white';" onkeypress="validasiInputanAngka(event);"/></span><br><br>
    <!-- END SHOW_JUMLAH_CETAK -->
    <span class="popuplabel">Keterangan</span><span class="popupinput"><textarea id="keterangan" name="keterangan" onfocus="this.style.background='white';" rows="2" cols="30">{KETERANGAN}</textarea></span><br><br>
    <span class="flatbutton" onclick="simpan();" id="buttonsave">SIMPAN</span>
  </div>
</center>