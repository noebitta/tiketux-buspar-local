<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<!--
<script src="{TPL}js/jquery.min.js" type="text/javascript"></script>
-->
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");
    function init(e){
        //control dialog
        dialog			    = dojo.widget.byId("dialog_set_jadwal");
        dlg_biaya_cancel    = document.getElementById("dialogcancel");

        dialogedit          = dojo.widget.byId("dialogedit");
        dlg_edit_cancel     = document.getElementById("dialogeditcancel");

        dlg_backup       = dojo.widget.byId("dialog_backup");
        dlg_backup_cancel   = document.getElementById("dialogcancel_backup");
    }
    dojo.addOnLoad(init);

    function setData(bulan){
        tahun	= document.getElementById('tahun').value;
        periode = document.getElementById('periode').value;
        cabang  = document.getElementById('cabang').value;
        window.location='{URL}'+'?&bulan='+bulan+'&tahun='+tahun+'&periode='+periode+'&cabang='+cabang;
    }

    // set jadwal sopir backup
    function setJadwalBackup(KodeSopir,Nama,Tanggal,Bulan,Tahun,IsReguler){
        document.getElementById('b_kodesupir').value = KodeSopir;
        document.getElementById('b_tanggal').value = Tanggal;
        document.getElementById('b_input_bulan').value = Bulan;
        document.getElementById('b_input_tahun').value = Tahun;
        document.getElementById('b_nama').innerHTML = Nama;
        document.getElementById('b_hidden_name').value = Nama;
        document.getElementById('statuskerja').value = IsReguler;

        if(IsReguler == 1){
            document.getElementById('b_status').innerHTML = "Reguler";
        }else{
            document.getElementById('b_status').innerHTML = "Backup";
        }

        new Ajax.Updater("b_rewrite_mobil","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=get_list_mobil" + "&KodeSopir="+KodeSopir,
                    onLoading: function(request)
                    {
                        Element.show('progressbar');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('progressbar');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });

        dlg_backup.show();
    }
    function Simpan(){
        if(statuskerja.value == ""){
            alert("Status Tidak Boleh Kosong")
        }else if(outlet.value == ""){
            alert("Outlet Terangkat Tidak Boleh Kosong");
        }else if(opt_tujuan.value == ""){
            alert("Outlet Tujuan Tidak Boleh Kosong");
        }else if(jam.value == ""){
            alert("Jam Keberangkatan Tidak Boleh Kosong");
        }else if(mobil.value == ""){
            alert("Mobil Tidak Boleh Kosong");
        }else{
            new Ajax.Request("pengaturan_jadwal_supir.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=InsertData"+
                "&KodeSopir="+b_kodesupir.value+
                "&Tgl="+b_tanggal.value+
                "&Bln="+b_input_bulan.value+
                "&Thn="+b_input_tahun.value+
                "&IdJurusan="+opt_tujuan.value+
                "&KodeJadwal="+jam.value+
                "&JamBerangkat="+JamBerangkat.value+
                "&NamaSupir="+b_hidden_name.value+
                "&StatusKerja="+statuskerja.value+
                "&Mobil="+mobil.value,
                onLoading: function(request){
                    progressbar.show();
                },
                onComplete: function(request){
                    //my_return = request.responseText;
                    //alert(my_return);
                    progressbar.hide();
                    dialog.hide();
                    console.log("oncomplete");
                    console.log(request);
                    document.forms["formcari"].submit();
                },
                onSuccess: function(request){
                    eval(request.responseText);

                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }
    }

    function Hapus(){
        new Ajax.Request("pengaturan_jadwal_supir.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=Hapus"+
            "&IdJadwalSopir="+IdJadwal.value,
            onLoading: function(request){
                progressbaredit.show();
            },
            onComplete: function(request){
                progressbaredit.hide();
                dialogedit.hide();
                document.forms["formcari"].submit();
            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }
    function getUpdateAsal(){

        new Ajax.Updater("b_rewrite_asal","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=get_asal",
                    onLoading: function(request)
                    {
                        Element.show('loading_asal');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_asal');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }
    function getUpdateTujuan(asal){
        // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]

        if(document.getElementById('b_rewrite_tujuan')){
            document.getElementById('b_rewrite_tujuan').innerHTML = "";
        }

        new Ajax.Updater("b_rewrite_tujuan","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "asal=" + asal + "&mode=get_tujuan",
                    onLoading: function(request)
                    {
                        Element.show('loading_tujuan');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_tujuan');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }
    function getUpdateJam(id_jurusan){
        if(document.getElementById('b_rewrite_jam')){
            document.getElementById('b_rewrite_jam').innerHTML = "";
        }

        new Ajax.Updater("b_rewrite_jam","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "id_jurusan=" + id_jurusan + "&mode=get_jam",
                    onLoading: function(request)
                    {
                        Element.show('loading_jam');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_jam');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }
    function ambiljam(kodejadwal){
        if(document.getElementById('b_rewrite_hidden_jam')){
            document.getElementById('b_rewrite_hidden_jam').innerHTML = "";
        }
        new Ajax.Updater("b_rewrite_hidden_jam","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "KodeJadwal=" + kodejadwal + "&mode=set_hidden_jam",
                    onLoading: function(request)
                    {
                        Element.show('loading_hidden_jam');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_hidden_jam');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    //fungsi untuk rewrite mode edit
    function ShowEditDialog(IdJadwal){

        document.getElementById('IdJadwal').value = IdJadwal;

        if(document.getElementById('rewriteEdit')){
            document.getElementById('rewriteEdit').innerHTML = "";
        }

        new Ajax.Updater("rewriteEdit","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=Edit" + "&idjadwal="+IdJadwal,
                    onLoading: function(request)
                    {
                        Element.show('progressbar');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('progressbar');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
        dialogedit.show();
    }
    function Edit(){
        new Ajax.Request("pengaturan_jadwal_supir.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=UpdateData"+
            "&IdJurusan="+editCabangTujuan.value+
            "&KodeJadwal="+KodeJAdwal.value+
            "&Mobil="+editmobil.value+
            "&IdJadwal="+IdJadwal.value,
            onLoading: function(request){
                progressbar.show();
            },
            onComplete: function(request){
                progressbar.hide();
                dialogedit.hide();
                document.forms["formcari"].submit();
            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }
    function getUpdateTujuanEdit(asal){

        if(document.getElementById('editCabangTujuan')){
            document.getElementById('editCabangTujuan').innerHTML = "";
        }
        new Ajax.Updater("editCabangTujuan","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "asal=" + asal + "&mode=get_tujuan",
                    onLoading: function(request)
                    {
                        Element.show('loading_tujuan');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_tujuan');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }
    function getUpdateJamEdit(id_jurusan){
        if(document.getElementById('KodeJAdwal')){
            document.getElementById('KodeJAdwal').innerHTML = "";
        }
        new Ajax.Updater("KodeJAdwal","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "id_jurusan=" + id_jurusan + "&mode=get_jam",
                    onLoading: function(request)
                    {
                        Element.show('loading_jam');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_jam');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }


    // set jadwal sopir reguler
    function setJadwal(KodeSopir,Nama,Tanggal,Bulan,Tahun) {
        document.getElementById('kodesupir').value = KodeSopir;
        document.getElementById('tanggal').value = Tanggal;
        document.getElementById('input_bulan').value = Bulan;
        document.getElementById('input_tahun').value = Tahun;
        document.getElementById('nama').innerHTML = Nama;
        document.getElementById('hidden_name').value = Nama;

        // set list mobil
        if(document.getElementById('rewrite_mobil')){
            document.getElementById('rewrite_mobil').innerHTML = "";
        }
        new Ajax.Updater("rewrite_mobil","pengaturan_jadwal_supir.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "KodeSopir=" + KodeSopir + "&mode=get_list_mobil",
                    onLoading: function(request)
                    {
                        Element.show('loading_tujuan');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_tujuan');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });

        dialog.show();
    }
    function simpanJadwal() {
        if(list_jadwal.value == ""){
            alert("SILAHKAN PILIH JADWAL TERLEBIH DAHULU");
        }else if(mobil.value == ""){
            alert("SILAHKAN PILIH MOBIL TERLEBIH DAHULU");
        }else{
            new Ajax.Request("pengaturan_jadwal_supir.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=setJadwal"+
                "&KodeSopir="+kodesupir.value+
                "&Tgl="+tanggal.value+
                "&Bln="+input_bulan.value+
                "&Thn="+input_tahun.value+
                "&NamaSupir="+hidden_name.value+
                "&Mobil="+mobil.value+
                "&kode_group="+list_jadwal.value,
                onLoading: function(request){
                    progressbar.show();
                },
                onComplete: function(request){
                    progressbar.hide();
                    dialog.hide();
                    console.log("oncomplete");
                    document.forms["formcari"].submit();
                },
                onSuccess: function(request){
                    eval(request.responseText);

                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }
    }

</script>
<!-- dialog tambah jadwal backup -->
<div dojoType="dialog" id="dialog_backup" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <input type="hidden" id="b_kodesupir" name="kodesupir">
                        <input type="hidden" id="b_tanggal" name="tanggal">
                        <input type="hidden" id="b_input_bulan" name="input_bulan">
                        <input type="hidden" id="b_input_tahun" name="input_tahun">
                        <input type="hidden" id="b_hidden_name" name="hidden_name">
                        <tr><td colspan='2' align="center"><h2>Penjadwalan Sopir</h2></td></tr>

                        <tr>
                            <td align="right" width="200">Supir :</td><td><b id="b_nama"></b></td>
                        </tr>
                        <tr>
                            <td align="right" width="200">Status :</td>
                            <td>
                                <b id="b_status"></b>
                                <input type="hidden" id="statuskerja">
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Mobil : </td>
                            <td>
                                <div id="b_rewrite_mobil"></div>
                            </td>
                        </tr>
                        <tr>
                            <td align='right' width="200">Outlet Berangkat : </td>
                            <td width="300">
                                <div id='b_rewrite_asal'></div>
                                <span id='loading_asal' style='display:none;'>
                                    <img src="{TPL}images/loading.gif"/>
                                    <font color='white' size=2>sedang memposes...</font>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Outlet Tujuan : </td>
                            <td>
                                <div id='b_rewrite_tujuan'></div>
                                <span id='loading_tujuan' style='display:none;'>
                                    <img src="{TPL}images/loading.gif"/>
                                    <font color='white' size=2>sedang memposes...</font>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Jam Berangkat : </td>
                            <td>
                                <div id='b_rewrite_jam'></div>
                                <span id='loading_jam' style='display:none;'>
                                    <img src="{TPL}images/loading.gif"/>
                                    <font color='white' size=2>sedang memposes...</font>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="b_rewrite_hidden_jam"></div>
                                <span id='loading_hidden_jam' style='display:none;'>
                                    <img src="{TPL}images/loading.gif"/>
                                    <font color='white' size=2>sedang memposes...</font>
                                </span>
                            </td>
                        </tr>
                        <script type="text/javascript">
                            getUpdateAsal();
                        </script>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogcancel_backup" value="&nbsp;Cancel&nbsp;" onClick="dlg_backup.hide();">
                    <input type="button" onclick="Simpan()" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!-- END dialog backup-->
<div dojoType="dialog" id="dialogedit" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="FormEdit">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <input type="hidden" id="IdJadwal">
                    <table id="rewriteEdit">
                        <tr><td colspan="2" align="center"><h2>Penjadwalan Sopir</h2></td></tr>

                        <tr><td align="right" width="200">Supir :</td><td><b id="edit_nama"></b></td></tr>
                        <tr><td align="right" width="200">Outlet Berangkat : </td>
                            <td width="300">

                            </td>
                        </tr>
                        <tr>
                            <td align="right">Outlet Tujuan : </td>
                            <td>

                            </td>
                        </tr>
                        <tr>
                            <td align="right">Jam Berangkat : </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <span id='progressbaredit' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <script language="JavaScript">
                        getUpdateTujuanEdit(editCabangAWAL.value);
                        getUpdateJamEdit(editCabangTujuan.value);
                    </script>
                    <input type="button" id="dialogeditcancel" value="&nbsp;Cancel&nbsp;" onClick="dialogedit.hide();">
                    <input type="button" onclick="Edit()"  id="dialogproses" value="&nbsp;Update&nbsp;">
                    <input type="button" onclick="Hapus()"  id="dialoghapus" value="&nbsp;Delete&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!-- END edit dialog-->
<!-- dialog tambah jadwal reguler -->
<div dojoType="dialog" id="dialog_set_jadwal" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table width="100%">
                        <input type="hidden" id="kodesupir" name="kodesupir">
                        <input type="hidden" id="tanggal" name="tanggal">
                        <input type="hidden" id="input_bulan" name="input_bulan">
                        <input type="hidden" id="input_tahun" name="input_tahun">
                        <input type="hidden" id="hidden_name" name="hidden_name">
                        <tr><td colspan='2' align="center"><h2>Penjadwalan Sopir</h2></td></tr>
                        <tr><td align="right" width="30%">Supir :</td><td width="70%"><b id="nama"></b></td></tr>
                        <tr>
                            <td align="right" width="30%">Judul Group Jadwal : </td>
                            <td width="70%">
                                <select name="list_jadwal" id="list_jadwal">
                                    {list_jadwal}
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="30%">Mobil : </td>
                            <td width="70%">
                                <div id="rewrite_mobil"></div>
                            </td>
                        </tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogcancel" value="&nbsp;Cancel&nbsp;" onClick="dialog.hide();">
                    <input type="button" onclick="simpanJadwal()"  id="dialogproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!-- END edit dialog reguler-->

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="center">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Penjadwalan Sopir</td>
                    <td colspan=2 align='right' class="bannernormal" valign='middle'>
                        <br>
                        <form action="{ACTION_CARI}" method="post" id="formcari">
                            <span id="tugas" style="display: inline;">
                            Periode :
                            <select id="periode" name="periode">
                                <option value="1" {PERIODE1}>Minggu 1 dan 2</option>
                                <option value="2" {PERIODE2}>Minggu 3 dan 4</option>
                            </select>
                            Bulan :
                            <select id="bulan" name="bulan">
                                {LIST_BULAN}
                            </select>
                            Tahun:
                            <input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />
                            Cabang Tugas Supir :
                            <select id="cabang" name="cabang" >
                                {CABANG}
                            </select>
                            </span>
                            <input type="submit" class="tombol" value="proses">
                        </form>
                    </td>
                </tr>
            </table>
            <!-- END HEADER-->
            <br>
            <br>
            <table width='100%'>
                <tr>
                    <th>Supir</th>
                    <!-- BEGIN HARI -->
                    <th>{HARI.tgl}</th>
                    <!-- END HARI -->
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td valign="middle">{ROW.Nama}</td>
                    {ROW.DATA}
                <tr>
                    <!-- END ROW -->

            </table>
            {NODATA}
        </td>
    </tr>
</table>
