<script type="text/javascript">

  function selectAll(){

        i=1;
        loop=true;
        record_dipilih="";
        do{
            str_var='checked_'+i;

            if(chk=document.getElementById(str_var)){
                chk.checked=true;
            }
            else{
                loop=false;
            }
            i++;
        }while(loop);

    }

  function deselectAll(){
        i=1;
        loop=true;
        record_dipilih="";
        do{
            str_var='checked_'+i;
            if(chk=document.getElementById(str_var)){
                chk.checked=false;
            }
            else{
                loop=false;
            }
            i++;
        }while(loop);

    }

  function simpan(){

    var inputvalid=true;

    inputvalid=validasiInput(dlgkodecabang) && inputvalid;
    inputvalid=validasiInput(dlgnama) && inputvalid;
    inputvalid=validasiInput(dlgalamat) && inputvalid;
    inputvalid=validasiInput(dlgkota) && inputvalid;
    inputvalid=validasiInput(dlgtelp) && inputvalid;
    inputvalid=validasiInput(dlgfax) && inputvalid;

    if(!inputvalid){
      return;
    }
    new Ajax.Request("pengaturan.cabang.php", {
      asynchronous: true,
      method: "post",
      parameters: "mode=3.1"+
      "&idcabang="+dlgidcabang.value+
      "&kodecabang="+dlgkodecabang.value+
      "&nama="+dlgnama.value+
      "&alamat="+dlgalamat.value+
      "&kota="+dlgkota.value+
      "&telp="+dlgtelp.value+
      "&fax="+dlgfax.value+
      "&isaktif="+dlgstatusaktif.value,

      onSuccess: function(request){

          if(isJson(request.responseText)){
              var result;
              result = JSON.parse(request.responseText);

              if(result["status"]=="OK") {
                  formdata.submit();
              }
              else{
                  alert("Gagal:"+result["error"]);
              }
          } else {
              alert("error:"+request.responseText);
          }


      },
      onFailure: function(request){
          alert('Error');
          assignError(request.responseText);
      }
    })
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function showDialogTambah(){
      setPopup(500,350);

    new Ajax.Updater("popupcontent","pengaturan.cabang.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=3",
      onLoading: function(request) {
      },
      onComplete: function(request) {
        toggleStatusAktif(true);
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function showDialogUbah(idcabang){
    setPopup(500,350);

    new Ajax.Updater("popupcontent","pengaturan.cabang.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2&idcabang="+idcabang,
      onLoading: function(request) {
        idcabang.value  = idcabang;
      },
      onComplete: function(request) {
          toggleStatusAktif(true);
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function toggleStatusAktif(inittoggle=false){

      if(!inittoggle){
          dlgstatusaktif.value  = 1 - dlgstatusaktif.value;
      }

      if(dlgstatusaktif.value==1){
          dlgbtnstatusaktif.className='crudgreen';
          dlgbtnstatusaktif.innerHTML='AKTIF';
      }else{
          dlgbtnstatusaktif.className='crud';
          dlgbtnstatusaktif.innerHTML='NON-AKTIF';
      }
  }

  function hapusData(idcabang=''){

        if(idcabang!=''){
            list_dipilih=idcabang;
        }
        else {
            i = 1;
            loop = true;
            list_dipilih = "";
            do {
                str_var = 'checked_' + i;

                if (chk = document.getElementById(str_var)) {
                    if (chk.checked) {
                        if (list_dipilih == "") {
                            list_dipilih += chk.value;
                        }
                        else {
                            list_dipilih += "," + chk.value;
                        }
                    }
                }
                else {
                    loop = false;
                }
                i++;
            } while (loop);
        }
        if(list_dipilih==""){
            alert("Anda belum memilih data yang akan dihapus!");
            return false;
        }
        if(confirm("Apakah anda yakin akan menghapus data ini?")){

            new Ajax.Request("pengaturan.cabang.php",{
                asynchronous: true,
                method: "post",
                parameters: "mode=4&idcabang="+list_dipilih,
                onLoading: function(request)
                {
                },
                onComplete: function(request)
                {

                },
                onSuccess: function(request)
                {
                    window.location.reload();
                    deselectAll();
                    alert(request.responseText);
                },
                onFailure: function(request)
                {
                }
            })
        }

        return false;

    }

  function ubahStatusAktif(idcabang){

      new Ajax.Request("pengaturan.cabang.php",{
          asynchronous: true,
          method: "post",
          parameters: "mode=5&idcabang="+idcabang,
          onLoading: function(request){
          },
          onComplete: function(request) {
          },
          onSuccess: function(request) {

              if(isJson(request.responseText)) {
                  var result;
                  result = JSON.parse(request.responseText);

                  if (result["status"] == "OK") {
                      document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;
                      document.getElementById("formdata").submit();
                  }
                  else {
                      alert("Gagal:" + result["pesan"]);
                  }
              }
              else{
                  alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
              }

          },
          onFailure: function(request)
          {
          }
      });
      return false;
  }

  function exportExcel(){
      document.getElementById("formdata").target = "_blank";
      document.getElementById("formdata").action = "{U_EXPORT_EXCEL}";
      document.getElementById("formdata").submit();
      document.getElementById("formdata").target = "";
      document.getElementById("formdata").action = "{ACTION_CARI}";
  }

  function init(e){
    popupwrapper	= dojo.widget.byId("popupcontainer");
    document.getElementById("wrappertablecontent").scrollTop={SCROLL_VALUE};

  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">
  <input type="hidden" id="scrollvalue" name="scrollvalue" value="0">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">{JUDUL_HALAMAN}</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="showDialogTambah();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->
    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="return hapusData();" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
    <!-- BEGIN EXPORT -->
    <a href="" onClick="exportExcel();return false;" class="crud">Export ke MS-Excel</a>
    <!-- END EXPORT -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  {JUDUL_TABEL}<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th></th>
      <th>No</th>
      <th onclick="setOrder(0)">Kode</th>
      <th onclick="setOrder(1)">Nama</th>
      <th onclick="setOrder(2)">Alamat</th>
      <th onclick="setOrder(3)">Kota</th>
      <th onclick="setOrder(4)">Telp</th>
      <th onclick="setOrder(5)">Fax</th>
      <th onclick="setOrder(7)">Aktif</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"><input type="checkbox" id="checked_{ROW.idx}" name="checked_{ROW.idcabang}" value="{ROW.idcabang}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="showDialogUbah('{ROW.idcabang}');" title="ubah data" ></span>&nbsp;&nbsp;
          <!-- END ACT_EDIT -->
          <!-- BEGIN ACT_DEL -->
          <span class="b_delete" onclick="hapusData('{ROW.idcabang}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
          <!-- BEGIN EXPORT -->
          <a href="" onClick="exportExcel();return false;" class="crud">Export ke MS-Excel</a>
          <!-- END EXPORT -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.kode}</td>
        <td align="center">{ROW.nama}</td>
        <td align="center">{ROW.alamat}</td>
        <td align="center">{ROW.kota}</td>
        <td align="center">{ROW.telp}</td>
        <td align="center">{ROW.fax}</td>
        <td align="center">
          <a href="" onclick="ubahStatusAktif('{ROW.idcabang}');return false;" class="{ROW.classcrudaktif}">&nbsp;{ROW.aktif}&nbsp;</a>
        </td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+3,sort.value,document.getElementById("tableheader"))
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>