<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<!--HEADER-->
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Pembayaran Sopir</td>
			<td align='right' valign='middle'>
				<form action="{ACTION_CARI}" method="post">
				<table>
					<tr><td class='bannernormal'>
						Kota:&nbsp;<select id='kota' name='kota'>{OPT_KOTA}</select>
						&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
						&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
						<input type="submit" value="cari" />&nbsp;
						<input type='hidden' name='mode' value='0'/>
					</td></tr>
				</table>
				</form>
			</td>
		</tr>
		<tr>
			<td colspan=2 align='center' valign='middle'>
				<table>
					<tr>
						<td>
							<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td colspan=2 align='right'>{PAGING}</td></tr>
	</table>
	<!-- END HEADER-->
	<table class="border" width='100%' >
  <tr>
     <th width=30>No</th>
		 <th width=200><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Nama Sopir</a></th>
		 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>NRP</a></th>
		 <th width=100><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Total Rit</a></th>
		 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Total Biaya</a></th>
   </tr>
   <!-- BEGIN ROW -->
   <tr class="{ROW.odd}">
     <td ><div align="right">{ROW.no}</div></td>
     <td ><div align="left"><b>{ROW.nama}</b></div></td>
		 <td ><div align="left">{ROW.nrp}</div></td>
     <td ><div align="right">{ROW.total_jalan}</div></td>
		 <td ><div align="right">{ROW.total_biaya}</div></td>
   </tr>
   <!-- END ROW -->
  </table>
	<table width='100%'>
		<tr>
			<td align='right' width='100%'>
				{PAGING}
			</td>
		</tr>
		<tr>
			<td align='left' valign='bottom' colspan=3>
			{SUMMARY}
			</td>
		</tr>
	</table>
 </td>
</tr>
</table>