<script src="{TPL}js/jquery.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('nama_invalid');
	Element.hide('hp_invalid');
	Element.hide('kode_mobil1');

	kode			= document.getElementById('kode_sopir');
	nama			= document.getElementById('nama');
	hp				= document.getElementById('hp');
	mobil1          = document.getElementById('mobil1').value;
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(!cekValue(hp.value)){	
		valid=false;
		Element.show('hp_invalid');
	}

	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_SOPIR_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Sopir</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='800'>
				<table width='400'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      		<input type="hidden" name="kode_sopir_old" value="{KODE_SOPIR_OLD}">
						<td width='200'><u>Kode Sopir</u></td>
						<td width='5'>:</td>
						<td>
							<input type="text" id="kode_sopir" name="kode_sopir" value="{KODE_SOPIR}" maxlength=50 onChange="Element.hide('kode_invalid');">
							<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
						<td><u>Kode Rolling</u></td>
						<td>:</td>
						<td>
							<input type="text" id="rolling" name="rolling" value="{ROLLING}" maxlength=100 onChange="Element.hide('rolling_invalid');">
							<span id='rolling_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
			      		<td><u>Nama</u></td>
						<td>:</td>
						<td>
							<input type="text" id="nama" name="nama" value="{NAMA}" maxlength=100 onChange="Element.hide('nama_invalid');">
							<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
			      		<td valign='top'>Alamat</td>
						<td  valign='top'>:</td>
						<td><textarea name="alamat" id="alamat" cols="30" rows="3"  maxlength=150>{ALAMAT}</textarea></td>
			    	</tr>
					<tr>
			      		<td>HP</td>
						<td>:</td>
						<td>
							<input type="text" id="hp" name="hp" value="{HP}" maxlength=50 onChange="Element.hide('hp_invalid');">
							<span id='hp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    	</tr>
					<tr>
			      		<td>No.SIM</td>
						<td>:</td>
						<td>
							<input type="text" id="no_sim" name="no_sim" value="{NO_SIM}" maxlength=50 />
						</td>
			    	</tr>
					<tr>
						<td>No. Rekening</td>
						<td>:</td>
						<td>
							<input type="text" id="no_rek" name="no_rek" value="{NO_REK}" maxlength=50 />
						</td>
					</tr>
                    <tr>
                        <td>Mobil 1</td>
						<td>:</td>
                        <td>
                            <select id="mobil1" name="mobil1">
                                {MOBIL1}
                            </select>
                            <span id='kode_mobil1' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                        </td>
                    </tr>
					<tr>
						<td>Mobil 2</td>
						<td>:</td>
						<td>
							<select id="mobil2" name="mobil2">
								{MOBIL2}
							</select>
						</td>
					</tr>
					<tr>
						<td>Mobil 3</td>
						<td>:</td>
						<td>
							<select id="mobil3" name="mobil3">
								{MOBIL3}
							</select>
						</td>
					</tr>
					<tr>
						<td>Cabang Kerja</td>
						<td>:</td>
						<td>
							<select id="kodecabang" name="kodecabang">
								{OPT_CABANG}
							</select>
						</td>
					</tr>
					<tr>
						<td>Status Aktif</td>
						<td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Status Kerja</td>
						<td>:</td>
						<td>
							<select id="isReguler" name="isReguler">
								<option value=1 {REGULER_1}>REGULER</option>
								<option value=0 {REGULER_0}>BACKUP</option>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>