<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_sopir.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_sopir="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_sopir.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&kode_sopir="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Sopir</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" size=50 />&nbsp;<input type="submit" value="cari" />
					</form>
				</td>
			</tr>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_SOPIR_ADD}">[+]Tambah Sopir</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Sopir</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
		<table width='100%' class="border">
    <tr>
       <th width=30></th>
       <th width=30>No</th>
		<th width=200>Nama sopir</th>
        <th width=100>Kode</th>
		<th width=100>Kode Rolling</th>
		<th width=100>Cabang</th>
			 <th width=100>No Rekening</th>
             <th width=150>MOBIL</th>
			 <th width=300>Alamat</th>
			 <th width=70>HP</th>
			 <th width=70>No.SIM</th>
		<th width=100>Status</th>
				<th width=100>Aktif</th>
			 <th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="center">{ROW.check}</div></td>
       <td><div align="right">{ROW.no}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
         <td><div align="left">{ROW.kode_sopir}</div></td>
		 <td><div align="left">{ROW.kode_rolling}</div></td>
		 <td><div align="left">{ROW.Cabang}</div></td>
			 <td><div align="left">{ROW.no_rek}</div></td>
         <td><div align="left">{ROW.mobil}</div></td>
       <td><div align="left">{ROW.alamat}</div></td>
			 <td><div align="left">{ROW.hp}</div></td>
			 <td><div align="left">{ROW.no_sim}</div></td>
			 <td><div align="center">{ROW.REG}</div></td>
		 <td><div align="center">{ROW.aktif}</div></td>
       <td><div align="center">{ROW.action}</div></td>
     </tr>  
     <!-- END ROW -->
		 {NO_DATA}
    </table>
    <table width='100%'>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_SOPIR_ADD}">[+]Tambah Sopir</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Sopir</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>