<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('nrp_invalid');
	Element.hide('nama_invalid');
	Element.hide('username_invalid');
	Element.hide('userpassword_invalid');
	Element.hide('konfirm_password_invalid');
	Element.hide('telp_invalid');
	Element.hide('hp_invalid');
	
	nrp			= document.getElementById('nrp');
	nama			= document.getElementById('nama');
	user_name	= document.getElementById('user_name');
	user_password	= document.getElementById('userpassword');
	kofirm_password	= document.getElementById('kofirm_password');
	submode		= document.getElementById('submode');
	telp			= document.getElementById('telp');
	hp				= document.getElementById('hp');
		
	if(nrp.value==''){
		valid=false;
		Element.show('nrp_invalid');
	}
	
	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}
	
	if(user_name.value==''){
		valid=false;
		Element.show('username_invalid');
	}
	
	if(submode.value==0){
		if(user_password.value==''){
			valid=false;
			Element.show('userpassword_invalid');
		}
		else{
			if(user_password.value!=kofirm_password.value){
				valid=false;
				Element.show('konfirm_password_invalid');
			}
		}
	}
	else{
		if(user_password.value!=''){
			if(user_password.value!=kofirm_password.value){
				valid=false;
				Element.show('konfirm_password_invalid');
			}
		}
	}
	
	if(!cekValue(hp.value)){	
		valid=false;
		Element.show('hp_invalid');
	}
	
	if(!cekValue(telp.value)){	
		valid=false;
		Element.show('telp_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data" action="{U_USER_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master User</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top'>
				<table width='500' valign='top'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr><td colspan=3><h3><u>Data Karyawan</u></h3></td></tr>
					<tr>
			      <input type="hidden" name="user_id" value="{USER_ID}">
						<td width='200'><u>No.Reg.Pegawai</u></td><td width='5'>:</td>
						<td>
							<input type="text" id="nrp" name="nrp" value="{NRP}" maxlength=20 onChange="Element.hide('nrp_invalid');">
							<span id='nrp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Nama</u></td><td>:</td>
						<td>
							<input type="text" id="nama" name="nama" value="{NAMA}" maxlength=100 onChange="Element.hide('nama_invalid');">
							<span id='nama_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Alamat</td><td  valign='top'>:</td><td><textarea name="alamat" id="alamat" cols="30" rows="3"  maxlength=150>{ALAMAT}</textarea></td>
			    </tr>
					<tr>
			      <td>Telp</td><td>:</td>
						<td>
							<input type="text" id="telp" name="telp" value="{TELP}" maxlength=50 onChange="Element.hide('telp_invalid');">
							<span id='telp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>HP</td><td>:</td>
						<td>
							<input type="text" id="hp" name="hp" value="{HP}" maxlength=50 onChange="Element.hide('hp_invalid');">
							<span id='hp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Email</td><td>:</td>
						<td>
							<input type="text" id="email" name="email" value="{EMAIL}" maxlength=50 />
						</td>
			    </tr>
				</table>
			</td>
			<td height=1 bgcolor='EFEFEF'></td>
			<td align='center' valign='top'>
				<table width=500>
					<tr><td><h2>&nbsp;</h2></td></tr>
					<tr><td colspan=3><h3><u>Data Login</u></h3></td></tr>
					<tr>
			      <td valign='top'><u>Username</u></td><td  valign='top'>:</td>
						<td>
							<input type="hidden" name="user_name_old" value="{USER_NAME_OLD}">
							<input type="text" id="user_name" name="user_name" value="{USER_NAME}" onChange="Element.hide('username_invalid');">
							<span id='username_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
						<td><u>Password</u></td><td>:</td>
						<td>
							<input type="password" id="userpassword" name="userpassword"  onChange="Element.hide('userpassword_invalid');" value="" />
							<span id='userpassword_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
						<td>Konfirmasi Password</td><td>:</td>
						<td>
							<input type="password" id="kofirm_password" name="kofirm_password"  onChange="Element.hide('konfirm_password_invalid');" value="">
							<span id='konfirm_password_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
			      <td>Cabang</td><td>:</td>
						<td>
							<select id='opt_cabang' name='opt_cabang'>
								{OPT_CABANG}
							</select>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>User Level</td><td  valign='top'>:</td>
						<td>
							<select id='opt_user_level' name='opt_user_level'>
							{OPT_USER_LEVEL}
							</select>
						</td>
			    </tr>
					<tr>
			      <td><u>Login berlaku hingga</u></td><td>:</td>
						<td>
							<input id="tgl_berlaku" name="tgl_berlaku" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERLAKU}">
						</td>
			    </tr>
					<tr>
						<td>Status Aktif</td><td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Cetak BBM</td><td>:</td>
						<td>
							<select id="cetak_bbm" name="cetak_bbm">
								<option value=0 {BBM_0}>TIDAK BISA CETAK BBM</option>
								<option value=1 {BBM_1}>BISA CETAK BBM</option>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" id='submode' name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>