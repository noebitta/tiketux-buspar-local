<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");
</script>
<script type="text/javascript">
    function init(e) {
        // create dialog
        dlg_approve						= dojo.widget.byId("dialogbuatapprove");
        dlg_dlg_approve_btn_cancel      = document.getElementById("dialogbuatapprovecancel");
    }
    dojo.addOnLoad(init);

    function ShowDialog(id) {
        document.getElementById("idjadwal").value = id;
        dlg_approve.show();
    }
    
    function Approve() {
        id = document.getElementById("idjadwal").value;
        keterangan = document.getElementById("keterangan").value;

        var kehadiran = document.getElementsByName("kehadiran");
        var status = 0;
        for(var i = 0; i < kehadiran.length; i++) {
            if(kehadiran[i].checked){
                status = kehadiran[i].value;
                break;
            }
        }

        new Ajax.Request("premi_sopir_detail.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=UbahKehadiran"+
            "&idJadwal="+id+
            "&keterangan="+keterangan+
            "&kehadiran="+status,
            onLoading: function(request){
                progressbar.show();
            },
            onComplete: function(request){
                progressbar.hide();
            },
            onSuccess: function(request){
                if(request.responseText == 1){
                    var cureent_url = window.location.href;
                    window.location = cureent_url;
                }else{
                    alert(request.responseText);
                }
            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }
</script>
<!--dialog-->
<div dojoType="dialog" id="dialogbuatapprove" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;">
        <table width="400">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr>
                            <td colspan='2'>
                                <h2>Approval Kehadiran</h2>
                                <input type="hidden" id="idjadwal">
                            </td>
                        </tr>
                        <tr>
                            <td align='right'>Ubah Kehadiaran :</td>
                            <td>
                                <input type="radio" name="kehadiran" id="hadir" value="1">Hadir &nbsp;&nbsp;
                                <input type="radio" name="kehadiran" id="alfa" value="0">Alfa
                            </td>
                        </tr>
                        <tr>
                            <td align='right'>Ketarangan :</td>
                            <td>
                                <textarea id="keterangan" name="keterangan" rows="3"></textarea>
                            </td>
                        </tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogbuatapprovecancel" value="&nbsp;Cancel&nbsp;" onClick="dlg_approve.hide();">
                    <input type="button" onclick="Approve();" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog-->
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="center">
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td colspan=2 valign='middle' class="bannerjudul">&nbsp;Detail Kehadiran {NamaSopir}</td>
                    <td valign='middle' class="bannerjudul" style="text-align: right">Total Kehadiran {TotalKerja} Hari</td>
                </tr>
            </table>
            <br>
            <table width='100%' class="border" cellpadding="6" cellspacing="2">
                <tr>
                    <th width="10">No</th width="10">
                    <th width="13%">Tanggal</th>
                    <th>Penjadwalan Sopir</th>
                    <th>Keberangkatan Sopir</th>
                    <th>Keterangan</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td width="5%" align="center"><font size="3">{ROW.no}</font></td>
                    <td><font size="3">{ROW.tanggal}</font></td>
                    <td><font size="3">{ROW.jadwal}</font></td>
                    <td><font size="3">{ROW.berangkat}</font></td>
                    <td>{ROW.keterangan}</td>
                </tr>
                <!-- END ROW -->
            </table>
        </td>
    </tr>
</table>