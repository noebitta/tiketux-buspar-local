<span class="popuptitle">{JUDUL}</span>
<center>
  <div class="wrapperpopup">
    <input type="hidden" id="dlgkodejadwalold" value="{KODE_JADWAL}"/>
    <input type="hidden" id="dlgcabangasal" value="{CABANG_ASAL}"/>
    <input type="hidden" id="dlgidjurusan" value="{ID_JURUSAN}"/>
    <input type="hidden" id="dlgcabangasalutama" value="{CABANG_ASAL_UTAMA}"/>
    <input type="hidden" id="dlgidjurusanutama" value="{ID_JURUSAN_UTAMA}"/>
    <input type="hidden" id="dlgjadwalutama" value="{KODE_JADWAL_UTAMA}"/>
    <input type="hidden" id="dlgstatusaktif" value="{STATUS_AKTIF}"/>
    <input type="hidden" id="dlghariaktif0" value="{HARI_AKTIF_0}"/>
    <input type="hidden" id="dlghariaktif1" value="{HARI_AKTIF_1}"/>
    <input type="hidden" id="dlghariaktif2" value="{HARI_AKTIF_2}"/>
    <input type="hidden" id="dlghariaktif3" value="{HARI_AKTIF_3}"/>
    <input type="hidden" id="dlghariaktif4" value="{HARI_AKTIF_4}"/>
    <input type="hidden" id="dlghariaktif5" value="{HARI_AKTIF_5}"/>
    <input type="hidden" id="dlghariaktif6" value="{HARI_AKTIF_6}"/>
    <input type="hidden" id="dlgstatusonline" value="{STATUS_ONLINE}"/>
    <input type="hidden" id="dlgjenisjadwal" value="{JENIS_JADWAL}"/>
    <span class="popuplabel">Kode Jadwal</span><span class="popupinput"><input type="text" id="dlgkodejadwal" onkeypress="validCharInput(event);" value="{KODE_JADWAL}" style="text-transform: uppercase;"/></span><br><br>
    <span class="popuplabel">Asal</span><span class="popupinput"><span id="dlgselcabangasal" class="combolistselection" onClick="setComboList(this,'dlgcabangasal','pengaturan.jadwal.php','mode=6',false,true,true);dlgidjurusan.value='';dlgselcabangtujuan.innerHTML='';">&nbsp;</span></span><br><br>
    <span class="popuplabel">Tujuan</span><span class="popupinput"><span id="dlgselcabangtujuan" class="combolistselection" onClick="if(dlgcabangasal.value!='') setComboList(this,'dlgidjurusan','pengaturan.jadwal.php','mode=6&cabangasal='+dlgcabangasal.value,false,true,true);">&nbsp;</span></span><br><br>
    <span class="popuplabel">Jam Berangkat</span><span class="popupinput">
      <select id="dlgjam" style="width:70px;text-align: center;" onclick="this.className='';">
        <!-- BEGIN OPT_JAM -->
        <option value="{OPT_JAM.value}" {OPT_JAM.selected}>{OPT_JAM.text}</option>
        <!-- END OPT_JAM -->
      </select>:
      <select id="dlgmenit" style="width:70px;text-align: center;">
        <!-- BEGIN OPT_MENIT -->
        <option value="{OPT_MENIT.value}" {OPT_MENIT.selected}>{OPT_MENIT.text}</option>
        <!-- END OPT_MENIT -->
      </select>
    </span><br><br>
    <span class="popuplabel">Layout Kursi</span><span class="popupinput">
      <select id="layoutkursi">
        <!-- BEGIN LAYOUT_KURSI -->
        <option value="{LAYOUT_KURSI.id}" {LAYOUT_KURSI.selected}>{LAYOUT_KURSI.kodelayout} {LAYOUT_KURSI.kapasitas}</option>
        <!-- END LAYOUT_KURSI -->
      </select>
    </span><br><br>
    <span class="popuplabel">Status Aktif</span><span class="popupinput"><a href="" id="dlgbtnstatusaktif" onclick="toggleStatusAktif();return false;">&nbsp;AKTIF&nbsp;</a></span><br><br>
    <span class="popuplabel">Hari Aktif</span><span class="popupinput">
      <table>
        <tr>
          <td><a href="" id="dlgbtnhariaktif6" onclick="toggleHariAktif(6);return false;">&nbsp;Min&nbsp;</a></td>
          <td><a href="" id="dlgbtnhariaktif0" onclick="toggleHariAktif(0);return false;">&nbsp;Sen&nbsp;</a></td>
          <td><a href="" id="dlgbtnhariaktif1" onclick="toggleHariAktif(1);return false;">&nbsp;Sel&nbsp;</a></td>
          <td><a href="" id="dlgbtnhariaktif2" onclick="toggleHariAktif(2);return false;">&nbsp;Rab&nbsp;</a></td>
        </tr>
        <tr>
          <td><a href="" id="dlgbtnhariaktif3" onclick="toggleHariAktif(3);return false;">&nbsp;Kam&nbsp;</a></td>
          <td><a href="" id="dlgbtnhariaktif4" onclick="toggleHariAktif(4);return false;">&nbsp;Jum&nbsp;</a></td>
          <td><a href="" id="dlgbtnhariaktif5" onclick="toggleHariAktif(5);return false;">&nbsp;Sab&nbsp;</a></td>
          <td>&nbsp;</td>
        </tr>
      </table></span><br><br>
    <span class="popuplabel">Status Online</span><span class="popupinput"><a href="" id="dlgbtnstatusonline" onclick="toggleIsOnline();return false;">&nbsp;OFFLINE&nbsp;</a></span><br><br>
    <span class="popuplabel">Jenis Jadwal</span><span class="popupinput"><a href="" id="dlgbtnjenisjadwal" onclick="toggleJenisJadwal();return false;">&nbsp;INDUK&nbsp;</a></span><br><br>
    <span id="dlgwrapperjadwalinduk" style="display: none;width: 100%;">
      <span class="popuplabel">Asal</span><span class="popupinput"><span id="dlgselcabangasalutama" class="combolistselection" onClick="setComboList(this,'dlgcabangasalutama','pengaturan.jadwal.php','mode=6',false,true,true);">&nbsp;</span></span><br><br>
      <span class="popuplabel">Tujuan</span><span class="popupinput"><span id="dlgselcabangtujuanutama" class="combolistselection" onClick="if(dlgcabangasalutama.value!='') setComboList(this,'dlgidjurusanutama','pengaturan.jadwal.php','mode=6&cabangasal='+dlgcabangasalutama.value,false,true,true);">&nbsp;</span></span><br><br>
      <span class="popuplabel">Jadwal</span><span class="popupinput"><span id="dlgseljadwalutama" class="combolistselection" onClick="if(dlgidjurusanutama.value!='' && dlgjam.value!='' && dlgmenit.value!='') setComboList(this,'dlgjadwalutama','pengaturan.jadwal.php','mode=7&idjurusan='+dlgidjurusanutama.value+'&jamberangkat='+dlgjam.value+':'+dlgmenit.value,false,true,true,'showDetailRute();');">&nbsp;</span></span><br><br>
    </span>
    <span class="popuplabel">Via</span><span class="popupinput" style="overflow: auto;height:20px;display: inline-block;"><span id="showdetailrute"></span></span><br><br>
    <span class="flatbutton" onclick="simpan();" id="buttonproses">SIMPAN</span>
  </div>
</center>