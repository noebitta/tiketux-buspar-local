<script type="text/javascript">

  function selectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;

      if(chk=document.getElementById(str_var)){
        chk.checked=true;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function deselectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;
      if(chk=document.getElementById(str_var)){
        chk.checked=false;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function toggleHariAktif(hari,inittoggle=false){
    
    elemen      = document.getElementById("dlghariaktif"+hari);

    if(!inittoggle) {
      elemen.value = 1 - elemen.value;
    }

    if(elemen.value==1){
      document.getElementById("dlgbtnhariaktif"+hari).className='crudgreen';
    }else{
      document.getElementById("dlgbtnhariaktif"+hari).className='crud';
    }

  }

  function toggleStatusAktif(inittoggle=false){

    if(!inittoggle){
      dlgstatusaktif.value  = 1 - dlgstatusaktif.value;
    }

    if(dlgstatusaktif.value==1){
      dlgbtnstatusaktif.className='crudgreen';
      dlgbtnstatusaktif.innerHTML='AKTIF';
    }else{
      dlgbtnstatusaktif.className='crud';
      dlgbtnstatusaktif.innerHTML='NON-AKTIF';
    }
  }

  function toggleJenisJadwal(inittoggle=false){

    if(!inittoggle){
      var inputvalid=true;

      inputvalid=validasiInput(dlgjam) && inputvalid;
      inputvalid=validasiInput(dlgmenit) && inputvalid;

      if(!inputvalid){
        return;
      }

      dlgjenisjadwal.value  = 1 - dlgjenisjadwal.value;
    }

    if(dlgjenisjadwal.value==0){
      dlgbtnjenisjadwal.className='crudgreen';
      dlgbtnjenisjadwal.innerHTML='INDUK';
      dlgwrapperjadwalinduk.style.display="none";
      setPopup(500,410);
    }else{
      dlgbtnjenisjadwal.className='crud';
      dlgbtnjenisjadwal.innerHTML='TRANSIT';
      dlgwrapperjadwalinduk.style.display="inline-block";
      setPopup(500,510);
    }

    popupwrapper.show();
  }

  function toggleIsOnline(inittoggle=false){

    if(!inittoggle){
      dlgstatusonline.value  = 1 - dlgstatusonline.value;
    }

    if(dlgstatusonline.value==1){
      dlgbtnstatusonline.className='crudgreen';
      dlgbtnstatusonline.innerHTML='ONLINE';
    }else{
      dlgbtnstatusonline.className='crud';
      dlgbtnstatusonline.innerHTML='OFFLINE';
    }
  }

  function showDialogTambah(){
    setPopup(500,410);

    new Ajax.Updater("popupcontent","pengaturan.jadwal.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1",
      onLoading: function(request) {
      },
      onComplete: function(request) {
        toggleHariAktif(0,true);
        toggleHariAktif(1,true);
        toggleHariAktif(2,true);
        toggleHariAktif(3,true);
        toggleHariAktif(4,true);
        toggleHariAktif(5,true);
        toggleHariAktif(6,true);

        toggleStatusAktif(true);

        toggleIsOnline(true);

        toggleJenisJadwal(true);
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function showDialogUbah(kodejadwal){
    setPopup(500,410);

    new Ajax.Updater("popupcontent","pengaturan.jadwal.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2&kodejadwal="+kodejadwal,
      onLoading: function(request) {
      },
      onComplete: function(request) {
        setComboList(document.getElementById('dlgselcabangasal'),'dlgcabangasal','pengaturan.jadwal.php','mode=6',false,false,true)
        setComboList(document.getElementById('dlgselcabangtujuan'),'dlgidjurusan','pengaturan.jadwal.php','mode=6&cabangasal='+dlgcabangasal.value,false,false,true)

        toggleHariAktif(0,true);
        toggleHariAktif(1,true);
        toggleHariAktif(2,true);
        toggleHariAktif(3,true);
        toggleHariAktif(4,true);
        toggleHariAktif(5,true);
        toggleHariAktif(6,true);

        toggleStatusAktif(true);

        toggleIsOnline(true);

        toggleJenisJadwal(true);

        if(dlgjenisjadwal.value==1) {
          setComboList(document.getElementById('dlgselcabangasalutama'), 'dlgcabangasalutama', 'pengaturan.jadwal.php', 'mode=6', false, false, true);
          setComboList(document.getElementById('dlgselcabangtujuanutama'), 'dlgidjurusanutama', 'pengaturan.jadwal.php', 'mode=6&cabangasal=' + dlgcabangasalutama.value, false, false, true);
          setComboList(document.getElementById('dlgseljadwalutama'), 'dlgjadwalutama', 'pengaturan.jadwal.php', 'mode=7&idjurusan=' + dlgidjurusanutama.value + '&jamberangkat=' + dlgjam.value + ':' + dlgmenit.value, false, false, true);
        }

        showDetailRute();

      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function showDetailRute(){

    if(dlgjenisjadwal.value==0){
      kodejadwal      =dlgkodejadwalold.value;
      kodejadwalutama =dlgkodejadwalold.value;
    }
    else{
      kodejadwal      =dlgkodejadwalold.value;
      kodejadwalutama =dlgjadwalutama.value;
    }

    if(kodejadwalutama==""){
      return false;
    }

    new Ajax.Updater("showdetailrute","pengaturan.jadwal.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2.1&kodejadwal="+kodejadwal+"&kodejadwalutama="+kodejadwalutama,
      onLoading: function(request){

      },
      onComplete: function(request){

      },
      onSuccess: function(request){
      },
      onFailure: function(request){
        assignError(request.responseText);
      }
    });
  }

  function simpan(){

    var inputvalid=true;

    inputvalid=validasiInput(dlgkodejadwal) && inputvalid;
    inputvalid=validasiInput(dlgcabangasal,dlgselcabangasal) && inputvalid;
    inputvalid=validasiInput(dlgidjurusan,dlgselcabangtujuan) && inputvalid;
    inputvalid=validasiInput(dlgjam) && inputvalid;
    inputvalid=validasiInput(dlgmenit) && inputvalid;
    inputvalid=validasiInput(layoutkursi) && inputvalid;

    if(dlgjenisjadwal.value==1){
      inputvalid=validasiInput(dlgcabangasalutama,dlgselcabangasalutama) && inputvalid;
      inputvalid=validasiInput(dlgidjurusanutama,dlgselcabangtujuanutama) && inputvalid;
      inputvalid=validasiInput(dlgjadwalutama,dlgseljadwalutama) && inputvalid;
    }

    if(!inputvalid){
      return;
    }

    var strhariaktif="";

    for(hari=0;hari<=6;hari++){
      strhariaktif += document.getElementById("dlghariaktif"+hari).value==1?hari+",":"";
    }

    strhariaktif  = strhariaktif.length>0?strhariaktif.substring(0,strhariaktif.length-1):"";

    new Ajax.Request("pengaturan.jadwal.php", {
      asynchronous: true,
      method: "post",
      parameters: "mode=1.1&kodejadwalold="+dlgkodejadwalold.value+
      "&kodejadwal="+dlgkodejadwal.value+
      "&idjurusan="+dlgidjurusan.value+
      "&jamberangkat="+dlgjam.value+":"+dlgmenit.value+
      "&jenislayout="+layoutkursi.value+
      "&jenisjadwal="+dlgjenisjadwal.value+
      "&kodejadwalutama="+dlgjadwalutama.value+
      "&hariaktif="+strhariaktif+
      "&isaktif="+dlgstatusaktif.value+
      "&isonline="+dlgstatusonline.value,
      onLoading: function(request){

      },
      onComplete: function(request){

      },
      onSuccess: function(request){

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            if(dlgkodejadwalold.value=="") {
              objbutton = new Object();
              popupwrapper.hide();

              objbutton["CLOSE"] = function(){popupnotif.hide();showDialogTambah();};
              objbutton["OK"] = function(){popupnotif.hide();showDialogTambah();};
              setPopupNotif(popupnotif,400,120,"DATA BERHASIL DITAMBAHKAN",objbutton);
              popupbuttonclose.onclick=null;
              addEvent(popupbuttonclose,"click",function(){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();});

            }
            else{
              dlgkodejadwalold.value=dlgkodejadwal.value;
              objbutton = new Object();
              popupwrapper.hide();
              objbutton["CLOSE"] = function(){popupnotif.hide();popupwrapper.show();};
              objbutton["OK"] = function(){popupnotif.hide();popupwrapper.show();};
              setPopupNotif(popupnotif,400,120,"DATA BERHASIL DIUBAH",objbutton);
              popupbuttonclose.onclick=null;
              addEvent(popupbuttonclose,"click",function(){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();});
            }
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }

      },
      onFailure: function(request){
        alert('Error');
        assignError(request.responseText);
      }
    });
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function hapusData(id=''){

    if(id!=''){
      list_dipilih="'"+id+"'";
    }
    else{
      i=1;
      loop=true;
      list_dipilih="";
      do{
        str_var='checked_'+i;

        if(chk=document.getElementById(str_var)){
          if(chk.checked){
            if(list_dipilih==""){
              list_dipilih +="'"+chk.value+"'";
            }
            else{
              list_dipilih +=",'"+chk.value+"'";
            }
          }
        }
        else{
          loop=false;
        }
        i++;
      }while(loop);
    }

    if(list_dipilih==""){
      alert("Anda belum memilih data yang akan dihapus!");
      return false;
    }

    if(confirm("Apakah anda yakin akan menghapus data ini?")){

      new Ajax.Request("pengaturan.jadwal.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=3&listid="+list_dipilih,
        onLoading: function(request){
        },
        onComplete: function(request){
        },
        onSuccess: function(request){
          if(isJson(request.responseText)) {
            var result;
            result = JSON.parse(request.responseText);

            if (result["status"] == "OK") {
              objbutton = new Object();
              popupwrapper.hide();
              objbutton["CLOSE"] = function (){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();};
              objbutton["OK"] = function (){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();};
              setPopupNotif(popupnotif, 400, 120, "DATA BERHASIL DIHAPUS", objbutton);
            }
            else {
              alert("Gagal:" + result["pesan"]);
            }
          }
          else{
            alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
          }

        },
        onFailure: function(request){
        }
      })
    }

    return false;

  }

  function ubahStatusAktif(kodejadwal){

    new Ajax.Request("pengaturan.jadwal.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=4&kodejadwal="+kodejadwal,
      onLoading: function(request){
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;
            document.getElementById("formdata").submit();
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }

      },
      onFailure: function(request)
      {
      }
    });
    return false;
  }

  function ubahStatusOnline(kodejadwal){

    new Ajax.Request("pengaturan.jadwal.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=5&kodejadwal="+kodejadwal,
      onLoading: function(request){
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;
            document.getElementById("formdata").submit();
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }

      },
      onFailure: function(request)
      {
      }
    });
    return false;
  }

  function exportExcel(){
    document.getElementById("formdata").target = "_blank";
    document.getElementById("formdata").action = "{U_EXPORT_EXCEL}";
    document.getElementById("formdata").submit();
    document.getElementById("formdata").target = "";
    document.getElementById("formdata").action = "{ACTION_CARI}";
  }

  function init(e){
    popupwrapper	= dojo.widget.byId("popupcontainer");
    popupnotif	  = dojo.widget.byId("popupnotifcontainer");
    document.getElementById("wrappertablecontent").scrollTop={SCROLL_VALUE};

  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">
  <input type="hidden" id="scrollvalue" name="scrollvalue" value="0">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Jadwal</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="showDialogTambah();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->
    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="return hapusData();" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
    <!-- BEGIN EXPORT -->
    <a href="" onClick="exportExcel();return false;" class="crud">Export ke MS-Excel</a>
    <!-- END EXPORT -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  Data Jadwal<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th></th>
      <th>No</th>
      <th onclick="setOrder(0)">Kode Jadwal</th>
      <th onclick="setOrder(1)">Jenis Jadwal</th>
      <th onclick="setOrder(2)">Jurusan</th>
      <th onclick="setOrder(3)">Jam Berangkat</th>
      <th onclick="setOrder(4)">Layout</th>
      <th onclick="setOrder(5)">Aktif</th>
      <th onclick="setOrder(6)">Online</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"><input type="checkbox" id="checked_{ROW.idx}" name="checked_{ROW.kode}" value="{ROW.kode}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="showDialogUbah('{ROW.kode}');" title="ubah data" ></span>&nbsp;
          <!-- END ACT_EDIT -->

          <!-- BEGIN ACT_DEL -->
          <span class="b_delete" onclick="hapusData('{ROW.kode}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.kode}</td>
        <td align="center">{ROW.jenisjadwal}</td>
        <td align="center">{ROW.jurusan}</td>
        <td align="center">{ROW.jam}</td>
        <td align="center">{ROW.layout}</td>
        <td align="center">
          <a href="" onclick="ubahStatusAktif('{ROW.kode}');return false;" class="{ROW.classcrudaktif}">&nbsp;{ROW.aktif}&nbsp;</a>
        </td>
        <td align="center">
          <a href="" onclick="ubahStatusOnline('{ROW.kode}');return false;" class="{ROW.classcrudonline}">&nbsp;{ROW.online}&nbsp;</a>
        </td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+3,sort.value,document.getElementById("tableheader"))
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>