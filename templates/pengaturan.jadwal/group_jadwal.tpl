<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
    dojo.require("dojo.widget.Dialog");
    function init(e) {
        //control dialog
        dialog_sopir	    = dojo.widget.byId("dialog_sopir");
        dlg_sopir_cancel    = document.getElementById("dialogsopircancel");

        dialog_mobil	    = dojo.widget.byId("dialog_mobil");
        dlg_mobil_cancel    = document.getElementById("dialogmobilcancel");

        dialog_title        = dojo.widget.byId("dialog_title");
        dlg_title_cancel    = document.getElementById("dialogtitlecancel");
    }
    dojo.addOnLoad(init);

    function setSupir(sopir,kode) {
        document.getElementById("kode_group_sopir").value = kode;
        document.getElementById('sopir').value = sopir;
        dialog_sopir.show();
    }

    function simpanSopir() {
        kode_group = document.getElementById("kode_group_sopir").value;
        kode_sopir = document.getElementById("sopir").value;

        new Ajax.Request("grouping_jadwal.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=setSopir&kodeSopir="+kode_sopir+"&kodeGroup="+kode_group,
            onLoading: function(request){

            },
            onComplete: function(request){

            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }

    function setMobil(mobil,kode) {
        document.getElementById("kode_group_mobil").value = kode;
        document.getElementById("mobil").value = mobil;
        dialog_mobil.show();
    }

    function simpanMobil() {
        kode_group = document.getElementById("kode_group_mobil").value;
        kode_mobil = document.getElementById("mobil").value;

        new Ajax.Request("grouping_jadwal.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=setMobil&kodeMobil="+kode_mobil+"&kodeGroup="+kode_group,
            onLoading: function(request){

            },
            onComplete: function(request){

            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }

    function setTitle(judul,kode) {
        document.getElementById("kode_group_title").value = kode;
        document.getElementById("title").value = judul;
        dialog_title.show();
    }

    function simpanTitle() {
        kode_group = document.getElementById("kode_group_title").value;
        kode_title = document.getElementById("title").value;

        new Ajax.Request("grouping_jadwal.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=setTitle&kodeTitle="+kode_title+"&kodeGroup="+kode_group,
            onLoading: function(request){

            },
            onComplete: function(request){

            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }

    function hapusData(kode) {
        if(confirm("Apakah anda yakin akan menghapus data ini?")){
            new Ajax.Request("grouping_jadwal.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=hapus&kodeGroup="+kode,
                onLoading: function(request){

                },
                onComplete: function(request){

                },
                onSuccess: function(request){
                    eval(request.responseText);

                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }
    }
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="center">
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Group Jadwal</td>
                    <td colspan=2 align='right' class="bannernormal" valign='middle'>
                        <br>
                        <form name="frm_cari" action="{ACTION_CARI}" method="post">
                            Cari:&nbsp;<input type="text" id="cari" name="cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
                        </form>
                    </td>
                </tr>
                <tr>
                    <td width='30%' align='left'>
                        <a href="{U_GROUP_JADWAL_ADD}">[+]Tambah Group</a>&nbsp;
                    </td>
                </tr>
            </table>
            <table width='100%' class="border">
                <tr>
                    <th style="width: 5%;">No</th>
                    <th style="width: 65%;">Group Jadwal</th>
                    <th style="width: 10%;">Title</th>
                    <th style="width: 5%;">Action</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td><div align="center" style="font-size: 13px;"><b>{ROW.no}</b></div></td>
                    <td><div align="left" style="font-size: 13px;"><b>{ROW.group}</b></div></td>
                    <td>
                        <b>{ROW.title}<br>
                            <input type="button" class="tombol" value="Set Title" onclick="setTitle('{ROW.title}',{ROW.kode_group})">
                        </b>
                    </td>
                    <!--
                    <td>
                        <div align="center" style="font-size: 13px;">
                            <b>{ROW.sopir}<br>
                                <input type="button" class="tombol" value="Set Sopir" onclick="setSupir('{ROW.kode_sopir}',{ROW.kode_group})">
                            </b>
                        </div>
                    </td>
                    <td>
                        <div align="center" style="font-size: 13px;">
                            <b>{ROW.mobil}<br>
                                <input type="button" class="tombol" value="Set Mobil" onclick="setMobil('{ROW.kode_kendaraan}',{ROW.kode_group})">
                            </b>
                        </div>
                    </td>
                    -->
                    <td>
                        <a  href="{ROW.EDIT}">Edit</a>&nbsp;|&nbsp;<a onclick='return hapusData({ROW.kode_group});'>Delete</a>
                    </td>
                </tr>
                <!-- END ROW -->
            </table>
        </td>
    </tr>
</table>
<div dojoType="dialog" id="dialog_sopir" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr><td colspan='2' align="center"><h2>Set Sopir</h2></td></tr>

                        <tr>
                            <td align="right" width="200">Supir :</td>
                            <td>
                                <select id="sopir" >
                                    {SOPIR}
                                </select>
                                <input type="hidden" id="kode_group_sopir">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogsopircancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_sopir.hide();">
                    <input type="button" onclick="simpanSopir()"  id="dialogproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<div dojoType="dialog" id="dialog_mobil" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr><td colspan='2' align="center"><h2>Set Mobil</h2></td></tr>

                        <tr>
                            <td align="right" width="200">Mobil :</td>
                            <td>
                                <select id="mobil">
                                    {MOBIL}
                                </select>
                                <input type="hidden" id="kode_group_mobil">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogmobilcancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_mobil.hide();">
                    <input type="button" onclick="simpanMobil()"  id="dialogproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<div dojoType="dialog" id="dialog_title" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table width="100%">
                        <tr><td colspan='2' align="center"><h2>Set Title Group Jadwal</h2></td></tr>

                        <tr>
                            <td align="right" width="10%">Title :</td>
                            <td width="90%">
                                <input type="text" name="title" id="title" style="width: 420px;">
                                <input type="hidden" id="kode_group_title">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogtitlecancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_title.hide();">
                    <input type="button" onclick="simpanTitle()"  id="dialogproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>