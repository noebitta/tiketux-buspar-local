<script language="JavaScript">

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_jadwal_invalid');
	
	kode_jadwal	= document.getElementById('kode_jadwal');
	
	if(!document.getElementById('opt_tujuan')){
		alert("Anda belum memilih tujuan");
		valid=false;
	}
	
	if(kode_jadwal.value==''){
		valid=false;
		Element.show('kode_jadwal_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

function getUpdateTujuan(asal,sub){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(sub==0){
			rewrite	='rewrite_tujuan';
			loading	='loading_tujuan';
			mode		='get_tujuan';
			jurusan	='{ID_JURUSAN}';
		}
		else{
			rewrite	='rewrite_sub_tujuan';
			loading	='loading_sub_tujuan';
			mode		='get_tujuan_utama';
			jurusan	='{SUB_ID_JURUSAN}';
		}
		
		if(document.getElementById(rewrite)){
			document.getElementById(rewrite).innerHTML = "";
    }
		
		new Ajax.Updater(rewrite,"pengaturan_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&jurusan="+jurusan+"&mode=" + mode,
        onLoading: function(request) 
        {
            Element.show(loading);
        },
        onComplete: function(request) 
        {
						Element.hide(loading);
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJadwal(id_jurusan){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		if(document.getElementById('rewrite_sub_jadwal')){
			document.getElementById('rewrite_sub_jadwal').innerHTML = "";
    }
		
		new Ajax.Updater('rewrite_sub_jadwal',"pengaturan_jadwal.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "jurusan=" + id_jurusan + "&kode_jadwal={KODE_SUB_JADWAL}&mode=get_jadwal",
        onLoading: function(request) 
        {
            Element.show('loading_sub_jadwal');
        },
        onComplete: function(request) 
        {
						Element.hide('loading_sub_jadwal');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function showSubJadwal(flag){
	
	if(flag==0){
		Element.hide('show_sub_jadwal');
	}
	else{
		Element.show('show_sub_jadwal');
	}
}
getUpdateTujuan("{ASAL}",0);
getUpdateTujuan("{SUB_ASAL}",1);
getUpdateJadwal({SUB_ID_JURUSAN});
</script>

<form name="frm_data_mobil" action="{U_JADWAL_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Jadwal</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='600'>
				<table width='600'>  
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <input type="hidden" name="kode_jadwal_old" value="{KODE_JADWAL_OLD}">
						<td width='200'><u>Kode Jadwal</u></td><td width='5'>:</td><td width='400'><input type="text" id="kode_jadwal" name="kode_jadwal" value="{KODE_JADWAL}" maxlength=50 onChange="Element.hide('kode_jadwal_invalid');"><span id='kode_jadwal_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    </tr>
					<tr>
			      <td>Asal</td><td>:</td>
						<td>
							<select id='opt_asal' name='opt_asal' onChange="getUpdateTujuan(this.value,0)">
								{OPT_ASAL}
							</select>
						</td>
			    </tr>
					<tr>
			      <td>Tujuan</td><td>:</td>
						<td>
							<div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Waktu berangkat</u></td><td>:</td>
						<td>
							<select id='opt_jam' name='opt_jam'>
								{OPT_JAM}
							</select>&nbsp;:&nbsp;
							<select id='opt_menit' name='opt_menit'>
								{OPT_MENIT}
							</select>
						</td>
			    </tr>
					<tr>
			      <td>Jum. Kursi</td><td>:</td>
						<td>
							<select id='opt_kursi' name='opt_kursi'>
								{OPT_KURSI}
							</select>
						</td>
			    </tr>
					<tr>
						<td>Status Aktif</td><td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
					<tr>
			      <td valign='top'>Jenis Jadwal</td><td valign='top'>:</td>
						<td valign='top'>
							<select id="flag_sub_jadwal" name="flag_sub_jadwal" onChange="showSubJadwal(this.value);" >
								<option value=0 {SUB_JADWAL_0}>Jadwal Utama</option>
								<option value=1 {SUB_JADWAL_1}>Jadwal Transit</option>
							</select>
						</td>
			    </tr>
					<tr>
						<td colspan=3>
							<span id='show_sub_jadwal' style='display:{SUB_JADWAL};'>
								<table width=600>
									<tr>
										<td colspan=3><b>Jadwal Utama</b></td>
									</tr>
									<tr>
										<td width=190>Asal Utama</td><td width=5>:</td><td><select id='opt_sub_asal' name='opt_sub_asal' onChange="getUpdateTujuan(this.value,1)">{OPT_SUB_ASAL}</select></td>
									</tr>
									<tr>
										<td>Tujuan Utama</td><td>:</td><td><div id='rewrite_sub_tujuan'></div><span id='loading_sub_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/></span></td>
									</tr>
									<tr>
										<td>Jadwal</td><td>:</td><td><div id='rewrite_sub_jadwal'></div><span id='loading_sub_jadwal' style='display:none;'><img src="{TPL}images/loading.gif"/></span></td>
									</tr>
								</table>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="hidden" name="txt_cari" value="{CARI}">
					<input type="hidden" name="page" value="{PAGE}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>