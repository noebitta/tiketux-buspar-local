<script type="text/javascript">
    function getUpdateTujuan(asal,no){

        rewrite = "rewrite_tujuan"+no;
        new Ajax.Updater(rewrite,"grouping_jadwal.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=get_tujuan&asal=" + asal,
                    onLoading: function(request)
                    {
                        Element.show(loading);
                    },
                    onComplete: function(request)
                    {
                        Element.hide(loading);
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    function getUpdateJam(id_jurusan,no){
        if(document.getElementById('rewrite_jam')){
            document.getElementById('rewrite_jam').innerHTML = "";
        }
        rewrite = "rewrite_jam"+no;
        new Ajax.Updater(rewrite,"grouping_jadwal.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "id_jurusan=" + id_jurusan + "&mode=get_jam",
                    onLoading: function(request)
                    {
                        Element.show('loading_jam');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_jam');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    function tambah() {
        jml_jadwal = document.getElementById('jml_jadwal').value;
        penghitung = document.getElementById('penghitung').value;

        no = parseInt(penghitung)+1;

        html = '<tr id="jadwal'+no+'">'+
                '<td align="right">Asal :</td>'+
                '<td align="left">'+
                '<select onchange="getUpdateTujuan(this.value,'+no+')" id="opt_asal" name="opt_asal[]">{ASAL}</select>'+
                '</td>'+
                '<td align="right">Tujuan :</td>'+
                '<td align="left">'+
                '<div id="rewrite_tujuan'+no+'">'+
                '<select><option value="">None</option></select>'+
                '</div>'+
                '</td>'+
                '<td align="right">Jam :</td>'+
                '<td align="left">'+
                '<div id="rewrite_jam'+no+'">'+
                '<select name="jam" id="jam"><option>Jam</option></select>'+
                '</div>'+
                '</td>'+
                '<td align="left">'+
                '<input type="button" class="tombol" value="Hapus" onclick="hapus('+no+')">'+
                '</td>'+
                '</tr>';
        document.getElementById('blok').insertAdjacentHTML('beforeend',html);

        document.getElementById('penghitung').value=no;
        document.getElementById('jml_jadwal').value=parseInt(jml_jadwal)+1;
    }

    function hapus(no) {

        document.getElementById("jadwal"+no).remove();

        jml_jadwal = document.getElementById('jml_jadwal').value;
        document.getElementById('jml_jadwal').value=parseInt(jml_jadwal)-1;
    }


</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="center">
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Tambah Group Jadwal</td>
                    <td colspan=2 align='right' class="bannernormal" valign='middle'>

                    </td>
                </tr>
            </table>
            <form action="{URL}" method="POST">
                <input type="hidden" name="kode_group" value="{kode_group}">
                <table>
                    <tr>
                        <td bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input type="button" class="tombol" value="Tambah" onclick="tambah()">
                            <input type="hidden" name="jml_jadwal" id="jml_jadwal" value="{jml_jadwal}">
                            <input type="hidden" name="penghitung" id="penghitung" value="{jml_jadwal}">
                        </td>
                    </tr>
                    <tr align="center">
                        <table id="blok">
                            <!-- BEGIN ROW -->
                            {ROW.jadwal}
                            <!-- END ROW -->
                        </table>
                    </tr>
                    <tr>
                        <td align="center">
                            <br><br>
                            <input type="submit" value="SIMPAN" class="tombol">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>