<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
  <script language="javaScript" type="text/javascript" src="includes/calendar.js"></script>
  <link href="includes/calendar.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25961668-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

  </script>
</head>

<script type='text/javascript' src='{ROOT}ajax/lib/prototype.js'></script>
<script type="text/javascript" src="{ROOT}ajax/dojo.js"></script>
<script type="text/javascript" src="{TPL}js/main.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script> 

<body>
	
  <!--BEGIN popuploading -->
  <div dojoType="dialog" id="popuploading" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
    <table width="300" cellpadding="0" cellspacing="0" >
      <tr><td align="center" valign="middle" style="padding-top: 10px;">
        <font style="color: white;font-size: 12px;">sedang mengerjakan</font><br>
        <img src="{TPL}/images/loading_bar.gif" />
      </td></tr>
    </table>
    <br>
  </div>
  <!--END popuploading-->

  <!-- POPUP -->
  <div dojoType="dialog" id="popupcontainer" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <div id="combolistcontainerpopup" style="display:none;z-index: 1000;"></div>
    <span class="popupclosebutton" onclick="popupwrapper.hide();" title="Tutup Popup Ini" id="popupbuttonclose">X</span>
    <div style="width:700px;height: 400px;background: white;" id="popupcontent" name="popupcontent">
    </div>
  </div>

  <!-- POPUP NOTIFICATION -->
  <div dojoType="dialog" id="popupnotifcontainer" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <span class="popupclosebutton" onclick="" title="Tutup Popup Ini" id="popupnotifbuttonclose">X</span>
    <div style="width:700px;height: 400px;background: white;" id="popupnotifcontent" name="popupnotifcontent">
      <span class="popuptitle">NOTIFIKASI</span>
      <center>
        <span id="popupnotifpesan" style="font-family: 'trebuchet ms';text-transform: uppercase;font-size: 14px;text-align: center;"></span><br><br>
        <span id="popupnotifbuttonok" class="flatbutton" onclick="" style="display:none;;">OK</span>
        <span id="popupnotifbuttoncancel" class="flatbutton" onclick="" style="display:none;">CANCEL</span>
        <span id="popupnotifbuttonya" class="flatbutton" onclick="" style="display:none;">YA</span>
        <span id="popupnotifbuttontidak" class="flatbutton" onclick="" style="display:none;">TIDAK</span>
      </center>
    </div>
  </div>

  <!-- COMBO LIST -->
  <div id="combolistcontainer" style="display:none;z-index: 1000;"></div>

  <div align='center'>
    <!-- BEGIN if_login -->
    <div id="wrapperoverallheader">
      <span id="overallheaderlogo"></span><span id="overallheadermid">www.daytrans.co.id</span><span id="overallheaderright"><span id="overallheaderusername">{USERNAME}</span></span>
    </div>
    <div id="overallheadershadow"></div>
    <div id="wrapperoverallsubheader">
      <span id="overallheaderbcrump">{BCRUMP}</span>
      <span id="overallheaderaction">Cabang: {CABANG_LOGIN} | <a id='show_ubah_password' href="#" onClick="showUbahPassword();return false">Ubah Password</a> | <a href="{U_LOGOUT}">Logout</a></span>
    </div>
    <!-- END if_login -->
    <div style="height: 25px;"></div>