<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script src="{TPL}js/jquery.min.js" type="text/javascript"></script>
<script src="{TPL}js/jquery.jqplot.js" type="text/javascript"></script>
<script src="{TPL}js/jquery.jqplot.css" type="text/javascript"></script>
<script src="{TPL}js/jqplot.meterGaugeRenderer.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script>
    $(document).ready(function(){
        capaiantiket = [{CAPAIANTIKET}];
        plot4 = $.jqplot('TIKET',[capaiantiket],{
            seriesDefaults: {
                renderer: $.jqplot.MeterGaugeRenderer,
                rendererOptions: {
                    label: 'TIKET',
                    labelPosition: 'bottom',
                    labelHeightAdjust: -5,
                    intervalOuterRadius: 85,
                    ticks: [{INTERVALTIKET}],
                }
            }
        });
        capaianpaket = [{CAPAIANPAKET}];
        plot5 = $.jqplot('PAKET',[capaianpaket],{
            seriesDefaults: {
                renderer: $.jqplot.MeterGaugeRenderer,
                rendererOptions: {
                    label: 'PAKET',
                    labelPosition: 'bottom',
                    labelHeightAdjust: -5,
                    intervalOuterRadius: 85,
                    ticks: [{INTERVALPAKET}],
                }
            }
        });
    });
</script>
<table width="100%" cellspacing="0" cellpadding="0" >
    <tr>
        <td class="whiter" valign="middle" align="center">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td align='center' valign='middle' class="bannerjudul" width="450">&nbsp;General Total Grafik Traget Cabang {Cabang} Tanggal {Tanggal}</td>
                </tr>
            </table>
            <!-- END HEADER-->
            </br>
            <!--BODY-->
            <table width="80%">
                <tr>
                    <td valign="top" width="40%">
                        <div style="width:40%; min-width:450px;" id="TIKET"></div>
                    </td>
                    <td valign="top" width="40%">
                        <div style="width:40%; min-width:450px;" id="PAKET"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>