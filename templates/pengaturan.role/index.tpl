<script type="text/javascript">

  function selectAll(){

    i=0;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;

      if(chk=document.getElementById(str_var)){
        chk.checked=true;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function deselectAll(){

    i=0;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;
      if(chk=document.getElementById(str_var)){
        chk.checked=false;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function simpan(namarole,id=""){

    if(namarole==""){
      inputnamarole.style.background="red";
      return;
    }

    popupwrapper.hide();

    new Ajax.Request("pengaturan.role.php", {
        asynchronous: true,
        method: "post",
        parameters: "mode=3&id="+id+"&namarole="+namarole,
        onLoading: function(request){
          showLoading();
        },
        onComplete: function(request){

        },
        onSuccess: function(request){
          var result;

          result = JSON.parse(request.responseText);

          if(result["status"]=="OK"){
            formdata.submit();
          }
          else{
            alert("Gagal:"+result["error"]);
            popupwrapper.hide();
          }
          document.getElementById("formdata").submit();
        },
        onFailure: function(request){
          alert('Error');
          assignError(request.responseText);
        }
      });
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }


  function tambahData(){
    setPopup(500,100);

    new Ajax.Updater("popupcontent","pengaturan.role.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1",
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function ubahData(id){
    setPopup(500,100);

    new Ajax.Updater("popupcontent","pengaturan.role.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2&id="+id,
      onLoading: function(request) {
        id.value  = id;
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function hapusData(id=''){

    if(id!=''){
      list_dipilih=id;
    }
    else{
      i=1;
      loop=true;
      list_dipilih="";
      do{
        str_var='checked_'+i;

        if(chk=document.getElementById(str_var)){
          if(chk.checked){
            if(list_dipilih==""){
              list_dipilih +=chk.value;
            }
            else{
              list_dipilih +=","+chk.value;
            }
          }
        }
        else{
          loop=false;
        }
        i++;
      }while(loop);
    }

    if(list_dipilih==""){
      alert("Anda belum memilih data yang akan dihapus!");
      return;
    }

    if(confirm("Apakah anda yakin akan menghapus data ini?")){

      new Ajax.Request("pengaturan.role.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=4&id="+list_dipilih,
        onLoading: function(request)
        {
        },
        onComplete: function(request)
        {

        },
        onSuccess: function(request)
        {
          window.location.reload();
          deselectAll();
        },
        onFailure: function(request)
        {
        }
      })
    }

    return false;

  }

  function init(e){
     popupwrapper	= dojo.widget.byId("popupcontainer");
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER_BY}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Role</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Cari <input type="text" id="cari" name="cari" value="{CARI}" />
      <input type="submit" value="cari" onclick="idxpage.value=0;" />&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="tambahData();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->
    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="return hapusData();" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  Data Role<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th></th>
      <th>No</th>
      <th onclick="setOrder(0)">Nama Role</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"><input type="checkbox" id="checked_{ROW.no}" name="checked_{ROW.id}" value="{ROW.id}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="ubahData('{ROW.id}');" title="ubah data" ></span>&nbsp;
          <!-- END ACT_EDIT -->

          <!-- BEGIN ACT_DEL -->
          <span class="b_delete" onclick="hapusData('{ROW.id}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.namarole}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+3,sort.value,document.getElementById("tableheader"))
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>