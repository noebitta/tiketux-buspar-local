<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
  
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40><td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Transaksi Deposit Tiketux</td></tr>
			<tr class='banner' height=40>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<!--<td class='bannernormal'>Status:&nbsp;</td><td><select name='status' id='status' ><option value="" {STATUS}>-semua-</option><option value="1" {STATUS1}>Dibayar</option><option value="0" {STATUS0}>Booking</option><option value="2" {STATUS2}>Batal</option></select></td>-->
							<td class='bannernormal'>Jenis Transaksi:&nbsp;<select id='filterstatus' name='filterstatus'><option value='' {OPT_STATUS}>-semua-</option><option value=0 {OPT_STATUS0}>Kredit</option><option value=1 {OPT_STATUS1}>Debit</option></select></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<!--<td>
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td bgcolor='D0D0D0'></td>-->
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td>
								<table class="border">
									<tr><td><b>Saldo Deposit</b></td><td>:</td><td align="right">Rp. {SALDO_DEPOSIT}</td></tr>
									<tr><td><b>Transaksi Debit</b></td><td>:</td><td align="right">Rp. {JUM_TRX_DEBIT}</td></tr>
									<tr><td><b>Transaksi Kredit</b></td><td>:</td><td align="right">Rp. {JUM_TRX_KREDIT}</td></tr>
								</table>
							</td>
							<td align='right' valign='bottom'>
								{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
			<tr>
				<th width=30><a class="th" >No</th>
				<th width=150><a class="th" >Waktu Trx</th>
				<th width=400><a class="th" >Keterangan</th>
				<th width=100><a class="th" >Jumlah</th>
				<th width=50><a class="th" >Jenis Trx</th>
				<th width=100><a class="th" >Saldo</th>
			</tr>
			<!-- BEGIN ROW -->
			<tr class="{ROW.odd}">
				<td><div align="right">{ROW.no}</div></td>
				<td><div align="center">{ROW.waktu_trx}</div></td>
				<td><div align="left">{ROW.keterangan}</div></td>
				<td><div align="right">{ROW.jumlah}</div></td>
				<td><div align="center">{ROW.jenis_trx}</div></td>
				<td><div align="right">{ROW.saldo}</div></td>
			</tr>  
			<!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>