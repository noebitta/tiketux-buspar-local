<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

function getDataMember(){
	
	idmember						= document.getElementById('idmember');
	nama								= document.getElementById('nama');
	hp									= document.getElementById('hp');
	hp2									= document.getElementById('hp2');
	email								= document.getElementById('email');
	jeniskelamin				= document.getElementById('jenis_kelamin');
	kota								= document.getElementById('kota');
	pekerjaan						= document.getElementById('pekerjaan');
	tglberlaku					= document.getElementById('tglberlaku');
	beforeproses				= document.getElementById('beforeproses');
	detailmember				= document.getElementById('detailmember');
	saldo								= document.getElementById('saldo');
	jumlah_debit				= document.getElementById('jumlahdebit');
	kode_booking				= document.getElementById('kodebooking');
	
	document.getElementById("kodetoken").value="";
	idmember.style.background="white";

	valid=true;
	
	if(idmember.value==''){valid=false;idmember.style.background="red";}

	if(!valid){
		exit;
	}
	
	new Ajax.Request("reservasi.member.debit.php?sid={SID}", 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=getdatamember&idmember="+idmember.value+"&kodebooking="+kode_booking.value,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			var proses_ok=false;
			eval(request.responseText);
			
			if(!proses_ok){
				alert("Terjadi kegagalan!");
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  });
}

function konfirmasiProses(){
  document.getElementById('kodetoken').value= "";
  konfirmasiproses.style.display="none";
  prosestoken.style.display="inline-block";

  idmember						= document.getElementById('idmember');
  kodereferensi				= document.getElementById('kodereferensi');
  kodebooking					= document.getElementById('kodebooking');

  new Ajax.Request("reservasi.member.debit.php?sid={SID}",
    {
      asynchronous: true,
      method: "post",
      parameters: "mode=kirimtoken&idmember="+idmember.value+"&koderef="+kodereferensi.value+"&kodebooking="+kodebooking.value,
      onLoading: function(request){
        popuploading.show();
      },
      onComplete: function(request){
      },
      onSuccess: function(request)
      {
        popuploading.hide();
      },
      onFailure: function(request)
      {
        popuploading.hide();
        alert(request.responseText);
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    })

}

function proses(){
	
	idmember						= document.getElementById('idmember');
	kodereferensi				= document.getElementById('kodereferensi');
	kodebooking					= document.getElementById('kodebooking');
	kodetoken						= document.getElementById('kodetoken');

	if(kodetoken.value==""){
		alert("Anda belum memasukkan kode token!");
		exit;
	}
	
	new Ajax.Request("reservasi.member.debit.php?sid={SID}", 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=debit&koderef="+kodereferensi.value+"&idmember="+idmember.value+"&kodebooking="+kodebooking.value+"&kodetoken="+kodetoken.value,
    onLoading: function(request){
			popuploading.show();
		console.log("onloading");
		},
    onComplete: function(request){
		console.log("oncomplete");
    },
    onSuccess: function(request) 
    {
		console.log("onsuccess");
			popuploading.hide();
			
			var status=false;
			
			eval(request.responseText);
			console.log(request.responseText);
			if(status=="OK"){
				window.close();
			  }
      		else{
				alert("Terjadi kegagalan!");
			}

	},
    onFailure: function(request)
    {
		console.log("onfailure");
			popuploading.hide();
			alert('Error !!! Cannot Save');
			assignError(request.responseText);
    }
  });
}

dojo.require("dojo.widget.Dialog");
	
function init(e) {
	// inisialisasi variabel
	popuploading				= dojo.widget.byId("popuploading");
}
	
dojo.addOnLoad(init);

</script>

<div class="bannerjudul">&nbsp;Day Trans Addict | Transaksi</div>
<!-- BEGIN PESAN -->
<div id="pesan" style="height: 30px;text-align: center;background:red;font-size:14px;padding-top: 10px;color:white;">
	<b>{PESAN.ISI}</b>
</div>
<!-- END PESAN -->
<div id="resvregmemberwrapper">
	<div id="resvregmembercontent">
		<!-- BEGIN MEMBER -->
		<input type="hidden" name="mode" value="{MODE}">
		<input type="hidden" id='submode' name="submode" value="{SUB}">
		<input type="hidden" id="kodereferensi" value="{MEMBER.KODE_REF}">
		<input type="hidden" id="kodebooking" value="{MEMBER.KODE_BOOKING}">
		<input type="hidden" id="idmember" value="{MEMBER.DTA}">
		
		<span id="detailmember">
			<span class="resvregmemberlabel"><b>Kode Ref.</b></span><span class="resvregmemberfield"><b>: {MEMBER.KODE_REF}</b></span></br>
			<span class="resvregmemberlabel"><b>Kode DTA.</b></span><span class="resvregmemberfield"><b>: {MEMBER.DTA}</b></span></br>
			<span class="resvregmemberlabel">Nama</span><span class="resvregmemberfield">: <span id="nama">{MEMBER.NAMA}</span></span></br>
			<span class="resvregmemberlabel">Handphone</span><span class="resvregmemberfield">: <span id="hp">{MEMBER.HP}</span></span></br>
			<span class="resvregmemberlabel">Handphone 2</span><span class="resvregmemberfield">: <span id="hp2">{MEMBER.HP2}</span></span></br>
			<span class="resvregmemberlabel">Email</span><span class="resvregmemberfield">: <span id="email"></span>{MEMBER.EMAIL}</span></br>
			<span class="resvregmemberlabel">Jenis Kelamin</span><span class="resvregmemberfield">: <span id="jenis_kelamin">{MEMBER.JENISKELAMIN}</span></span></br>
			<span class="resvregmemberlabel">Kota</span><span class="resvregmemberfield">: <span id="kota">{MEMBER.KOTA}</span></span></br>
			<span class="resvregmemberlabel">Pekerjaan</span><span class="resvregmemberfield">: <span id="pekerjaan">{MEMBER.PEKERJAAN}</span></span></br>
			<span class="resvregmemberlabel">Berlaku Hingga</span><span class="resvregmemberfield">: <span id="tglberlaku">{MEMBER.TGLBERLAKU}</span></span></br>
			<br>
			<h3>Detail Transaksi</h3>
			<span class="resvregmemberlabel">Saldo</span><span class="resvregmemberfield">: Rp.<span id="saldo">{MEMBER.POIN}</span> Poin</span></br>
			<span class="resvregmemberlabel">Jumlah Debit</span><span class="resvregmemberfield">: <span id="jumlahdebit">{MEMBER.JMLDEBIT}</span> Poin</span></br>
			<br>
			<font size='1'>masukkan kode token yang dikirimkan sistem kepada pelangan</font>
			<br><br>
			<span class="resvregmemberlabel"><b>Kode Token</b></span><span class="resvregmemberfield">: <input type="text" id="kodetoken" name="kodetoken" onfocus="this.style.background='white';" maxlength="6" onkeypress="validasiAngka(event);"></span></br>
			<br>
			<input type="button" onClick="window.close();" value="&nbsp;&nbsp;&nbsp;BATAL&nbsp;&nbsp;&nbsp;">
			&nbsp;&nbsp;&nbsp;<input type="button" onClick="proses();" value="&nbsp;&nbsp;&nbsp;PROSES&nbsp;&nbsp;&nbsp;">
			</span>
		</span>
		<!-- END MEMBER -->
	</div>
</div>

