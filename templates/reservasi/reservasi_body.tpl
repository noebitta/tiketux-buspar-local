<input type="hidden" value="{HARGA_MINIMUM_PAKET}" id="hdn_harga_minimum_paket">
<input type="hidden" value="{SID}" id="hdn_SID">
<input type="hidden" value="{USERLEVELLOGED}" id="level_login">
<input type="hidden" value="{USERLEVEL}" id="user_level">
<input type="hidden" value=0 id="flag_mutasi">
<input type="hidden" value=0 id="flag_mutasi_paket">
<input type="hidden" value='' id="id_jurusan_aktif">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi.js"></script>

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td bgcolor='ffffff'>
		<table>
			<tr><td><h2>Cetak Manifest</h2></td></tr>
			<tr><td><div id="rewrite_list_mobil"></div></td></tr>
			<tr><td><div id="rewrite_list_sopir"></div></td></tr>
		</table>
		<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_SPJ_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
		<input type="button" onclick="CetakSPJ();" id="dialog_SPJ_btn_OK" value="&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog SPJ-->

<!--dialog keberangkatan pelanggan-->
<div dojoType="dialog" id="dialog_cari_keberangkatan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='700'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Keberangkatan</h2></td></tr>
			<tr><td><div id="rewrite_keberangkatan_pelanggan"></div></td></tr>
		</table>
		<span id='progress_cari_jadwal' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_jadwal_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog keberangkatan pelanggan-->

<!--dialog antrian-->
<div dojoType="dialog" id="dialog_daftar_antrian" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='700'>
			<tr>
				<td bgcolor='ffffff' height=300 valign='top' align='center'>
					<table>
						<tr><td><h2>ANTRIAN</h2></td></tr>
						<tr><td><div id="rewrite_daftar_antrian"></div></td></tr>
					</table>
					<span id='progress_cari_jadwal' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
					<br>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br>
					<input type="button" id="dialog_antrian_btn_Cancel" value="&nbsp;Cancel&nbsp;">
				</td>
			</tr>
		</table>
	</form>
</div>
<!--enddialog antrian-->


<!--dialog Paket-->
<div dojoType="dialog" id="dialog_cari_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='900'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Paket Xpress</h2></td></tr>
			<tr><td><div id="rewrite_cari_paket"></div></td></tr>
		</table>
		<span id='progress_cari_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_paket_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog Paket-->

<!--dialog isian paket -->
<div dojoType="dialog" id="dialog_isian_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table cellspacing=0 cellpadding=0 width='100%' height='100%'>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Pengirim</h2></td></tr>
						<tr><td>Telp Pengirim <span id='dlg_paket_telp_pengirim_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_telp_pengirim' type='text' maxlength='15' onFocus="Element.hide('dlg_paket_telp_pengirim_invalid');" onBlur="cariDataPelangganByTelp4Paket(this.value,1)"/></td></tr>
						<tr><td width=200>Nama Pengirim <span id='dlg_paket_nama_pengirim_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td width=5>:</td><td><input id='dlg_paket_nama_pengirim' type='text' maxlength='100' onFocus="Element.hide('dlg_paket_nama_pengirim_invalid');" /></td></tr>
						<tr><td>Alamat Pengirim</td><td>:</td><td><input id='dlg_paket_alamat_pengirim' type='text' maxlength='100' /></td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr><td colspan=3><h2>Data Penerima</h2></td></tr>
						<tr><td>Telp Penerima <span id='dlg_paket_telp_penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_telp_penerima' type='text' maxlength='15' onFocus="Element.hide('dlg_paket_telp_penerima_invalid');" onBlur="cariDataPelangganByTelp4Paket(this.value,0)"/></td></tr>
						<tr><td width=200>Nama Penerima <span id='dlg_paket_nama_penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td width=5>:</td><td><input id='dlg_paket_nama_penerima' type='text' maxlength='100' onFocus="Element.hide('dlg_paket_nama_penerima_invalid');" /></td></tr>
						<tr><td>Alamat Penerima</td><td>:</td><td><input id='dlg_paket_alamat_penerima' type='text' maxlength='100' /></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan=2><br><h2>Data Paket</h2></td></tr>
			<tr>
				<td width='50%' valign='top'>
					<table>
						<!--<tr><td width='50%'>Jumlah Pax<span id='dlg_paket_koli_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_jumlah_koli' type='text' maxlength='10' onFocus="Element.hide('dlg_paket_koli_invalid');" onBlur="hitungHargaPaket(dlg_paket_layanan.value);"/></td></tr>-->
						<tr><td>Berat (Kg) <span id='dlg_paket_berat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td>:</td><td><input id='dlg_paket_berat' type='text' maxlength='10' onFocus="Element.hide('dlg_paket_berat_invalid'); " onkeypress='validasiInputanAngka(event);' onBlur="hitungHargaPaket(document.getElementById('dlg_paket_layanan').value);" />&nbsp;Kg.</td></tr>
						<!--<tr>
							<td>Jenis Barang</td>
							<td>:</td>
							<td>
								<select id='dlg_paket_jenis_barang'>
									<option value="DOKUMEN">Dokumen</option>
									<option value="PAKET">Paket</option>
								</select>
							</td>
						</tr>-->
						<tr>
							<td>Layanan</td>
							<td>:</td>
							<td>
								<input id='dlg_paket_harga_kg_pertama_1' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_pertama_2' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_pertama_3' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_pertama_4' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_pertama_5' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_pertama_6' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_berikutnya_1' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_berikutnya_2' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_berikutnya_3' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_berikutnya_4' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_berikutnya_5' type='hidden' value='0'/>
								<input id='dlg_paket_harga_kg_berikutnya_6' type='hidden' value='0'/>
								<select id='dlg_paket_layanan' onChange="hitungHargaPaket(this.value);">
									<option value="1">Dokumen</option>
									<option value="2">Xtra Small</option>
									<option value="3">Small</option>
									<option value="4">Medium</option>
									<option value="5">Large</option>
									<option value="6">Xtra Large</option>
								</select>
							</td>
						</tr>
						<tr><td>Harga</td><td>:</td><td>Rp. <span id='rewrite_dlg_paket_harga_paket_show'></span></td></tr>
					</table>
				</td>
				<td width='50%' valign='top'>
					<table>
						<tr>
							<td>Cara Pembayaran</td>
							<td>:</td>
							<td>
								<select id='dlg_paket_cara_bayar'>
									<option value="0">Tunai</option>
									<option value="2">Bayar di Tujuan</option>
								</select>
							</td>
						</tr>
						<tr><td valign='top'>Keterangan<span id='dlg_paket_keterangan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td><td valign='top'>:</td><td><textarea id='dlg_paket_keterangan' rows='3' cols='30' onFocus="Element.hide('dlg_paket_keterangan_invalid');" ></textarea></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_paket.hide();" id="dlg_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="pesanPaket();" id="dlg_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog isian paket-->

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<div id="rewrite_ambil_paket"></div>
		<span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog ambil paket-->

<!--dialog DISCOUNT-->
<div dojoType="dialog" id="dialog_discount" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Daftar Jenis Harga</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td>Silahkan pilih harga tiket</td><td>:</td><td><div id="rewrite_list_discount_dialog"></div><input type='hidden' id='hdn_bayar_no_tiket'/></td></tr>
			<tr height=30><td>Silahkan Kategori Penumpang</td><td>:</td><td><div id="rewrite_list_kategori_dialog"></div><input type='hidden' id='kategori_penumpang'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_disc_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_disc_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_discount_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiDiscount(hdn_bayar_no_tiket.value);" id="dialog_discount_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog DISCOUNT-->

<!--dialog CETAK ULANG TIKET-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='cetak_ulang_tiket_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiket();" id="dialog_cetak_ulang_tiket_btn_OK" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog CETAK ULANG TIKET-->

<!--dialog OTP-->
<div dojoType="dialog" id="dialog_cetak_ulang_tiket_otp" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Cetak Ulang Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,silahkan masukkan kode OTP dari pelanggan</td></tr>
			<tr height=30><td>OTP</td><td>:</td><td><input type='password' id='cetak_ulang_tiket_password_otp' maxlength='6'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_cetak_ulang_tiket_btn_Cancel_otp" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="cetakUlangTiketByOTP();" id="dialog_cetak_ulang_tiket_btn_OK_otp" value="Cetak Ulang Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog OTP-->

<!--dialog KOREKSI ASURANSI-->
<div dojoType="dialog" id="dialog_koreksi_asuransi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Koreksi Asuransi</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3><div id="rewrite_list_plan_asuransi"></div><input type='hidden' id='hdn_no_tiket_koreksi_asuransi'/></td></tr>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='korek_asuransi_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='korek_asuransi_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_koreksi_asuransi_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="koreksiAsuransi(hdn_no_tiket_koreksi_asuransi.value);" id="dialog_koreksi_asuransi_btn_OK" value="Simpan perubahan">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog KOREKSI ASURANSI-->

<!--dialog BATAL-->
<div dojoType="dialog" id="dialog_batal" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pembatalan Tiket</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Untuk melakukan proses ini,minimal akses anda haruslah supervisor<br> silahkan masukkan username dan password anda</td></tr>
			<tr height=30><td>Username</td><td>:</td><td><input type='text' id='batal_username'/></td></tr>
			<tr height=30><td>Password</td><td>:</td><td><input type='password' id='batal_password'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_batal_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="batal(batal_no_tiket,batal_no_kursi);" id="dialog_batal_btn_OK" value="Batalkan Tiket">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog BATAL-->

<!--dialog Input Kode Voucher-->
<div dojoType="dialog" id="dialog_input_voucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Voucher</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td colspan=3>Silahkan masukkan KODE VOUCHER</td></tr>
			<tr height=30><td>Kode Voucher</td><td>:</td><td><input type='password' id='input_kode_voucher'/></td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="button" id="dialog_input_voucher_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="bayarByVoucher(input_kode_voucher.value);" id="dialog_input_voucher_btn_ok" value="   Bayar   ">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Input Kode Voucher-->

<!--dialog ponta-->
<div dojoType="dialog" id="dialog_input_ponta" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table>
			<tr><td><h2>Reedem Point</h2></td></tr>
			<tr>
				<td align='center'>
					<table bgcolor='white' width='100%'>
						<tr height=30><td colspan=3>Silahkan masukkan Kode OTP</td></tr>
						<tr height=30><td colspan=3><input type="password" name="input_otp_reedem" id="input_otp_redem" style="width: 250px;"></td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" id="dialog_input_ponta_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
					<input type="button" onclick="BayarByPonta();" id="dialog_input_ponta_btn_ok" value="   Proses   ">
				</td>
			</tr>
		</table>
	</form>
</div>
<!--end dialog ponta-->

<!--dialog_progres-->
<div dojoType="dialog" id="dialog_progres" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table bgcolor='white'>
			<tr>
				<td align='center'>
					Loading...<br>
					<img src="{TPL}images/loading_bar.gif" />
				</td>
			</tr>
		</table>
	</form>
</div>
<!--end dialog_progres-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30>
				<td>Silahkan pilih jenis diskon</td>
			</tr>
			<tr>
				<td align="center"><div id="rewrite_checkbox_discount"></div></td>
			</tr>
			<tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
			<tr><td>
				<input type='hidden' id='kode_booking_go_show' value='' />
				<input type='hidden' id='no_tiket_goshow' value='' />
				<table width='100%'>
					<tr>
						<td width='33%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(0);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_tunai.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>    
						</td>
						<td width='33%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(5);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_edc.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(5);return false;"><span class="genmed">EDC</span></a>
						</td>
						<td width='33%' align='center' valign='middle'>
							<a href="" onClick="showInputVoucher();return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_voucher.png" /></a>
							<br />
							<a href="" onClick="showInputVoucher();return false;"><span class="genmed">Voucher</span></a>
						</td>
					</tr>
					<tr>
						<td width='33%' align='center' valign='middle'>
							<span id="pembayaranMember">

							<a href="" onClick="showDebitMember();return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_member.png" /></a>
							<br />
							<a href="" onClick="showDebitMember();return false;"><span class="genmed">Kartu Member</span></a>

							</span>
						</td>

						<td width='33%' align='center' valign='middle'>
							<span id="pembayaranReedem">
								<a href="" onClick="InputPonta(); return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_ponta.png" /></a>
								<br />
								<a href="" onClick="InputPonta(); return false;"><span class="genmed">REDEM POINT</span></a>
							</span>
						</td>
						<!--<td width='33%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(7);return false;"><img src="{TPL}images/icon_indomaret.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(7);return false;"><span class="genmed">Indomaret</span></a>
						</td>-->

						<!-- BEGIN show_dialog_pembayaran_transfer -->
						<td align='center' valign='middle'>
                            <a href="" onClick="CetakTiket(6);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_transfer.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(6);return false;"><span class="genmed">TRANSFER</span></a>
						</td>
						<!-- END show_dialog_pembayaran_transfer -->
					</tr>
				</table>
			</td></tr>

		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog Pilih Pembayaran paket-->
<div dojoType="dialog" id="dialog_pembayaran_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;">
        <table>
            <tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
            <tr>
                <td align='center'>
                    <table bgcolor='white' width='100%'>
                        <tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
                        <tr><td>
                                <input type='hidden' id='kode_booking_go_show' value='' />
                                <table width='100%'>
                                    <tr>
                                        <td width='50%' align='center' valign='middle'>
                                            <a href="" onClick="cetakTiketPaket(0);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_tunai.png" /></a>
                                            <br />
                                            <a href="" onClick="cetakTiketPaket(0);return false;"><span class="genmed">Tunai</span></a>
                                        </td>
                                        <td width='50%' align='center' valign='middle'>
                                            <a href="" onClick="cetakTiketPaket(5);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_edc.png" /></a>
                                            <br />
                                            <a href="" onClick="cetakTiketPaket(5);return false;"><span class="genmed">EDC</span></a>
                                        </td>
                                    </tr>
                                </table>
                            </td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="reset" id="dialog_pembayaran_paket_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog Pilih Pembayaran paket-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="whiter" valign="middle" align="center">
 
<table width="100%" cellspacing="0" cellpadding="4" border="0">
<tr>
	<td height='80' width='100%'>
		<table width='100%' height='100%' cellspacing=0 cellpadding=0 class='header_reservasi'>
			<tr>	
				<td align='left' valign='middle' width='7%'  background='./templates/images/icon_cari.png' STYLE='background-repeat: no-repeat;background-position: left top;'></td>
				<td align='left' valign='middle' width='20%'>
          <span style="display: inline-block;">
            <font color='505050'><b>Cari keberangkatan</b></font><br>
            <input name="txt_cari_jadwal" id="txt_cari_jadwal" type="text" placeholder="telepon pelanggan">
            <input class='tombol' name="btn_periksajadwal" id="btn_periksajadwal" value="Cari" type="button" onClick="periksaJadwal(txt_cari_jadwal.value)">
          </span>
          <br><br>
          <span style="display: inline-block;">
            <font color='505050'><b>Cari paket Xpress</b></font><br>
            <input name="txt_cari_paket" id="txt_cari_paket" type="text" placeholder="resi/telp pelanggan">
            <input class='tombol' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)">
          </span>
        </td>
				<td align='right' width='63%' valign='middle'>
					<div class="resvmenuicon" id="resvmenuiconpengumuman"><div id="rewritepengumuman"></div></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_LAPORAN_PENJUALAN}return false" class="menu"><img src="{TPL}images/icon_penjualan.png" /><br/>Laporan Penjualan</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_LPPC}return false" class="menu"><img src="{TPL}images/icon_cashflow_sml.png" /><br/>LPPC</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_LAPORAN_UANG}return false;" class="menu"><img src="{TPL}images/icon_penjualan_uang.png" /><br/>Laporan Uang</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_BIAYA_TAMBAHAN}return false;" class="menu"><img src="{TPL}images/icon_biaya_tambahan.png" /><br/>Biaya Tambahan</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_BIAYA_PETTY_CASH}return false;" class="menu"><img src="{TPL}images/icon_pettycash_sml.png" /><br/>Petty Cash</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_PAKET}return false;" class="menu"><img src="{TPL}images/icon_paket_service.png" /><br/>Ekspedisi</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_TOPUP_MEMBER};return false;" class="menu"><img src="{TPL}images/icon_topup_member.png" /><br/>Top Up DT Addict</a></div>
					<div class="resvmenuicon"><a href="#" onclick="{U_REG_MEMBER}return false;" class="menu"><img src="{TPL}images/icon_reg_member.png" /><br/>Daftar DT Addict</a></div>
					<div class="resvmenuicon" id="bg_antrian"><a href="#" onclick="{U_ANTRIAN}return false;"  class="menu"><font color="red" size="5"><b><blink><div id="rewriteantrian"></div></blink></b></font><br/>Antrian</a></div>
					<!-- target user dihilangkan
					<div class="resvmenuicon" style="padding-top: 20px;background-color: #aaaaaa; border-radius: 5px 5px 5px 5px;border: 1px solid #a0a0a0;height:60px;width:140px;"><span style="font-size: 14px;font: trebuchet;font-weight: bold;color: #e0e0e0;">TARGET</span><br><span style="font-size: 18px; color: white;"><span id="rewritetarget"></span></span></div>
					-->
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
 <td valign="middle" align="left">    
  <table width="100%" class='reservasi_background' cellpadding='0' cellspacing='0'>
		<tr>
			<td width="230" valign="top">       
				<table border="0" width="100%" cellpadding='0' cellspacing='0'>
					<tr><td colspan=2 class='formHeader'><strong>1. Jadwal</strong></td>
					</tr>
					<tr>
						<td><td colspan=2 align='center'><strong>Tanggal keberangkatan</strong></td>
					</tr>
					<tr>
						<td colspan=2 align='center'>
							<!--  kolom kalender -->
							<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
							</iframe>
		
							<form name="formTanggal">
								<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
							</form>
							<!--end kolom kalender-->
							
							<input name="update" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
							
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width='100%'> 
							  <tr><td>KOTA KEBERANGKATAN<br></td></tr>
						    <tr>
									<td>
										<select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<div id="rewritetype"></div>
						</td>
					</tr>
					<!-- Bagian ini akan kita rewite saat requestForUpdate dipanggil -->
					<tr>
						<td colspan="2" height=150 valign='top'>
							<div id="rewriteall">
								<div id="rewriteasal"></div>
								<div id="rewritetujuan"></div>
								<div id="rewritejadwal"></div>
								<!--<span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span>-->
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><span class="noticeMessage">KETERANGAN:</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_kosong_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi masih kosong</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_pesan_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke POOL INI</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_konfirm_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke POOL ini</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_pesan1_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah di-BOOK berangkat dari/ke pool PICK-UP</span></td>
					</tr>
					<tr>
						<td valign="top"><img src='./templates/images/kursi_konfirm1_ket.gif' /></td>
						<td><span class="noticeMessage">Kursi sudah DIBAYAR berangkat dari/ke pool PICK-UP</span></td>
					</tr>
				</table>
				<!-- INFO SISA KURSI KOSONG 2 JAM BERIKUTNYA -->
				<!--<div class='border' align='center'><b>Sisa kursi di jadwal-jadwal berikutnya</b><br><hr>
				<div align='left' id="rewrite_sisa_kursi"></div>
				</div>-->
				<!-- info tweater-->
				<!--<script src='http://widgets.twimg.com/j/2/widget.js'></script>
				<script>
				new TWTR.Widget({
				  version: 2,
				  type: 'profile',
				  rpp: 7,
				  interval: 45000,
				  width: 250,
				  height: 300,
				  theme: {
				    shell: {
				      background: '#e64017',
				      color: '#ffffff'
				    },
				    tweets: {
				      background: '#fad419',
				      color: '#6e6e6e',
				      links: '#0753eb'
				    }
				  },
				  features: {
				    scrollbar: true,
				    loop: true,
				    live: true,
				    hashtags: true,
				    timestamp: true,
				    avatars: false,
				    behavior: 'default'
				  }
				}).render().setUser('@TMCPoldaMetro').start();
				</script>
				<!--end info teater-->
			</td>
			<td width=1 bgcolor='e0e0e0'></td>
			<td valign='top' width='400'>
				<!--LAYOUT KURSI DAN ISIAN DATA PENUMPANG-->
				<table width='100%' cellpadding='0' cellspacing='0'>
					<tr>
						<td width='100%' valign='top' align='center'>
							<!--LAYOUT KURSI-->
							<div align='left' class='formHeader'><strong>2. Layout Kursi</strong></div>
							<br>
							<span id='loading_layout_kursi' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Loading...</font></span>
							<div id="rewritemobil"></div>
						</td>
					</tr>
					<tr width='100%'>
						<td align='center' valign='top' width='50%'>
							<div id="rewritepaket">
							<!-- LAYOUT PAKET-->
							</div>
						</td>
					</tr>
				</table>
			</td>       
			<td width=1 bgcolor='e0e0e0'></td>
			<!--LAYOUT DATA ISIAN PENUMPANG-->
			<td valign='top' width='370'>
				<div align='left' class='formHeader'><strong>3. Data Penumpang</strong></div>
				<br>							
				<!--data pelanggan-->
				<div align='center'>
					<span id='loading_data_penumpang' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Sedang memproses...</font></span>
					<div id="dataPelanggan"></div>
				</div>
				
					<table width='100%'>
						<tr>
							<td width='100%'>
								<table width="100%"> 
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_member_not_found' style='display:none;'>
												<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>ID Member belum pernah terdaftar</b></font></td></tr></table>
											</span>
											<span id='progress_cari_member' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data member...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
									<tr>
										<td><font size=3><b>Telepon:</b></font></td>
										<td><input name="hdn_no_telp_old" id="hdn_no_telp_old" type="hidden"/>
											<input name="ftelp" id="ftelp" type="text" onkeypress='validasiNoTelp(event);'  onFocus="this.style.background='';Element.hide('progress_cari_penumpang');" onBlur="cariDataPelangganByTelp(ftelp.value)">
											<!--<input class='tombol' name="btn_periksanotelp" id="btn_periksanotelp" value="Cari" type="button" onClick="cariDataPelangganByTelp(ftelp.value)">-->
										</td>
									</tr>
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_not_found' style='display:none;'>
												<table width='100%'><tr><td bgcolor='ffff00' align='center'><font color='ff0000 '><b>No.Telp belum pernah terdaftar</b></font></td></tr></table>
											</span>
											<span id='progress_cari_penumpang' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data penumpang...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
									<tr><td>Nama:</td><td><input name="fnama" id="fnama" type="text" onFocus="this.style.background='';Element.hide('label_not_found');"></td></tr>
									<!--<tr>
										<td>Penumpang:</td>
										<td><span id="rewrite_list_discount"></span><span id="rewrite_keterangan_member" style='display:none'><b>Pelanggan Setia</b></span></td>
									</tr>-->
									<tr>
										<td>Penumpang:</td>
										<td><span id="rewrite_list_kategori">
												<select name="kategori_penumpang" id="kategori_penumpang">
													<option value="">Silahkan Pilih</option>
													<option value="1">Dewasa/Umum</option>
													<option value="2">Manula</option>
												</select>
											</span>
										</td>
									</tr>
                                    <!--UPDATE 31-03-2016 TAMBAH FIELD KATU PONTA -->
                                    <tr>
                                        <td>Kartu Ponta:</td>
                                        <td>
                                            <input type="text" name="ponta" id="ponta" onblur="cariKartuPonta()">
											<input type="hidden" id="valid_ponta" value="1">
                                        </td>
                                    </tr>
									<tr>
										<td colspan=3 align='center' height=30 >
											<span id='label_ponta_not_found' style='display:none;'>
												<table width='100%'>
													<tr>
														<td bgcolor='ffff00' align='center'>
															<font color='ff0000 '>
																<b>Kartu Ponta Tidak Terdaftar</b>
															</font>
														</td>
													</tr>
												</table>
											</span>
											<span id='label_ponta_max_trx' style='display:none;'>
												<table width='100%'>
													<tr>
														<td bgcolor='ffff00' align='center'>
															<font color='ff0000 '>
																<b>Kartu Telah digunakan lebih dari 3 kali hari ini</b>
															</font>
														</td>
													</tr>
												</table>
											</span>
											<span id='progress_cari_penumpang' style='display:none;'>
												<table width='100%'><tr><td bgcolor='778899' align='center'><img src='./templates/images/loading.gif' /><font color='EFEFEF' size=2>&nbsp;&nbsp;<b>mencari data penumpang...</b></font></td></tr></table>
											</span>
										</td>
									</tr>
                                    <!--UPDATE 31-03-2016 END FIELD KATU PONTA -->
									<tr>
										<td>Keterangan: <span id='alamat_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
										<td><textarea name="fadd" id="fadd" cols="30" rows="2" onFocus="Element.hide('alamat_invalid');Element.hide('label_not_found');"></textarea>
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="checkbox" name="is_infant" id="is_infant" onclick="setIsInfant()" value="1">Bersama Bayi</td>
									</tr>
									<tr>
										<td></td>
										<td><div id="infant" style="display: none;">Nama Bayi : <input type="text" name="nama_infant" id="nama_infant" > </div></td>
									</tr>
									<tr><td align='center' colspan=2 height=40><input type="button" onclick="pesanKursi(1);" id="hider1" value="               G O   S H O W                "></td></tr>
									<tr><td align='center' colspan=2 height=40><input type="button" onclick="pesanKursi(0);" id="hider1" value="               B O O K I N G                "></td></tr>
									<tr><td align='center' colspan=2 height=40>Daftar Tunggu:<select type='text' id='jumlahpesanwl' ><option value='1'/>1 orang</option><option value='2'/>2 orang</option><option value='3'/>3 orang</option></select>&nbsp;<input type="button" onclick="saveWaitingList();" id="hider2" value="TAMBAH KE DAFTAR&nbsp;"></td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign='top'>
								<table width='100%' >
									<tr><td  align='center' height=40 ><input type="button" onclick="resetIsianPaket();dialog_paket.show();" id="btn_paket" value="          P A K E T   X P R E S S           "></td></tr>
									<!--<tr><td class="border" align='center'><input type="button" onclick="showDialogSwapKartu();" id="btn_transaksi_member" value="Transaksi Member"/></td></tr>-->
								</table>
							</td>
						</tr>
						<tr>
							<td align='center' valign='top' width='100%' colspan=3>
								<!-- LAYOUT WAITING LIST-->
								<!--<div id="rewritewaitinglist"></div>-->
							</td>
						</tr>
					</table>

			</td>
    </tr>        
  </table>   
 </td>
</tr>
</table>

</td>
</tr>
</table>