<div id="resvdetailbookwrapper">
	<input type='hidden' name='no_tiket' id='no_tiket' value='{NO_TIKET}'>
	<input type='hidden' name='kode_booking' id='kode_booking' value='{KODE_BOOKING}'>
	<input type='hidden' name='cetak_tiket' id='cetak_tiket' value='{CETAK_TIKET}'>
	<input type='hidden' name='kursi' id='kursi' value='{KURSI}'>
	<input type="hidden" name="otpreedem" id="otpreedem" value="{OTPREEDEM}">
	
	<h1>Kursi: {KURSI}</h1>
	<h3>({KODE_JADWAL})</h3>
	
	<div id="resvdetailbookcontent">
		<h3>PENUMPANG</h3>
		<span class="resvdetailbooklabel">#Booking</span><span class="resvdetailbookfield" style="color:blue;"><b>: {KODE_BOOKING}</b></span></br>
		<span class="resvdetailbooklabel">#Tiket</span><span class="resvdetailbookfield"><b>: {NO_TIKET}</b></span></br>
		<!-- BEGIN showpaymentcode -->
		<span class="resvdetailbooklabel">Kode payment</span><span class="resvdetailbookfield"><b>: {PAYMENT_CODE}</b></span></br>
		<!-- END showpaymentcode -->

		<!-- BEGIN showOtpReedem -->
		<span class="resvdetailbooklabel">OTP Reedem Point</span><span class="resvdetailbookfield"><b>: {OTPREEDEM}</b></span></br>
		<!-- END showOtpReedem -->

		<span class="resvdetailbooklabel">Id Ponta</span><span class="resvdetailbookfield"><b>: {IDPONTA}</b></span></br>
		<span class="resvdetailbooklabel">Telepon</span><span class="resvdetailbookfield">: <input type="text" id="ubah_telp_penumpang" value="{TELP}" onkeypress="validasiNoTelp(event);" onFocus="this.style.background='white';" /></span></br>
		<span class="resvdetailbooklabel">Nama</span><span class="resvdetailbookfield">: <input type="text" id="ubah_nama_penumpang" value="{NAMA}" onFocus="this.style.background='white';" /></span></br>
		<span class="resvdetailbooklabel">Kategori</span><span class="resvdetailbookfield">:
			<select id="kategori_penumpang">{LIST_KATEGORI}</select>
		</span></br>
		<!--
        <span class="resvdetailbooklabel">Penumpang</span><span class="resvdetailbookfield">:
			<!-- BEGIN showlistdiscount -->
			<select id='id_discount'>{LIST_DISCOUNT}</select>
			<!-- END showlistdiscount -->
			
			<!-- BEGIN showpromoberlaku -->
			{LIST_DISCOUNT}
			<!-- END showpromoberlaku -->
		</span></br>
		-->
		<!-- BEGIN showinfant -->
		<span class="resvdetailbooklabel">Nama Bayi</span><span class="resvdetailbookfield">: <input type="text" id="ubah_nama_bayi" value="{NAMA_INFANT}" onFocus="this.style.background='white';" />
		{WITH_INFANT}</span></br>
		<!-- END showinfant -->

		<span class="resvdetailbooklabel">Keterangan</span><span class="resvdetailbookfield" style="vertical-align: top;">: <textarea id="ubah_alamat_penumpang" cols="30" rows="2">{KETERANGAN}</textarea></span></br>
		<!-- BEGIN tombolsimpan -->
		<div style="text-align: center;"><br><input type="button" value="  Update  " onClick="ubahDataPenumpang('{NO_TIKET}');" /></div>
		<!-- END tombolsimpan -->
		
		<hr noshade>
		<h3>TARIF</h3>
		<span class="resvdetailbooklabel">Harga tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{HARGA_TIKET}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{DISCOUNT}</span></br>
		<!-- BEGIN showasuransi -->
		<span class="resvdetailbooklabel">Asuransi</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{CHARGE}</span></br>
		<!-- END showasuransi -->
		<span class="resvdetailbooklabel">Total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{TOTAL}</b></span></br>
		
		<!-- BEGIN showalltarif -->
		<hr noshade>
		<h3>KESELURUHAN TARIF</h3>
		<span class="resvdetailbooklabel">Total tiket</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">{TOTAL_TIKET}</span></br>	
		<span class="resvdetailbooklabel">Sub total</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{SUB_TOTAL}</span></br>	
		<span class="resvdetailbooklabel">Diskon</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;">Rp.{TOTAL_DISCOUNT}</span></br>	
		<span class="resvdetailbooklabel">Total bayar</span><span class="resvdetailbookfield">:</span><span style="float: right;padding-right: 20px;"><b>Rp.{TOTAL_BAYAR}</b></span></br>	
		<!-- END showalltarif -->
		
		<hr noshade>
		<h3>INFO</h3>
		<span class="resvdetailbooklabel">CSO pemesan</span><span class="resvdetailbookfield">: <b>{CSO_PEMESAN}</b></span></br>
		<span class="resvdetailbooklabel">Waktu Pesan</span><span class="resvdetailbookfield">: {WAKTU_PESAN}</span></br>
		<span class="resvdetailbooklabel">Status Pesanan</span><span class="resvdetailbookfield">: <b>BELUM DIBAYAR</b></span></br>
		
		<!-- BEGIN infomutasi -->
		<hr noshade>
		<h3>MUTASI</h3>
		<span class="resvdetailbooklabel">Waktu mutasi</span><span class="resvdetailbookfield">: {WAKTU_MUTASI}</span></br>	
		<span class="resvdetailbooklabel">Dimutasi dari</span><span class="resvdetailbookfield">: {DIMUTASI_DARI}</span></br>	
		<span class="resvdetailbooklabel">Dimutasi oleh</span><span class="resvdetailbookfield">: {DIMUTASI_OLEH}</span></br>	
		<!-- END infomutasi -->
		
		<div style="text-align: center;">
			<hr noshade>
			<!-- BEGIN showtombol -->
			<input type='button' onclick="cetakTiketCepat('{KODE_BOOKING}');getDataListCheckBoxDiscount(0,0);" value="CETAK TIKET" style="width: 230px;height: 40px;font-size: 14px;font-weight: bold;" /><br><br>
			<input type='button' onclick="{BATAL_ACTION};" value="BATAL" style="width: 230px;height: 20px;font-size: 14px;font-weight: bold;"/><br><br>
			<input type='button' onclick="setFlagMutasi();" id='btn_mutasi' value="MUTASI PENUMPANG" style="width: 230px;height: 20px;font-size: 14px;font-weight: bold;"><br><br>
			<!-- END showtombol -->
			<!-- BEGIN showtombolindomaret -->
			<input type='button' onclick="requestIndomaret('{KODE_BOOKING}');" id='btn_indomaret' value="Bayar Indomaret" style="width: 230px;height: 20px;font-size: 14px;font-weight: bold;">
			<!-- END showtombolindomaret -->
			<!-- BEGIN show_batal_online -->
			<input type='button' onclick="{BATAL_ACTION};" value="BATAL" style="width: 230px;height: 20px;font-size: 14px;font-weight: bold;"/><br><br>
			<!-- END shoe_batal_online -->
		</div>
		
		
	</div>
</div>

