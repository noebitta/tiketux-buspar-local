<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	= document.getElementById('tahun').value;
		
		window.location="{URL}?bulan="+bulan+"&tahun="+tahun;
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" >
<tr >
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr height=40 class="bannercari">
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Rekap Reservasi</td>
				<td class="bannerjudul">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<br>
					<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align="center">
								{LIST_BULAN}
								&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;												
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
	<table class="border" width='100%' >
    <tr style="background:#da251d;color:white;text-align:center;">
       	<td rowspan="2">Route</td>
		<td rowspan="2">Wilayah</td>
		<td colspan="{JUMLAH_HARI}">Tanggal</td>
		<td rowspan="2">Total Counter</td>
    </tr>
    <tr style="background:#da251d;color:white;text-align:center;">
       	{LIST_TANGGAL}
    </tr>

    {LIST_DATA}

	<tr bgcolor='ffff00'>
        <td colspan="2"><div align="center"><font size=3 color='{ROW.font_color}'><b>TOTAL</b></font></div></td>
        {LIST_TOTAL}
        <td align="right"><b>{TOTAL_KESELURUHAN}</b></td>
    </tr>
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>