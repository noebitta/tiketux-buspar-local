<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

function validateInput(){

	nama		= document.getElementById('nama');
	hp		    = document.getElementById('hp');
	alamat		= document.getElementById('alamat');
	kota		= document.getElementById('kota');
	pekerjaan	= document.getElementById('pekerjaan');
	ponta		= document.getElementById('kartuponta');
    Element.hide('ponta_invalid');
  nama.style.background="white";
	hp.style.background="white";
	alamat.style.background="white";
	kota.style.background="white";
	pekerjaan.style.background="white";
	ponta.style.background="white";
	valid=true;
	
	if(nama.value==''){valid=false;nama.style.background="red";}
	if(ponta.value==''){valid=false; Element.show('ponta_invalid');}
	hp_string	= hp.value;
	if(hp.value=='' || hp_string.length<9){valid=false;hp.style.background="red";}
  if(email.value==''){valid=false;email.style.background="red";}
	if(alamat.value==''){valid=false;alamat.style.background="red";}
	if(kota.value==''){valid=false;kota.style.background="red";}
	if(pekerjaan.value==''){valid=false;pekerjaan.style.background="red";}
	
	if(valid){
    popuploading.show();
		return true;
	}
	else{
		return false;
	}
}

dojo.require("dojo.widget.Dialog");

function init(e) {
  // inisialisasi variabel
  popuploading				= dojo.widget.byId("popuploading");
}

dojo.addOnLoad(init);

</script>

<div class="bannerjudul">&nbsp;Day Trans Addict | Pendaftaran</div>
<!-- BEGIN pesan -->
<div style="height: 30px;text-align: center;background:{BGCOLOR_PESAN};font-size:14px;padding-top: 10px;color:{COLOR_PESAN};"><b>{PESAN}</b></div>
<!-- END pesan -->
<div id="resvregmemberwrapper">
	<div id="resvregmembercontent">
		<form name="frm_data" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
		
		<input type="hidden" name="mode" value="{MODE}">
		<input type="hidden" id='submode' name="submode" value="{SUB}">
		<input type="hidden" name="id_member" value="{ID_MEMBER}">
		
		<span class="resvregmemberlabel"><b>Kode DTA</b></span><span class="resvregmemberfield"><b>: {ID_MEMBER}</b></span></br>
		<span class="resvregmemberlabel"><b>ID Kartu Ponta</b></span><span class="resvregmemberfield">: <input type="text" id="kartuponta" name="kartuponta" value="{NOMOR_KARTU}" maxlength=50><span id='ponta_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></span></br>
		<span class="resvregmemberlabel"><u>Nama</u></span><span class="resvregmemberfield">: <input type="text" id="nama" name="nama" value="{NAMA}" maxlength=50 onfocus="this.style.background='white';" style="text-transform: uppercase;"></span></br>
		<span class="resvregmemberlabel"><u>Handphone</u></span><span class="resvregmemberfield">: <input type="text" id="hp" name="hp" value="{HP}" onkeypress='validasiNoTelp(event);' maxlength=20 onfocus="this.style.background='white';"></span></br>
		<span class="resvregmemberlabel">Handphone 2</span><span class="resvregmemberfield">: <input type="text" id="hp2" name="hp2" value="{HP2}" onkeypress='validasiNoTelp(event);' maxlength=20 /></span></br>
		<span class="resvregmemberlabel"><u>Email</u></span><span class="resvregmemberfield">: <input type="text" id="email" name="email" value="{EMAIL}"  maxlength=30 style="text-transform: lowercase"></span></br>
		<span class="resvregmemberlabel">Jenis Kelamin</span><span class="resvregmemberfield">: <select id="jenis_kelamin" name="jenis_kelamin"><option value=0 {JK_0}>Laki-laki</option><option value=1 {JK_1}>Perempuan</option></select></span></br>
		<span class="resvregmemberlabel">Tempat Lahir</span><span class="resvregmemberfield">: <input type="text" id="tempat_lahir" name="tempat_lahir" value="{TEMPAT_LAHIR}" maxlength=30></span></br>
		<span class="resvregmemberlabel"><u>Tanggal Lahir</u></span><span class="resvregmemberfield">: <input id="tgl_lahir" name="tgl_lahir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_LAHIR}"></span></br>
		<span class="resvregmemberlabel"><u>Alamat</u></span>: <span class="resvregmemberfield"><textarea name="alamat" id="alamat" cols="30" rows="3"  maxlength=150 onfocus="this.style.background='white';">{ALAMAT}</textarea></span></br>
		<span class="resvregmemberlabel"><u>Kota</u></span><span class="resvregmemberfield">: <input type="text" id="kota" name="kota" value="{KOTA}" maxlength=30 onfocus="this.style.background='white';"></span></br>
		<span class="resvregmemberlabel"><u>Pekerjaan</u></span><span class="resvregmemberfield">: <input type="text" id="pekerjaan" name="pekerjaan" value="{PEKERJAAN}" maxlength=50 onfocus="this.style.background='white';" /></span></br>
		<span class="resvregmemberlabel">Berlaku Hingga</span><span class="resvregmemberfield">: <input id="tgl_berlaku" name="tgl_berlaku" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERLAKU}"></span></br>
		<br>
		<span style="text-align: center;display: inline-block; width: 400px;">
			<input type="button" onClick="window.close();" value="&nbsp;&nbsp;&nbsp;{TOMBOL_BATAL_VAL}&nbsp;&nbsp;&nbsp;">
			<!-- BEGIN tombolsubmit -->
			&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			<!-- END tombolsubmit -->
		</span>
		</form>
	</div>
</div>

