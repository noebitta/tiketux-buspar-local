<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
		window.opener.getUpdateMobil();
	}
</script>	 

<body class='tiket'>
	
<!-- BEGIN ROW -->
<div style="font-size:14px;width:400px;height: 470px;">
	<span style="font-size:22px;">{ROW.NAMA_PERUSAHAAN}</span><br>
	<br><br><br>
	{ROW.NO_TIKET}<br>
	{ROW.DUPLIKAT}
	By: KARTU MEMBER<br>
	Pergi:{ROW.TGL_BERANGKAT}<br>
	{ROW.JURUSAN}<br>
	
	<div style="text-align: center;width:400px;">
		{ROW.JAM_BERANGKAT}<br>
		{ROW.NAMA_PENUMPANG}<br>
		Kursi<br>
		<span style="font-size:22px;">{ROW.NOMOR_KURSI}</span><br>
		{ROW.NAMA}<br>
	</div>
	DIBAYAR DENGAN DEPOSIT MEMBER<br>
	CSO:{ROW.CSO}<br>
	WaktuCetak<br>
	{ROW.WAKTU_CETAK}<br>
	<div style="text-align: center;width:400px;"">
		<br>
		{ROW.PESAN}<br>
		<br>
		-- Terima Kasih --
	</div><br>
</div>
<!-- END ROW -->
<!-- BEGIN MEMBER -->
<div style="font-size:14px;width:400px; height: 450px;">
	<div style="text-align: center;width:400px;">
		<br><br><br>
		------DATA MEMBER----<br>
		<br>
		<b>#Referensi:{MEMBER.KODE_REFERENSI}</b><br>
		#Member:{MEMBER.KODE_MEMBER}<br>
		Nama:{MEMBER.NAMA_MEMBER}<br>
		Berlaku hingga:{MEMBER.MASA_BERLAKU}<br>
		Credit:{MEMBER.JUMLAH_TIKET_DIBELI} Poin<br>
		<b>Saldo:{MEMBER.SALDO} Poin<br></b>
		<br>
	</div>
	<br><br><br>
</div>
<!-- END MEMBER -->
</body>

<script language="javascript">
	printWindow();
	window.close();
</script>
</html>
