<div class="resvsectioncard">
  <input type="hidden" id="kodejadwalutama" name="kodejadwalutama" value="{KODE_JADWAL_UTAMA}"/>
  <span class="resvsectiontitle">Data Keberangkatan</span>
  <div class="resvsectioncontent">
    <span class="resvisidatalabel">MANIFEST</span><span class="resvdisidatafield">:{NO_MANIFEST_PAKET}</span>
    <!-- BEGIN MANFIFEST_INDUK -->
    <span class="resvisidatalabel">MANIFEST INDUK</span><span class="resvdisidatafield">:{MANFIFEST_INDUK.nomanifest}</span>
    <!-- END MANIFEST_INDUK -->
    <span class="resvisidatalabel">UNIT</span><span class="resvdisidatafield">:{NO_UNIT} ({NO_PLAT})</span>
    <span class="resvisidatalabel">DRIVER</span><span class="resvdisidatafield">:{DRIVER} {NIK}</span>
    <span class="resvisidatalabel">TGL.BERANGKAT</span><span class="resvdisidatafield">:<b>{TGL_BERANGKAT}</b></span>
    <span class="resvisidatalabel">JADWAL</span><span class="resvdisidatafield">:<b>{KODE_JADWAL}</b></span>
    <!-- BEGIN ALERT_JADWALOFF -->
    <br><br><span style="text-transform: uppercase;font-size: 16px;color: red;">{ALERT_JADWALOFF.pesan}</span><br><br>
    <!-- END ALERT_JADWALOFF -->
    <!-- BEGIN BTN_MANIFEST -->
    <span class="flatbutton" onClick="{BTN_MANIFEST.action}"  style="padding-top: 3px;width: 150px;height: 20px;margin-top:10px;margin-bottom: 10px;">CETAK MANIFEST</span>
    <!-- END BTN_MANIFEST -->
    <!-- BEGIN BTN_MUTASI -->
    <span class="flatbutton" onClick="prosesMutasiPaket();"  style="padding-top: 3px;width: 150px;height: 20px;margin-top:10px;margin-bottom: 10px;">MUTASIKAN KE SINI</span>
    <!-- END BTN_MUTASI -->
  </div>
</div>
<!-- BEGIN SHOW_HEADER_LAYOUT -->
<input type="hidden" value='OK' id="jadwalok"/>
<div class="resvsectioncard">
  <span class="resvsectiontitle">Susunan Kursi</span>
  <div class="resvsectioncontent">
    Total Penumpang: {TOTAL_PENUMPANG} | Total Omzet: Rp.{OMZET_PENUMPANG}
  </div>
  <div id="resvpaketcontainerkursi">
    <table id="tablelayout"></table>
  </div>
</div>
<!-- END SHOW_HEADER_LAYOUT -->

