<link rel="stylesheet" href="{TPL}/kalender/kalender.css" type="text/css" />
<script language="JavaScript"  src="{TPL}kalender/kalender.js"></script>
<script type="text/javascript">filePath = '{TPL}js/dropdowncalendar/images/';</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript"  src="{TPL}js/reservasi.js"></script>
<form id="formsubmit" method="post" action="" target="newwindow">
  <input type="hidden" value="" id="kodebookdipilih" name="kodebookdipilih"/>
  <input type="hidden" value="" id="notiketdipilih" name="notiketdipilih"/>
  <input type="hidden" value="" id="tujuandipilih" name="tujuandipilih"/>
  <input type="hidden" value="" id="otp" name="otp"/>
</form>

<form id="formsubmitmanifest" method="post" action="" target="newwindow">
  <input type="hidden" value="" id="submittglberangkat" name="submittglberangkat"/>
  <input type="hidden" value="" id="submitjadwal" name="submitjadwal"/>
  <input type="hidden" value="" id="otpmanifest" name="otpmanifest"/>
</form>

<input type="hidden" value="0" id="flagmutasiresv" />
<input type="hidden" value="" id="tglberangkat" />
<input type="hidden" value="" id="kotadipilih" />
<input type="hidden" value="" id="asaldipilih" />
<input type="hidden" value="" id="jadwaldipilih" />
<input type="hidden" value="" id="kotadipilihid" />
<input type="hidden" value="" id="asaldipilihid" />
<input type="hidden" value="" id="tujuandipilihid" />
<input type="hidden" value="" id="jadwaldipilihid" />

<!-- SHOW PESAN BARU -->
<span class="resvnotif" style="display: none;">Anda Memiliki <span id="jumlahpesanbaru"></span> pesan baru. Klik untuk membuka</span>
<span id="resvmutasimodeon" style="display: none;">anda sedang berada dalam mode mutasi! <a href="" onclick="batalkanModeMutasi();return false;" class="crud">&nbsp;Batalkan Mode Mutasi&nbsp;</a></span>

<div id="wrapperresvbody" style="position: relative">
  <div id="bodyoverlay" class="resvsectionoverlayload" style="display: none;"></div>
  <div id="wrapperresvheader" style="box-shadow: 0px 5px 5px grey;height: 50px;">
    <span style="padding-left: 10px;padding-top:10px;display:inline-block; float: left;"><input name="inputcari" id="inputcari" type="text" placeholder="kode book/no.tiket/telp" style="width: 210px;text-align: center;text-transform: uppercase;height: 23px;font-size: 13px" onclick="this.style.background='white';" onkeypress="validCharInput(event);">&nbsp; <span class="flatbutton" onclick="cariDataReservasi(inputcari.value);"  style="padding-top: 3px;width: 70px;height: 20px;">CARI</span></span>
    <span style="padding-right: 4px;display:inline-block; float: right;">
      &nbsp;
    </span>
  </div>

  <div style="padding-top: 20px;"></div>
  <div id="wrapperresvinput">
    <div class="resvsection" style="width:23%;">
      <div class="resvsectioncard" style="height: 190px;">
        <div id="showkalender" style="height:170px;padding-top:"></div>
      </div>

      <div class="resvsectioncard">
        <span class="resvsectiontitle">Kota Asal</span>
        <div id="resvshowkota">
          <!-- BEGIN OPT_KOTA -->
          <div class="resvlistpilihan" id="kotaasal{OPT_KOTA.idx}" onclick="setCabangBerangkat('{OPT_KOTA.kota}',this.id);">{OPT_KOTA.kota}</div><br>
          <!-- END OPT_KOTA -->
        </div>
      </div>

      <div class="resvsectioncard">

        <span class="resvsectiontitle">Point Asal</span>
        <div class="resvsectioncontent">
          <div id="resvshowpointasal">pilih kota</div>
          <div id="asaloverlay" class="resvsectionoverlayload" style="display: none;"></div>
        </div>
      </div>

      <div class="resvsectioncard">
        <span class="resvsectiontitle">Point Tujuan</span>
        <div class="resvsectioncontent">
          <div id="resvshowpointtujuan">
            pilih point asal
          </div>
          <div id="tujuanoverlay" class="resvsectionoverlayload" style="display: none;"></div>
        </div>
      </div>

      <div class="resvsectioncard">
        <span class="resvsectiontitle">Jadwal</span>
        <div class="resvsectioncontent">
          <div id="resvshowjadwal">
            pilih point tujuan
          </div>
          <div id="jadwaloverlay" class="resvsectionoverlayload" style="display: none;"></div>
        </div>
      </div>
    </div>

    <div class="resvsection" style="width:37%;">
      <div class="resvsectioncontent">
        <div id="showlayout"></div>
        <div id="layoutoverlay" class="resvsectionoverlayload" style="display: none;"></div>
      </div>
    </div>

    <div class="resvsection" style="width:39%;">

      <div class="resvsectioncontent">

        <div id="inputoverlay" class="resvsectionoverlay" style="display: none;"></div>

        <div class="resvsectioncard" style="padding-bottom: 10px">
          <span class="resvsectiontitle" id="sectiondatapemesan">Data Pemesanan</span>
          <span class="resvisidatalabel">Telp.Pemesan</span><span class="resvdisidatafield">&nbsp;<input id="telppemesan" type="text" maxlength="15" onkeypress="validasiNoTelp(event);" onFocus="this.style.background='white';" onBlur="cariDataPelanggan(this.value,1)"/></span>
          <span class="resvisidatalabel">Nama Pemesan</span><span class="resvdisidatafield">&nbsp;<input id="namapemesan" type="text" maxlength="100" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"/></span>
          <span class="resvisidatalabel">Keterangan</span><span class="resvdisidatafield">&nbsp;<textarea id="keterangan" rows="3" rcols="40" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"></textarea></span>
          <span id="wrapperinputwaitinglist" style="display:none;">
            <span class="resvisidatalabel">Jumlah Kursi W/L</span><span class="resvdisidatafield">&nbsp;<select id="jumlahkursiwl" onFocus="this.style.background='white';"><option value="1">1 orang</option><option value="2">2 orang</option><option value="3">3 orang</option></select></span>
            <br><br>
            <span class="flatbutton" id="btngoshow" onClick="tambahWaitingList();"  style="padding-top: 3px;width: 70px;height: 20px;">W/LIST</span>
          </span>
        </div>

        <div class="resvsectioncard" style="padding-bottom: 10px">
          <span class="resvsectiontitle" id="sectiondatapenumpang">Data Penumpang</span>
          <span id="wrappernamapenumpang">belum ada kursi dipilih</span>
        </div>

        <div class="resvsectioncard" style="padding-bottom: 10px">
          <br>
          <span class="resvisidatalabel">Kode Voucher (jika ada)</span><span class="resvdisidatafield">&nbsp;<input id="kodevoucher" type="text" maxlength="100" onkeypress="validCharInput(event);" onFocus="this.style.background='white';"/></span><br><br>
          <span class="flatbutton" onClick="resetInput();"  style="padding-top: 3px;width: 70px;height: 20px;">RESET</span>&nbsp;
          <span class="flatbutton" id="btnbooking" onClick="prosesBooking();"  style="padding-top: 3px;width: 70px;height: 20px;">BOOKING</span>
          <span class="flatbutton" id="btngoshow" onClick="prosesGoshow();"  style="padding-top: 3px;width: 70px;height: 20px;">GO SHOW</span>
        </div>
      </div>
    </div>
  </div>
</div>
