<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

function getDataMember(){
	
	ponta      = document.getElementById('ponta');
	jumlahtopup	  = document.getElementById('jumlahtopup');
	nama			    = document.getElementById('nama');
	hp				    = document.getElementById('hp');
	hp2				    = document.getElementById('hp2');
	email			    = document.getElementById('email');
	jeniskelamin	= document.getElementById('jenis_kelamin');
	kota			    = document.getElementById('kota');
	pekerjaan		  = document.getElementById('pekerjaan');
	tglberlaku		= document.getElementById('tglberlaku');
	detailmember	= document.getElementById('detailmember');

	ponta.style.background="white";
	valid=true;
	
	if(ponta.value==''){valid=false;ponta.style.background="red";}
  if(jumlahtopup.value=='' || jumlahtopup.value<100000){valid=false;jumlahtopup.style.background="red";}

	if(!valid){
		exit;
	}
	
	new Ajax.Request("reservasi.member.topup.php?sid={SID}", 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=getdatamember&ponta="+ponta.value+"&jumlahtopup="+jumlahtopup.value,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			if(request.responseText==0){
				alert("Data member TIDAK DITEMUKAN");
			}
			else{
				pilihpaket.style.display="none";
				detailmember.style.display="block";
				ponta.readOnly	= true;
				ponta.style.border="0px";
				eval(request.responseText);
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
}

function konfirmasiProses(){
  document.getElementById('kodetoken').value= "";
  konfirmasiproses.style.display="none";
  prosestoken.style.display="inline-block";

  idmember						= document.getElementById('idmember').innerHTML;
  jumlahtopup					= document.getElementById('jumlahtopup');
  kodereferensi       = document.getElementById('kodereferensi');

  new Ajax.Request("reservasi.member.topup.php?sid={SID}",
    {
      asynchronous: true,
      method: "post",
      parameters: "mode=kirimtoken&idmember="+idmember+"&koderef="+kodereferensi.value+"&jumlahtopup="+jumlahtopup.value,
      onLoading: function(request){
        popuploading.show();
      },
      onComplete: function(request){
      },
      onSuccess: function(request)
      {
        popuploading.hide();
          if(request.responseText == 0){
              alert('Terjadi Kesalahan');
          }
      },
      onFailure: function(request)
      {
        popuploading.hide();
        alert(request.responseText);
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    })

}

function proses(){

	idmember						= document.getElementById('idmember').innerHTML;
  kodereferensi       = document.getElementById('kodereferensi');
	kodetoken						= document.getElementById('kodetoken');

	if(kodetoken.value==""){
		alert("Anda belum memasukkan kode token!");
		exit;
	}

	new Ajax.Request("reservasi.member.topup.php?sid={SID}", 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=topup&idmember="+idmember+"&koderef="+kodereferensi.value+"&kodetoken="+kodetoken.value,
    onLoading: function(request){
			popuploading.show();
		},
    onComplete: function(request){
    },
    onSuccess: function(request) 
    {
			popuploading.hide();

      status="";
      err="";

      eval(request.responseText);

			if(status=="OK"){
				window.location="reservasi.member.topup.php?sid={SID}&mode=cetak&kodereferensi="+kodereferensi.value;
			}
			else{
				switch(err){
          case 1:
            alert("Kode token tidak benar!");
            break;

          case 2:
            alert("Data tidak ditemukan!");
            break;

          case 3:
            alert("Kode Referensi "+kodereferensi.value+" sudah terlebih dahulu diproses!");
            break;


          default:
            alert("Terjadi kegagalan!");
            break;
        }
      }
		},
    onFailure: function(request) 
    {
			popuploading.hide();
			alert(request.responseText);
			alert('Error !!! Cannot Save');        
			assignError(request.responseText);
    }
  })
}

dojo.require("dojo.widget.Dialog");
	
function init(e) {
	// inisialisasi variabel
popuploading				= dojo.widget.byId("popuploading");
		
}
	
dojo.addOnLoad(init);

</script>

<div class="bannerjudul">&nbsp;Day Trans Addict | Top Up</div>
<!-- BEGIN pesan -->
<div style="height: 30px;text-align: center;background:{BGCOLOR_PESAN};font-size:14;padding-top: 10px;color:{COLOR_PESAN};"><b>{PESAN}</b></div>
<!-- END pesan -->
<div id="resvregmemberwrapper">
	<div id="resvregmembercontent">
		<input type="hidden" name="mode" value="{MODE}">
		<input type="hidden" id='submode' name="submode" value="{SUB}">
		<input type="hidden" id="kodereferensi" value="{KODE_REF}">
		
		<span class="resvregmemberlabel"><b>Kode Ref.</b></span><span class="resvregmemberfield"><b>: {KODE_REF}</b></span></br>
		<span class="resvregmemberlabel"><u>Kartu Ponta</u></span><span class="resvregmemberfield">: <input type="text" id="ponta" name="ponta" maxlength=30 onfocus="this.style.background='white';" style="text-transform: uppercase;"></span></br>

		<span id="pilihpaket" style="display: block;">
			<span class="resvregmemberlabel"><u>Jumlah (Rp.)</u></span><span class="resvregmemberfield">: <input type="text" id="jumlahtopup" name="jumlahtopup" onkeypress="validasiAngka(event);" onfocus="this.style.background='white';"/> (Min: Rp. 100.000)</span></br>
			<br>
      Catatan: Harga 1 Poin = Rp.{HARGA_POINT}
      <br><br>
			<span style="padding-left: 120px;"><input type="button" id="btnpreproses" value="&nbsp;&nbsp;&nbsp;PROSES&nbsp;&nbsp;&nbsp;" onclick="getDataMember();" /></span>
    </span>
	
		<span id="detailmember" style="display: none;">
            <span class="resvregmemberlabel">Kode DTA</span><span class="resvregmemberfield">: <span id="idmember"></span></span></br>
			<span class="resvregmemberlabel">Jumlah</span><span class="resvregmemberfield">: <span id="jumlahtopupshow"></span></span></br>
			<span class="resvregmemberlabel">Nama</span><span class="resvregmemberfield">: <span id="nama"></span></span></br>
			<span class="resvregmemberlabel">Handphone</span><span class="resvregmemberfield">: <span id="hp"></span></span></br>
			<span class="resvregmemberlabel">Handphone 2</span><span class="resvregmemberfield">: <span id="hp2"></span></span></br>
			<span class="resvregmemberlabel">Email</span><span class="resvregmemberfield">: <span id="email"></span></span></br>
			<span class="resvregmemberlabel">Jenis Kelamin</span><span class="resvregmemberfield">: <span id="jenis_kelamin"></span></span></br>
			<span class="resvregmemberlabel">Kota</span><span class="resvregmemberfield">: <span id="kota"></span></span></br>
			<span class="resvregmemberlabel">Pekerjaan</span><span class="resvregmemberfield">: <span id="pekerjaan"></span></span></br>
			<span class="resvregmemberlabel">Berlaku Hingga</span><span class="resvregmemberfield">: <span id="tglberlaku"></span></span></br>
      <span class="resvregmemberlabel">Saldo</span><span class="resvregmemberfield">: <span id="saldo"></span> Poin</span></br>
			<span id="konfirmasiproses" style="text-align: center;display: inline-block; width: 400px;">
				<br>
        <font size='1'>anda akan melakukan top up member, silahkan klik proses untuk melanjutkan proses ini...</font>
        <br><br>
        <input type="button" onClick="window.close();" value="&nbsp;&nbsp;&nbsp;BATAL&nbsp;&nbsp;&nbsp;">
				&nbsp;&nbsp;&nbsp;<input type="button" onClick="konfirmasiProses();" value="&nbsp;&nbsp;&nbsp;PROSES&nbsp;&nbsp;&nbsp;">
			</span>
      <span id="prosestoken" style="text-align: center;display: none; width: 400px;">
        <br>
        <font size='1'>masukkan kode token yang dikirimkan sistem kepada pelangan</font>
        <br><br>
        <span class="resvregmemberlabel"><b>Kode Token</b></span><span class="resvregmemberfield">: <input type="text" id="kodetoken" name="kodetoken" onfocus="this.style.background='white';" maxlength="6" onkeypress="validasiAngka(event);"></span></br>
        <br>
        <input type="button" onClick="window.close();" value="&nbsp;&nbsp;&nbsp;BATAL&nbsp;&nbsp;&nbsp;">
				&nbsp;&nbsp;&nbsp;<input type="button" onClick="proses();" value="&nbsp;&nbsp;&nbsp;PROSES&nbsp;&nbsp;&nbsp;">
			</span>
		</span>
		
	</div>
</div>

