<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
	
	function gunakanData(id,nama,telp,keterangan){
		new Ajax.Request("reservasi.waitinglist.php?sid={SID}",{
			asynchronous: true,
			method: "post",
			parameters: "mode=gunakan&id="+id,
			onLoading: function(request){
				popuploading.show();
			},
			onComplete: function(request){
			},
			onSuccess: function(request){
				window.opener.document.getElementById("ftelp").value	= telp;
				window.opener.document.getElementById("fnama").value	= nama;
				window.opener.document.getElementById("fadd").value	= keterangan;
				window.opener.getUpdateMobil();
				window.close();
			},
			onFailure: function(request) 
			{
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		})
	}
	
	function hapusData(id){
		if(!confirm("Apakah anda yakin akan menghapus data ini?")){
			exit;
		}
		
		new Ajax.Request("reservasi.waitinglist.php?sid={SID}",{
			asynchronous: true,
			method: "post",
			parameters: "mode=hapus&id="+id,
			onLoading: function(request){
				popuploading.show();
			},
			onComplete: function(request){
			},
			onSuccess: function(request){
				window.opener.getUpdateMobil();
				location.reload();
			},
			onFailure: function(request) 
			{
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		})
	}
	
	dojo.require("dojo.widget.Dialog");
	
	function init(e) {
		// inisialisasi variabel
		popuploading				= dojo.widget.byId("popuploading");
		
	}
	
	dojo.addOnLoad(init);
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr class="banner" height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Daftar Tunggu {DATA_KEBERANGKATAN}</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
		</table>
		<table>
    <tr>
			 <th width=50>No.</th>
			 <th width=150>Nama</th>
			 <th width=130>Telp</th>
			 <th width=70>Jum.Pesan</th>
			 <th width=150>Keterangan</th>
			 <th width=400>Remark</th>
			 <th width=150>Act</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
			 <td align="center">{ROW.nama}</td>
			 <td align="center">{ROW.telp}</td>
			 <td align="center">{ROW.jumlah_pesan}</td>
			 <td align="center">{ROW.keterangan}</td>
			 <td align="center">{ROW.remark}</td>
       <td align="center">{ROW.act}</td>
     </tr>  
     <!-- END ROW -->
    </table>
 </td>
</tr>
</table>