<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
</script>	 

<body class='tiket'>
<div style="font-size:14px;width:400px;height: 470px;">
	<span style="font-size:22px;">
		REKAP SETORAN<br>
		{NAMA_PERUSAHAAN}<br>
	</span>
	-----------------------------------------<br>
	CSO:{NAMA_CSO}<br>
	Tgl:{TANGGAL_TRANSAKSI}<br>
	-----------------------------------------<br>
	PENJUALAN TIKET<br>
	-----------------------------------------<br>
	<!-- BEGIN TIKET -->
	{TIKET.JURUSAN}
	Jam:{TIKET.JAM}<br>
	{TIKET.KET_SPJ}<br>
	Pnp:{TIKET.LIST_PENUMPANG}<br>
	Tunai:Rp.{TIKET.TUNAI}<br>
	{TIKET.DEBIT}
	{TIKET.KREDIT}
	-----------------------------------------<br>
	<!-- END TIKET -->
	{SHOW_SUB_TOTAL_TIKET}
	-----------------------------------------<br>
	<!-- BEGIN TOPUP -->
	TOP UP MEMBER<br>
	-----------------------------------------<br>
	Jum.Trx: {TOPUP.JUMLAH_TRANSAKSI}<br>
	Total:Rp.{TOPUP.TOTAL_TOPUP}
	-----------------------------------------<br>
	<!-- END TOPUP -->
	<!-- BEGIN PAKET -->
	PENJUALAN PAKET<br>
	-----------------------------------------<br>
	Jum.Trx: {PAKET.JUMLAH_TRANSAKSI}<br>
	Jum.Pax: {PAKET.JUMLAH_PAX}<br>
	Jum.Kilo:{PAKET.JUMLAH_KILO}<br>
	Tunai:Rp.{PAKET.TUNAI}<br>
	{PAKET.DEBIT}
	{PAKET.KREDIT}
	Penj.Paket:Rp.{PAKET.TOTAL_PENJUALAN}
	-----------------------------------------<br>
	<!-- END PAKET -->
	REKAP BIAYA<br>
	-----------------------------------------<br>
	Sopir: Rp.{BIAYA_SOPIR}<br>
	Ins.Sopir: Rp.{BIAYA_INSENTIF_SOPIR}<br>
	Parkir: Rp.{BIAYA_PARKIR}<br>
	Tol: Rp.{BIAYA_TOL}<br>
	Tambahan BBM: Rp.{BIAYA_TAMBAHAN_BBM}<br>
	<br>
	Total Biaya: Rp.{TOTAL_BIAYA}<br>
	-----------------------------------------<br>
	SETORAN TUNAI: Rp.{TOTAL_SETORAN}<br>
	{SHOW_TOTAL_SETORAN}
	Waktu Cetak {WAKTU_CETAK}<br>
	
</div>
</body>

<script language="javascript">
	printWindow();
	window.close();
</script>
</html>
