<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="whiter" valign="middle" align="left">
					<form action="{ACTION_CARI}" method="post">
						<!--HEADER-->
						<table width='100%' cellspacing="0">
							<tr class="banner" height=40>
								<td align='center' valign='middle' class="bannerjudul">&nbsp;Rekap Tiket</td>
								<td align='right' valign='middle' >
									<table>
										<tr><td class='bannernormal'>
											&nbsp;Periode:&nbsp;<input readonly="yes"  id="p0" name="p0" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
											&nbsp; s/d &nbsp;<input readonly="yes"  id="p1" name="p1" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
											<input type="hidden" id="p2" name="p2" value="{PENCARI}" />											
											<input type="hidden" id="p3" name="p3" value="{JENIS_LAPORAN}" />											
										</td></tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan=2 align='center' valign='middle'>
									<table>
										<tr>
											<td>
												<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- END HEADER-->
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				<h2>Perolehan Tiket</h2>
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table>
    <tr>
       <th width=30>No</th>
			 <th width=200>Waktu Cetak</th>
			 <th width=200>No.Tiket</th>
			 <th width=200>Waktu Berangkat</th>
			 <th width=100>Kode Jadwal</th>
			 <th width=200>Nama</th>
			 <th width=100>Telp</th>
			 <th width=50>Kursi</th>
			 <th width=100>Harga Tiket</th>
			 <th width=100>Discount</th>
			 <th width=70>Total</th>
             <th width=70>Bayar</th>
             <th width=100>Tipe Disc.</th>
			 <th width=100>#Manifest</th>
			 <th width=100>CSO</th>
			 <th width=100>Status</th>
			 <th width=100>Ket.</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="center">{ROW.no}</td>
       <td align="center">{ROW.waktu_cetak}</td>
			 <td align="center">{ROW.no_tiket}</td>
       <td align="center">{ROW.waktu_berangkat}</td>
       <td align="center">{ROW.kode_jadwal}</td>
			 <td align="center">{ROW.nama}</td>
			 <td align="center">{ROW.telp}</td>
			 <td align="center">{ROW.no_kursi}</td>
			 <td align="right">{ROW.harga_tiket}</td>
			 <td align="right">{ROW.discount}</td>
			 <td align="right">{ROW.total}</td>
             <td align="right">{ROW.bayar}</td>
			 <td align="center">{ROW.tipe_discount}</td>
			 <td align="center">{ROW.manifest}</td>
			 <td align="center">{ROW.cso}</td>
       <td align="center">{ROW.status}</td>
       <td align="center">{ROW.ket}</td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
<tr><td><hr color='d0d0d0'></td></tr>
<tr>
	<td>
		<table>
		<tr>
				<td align='left' valign='bottom' colspan=3>
				<h2>Perolehan Paket</h2>
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
    <tr>
       <th width=30>No</th>
			 <th width=200>Waktu Cetak</th>
			 <th width=200>#Resi</th>
			 <th width=100>Kode Jadwal</th>
			 <th width=200>Dari</th>
			 <th width=200>Untuk</th>
			 <th width=100>Harga Paket</th>
        <th width=70>Bayar</th>
			 <th width=200>CSO</th>
			 <th width=100>Status</th>
			 <th width=100>Ket.</th>
     </tr>
     <!-- BEGIN ROWPAKET -->
     <tr class="{ROWPAKET.odd}">
       <td align="right">{ROWPAKET.no}</td>
       <td align="center">{ROWPAKET.waktu_cetak}</td>
			 <td align="center">{ROWPAKET.no_tiket}</td>
       <td align="center">{ROWPAKET.kode_jadwal}</td>
			 <td align="center">{ROWPAKET.dari}</td>
			 <td align="center">{ROWPAKET.untuk}</td>
			 <td align="right">{ROWPAKET.harga_paket}</td>
         <td align="center">{ROWPAKET.bayar}</td>
			 <td align="center">{ROWPAKET.cso}</td>
       <td align="center">{ROWPAKET.status}</td>
       <td align="center">{ROWPAKET.ket}</td>
     </tr>  
     <!-- END ROWPAKET -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</td>
</tr>
	<!--
	<tr>
		<td>
			<table>
				<tr>
					<td align='left' valign='bottom' colspan=3>
						<h2>Biaya Tambahan</h2>
					</td>
				</tr>
				<tr>
					<td colspan=3 align='right' valign='bottom'>
						{PAGING}
					</td>
				</tr>
				<tr>
					<th width=30>No</th>
					<th width=200>Cabang</th>
					<th width=200>Nama Releaser</th>
					<th width=100>Nama Pembuat</th>
					<th width=200>Nama Penerima</th>
					<th width=200>Tanggal Buat</th>
                    <th width=200>Tanggal Cetak</th>
					<th width=100>Status</th>
					<th width=200>Jenis Biaya</th>
					<th width=100>Jumlah</th>
					<th width=100>Ket.</th>
				</tr>
				<!-- BEGIN ROWTAMBAHAN -->
				<tr class="{ROWTAMBAHAN.odd}">
					<td align="right">{ROWTAMBAHAN.no}</td>
					<td align="center">{ROWTAMBAHAN.cabang}</td>
					<td align="center">{ROWTAMBAHAN.releaser}</td>
					<td align="center">{ROWTAMBAHAN.pembuat}</td>
					<td align="center">{ROWTAMBAHAN.penerima}</td>
					<td align="center">{ROWTAMBAHAN.tgl_buat}</td>
                    <td align="center">{ROWTAMBAHAN.tgl_cetak}</td>
					<td align="right">{ROWTAMBAHAN.status}</td>
					<td align="center">{ROWTAMBAHAN.jenis_biaya}</td>
					<td align="center">{ROWTAMBAHAN.jumlah}</td>
					<td align="center">{ROWTAMBAHAN.ket}</td>
				</tr>
				<!-- END ROWTAMBAHAN -->
			</table>
			<table width='100%'>
				<tr>
					<td align='right' width='100%'>
						{PAGING}
					</td>
				</tr>
				<tr>
					<td align='left' valign='bottom' colspan=3>
						{SUMMARY}
					</td>
				</tr>
			</table>
		</td>
	</tr>
	-->
</table>