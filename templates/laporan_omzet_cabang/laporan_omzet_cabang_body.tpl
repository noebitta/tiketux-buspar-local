<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Omzet Cabang</td>
				<td align='right' valign='middle'>
					<table>
						<tr><td class='bannernormal'>
							&nbsp;Periode:&nbsp;<select id="filterby" name="filterby"><option value=0 {FILTER_BY0}>Tgl.Berangkat</option><option value=1 {FILTER_BY1}>Tgl.Cetak</option></select>
							&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
							&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
							&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
							<input type="submit" value="cari" />&nbsp;								
						</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='center' valign='bottom' colspan=3>
								<table class="border">
									<tr><td colspan='7' align="center"><b>TOTAL</b></td></tr>
									<tr>
										<td>
											<font style="color: blue;"><b>Book</b>={TOTAL_B}</font>|
											<b>Umum</b>={TOTAL_U}|
											<b>Anak/Lansia</b>={TOTAL_M}|
											<b>Member</b>={TOTAL_K}|
											<b>Free Staff</b>={TOTAL_KK}|
											<b>Gift Voucher</b>={TOTAL_V}|
											<b>Delay</b>={TOTAL_G}|
											<b>Return</b>={TOTAL_R}|
											<b>Vcr.Return</b>={TOTAL_VR}|
											<b>Online</b>={TOTAL_O}|
											<font style="color: green;"><b>TOTAL</b>={TOTAL_T}</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30 rowspan=2 >No</th>
			 <th width=400 rowspan=2><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Cabang</a></th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Cabang</a></th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Alamat</a></th>
			 <th width=100 rowspan=2><a class="th">Open Trip</a></th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Trip</a></th>
			 <th colspan=11 class='thin'>Total Penumpang</th>
			 <th width=100 rowspan=2><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Pnp<br>/Trip</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Tot.Omz.Pnp (Rp.)</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>Tot.Pkt</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_14}' title='{TIPS_SORT_14}'>Tot.Omz.Pkt</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_15}' title='{TIPS_SORT_15}'>Tot.Disc</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_17}' title='{TIPS_SORT_17}'>Tot.Biaya Langsung</a></th>
			<th width=200 rowspan=2><a class="th" href='{A_SORT_16}' title='{TIPS_SORT_16}'>Tot.Biaya Tambahan</a></th>
			 <th width=200 rowspan=2><a class="th" href='{A_SORT_18}' title='{TIPS_SORT_18}'>Tot.Laba Kotor</a></th>
			 <th width=100 rowspan=2>Action</a></th>
     </tr>
		 <tr>
			<th class='thin'><a class="th" href='{A_SORT_19}' title='{TIPS_SORT_19}'>B</a></th>
			<th class='thin'><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>U</a></th>
			<th class='thin'><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>A</a></th>
			<th class='thin'><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>M</a></th>
			<th class='thin'><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>F</a></th>
			<th class='thin'><a class="th" href='{A_SORT_19}' title='{TIPS_SORT_19}'>V</a></th>
			<th class='thin'><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>D</a></th>
			<th class='thin'><a class="th" href='{A_SORT_20}' title='{TIPS_SORT_20}'>R</a></th>
			<th class='thin'><a class="th" href='{A_SORT_21}' title='{TIPS_SORT_21}'>VR</a></th>
			<th class='thin'><a class="th" href='{A_SORT_22}' title='{TIPS_SORT_22}'>O</a></th>
			<th class='thin'><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>T</a></th>
		 </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="left">{ROW.cabang}</div></td>
			 <td ><div align="left">{ROW.kode_cabang}</div></td>
			 <td ><div align="left">{ROW.alamat}</div></td>
			 <td ><div align="right">{ROW.open_trip}</div></td>
			 <td ><div align="right">{ROW.total_keberangkatan}</div></td>
			 <td width=60 style="background: blue;color: white;"><div align="right" >{ROW.total_penumpang_b}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_u}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_m}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_k}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_kk}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_v}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_g}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_r}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_vr}</div></td>
			 <td width=60><div align="right" >{ROW.total_penumpang_o}</div></td>			 
			 <td width=60 style="background: yellow"><div align="right" >{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.rata_pnp_per_trip}</div></td>
			 <td ><div align="right">{ROW.total_omzet}</div></td>
			 <td ><div align="right">{ROW.total_paket}</div></td>
			 <td ><div align="right">{ROW.total_omzet_paket}</div></td>
			 <td ><div align="right">{ROW.total_discount}</div></td>
			 <td ><div align="right">{ROW.total_biaya}</div></td>
		     <td ><div align="right">{ROW.total_tambahan}</div></td>
			 <td ><div align="right">{ROW.total_profit}</div></td>
       <td ><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align="center">
					<table class="border">
						<tr><td colspan='7' align="center"><b>TOTAL</b></td></tr>
						<tr>
							<td>
								<font style="color: blue;"><b>Book</b>={TOTAL_B}</font>|
									<b>Umum</b>={TOTAL_U}|
									<b>Anak/Lansia</b>={TOTAL_M}|
									<b>Member</b>={TOTAL_K}|
									<b>Free Staff</b>={TOTAL_KK}|
									<b>Gift Voucher</b>={TOTAL_V}|
									<b>Delay</b>={TOTAL_G}|
									<b>Return</b>={TOTAL_R}|
									<b>Vcr.Return</b>={TOTAL_VR}|
									<b>Online</b>={TOTAL_O}|
								<font style="color: green;"><b>TOTAL</b>={TOTAL_T}</font>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
					<table>
						<tr><td>B</td><td>=</td><td>Penumpang Booking</td></tr>
						<tr><td>U</td><td>=</td><td>Penumpang Umum</td></tr>
						<tr><td>A</td><td>=</td><td>Anak/Lansia</td></tr>
						<tr><td>M</td><td>=</td><td>Member</td></tr>
						<tr><td>F</td><td>=</td><td>Special Ticket Staff</td></tr>
						<tr><td>V</td><td>=</td><td>Gift Voucher</td></tr>
						<tr><td>D</td><td>=</td><td>Delay (upto 2 hours)</td></tr>
						<tr><td>R</td><td>=</td><td>Tiket Return</td></tr>
						<tr><td>VR</td><td>=</td><td>Voucher Return</td></tr>
						<tr><td>O</td><td>=</td><td>Online</td></tr>
						<tr><td>T</td><td>=</td><td>Total Penumpang</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>