<script language="JavaScript">

    function validateInput(){

        valid=true;

        Element.hide('jenis_invalid');
        Element.hide('coa_invalid');

        coa			= document.getElementById('coa');
        jenis			= document.getElementById('jenis');

        if(coa.value==''){
            valid=false;
            Element.show('coa_invalid');
        }

        if(jenis.value==''){
            valid=false;
            Element.show('jenis_invalid');
        }

        if(valid){
            return true;
        }
        else{
            return false;
        }
    }

</script>

<form action="{URL}" method="post" onSubmit='return validateInput();'>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr class='banner' height=40>
            <td align='center' valign='middle' class="bannerjudul">&nbsp;MASTER BIAYA COA</td>
        </tr>
        <tr>
            <td class="whiter" valign="middle" align="center">
                <br><br><br>
                <table width='800'>
                    <tr>
                        <td align='center' valign='top' width='800'>
                            <table width='400'>
                                <tr>
                                    <td width='200'><u>Biaya</u></td><td width='5'>:</td>
                                    <td>
                                        <select name="biaya" id="biaya">
                                            <option value="1" {JENIS1}>Biaya Tambahan</option>
                                            <option value="2" {JENIS2}>Petty Cash</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><u>Jenis Biaya</u></td><td>:</td>
                                    <td>
                                        <input type="text" name="jenis" id="jenis" onchange="Element.hide('jenis_invalid');" value="{BIAYA}">
                                    </td>
                                    <td>
                                        <span id='jenis_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign='top'>Kode COA</td><td  valign='top'>:</td>
                                    <td>
                                        <input type="text" id="coa" name="coa" onchange="Element.hide('coa_invalid');" value="{COA}">
                                    </td>
                                    <td>
                                        <span id='coa_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3 align='center' valign='middle' height=40>
                            <input type="hidden" name="mode" value="{MODE}">
                            <input type="hidden" name="id" value="{ID}">
                            <input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>