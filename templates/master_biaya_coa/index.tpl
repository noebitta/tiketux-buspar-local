<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    function init(e){
        //control dialog
        dialog_biaya			= dojo.widget.byId("dialogbiayatambahan");
        dlg_biaya_cancel        = document.getElementById("dialogcancel");
    }
    dojo.addOnLoad(init);
    function ShowDialog(){
        dialog_biaya.show();
    }
    function InsertBiaya(){
        if(coa.value == ""){
            alert("Kode COA Tidak Boleh Kosong");
        }else if(jenis.value == ""){
            alert("Jenis Biaya Tidak Boleh Kosong");
        }else{
            new Ajax.Request("master_biaya_coa.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=InsertData"+
                "&jenis="+jenis.value+
                "&coa="+coa.value,
                onLoading: function(request){
                    progressbar.show();
                },
                onComplete: function(request){
                    progressbar.hide();
                    dialog_biaya.hide();
                    console.log("oncomplete");
                },
                onSuccess: function(request){
                    eval(request.responseText);

                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }
    }
</script>
<!--dialog biaya tambahan-->
<div dojoType="dialog" id="dialogbiayatambahan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr><td colspan='2' align="center"><h2>Kode Biaya </h2></td></tr>
                        <input type="hidden" name="mode" value="InsertData"><tr><td align="right">Jenis : </td><td>
                                <select id="jenis" name="jenis">
                                    <option value="">--Pilih Jenis--</option>
                                    <option value="0">JASA</option>
                                    <option value="1">TOL</option>
                                    <option value="2">SUPIR</option>
                                    <option value="3">BBM</option>
                                    <option value="4">PARKIR</option>
                                    <option value="5">SUPIR KUMULATIF</option>
                                    <option value="6">VOUCHER BBM</option>
                                </select>
                            </td>
                        </tr>
                        <tr><td align="right">COA : </td><td><input type="text" name="coa" id="coa"></td></tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogcancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_biaya.hide();">
                    <input type="button" onclick="InsertBiaya()"  id="dialogbiayatambahanproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog biaya tambahan-->

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr height=40 class='banner'>
                        <td align='center' valign='middle' class="bannerjudul"></td>
                        <td class="bannerjudul">&nbsp;MASTER BIAYA COA</td>
                    </tr>
                    <tr>
                        <td colspan=2 align='left' valign='middle'>
                            <table>
                                <tr>
                                    <td >
                                        <a href="{ADD}"><font size="3"><u>Tambah</u></font> </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- END HEADER-->
            </form>
        </td>
        <!-- BODY -->
        <table class="border" width="100%">
            <thead>
            <tr>
                <th width="100">BIAYA</th>
                <th width="150">JENIS BIAYA</th>
                <th width="100">KODE COA</th>
                <th width="50">ACTION</th>
            </tr>
            </thead>
            <tbody>
            <!-- BEGIN ROW -->
            <tr class="even">
                <td><font size="3">{ROW.FlagJenis}</font> </td>
                <td><font size="3">{ROW.JenisBiaya}</font></td>
                <td><font size="3">{ROW.KodeCOA}</font></td>
                <td align="center">
                    <a href="{ROW.EDIT}"><u>Edit</u></a>
                </td>
            </tr>
            <!-- END ROW -->
            </tbody>
        </table>
        <!--END BODY-->
    </tr>
</table>