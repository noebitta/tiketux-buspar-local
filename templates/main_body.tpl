<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="icon" type="image/ico" href="favicon.ico">
    {META}
    <title>{SITENAME} :: Login</title>
    <link rel="stylesheet" href="./templates/trav.css" type="text/css" />
  </head>
  <script type="text/javascript" src="./templates/js/fingerprint.js"></script>
  <body id="loginbody">
    <div align="center">
      <div id="loginwrapper">
        <form class="loginform" name="formlogin" action="auth.php" method="post">
          <input type="hidden" id="hostuuid" name="hostuuid" value=""/>
          <input type="hidden" id="mode" name="mode" value="0"/>
          <p class="logintitle">
            <span><img src="./templates/images/login/logomitra.png" style="max-width: 200px;"/></span><br>
            <img src="./templates/images/login/logotiketux.png" style="max-width: 100px;"/>
          </p>
          <span style="height:60px;width:100%;display: inline-block"><input id="username" name="username" size="30" value=""  placeholder='Username'  autofocus="" type="text"/></span><br>
          <span style="height:60px;width:100%;display: inline-block"><input id="password" name="password" size="30" value=""  placeholder='Password'  autofocus="" type="password"/></span><br>
          <input id="loginbutton" name="loginbutton" class="flatbutton" value="LOG IN" type="submit"/>
        </form>
      </div>
    </div>
  </body>
  <script>
    var fp = new Fingerprint2();
    fp.get(function(result, components) {document.getElementById("hostuuid").value = result;});
  </script>
</html>
