<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}

  function getUpdateAsal(kota){

	  new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
		  asynchronous: true,
		  method: "get",

		  parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
		  onLoading: function(request){
		  },
		  onComplete: function(request){
			  Element.show('rewrite_asal');
		  },
		  onFailure: function(request){
			  assignError(request.responseText);
		  }
	  });
  }
  getUpdateAsal("{KOTA}");
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td valign='middle' class="bannerjudul">&nbsp;Export Untuk GP</td>
		</tr>
		<tr>
			<td colspan=2 align='center' valign='middle'>
				<br>
				<table>
					<tr>
						<td>Cabang</td>
						<td><div id='rewrite_asal'></div></td>
					</tr>
					<tr>
						<td>Periode</td>
						<td><input readonly="yes"  id="awal" name="awal" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{AWAL}" size=10> - <input readonly="yes"  id="akhir" name="akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{AKHIR}" size=10>
						&nbsp;
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><input type="button" value="Export Data Penjualan" onClick="{ACTION_EXPORT_PENJUALAN}"/></td>
						<td><input type="button" value="Export Data Biaya" onClick="{ACTION_EXPORT_BIAYA}"/></td>
					</tr>

				</table>
			</td>
		</tr>
	</table>
 </td>
</tr>
</table>