<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
  <script language="javaScript" type="text/javascript" src="includes/calendar.js"></script>
  <link href="includes/calendar.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25961668-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

</script>
</head>  
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/lib/prototype.js"></script>
<script type='text/javascript' src='{ROOT}js/prototype.js'></script>
<script type='text/javascript' src='{ROOT}js/scriptaculous.js?load=effects'></script>
<script type='text/javascript' src='{ROOT}js/prototip.js'></script>
<link rel="stylesheet" type="text/css" href="{ROOT}css/prototip.css" />

<script type="text/javascript" src="{ROOT}ajax/dojo.js"></script>

<script type="text/javascript" src="{TPL}js/main.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script> 

<body>
	
<!--BEGIN popuploading -->
<div dojoType="dialog" id="popuploading" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="300" cellpadding="0" cellspacing="0" >
	<tr><td align="center" valign="middle" style="padding-top: 10px;">
		<font style="color: white;font-size: 12px;">sedang mengerjakan</font><br>
		<img src="{TPL}/images/loading_bar.gif" />
	</td></tr>
</table>
<br>
</div>
<!--END popuploading-->
<!-- BEGIN if_login -->
<div align='center'>
<table width="100%" cellpadding=0 cellspacing=0>
<tr><td>
<table width='100%' cellpadding=0 cellspacing=0>
<tr>
	<!--<td class="banner" background='./templates/images/bg_tombol_header.png' STYLE='background-repeat: no-repeat;background-position: right bottom;'>-->
	<td>
		<table width='100%' cellpadding=0 cellspacing=0>
			<tr>
				<td height=40 valign='bottom' class="bannercari">
					&nbsp;<img src="{TPL}images/logo_header.gif" />
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="header_shadow">&nbsp;</td>
</tr>
</table>
<!-- END if_login -->
<input id='hdn_SID' name='hdn_SID' type='hidden' value='{SID}' />