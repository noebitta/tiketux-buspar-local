<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    document.getElementById('{CABANG}').selected = true;
    var cabang = document.getElementById('cabangasal');
    cabang.value = '{CABANG}';
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }
</script>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penerimaan Pengeluaran Counter</td>
                    <td class="bannerjudul">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <a href='#' onClick="{EXCEL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                    </td>
                    <td align="right">
                        <font size="3"><b>MODAL : Rp. {MODAL}</b></font>
                    </td>
                </tr>
            </table>
            <!--END HEADER-->
            <!--BODY-->
            <table  class="border" width='100%'>
                <tr>
                    <td rowspan="3" valign="middle" align="center" bgcolor="#708090" ><font size=3 color="white"><b>No</b></font></td>
                    <td rowspan="3" valign="middle" bgcolor="#708090" align="center"><font size=3 color="white"><b>Keterangan</b></font></td>
                    <td colspan="3" align="center" bgcolor="#708090"><font size=3 color="white"><b>Penjualan</b></font></td>
                    <td colspan="2" align="center" bgcolor="#708090"><font size=3 color="white"><b>Pengeluaran</b></font></td>
                <tr>
                <tr>
                    <td align="center" bgcolor="#708090"><font color="white"><b>Cash</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>EDC</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>SYSTEM</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>LPOC</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>OTHER</b></font></td>
                </tr>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td align="center"><font size=2><b>{ROW.no}</b></font></td>
                    <td><font size=2>{ROW.NoSPJ} / {ROW.KodeJurusan} {ROW.JamBerangkat} - {ROW.NoPolisi} / {ROW.Sopir}</font></td>
                    <td align="right">{ROW.total_cash}</td>
                    <td align="right">{ROW.total_edc}</td>
                    <td align="right">{ROW.total_sistem}</td>
                    <td align="right"><font size=2>{ROW.total_lpoc}</font></td>
                    <td align="right"><font size=2></font></td>
                </tr>
                <!-- END ROW -->
                <!-- BEGIN ROWBIAYA -->
                <tr class="{ROWBIAYA.odd}">
                    <td align="center"><font size=2><b>{ROWBIAYA.no}</b></font></td>
                    <td><font size=2>{ROWBIAYA.Penerima} / {ROWBIAYA.Jurusan} / {ROWBIAYA.Keterangan}</font></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"><font size=2></font></td>
                    <td align="right"><font size=2>{ROWBIAYA.Jumlah}</font></td>
                </tr>
                <!-- END ROWBIAYA -->
                <tr  bgcolor='ffff00'>
                    <td colspan="2" align="left"><font size=3><b>&nbsp;&nbsp;&nbsp;Total</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_CASH}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_EDC}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_SISTEM}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_LPOC}</b></font></td>
                    <td align="right"><font size=3><b>{BLAIN}</b></font></td>
                </tr>
            </table>
            <!--END BODY-->
            <table align="right" width="250">
                <tr>
                    <td>
                        <input type="hidden" name="setoran" id="setoran" value="{SETORAN}">
                        <p align="right"><font size="3"><b>SETORAN : Rp. {SETORAN}</b></font></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <Td>
                        <input type="hidden" value="{SALDO}" id="saldo" name="saldo">
                        <p align="right"><font size="3"><b>SALDO : Rp. {SALDO}</b></font></p>
                    </Td>
                </tr>
            </table>
        </td>
    </tr>
</table>