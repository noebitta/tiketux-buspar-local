<script language="JavaScript">
// global

function ValidasiAngka(objek,kolom){
	temp_nilai=objek.value*0;
	
	if(temp_nilai!=0){
		alert(kolom+" harus angka!");
		objek.setFocus;exit;
	}
	
}

function selectAll(){

	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			chk.checked=true;
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);

}

function deselectAll(){

	i=1;
	loop=true;
	record_dipilih="";
	do{
		str_var='checked_'+i;
		if(chk=document.getElementById(str_var)){
			chk.checked=false;
		}
		else{
			loop=false;
		}
		i++;
	}while(loop);

}

function hapusData(kode){

	if(confirm("Apakah anda yakin akan menghapus data ini?")){

		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}

		new Ajax.Request("jenis_discount.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=hapus&list_discount="+list_dipilih,
			onLoading: function(request)
			{
			},
			onComplete: function(request)
			{

			},
			onSuccess: function(request)
			{

				if(request.responseText==1){
					window.location.reload();
					deselectAll();
				}
				else if(request.responseText==0){
					alert("Anda tidak memiliki akses untuk menghapus data ini!");
				}
				else{
					alert("Terjadi Kegagalan");
				}
			},
			onFailure: function(request)
			{
			}
		})
	}

	return false;

}

function ubahStatus(id_discount){
  //ubah account
	
	new Ajax.Request("jenis_discount.php?sid={SID}", 
		{
	    asynchronous: true,
	    method: "get",
	    parameters: "mode=ubah_status"+"&id_discount="+id_discount,
	    onLoading: function(request) 
	    {
	    },
	    onComplete: function(request) 
	    {

	    },
	    onSuccess: function(request) 
	    {
			if(request.responseText == 2){
				alert("Anda tidak memiliki akses untuk mengubah data ini!");exit;
			}
		},
	    onFailure: function(request) 
	    {     
	       assignError(request.responseText);
	    }
	  });
}


</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td class="whiter" valign="middle" align="center">
			<table width='100%' cellspacing="0">
				<tr class='banner' height=40>
					<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Promo</td>
					<td colspan=2 align='right' class="bannernormal" valign='middle'>
						<br>
						<form action="{ACTION_CARI}" method="post">
							Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
						</form>
					</td>
				</tr>
				<tr>
					<td width='30%' align='left'>
						<a href="{U_ADD}">[+]Tambah Diskon</a>&nbsp;|&nbsp;
						<a href="" onClick="return hapusData('');">[-]Hapus Diskon</a></td>
					<td width='70%' align='right'>
						<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
						<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
					</td>
				</tr>
			</table>
			<table width='100%' class="border">
				<tr>
					<th width=30></th>
					<th width=30>No</th>
					<th width=200>Kode Diskon</th>
					<th width=200>Nama Diskon</th>
					<th width=100>Besar Diskon</th>
					<th width=100>Jenis</th>
					<th width=100>Aktif</th>
					<th width=150>Action</th>
				</tr>
				<!-- BEGIN ROW -->
				<tr class="{ROW.odd}">
					<td><div align="center">{ROW.check}</div></td>
					<td><div align="center">{ROW.no}</div></td>
					<td><div align="left">{ROW.kode_diskon}</div></td>
					<td><div align="left">{ROW.nama_diskon}</div></td>
					<td><div align="center">{ROW.besar_diskon}</div></td>
					<td><div align="center">{ROW.jenis}</div></td>
					<td><div align="center">{ROW.aktif}</div></td>
					<td><div align="center">{ROW.action}</div></td>
				</tr>
				<!-- END ROW -->
				{NO_DATA}
			</table>
			<table width='100%'>
				<tr>
					<td width='30%' align='left'>
						<a href="{U_ADD}">[+]Tambah Diskon</a>&nbsp;|&nbsp;
						<a href="" onClick="return hapusData('');">[-]Hapus Diskon</a></td>
					<td width='70%' align='right'>
						<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
						<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
