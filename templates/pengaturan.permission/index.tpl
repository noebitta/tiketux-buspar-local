<script type="text/javascript">

  function selDeselCheckbox(checked,idgroup){
    chk_groups = document.getElementsByName("checked_"+idgroup);

    len_groups = chk_groups.length;

    for(i=0;i<len_groups;i++){
      chk_groups[i].checked=checked;

      chk_groups[i].disabled = (!checked && chk_groups[i].value!=idgroup)?true:false;

      chk_menus = document.getElementsByName("checked_"+chk_groups[i].value);

      len_menus = chk_menus.length;

      for(j=0;j<len_menus;j++){
        chk_menus[j].checked=checked;
      }

    }

  }

  function simpan(){

    showLoading();

    var i=1;

    var listpage = [];

    while(chkpage= document.getElementById("checked_"+i)){

      if(chkpage.checked) {
        listpage.push(chkpage.value);
      }

      i++;
    }

    var jsonlistpage  = JSON.stringify(listpage);

    new Ajax.Request("pengaturan.permission.php", {
        asynchronous: true,
        method: "post",
        parameters: "mode=1&id="+id.value+"&listpage="+jsonlistpage,
        onLoading: function(request){

        },
        onComplete: function(request){

        },
        onSuccess: function(request){

          var result;

          result = JSON.parse(request.responseText);

          if(result["status"]=="OK"){
            formdata.submit();
          }
          else{
            alert("Gagal:"+result["error"]);
            popupwrapper.hide();
          }
          document.getElementById("formdata").submit();
        },
        onFailure: function(request){
          alert('Error');
          assignError(request.responseText);
        }
      });
  }

  function filterPermission(idrole){
    id.value  = idrole;
    formdata.submit();
  }

  function init(e){
    popupwrapper	= dojo.widget.byId("popupcontainer");
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="{ID}">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER_BY}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Permission</div>
    <div class="headerfilter" style="padding-top: 7px;">

    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <span class="flatbutton" onclick="simpan();">Simpan</span>
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
  <br>
</form>

<br>
<hr>
<div class="tabletitle">
  Permission untuk Role:
  <select id="role" name="role" onchange="filterPermission(this.value);">
    <!-- BEGIN OPT_ROLE -->
    <option value="{OPT_ROLE.id}" {OPT_ROLE.selected}>{OPT_ROLE.nama}</option>
    <!-- END OPT_ROLE -->
  </select>
  <br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th></th>
      <th>Group Menu</th>
      <th>Menu</th>
      <th>Submenu</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center">
          <input type="checkbox" id="checked_{ROW.no}" name="checked_{ROW.idgroup}" value="{ROW.id}" {ROW.checked} onclick="{ROW.onclick}"/>
        </td>
        <td align="center"><b>{ROW.groupmenu}</b></td>
        <td align="center">{ROW.menu}</td>
        <td align="center">{ROW.submenu}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>