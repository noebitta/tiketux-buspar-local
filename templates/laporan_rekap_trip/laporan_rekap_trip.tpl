<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }

    function setData(bulan){
        tahun	=document.getElementById('tahun').value;

        window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
    }

</script>

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Trip</td>
                    <td class="bannerjudul">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <table width='100%'>
                            <tr>
                                <td colspan="2" align='right' valign='bottom'>
                                    <form action="{ACTION_CARI}" method="get">
                                        {LIST_BULAN}
                                        &nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div align="center">
                   <b>
                       <font size=5 color='008609'>
                           TRIP DT JAKARTA-BANDUNG
                       </font>
                </b>
            </div>
        </td>
    </tr>
    <tr>
        <table width="100%" class="border">
            <tbody>
            <tr>
                <th>Hari</th>
                <th>Tanggal</th>
                <!-- BEGIN Cabang -->
                <th>{Cabang.nama}</th>
                <!-- END Cabang -->
                <th>Total</th>
            </tr>
            <!-- BEGIN ROW -->
            <tr class="{ROW.odd}">
                <td>{ROW.hari}</td>
                <td>{ROW.tanggal}</td>
                {ROW.trip_jakarta_dt}
                <td align="center">{ROW.sum_trip_jakarta_dt}</td>
            </tr>
            <!-- END ROW -->
            <tr>
                <th colspan="2">GRAND TOTAL</th>
                <!-- BEGIN Cabang -->
                <th><div align="center">{Cabang.sum_jkt_dt}</div></th>
                <!-- END Cabang -->
                <th><div align="center">{GT_JAKARTA_DT}</div></th>
            </tr>
            </tbody>
        </table>
    </tr>
    <tr>
        <td>
            <div align="center">
                <b>
                    <font size=5 color='008609'>
                        TRIP DT BANDUNG-JAKARTA
                    </font>
                </b>
            </div>
        </td>
    </tr>
    <tr>
        <table width="100%" class="border">
            <tbody>
            <tr>
                <th>Hari</th>
                <th>Tanggal</th>
                <!-- BEGIN Bandung -->
                <th>{Bandung.nama}</th>
                <!-- END Bandung -->
                <th>Total</th>
            </tr>
            <!-- BEGIN ROW -->
            <tr class="{ROW.odd}">
                <td>{ROW.hari}</td>
                <td>{ROW.tanggal}</td>
                {ROW.trip_bandung_dt}
                <td align="center">{ROW.sum_trip_bandung_dt}</td>
            </tr>
            <!-- END ROW -->
            <tr>
                <th colspan="2">GRAND TOTAL</th>
                <!-- BEGIN Bandung -->
                <th><div align="center">{Bandung.sum_bdg_dt}</div></th>
                <!-- END Bandung -->
                <th><div align="center">{GT_BANDUNG_DT}</div></th>
            </tr>
            </tbody>
        </table>
    </tr>
    <!-- WH -->
    <tr>
        <td>
            <br><br><br>
            <div align="center">
                <b>
                    <font size=5 color='008609'>
                        TRIP WH JAKARTA-BANDUNG
                    </font>
                </b>
            </div>
        </td>
    </tr>
    <tr>
        <table width="100%" class="border">
            <tbody>
            <tr>
                <th>Hari</th>
                <th>Tanggal</th>
                <!-- BEGIN Cabang -->
                <th>{Cabang.nama}</th>
                <!-- END Cabang -->
                <th>Total</th>
            </tr>
            <!-- BEGIN ROW -->
            <tr class="{ROW.odd}">
                <td>{ROW.hari}</td>
                <td>{ROW.tanggal}</td>
                {ROW.trip_jakarta_wh}
                <td align="center">{ROW.sum_trip_jakarta_wh}</td>
            </tr>
            <!-- END ROW -->
            <tr>
                <th colspan="2">GRAND TOTAL</th>
                <!-- BEGIN Cabang -->
                <th><div align="center">{Cabang.sum_jkt_wh}</div></th>
                <!-- END Cabang -->
                <th><div align="center">{GT_JAKARTA_WH}</div></th>
            </tr>
            </tbody>
        </table>
    </tr>
    <tr>
        <td>
            <br><br>
            <div align="center">
                <b>
                    <font size=5 color='008609'>
                        TRIP WH BANDUNG-JAKARTA
                    </font>
                </b>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" class="border">
                <tbody>
                <tr>
                    <th>Hari</th>
                    <th>Tanggal</th>
                    <!-- BEGIN Bandung -->
                    <th>{Bandung.nama}</th>
                    <!-- END Bandung -->
                    <th>Total</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td>{ROW.hari}</td>
                    <td>{ROW.tanggal}</td>
                    {ROW.trip_bandung_wh}
                    <td align="center">{ROW.sum_trip_bandung_wh}</td>
                </tr>
                <!-- END ROW -->
                <tr>
                    <th colspan="2">GRAND TOTAL</th>
                    <!-- BEGIN Bandung -->
                    <th><div align="center">{Bandung.sum_bdg_wh}</div></th>
                    <!-- END Bandung -->
                    <th><div align="center">{GT_BANDUNG_WH}</div></th>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>