<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    function init(e){
        //control dialog
        dialog_biaya			= dojo.widget.byId("dialogbiayatambahan");
        dlg_biaya_cancel        = document.getElementById("dialogcancel");
        dialog_cetak            = dojo.widget.byId('dialogcetakulang');
        dlg_cetak_batal         = document.getElementById('dialogbatal');
    }
    dojo.addOnLoad(init);
    function ShowDialog(){
        window.open("biaya_tambahan.php?mode=add&sid={SID}",'_self');
    }

    function ShowDialogCetak(otp,id){
        document.getElementById("idbiaya").value = id;
        document.getElementById("kode").value = otp;
        dialog_cetak.show();
    }

    function Release(KodeBiaya){
        tanggal_mulai = document.getElementById('tanggal_mulai').value;
        tanggal_akhir = document.getElementById('tanggal_akhir').value;


        new Ajax.Request("biaya_tambahan.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=release&KodeBiaya="+KodeBiaya,
            onLoading: function(request)
            {
            },
            onComplete: function(request)
            {

            },
            onSuccess: function(request)
            {
                window.open("biaya_tambahan.php?sid={SID}&tanggal_mulai="+tanggal_mulai+"&tanggal_akhir="+tanggal_akhir,'_self');
            },
            onFailure: function(request)
            {
            }
        });

    }

    function CetakUlang(){
        KODEOTP = document.getElementById("kode").value;
        INPUTOTP= document.getElementById("otp").value;
        ID      = document.getElementById("idbiaya").value;
        if(KODEOTP == ""){
            alert("Kode OTP tidak Boleh Kosong");
        }else if(KODEOTP != INPUTOTP){
            alert("KODE OTP TIDAK COCOK !!!");
        }else{
            StartCustomeSize('biaya_tambahan_cetak.php?sid={SID}&id='+ID,'600','650');
            dialog_cetak.hide();
            document.getElementById("otp").value = "";
            location.reload();
        }
    }

    function refresh_page() {
        window.location = '{URL}'+'&tanggal_mulai='+'{TGL_AWAL}'+'&tanggal_akhir='+'{TGL_AKHIR}'+'&txt_cari='+'{TXT_CARI}';
    }

</script>

<!--dialog cetak ulang-->
<div dojoType="dialog" id="dialogcetakulang" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500">
    <form onsubmit="false" id="FormCetakUlang">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr><td colspan='2' align="center"><h2>Cetak Ulang Struk Biaya Tambahan</h2></td></tr>
                        <tr><td align="right">Kode OTP : </td><td><input name="otp" id="otp" type="text"></td></tr>
                        <input type="hidden" name="kode" id="kode">
                        <input type="hidden" name="idbiaya" id="idbiaya">
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogbatal" value="&nbsp;Cancel&nbsp;" onClick="dialog_cetak.hide();">
                    <input type="button" onclick="CetakUlang()"  id="dialogcetakulangproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog cetak ulang-->
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr height=40 class='banner'>
                        <td align='center' valign='middle' class="bannerjudul">BIAYA TAMBAHAN</td>
                        <td align='right' valign='middle' >
                            <table>
                                <tr>
                                    <td class='bannernormal'>
                                        <!-- BEGIN show_list_cabang -->
                                         Cabang : <select id='cabangasal' name='cabang' >{LIST_CABANG}</select>
                                        <!-- END show_list_cabang -->
                                        &nbsp;Periode :&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                                        &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                                        &nbsp;Cari :&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                                        <input type="submit" value="cari" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align='left' valign='middle' width="50%">
                                        <table>
                                            <tr>
                                                <td >
                                                    <input class="tombol" value="Tambah Biaya" id="tambah_biaya" type="button" onclick="ShowDialog()">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td colspan=2 align='left' valign='middle' width="50%">
                                        <table>
                                            <tr>
                                                <td>
                                                    <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- END HEADER-->
            </form>
        </td>
                <!-- BODY -->
        <table class="border" width="100%">
            <thead>
            <tr>
                <th width="30">No</th>
                <th width="100">CABANG</th>
                <th width="150">PEMBUAT</th>
                <th width="150">TANGGAL BUAT</th>
                <th width="100">NO POLISI</th>
                <th width="100">JURUSAN</th>
                <th width="150">JENIS BIAYA</th>
                <th width="100">JUMLAH</th>
                <th width="100">REALISASI 1</th>
                <th width="100">REALISASI 2</th>
                <th width="100">SELISIH</th>
                <th width="150">PENERIMA</th>
                <th width="150">KETERANGAN</th>
                <th width="150">TANGGAL CETAK</th>
                <th width="250">CETAK</th>
                <th width="150">PENCETAK</th>
                {KOLOM_OTP}
                {KOLOM_REALISASI}
                {KOLOM_ACTION}
            </tr>
            </thead>
           <tbody>
           <!-- BEGIN ROW -->
           <tr class="even">
               <td>{ROW.No}</td>
               <td >{ROW.KodeCabang}</td>
               <td>{ROW.Pembuat}</td>
               <td><div align="center">{ROW.TglBuat}</div></td>
               <td><div>{ROW.NoPolisi}</div></td>
               <td><div>{ROW.Jurusan}</div></td>
               <td><table>{ROW.JenisBiaya}</table></td>
               <td><table>{ROW.Jml}</table></td>
               <td><table>{ROW.Realisasi1}</table></td>
               <td><table>{ROW.Realisasi2}</table></td>
               <td><table>{ROW.Selisih}</table></td>
               <td><div>{ROW.Penerima}</div></td>
               <td><div>{ROW.Keterangan}</div></td>
               <td><div align="center">{ROW.TglCetak}</div></td>
               <td>
                   <div align="center">
                       <a href="#" onclick="{ROW.Onclik}">
                           <img src="{TPL}/images/b_print.png">
                       </a>
                   </div>
               </td>
               <td>{ROW.Pencetak}</td>
               {ROW.OTP}
               {ROW.REALISASI}
               {ROW.ACTION}
           </tr>
           <!-- END ROW -->
           </tbody>
        </table>
        <!--END BODY-->
    </tr>
</table>