<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Berita Acara Checker</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<!--<td class='bannernormal'><select onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select></td>
							<td class='bannernormal'>Asal:&nbsp;</td><td><div id='rewrite_asal'></div></td>
							<td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div></td>-->
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       			 <th width=30>No</th>
			 <th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jadwal</a></th>
			 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jurusan</a></th>
			 <th width=100><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Tanggal BA</a></th>
			 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>#Nomor BA</a></th>
			 <th width=200><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>Sopir</a></th>
			 <th width=70><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>#Body</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Checker</a></th>
			 <th width=100>Jml Liter</th>
			 <th width=70><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Pnp.Chk</a></th>
			 <th width=70><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Pkt.Chk</a></th>
			 <th width=100><a class="th">Catatan</a></th>
             <th width=100>Foto</th>
</tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       			 <td align="right">{ROW.no}</td>
       			 <td align="center">{ROW.jadwal}</td>
			 <td align="center">{ROW.kodejurusan}</td>
       			 <td align="center">{ROW.tgl_ba}</td>
			 <td align="center">{ROW.noba}</td>
			 <td align="center">{ROW.sopir}</td>
			 <td align="center">{ROW.mobil}</td>
			 <td align="center">{ROW.checker}</td>
			 <td align="center">{ROW.literbbm}</td>
			 <td align="right" style="background: {ROW.bgpnpchk};">{ROW.penumpangcheck}</td>
			 <td align="right" style="background: {ROW.bgpktchk};">{ROW.paketcheck}</td>
			 <td align="right">{ROW.catatan}</td>
             <td align="center">{ROW.foto}</td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>
