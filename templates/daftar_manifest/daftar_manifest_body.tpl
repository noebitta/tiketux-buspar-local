<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}

    function hapusData(kode){

	  if(confirm("Apakah anda yakin akan menghapus data ini?")){



		  new Ajax.Request("daftar_manifest.php?sid={SID}",{
			  asynchronous: true,
			  method: "get",
			  parameters: "mode=delete&no_spj="+kode,
			  onLoading: function(request)
			  {
			  },
			  onComplete: function(request)
			  {

			  },
			  onSuccess: function(request)
			  {

				  if(request.responseText==1){
					  window.location.reload();
				  }
				  else if(request.responseText==0){
					  alert("Anda tidak memiliki akses untuk menghapus data ini!");
				  }
				  else if(request.responseText==2){
					  alert("Gagal hapus data tabel biaya op!");
				  }
				  else if(request.responseText==3){
					  alert("Gagal hapus data tabel ba op!");
				  }
				  else if(request.responseText==4){
					  alert("Gagal update data tabel posisi!");
				  }
				  else if(request.responseText==5){
					  alert("Gagal update data tabel reservasi!");
				  }
				  else if(request.responseText==6){
					  alert("Gagal hapus data tabel spj!");
				  }
				  else{
					  alert("Terjadi kegagalan!");
				  }
			  },
			  onFailure: function(request)
			  {
			  }
		  })
	  }

	  return false;

  }

    function ubahStatus(kode,status_terakhir){

      new Ajax.Request("daftar_manifest.php?sid={SID}",{
          asynchronous: true,
          method: "get",
          parameters: "mode=ubahstatus&nospj="+kode+"&status="+status_terakhir,
          onLoading: function(request)
          {
          },
          onComplete: function(request)
          {
              alert(parameter);
          },
          onSuccess: function(request)
          {
              window.location.reload();
          },
          onFailure: function(request)
          {
          }
      });

      return false;

  }
	
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Daftar Manifest</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'><select onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select></td>
							<td class='bannernormal'>Asal:&nbsp;</td><td><div id='rewrite_asal'></div></td>
							<td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div></td>
							<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
							<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='right' valign='bottom'>
							{PAGING}
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>No</th>
			 <th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jadwal</a></th>
			 <th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jadwal</a></th>
			 <th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Berangkat</a></th>
			 <th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Keterlambatan</a></th>
			 <th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>#Manifest</a></th>
			 <th width=200><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Sopir</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>Mobil</a></th>
			 <th width=100><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>No Polisi</a></th>
			 <th width=100><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Jum.Pnp</a></th>
			<th width=70><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Paket</a></th>
			<th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Check</a></th>
			<th width=100><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Checker</a></th>
			<th width=70><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Pnp.Chk</a></th>
			<th width=70><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>Pkt.Chk</a></th>
			<th width=70><a class="th" >Pnp.Tbh</a></th>
			<th width=70><a class="th" >Pnp.NoTkt</a></th>
			<th width=70><a class="th" >Jml.Koli</a></th>
			<th width=70><a class="th" >Isi BBM</a></th>
			<th width=70><a class="th" >Jml Liter</a></th>
        <th width=70><a class="th" >Trip</a></th>
			<th width=100><a class="th">Catatan</a></th>
			<th width=70><a class="th">Foto</a></th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="right">{ROW.no}</div></td>
       <td ><div align="center">{ROW.jadwal}</div></td>
		 <td ><div align="left">{ROW.kodejadwal}</div></td>
		 <td ><div align="center">{ROW.berangkat}</div></td>
		 <td ><div align="center" class='{ROW.flagterlambat}'>{ROW.keterlambatan}</div></td>
		 <td ><div align="left">{ROW.manifest}</div></td>
		 <td ><div align="left">{ROW.sopir}</div></td>
		 <td ><div align="left">{ROW.mobil}</div></td>
		 <td ><div align="left">{ROW.nopol}</div></td>
		 <td ><div align="right">{ROW.penumpang}</div></td>
		 <td align="right">{ROW.paket}</td>
		 <td align="center">{ROW.waktucheck}</td>
		 <td align="center">{ROW.checker}</td>
		 <td align="right" style="background: {ROW.bgpnpchk};">{ROW.penumpangcheck}</td>
		 <td align="right" style="background: {ROW.bgpktchk};">{ROW.paketcheck}</td>
		 <td align="right" style="background: {ROW.bgpnptchk};">{ROW.penumpangtcheck}</td>
		 <td align="right" style="background: {ROW.bgpnpttchk};">{ROW.penumpangttcheck}</td>
		 <td align="center">{ROW.koli}</td>
		 <td align="center" style="background: {ROW.bgbbm};">{ROW.bbm}</td>
		 <td align="center">{ROW.literbbm}</td>
         <td align="center" style="background: {ROW.bgtrip};">{ROW.trip}</td>
		 <td align="right">{ROW.catatan}</td>
		 <td align="center">{ROW.foto}</td>
		 <td align="center">{ROW.linkHapus}</td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>