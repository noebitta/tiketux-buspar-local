<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<form action="{URL}" method="post">
    <input type="hidden" value="{MODE}" name="mode">
    <input type="hidden" value="{SUBMODE}" name="submode">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr class='banner' height=40>
            <td align='center' valign='middle' class="bannerjudul">&nbsp;Biaya Tambahan</td>
        </tr>
        <tr>
            <td class="whiter" valign="middle" align="center">
                <table width='1000'>
                    <tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
                    <tr>
                        <td align='center' valign='top' width='500'>
                            <table width='500'>
                                <tr>
                                    <td colspan=3><h2>Realisasi Biaya Tambahan {SUBMODE}</h2></td>
                                </tr>
                                <tr>
                                    <td width='200'><u>Mobil</u></td><td width='5'>:</td>
                                    <td width='300'>
                                        {MOBIL}
                                    </td>
                                </tr>
                                <tr>
                                    <td width='200'><u>Jurusan</u></td><td width='5'>:</td>
                                    <td width='300'>
                                        {JURUSAN}
                                    </td>
                                </tr>
                                <tr>
                                    <td width='200'><u>Penerima</u></td><td width='5'>:</td>
                                    <td width='300'>
                                        {SUPIR}
                                    </td>
                                </tr>
                                <!-- BEGIN ROWJENIS -->
                                {ROWJENIS.JENIS}
                                <!-- END ROWJENIS -->
                                <tr>
                                    <td width="200">Keterangan</td><td width="5">:</td>
                                    <td>
                                        <textarea id="keterangan" name="keterangan">{KETERANGAN}</textarea>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3 align='center' valign='middle' height=40>
                            <input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
