<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css">
  <link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: Error</title>
  <link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<body>
<div align="center">
  <div>
    <span style="display:inline-block;background: #da251d; color: white;width: 100%;font-family: 'trebuchet ms';font-size: 16px;padding-top: 10px;padding-bottom: 10px;text-align: left;">
      <img src="./templates/images/logo_header.gif" style="max-width: 200px;"/>
    </span><br><br>
    <div style="width:600px;height:260px;box-shadow: 3px 3px 5px grey;">
      <span style="display:inline-block;height: 210px;width: 100%;">
        <span style="display:inline-block;background: #a1a1a1; color: white;width: 100%;font-family: 'trebuchet ms';font-size: 16px;padding-top: 10px;padding-bottom: 10px">ERROR</span>
        <span style="display:inline-block;float:left;padding-left: 10px;padding-top: 10px;"><img src="./templates/images/tetuko.png" style="max-width: 200px;"/></span>
        <span style="display:inline-block;float:right;vertical-align:middle;width: 400px;font-family: 'trebuchet ms'; font-size: 14px;padding-top: 10px;text-transform: uppercase;">
          {PESAN}
        </span>
      </span>
      <span style="float: right;padding-right: 10px;">
        <span class="flatbutton" onclick="history.back();">OK</span>
      </span>
    </div>
  </div>
</div>
</body>
</html>


