<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
	// komponen khusus dojo
	dojo.require("dojo.widget.Dialog");

	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}

	function setData(bulan){
		tahun	=document.getElementById('tahun').value;

		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}

	function getUpdateAsal(kota){

		new Ajax.Updater("rewrite_asal","daftar_manifest_kendaraan.php?sid={SID}", {
			asynchronous: true,
			method: "get",

			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){
				assignError(request.responseText);
			}
		});
	}

	function getKendaraan(){

		new Ajax.Updater("rewrite_kendaraan","daftar_manifest_kendaraan.php?sid={SID}", {
			asynchronous: true,
			method: "get",

			parameters: "mode=getmobil",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_kendaraan');
			},
			onFailure: function(request){
				assignError(request.responseText);
			}
		});
	}

	function getUpdateTujuan(asal){
		// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]

		new Ajax.Updater("rewrite_tujuan","daftar_manifest_kendaraan.php?sid={SID}",
				{
					asynchronous: true,
					method: "get",
					parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
					onLoading: function(request)
					{
					},
					onComplete: function(request)
					{
					},
					onFailure: function(request)
					{
						assignError(request.responseText);
					}
				});
	}
	function hapusData(kode){

		if(confirm("Apakah anda yakin akan menghapus data ini?")){



			new Ajax.Request("daftar_manifest.php?sid={SID}",{
				asynchronous: true,
				method: "get",
				parameters: "mode=delete&no_spj="+kode,
				onLoading: function(request)
				{
				},
				onComplete: function(request)
				{

				},
				onSuccess: function(request)
				{

					if(request.responseText==1){
						window.location.reload();
					}
					else if(request.responseText==0){
						alert("Anda tidak memiliki akses untuk menghapus data ini!");
					}
					else if(request.responseText==2){
						alert("Gagal hapus data tabel biaya op!");
					}
					else if(request.responseText==3){
						alert("Gagal hapus data tabel ba op!");
					}
					else if(request.responseText==4){
						alert("Gagal update data tabel posisi!");
					}
					else if(request.responseText==5){
						alert("Gagal update data tabel reservasi!");
					}
					else if(request.responseText==6){
						alert("Gagal hapus data tabel spj!");
					}
					else{
						alert("Terjadi kegagalan!");
					}
				},
				onFailure: function(request)
				{
				}
			})
		}

		return false;

	}

	function showDialogBuatVoucher(spj){
		no_spj.value = spj;
		/*nilaivoucher.value="";
		 keterangan.value="";
		 jumlahcetak.value="";*/
		dlg_buat_voucher.show();
	}

	function showDialogEditVoucher(spj){
		//no_spj_edit.value = spj;

		new Ajax.Updater("dialogeditvoucher","daftar_manifest_kendaraan.php?sid={SID}", {
			asynchronous: true,
			method: "get",

			parameters: "mode=edit_voucher&no_spj="+spj,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
			},
			onFailure: function(request){
				assignError(request.responseText);
			}
		});

		dlg_edit_voucher.show();
	}

	function validasiLiter(evt){
		var theEvent = evt || window.event;

		var key = theEvent.keyCode || theEvent.which;

		key = String.fromCharCode(key);

		var regex = /[0-9]/;

		if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 ||
				[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;

		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			theEvent.preventDefault();
		}
	}

	function getMobil(){

		new Ajax.Updater("rewrite_mobil","laporan_voucher_bbm.php?sid={SID}", {
			asynchronous: true,
			method: "get",

			parameters: "mode=get_mobil",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_mobil');
			},
			onFailure: function(request){
				assignError(request.responseText);
			}
		});
	}

	function getDaftarSopir(){


		new Ajax.Updater("dlgdaftarsopircbosopir","laporan_voucher_bbm.php?sid={SID}",
				{
					asynchronous: true,
					method: "get",
					parameters: "mode=get_sopir",
					onLoading: function(request)
					{
						//dlg_daftar_sopir.show();
						Element.show('dlgdaftarsopirloading');
					},
					onComplete: function(request)
					{
						//document.getElementById("dlgdaftarsopirkodejadwal").innerHTML	= kodejadwal;
						Element.hide('dlgdaftarsopirloading');
					},
					onFailure: function(request)
					{
						assignError(request.responseText);
					}
				});
	}

	function buatVoucher(){

		is_valid=true;


		if(kilometer.value==""){
			kilometer.style.background = 'red';
			is_valid = false;
		}

		if(jml_liter.value==""){
			jml_liter.style.background = 'red';
			is_valid = false;
		}

		if(!is_valid){
			exit;
		}

		new Ajax.Request("daftar_manifest_kendaraan.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=add_voucher"+
			"&no_spj="+no_spj.value+
				/*"&tgl_berangkat="+tgl_berangkat.value+
				 "&kode_jadwal="+opt_jadwal.value+
				 "&id_jurusan="+opt_tujuan.value+
				 "&mobil="+mobil.value+
				 "&sopir="+dlgcbosopir.value+*/
			"&kilometer="+kilometer.value+
			"&jenis_bbm="+jenis_bbm.value+
			"&jml_liter="+jml_liter.value+
			"&kode_spbu="+kode_spbu.value,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
				dlg_buat_voucher.hide();
			},
			onSuccess: function(request){
				eval(request.responseText);
			},
			onFailure: function(request){
				alert('Error !!! Cannot Save');
				assignError(request.responseText);
			}
		});
	}

	function updateVoucher(){
		is_valid=true;


		if(kilometer_edit.value==""){
			kilometer_edit.style.background = 'red';
			is_valid = false;
		}

		if(jml_liter_edit.value==""){
			jml_liter_edit.style.background = 'red';
			is_valid = false;
		}

		if(!is_valid){
			exit;
		}


		new Ajax.Request("daftar_manifest_kendaraan.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=update_voucher"+
			"&no_spj_edit="+no_spj_edit.value+
				/*"&tgl_berangkat="+tgl_berangkat.value+
				 "&kode_jadwal="+opt_jadwal.value+
				 "&id_jurusan="+opt_tujuan.value+
				 "&mobil="+mobil.value+
				 "&sopir="+dlgcbosopir.value+*/
			"&kilometer_edit="+kilometer_edit.value+
			"&jenis_bbm_edit="+jenis_bbm_edit.value+
			"&jml_liter_edit="+jml_liter_edit.value+
			"&kode_spbu_edit="+kode_spbu_edit.value,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
				dlg_buat_voucher.hide();
			},
			onSuccess: function(request){
				eval(request.responseText);
			},
			onFailure: function(request){
				alert('Error !!! Cannot Save');
				assignError(request.responseText);
			}
		});
	}

	function approve(kode){

		if(confirm("Apakah anda yakin akan melakukan aksi ini?")){



			new Ajax.Request("laporan_voucher_bbm.php?sid={SID}",{
				asynchronous: true,
				method: "get",
				parameters: "mode=approve&no_spj="+kode,
				onLoading: function(request)
				{
				},
				onComplete: function(request)
				{

				},
				onSuccess: function(request)
				{

					if(request.responseText==1){
						window.location.reload();
					}
					else{
						alert("Terjadi kegagalan!");
					}
				},
				onFailure: function(request)
				{
				}
			})
		}

		return false;

	}

	function init(e){
		dlg_buat_voucher			= dojo.widget.byId("dialogbuatvoucher");
		dlg_buat_voucher_btn_cancel = document.getElementById("dialogbuatvouchercancel");

		dlg_edit_voucher			= dojo.widget.byId("dialogeditvoucher");
		dlg_edit_voucher_btn_cancel = document.getElementById("dialogeditvouchercancel");
	}

	dojo.addOnLoad(init);
	getKendaraan();
	getMobil();
	getDaftarSopir();
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
</script>

<!--dialog Create Voucher-->
<div dojoType="dialog" id="dialogbuatvoucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width="400">
			<tr>
				<td bgcolor='ffffff' align='center'>
					<table>
						<tr><td colspan='2'><h2>Buat Voucher</h2></td></tr>
						<tr>
							<td align='right'>Manifest :</td>
							<td>
								<input id="no_spj" name="no_spj" type="text" onfocus="this.style.background='white'" value="" />
							</td>
						</tr>
						<!-- <tr>
				<td align='right'>Tgl Berangkat:</td>
				<td>
					<input id="tgl_berangkat" name="tgl_berangkat" type="text" readonly='yes' size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERANGKAT}">
				</td>
			</tr>
			<tr>
				<td align='right'>Cabang Asal:</td>
				<td>
					<select name='opt_cabang_asal' id='opt_cabang_asal' onChange="getUpdateTujuan(this.value)" onfocus="this.style.background='white'">{CABANG_ASAL}</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Cabang Tujuan:</td>
				<td>
					<div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span>
				</td>
			</tr>
			<tr>
				<td align='right'>Jadwal:</td>
				<td>
					<div id="rewrite_jadwal"></div>
				</td>
			</tr>-->
						<tr>
							<td align='right'>Nama SPBU:</td>
							<td>
								<select id='kode_spbu'>
									<option value='1'>SPBU 57</option>
									<option value='2'>SPBU 72</option>
								</select>
							</td>
						</tr>
						<!-- <tr>
				<td align='right'>Kendaraan:</td>
				<td>
					<div id='rewrite_mobil'></div>
				</td>
			</tr>
			<tr>
				<td align='right'>Sopir:</td>
				<td>
					<span id="dlgdaftarsopircbosopir"></span><span id='dlgdaftarsopirloading' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span>
				</td>
			</tr> -->
						<tr>
							<td align='right'>Kilometer:</td>
							<td><input id="kilometer" name="kilometer" type="text" onfocus="this.style.background='white'" size="5" onkeypress="validasiLiter(event)" /></td>
						</tr>
						<tr>
							<td align='right'>Jenis BBM:</td>
							<td>
								<select id='jenis_bbm'>
									<option value='1'>SOLAR</option>
									<option value='2'>PREMIUM</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align='right'>Jumlah Liter:</td>
							<td><input id="jml_liter" name="jml_liter" type="text" onfocus="this.style.background='white'" size="5" onkeypress="validasiLiter(event)" /></td>
						</tr>
						<!-- <tr>
				<td align='right'>Harga:</td>
				<td>
					{HARGA_LITER} / Liter
				</td>
			</tr> -->
					</table>
					<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
					<br>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br>
					<input type="button" id="dialogbuatvouchercancel" value="&nbsp;Cancel&nbsp;" onClick="dlg_buat_voucher.hide();">
					<input type="button" onclick="buatVoucher();" id="dialogbuatvoucherproses" value="&nbsp;Proses&nbsp;">
				</td>
			</tr>
		</table>
	</form>
</div>
<!--END dialog create voucher-->

<!--dialog Edit Voucher-->
<div dojoType="dialog" id="dialogeditvoucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
</div>
<!--END dialog edit voucher-->

<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td class="whiter" valign="middle" align="left">
			<form action="{ACTION_CARI}" method="post">
				<!--HEADER-->
				<table width='100%' cellspacing="0">
					<tr class='banner' height=40>
						<td align='center' valign='middle' class="bannerjudul">&nbsp;BBM Per Kendaraan</td>
						<td align='right' valign='middle'>
							<table>
								<tr>
									<td class='bannernormal'><select onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select></td>
									<td class='bannernormal'>Asal:&nbsp;</td><td><div id='rewrite_asal'></div></td>
									<td class='bannernormal'>&nbsp;Tujuan:&nbsp;</td><td><div id='rewrite_tujuan'></div></td>
									<td class='bannernormal'>&nbsp;Kendaraan:&nbsp;</td><td><div id='rewrite_kendaraan'></div></td>
									<td class='bannernormal' colspan=2>&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}"></td>
									<td class='bannernormal' colspan=2>&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}"></td>
									<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>
									<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan=2 align='center' valign='middle'>
							<table>
								<tr>
									<td>
										<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan=2>
							<table width='100%'>
								<tr>
									<td align='right' valign='bottom'>
										{PAGING}
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<!-- END HEADER-->
				<table class="border" width='100%' >
					<tr>
						<th width=30>No</th>
						<th width=70><a class="th" href='{A_SORT_7}' title='{TIPS_SORT_7}'>#Body</a></th>
						<th width=150><a class="th" href='{A_SORT_1}' title='{TIPS_SORT_1}'>Jadwal</a></th>
						<th width=100><a class="th" href='{A_SORT_2}' title='{TIPS_SORT_2}'>Kode Jadwal</a></th>
						<th width=150><a class="th" href='{A_SORT_3}' title='{TIPS_SORT_3}'>Berangkat</a></th>
						<th width=100><a class="th" href='{A_SORT_4}' title='{TIPS_SORT_4}'>Delay</a></th>
						<th width=100><a class="th" href='{A_SORT_5}' title='{TIPS_SORT_5}'>#Manifest</a></th>
						<th width=100><a class="th" href='{A_SORT_6}' title='{TIPS_SORT_6}'>Sopir</a></th>
						<!-- <th width=70><a class="th" href='{A_SORT_8}' title='{TIPS_SORT_8}'>Pnp</a></th>
			 <th width=70><a class="th" href='{A_SORT_9}' title='{TIPS_SORT_9}'>Paket</a></th> -->
						<th width=100><a class="th" href='{A_SORT_10}' title='{TIPS_SORT_10}'>Tgl Isi</a></th>
						<th width=100><a class="th" href='{A_SORT_11}' title='{TIPS_SORT_11}'>Petugas Isi</a></th>
						<!-- <th width=70><a class="th" href='{A_SORT_12}' title='{TIPS_SORT_12}'>Pnp.Chk</a></th>
			 <th width=70><a class="th" href='{A_SORT_13}' title='{TIPS_SORT_13}'>Pkt.Chk</a></th>
			 <th width=70><a class="th" >Pnp.Tbh</a></th>
                         <th width=70><a class="th" >Pnp.NoTkt</a></th>
                         <th width=70><a class="th" >Jml.Koli</a></th> -->
						<th width=70><a class="th" >Jml Liter</a></th>
						<th width=70><a class="th" >KM</a></th>
						<th width=70><a class="th" >SPBU</a></th>
						<th width=100><a class="th">Catatan</a></th>
						<th width=70><a class="th" >Foto</a></th>
						<th width=70><a class="th" >Action</a></th>
						<th width=70><a class="th" >Approve</a></th>
					</tr>
					<!-- BEGIN ROW -->
					<tr class="{ROW.odd}">
						<td align="right">{ROW.no}</td>
						{ROW.mobil}
						<td align="center">{ROW.jadwal}</td>
						<td align="center">{ROW.kodejadwal}</td>
						<td align="center">{ROW.berangkat}</td>
						<td align="center" class='{ROW.flagterlambat}'>{ROW.keterlambatan}</td>
						<td align="center">{ROW.manifest}</td>
						<td align="center">{ROW.sopir}</td>
						<!-- <td align="right">{ROW.penumpang}</td>
			 <td align="right">{ROW.paket}</td> -->
						<td align="center">{ROW.tglbbm}</td>
						<td align="center">{ROW.petugasbbm}</td>
						<!-- <td align="right" style="background: {ROW.bgpnpchk};">{ROW.penumpangcheck}</td>
			 <td align="right" style="background: {ROW.bgpktchk};">{ROW.paketcheck}</td>
			 <td align="right" style="background: {ROW.bgpnptchk};">{ROW.penumpangtcheck}</td>
                         <td align="right" style="background: {ROW.bgpnpttchk};">{ROW.penumpangttcheck}</td>
                         <td align="center">{ROW.koli}</td> -->
						<td align="center" style="background: {ROW.bgbbm};">{ROW.literbbm}</td>
						<td align="center">{ROW.kilometer}</td>
						<td align="center">{ROW.spbu}</td>
						<td align="right">{ROW.catatan}</td>
						<td align="center">{ROW.foto}</td>
						<td align="center"><div title="Create Voucher BBM"><a href='#' onClick="{ROW.act}"> <img src="{TPL}/images/b_manifest.png"></a></div></td>
						<td align="center" style="background: {ROW.bgstatus};">{ROW.app}</td>
					</tr>
					<!-- END ROW -->
				</table>
				<table width='100%'>
					<tr>
						<td align='right' width='100%'>
							{PAGING}
						</td>
					</tr>
					<tr>
						<td align='left' valign='bottom' colspan=3>
							{SUMMARY}
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
