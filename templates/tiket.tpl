<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">

	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}

</script>	 

<body class="tiket">
<!--
	UPDATE 5 APRIL
	TIKET JADI 2 COPI, PERTAM UNTUK CUSTOMER KEDUA UNTUK COUNTER
	-->

<!-- UNTUK CUSTOMER-->
<div id="pax" style="margin-left: 18px; margin-bottom: 10px;">
	<span style="font-size:22px;">PAX</span>
	<!-- BEGIN ROW -->
	<div id="tiket" style="font-size:16px; width:180px; margin-left: 20px; margin-bottom: 10px;">
			<br><br>
			<div align="center">
				{ROW.NO_TIKET}<br>
				{ROW.DUPLIKAT}
				<img src="{ROW.QR_CODE}">
			</div>
			By:{ROW.JENIS_BAYAR}<br>
			Pergi:{ROW.TGL_BERANGKAT}<br>
			{ROW.JURUSAN}<br>
			<div style="text-align: center;width: 180px">
				({ROW.KATEGORI})<br>
			</div>
			<div style="text-align: center;width:180px;">
				{ROW.JAM_BERANGKAT}<br>
				{ROW.NAMA_PENUMPANG}<br>
				Kursi<br><br>
				<span style="font-size:50px;"><strong>{ROW.NOMOR_KURSI}</strong></span><br>
				{ROW.NAMA}<br>
			</div>
			{ROW.HARGA_TIKET}
			Disc.:{ROW.DISKON}<br>
			Bayar:<span style="font-size: 16px;"><strong>{ROW.BAYAR}</strong></span><br>
			{ROW.POINT_MEMBER}<br>
			{ROW.AWARDPONTA}<br>
			{ROW.BALANCEPONTA}<br>
			CSO:{ROW.CSO}<br>
			WaktuCetak<br>
			{ROW.WAKTU_CETAK}<br><br>
			<span style="font-size: 12px">{ROW.PESANPONTA}</span><br><br>
			<span style="font-size: 12px">{ROW.PESAN}</span>
			<br>
			<p style="text-align: center;width:180px;">
				-- Terima Kasih --
			</p>
	</div>
	<!-- END ROW -->
	<!-- BEGIN KUPON_MERCH -->
	<div id="kupon_merch" style="font-size:14px;width:180px;   margin-left: 20px;margin-bottom: 10px;">
		<br>
		------KUPON MERCHANDISE-----<br>
		Nama:{KUPON_MERCH.nama}<br>
		Telp:{KUPON_MERCH.telp}<br>
		#Tiket:{KUPON_MERCH.notiket}<br>
		Tgl:{KUPON_MERCH.tgl}<br>
		#Jadwal:{KUPON_MERCH.jadwal}<br>
		Tukarkan kupon ini dengan merchandise selama persediaan masih ada
	</div>
	<!-- END KUPON_MERCH -->
	<!-- BEGIN kupon_undian -->
	<div id="kupon_undian" style="font-size:14px;width:180px;   margin-left: 20px; margin-bottom: 10px;">

		<div style="text-align: center;width:180px;">
			<br><br><br>
			---KUPON UNDIAN--<br>
			<br>
			<span style="font-size:20px;">{KODE_KUPON}</span><br><br>
			<b>{ID_MEMBER}</b><br>
			Nama:{NAMA_PENUMPANG}<br><br>
			Telp:{TELP_PENUMPANG}<br><br>
			#Tiket:{NO_TIKET}<br>
			WaktuCetak<br>
			{WAKTU_CETAK}<br>
			CSO:{PENCETAK_KUPON}<br><br><br>
		</div>
	</div>
	<!-- END kupon_undian -->
	<!-- BEGIN VOUCHER -->
	<div id="voucher" style="font-size:14px;width:180px; margin-left: 20px; margin-bottom: 10px;">
		<div style="text-align: center;width:180px;">
			<br>
			------VOUCHER------<br>
			<br>
			Nama:{VOUCHER.NAMA_PENUMPANG}<br><br>
			{VOUCHER.JURUSAN_VOUCHER}<br><br>
			TIKET BALIK<br><br>
			FREE<br><br>
			<span style="font-size:20px;">{VOUCHER.KODE_VOUCHER}</span><br><br>
			berlaku hingga<br>
			{VOUCHER.EXPIRED_VOUCHER}<br><br>
			{VOUCHER.PESAN_TUSLAH_VOUCHER}
		</div>
	</div>
	<!-- END VOUCHER -->
	<!-- BEGIN INFANT -->
	<div id="infant" style="font-size:14px;width:180px;  margin-left: 20px;">
		{INFANT.NO_TIKET}<br>
		BAYI<br>
		Pergi:{INFANT.TGL_BERANGKAT}<br>
		{INFANT.JURUSAN}<br>
		<div style="text-align: center;width:180px;">
			{INFANT.JAM_BERANGKAT}<br>
			{INFANT.NAMA_BAYI}<br>
			<span style="font-size:20px;"><strong>NON_SEAT</strong></span>
		</div>
		<br>
		Asuransi:<span style="font-size: 16px;"><strong>{INFANT.ASURANSI}</strong></span><br>
		CSO:{INFANT.CSO}<br>
		WaktuCetak<br>
		{INFANT.WAKTU_CETAK}<br>
		<br>
		<div style="text-align: center;width:180px;">-- Terima Kasih --</div><br>
	</div>
	<!-- END INFANT -->
</div>

	<!-- UNTUK COUNTER-->
<div id="pax2" style="margin-left: 18px; margin-bottom: 10px;">
	<span style="font-size:22px;">COUNTER</span>
	<!-- BEGIN ROW -->
	<div id="tiket2" style="font-size:16px; width:180px; margin-left: 20px; margin-bottom: 10px;">
		<br><br>
		<div align="center">
			{ROW.NO_TIKET}<br>
			{ROW.DUPLIKAT}
			<img src="{ROW.QR_CODE}">
		</div>
		By:{ROW.JENIS_BAYAR}<br>
		Pergi:{ROW.TGL_BERANGKAT}<br>
		{ROW.JURUSAN}<br>
		<div style="text-align: center;width: 180px">
			({ROW.KATEGORI})<br>
		</div>
		<div style="text-align: center;width:180px;">
			{ROW.JAM_BERANGKAT}<br>
			{ROW.NAMA_PENUMPANG}<br>
			Kursi<br><br>
			<span style="font-size:50px;"><strong>{ROW.NOMOR_KURSI}</strong></span><br>
			{ROW.NAMA}<br>
		</div>
		{ROW.HARGA_TIKET}
		Disc.:{ROW.DISKON}<br>
		Bayar:<span style="font-size: 16px;"><strong>{ROW.BAYAR}</strong></span><br>
		{ROW.POINT_MEMBER}<br>
		{ROW.AWARDPONTA}<br>
		{ROW.BALANCEPONTA}<br>
		CSO:{ROW.CSO}<br>
		WaktuCetak<br>
		{ROW.WAKTU_CETAK}<br><br>
		<span style="font-size: 12px">{ROW.PESANPONTA}</span><br><br>
		<span style="font-size: 12px">{ROW.PESAN}</span>
		<br>
		<p style="text-align: center;width:180px;">
			-- Terima Kasih --
		</p>
	</div>
	<!-- END ROW -->
	<!-- BEGIN KUPON_MERCH -->
	<div id="kupon_merch2" style="font-size:14px;width:180px;   margin-left: 20px;margin-bottom: 10px;">
		<br>
		------KUPON MERCHANDISE-----<br>
		Nama:{KUPON_MERCH.nama}<br>
		Telp:{KUPON_MERCH.telp}<br>
		#Tiket:{KUPON_MERCH.notiket}<br>
		Tgl:{KUPON_MERCH.tgl}<br>
		#Jadwal:{KUPON_MERCH.jadwal}<br>
		Tukarkan kupon ini dengan merchandise selama persediaan masih ada
	</div>
	<!-- END KUPON_MERCH -->
	<!-- BEGIN kupon_undian -->
	<div id="kupon_undian2" style="font-size:14px;width:180px;   margin-left: 20px; margin-bottom: 10px;">

		<div style="text-align: center;width:180px;">
			<br><br><br>
			---KUPON UNDIAN--<br>
			<br>
			<span style="font-size:20px;">{KODE_KUPON}</span><br><br>
			<b>{ID_MEMBER}</b><br>
			Nama:{NAMA_PENUMPANG}<br><br>
			Telp:{TELP_PENUMPANG}<br><br>
			#Tiket:{NO_TIKET}<br>
			WaktuCetak<br>
			{WAKTU_CETAK}<br>
			CSO:{PENCETAK_KUPON}<br><br><br>
		</div>
	</div>
	<!-- END kupon_undian -->
	<!-- BEGIN VOUCHER -->
	<div id="voucher2" style="font-size:14px;width:180px; margin-left: 20px; margin-bottom: 10px;">
		<div style="text-align: center;width:180px;">
			<br>
			------VOUCHER------<br>
			<br>
			Nama:{VOUCHER.NAMA_PENUMPANG}<br><br>
			{VOUCHER.JURUSAN_VOUCHER}<br><br>
			TIKET BALIK<br><br>
			FREE<br><br>
			<span style="font-size:20px;">{VOUCHER.KODE_VOUCHER}</span><br><br>
			berlaku hingga<br>
			{VOUCHER.EXPIRED_VOUCHER}<br><br>
			{VOUCHER.PESAN_TUSLAH_VOUCHER}
		</div>
	</div>
	<!-- END VOUCHER -->
	<!-- BEGIN INFANT -->
	<div id="infant2" style="font-size:14px;width:180px;  margin-left: 20px;">
		{INFANT.NO_TIKET}<br>
		BAYI<br>
		Pergi:{INFANT.TGL_BERANGKAT}<br>
		{INFANT.JURUSAN}<br>
		<div style="text-align: center;width:180px;">
			{INFANT.JAM_BERANGKAT}<br>
			{INFANT.NAMA_BAYI}<br>
			<span style="font-size:20px;"><strong>NON_SEAT</strong></span>
		</div>
		<br>
		Asuransi:<span style="font-size: 16px;"><strong>{INFANT.ASURANSI}</strong></span><br>
		CSO:{INFANT.CSO}<br>
		WaktuCetak<br>
		{INFANT.WAKTU_CETAK}<br>
		<br>
		<div style="text-align: center;width:180px;">-- Terima Kasih --</div><br>
	</div>
	<!-- END INFANT -->
</div>


</body>

<script language="javascript">

	if({antrian} != 1){
		window.opener.getTarget();
		window.opener.getUpdateMobil();
	}
  	printWindow();
    window.close();

</script>
</html>
