<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script>
<script type="text/javascript">

function showData(){
  // Mendapatkan nilai kursi dari database (why ? biar ga bentrok :P)
	
	document.getElementById("rewrite_data_tiket").innerHTML="";
	no_tiket	= document.getElementById("txt_cari").value;
	
	new Ajax.Updater("rewrite_data_tiket","pembatalan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "no_tiket="+no_tiket+"&mode=show",
        onLoading: function(request) 
        {
					dialog_data.show();
					Element.show('loading_data_penumpang');
        },
        onComplete: function(request) 
        {              
					Element.hide('loading_data_penumpang');
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function batal(){
	
	no_tiket	= document.getElementById("no_tiket").value;
	no_kursi	= document.getElementById("no_kursi").value;
	
	if(confirm("Apakah anda yakin akan membatalkan tiket ini?")){
	
		new Ajax.Request("pembatalan.php?sid={SID}",
		{
			asynchronous: true,
			method: "get",
			parameters: "mode=pembatalan&no_tiket="+no_tiket+"&no_kursi="+no_kursi,
			onLoading: function(request) {
				Element.show('loading_data_penumpang');
			},
			onComplete: function(request) 
			{
			
			},
			onSuccess: function(request) {			
				Element.hide('loading_data_penumpang');
				
				if(request.responseText==1){
					alert('Tiket berhasil dibatalkan');
					document.getElementById('rewrite_data_tiket').innerHTML='';
					dialog_data.hide();
					document.getElementById("txt_cari").value="";
				}
				else{
					alert("Anda tidak memiliki wewenang untuk membatalkan tiket ini!");
				}
				
			},
			onFailure: function(request) 
			{
			}
			})  
	}
	
	return false;
		
}

function init(e) {
	
	//control dialog data
	dialog_data = dojo.widget.byId("dialog_data");
}

dojo.addOnLoad(init);

</script>

<!--dialog DATA TIKET-->
<div dojoType="dialog" id="dialog_data" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table height='300' width='500'>
<tr><td height='30'><h2>Data Tiket</h2></td></tr>
<tr>
	<td align='center' valign='top' bgcolor='ffffff'>
		<div id="rewrite_data_tiket"></div><span id='loading_data_penumpang' style='display:none;'><img src='{TPL}images/loading2.gif' /><font size=2 color='A0A0A0'>&nbsp;Sedang memproses...</font></span>
	</td>
</tr>
<tr>
	<td colspan='3' align='center'> 
		<br><br>
		<input type='button' onClick='dialog_data.hide();' value='           Kembali         '/>&nbsp;
		<input type='button' onclick="batal();" id='hider2' value='&nbsp;&nbsp;Batalkan tiket&nbsp;&nbsp;'>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog DATA TIKET-->


<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Pembatalan Tiket</td>
			</tr>
		</table>
 </td>
</tr>
<tr>
	<td>
		<table width='100%'>
			<tr>
				<td align='center' valign='middle'>
					<br>
					<br>
					<br>
						Silahkan masukkan nomor tiket yang dibatalkan:&nbsp;<input type="text" id="txt_cari" name="txt_cari" />&nbsp;<input type="button" value="Batalkan" onClick="showData();" />&nbsp;
					<br>
					<br>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>