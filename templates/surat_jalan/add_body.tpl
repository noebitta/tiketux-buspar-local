<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>

<form name="frm_data_jurusan" action="{U_JURUSAN_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Surat Jalan</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='500'>
				<input type="hidden" name="no_sj" value="{NO_SJ}">
				<table width='500'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
						<td width='200'><u>Waktu Surat Jalan</u></td><td width='5'>:</td>
						<td width='300'>
							<input readonly="yes"  id="tgl_sj" name="tgl_sj" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_SJ}">&nbsp;
							Pkl.
							<select id='opt_jam' name='opt_jam'>
								{OPT_JAM}
							</select>&nbsp;:
							<select id='opt_menit' name='opt_menit'>
								{OPT_MENIT}
							</select>
						</td>
			    </tr>
					<tr>
						<td>Pool Keluar</td><td>:</td>
						<td>
							<select id='pool' name='pool'>
								{OPT_POOL}
							</select>
						</td>
					</tr>
					<tr>
						<td>Kendaraan</td><td>:</td>
						<td>
							<select id='kendaraan' name='kendaraan'>
								{OPT_KENDARAAN}
							</select>
						</td>
					</tr>
					<tr>
						<td>Sopir</td><td>:</td>
						<td>
							<select id='sopir' name='sopir'>
								{OPT_SOPIR}
							</select>
						</td>
					</tr>
					<tr>
						<td>Jumlah Trip</td><td>:</td>
						<td>
							<select id='trip' name='trip' onChange="setDetailBiaya();">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3" selected>3</option>
								<option value="4">4</option>
							</select>&nbsp;trip
						</td>
					</tr>
				</table>
			</td>
			<td width=1 bgcolor='D0D0D0'></td>
			<td align='center' valign='top' width='500'>
				<span id="rewrite_detail_biaya"></span>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
				<input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>           
	</table>
	</td>
</tr>
</table>
</form>

<script language="JavaScript">

var no_sj;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function getUpdateAsal(idx){
	
	new Ajax.Updater("rewrite_asal"+idx,"surat_jalan.php?sid={SID}", {
		asynchronous: true,
		method: "get",
		
		parameters: "mode=getasal&asal={ASAL}&idx="+idx,
		onLoading: function(request){
		},
		onComplete: function(request){
		},
		onFailure: function(request){ 
			assignError(request.responseText); 
		}
	});				
}

function getUpdateTujuan(asal,idx){
  // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
	
	new Ajax.Updater("rewrite_tujuan"+idx,"surat_jalan.php?sid={SID}", 
  {
      asynchronous: true,
      method: "get",
			parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan&idx="+idx,
      onLoading: function(request) 
      {
      },
      onComplete: function(request) 
      {
				//alert(request.responseText);
      },
      onFailure: function(request) 
      { 
        assignError(request.responseText); 
      }
  });   
}

function getBiaya(asal,idx){
	
	new Ajax.Updater("rewrite_biaya"+idx,"surat_jalan.php?sid={SID}", {
		asynchronous: true,
		method: "get",
		
		parameters: "mode=getbiaya&asal="+asal+"&idx="+idx+"&no_sj={NO_SJ}",
		onLoading: function(request){
		},
		onComplete: function(request){
		},
		onFailure: function(request){ 
			assignError(request.responseText); 
		}
	});				
}

function setDetailBiaya(){
	
	trip	= document.getElementById("trip").value;
	
	new Ajax.Updater("rewrite_detail_biaya","surat_jalan.php?sid={SID}", {
		asynchronous: true,
		method: "get",
		
		parameters: "mode=setdetailbiaya&no_sj={NO_SJ}&trip="+trip,
		onLoading: function(request){
		},
		onComplete: function(request){
		},
		onFailure: function(request){ 
			assignError(request.responseText); 
		}
	});				
}

setDetailBiaya();

function validateInput(){
	
	valid=true;
	
	valid	= (document.getElementById("kendaraan").value!="")?true:false;
	valid	= (document.getElementById("sopir").value!="")?true:false;
	
	trip	= document.getElementById("trip").value;
	
	for(idx=0;idx<trip;idx++){
		tujuan_temp=document.getElementById('tujuan'+idx);
		if(tujuan_temp){
			if(tujuan_temp.value==""){
				valid=false;
				idx=trip;
			}
		}
		else{
			valid=false;
			idx=trip;
		}
	}
	
	if(valid){
		return true;
	}
	else{
		alert("SIlahkan lengkapi data isian anda!");
		return false;
	}
}
</script>