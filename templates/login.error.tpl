<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css">
  <link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: Login</title>
  <link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<body id="loginbody">
<div align="center">
  <div id="loginwrapper">
    <form class="loginform" name="formlogin" action="auth.php" method="post">
      <span style="display:inline-block;background: #da251d; color: white;width: 100%;font-family: 'trebuchet ms';font-size: 16px;padding-top: 10px;padding-bottom: 10px">ERROR LOGIN</span>
      <span style="display:inline-block;vertical-align:middle;width: 100%;font-family: 'trebuchet ms'; font-size: 14px;height: 266px;padding-top: 10px;">
         <img src="./templates/images/login/loginerror.png" style="max-width: 200px;"/><br>
        {PESAN}
      </span>
      <input id="loginbutton" name="loginbutton" class="flatbutton" value="OK" type="button" onclick="window.location='.';"/>
    </form>
  </div>
</div>
</body>
</html>
