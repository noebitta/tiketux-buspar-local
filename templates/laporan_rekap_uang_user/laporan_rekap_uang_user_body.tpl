<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location="{URL}&bulan="+bulan+"&tahun="+tahun;
	}
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr height=40 class="bannercari">
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Uang</td>
				<td class="bannerjudul">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<br>
					<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<table width='100%'>
						<tr>
							<td align='left'>
								<a href="{U_LAPORAN_OMZET_GRAFIK}"><img src="{TPL}images/icon_grafik.png" /></a>
								<a href="{U_LAPORAN_OMZET_GRAFIK}">Lihat Grafik</a>
							</td>
							<td align='right' valign='bottom'>
								{LIST_BULAN}
								&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp;												
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table class="border" width='100%' >
    <tr>
       <th width=30>Tanggal</th>
			 <th width=200>Jum. Penumpang </th>
			 <th width=200>Omzet Penumpang</th>
			 <th width=200>Discount</th>
			 <th width=200>Pendapatan Penumpang</th>
			 <th width=200>Jum. Paket</th>
			 <th width=200>Omzet Paket</th>
			 <th width=200>Jum.Asuransi</th>
			 <th width=200>Omz.Asuransi</th>
			 <th width=200>Biaya Op.</th>
			 <th width=200>Biaya Tambahan</th>
			 <th width=200>Total Setor</th>
			 <th width=100>Act</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>{ROW.tgl}</b></font></div></td>
			 <td ><div align="right">{ROW.total_penumpang}</div></td>
			 <td ><div align="right">{ROW.total_omzet_penumpang}</div></td>
			 <td ><div align="right">{ROW.total_discount}</div></td>
			 <td ><div align="right">{ROW.pendapatan_penumpang}</div></td>
			 <td ><div align="right">{ROW.total_paket}</div></td>
			 <td ><div align="right">{ROW.total_omzet_paket}</div></td>
			 <td ><div align="right">{ROW.total_asuransi}</div></td>
			 <td ><div align="right">{ROW.total_omzet_asuransi}</div></td>
			 <td ><div align="right">{ROW.total_biaya}</div></td>
			 <td ><div align="right">{ROW.total_biaya_tambahan}</div></td>
			 <td ><div align="right">{ROW.total_profit}</div></td>
			 <td ><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
		 <tr bgcolor='ffff00'>
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>TOTAL</b></font></div></td>
			 <td ><div align="right"><b>{SUM_PENUMPANG}</b></div></td>
			 <td ><div align="right"><b>{SUM_OMZET_PENUMPANG}</b></div></td>
			 <td ><div align="right"><b>{SUM_DISCOUNT}</b></div></td>
			 <td ><div align="right"><b>{SUM_PENDAPATAN_PENUMPANG}</b></div></td>
			 <td ><div align="right"><b>{SUM_PAKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_OMZET_PAKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_ASURANSI}</b></div></td>
			 <td ><div align="right"><b>{SUM_OMZET_ASURANSI}</b></div></td>
			 <td ><div align="right"><b>{SUM_BIAYA}</b></div></td>
			 <td ><div align="right"><b>{SUM_BIAYA_TAMBAHAN}</b></div></td>
			 <td ><div align="right"><b>{SUM_PROFIT}</b></div></td>
			 <td ><div align="right"><b>&nbsp;</b></div></td>
     </tr>
		 <tr>
       <th width=30>Tanggal</th>
			 <th width=200>Jum. Penumpang </th>
			 <th width=200>Omzet Penumpang</th>
			 <th width=200>Discount</th>
			 <th width=200>Pendapatan Penumpang</th>
			 <th width=200>Jum. Paket</th>
			 <th width=200>Omzet Paket</th>
			 <th width=200>Jum.Asuransi</th>
			 <th width=200>Omz.Asuransi</th>
			 <th width=200>Biaya Op.</th>
			 <th width=200>Biaya Tambahan</th>
			 <th width=200>Total Setor</th>
			 <th width=100>Act</th>
     </tr>
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>