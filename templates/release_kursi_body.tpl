<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }

    function Release(no_tiket, kursi) {
        new Ajax.Request("release_kursi.php?sid={SID}",{
            asynchronous: true,
            method: "post",
            parameters: "mode=release"+
            "&tiket="+no_tiket+
            "&kursi="+kursi,
            onLoading: function(request){
                progressbar.show();
            },
            onComplete: function(request){
                console.log(request.responseText);
                if(request.responseText == 1){
                    alert("Kursi Berhasil di Release");
                    document.forms["formcari"].submit();
                }else{
                    alert("Terjadi Kesalahan");
                }
            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post" id="formcari">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr class='banner' height=40>
                        <td align='center' valign='middle' class="bannerjudul">&nbsp;Release Kursi</td>
                        <td align='right' valign='middle'>
                            <table>
                                <tr><td class='bannernormal'>
                                        &nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                                        &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                                        &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                                        <input type="submit" value="cari" />&nbsp;
                                    </td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <b>Tiket Online Belum Bayar Lebih Dari 2 Jam Belum Batal</b>
            <table class="border" width="100%">
                <tr>
                    <th>No</th>
                    <th>Berangkat</th>
                    <th>Pesan</th>
                    <th>No Tiket</th>
                    <th>Jadwal</th>
                    <th>Kursi</th>
                    <th>Penumpang</th>
                    <th>Action</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td align="center">{ROW.no}</td>
                    <td align="center">{ROW.tanggal}</td>
                    <td align="center">{ROW.pesan}</td>
                    <td align="center">{ROW.tiket}</td>
                    <td align="center">{ROW.jadwal}</td>
                    <td align="center">{ROW.kursi}</td>
                    <td align="center">{ROW.nama}</td>
                    <td align="center">
                        <a onclick="Release('{ROW.tiket}','{ROW.kursi}');">Release</a>
                    </td>
                </tr>
                <!-- END ROW -->
            </table>
            <b>Gagal Mutasi</b>
            <table class="border" width="100%">
                <tr>
                    <th>No</th>
                    <th>Berangkat</th>
                    <th>Pesan</th>
                    <th>No Tiket</th>
                    <th>Jadwal</th>
                    <th>Kursi</th>
                    <th>Penumpang</th>
                    <th>Action</th>
                </tr>
                <!-- BEGIN POSISI -->
                <tr class="{POSISI.odd}">
                    <td align="center">{POSISI.no}</td>
                    <td align="center">{POSISI.tanggal}</td>
                    <td align="center">{POSISI.pesan}</td>
                    <td align="center">{POSISI.tiket}</td>
                    <td align="center">{POSISI.jadwal}</td>
                    <td align="center">{POSISI.kursi}</td>
                    <td align="center">{POSISI.nama}</td>
                    <td align="center">
                        <a onclick="Release('{POSISI.tiket}','{POSISI.kursi}');">Release</a>
                    </td>
                </tr>
                <!-- END POSISI -->
            </table>
        </td>
    </tr>
</table>