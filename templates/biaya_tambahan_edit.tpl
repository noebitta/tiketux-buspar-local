<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

    function validateInput(){

        valid=true;

        Element.hide('mobil_invalid');
        Element.hide('jurusan_invalid');
        Element.hide('penerima_invalid');
        Element.hide('keterangan_invalid');

        no_polisi					= document.getElementById('mobil');
        jurusan						= document.getElementById('jurusan');
        penerima					= document.getElementById('penerima');
        keterangan		= document.getElementById('ket');

        if(no_polisi.value==''){
            valid=false;
            Element.show('mobil_invalid');
        }

        if(jurusan.value==''){
            valid=false;
            Element.show('jurusan_invalid');
        }

        if(penerima.value == ''){
            valid=false;
            Element.show('penerima_invalid');
        }

        if(keterangan.value == ''){
            valid=false;
            Element.show('keterangan_invalid');
        }



        if(valid){
            return true;
        }
        else{
            return false;
        }
    }
    function getUpdatePenerima(){

        if(katpenerima1.checked){
            kategori = katpenerima1.value;
        }else{
            kategori = katpenerima2.value;
        }
        new Ajax.Updater("rewritePenerima","biaya_tambahan.php?sid="+SID, {
            asynchronous: true,
            method: "get",

            parameters: "mode=KolomPenerima&kategori="+kategori,
            onLoading: function(request){
                document.getElementById('rewritePenerima').innerHTML = "";
            },
            onComplete: function(request){
                Element.show('rewritePenerima');
                //Element.hide('progress_asal');
            },
            onFailure: function(request){
                assignError(request.responseText);
            }
        });

    }
</script>

<form action="{URL}" method="post" onSubmit='return validateInput()';>
    <input type="hidden" value="{MODE}" name="mode">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr class='banner' height=40>
            <td align='center' valign='middle' class="bannerjudul">&nbsp;Edit Biaya Tambahan</td>
        </tr>
        <tr>
            <td class="whiter" valign="middle" align="center">
                <table width='1000'>
                    <tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
                    <tr>
                        <td align='center' valign='top' width='500'>
                            <table width='500'>
                                <tr>
                                    <td colspan=3><h2>Tambah Biaya Tambahan</h2></td>
                                </tr>
                                <tr>
                                    <td width='200'><u>Mobil</u></td><td width='5'>:</td>
                                    <td width='300'>
                                        <select id='mobil' name='mobil'>{MOBIL}</select>
                                        <span id='mobil_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width='200'><u>Jurusan</u></td><td width='5'>:</td>
                                    <td width='300'>
                                        <select id="jurusan" name="jurusan">{JURUSAN}</select>
                                        <span id='jurusan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width='200'><u>Penerima</u></td><td width='5'>:</td>
                                    <td width='300'>
                                        <input type="radio" value="1" name="katpenerima" id="katpenerima1" onChange="getUpdatePenerima()" {CEK}>Sopir
                                        <input type="radio" value="2" name="katpenerima" id="katpenerima2" onChange="getUpdatePenerima()">Mekanik
                                        <input type="radio" value="3" name="katpenerima" id="katpenerima3" onChange="getUpdatePenerima()" {CEK2}>Operasional
                                        <!--
                                        <select id='penerima' name='penerima'>{SUPIR}</select>
                                        <span id='penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                        -->
                                    </td>
                                </tr>
                                <tr id="rewritePenerima">
                                    {PENERIMA}
                                </tr>
                                <!-- BEGIN ROWJENIS -->
                                {ROWJENIS.JENIS}
                                <!-- END ROWJENIS -->
                                <tr>
                                    <td width="200">Keterangan</td><td width="5">:</td>
                                    <td>
                                        <textarea name="ket" id="ket" rows="4">{KETERANGAN}</textarea>
                                        <span id='keterangan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=3 align='center' valign='middle' height=40>
                            <input type="hidden" name="mode" value="{MODE}">
                            <input type="hidden" name="submode" value="{SUB}">
                            <input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
<script language="javascript">
    //getUpdatePenerima();
</script>
