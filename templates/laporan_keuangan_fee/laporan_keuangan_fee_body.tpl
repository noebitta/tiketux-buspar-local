<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function closeRekon(tgl,total_fee){
		
		new Ajax.Request("laporan_keuangan_fee.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=closerekon&total_fee="+total_fee+"&tgl_transaksi="+tgl,
	   onLoading: function(request) 
	   {
				loading.show();
	   },
	   onComplete: function(request) 
	   {
				
	   },
	   onSuccess: function(request) 
	   {
			window.location.reload();
		 },
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function bayarRekon(id,tgl,total_fee){
		
		new Ajax.Request("laporan_keuangan_fee.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=bayarrekon&id="+id+"&tgl_transaksi="+tgl+"&total_fee="+total_fee,
	   onLoading: function(request) 
	   {
				loading.show();
	   },
	   onComplete: function(request) 
	   {
				
	   },
	   onSuccess: function(request) 
	   {
			window.location.reload();
		 },
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function bayarRekonAll(){
	
	if(confirm("Anda akan mengubah status rekon, klik 'OK' untuk melanjutkan atau 'Cancel' untuk membatalkan")){
		
		loop=true;
		list_dipilih="";
		for(i=1;i<={JUMLAH_HARI};i++){
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				if(chk.checked){
					if(list_dipilih==""){
						list_dipilih +=chk.value;
					}
					else{
						list_dipilih +=","+chk.value;
					}
				}
			}
		};
		
		new Ajax.Request("laporan_keuangan_fee.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=bayarrekonall&id="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {		
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function checkAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		for(i=1;i<={JUMLAH_HARI};i++){
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
		}
		
}

function uncheckAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		for(i=1;i<={JUMLAH_HARI};i++){
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
		}
		
}

function init(e) {
  // inisialisasi variabel
	
	//control dialog loading
	loading = dojo.widget.byId("loading");
	
}

dojo.addOnLoad(init);
</script>

<!--dialog Loading-->
<div dojoType="dialog" id="loading" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<br>
<table>
	<tr>
		<td align="center" valign="middle"><img src="{TPL}/images/loading2.gif"><br></td>
		<td align="center" valign="middle"><font color="white">Silahkan tunggu, sistem sedang memproses permintaan anda...</font> </td>
	</tr>
</table>
<br>
</div>
<!--END dialog Loading-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr height=40 class='banner'>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Fee Transaksi</td>
				<td class="bannerjudul">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td valign="bottom">
								<a href="{U_LAPORAN_OMZET_GRAFIK}"><img src="{TPL}images/icon_grafik.png" /></a>
								<a href="{U_LAPORAN_OMZET_GRAFIK}">Lihat Grafik</a>
							</td>
							<td>&nbsp;&nbsp;</td>
							<td valign="bottom">
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<br>
		<table width="100%" cellspacing="0" cellpadding="0" >
			<tr>
				<td align='right' colspan="2">
					<b>Total Fee Terhutang: Rp. {JUMLAH_REKON_TERHUTANG}</b>&nbsp;|
					&nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size=10 maxlength=4 />&nbsp										
				</td>
			</tr>
			<tr><td align='left'><a href='#' onClick='bayarRekonAll();false;'>Rekon Lunas</a></td><td align="right">{LIST_BULAN}</td></tr>
		</table>
		
		<table class="border" width="100%" >
    <tr>
       <th width='10'><input type='checkbox' id='checkall' name='checkall' onClick="if(this.checked){checkAll();}else{uncheckAll();}" /></th>
			 <th width="150">Tanggal</th>
			 <th width="150">Trip</th>
			 <th width="150">Tiket</th>
			 <th width="150">Tiket Batal</th>
			 <th width="150">Tiket Online</th>
			 <th width="150">Fee Tiket</th>
			 <th width="150">SMS</th>
			 <th width="150">Fee SMS</th>
			 <th width="150">Total Fee</th>
			 <th width="150">Status Rekon</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}" onClick="document.getElementById('checked_'+{ROW.idx_tgl}).checked=!document.getElementById('checked_'+{ROW.idx_tgl}).checked;">
			 <td><div align="center">{ROW.check}</div></td>
       <td title='Tanggal'><div align="center"><font size=3 color='{ROW.font_color}'><b>{ROW.tgl}</b></font></div></td>
			 <td title='Jumlah Trip'><div align="right">{ROW.spj}</div></td>
			 <td title='Jumlah Tiket'><div align="right">{ROW.tiket}</div></td>
			 <td title='Jumlah Tiket Batal'><div align="right">{ROW.tiket_batal}</div></td>
			 <td title='Fee Tiket'><div align="right">{ROW.tiket_online}</div></td>
			 <td title='Fee Tiket'><div align="right">{ROW.total_fee_tiket}</div></td>
			 <td title='Jumlah SMS'><div align="right">{ROW.total_sms}</div></td>
			 <td title='Fee SMS'><div align="right">{ROW.fee_sms}</div></td>
			 <td title='Total Fee'><div align="right">{ROW.total_fee}</div></td>
			 <td title='Status Rekon'><div align="center">{ROW.status_rekon}</div></td>
     </tr>
     <!-- END ROW -->
		 <tr bgcolor='ffff00'>
			 <td>&nbsp;</td>
       <td ><div align="center"><font size=3 color='{ROW.font_color}'><b>TOTAL</b></font></div></td>
			 <td ><div align="right"><b>{SUM_SPJ}</b></div></td>
			 <td ><div align="right"><b>{SUM_TIKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_TIKET_BATAL}</b></div></td>
			 <td ><div align="right"><b>{SUM_TIKET_ONLINE}</b></div></td>
			 <td ><div align="right"><b>{SUM_FEE_TIKET}</b></div></td>
			 <td ><div align="right"><b>{SUM_SMS}</b></div></td>
			 <td ><div align="right"><b>{SUM_SMS_FEE}</b></div></td>
			 <td ><div align="right"><b>{SUM_TOTAL_FEE}</b></div></td>
			 <td ><div align="right">&nbsp;</div></td>
     </tr>
    </table>
	</form>
 </td>
</tr>
</table>