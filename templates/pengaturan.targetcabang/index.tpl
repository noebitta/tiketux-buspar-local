<script language="JavaScript">

    function reset(){

        if(!confirm("Anda akan mereset target Counter Bulan ["+bulan.options[bulan.selectedIndex].text+" "+tahun.options[tahun.selectedIndex].text+"]! Tekan OK untuk melanjutkan!")){
            exit;
        }

        new Ajax.Request("pengaturan.targetcounter.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "post",
                    parameters:
                    "&mode=reset"+
                    "&bulan="+bulan.value+
                    "&tahun="+tahun.value,
                    onLoading: function(request){
                    },
                    onComplete: function(request){
                    },
                    onSuccess: function(request){
                        popuploading.show();
                        window.location.reload();
                    },
                    onFailure: function(request){
                        alert('Error !!! Cannot Save');
                        assignError(request.responseText);
                    }
                })

    }

    dojo.require("dojo.widget.Dialog");

    function init(e) {
        // inisialisasi variabel
        popuploading				= dojo.widget.byId("popuploading");
    }

    dojo.addOnLoad(init);

</script>

<form id="formsubmit" action="{ACTION_CARI}" method="post" onsubmit="popuploading.show();">
    <input type="hidden" name="page" value="{PAGE}"/>
    <input type="hidden" name="mode" value="{MODE}" />

    <div class="banner">
        <div class="bannerjudul">
            <div style="float: left;">
                Pengaturan Target Counter
            </div>
            <div class="bannernormal" style="float: right;padding-top: 5px;">
                Bulan:&nbsp;
                <select id="bulan" name="bulan" onChange="popuploading.show();form.submit();">
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                </select>&nbsp;
                Tahun:&nbsp; <select id="tahun" name="tahun" onChange="popuploading.show();form.submit();">
                    <!-- BEGIN TAHUN -->
                    <option value="{TAHUN.value}">{TAHUN.value}</option>
                    <!-- END TAHUN -->
                </select>&nbsp;
                Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" size=50 />&nbsp;<input type="submit" value="cari" />&nbsp;
            </div>
        </div>
    </div>
    <br>
    <div align="center">
        <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
    </div>
    <br>
    <div style="width: 100%;padding-bottom: 3px;">
        <div style="float: left;">
            <a href="#" onclick="popuploading.show();formsubmit.submit();">Simpan</a>&nbsp;|&nbsp;
            <a href="#" onClick="reset();">Reset</a>
        </div>
    </div>
    <br>

    <table width='100%' class="border">
        <tr>
            <th width=30>No</th>
            <th width=50>Kode Cabang</th>
            <th width=100>Cabang</th>
            <th width=100>Kota</th>
            <th width=80>Target Pax</th>
            <th width=50>Real Pax</th>
            <th width=50>Capaian Pax</th>
            <th width=80>Target Paket</th>
            <th width=50>Real Paket</th>
            <th width=50>Capaian Paket</th>
        </tr>
        <!-- BEGIN ROW -->
        <tr class="{ROW.odd}">
            <td align="center">{ROW.no}</td>
            <td align="center">{ROW.KodeCabang}</td>
            <td align="center">{ROW.Cabang}</td>
            <td align="center">{ROW.Kota}</td>
            <td align="center">
                <input type="hidden" name="KodeCabang{ROW.no}" value="{ROW.KodeCabang}"/>
                <input type="hidden" name="targetbeforepax{ROW.no}" value="{ROW.targetpax}"/>
                <input type="text" name="targetinputpax{ROW.no}" value='{ROW.targetpax}' style="text-align: right;" size="10" maxlength="4"/>
            </td>
            <td align="center">{ROW.realpax}</td>
            <td align="center">{ROW.capaianpax}%</td>
            <td align="center">
               <input type="hidden" name="targetbeforepaket{ROW.no}" value="{ROW.targetpaket}">
                <input type="text" name="targetinputpaket{ROW.no}" value="{ROW.targetpaket}" style="..." size="10" maxlength="4">
            </td>
            <td align="center">{ROW.realpaket}</td>
            <td align="center">{ROW.capaianpaket}%</td>
        </tr>
        <!-- END ROW -->
    </table>
    <!-- BEGIN NO_DATA -->
    <div style="background-color: yellow;font-size: 16px;">tidak ada ditemukan</div>
    <!-- END NO_DATA -->

    <div style="width: 100%;">
        <div style="float: left;">
            <a href="#" onclick="popuploading.show();formsubmit.submit();">Simpan</a>&nbsp;|&nbsp;
            <a href="#" onClick="reset();">Reset</a>
        </div>
    </div>
</form>

<script language="JavaScript">
    bulan.value={BULAN};
    tahun.value={TAHUN};
</script>
