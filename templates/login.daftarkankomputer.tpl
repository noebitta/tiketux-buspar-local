<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css">
    <link rel="icon" type="image/ico" href="favicon.ico">
    {META}
    <title>{SITENAME} :: Daftarkan Komputer</title>
    <link rel="stylesheet" href="./templates/trav.css" type="text/css" />
  </head>
  <body id="loginbody">
    <!-- COMBO LIST -->
    <div id="combolistcontainer" style="display:none;z-index: 9;"></div>
    <div align="center">
      <div id="loginwrapper">
        <form class="loginform" name="formdaftar" id="formdaftar" action="login.daftarkankomputer.php" method="post" style="height: 430px;">
          <input type="hidden" id="hostuuid" name="hostuuid" value=""/>
          <input type="hidden" id="cabang" name="cabang" value=""/>
          <input type="hidden" id="mode" name="mode" value=""/>
          <input type="hidden" id="user" name="user" value="{USER}"/>
          <input type="hidden" id="token" name="token" value="{TOKEN}"/>
          <p class="logintitle">
            <span><img src="./templates/images/login/logomitra.png" style="max-width: 200px;"/></span><br>
            <img src="./templates/images/login/logotiketux.png" style="max-width: 100px;"/>
          </p>
          Komputer anda belum terdaftar! Agar dapat mengakses sistem ini, silahkan daftarkan komputer anda.
          <span style="height:60px;width:100%;display: inline-block;"><input id="namakomputer" name="namakomputer" size="30" value=""  placeholder='nama komputer'  autofocus="" type="text" onfocus="this.style.background='white'" style="text-transform: uppercase;" onkeypress="validCharInput(event);"/></span><br>
          CABANG <span id="selcabang" class="combolistselection" onClick="setComboList(this,'cabang','login.daftarkankomputer.php','mode=1&user={USER}&token={TOKEN}',false,true);">&nbsp;</span><br><br>
          <input id="loginbutton" name="loginbutton" class="flatbutton" value="DAFTARKAN  " type="button" onclick="proses();"/><br><br>
          <span class="flatbutton" onclick="window.location='.';">BATAL</span>
        </form>
      </div>
    </div>
  </body>
  <script type="text/javascript" src="./templates/js/fingerprint.js"></script>
  <script type="text/javascript">
    var fp = new Fingerprint2();
    fp.get(function(result, components){document.getElementById("hostuuid").value = result;});
  </script>
  <script type="text/javascript" src="./ajax/lib/prototype.js"></script>
  <script type="text/javascript" src="./templates/js/main.js"></script>
  <script type="text/javascript">
    function proses(){
      var valid=true;

      if(!validasiInput(namakomputer)){
        return false;
      }

      if(cabang.value==""){
        valid = false;
        alert("Anda harus memilih cabang dari komputer ini berada");
        return false;
      }

      if(valid){
        mode.value = 2;
        formdaftar.submit();
      }

    }
  </script>
</html>
