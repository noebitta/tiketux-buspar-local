<script type="text/javascript">

  function antarPaket(){

    if(!validasiInput(namakurir)){
      return false;
    }

    if(confirm("Anda akan melakukan proses pengantaran paket, klik OK untuk melanjutkan proses ini!")){

      var idxrow=1;
      var loop=true;
      var listresi="";

      var tabledata = document.getElementById("tablecontent");

      do{
        elmchk = document.getElementById("chkrow"+idxrow);

        if(elmchk){
          if(elmchk.value==1){
            noresi    = "'"+tabledata.rows[idxrow-1].cells[2].innerHTML+"'";
            listresi  += listresi!=""?","+noresi:noresi;
          }
        }
        else{
          loop=false;
        }

        idxrow++;
      }while(loop);

      if(listresi==""){
        alert("Anda belum memilih barang-barang yang akan diantar!");
        return false;
      }

      new Ajax.Request("paket.antaran.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=3&listresi="+listresi+"&namakurir="+namakurir.value,
        onLoading: function(request){
        },
        onComplete: function(request){
        },
        onSuccess: function(request){

          if(isJson(request.responseText)) {
            var result;
            result = JSON.parse(request.responseText);

            if (result["status"] == "OK") {
              window.location.reload();
            }
            else {
              alert("Gagal:" + result["pesan"]);
            }
          }
          else{
            alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
          }
        },
        onFailure: function(request){
          alert('Error !!! Cannot Save');
          assignError(request.responseText);
        }
      });

    }

  }

  function selectRow(elmrow,idrow){

    elmchk      = document.getElementById("chkrow"+idrow);
    elmshowchk  = document.getElementById("showchkrow"+idrow);

    elmchk.value= 1-elmchk.value;

    if(elmchk.value==0){
      elmshowchk.className  = "b_chk";
      elmrow.className      = ((elmrow.cells[1].innerHTML*1)%2==0?"even":"odd");
    }
    else{
      elmshowchk.className  = "b_ok";
      elmrow.className      = "yellowcell";
    }

  }

  function showDialogKurir(){

    setPopup(500,150);

    new Ajax.Updater("popupcontent","paket.antaran.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2",
      onLoading: function(request) {
      },
      onComplete: function(request) {
        document.getElementById("resipaketscan").focus();
      },
      onSuccess: function(request) {
        popupwrapper.show();
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function init(e){
     popupwrapper	= dojo.widget.byId("popupcontainer");

    setComboList(document.getElementById("seltujuan"),"tujuan","paket.checkin.php","mode=1",true,false);
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="{MODE}">
  <input type="hidden" id="tujuan" name="tujuan" value="{FIL_TUJUAN}">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Daftar Antaran Paket</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Tujuan:<span id="seltujuan" class="combolistselection" onClick="setComboList(this,'tujuan','paket.checkin.php','mode=1',true,true);">&nbsp;</span>
      Periode Berangkat:<input readonly="yes" class="drop_calendar" id="filtglawal" name="filtglawal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
      s.d.<input readonly="yes" class="drop_calendar" id="filtglakhir" name="filtglakhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
      Cari:<input type="text" id="cari" name="cari" value="{CARI}" onkeypress="validCharInput(event);" placeholder="resi/telp/alamat"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
   <!-- CRUD -->
    <span class="flatbutton" onClick="showDialogKurir();">ANTAR BARANG</span>
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>
<br><br>
<hr>
<div class="tabletitle">
  Daftar Antaran Paket<br>
  <span style="display: inline-block;float: left;padding-left: 10px;font-size: 12px;text-transform: none;"><span style="display: inline-block;width:20px; height: 10px;" class="greencell"></span>:Dalam Pengantaran</span>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th rowspan="2"></th>
      <th rowspan="2">No</th>
      <th rowspan="2" onclick="setOrder(0)">Resi</th>
      <th rowspan="2" onclick="setOrder(1)">Tgl.Berangkat</th>
      <th rowspan="2" onclick="setOrder(2)">Kode Jadwal</th>
      <th rowspan="2" onclick="setOrder(3)">Tujuan Akhir</th>
      <th rowspan="2" onclick="setOrder(4)">Status</th>
      <th colspan="3">Pengirim</th>
      <th colspan="3">Penerima</th>
      <th colspan="3">Barang</th>
      <th rowspan="2">Remark</th>
    </tr>
    <tr>
      <th onclick="setOrder(5)">Nama</th>
      <th onclick="setOrder(6)">Telp</th>
      <th onclick="setOrder(7)">Alamat</th>
      <th onclick="setOrder(8)">Nama</th>
      <th onclick="setOrder(9)">Telp</th>
      <th onclick="setOrder(10)">Alamat</th>
      <th onclick="setOrder(11)">Berat</th>
      <th onclick="setOrder(12)">Layanan</th>
      <th onclick="setOrder(13)">Keterangan</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.classrow}" onclick="{ROW.act};return false;" style="cursor: pointer;">
        <td align="center">
          <span id="showchkrow{ROW.no}" class="{ROW.classchk}"><input type="hidden" id="chkrow{ROW.no}" value="0"/></span></td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.noresi}</td>
        <td align="center">{ROW.tglberangkat}</td>
        <td align="center">{ROW.kodejadwal}</td>
        <td align="center">{ROW.tujuanakhir}</td>
        <td align="center">{ROW.status}</td>
        <td align="center">{ROW.namapengirim}</td>
        <td align="center">{ROW.telppengirim}</td>
        <td align="center">{ROW.alamatpengirim}</td>
        <td align="center">{ROW.namapenerima}</td>
        <td align="center">{ROW.telppenerima}</td>
        <td align="center">{ROW.alamatpenerima}</td>
        <td align="center">{ROW.berat} Kg</td>
        <td align="center">{ROW.layanan}</td>
        <td align="center">{ROW.keterangan}</td>
        <td align="center">{ROW.remark}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+1,sort.value,document.getElementById("tableheader"));
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>