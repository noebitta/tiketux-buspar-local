<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    function init(e){
        //control dialog
        dialog_biaya			= dojo.widget.byId("dialogbiayatambahan");
        dlg_biaya_cancel        = document.getElementById("dialogcancel");
    }
    dojo.addOnLoad(init);
    function ShowDialog(kodecabang,nama){
        dialog_biaya.show();
        document.getElementById("namacabang").textContent=nama;
        document.getElementById("kodecabang").value=kodecabang;
    }
    function InsertBiaya(){
        if(jumlah.value == ""){
            alert("Jumlah Biaya Tidak Boleh Kosong");
        }else{
            new Ajax.Request("petty_cash.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=InsertData"+
                "&kodecabang="+kodecabang.value+
                "&jumlah="+jumlah.value,
                onLoading: function(request){
                    progressbar.show();
                },
                onComplete: function(request){
                    progressbar.hide();
                    dialog_biaya.hide();
                    console.log("oncomplete");
                    document.getElementById("myForm").reset();
                },
                onSuccess: function(request){
                    eval(request.responseText);

                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }
    }
</script>
<!--dialog biaya tambahan-->
<div dojoType="dialog" id="dialogbiayatambahan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr><td colspan='2' align="center"><h2>TOP UP PETTY CASH</h2></td></tr>
                        <input type="hidden" name="mode" value="InsertData">
                        <input type="hidden" name="kodecabang" id="kodecabang">
                        <tr><td align='right' width="200">CABANG : </td><td width="300"><span id="namacabang"></span></tr>
                        <tr><td align="right">Jumlah : </td><td><input type="text" name="jumlah" id="jumlah"></td></tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogcancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_biaya.hide();">
                    <input type="button" onclick="InsertBiaya()"  id="dialogbiayatambahanproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog biaya tambahan-->

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr height=40 class='banner'>
                        <td align='center' valign='middle' class="bannerjudul">TOP UP PETTY CASH</td>
                        <form action="{ACTION_CARI}" method="post">
                        <td align="right" class="bannernormal">Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" size=50 />&nbsp;<input type="submit" value="cari" />&nbsp;</td>
                        </form>
                    </tr>
                </table>
                <!-- END HEADER-->
        </td>
        <!-- BODY -->
        <table class="border" width="100%">
            <br>
            <thead>
            <tr>
                <th width="30">No</th>
                <th width="50">KODE CABANG</th>
                <th width="100">CABANG</th>
                <th width="150">KOTA</th>
                <th width="150">SALDO</th>
                <th width="70">ACTION</th>
            </tr>
            </thead>
            <tbody>
            <!-- BEGIN ROW -->
            <tr class="even">
                <td align="center"><font size="2"> {ROW.no} </font></td>
                <td><font size="2"> {ROW.KodeCabang} </font></td>
                <td><font size="2"> {ROW.Cabang} </font></td>
                <td><font size="2"> {ROW.Kota} </font></td>
                <td align="center"><font size="2"> {ROW.Saldo} </font></td>
                <td align="center">
                    {ROW.topup}
                </td>
            </tr>
            <!-- END ROW -->
            </tbody>
        </table>
        <!--END BODY-->
    </tr>
</table>
<div style="height: 50px;"></div>