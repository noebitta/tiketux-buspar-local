<script language="JavaScript"  src="{TPL}js/stok_kardus.js"></script>

<form action="{ACTION_CARI}" method="post">
  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Pengaturan Role</div>
    <div class="headerfilter" style="padding-top: 7px;">&nbsp;</div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;">
    <!-- BEGIN TOMBOL_CRUD -->
    <a href="#" onclick="ShowJenisKardus();">Tambah Jenis Kardus</a>
    <!-- END TOMBOL_CRUD -->
  </div>
  <div style="float: right;padding-bottom: 2px;">
    {PAGING}
  </div>
</form>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th rowspan="2">No</th>
      <th rowspan="2">Kode Cabang</th>
      <th rowspan="2">Nama Cabang</th>
      <th rowspan="2">Kota</th>
      <th colspan="4">Stok</th>
      <th rowspan="2">Action</th>
    </tr>
    <tr>
      <th>S</th>
      <th>M</th>
      <th>L</th>
      <th>H</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"  >{ROW.no}</div></td>
        <td align="center" >{ROW.kodecabang}</div></td>
        <td align="center" >{ROW.namacabang}</div></td>
        <td align="center" >{ROW.kota}</div></td>
        {ROW.stok}
        <td align="center" >{ROW.action}</div></td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>

<!--dialog jenis kardus-->
<div dojoType="dialog" id="dialog_JenisKardus" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='500'>
		<tr>
			<td><h1>Master Jenis Kardus</h1></td>
		</tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="jenis_kardus"></div>
				<span id='progress_jenis_kardus' style='display:none;'><img src='{TPL}images/loading.gif' />
					<font size=2 color='ffffff'>sedang memproses...</font>
				</span>
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;"> &nbsp;
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog jenis kardus-->

<!--dialog tambah stok kardus-->
<div dojoType="dialog" id="dialog_TambahStok" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='500'>
		<tr><td><h1>Tambah Stok Kardus</h1></td></tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="tambah_stok_kardus"></div>
				<!--span id='progress_stok_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span-->
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog tambah stok kardus-->

<!--dialog stok opname kardus -->
<div dojoType="dialog" id="dialog_StokOpname" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='500'>
		<tr><td><h1>Stok Opname Kardus</h1></td></tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="stok_opname_kardus"></div>
				<span id='progress_opname_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog stok opname kardus -->

<!--dialog log history kardus -->
<div dojoType="dialog" id="dialog_LogHistory" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
	<form onsubmit="return false;">
		<table width='800'>
		<tr><td><h1>Log History</h1></td></tr>
		<tr>
			<td bgcolor='ffffff' height=300 valign='top' align='center'>
				<div id="log_history_kardus"></div>
				<span id='progress_log_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
			</td>
		</tr>
		<tr>
		   <td colspan="2" align="center">
				<br>
				<input type="button" onClick="window.location.reload();" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
			 </td>
		</tr>
		</table>
	</form>
</div>
<!--END dialog log history kardus -->


