<span class="popuptitle">{OPERATION} Halaman {PAGE_ID}</span>
<center>
  <div style="width: 400px;">
    <input type="hidden" id="idold" name="idold" value="{PAGE_ID}" />
    <span class="popuplabel">Id Halaman</span><span class="popupinput"><input type="text" id="inputpageid" name="inputpageid" value="{PAGE_ID}" onfocus="this.style.background='white';" {READONLY} onkeypress="validasiInputanAngka(event);" /></span><br><br>
    <span class="popuplabel">Nama Halaman</span><span class="popupinput"><input type="text" id="inputnamapage" name="inputnamapage" value="{NAMA_PAGE}" onfocus="this.style.background='white';" {READONLY}  onkeypress="validCharInput(event);" /></span><br><br>
    <span class="popuplabel">Nama Ditampilkan</span><span class="popupinput"><input type="text" id="inputnamatampil" name="inputnamatampil" value="{NAMA_TAMPIL}" onfocus="this.style.background='white';" {READONLY} onkeypress="validCharInput(event);"/></span><br><br>
    <span class="popuplabel">Group</span><span class="popupinput"><input type="text" id="inputgroup" name="inputgroup" value="{GROUP}" onfocus="this.style.background='white';" {READONLY} onkeypress="validCharInput(event);"/></span><br><br>
    <span class="popuplabel">Urutan</span><span class="popupinput"><input type="text" id="inputurutan" name="inputurutan" value="{URUTAN}" onfocus="this.style.background='white';" {READONLY} onkeypress="validasiInputanAngka(event);"/></span><br><br>
    <span class="popuplabel">Nama File</span><span class="popupinput"><input type="text" id="inputnamafile" name="inputnamafile" value="{NAMA_FILE}" onfocus="this.style.background='white';" {READONLY} onkeypress="validCharInput(event);"/></span><br><br>
    <span class="popuplabel">Image Icon</span><span class="popupinput"><input type="text" id="inputicon" name="inputicon" value="{ICON}" onfocus="this.style.background='white';" {READONLY} onkeypress="validCharInput(event);"/></span><br><br>
    <span class="popuplabel">Penampilan</span><span class="popupinput"><select id="inputpenampilan" name="inputpenampilan"><option value="0" {TAMPIL_SEL0}>Tampil</option><option value="1" {TAMPIL_SEL1}>Sembunyi</option></select></span><br><br>
    <span class="flatbutton" onclick="simpan();" id="buttonsave">SIMPAN</span>
  </div>
</center>