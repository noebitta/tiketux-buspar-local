<script type="text/javascript">

  function selectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;

      if(chk=document.getElementById(str_var)){
        chk.checked=true;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function deselectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;
      if(chk=document.getElementById(str_var)){
        chk.checked=false;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function simpan(){

    var inputvalid=true;

    inputvalid=validasiInput(inputpageid) && inputvalid;
    inputvalid=validasiInput(inputnamapage) && inputvalid;
    inputvalid=validasiInput(inputnamatampil) && inputvalid;
    inputvalid=validasiInput(inputurutan) && inputvalid;
    inputvalid=validasiInput(inputnamafile) && inputvalid;

    if(!inputvalid){
      return;
    }

    new Ajax.Request("pengaturan.halaman.php", {
        asynchronous: true,
        method: "post",
        parameters: "mode=3&id="+inputpageid.value+
          "&idold="+idold.value+
          "&namapage="+inputnamapage.value+
          "&namatampil="+inputnamatampil.value+
          "&group="+inputgroup.value+
          "&urutan="+inputurutan.value+
          "&namafile="+inputnamafile.value+
          "&icon="+inputicon.value+
          "&ishide="+inputpenampilan.value,
        onLoading: function(request){

        },
        onComplete: function(request){

        },
        onSuccess: function(request){
          var result;

          result = JSON.parse(request.responseText);

          if(result["status"]=="OK") {
            formdata.submit();
          }
          else{
            alert("Gagal:"+result["error"]);
          }

        },
        onFailure: function(request){
          alert('Error');
          assignError(request.responseText);
        }
      });
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }


  function tambahData(){
    setPopup(500,350);

    new Ajax.Updater("popupcontent","pengaturan.halaman.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1",
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function ubahData(id){
    setPopup(500,350);

    new Ajax.Updater("popupcontent","pengaturan.halaman.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2&id="+id,
      onLoading: function(request) {
        id.value  = id;
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function hapusData(id=''){

    if(id!=''){
      list_dipilih=id;
    }
    else{
      i=1;
      loop=true;
      list_dipilih="";
      do{
        str_var='checked_'+i;

        if(chk=document.getElementById(str_var)){
          if(chk.checked){
            if(list_dipilih==""){
              list_dipilih +=chk.value;
            }
            else{
              list_dipilih +=","+chk.value;
            }
          }
        }
        else{
          loop=false;
        }
        i++;
      }while(loop);
    }

    if(list_dipilih==""){
      alert("Anda belum memilih data yang akan dihapus!");
      return false;
    }

    if(confirm("Apakah anda yakin akan menghapus data ini?")){

      new Ajax.Request("pengaturan.halaman.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=4&id="+list_dipilih,
        onLoading: function(request)
        {
        },
        onComplete: function(request)
        {

        },
        onSuccess: function(request)
        {
          window.location.reload();
          deselectAll();
        },
        onFailure: function(request)
        {
        }
      })
    }

    return false;

  }

  function init(e){
     popupwrapper	= dojo.widget.byId("popupcontainer");
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Halaman</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="tambahData();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->
    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="return hapusData();" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  Data Halaman<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th></th>
      <th>No</th>
      <th onclick="setOrder(0)">Id Halaman</th>
      <th onclick="setOrder(1)">Nama Halaman</th>
      <th onclick="setOrder(2)">Nama Ditampilkan</th>
      <th onclick="setOrder(3)">Group</th>
      <th onclick="setOrder(4)">Urutan</th>
      <th onclick="setOrder(5)">Nama File</th>
      <th onclick="setOrder(6)">Icon</th>
      <th onclick="setOrder(7)">Penampilan</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"><input type="checkbox" id="checked_{ROW.idx}" name="checked_{ROW.pageid}" value="{ROW.pageid}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="ubahData('{ROW.pageid}');" title="ubah data" ></span>&nbsp;
          <!-- END ACT_EDIT -->

          <!-- BEGIN ACT_DEL -->
          <span class="b_delete" onclick="hapusData('{ROW.pageid}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.pageid}</td>
        <td align="center">{ROW.namapage}</td>
        <td align="center">{ROW.namatampil}</td>
        <td align="center">{ROW.grouppage}</td>
        <td align="center">{ROW.urutan}</td>
        <td align="center">{ROW.namafile}</td>
        <td align="center">{ROW.imageicon}</td>
        <td align="center"><a href="" onclick="hideUnhide({ROW.pageid});return false;" class="crud" title="klik untuk {ROW.titlepenampilan}">{ROW.penampilan}</a></td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+3,sort.value,document.getElementById("tableheader"))
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>