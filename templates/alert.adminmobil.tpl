<!-- BEGIN alert_tilang -->
<span align="center" style="background-color: yellow;padding:5px;font-family: 'trebuchet ms';position: fixed; height:20px; left:0px; bottom:0px;display: inline-block;z-index: 1;">
  Minggu ini terdapat sidang tilang. <a href="{alert_tilang.url}">Klik untuk melihat detail</a>
</span>
<br>
<!-- END alert_tilang -->

<!-- BEGIN alert_pajak -->
<span align="center" style="background-color: yellow;padding:5px;font-family: 'trebuchet ms';position: fixed; height:20px; right:0px; bottom:0px;display: inline-block;z-index: 1;">
  Minggu ini terdapat jatuh tempo PAJAK / STNK / KIR. <a href="{alert_pajak.url}">Klik untuk melihat detail</a>
</span>
<br>
<!-- END alert_pajak -->
