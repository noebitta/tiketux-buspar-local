<script type="text/javascript">
  filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">

  function getOTPCetak(nomanifest,idxelement){

    new Ajax.Request("paket.listotprequestcetakmanifest.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1"+
      "&nomanifest="+nomanifest,
      onLoading: function(request){
      },
      onComplete: function(request){
      },
      onSuccess: function(request){

        var result;
        result = JSON.parse(request.responseText);

        if(result["status"]=="OK"){
          document.getElementById("otp"+idxelement).innerHTML=result["otp"];
          document.getElementById("approver"+idxelement).innerHTML=result["approver"];
          document.getElementById("waktuapprove"+idxelement).innerHTML=result["waktuapprove"];
          setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
        }
        else{
          alert("Gagal:"+result["pesan"]);
        }
      },
      onFailure: function(request){
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });
  }


  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function init(e){
     popupwrapper	= dojo.widget.byId("popupcontainer");
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">List Request OTP Cetak Ulang Manifest</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Periode:<input readonly="yes" class="drop_calendar" id="filtglawal" name="filtglawal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
      s.d.<input readonly="yes" class="drop_calendar" id="filtglakhir" name="filtglakhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
   <!-- CRUD -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  List Request<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th>No</th>
      <th onclick="setOrder(0)">No.Manifest</th>
      <th onclick="setOrder(1)">Tgl.Berangkat</th>
      <th onclick="setOrder(2)">Kode Jadwal</th>
      <th onclick="setOrder(3)">Jam Berangkat</th>
      <th onclick="setOrder(4)">Kode Unit</th>
      <th onclick="setOrder(5)">No.Plat</th>
      <th onclick="setOrder(6)">Kode Driver</th>
      <th onclick="setOrder(7)">Driver</th>
      <th onclick="setOrder(8)">CSO Requester</th>
      <th onclick="setOrder(9)">Waktu Request</th>
      <th onclick="setOrder(10)">Alasan</th>
      <th onclick="setOrder(11)">OTP</th>
      <th onclick="setOrder(12)">Otorisasi Oleh</th>
      <th onclick="setOrder(13)">Waktu Otorisasi</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.nomanifest}</td>
        <td align="center">{ROW.tblberangkat}</td>
        <td align="center">{ROW.kodejadwal}</td>
        <td align="center">{ROW.jamberangkat}</td>
        <td align="center">{ROW.kodeunit}</td>
        <td align="center">{ROW.noplat}</td>
        <td align="center">{ROW.kodedriver}</td>
        <td align="center">{ROW.driver}</td>
        <td align="center">{ROW.csorequester}</td>
        <td align="center">{ROW.wakturequest}</td>
        <td align="center">{ROW.alasan}</td>
        <td align="center">
          <span id="otp{ROW.no}"}>
            <!-- BEGIN ACT_GETOTP -->
            <a href="" onclick="getOTPCetak('{ROW.nomanifest}',{ROW.no});return false;" class="crud">&nbsp;get OTP&nbsp;</a>
            <!-- END ACT_GETOTP -->
            {ROW.otp}
          </span>
        </td>
        <td align="center"><span id="approver{ROW.no}"}>{ROW.otorisasioleh}</span></td>
        <td align="center"><span id="waktuapprove{ROW.no}"}>{ROW.waktuotorisasi}</span></td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+1,sort.value,document.getElementById("tableheader"));
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>