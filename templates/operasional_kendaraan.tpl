<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr class='banner' height=40>
                        <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Operasional Kendaraan</td>
                        <td align='right' valign='middle'>
                            <table>
                                <tr>
                                    <td class='bannernormal'>
                                        Bulan:&nbsp;
                                        <select id="bulan" name="bulan">
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>&nbsp;
                                        &nbsp;Tahun:&nbsp;<input type="text" id="tahun" name="tahun" value="{TAHUN}" size="4" maxlength="4"/>&nbsp;
                                        &nbsp;Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" />&nbsp;
                                        <input type="submit" value="cari" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <script type="text/javascript">
                                document.getElementById("bulan").value = {BULAN};
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=2 align='center' valign='middle'>
                            <table>
                                <tr>
                                    <td>
                                        <a href='#' onClick="{CETAK}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=2>
                            <table width='100%'>
                                <tr>
                                    <td align='right' valign='bottom'>
                                        {PAGING}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- END HEADER-->
                <table class="border" width='100%' >
                    <tr>
                        <th width=30>No</th>
                        <th>No Polisi</th>
                        <th>Kode Kendaraan</th>
                        <th>Jumlah Kursi</th>
                        <th>Kilometer</th>
                        <th>BBM</th>
                        <th>Biaya</th>
                    </tr>
                    <!-- BEGIN ROW -->
                    <tr class="{ROW.odd}">
                        <td ><div align="right">{ROW.no}</div></td>
                        <td ><div align="left"><font size=5>{ROW.no_polisi}</font></div></td>
                        <td ><div align="left"><font size=5>{ROW.kode_kendaraan}</font></div></td>
                        <td ><div align="center"><font size=5>{ROW.kursi}</font></div></td>
                        <td ><div align="rifht"><font size=5>{ROW.KM} </font></div></td>
                        <td ><div align="rifht"><font size=5>{ROW.LITER} liter</font></div></td>
                        <td ><div align="right"><font size=5>Rp. {ROW.BIAYA}</font></div></td>
                    </tr>
                    <!-- END ROW -->
                </table>
                <table width='100%' >
                    <tr>
                        <td align='right' width='100%' colspan=3>
                            {PAGING}
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>