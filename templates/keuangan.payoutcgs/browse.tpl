<span class="popuptitle">{DRIVER}</span>
<center>
  <div class="tabletitle">Rekap Manifest</div>
  <div class="wrappertabledata" style="height: 360px;">
    <table class="flat" id="tableheaderbrowse">
      <tr>
        <th>No</th>
        <th>Tgl.Berangkat</th>
        <th>Jam Berangkat</th>
        <th>Asal</th>
        <th>Tujuan</span></th>
        <th>#Jadwal</th>
        <th>#Unit</th>
        <th>Waktu Cetak</th>
      </tr>
    </table>
    <div class="wrapperinnertable" id="wrappertablecontentbrowse" style="height: 330px;">
      <table class="flat" id="tablecontentbrowse">
        <!-- BEGIN ROW -->
        <tr class="{ROW.odd}">
          <td align="center">{ROW.no}</td>
          <td align="center">{ROW.tglberangkat}</td>
          <td align="center">{ROW.jamberangkat}</td>
          <td align="center">{ROW.cabangasal}</td>
          <td align="center">{ROW.cabangtujuan}</td>
          <td align="center">{ROW.kodejadwal}</td>
          <td align="center">{ROW.nopol}</td>
          <td align="center">{ROW.waktucetak}</td>
        </tr>
        <!-- END ROW -->
      </table>
    </div>
  </div>

</center>