<script type="text/javascript">

  function prosesPayout(kodesopir,jenisbayar){

    popupwrapper.hide();

    new Ajax.Request("keuangan.payoutcgs.payout.php", {
        asynchronous: true,
        method: "post",
        parameters: "mode=1&siklus="+siklus.value+"&bulan="+bulan.value+"&tahun="+tahun.value+"&jenisbayar="+jenisbayar+"&kodesopir="+kodesopir,
        onLoading: function(request){
          showLoading();
        },
        onComplete: function(request){

        },
        onSuccess: function(request){
          var result;
          result = JSON.parse(request.responseText);

          if(result["status"]=="OK"){
            cetakResi(result["id"]);
          }
          else{
            alert("Gagal:"+result["error"]);
            popupwrapper.hide();
          }
          document.getElementById("formdata").submit();
        },
        onFailure: function(request){
          alert('Error');
          assignError(request.responseText);
        }
      });
  }

  function cetakResi(id){
    Start("keuangan.payoutcgs.payout.php?mode=2&id="+id);
  }

  function pilihBayar(kodesopir,namasopir){
    setPopup(400,133);

    new Ajax.Updater("popupcontent","keuangan.payoutcgs.payout.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=0&kodesopir="+kodesopir+"&nama="+namasopir,
      onLoading: function(request) {
      },
      onComplete: function(request) {
        popupwrapper.show();
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    });
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function browseData(kodesopir,nama){

    setPopup(700,400);

    new Ajax.Updater("popupcontent","keuangan.payoutcgs.browse.php",{
      asynchronous: true,
      method: "post",
      parameters: "siklus="+siklus.value+"&bulan="+bulan.value+"&tahun="+tahun.value+"&kodesopir="+kodesopir+"&nama="+nama,
      onLoading: function(request) {
      },
      onComplete: function(request) {
        setLayoutTable(document.getElementById("tableheaderbrowse"),document.getElementById("tablecontentbrowse"),document.getElementById("wrappertablecontentbrowse"));
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })

    popupwrapper.show();
  }

  function init(e){
     popupwrapper						= dojo.widget.byId("popupcontainer");
     //dlg_buat_voucher_btn_cancel = document.getElementById("dialogbuatvouchercancel");
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER_BY}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Laporan Rekap CGS</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Siklus:<select id="siklus" name="siklus"><option value="0" {OPT_SIKLUS0}>Payout 15</option><option value="1" {OPT_SIKLUS1}>Payout 30</option></select>
      Bulan:<select id="bulan" name="bulan">
          <!-- BEGIN OPT_BLN -->
          <option value="{OPT_BLN.value}" {OPT_BLN.selected}>{OPT_BLN.text}</option>
          <!-- END OPT_BLN -->
        </select>
      Tahun:<select id="tahun" name="tahun">
          <!-- BEGIN OPT_THN -->
          <option value="{OPT_THN.value}" {OPT_THN.selected}>{OPT_THN.text}</option>
          <!-- END OPT_THN -->
        </select>
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN TOMBOL_CRUD -->
    <a href='#' onClick="Start('{EXPORT_EXCEL}');return false;"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Export ke MS EXCEL</a>
    <!-- END TOMBOL_CRUD -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  data rekap payout {PERIODE_SIKLUS}<br><br>
  Total Trip: {TOTAL_TRIP} | Total CGS: Rp. {TOTAL_CGS} | Total Payout: Rp. {TOTAL_PAYOUT} | Payout Terhutang: Rp. {PAYOUT_TERHUTANG}
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th></th>
      <th>No</th>
      <th onclick="setOrder(0)">Kode Sopir</th>
      <th onclick="setOrder(1)">Nama Sopir</th>
      <th onclick="setOrder(2)">Total Trip</th>
      <th onclick="setOrder(3)">Total CGS</span></th>
      <th onclick="setOrder(4)">Jurusan</th>
      <th onclick="setOrder(5)">No Rekening</th>
      <th onclick="setOrder(6)">Bank</th>
      <th onclick="setOrder(7)">Cabang Bank</th>
      <th>Remark</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center">
          <!-- BEGIN ACT_BROWSE -->
          <span class="b_browse{ROW.ACT_BROWSE.disabled}" onclick="{ROW.ACT_BROWSE.act}" title="{ROW.ACT_BROWSE.title}" ></span>&nbsp;
          <!-- END ACT_BROWSE -->

          <!-- BEGIN ACT_PAY -->
          <span class="b_ok{ROW.ACT_PAY.disabled}" onclick="{ROW.ACT_PAY.act}" title="{ROW.ACT_PAY.title}" ></span>&nbsp;
          <!-- END ACT_PAY -->

          <!-- BEGIN ACT_PRINT -->
          <span class="b_print" onclick="{ROW.ACT_PRINT.act}" title="{ROW.ACT_PRINT.title}" ></span>&nbsp;
          <!-- END ACT_PRINT -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.kodesopir}</td>
        <td align="center">{ROW.nama}</td>
        <td align="right">{ROW.totaltrip}</td>
        <td align="right">{ROW.totalcgs}</td>
        <td align="center">{ROW.jurusan}</td>
        <td align="center">{ROW.norek}</td>
        <td align="center">{ROW.bank}</td>
        <td align="center">{ROW.cabangbank}</td>
        <td align="center">{ROW.remark}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+2,sort.value,document.getElementById("tableheader"))
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>