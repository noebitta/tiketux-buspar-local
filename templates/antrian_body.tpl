<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");
    dojo.addOnLoad(init);

    var SID;

    function init(e) {

        SID =document.getElementById("hdn_SID").value;

        //control dialog pembayaran
        dialog_pembayaran = dojo.widget.byId("dialog_pembayaran");
        btn_pembayaran_OK = document.getElementById("dialog_pembayaran_btn_OK");
        btn_pembayaran_Cancel = document.getElementById("dialog_pembayaran_btn_Cancel");
        dialog_pembayaran.setCloseControl(btn_pembayaran_Cancel);

        //control dialog box input voucher
        dialog_voucher = dojo.widget.byId("dialog_input_voucher");
        dialog_voucher_btn_ok = document.getElementById("dialog_input_voucher_btn_ok");
        dialog_voucher_btn_cancel = document.getElementById("dialog_input_voucher_btn_ok_cancel");
        dialog_voucher.setCloseControl(dialog_voucher_btn_cancel);

        //contro dialog box input id ponta
        dialog_ponta = dojo.widget.byId("dialog_input_ponta");
        dialog_ponta_btn_ok = document.getElementById("dialog_input_ponta_btn_ok");
        dialog_ponta_btn_cancel = document.getElementById("dialog_input_ponta_btn_ok_cancel");
        dialog_ponta.setCloseControl(dialog_ponta_btn_cancel);
    }

    function Bayar(kode_booking,kode_jadwal,tgl_berangkat,no_tiket) {
        CheckIsianPonta(kode_booking);
        document.getElementById('kode_booking_go_show').value=kode_booking;
        document.getElementById('no_tiket_goshow').value=no_tiket;
        dialog_pembayaran.show(kode_booking);
        getDataListCheckBoxDiscount(0,0,kode_jadwal,tgl_berangkat);
    }

    function getDataListCheckBoxDiscount(flag_koreksi,jenis_penumpang,kode_jadwal,tgl){
        // Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu

        if(flag_koreksi==0){
            rewrite	= "rewrite_checkbox_discount";
        }

        new Ajax.Updater(rewrite,"reservasi.php?sid="+SID,
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "mode=list_jenis_discount&kode_jadwal="+kode_jadwal+"&tgl_berangkat="+tgl+"&flag_koreksi="+flag_koreksi+"&jenis_penumpang="+jenis_penumpang,
                    onLoading: function(request)
                    {
                    },
                    onComplete: function(request)
                    {
                    },
                    onSuccess: function(request)
                    {
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                })
    }

    function CheckIsianPonta(kodebooking) {
        //periksa jika saat booking ponta tidak di isi maka pembayaran member addict dan reedem point jangan ditampilkan
        new Ajax.Request("reservasi.php?sid="+SID,
                {
                    asynchronous: true,
                    method: "get",
                    parameters: "kodebooking="+kodebooking+
                    "&mode=cekmemberByPonta",
                    onLoading: function(request)
                    {
                    },
                    onComplete: function(request)
                    {

                    },
                    onSuccess: function(request)
                    {

                        eval(request.responseText);
                    },
                    onFailure: function(request)
                    {
                        alert('Error !!! Cannot Save');
                        assignError(request.responseText);
                    }
                });
    }


    function CetakTiket(jenis_pembayaran){
        //mencetak tiket

        jenis_discount = "";
        if(!document.getElementById('kode_booking')){
            kode_booking	= document.getElementById('kode_booking_go_show').value;
            cetak_tiket	= 0;
        }
        else{
            kode_booking	= document.getElementById('kode_booking').value;
            cetak_tiket	= document.getElementById('cetak_tiket')?document.getElementById('cetak_tiket').value:0;
        }
        if(cetak_tiket != 1){
            jenis_discount = '';
            if(document.getElementById('opt_jenis_discount_baru')){
                jenis_discount = document.getElementById('opt_jenis_discount_baru').value;
            }
        }

        if(!document.getElementById('no_tiket')){
            no_tiket	= "";
        }
        else{
            no_tiket		= document.getElementById('no_tiket').value;
        }

        parameter		= "tiket.php?sid="+SID+"&cetak_tiket="+cetak_tiket+
                "&kode_booking="+kode_booking+"&no_tiket="+no_tiket+
                "&jenis_pembayaran="+jenis_pembayaran+
                "&jenis_discount="+jenis_discount+"&antrian=1";



        Start(parameter);

        dialog_pembayaran.hide();

        window.location.reload(true);
    }

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post" id="formcari">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr class='banner' height=40>
                        <td align='center' valign='middle' class="bannerjudul">Antrian</td>
                        <td align='right' valign='middle'>
                            <table>
                                <tr>
                                    <td class='bannernormal'>
                                        &nbsp;Tanggal Antrian:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                                        &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                                        <input type="submit" value="cari" />&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- END HEADER-->
            </form>
        </td>
    </tr>
    <tr>
        <td>
            <table class="border" width='100%' >
                <tr>
                    <th>No Antrian</th>
                    <th>Nama</th>
                    <th>Tanggal Antrian</th>
                    <th>Kode Booking</th>
                    <th>Jurusan</th>
                    <th>Tanggal Berangkat</th>
                    <th>Jam Berangkat</th>
                    <th>Kursi</th>
                    <th>Action</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td><div align="center">{ROW.no_antrian}</div></td>
                    <td>{ROW.nama}</td>
                    <td>{ROW.tgl_antri}</td>
                    <td>{ROW.kode_booking}</td>
                    <td>{ROW.jurusan}</td>
                    <td>{ROW.format_tgl_berangkat}</td>
                    <td>{ROW.jam_berangkat}</td>
                    <td>{ROW.kursi}</td>
                    <td>
                        <a onclick="Bayar('{ROW.kode_booking}','{ROW.kode_jadwal}','{ROW.tgl_berangkat}','{ROW.no_tiket}');">Bayar</a>
                    </td>
                </tr>
                <!-- END ROW -->
            </table>
        </td>
    </tr>
</table>

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;">
        <table>
            <tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
            <tr>
                <td align='center'>
                    <table bgcolor='white' width='100%'>
                        <tr height=30>
                            <td>Silahkan pilih jenis diskon</td>
                        </tr>
                        <tr>
                            <td align="center"><div id="rewrite_checkbox_discount"></div></td>
                        </tr>
                        <tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
                        <tr><td>
                                <input type='hidden' id='kode_booking_go_show' value='' />
                                <input type='hidden' id='no_tiket_goshow' value='' />
                                <table width='100%'>
                                    <tr>
                                        <td width='33%' align='center' valign='middle'>
                                            <a href="" onClick="CetakTiket(0);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_tunai.png" /></a>
                                            <br />
                                            <a href="" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>
                                        </td>
                                        <td width='33%' align='center' valign='middle'>
                                            <a href="" onClick="CetakTiket(5);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_edc.png" /></a>
                                            <br />
                                            <a href="" onClick="CetakTiket(5);return false;"><span class="genmed">EDC</span></a>
                                        </td>
                                        <td width='33%' align='center' valign='middle'>
                                            <a href="" onClick="showInputVoucher();return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_voucher.png" /></a>
                                            <br />
                                            <a href="" onClick="showInputVoucher();return false;"><span class="genmed">Voucher</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='33%' align='center' valign='middle'>
							<span id="pembayaranMember">

							<a href="" onClick="showDebitMember();return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_member.png" /></a>
							<br />
							<a href="" onClick="showDebitMember();return false;"><span class="genmed">Kartu Member</span></a>

							</span>
                                        </td>

                                        <td width='33%' align='center' valign='middle'>
							<span id="pembayaranReedem">
								<a href="" onClick="InputPonta(); return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_ponta.png" /></a>
								<br />
								<a href="" onClick="InputPonta(); return false;"><span class="genmed">REDEM POINT</span></a>
							</span>
                                        </td>
                                        <!--<td width='33%' align='center' valign='middle'>
							<a href="" onClick="CetakTiket(7);return false;"><img src="{TPL}images/icon_indomaret.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(7);return false;"><span class="genmed">Indomaret</span></a>
						</td>-->

                                        <!-- BEGIN show_dialog_pembayaran_transfer -->
                                        <td align='center' valign='middle'>
                                            <a href="" onClick="CetakTiket(6);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_transfer.png" /></a>
                                            <br />
                                            <a href="" onClick="CetakTiket(6);return false;"><span class="genmed">TRANSFER</span></a>
                                        </td>
                                        <!-- END show_dialog_pembayaran_transfer -->
                                    </tr>
                                </table>
                            </td></tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog Input Kode Voucher-->
<div dojoType="dialog" id="dialog_input_voucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;">
        <table>
            <tr><td><h2>Voucher</h2></td></tr>
            <tr>
                <td align='center'>
                    <table bgcolor='white' width='100%'>
                        <tr height=30><td colspan=3>Silahkan masukkan KODE VOUCHER</td></tr>
                        <tr height=30><td>Kode Voucher</td><td>:</td><td><input type='password' id='input_kode_voucher'/></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="button" id="dialog_input_voucher_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
                    <input type="button" onclick="bayarByVoucher(input_kode_voucher.value);" id="dialog_input_voucher_btn_ok" value="   Bayar   ">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog Input Kode Voucher-->

<!--dialog ponta-->
<div dojoType="dialog" id="dialog_input_ponta" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="return false;">
        <table>
            <tr><td><h2>Reedem Point</h2></td></tr>
            <tr>
                <td align='center'>
                    <table bgcolor='white' width='100%'>
                        <tr height=30><td colspan=3>Silahkan masukkan Kode OTP</td></tr>
                        <tr height=30><td colspan=3><input type="password" name="input_otp_reedem" id="input_otp_redem" style="width: 250px;"></td></tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="button" id="dialog_input_ponta_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
                    <input type="button" onclick="BayarByPonta();" id="dialog_input_ponta_btn_ok" value="   Proses   ">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--end dialog ponta-->