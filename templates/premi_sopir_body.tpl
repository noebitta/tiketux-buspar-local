<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="center">
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Premi Sopir</td>
                    <td colspan=2 align='right' class="bannernormal" valign='middle'>
                        <br>
                        <form action="{ACTION_CARI}" method="post">
                            Periode :&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                            &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                            &nbsp;Cari :&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                            <input type="submit" value="Cari">
                        </form>
                    </td>
                </tr>
                <tr>
                    <td width='30%' align='left'></td>
                    <td width='70%' align='right'>
                        {PAGING}
                    </td>
                </tr>
            </table>
            <table width='100%' class="border" cellpadding="6" cellspacing="2">
                <tr>
                    <th width=30>No</th>
                    <th width=100>Kode</th>
                    <th width=200>Nama sopir</th>
                    <th width=100>Status</th>
                    <th width=100>Cabang</th>
                    <th width=100>Total Kehadiran</th>
                    <th width=100>Detail</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td><div align="center">{ROW.no}</div></td>
                    <td><div align="left">{ROW.kode_sopir}</div></td>
                    <td><div align="left">{ROW.nama}</div></td>
                    <td><div align="center">{ROW.status}</div></td>
                    <td><div align="left">{ROW.cabang}</div></td>
                    <td><div align="center">{ROW.hari_kerja}</div></td>
                    <td><div align="center">{ROW.detail}</div></td>
                </tr>
                <!-- END ROW -->
                {NO_DATA}
            </table>
            <table width='100%'>
                <tr>
                    <td width='30%' align='left'></td>
                    <td width='70%' align='right'>
                        {PAGING}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>