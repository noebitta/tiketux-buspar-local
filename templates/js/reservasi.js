var arrkursidipilih = new Object();

function init(e) {

  flagmutasiresv.value=0;
  kodebookdipilih.value="";

  if(tglberangkat.value==""){
    initbulan   = "";
    inittahun   = "";
  }
  else{
    strtanggal  = tglberangkat.value;
    initbulan   = strtanggal.substr(3,2)*1;
    inittahun   = strtanggal.substr(6,4)*1;
  }

  setKalender(tglberangkat.value,initbulan,inittahun);

  popupwrapper	= dojo.widget.byId("popupcontainer");
  popupnotif	  = dojo.widget.byId("popupnotifcontainer");

  if(kotadipilih.value!=""){
    setCabangBerangkat(kotadipilih.value,kotadipilihid.value);
  }

  if(asaldipilih.value!="") {
    setCabangTujuan(asaldipilih.value, asaldipilihid.value);
  }

  if(tujuandipilih.value!="") {
    setJadwal(tujuandipilih.value, tujuandipilihid.value);
  }

  if(jadwaldipilih.value!="") {
    setLayout(tglberangkat.value, jadwaldipilih.value, jadwaldipilihid.value);
  }

  beginrefresh();

}

dojo.addOnLoad(init);

function pilihTanggal(tanggal){
  bulandipilih.value  = tanggal.substring(3,5)*1;
  tahundipilih.value  = tanggal.substring(6,10)*1;

  setKalender(tanggal,bulandipilih.value,tahundipilih.value);

  tglberangkat.value= tanggal;

  if(tujuandipilih.value!="") {
    setJadwal(tujuandipilih.value, tujuandipilihid.value);

    setLayout(tanggal,jadwaldipilih.value,jadwaldipilihid.value);
  }
}

function setCabangBerangkat(kota,kotaid){

  if(document.getElementById(kotadipilihid.value)) {
    document.getElementById(kotadipilihid.value).className = "resvlistpilihan";
  }

  asaloverlay.style.display="block";

  new Ajax.Updater("resvshowpointasal","reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=1&kota="+kota,
    onLoading: function(request){
      document.getElementById("resvshowpointtujuan").innerHTML="pilih point asal";
      document.getElementById("resvshowjadwal").innerHTML="pilih point tujuan";
    },
    onComplete: function(request){
      kotadipilih.value   = kota;
      kotadipilihid.value =kotaid;
      document.getElementById(kotadipilihid.value).className="resvlistpilihansel";
    },
    onSuccess: function(request){
      asaloverlay.style.display="none";
    },
    onFailure: function(request){
      assignError(request.responseText);
    }
  });

}

function setCabangTujuan(cabangasal,cabangasalid){

  if(document.getElementById(asaldipilihid.value)) {
    document.getElementById(asaldipilihid.value).className = "resvlistpilihan";
  }

  tujuanoverlay.style.display="block";

  new Ajax.Updater("resvshowpointtujuan","reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=2&cabangasal="+cabangasal,
    onLoading: function(request){
      document.getElementById("resvshowjadwal").innerHTML="pilih point tujuan";
    },
    onComplete: function(request){
      asaldipilih.value   = cabangasal;
      asaldipilihid.value = cabangasalid;
      document.getElementById(asaldipilihid.value).className="resvlistpilihansel";
    },
    onSuccess: function(request){
      tujuanoverlay.style.display="none";
    },
    onFailure: function(request){
      assignError(request.responseText);
    }
  });

}

function setJadwal(jurusan,cabangtujuanid){

  if(document.getElementById(tujuandipilihid.value)) {
    document.getElementById(tujuandipilihid.value).className = "resvlistpilihan";
  }

  jadwaloverlay.style.display="block";

  new Ajax.Updater("resvshowjadwal","reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=3&tanggal="+tglberangkat.value+"&jurusan="+jurusan,
    onLoading: function(request){
    },
    onComplete: function(request){
      tujuandipilih.value   = jurusan;
      tujuandipilihid.value = cabangtujuanid;
      document.getElementById(tujuandipilihid.value).className="resvlistpilihansel";

      document.getElementById("cabangtujuanakhirid").value  = kodecabangtujuan.value;
      setComboList(document.getElementById("cabangtujuanakhir"),"cabangtujuanakhirid","reservasi.php","mode=14&kota="+kotatujuan.value,false,false);

    },
    onSuccess: function(request){
      jadwaloverlay.style.display="none";
    },
    onFailure: function(request){
      assignError(request.responseText);
    }
  });

}

function setLayout(tanggal,kodejadwal,jadwalid){

  if(document.getElementById(jadwaldipilihid.value)) {
    document.getElementById(jadwaldipilihid.value).className = "resvlistpilihan";
  }

  new Ajax.Updater("showlayout","reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=4.0&tanggal="+tanggal+"&jadwal="+kodejadwal+"&ismutasimode="+flagmutasiresv.value,
    onLoading: function(request){
    },
    onComplete: function(request){
      jadwaldipilih.value   = kodejadwal;
      jadwaldipilihid.value = jadwalid;

      document.getElementById(jadwaldipilihid.value).className="resvlistpilihansel";

      if(document.getElementById("jadwalok") && flagmutasiresv.value==0){
        inputoverlay.style.display="none";
      }
      else{
        inputoverlay.style.display="block";
        resetInput();
      }

      getStatusKursi(tanggal,kodejadwal,document.getElementById("kodejadwalutama").value);

    },
    onSuccess: function(request){
      //layoutoverlay.style.display="none";
    },
    onFailure: function(request){
      assignError(request.responseText);
    }
  });
}

function getStatusKursi(tglberangkat,kodejadwal,kodejadwalutama=""){

  if(tglberangkat=="" || kodejadwal=="") {
    return;
  }

  if(document.getElementById("resvpaketcontainerkursi")){
    lastscrollvalue = document.getElementById("resvpaketcontainerkursi").scrollTop;
  }
  else{
    lastscrollvalue = 0;
  }

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=4.1&tanggal="+tglberangkat+"&jadwal="+kodejadwal+"&kodejadwalutama="+kodejadwalutama+"&ismutasimode="+flagmutasiresv.value,
    onLoading: function(request){
    },
    onComplete: function(request){

      document.getElementById("resvpaketcontainerkursi").scrollTop=lastscrollvalue;

      if(isJson(request.responseText)) {

        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          resetTimerRefresh();
          renderLayout(result["baris"],result["kolom"], result["petalayout"]);
        }
        else {
          objbutton = new Object();
          objbutton["CLOSE"] = function(){popupnotif.hide();};
          objbutton["OK"] = function(){popupnotif.hide();};
          setPopupNotif(popupnotif,400,120,"Gagal:" + result["pesan"],objbutton);
        }
      }
      else{
        objbutton = new Object();
        objbutton["CLOSE"] = function(){popupnotif.hide();};
        objbutton["OK"] = function(){popupnotif.hide();};
        setPopupNotif(popupnotif,400,120,"TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")",objbutton);
      }
    },
    onSuccess: function(request){

    },
    onFailure: function(request){
      assignError(request.responseText);
    }
  });
}

function renderLayout(baris=0,kolom=0,petakursi=""){

  if(baris<=0 || kolom<=0){
    return false;
  }

  if(petakursi!="") {
    tempstring=petakursi;
    tempstring=tempstring.replace(/\'/g,'"');

    if(!isJson(tempstring)){
      return false;
    }

    arrmatrixlayout = JSON.parse(tempstring);

  }

  tablelayout.innerHTML= "";

  for(ibaris=0;ibaris<baris;ibaris++){
    var row = tablelayout.insertRow(ibaris);

    for(ikolom=0;ikolom<kolom;ikolom++){
      var cell  = row.insertCell(ikolom);

      classrender 		= "";
      classlabelkursi	= "";
      labelvalue  		= "";
      namakursi       = "";
      actiononclick   = "";

      if(arrmatrixlayout[ibaris+"_"+ikolom]){

        switch(arrmatrixlayout[ibaris+"_"+ikolom]["status"]){
          case "x":
            classrender 		= "renderkursinone";
            classlabelkursi	= "renderlabelkursinone";
            break;
          case "p":
            classrender 		= "renderkursikosong";
            classlabelkursi	= "renderlabelkursikosong";
            labelvalue  		= arrmatrixlayout[ibaris+"_"+ikolom]["label"];
            actiononclick   = "pilihKursi(\""+arrmatrixlayout[ibaris+"_"+ikolom]["label"]+"\");";
            break;
          case "s":
            classrender 		= "renderkursisopir";
            classlabelkursi	= "renderlabelkursisopir";
            labelvalue  		= "SOPIR";
            break;
          case "b":
            classrender 		= "renderkursibook";
            classlabelkursi	= "renderlabelkursibook";
            labelvalue  		= arrmatrixlayout[ibaris+"_"+ikolom]["label"];
            strnama         = arrmatrixlayout[ibaris+"_"+ikolom]["nama"];
            namakursi       = "<div style='padding-top: 40px; width: 100%;' title='"+strnama+"'><div class='rendernamakursi'>"+strnama.substr(0,18)+"</div></div>";
            break;
          case "c":
            classrender 		= "renderkursikonfirm";
            classlabelkursi	= "renderlabelkursikonfirm";
            labelvalue  		= arrmatrixlayout[ibaris+"_"+ikolom]["label"];
            strnama         = arrmatrixlayout[ibaris+"_"+ikolom]["nama"];
            namakursi       = "<div style='padding-top: 40px; width: 100%;' title='"+strnama+"'><div class='rendernamakursi'>"+strnama.substr(0,18)+"</div></div>";
            break;
          case "t":
            classrender 		= "renderkursikosong selected";
            classlabelkursi	= "renderlabelkursikosong";
            labelvalue  		= arrmatrixlayout[ibaris+"_"+ikolom]["label"];
            break;
          case "d":
            classrender 		= "renderkursikosong block";
            classlabelkursi	= "renderlabelkursikosong block";
            labelvalue  		= arrmatrixlayout[ibaris+"_"+ikolom]["label"];
            namakursi       = "<div style='padding-top: 40px;'><div class='rendernamakursi block'>"+arrmatrixlayout[ibaris+"_"+ikolom]["nama"]+"</div></div>";
            break;
        }
      }
      else{
        classrender = "renderkursinone";
        displaylabel= "none";
        labelvalue  = "";
        namakursi   = "";
        arrmatrixlayout[ibaris+"_"+ikolom] = {status:'x',label:''};
      }

      cell.innerHTML="<div id='elmmatrix_"+ibaris+"_"+ikolom+"' class='"+classrender+"' onclick='"+actiononclick+";return false;'><div class='"+classlabelkursi+"'>"+labelvalue+"</div>"+namakursi+"</div>";

    }
  }

}

function pilihKursi(nomorkursi){
  alert(nomorkursi);
}

function showDialogPembayaran(){

  var isvalid=true;

  isvalid = validasiInput(telppengirim) && isvalid;
  isvalid = validasiInput(namapengirim) && isvalid;
  isvalid = validasiInput(alamatpengirim) && isvalid;
  isvalid = validasiInput(telppenerima) && isvalid;
  isvalid = validasiInput(namapenerima) && isvalid;
  isvalid = validasiInput(alamatpenerima) && isvalid;
  isvalid = validasiInput(beratpaket) && isvalid;
  isvalid = validasiInput(keteranganpaket) && isvalid;

  if(!isvalid) {
    return false;
  }

  if(jadwaldipilih.value==""){
    alert("Anda belum memilih keberangkatan!");
    return false;
  }

  setPopup(400,180);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=5&tglberangkat="+tglberangkat.value+"&jurusan="+tujuandipilih.value+
    "&kodejadwal="+jadwaldipilih.value+"&layanan="+layananpaket.value+"&jeniskardus="+jeniskardus.value+"&berat="+beratpaket.value+
    "&isdijemput="+(paketdijemput.checked?1:0)+"&isdiantar="+(paketdiantar.checked?1:0)+"&kodevoucher="+kodevoucher.value,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function getDaftarHarga(){

  if(tujuandipilih.value==""){
    return false;
  }

  new Ajax.Request("reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=6&jurusan="+tujuandipilih.value,
    onLoading: function(request){
    },
    onComplete: function(request){
    },
    onSuccess: function(request){
      eval(request.responseText);
    },
    onFailure: function(request){
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

function hitungHargaPaket(){

  if(!validasiInput(beratpaket.value)){
    return;
  }

  idxlayanan  = layananpaket.value;

  totalhargapaket	= (document.getElementById("hargakgpertama"+idxlayanan).value*1+document.getElementById("hargakgberikutnya"+idxlayanan).value*((beratpaket.value-1)>=0?beratpaket.value-1:0));

  totalhargapaket = totalhargapaket + (paketdijemput.checked?chargejemput.value*1:0)+(paketdiantar.checked?chargeantar.value*1:0);

  showhargapaket.innerHTML	=  formatUang(totalhargapaket,'.');
}

function prosesBayar(jenisbayar){

  if(cabangtujuanakhirid.value!=kodecabangtujuan.value){
    if(!confirm("Dikarenakan tujuan akhir dari barang tidak sama dengan tujuan dari jadwal ini, maka barang akan ditransitkan di checker, klik OK jika setuju!")){
      return false;
    }
  }

  popupwrapper.hide();
  bodyoverlay.style.display="block";

  new Ajax.Request("reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=7"+
    "&kodevoucher="+kodevoucher.value+
    "&tglberangkat="+tglberangkat.value+
    "&jurusan="+tujuandipilih.value+
    "&kodejadwal="+jadwaldipilih.value+
    "&telppengirim="+telppengirim.value+
    "&namapengirim="+namapengirim.value+
    "&alamatpengirim="+alamatpengirim.value+
    "&isdijemput="+(paketdijemput.checked?1:0)+
    "&telppenerima="+telppenerima.value+
    "&nama_penerima="+namapenerima.value+
    "&alamatpenerima="+alamatpenerima.value+
    "&isdiantar="+(paketdiantar.checked?1:0)+
    "&berat="+beratpaket.value+
    "&layanan="+layananpaket.value+
    "&jeniskardus="+jeniskardus.value+
    "&carabayar="+carabayar.value+
    "&keterangan="+keteranganpaket.value+
    "&jenisbayar="+jenisbayar+
    "&tujuanakhir="+cabangtujuanakhirid.value,
    onLoading: function(request){
    },
    onComplete: function(request){
    },
    onSuccess: function(request){

      if(isJson(request.responseText)) {
        bodyoverlay.style.display = "none";

        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          resetTimerRefresh();
          setLayout(tglberangkat.value, jadwaldipilih.value, jadwaldipilihid.value)
          resetInput();
          cetakResi(result["pesan"]);
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function(request){
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });


}

function resetInput(){
  kodevoucher.value = "";
  telppemesan.value = "";
  namapemesan.value = "";
  keterangan.value  = "";

  wrappernamapenumpang.innerHTML="";

  var strlistnamapenumpang="";

  for(var key in arrkursidipilih){
    if (arrkursidipilih.hasOwnProperty(key)) {
      arrkursidipilih[key] = "";
      strlistnamapenumpang +="<span class='resvisidatalabel'>Nama di Kursi "+key+"</span><span class='resvdisidatafield'>&nbsp;<input id='namapenumpang'"+key+" type='text' value='' maxlength='30' onkeypress='validCharInput(event);'  onFocus=\"this.style.background='white';\"/></span>";
    }
  }

  wrappernamapenumpang.innerHTML	= (strlistnamapenumpang!="")?strlistnamapenumpang:"belum ada kursi dipilih";

}

function showDetailReservasi(noresi){


  setPopup(800,450);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=8&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function showDialogReqOTPCetak(noresi){

  setPopup(500,240);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=10&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function requestOTPCetak(noresi){

  if(!validasiInput(alasandlg)){
    return false;
  }

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=10.1" +
    "&noresi=" + noresi +
    "&alasan="+alasandlg.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          showDialogOTPCetak(noresi);
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

function cetakResi(noresi,cetakulang=0){

  if(cetakulang==0){
    kodebookdipilih.value= noresi;
    window.open("","newwindow","toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    formtosubmit  = document.getElementById("formsubmit");
    formtosubmit.action="paket.resi.php"
    formtosubmit.target="newwindow";
    formtosubmit.submit();

    return false;
  }

  if(!document.getElementById("otpdlg")){
    return false;
  }

  if(!validasiInput(document.getElementById("otpdlg"))){
    return false;
  }

  otp.value=document.getElementById("otpdlg").value;

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=10.3" +
    "&noresi=" + noresi +
    "&otp="+otp.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          kodebookdipilih.value = noresi;

          if (result["pesan"] == "APPROVED") {
            popupwrapper.hide();
            window.open("", "newwindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
            formtosubmit = document.getElementById("formsubmit");
            formtosubmit.action = "paket.resi.php"
            formtosubmit.target = "newwindow";
            formtosubmit.submit();
          }
          else {
            alert("OTP anda ditolak!");
            otpdlg.value = "";
          }
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });

}

function showDialogOTPCetak(noresi){

  setPopup(500,170);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=10.2"+
    "&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function showDialogReqOTPBatal(noresi){

  setPopup(500,240);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=11&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function requestOTPBatal(noresi){

  if(!validasiInput(alasandlg)){
    return false;
  }

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=11.1" +
    "&noresi=" + noresi +
    "&alasan="+alasandlg.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          showDialogOTPBatal(noresi);
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

function showDialogOTPBatal(noresi){

  setPopup(500,170);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=11.2"+
    "&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function batalkanPaket(noresi){

  if(!document.getElementById("otpdlg")){
    return false;
  }

  if(!validasiInput(document.getElementById("otpdlg"))){
    return false;
  }

  otp.value=document.getElementById("otpdlg").value;

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=11.3" +
    "&noresi=" + noresi +
    "&otp="+otp.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          kodebookdipilih.value = noresi;

          if (result["pesan"] == "APPROVED") {
            popupwrapper.hide();
            setLayout(tglberangkat.value, jadwaldipilih.value, jadwaldipilihid.value);
          }
          else {
            alert("OTP anda ditolak!");
            otpdlg.value = "";
          }
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });

}

function showDialogReqOTPMutasi(noresi){

  setPopup(500,240);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=12&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function requestOTPMutasi(noresi){

  if(!validasiInput(alasandlg)){
    return false;
  }

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=12.1" +
    "&noresi=" + noresi +
    "&alasan="+alasandlg.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          showDialogOTPMutasi(noresi);
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

function showDialogOTPMutasi(noresi){

  setPopup(500,170);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=12.2"+
    "&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function mutasiPaket(noresi){

  if(!document.getElementById("otpdlg")){
    return false;
  }

  if(!validasiInput(document.getElementById("otpdlg"))){
    return false;
  }

  otp.value=document.getElementById("otpdlg").value;

  flagmutasiresv.value=0;

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=12.3" +
    "&noresi=" + noresi +
    "&jurusan=" + tujuandipilih.value+
    "&otp="+otp.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          kodebookdipilih.value = noresi;

          if (result["pesan"] == "APPROVED") {
            flagmutasiresv.value = 1;
            resvmutasimodeon.style.display = "block";
            inputoverlay.style.display = "block";
            setLayout(tglberangkat.value, jadwaldipilih.value, jadwaldipilihid.value);
            popupwrapper.hide();
          }
          else {
            alert("OTP anda ditolak!");
            otpdlg.value = "";
          }
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });

}

function batalkanModeMutasi(){
  otp.value= "";
  flagmutasiresv.value=0;
  resvmutasimodeon.style.display="none";
  inputoverlay.style.display="none";
  setLayout(tglberangkat.value, jadwaldipilih.value, jadwaldipilihid.value);
}

function prosesMutasiPaket(){

  if(otp.value==""){
    batalkanModeMutasi();
    return false;
  }
  else{
    konfirmmutasi=confirm("Apakah anda yakin akan memutasikan transaksi ini?");
  }

  if(konfirmmutasi) {

    new Ajax.Request("reservasi.php", {
      asynchronous: true,
      method: "post",
      parameters: "mode=12.4" +
      "&noresi=" + kodebookdipilih.value +
      "&jurusan=" + tujuandipilih.value+
      "&tanggal=" + tglberangkat.value+
      "&jadwal=" + jadwaldipilih.value+
      "&otp="+otp.value,
      onLoading: function (request) {
      },
      onComplete: function (request) {
      },
      onSuccess: function (request) {

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            kodebookdipilih.value = "";

            if (result["pesan"] == "DONE") {
              batalkanModeMutasi();
            }
            else {
              alert("Proses mutasi ditolak!");
              otp.value = "";
            }
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }
      },
      onFailure: function (request) {
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });
  }
}

var wakturefresh=10;//detik
var countdown=wakturefresh;

function beginrefresh(){

  if (countdown==1) {
    countdown=wakturefresh;
    beginrefresh();
    //merefresh pengumumman
    //cekPengumuman();
    if(jadwaldipilih.value!="") {
      getStatusKursi(tglberangkat.value, jadwaldipilih.value,document.getElementById("kodejadwalutama").value);
    }

  }
  else {
    countdown-=1;
    setTimeout("beginrefresh()",1000);
  }

}

function resetTimerRefresh(){
  countdown=wakturefresh;
}

function cariDataPelanggan(notelp,pengirim=1){

  if(pengirim){
    sectiondatapengirim.innerHTML="data pengirim ...mencari data...";
  }
  else{
    sectiondatapenerima.innerHTML="data penerima ...mencari data...";
  }

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=13" +
    "&notelp=" + notelp,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (pengirim) {
          if (result["notelp"] == notelp) {
            namapengirim.value = result["nama"];
            alamatpengirim.value = result["alamat"];
          }

          sectiondatapengirim.innerHTML = "data pengirim";
        }
        else {
          if (result["notelp"] == notelp) {
            namapenerima.value = result["nama"];
            alamatpenerima.value = result["alamat"];
          }

          sectiondatapenerima.innerHTML = "data penerima";
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }

    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });

}

function cariDataReservasi(cari){

  arrkursidipilih["5"]="Dena";
  arrkursidipilih["6"]="Betty";
  arrkursidipilih["7"]="Iwan";

  wrappernamapenumpang.innerHTML="";

  for(var key in arrkursidipilih){
    if (arrkursidipilih.hasOwnProperty(key)) {
      wrappernamapenumpang.innerHTML +="<span class='resvisidatalabel'>Nama di Kursi "+key+"</span><span class='resvdisidatafield'>&nbsp;<input id='namapenumpang'"+key+" type='text' value='"+arrkursidipilih[key]+"' maxlength='30' onkeypress='validCharInput(event);'  onFocus=\"this.style.background='white';\"/></span>";
    }
  }

  return;

	if(!validasiInput(inputcari)){
    return false;
  }

  setPopup(600,300);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=9"+
    "&cari="+cari,
    onLoading: function(request) {
    },
    onComplete: function(request) {

      setLayoutTable(document.getElementById("tableheaderbrowse"),document.getElementById("tablecontentbrowse"),document.getElementById("wrappertablecontentbrowse"));
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function showDialogReqOTPCetakManifest(nomanifest){

  setPopup(500,240);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=15&nomanifest="+nomanifest,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function showDialogOTPCetakManifest(nomanifest){

  setPopup(500,170);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=15.2"+
    "&nomanifest="+nomanifest,
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function requestOTPCetakManifest(nomanifest){

  if(!validasiInput(alasandlg)){
    return false;
  }

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=15.1" +
    "&nomanifest=" + nomanifest +
    "&alasan="+alasandlg.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {
          showDialogOTPCetakManifest(nomanifest);
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

function cetakManifest(nomanifest,cetakulang=0){

  submittglberangkat.value    = tglberangkat.value;
  submitjadwal.value          = jadwaldipilih.value;

  if(cetakulang==0){
    window.open("","newwindow","toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    formtosubmit  = document.getElementById("formsubmitmanifest");
    formtosubmit.action="paket.manifest.php"
    formtosubmit.target="newwindow";
    formtosubmit.submit();

    return false;
  }

  if(!document.getElementById("otpdlg")){
    return false;
  }

  if(!validasiInput(document.getElementById("otpdlg"))){
    return false;
  }

  otpmanifest.value=document.getElementById("otpdlg").value;

  new Ajax.Request("reservasi.php", {
    asynchronous: true,
    method: "post",
    parameters: "mode=15.3" +
    "&nomanifest=" + nomanifest +
    "&otp="+otpmanifest.value,
    onLoading: function (request) {
    },
    onComplete: function (request) {
    },
    onSuccess: function (request) {

      if(isJson(request.responseText)) {
        var result;
        result = JSON.parse(request.responseText);

        if (result["status"] == "OK") {

          if (result["pesan"] == "APPROVED") {

            popupwrapper.hide();
            window.open("", "newwindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
            formtosubmit = document.getElementById("formsubmitmanifest");
            formtosubmit.action = "paket.manifest.php"
            formtosubmit.target = "newwindow";
            formtosubmit.submit();
          }
          else {
            alert("OTP anda ditolak!");
            otpdlg.value = "";
          }
        }
        else {
          alert("Gagal:" + result["pesan"]);
        }
      }
      else{
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
      }
    },
    onFailure: function (request) {
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

function showDialogAmbilPaket(noresi,isotp=0){

  setPopup(500,200);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=16"+
    "&noresi="+noresi+"&isotp="+isotp,
    onLoading: function(request) {
    },
    onComplete: function(request) {
      popupwrapper.show();
    },
    onSuccess: function(request) {
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function prosesPengambilan(){

  var inputvalid=true;

  inputvalid  = validasiInput(document.getElementById("namapengambilpaket")) && inputvalid;
  inputvalid  = validasiInput(document.getElementById("telppengambilpaket")) && inputvalid;
  inputvalid  = validasiInput(document.getElementById("ktppengambilpaket")) && inputvalid;

  if(document.getElementById("otppengambilan")){
    inputvalid = validasiInput(document.getElementById("otppengambilan")) && inputvalid;
    otppengambilan  = document.getElementById("otppengambilan").value;
  }
  else{
    otppengambilan  = "";
  }

  if(!inputvalid){
    return false;
  }

  konfirm=confirm("Pastikan data yang dimasukkan sudah benar! Klik OK untuk melanjutkan proses pengambilan barang.");

  if(konfirm) {

    new Ajax.Request("reservasi.php", {
      asynchronous: true,
      method: "post",
      parameters: "mode=16.1" +
      "&noresi=" + document.getElementById("noresiambilpaket").value +
      "&namapengambil=" + document.getElementById("namapengambilpaket").value +
      "&telppengambil=" + document.getElementById("telppengambilpaket").value +
      "&ktppengambil=" + document.getElementById("ktppengambilpaket").value +
      "&otp=" + otppengambilan,
      onLoading: function (request) {
      },
      onComplete: function (request) {
      },
      onSuccess: function (request) {

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            popupwrapper.hide();
            alert("Proses pengambilan paket BERHASIL!")
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }
      },
      onFailure: function (request) {
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });
  }
}

function showDialogTerimaPaket(noresi){

  setPopup(500,200);

  new Ajax.Updater("popupcontent","reservasi.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=17"+
    "&noresi="+noresi,
    onLoading: function(request) {
    },
    onComplete: function(request) {
      popupwrapper.show();
    },
    onSuccess: function(request) {
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function prosesPenerimaan(){

  var inputvalid=true;

  inputvalid  = validasiInput(document.getElementById("namapenerimapaket")) && inputvalid;
  inputvalid  = validasiInput(document.getElementById("telppenerimapaket")) && inputvalid;

  if(!inputvalid){
    return false;
  }

  konfirm=confirm("Pastikan data yang dimasukkan sudah benar! Klik OK untuk melanjutkan proses ini.");

  if(konfirm) {

    new Ajax.Request("reservasi.php", {
      asynchronous: true,
      method: "post",
      parameters: "mode=17.1" +
      "&noresi=" + document.getElementById("noresiterimapaket").value +
      "&namapenerima=" + document.getElementById("namapenerimapaket").value +
      "&telppenerima=" + document.getElementById("telppenerimapaket").value,
      onLoading: function (request) {
      },
      onComplete: function (request) {
      },
      onSuccess: function (request) {

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            popupwrapper.hide();
            alert("Proses penerimaan paket BERHASIL!")
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }
      },
      onFailure: function (request) {
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });
  }
}




