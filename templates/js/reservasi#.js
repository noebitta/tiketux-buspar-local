var SID;
var harga_minimum_paket;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function cekValueNoTelp(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function validasiNoTelp(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}


function ShowDialogKode(jenis){
	document.getElementById('jenis_pembatalan').value=jenis;
	//document.getElementById('kode').value='';
	dlg_TanyaHapus.show();
	//HapusBerdasarkanJenis();
}

function Right(str, n){
	/*
      IN: str - the string we are RIGHTing
      n - the number of characters we want to return

      RETVAL: n characters from the right side of the string
       */
  if (n <= 0)     // Invalid bound, return blank string
		return "";
  else if (n > String(str).length)   // Invalid bound, return
		return str;                     // entire string
  else { // Valid bound, return appropriate substring
		var iLen = String(str).length;
    return String(str).substring(iLen, iLen - n);
  }
}

Number.prototype.formatMoney = function(c, d, t){
	var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function ValidasiAngka(objek,kolom){
	temp_nilai=objek.value*0;
	
	if(temp_nilai!=0){
		alert(kolom+" harus angka!");
		objek.setFocus;exit;
	}
	
	if(objek.value<0){
		alert(kolom+" tidak boleh kurang dari 0!");
		objek.setFocus;exit;
	}
	
}

function validasiInputanAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
			|| [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39 || [evt.keyCode||evt.which]==116)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function FormatUang(uang,separator){
	len_uang = String(uang).length;
	return_val='';
	for (i=len_uang;i>=0;i--){
		if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

		return_val =String(uang).substring(i,i-1)+return_val;
	}
	
	return return_val;
}

function getUpdateAsal(kota_asal,mode){
	// fungsi ini mengubah RUTE menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   			
	
	document.getElementById('rewritepaket').innerHTML="";
	
	if(document.getElementById('flag_mutasi').value!=1 && document.getElementById('flag_mutasi_paket').value!=1){
		document.getElementById('dataPelanggan').innerHTML='';
	}
	
	//mengambil tanggal yang diinput user dan rute
	tgl_user  = document.getElementById('p_tgl_user').value;
	
	//memeriksa combo rewritejadwal sudah ditampilkan atau belum
	
	temp_combo_rute=document.getElementById('rewritejadwal').innerHTML;
	
	if (temp_combo_rute != ""){
		//jika sudah muncul, akan diambil nilai dari combo rute
		jam=document.getElementById('p_jadwal').value;
		id_jurusan	= document.getElementById('tujuan').value;
		//getUpdateJadwal(id_jurusan);
	}
	else{
		//jika belum dimunculkan, maka nilai dari rute akan diisi dengan kosong
		jam='';
		mode=0;
	}

	document.getElementById('rewritemobil').innerHTML = "";
					 
	if(mode==0){
		
		document.getElementById('rewritetujuan').innerHTML = "";
		document.getElementById('rewritejadwal').innerHTML = "";
		
		new Ajax.Updater("rewriteasal","reservasi.php?sid="+SID, {
			asynchronous: true,
			method: "get",

			parameters: "mode=asal&kota_asal="+kota_asal,
			onLoading: function(request){
				Element.show('progress_asal');
			},
			onComplete: function(request){
				Element.show('rewriteasal');
				Element.hide('progress_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});
		
	}
	else{
		getUpdateMobil();
		//setSisaKursi();
		getUpdatePaket();
		//getPengumuman();
		getDataListDiscount(0,0);
		getListHargaPaket();
	}
	
			
}

function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
    
		document.getElementById('rewritemobil').innerHTML = "";
    
		if(document.getElementById('flag_mutasi').value!=1 && document.getElementById('flag_mutasi_paket').value!=1){
			document.getElementById('dataPelanggan').innerHTML='';
		}
		
		new Ajax.Updater("rewritetujuan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&mode=tujuan",
        onLoading: function(request) 
        {
            Element.show('progress_tujuan');
            document.getElementById('rewritejadwal').innerHTML="";
						//Element.hide('rewritejadwal');
        },
        onComplete: function(request) 
        {
            Element.show('rewritetujuan');
						Element.hide('progress_tujuan');
						//Element.show('rewriteall');
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function getUpdateJadwal(id_jurusan){
	
	tgl=document.getElementById('p_tgl_user').value;
	//document.getElementById('p_jadwal').value="";
	
	// mengupdate JAM sesuai dengan RUTE yang dipilih
	document.getElementById('id_jurusan_aktif').value = id_jurusan;
	
	document.getElementById('rewritemobil').innerHTML = "";
	document.getElementById('rewritepaket').innerHTML="";	
	
	new Ajax.Updater("rewritejadwal","reservasi.php?sid="+SID, 
	{
		asynchronous: true,
		method: "get",
		parameters: "tgl=" + tgl + "&id_jurusan=" + id_jurusan +"&mode=jam",
		onLoading: function(request) 
		{
			document.getElementById('rewritejadwal').innerHTML="";	
			Element.show('progress_jam');
		},
			onComplete: function(request) 
		{
			Element.hide('progress_jam');
			Element.show('rewritejadwal');
		},                
		onFailure: function(request) 
		{ 
			assignError(request.responseText);
		}
	});
}

function changeRuteJam(){
	
	Element.show('loading_layout_kursi');
	getUpdateMobil();
	getUpdatePaket();
	getDataListDiscount(0,0);
	getListHargaPaket();
	//getUpdateWaitingList();
	Element.hide('loading_layout_kursi');
	//setSisaKursi();
	
}

var obj_jadwal_dipilih;

function pilihJadwal(obj){
	if(obj_jadwal_dipilih){
		obj_jadwal_dipilih.className='a';
	}
	
	obj.className='selected';
	obj_jadwal_dipilih=obj;
}

function getUpdateMobil(){
		
    // update informasi layout-mobil sesuai TANGGAL, RUTE dan JAM
    var tgl  		= document.getElementById('p_tgl_user').value;
    var jadwal  = document.getElementById('p_jadwal').value;
		
		if(!document.getElementById('kode_booking') || jadwal=="" || jadwal=="(none)"){
			kode_booking="";
			
			if(document.getElementById('flag_mutasi').value!=1 && document.getElementById('flag_mutasi_paket').value!=1){
				document.getElementById('dataPelanggan').innerHTML='';
			}
			
		}
		else{
			kode_booking=document.getElementById('kode_booking').value;
		}
		
		new Ajax.Updater("rewritemobil","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "tanggal=" + tgl + "&jadwal="+ jadwal +"&kode_booking_dipilih="+kode_booking+"&mode=mobil",
        onLoading: function(request) 
        {
          count_timer_layout_kursi = interval_halt_layout;
					Element.show('progress_kursi'); 
        },
        onComplete: function(request) 
        {   	    
					count_timer_layout_kursi = interval_timer_layout;
					document.getElementById('answerrute').innerHTML=document.getElementById('p_jadwal').value;  		
					Element.hide('progress_kursi');
					
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdateStatusKursi(no_kursi){
		
    // update informasi layout-mobil sesuai TANGGAL, RUTE dan JAM
    var tgl  		= document.getElementById('p_tgl_user').value;
    var jadwal  = document.getElementById('p_jadwal').value;
		
		target_element	= "rewrite_kursi"+no_kursi;	
		
		new Ajax.Updater(target_element,"reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "no_kursi=" + no_kursi + "&tanggal=" + tgl + "&jadwal="+ jadwal +"&mode=ambil_status_kursi",
        onLoading: function(request) 
        {
					count_timer_layout_kursi = interval_halt_layout;
					Element.show('progress_kursi'); 
        },
        onComplete: function(request) 
        {
					count_timer_layout_kursi = interval_timer_layout;
					Element.hide('progress_kursi');
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getPengumuman(){

		new Ajax.Updater("rewritepengumuman","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=pengumuman",
        onLoading: function(request) 
        {
            
        },
        onComplete: function(request) 
        {   
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdatePaket(){
		// update informasi layout-paket sesuai TANGGAL, RUTE dan JAM
    var tgl  							= document.getElementById('p_tgl_user').value;
    var jadwal  					= document.getElementById('p_jadwal').value;
    var flag_mutasi_paket = document.getElementById('flag_mutasi_paket').value;
    
		new Ajax.Updater("rewritepaket","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "tanggal=" + tgl + "&jadwal="+ jadwal + "&flag_mutasi=" + flag_mutasi_paket  + "&mode=paketlayout",
        onLoading: function(request) 
        {
					count_timer_paket	= interval_halt_paket;
					//Element.show('progress_paket'); 
        },
        onComplete: function(request) 
        {
					count_timer_paket = interval_timer_paket;
					//Element.hide('progress_paket'); 
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdateWaitingList(){
		
    // update informasi layout-paket sesuai TANGGAL, RUTE dan JAM
    var tgl  = document.getElementById('p_tgl_user').value;
    var jam  = document.getElementById('p_jadwal').value;
    
		new Ajax.Updater("rewritewaitinglist","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "tanggal=" + tgl + "&jam="+ jam +"&mode=waiting_list&aksi=tampilkan",
        onLoading: function(request) 
        {
					Element.show('progress_waiting_list'); 
        },
        onComplete: function(request) 
        {   	    
					Element.hide('progress_waiting_list'); 
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });       
}

function setDialogSPJ(){
    //menampilkan dialog spj

		sopir_sekarang  = document.getElementById('hide_nama_sopir_sekarang').value;
		mobil_sekarang  = document.getElementById('hide_mobil_sekarang').value;
		kode_jadwal  		= document.getElementById('p_jadwal').value;
		no_spj					= document.getElementById('txt_spj').value;  
		
		new Ajax.Updater("rewrite_list_sopir","SPJ.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "kode_jadwal="+kode_jadwal+"&sopir_sekarang=" + sopir_sekarang +"&mobil_sekarang=" + mobil_sekarang + "&no_spj="+no_spj+"&aksi=0",
        onLoading: function(request) 
        {
						document.getElementById('hide_nama_sopir_sekarang').innerHTML="";
						ocument.getElementById('hide_mobil_sekarang').innerHTML="";
            Element.show('progress_dialog_spj');
        },
        onComplete: function(request) 
        {
            Element.hide('progress_dialog_spj');
        },                
        onFailure: function(request) 
        { 
            assignError(request.responseText);
        }
    });
	
	dialog_SPJ.show();
}

function getDataListDiscount(flag_koreksi,jenis_penumpang){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		kode_jadwal  		= document.getElementById('p_jadwal').value;
		tgl  						= document.getElementById('p_tgl_user').value;
		
		if(flag_koreksi==0){
			rewrite	= "rewrite_list_discount";
		}
		else{
			rewrite	= "rewrite_list_discount_dialog";
		}
		
		new Ajax.Updater(rewrite,"reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "mode=list_jenis_discount&kode_jadwal="+kode_jadwal+"&tgl_berangkat="+tgl+"&flag_koreksi="+flag_koreksi+"&jenis_penumpang="+jenis_penumpang,
	        onLoading: function(request) 
	        {
	        },
	        onComplete: function(request) 
	        {
	        },
	        onSuccess: function(request) 
	        {
	        },
	        onFailure: function(request) 
	        {
	          assignError(request.responseText);
	        }
	  }) 
}

function getListHargaPaket(){
	
	id_jurusan	= document.getElementById('id_jurusan_aktif').value
	
	if(id_jurusan==""){
		return;
	}
	
	new Ajax.Request("reservasi_paket.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "id_jurusan="+id_jurusan+
								"&mode=ambil_list_harga_paket&paket=0",
    onLoading: function(request){},
    onComplete: function(request){},
    onSuccess: function(request){		
			eval(request.responseText);
		},
    onFailure: function(request){
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function getDataListAsuransi(no_tiket){
	// Mendapatkan dan menampilkan list plan asuransi
	
	new Ajax.Updater("rewrite_list_plan_asuransi","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "mode=listplanasuransi&no_tiket="+no_tiket,
	        onLoading: function(request) 
	        {
	        },
	        onComplete: function(request) 
	        {
	        },
	        onSuccess: function(request) 
	        {
	        },
	        onFailure: function(request) 
	        {
	          assignError(request.responseText);
	        }
	  }) 
}

function showChair(chno,no_tiket){
	
	// Mendapatkan nilai kursi dari database (why ? biar ga bentrok :P)
	document.getElementById('flag_mutasi').value=0; 
	no_spj	= document.getElementById('txt_spj').value; 
	document.getElementById("dataPelanggan").innerHTML="<table><tr><td height='500'></td></tr></table>";
	
	new Ajax.Updater("dataPelanggan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "kursi="+chno+"&no_tiket="+no_tiket+"&no_spj="+no_spj+"&mode=showchair",
        onLoading: function(request) 
        {
					Element.show('loading_data_penumpang');
        },
        onComplete: function(request) 
        {              
					Element.hide('loading_data_penumpang');
					
					if(document.getElementById("kode_booking")){
						getUpdateMobil();
					}
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function showPaket(no_tiket){
	document.getElementById('flag_mutasi_paket').value=0; 
	
	new Ajax.Updater("dataPelanggan","reservasi.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "no_tiket="+no_tiket+"&mode=paketdetail",
        onLoading: function(request) 
        {
					Element.show('loading_data_penumpang');
        },
        onComplete: function(request) 
        {              
					Element.hide('loading_data_penumpang');
				
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });  
}

function updateTemp(chno){
  // Mendapatkan nilai kursi dari database (why ? biar ga bentrok :P)
	
	tgl  				= document.getElementById('p_tgl_user').value;
	jam 	 			= document.getElementById('p_jadwal').value; 	
	layout_kursi= document.getElementById('hide_layout_kursi').value; 	
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}

	if(document.getElementById('flag_mutasi').value!=1 && document.getElementById('flag_mutasi_paket').value!=1){
		document.getElementById('dataPelanggan').innerHTML='';
	}
	else{
		mutasiPenumpang(chno);
		return;
	}
	
	target_element	= "rewrite_kursi"+chno;	
	
	new Ajax.Updater(target_element,"reservasi.php?sid="+SID, 
  {
      asynchronous: true,
      method: "get",
      parameters: "tanggal=" + tgl + "&jam="+ jam +"&kursi="+chno+"&no_spj="+no_spj+"&layout_kursi="+layout_kursi+"&mode=temp",
      onLoading: function(request) 
      {
				Element.show('progress_kursi'); 
      },
      onComplete: function(request) 
      {
      	Element.hide('progress_kursi');
      },
      onSuccess: function(request) 
   		{
   		},
      onFailure: function(request) 
      { 
				assignError(request.responseText); 
      }
  });      
		
}

function pesanKursi(go_show){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  kode_jadwal  	= document.getElementById('p_jadwal').value;  
	telp_old			= document.getElementById('hdn_no_telp_old').value;
	nama  				= document.getElementById('fnama');
  telepon  			= document.getElementById('ftelp');
	alamat  			= document.getElementById('fadd').value;
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	
	if(document.getElementById('opt_jenis_discount')){
		id_discount		= document.getElementById('opt_jenis_discount').value;
	}
	else{
		id_discount="";
	}
	valid=true;
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  if ((nama.value=='') || (nama.value==String.Empty)){
		nama.style.background="red";
		valid=false;
	}
  
	if ((telepon.value=='') || !cekValueNoTelp(telepon.value)){
		telepon.style.background="red";
		valid=false;
  }
	
  if(!valid){
		return;
	}
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"mode=book&nama="+nama.value+"&alamat="+alamat+"&no_telp_old="+telp_old+
			"&telepon="+telepon.value+"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&layout_kursi="+layout_kursi+"&jenis_discount="+id_discount,
			
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
			segment_data	= request.responseText.split("|");
			header_data		= segment_data[0].split("/");
			data_kursi		= segment_data[1].split("/");
			flag_sukses		= header_data[0];			
			
			if(flag_sukses=="0"){
				alert('Anda belum memilih kursi yang akan dipesan!');
			}
			else if(flag_sukses==1){
				document.getElementById('hdn_no_telp_old').value='';
				document.getElementById('fnama').value='';
				document.getElementById('fadd').value='';
			  document.getElementById('ftelp').value='';
				Element.show('rewrite_list_discount');
				Element.hide('rewrite_keterangan_member');
				
				//getUpdateMobil(); // update layout mobil :
				jumlah_kursi_dipesan	= data_kursi.length;
				
				for(idx=0;idx<jumlah_kursi_dipesan;idx++){
					getUpdateStatusKursi(data_kursi[idx]);
				}
				
				if(go_show==1){
					document.getElementById('kode_booking_go_show').value=header_data[1];
					document.getElementById('no_tiket_goshow').value=header_data[2];
					dialog_pembayaran.show(header_data[1]);
				}
				
				getDataListDiscount(0,0);
				Element.hide('progress_kursi');
				
			}
			else if(flag_sukses==2){
				alert("Penumpang dengan nomor telepon " + telepon + " sudah pernah membooking kursi, untuk dapat melakukan pembooking, silahkan lakukan pelunasan atau batalkan pemesanan sebelumnya.");
				periksaJadwal(telepon);
			}
			else if(flag_sukses==3){
				alert("Anda tidak diperbolehkan membooking kursi lebih dari 8 hari dari hari ini.");
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			Element.hide('progress_kursi');
			Element.hide('label_not_found');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function ubahDataPenumpang(no_tiket){
  
  // melakukan saving / update data penumpang/
	kursi  				= document.getElementById('kursi').value;
	nama  				= document.getElementById('ubah_nama_penumpang').value;
	alamat  			= document.getElementById('ubah_alamat_penumpang').value;
  telepon  			= document.getElementById('ubah_telp_penumpang').value;
  id_discount		= document.getElementById('id_discount')? document.getElementById('id_discount').value:"";
	
	valid=true;
	
  if ((nama=='') || (nama==String.Empty)){
		Element.show('ubah_nama_invalid');
		valid=false;
	}
  
	if ((telepon=='') || !cekValueNoTelp(telepon)){
		Element.show('ubah_telp_invalid');
		valid=false;
  }
	
  if(!valid){
		return;
	}
	
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"mode=ubahdatapenumpang&no_tiket="+no_tiket+"&nama="+nama+"&alamat="+alamat+"&telepon="+telepon+"&kursi="+kursi+"&id_discount="+id_discount,
			
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				alert('Data penumpang berhasil diubah!');
			}
			else{
				alert("Terjadi kegagalan!"+request.responseText);
			}
			
			Element.hide('progress_kursi');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}


var batal_no_tiket,batal_no_kursi;

function tampilkanDialogPembatalan(no_tiket,no_kursi){
	document.getElementById('batal_username').value	= '';
	document.getElementById('batal_password').value	= '';
	dialog_pembatalan.show();
	batal_no_tiket	= no_tiket;
	batal_no_kursi	= no_kursi;
}

function batal(no_tiket,no_kursi){
	
	if(confirm("Apakah anda yakin akan membatalkan tiket ini?")){
	
		new Ajax.Request("reservasi.php?sid="+SID,{
			asynchronous: true,
			method: "post",
			parameters: "mode=pembatalan"+
				"&no_tiket="+no_tiket+
				"&no_kursi="+no_kursi+
				"&username="+batal_username.value+
				"&password="+batal_password.value,
			onLoading: function(request) {
				Element.show('loading_data_penumpang');
			},
			onComplete: function(request) 
			{
			
			},
			onSuccess: function(request) {
				
				if(request.responseText==1){
					document.getElementById('dataPelanggan').innerHTML='';
					getUpdateMobil();
				}
				else{
					alert("Anda tidak memiliki wewenang untuk membatalkan tiket ini!");
				}
				
				Element.hide('loading_data_penumpang');
				dialog_pembatalan.hide();
			},
			onFailure: function(request) 
			{
			}
			})  
	}
	
	return false;
		
}

function batalPaket(no_tiket){
	
	if(confirm("Apakah anda yakin akan membatalkan paket ini?")){
	
		new Ajax.Request("reservasi.php?sid="+SID,{
			asynchronous: true,
			method: "get",
			parameters: "mode=paketbatal&no_tiket="+no_tiket,
			onLoading: function(request) {
				Element.show('loading_data_penumpang');
			},
			onComplete: function(request) 
			{
			
			},
			onSuccess: function(request) {			
				
				if(request.responseText==1){
					document.getElementById('dataPelanggan').innerHTML='';
					getUpdateMobil();
					getUpdatePaket();
				}
				else{
					alert("Anda tidak memiliki wewenang untuk membatalkan tiket ini!");
				}
				
				Element.hide('loading_data_penumpang');
			},
			onFailure: function(request) 
			{
			}
			})  
	}
	
	return false;
		
}

function hitungHargaPaket(jenis_layanan){
	
	berat	 			= document.getElementById('dlg_paket_berat').value;
	jumlah_koli	= 1; //document.getElementById('dlg_paket_jumlah_koli').value;
	
	if(berat<=0 || berat==''){
		document.getElementById('rewrite_dlg_paket_harga_paket_show').innerHTML	=  0;
		return;
	}
	
	switch(jenis_layanan){
		case '1':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_1').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_1').value; 
		break;
		
		case '2':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_2').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_2').value; 
		break;
		
		case '3':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_3').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_3').value; 
		break;
		
		case '4':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_4').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_4').value; 
		break;
		
		case '5':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_5').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_5').value;
		break;
		
		case '6':
			harga_paket_kg_pertama		= document.getElementById('dlg_paket_harga_kg_pertama_6').value; 
			harga_paket_kg_berikutnya	= document.getElementById('dlg_paket_harga_kg_berikutnya_6').value; 
		break;
	}
	
	total_harga_paket	= (harga_paket_kg_pertama*1 + harga_paket_kg_berikutnya*(berat-1))*jumlah_koli;
	
	document.getElementById('rewrite_dlg_paket_harga_paket_show').innerHTML	=  FormatUang(total_harga_paket,'.');
}

function pesanPaket(){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  						= document.getElementById('p_tgl_user').value;
  kode_jadwal  		= document.getElementById('p_jadwal').value;  
	nama_pengirim		= document.getElementById('dlg_paket_nama_pengirim').value;	
	alamat_pengirim	= document.getElementById('dlg_paket_alamat_pengirim').value;	
  telepon_pengirim= document.getElementById('dlg_paket_telp_pengirim').value;	
	nama_penerima		= document.getElementById('dlg_paket_nama_penerima').value;		
	alamat_penerima	= document.getElementById('dlg_paket_alamat_penerima').value;		
  telepon_penerima= document.getElementById('dlg_paket_telp_penerima').value;		
  keterangan			= document.getElementById('dlg_paket_keterangan').value;		
  jumlah_koli			= 1;//document.getElementById('dlg_paket_jumlah_koli').value;			
  berat						= document.getElementById('dlg_paket_berat').value;			
  //jenis_barang		= ''; //document.getElementById('dlg_paket_jenis_barang').value;
  layanan					= document.getElementById('dlg_paket_layanan').value;			
  cara_bayar			= document.getElementById('dlg_paket_cara_bayar').value;			
	
	valid=true;
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  if ((nama_pengirim=='') || (nama_pengirim==String.Empty)){
		Element.show('dlg_paket_nama_pengirim_invalid');
		valid=false;
	}
	
	if ((telepon_pengirim=='') || !cekValueNoTelp(telepon_pengirim)){
		Element.show('dlg_paket_telp_pengirim_invalid');
		valid=false;
  }
	
	if ((nama_penerima=='') || (nama_penerima==String.Empty)){
		Element.show('dlg_paket_nama_penerima_invalid');
		valid=false;
	}

	if ((telepon_penerima=='') || !cekValueNoTelp(telepon_penerima)){
		Element.show('dlg_paket_telp_penerima_invalid');
		valid=false;
  }
	
	if (keterangan==''){
		Element.show('dlg_paket_keterangan_invalid');
		valid=false;
  }
	
	if (!cekValue(jumlah_koli)){
		Element.show('dlg_paket_koli_invalid');
		valid=false;
  }
	
	if(jumlah_koli=='' || jumlah_koli<=0){
		//Element.show('dlg_paket_koli_invalid');
		valid=false;
	}
	
	/*if (!cekValue(berat)){
		Element.show('dlg_paket_berat_invalid');
		valid=false;
  }*/
	
  if(!valid){
		return;
	}

  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"nama_pengirim="+nama_pengirim+"&alamat_pengirim="+alamat_pengirim+"&telepon_pengirim="+telepon_pengirim+
			"&nama_penerima="+nama_penerima+"&alamat_penerima="+alamat_penerima+"&telepon_penerima="+telepon_penerima+
			"&keterangan="+keterangan+"&jumlah_koli="+jumlah_koli+
			"&berat="+berat+"&layanan="+layanan+
			"&cara_bayar="+cara_bayar+
			"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&mode=pakettambah",
			
    onLoading: function(request) 
    {
			Element.show('progress_paket');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				resetIsianPaket();
				getUpdatePaket(); // update layout mobil :
				//Element.hide('progress_paket');
				dialog_paket.hide();
				
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			//Element.hide('progress_paket');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function resetIsianPaket(){
  
	Element.hide('dlg_paket_nama_pengirim_invalid');
	Element.hide('dlg_paket_telp_pengirim_invalid');
	Element.hide('dlg_paket_nama_penerima_invalid');
	Element.hide('dlg_paket_telp_penerima_invalid');
	Element.hide('dlg_paket_keterangan_invalid');
	//Element.hide('dlg_paket_koli_invalid');
	Element.hide('dlg_paket_berat_invalid');
	
	document.getElementById('dlg_paket_nama_pengirim').value		="";
	document.getElementById('dlg_paket_alamat_pengirim').value	="";	
	document.getElementById('dlg_paket_telp_pengirim').value		="";	
	document.getElementById('dlg_paket_nama_penerima').value		="";		
	document.getElementById('dlg_paket_alamat_penerima').value	="";	
	document.getElementById('dlg_paket_telp_penerima').value		="";		
	document.getElementById('dlg_paket_keterangan').value				="";
	//document.getElementById('dlg_paket_jumlah_koli').value			="";			
	document.getElementById('dlg_paket_berat').value						="";
	
	hitungHargaPaket(document.getElementById('dlg_paket_layanan').value);
}

function ambilDataPaket(no_resi){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		dialog_cari_paket.hide();
		
		document.getElementById('rewrite_ambil_paket').innerHTML="";
		
		if(no_resi==""){
			alert("Anda belum memasukkan no resi paket yang akan dicari!");
			return;
		}
		
		dialog_ambil_paket.show();
		
		new Ajax.Updater("rewrite_ambil_paket","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_tiket=" +no_resi+"&mode=paketdetail&submode=ambil",
	        onLoading: function(request){
						Element.show('progress_ambil_paket');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_ambil_paket');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
		
}

function prosesAmbilPaket(){
  
  // melakukan saving / update transaksi/
	
	no_tiket					= document.getElementById('dlg_ambil_paket_no_tiket').value;	
	nama_pengambil		= document.getElementById('dlg_ambil_paket_nama_pengambil').value;	
	no_ktp_pengambil	= document.getElementById('dlg_ambil_paket_no_ktp_pengambil').value;				
	cara_bayar				= document.getElementById('hdn_paket_cara_pembayaran').value;			
	
	if(cara_bayar==2){
		if(confirm("Paket ini akan dibayar oleh pelanggan yang mengambil paket, " + 
				"silahkan informasikan terlebih dahulu kepada pelanggan."+
				"Klik OK untuk melanjutkan proses pengambilan atau klik CANCEL untuk membatalkannya!")){
			}
			else{
				return;
			}
	}
	
	valid=true;
	Element.hide('dlg_ambil_paket_nama_pengambil_invalid');
	Element.hide('dlg_ambil_paket_no_ktp_pengambil_invalid');
	
  if (no_tiket==''){
		return false;
	}
  
	if (nama_pengambil=='' || (nama_pengambil==String.Empty)){
		Element.show('dlg_ambil_paket_nama_pengambil_invalid');
		valid=false;
  }
	
	if ((no_ktp_pengambil=='') || (no_ktp_pengambil==String.Empty)){
		Element.show('dlg_ambil_paket_no_ktp_pengambil_invalid');
		valid=false;
	}
	
  if(!valid){
		return;
	}

  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"no_tiket="+no_tiket+"&nama_pengambil="+nama_pengambil+"&no_ktp_pengambil="+no_ktp_pengambil+
			"&mode=paketambil",
			
    onLoading: function(request) 
    {
			Element.show('progress_ambil_paket');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				Element.hide('progress_ambil_paket');
				dialog_ambil_paket.hide();
			}
			else{
				alert("Terjadi kegagalan pengambilan paket!");
			}
			
			//Element.hide('progress_paket');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  }) 
}


function saveWaitingList(){
  
	if (document.getElementById('txt_spj').value!=""){
		alert("Mobil sudah diberangkatkan, tidak dapat menambahkan ke daftar tunggu!");
		exit;
	}
	
  // melakukan saving / update waiting list/
	tgl  			= document.getElementById('p_tgl_user').value;
  kodejadwal= document.getElementById('p_jadwal').value;
	telepon  	= document.getElementById('ftelp');
	nama  		= document.getElementById('fnama');
	alamat  	= document.getElementById('fadd').value;
	jum_kursi_wl= document.getElementById('jumlahpesanwl').value;
	
	isvalid=true;
	
	if (telepon.value=='' || telepon.value==String.Empty){
		telepon.style.background="red";
    isvalid=false;
	  //return;
  }
	
  // validasi	
	if (nama.value=='' || nama.value==String.Empty){
		nama.style.background="red";
    isvalid=false;
	  //return;
  }
	
	if(!isvalid){
		exit;
	}
  
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "mode=waitinglist&aksi=tambah&nama="+nama.value+"&ket="+alamat+"&telepon="+telepon.value+"&tanggal=" +tgl+ "&kodejadwal="+kodejadwal+"&jum_kursi="+jum_kursi_wl,
    onLoading: function(request) 
    {
			Element.show('progress_waiting_list');
    },
    onComplete: function(request) {			
    },
    onSuccess: function(request) 
    {
			if(request.responseText==0){
				getUpdateMobil();
				document.getElementById('fnama').value='';
				document.getElementById('fadd').value='';
				document.getElementById('ftelp').value='';
				document.getElementById("jumlahpesanwl").selectedIndex="0";
			}
			else if(request.responseText==1){
				alert("Jumlah Daftar Tunggu sudah penuh!");
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
}

function deleteWaitingList(id_waiting_list){
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "id_waiting_list="+id_waiting_list+"&mode=waiting_list&aksi=hapus",
    onLoading: function(request) 
    {
			Element.show('progress_waiting_list');
    },
    onComplete: function(request) 
    {			
    },
    onSuccess: function(request) 
    {
			getUpdateWaitingList(); // update layout waiting list
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function tampilkanDialogCetakUlangTiket(no_tiket){
	document.getElementById('cetak_ulang_tiket_username').value	= '';
	document.getElementById('cetak_ulang_tiket_password').value	= '';
	dialog_cetak_ulang_tiket.show();
}

function tampilkanDialogCetakUlangTiketOTP(no_tiket){
	document.getElementById('cetak_ulang_tiket_password_otp').value	= '';
	dialog_cetak_ulang_tiket_otp.show();
}

function tampilkanDialogDiscount(no_tiket,jenis_penumpang){
	document.getElementById('hdn_bayar_no_tiket').value=no_tiket;
	getDataListDiscount(1,jenis_penumpang);
	document.getElementById('korek_disc_username').value	= '';
	document.getElementById('korek_disc_password').value	= '';
	dialog_discount.show();
}

function ubahTiket(){
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_tiket="+dlg_no_tiket.innerHTML+
								"&nama="+dlg_nama.value+
								"&add="+dlg_address.value+
								"&telepon="+dlg_telp.value+
								"&hp="+dlg_hp.value+
								"&discount="+dlg_discount.value+
								"&mode=ubahTiket",
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
       alert("Data tiket telah diubah!");
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function cetakTiketCepat(kode_booking){
	document.getElementById('kode_booking_go_show').value=kode_booking;
	dialog_pembayaran.show(kode_booking);
}

function CetakTiket(jenis_pembayaran){
  //mencetak tiket 
	
	if(!document.getElementById('kode_booking')){
		kode_booking	= document.getElementById('kode_booking_go_show').value;
		cetak_tiket	= 0;
	}
	else{
		kode_booking	= document.getElementById('kode_booking').value;
		cetak_tiket	= document.getElementById('cetak_tiket')?document.getElementById('cetak_tiket').value:0;
	}
	
	if(!document.getElementById('flag_paket')){
		
		if(!document.getElementById('no_tiket')){
			no_tiket	= "";
		}
		else{
			no_tiket		= document.getElementById('no_tiket').value;
		}
		
		layout_kursi= document.getElementById('hide_layout_kursi').value;
		parameter		= "tiket.php?sid="+SID+"&cetak_tiket="+cetak_tiket+
			"&kode_booking="+kode_booking+"&no_tiket="+no_tiket+
			"&jenis_pembayaran="+jenis_pembayaran+
			"&layout_kursi="+layout_kursi;
	}
	else{
		no_tiket		= document.getElementById('kode_booking').value;
		parameter		= "tiket_paket.php?sid="+SID+"&no_tiket='"+no_tiket+"'&jenis_pembayaran="+jenis_pembayaran;
	}
	
  Start(parameter);
	
	dialog_pembayaran.hide();
	getUpdateMobil();
	getUpdatePaket();
	
	document.getElementById("dataPelanggan").innerHTML="";
}

function CetakTiketByVoucher(kode_voucher){
  //mencetak tiket 
	
	if(!document.getElementById('kode_booking')){
		kode_booking	= document.getElementById('kode_booking_go_show').value;
		cetak_tiket	= 0;
	}
	else{
		kode_booking	= document.getElementById('kode_booking').value;
		cetak_tiket	= document.getElementById('cetak_tiket')?document.getElementById('cetak_tiket').value:0;
	}
	
	if(document.getElementById('no_tiket')){
		no_tiket	= document.getElementById('no_tiket').value;
	}
	else{
		no_tiket	= document.getElementById('no_tiket_goshow').value
	}

	layout_kursi= document.getElementById('hide_layout_kursi').value;
	
	parameter		= "tiket.php?sid="+SID+"&cetak_tiket="+cetak_tiket+
		"&kode_booking="+kode_booking+"&no_tiket="+no_tiket+
		"&jenis_pembayaran=3"+
		"&layout_kursi="+layout_kursi+"&kode_voucher="+kode_voucher;

  Start(parameter);
	
	dialog_pembayaran.hide();
	getUpdateMobil();
	getUpdatePaket();
	
	document.getElementById("dataPelanggan").innerHTML="";
}

function cetakUlangTiket(){

	username	= document.getElementById('cetak_ulang_tiket_username').value;
	password	= document.getElementById('cetak_ulang_tiket_password').value;
	no_tiket	= document.getElementById('no_tiket').value;
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=cetakulangtiket&no_tiket="+no_tiket+"&username="+username+"&password="+password,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			dialog_cetak_ulang_tiket.hide();
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}

function cetakUlangTiketByOTP(){
	password	= document.getElementById('cetak_ulang_tiket_password_otp').value;
	no_tiket	= document.getElementById('no_tiket').value;
	
	if(password==''){
		alert('Anda belum memasukkan kode OTP!');
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=cetakulangtiketotp&no_tiket="+no_tiket+"&password="+password,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			dialog_cetak_ulang_tiket_otp.hide();
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}

/*function PilihTiket(){
  //mencetak SPJ
	
	tgl_berangkat  	= document.getElementById('p_tgl_user').value;
  kode_jadwal 		= document.getElementById('p_jadwal').value;
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
  Start("tiket.php?sid={SID}&tgl_berangkat="+tgl_berangkat+"&kode_jadwal="+kode_jadwal+"&mode=pilih_tiket&layout_kursi="+layout_kursi);
}*/

function CetakSPJ(){
  //mencetak SPJ
	tgl_berangkat  	= document.getElementById('p_tgl_user').value;
  kode_jadwal 		= document.getElementById('p_jadwal').value; 
  sopir_dipilih		= document.getElementById('list_sopir').value; 
	mobil_dipilih		= document.getElementById('list_mobil').value; 
	no_spj					= document.getElementById('txt_spj').value;
	parameter_biaya_bbm	= document.getElementById('biaya_bbm')?"&biaya_bbm="+document.getElementById('biaya_bbm').value:"";	
  
	if(sopir_dipilih!='' && mobil_dipilih!=''){	
		Start("SPJ.php?sid="+SID+"&tgl_berangkat="+tgl_berangkat+"&kode_jadwal="+kode_jadwal+"&mobil_dipilih="+mobil_dipilih+"&sopir_dipilih="+sopir_dipilih+"&no_spj="+no_spj+parameter_biaya_bbm+"&aksi=1");
		getUpdateMobil();
		dialog_SPJ.hide();
	}
	else{
		alert("Anda belum memilih kendaraan atau nama sopir!");
	}
}

function cariDataPelangganByTelp(no_telp){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	
	document.getElementById("ftelp").style.background="";
	document.getElementById("fnama").style.background="";
	
	if(no_telp==""){
		return;
	}
	
	if(!cekValueNoTelp(no_telp)){	
		document.getElementById("ftelp").style.background="red";
		exit;
	}
		
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_telp="+no_telp+
								"&mode=periksa_no_telp&paket=0",
    onLoading: function(request) 
    {
			Element.hide('label_not_found');
			Element.show('progress_cari_penumpang');
    },
    onComplete: function(request) 
    {
			Element.hide('progress_cari_penumpang');
    },
    onSuccess: function(request) 
    {		
			if(request.responseText==0){
				Element.show('label_not_found');
			}
			else{
				eval(request.responseText);
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function cariDataPelangganByTelp4Paket(no_telp,flag){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	
	Element.hide('dlg_paket_telp_pengirim_invalid');
	Element.hide('dlg_paket_nama_pengirim_invalid');
	Element.hide('dlg_paket_telp_penerima_invalid');
	Element.hide('dlg_paket_nama_penerima_invalid');
	
	if(no_telp==""){
		return;
	}
	
	if(!cekValueNoTelp(no_telp)){	
		alert("Nomor telepon yang anda masukkan tidak benar!");
		return;
	}
		
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "no_telp="+no_telp+
								"&mode=periksa_no_telp&paket=1&flag="+flag,
    onLoading: function(request) 
    {
			//Element.hide('label_not_found');
			//Element.show('progress_cari_penumpang');
			if(flag==1){
				document.getElementById("dlg_paket_nama_pengirim").value="Mencari...";
			}
			else{
				document.getElementById("dlg_paket_nama_penerima").value="Mencari...";
			}
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {		
			if(request.responseText==0){
				//Element.show('label_not_found');
				if(flag==1){
					document.getElementById("dlg_paket_nama_pengirim").value="";
				}
				else{
					document.getElementById("dlg_paket_nama_penerima").value="";
				}
			}
			else{
				eval(request.responseText);
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function periksaJadwal(no_telp){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		//no_telp=document.getElementById('ftelp').value;
		document.getElementById('rewrite_keberangkatan_pelanggan').innerHTML="";
		
		if(no_telp==""){
			alert("Anda belum memasukkan no telepon yang akan dicari!");
			return;
		}
		
		dialog_cari_jadwal.show();
		
		new Ajax.Updater("rewrite_keberangkatan_pelanggan","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_telp=" +no_telp+"&mode=cari_jadwal_pelanggan",
	        onLoading: function(request){
						Element.show('progress_cari_jadwal');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_cari_jadwal');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
}

function periksaPaket(no_resi){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		
		document.getElementById('rewrite_cari_paket').innerHTML="";
		
		if(no_resi==""){
			alert("Anda belum memasukkan no resi paket yang akan dicari!");
			return;
		}
		
		dialog_cari_paket.show();
		
		new Ajax.Updater("rewrite_cari_paket","reservasi.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_resi=" +no_resi+"&mode=cari_paket",
	        onLoading: function(request){
						Element.show('progress_cari_paket');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_cari_paket');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
}

function koreksiDiscount(no_tiket){

	jenis_discount=document.getElementById('opt_jenis_discount_koreksi').value;
	username	= document.getElementById('korek_disc_username').value;
	password	= document.getElementById('korek_disc_password').value;
	
	if(jenis_discount==''){
		jum_discount=0;
	}
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=beridiscount&jenis_discount="+jenis_discount+"&no_tiket="+no_tiket+"&username="+username+"&password="+password,
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
      if(request.responseText==1){
				alert("Pengubahan harga berhasil disimpan");
				getUpdateMobil(); // update layout mobil :
				dialog_discount.hide();
			}
			else if(request.responseText==0){
				document.getElementById('korek_disc_username').value	= '';
				document.getElementById('korek_disc_password').value	= '';
				alert("Anda tidak memiliki otoritas untuk melakukan proses ini");
			}
			else{
				alert("Terjadi kegagalan dalam pengubahan harga");
			}
			
			
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}

function koreksiAsuransi(no_tiket){
	
	plan_asuransi=document.getElementById('plan_asuransi').value;
	username	= document.getElementById('korek_asuransi_username').value;
	password	= document.getElementById('korek_asuransi_password').value;
	
	
	if(username=='' || password==''){
		alert('Anda belum memasukkan username dan password yang memiliki otoritas untuk proses ini!');
		return;
	}
	
	tgl_lahir="";
	
	if(plan_asuransi>0){
		tgl_lahir	= document.getElementById("opt_thn_lahir").value+"-"+ Right("0"+document.getElementById("opt_bln_lahir").value,2)+"-"+ Right("0"+document.getElementById("opt_tgl_lahir").value,2);
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: "mode=koreksiasuransi&plan_asuransi="+plan_asuransi+"&tgl_lahir="+tgl_lahir+"&no_tiket="+no_tiket+"&username="+username+"&password="+password,
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
    },
    onSuccess: function(request) 
    {
			document.getElementById('korek_asuransi_username').value	= '';
			document.getElementById('korek_asuransi_password').value	= '';
			
      if(request.responseText==1){
				alert("Koresi asuransi berhasil dilakukan");
				dialog_koreksi_asuransi.hide();
			}
			else if(request.responseText==0){
				alert("Anda tidak memiliki otoritas untuk melakukan proses ini");
			}
			else{
				alert("Terjadi kegagalan dalam koreksi asuransi");
			}
			
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })
	
	Element.hide('progress_kursi');
}


function showDialogSwapKartu(){
	tgl_berangkat = document.getElementById('p_tgl_user').value;
  kode_jadwal 	= document.getElementById('p_jadwal').value; 
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	
	this.open("member_transaksi.php?sid="+SID+"&tgl_berangkat="+tgl_berangkat+"&kode_jadwal="+kode_jadwal+"&layout_kursi="+layout_kursi,"CtrlWindow","top=200,left=200,width=600,height=400,scrollbars=1,resizable=0");	
	
}

function bayarByKartu(kode_book){
	tgl_berangkat = document.getElementById('p_tgl_user').value;
  kode_jadwal 	= document.getElementById('p_jadwal').value; 
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	this.open("member_transaksi.php?sid="+SID+"&mode=pilih_bayar&kode_book="+kode_book+"&kode_jadwal="+kode_jadwal+"&tgl_berangkat="+tgl_berangkat+"&layout_kursi="+layout_kursi,"CtrlWindow","top=200,left=200,width=600,height=400,scrollbars=1,resizable=0");	
	
}

function setFlagMutasi(){
	
	if(document.getElementById('flag_mutasi').value!=1){
		if(confirm("Anda akan mengaktifkan mode mutasi, " + 
			"ketika anda memilih kursi, maka secara otomatis kursi lama akan dimutasikan ke kursi yang baru."+
			"Klik OK untuk melanjutkan proses mutasi atau klik CANCEL untuk keluar dari mode mutasi!")){
			lanjut = true;
		}
		else{
			lanjut = false;
		}
	}
	else{
		lanjut=true;
	}
	
	if(lanjut){
		document.getElementById('flag_mutasi').value	= 1-document.getElementById('flag_mutasi').value;
		
		if(document.getElementById('flag_mutasi').value==1){
			document.getElementById('btn_mutasi').value="BATALKAN MUTASI";
		}
		else{
			document.getElementById('btn_mutasi').value="MUTASI PENUMPANG";
		}
	}
}

function mutasiPenumpang(no_kursi){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  kode_jadwal  	= document.getElementById('p_jadwal').value;  
	no_tiket			= document.getElementById('no_tiket').value;
	layout_kursi	= document.getElementById('hide_layout_kursi').value;
	
	if(!confirm("Apakah anda yakin akan memindahkan penumpang ke kursi ini?")){
		exit;
	}
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"no_tiket="+no_tiket+"&no_kursi="+no_kursi+"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&layout_kursi="+layout_kursi+"&mode=mutasipenumpang",
			
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				document.getElementById('flag_mutasi').value=0;
				document.getElementById('dataPelanggan').innerHTML='';
				
				getUpdateMobil(); // update layout mobil :
				
			}
			else if(request.responseText==2){
				getUpdateMobil(); // update layout mobil :
				alert("Kursi yang dituju sudah dipesan oleh penumpang lain!");
			}
			else{
				alert("Terjadi kegagalan!"+request.responseText);
			}
			
			Element.hide('progress_kursi');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function setFlagMutasiPaket(){
	
	if(document.getElementById('flag_mutasi_paket').value!=1){
		if(confirm("Anda akan mengaktifkan mode mutasi, " + 
			"ketika anda memilih menekan tombol mutasi pada daftar paket, maka secara otomatis paket lama akan dimutasikan ke jadwal yang baru."+
			"Klik OK untuk melanjutkan proses mutasi atau klik CANCEL untuk keluar dari mode mutasi!")){
			lanjut = true;
		}
		else{
			lanjut = false;
		}
	}
	else{
		lanjut=true;
	}
	
	if(lanjut){
		document.getElementById('flag_mutasi_paket').value	= 1-document.getElementById('flag_mutasi_paket').value;
		
		if(document.getElementById('flag_mutasi_paket').value==1){
			document.getElementById('btn_mutasi_paket').value="     Batalkan Mutasi     ";
		}
		else{
			document.getElementById('btn_mutasi_paket').value="      Mutasi Paket       ";
		}
		
		getUpdatePaket();
	}
}

function mutasiPaket(){
  
  // melakukan saving / update transaksi/
	
	if(!document.getElementById('p_jadwal')){
		alert("Anda belum memilih jadwal keberangkatan!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  kode_jadwal  	= document.getElementById('p_jadwal').value;  
	no_tiket			= document.getElementById('kode_booking').value;
	
	if(!confirm("Apakah anda yakin akan memindahkan paket ke jadwal ini?")){
		exit;
	}
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
	
  new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"no_tiket="+no_tiket+"&tanggal=" +tgl+ "&kode_jadwal="+kode_jadwal+
			"&no_spj="+no_spj+"&mode=mutasipaket",
			
    onLoading: function(request) 
    {
			Element.show('progress_paket');
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {			
			
			if(request.responseText==1){
				document.getElementById('flag_mutasi_paket').value=0;
				document.getElementById('dataPelanggan').innerHTML='';
				
				getUpdateMobil(); // update layout mobil :
				getUpdatePaket();
				
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			//Element.hide('progress_paket');
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function setComboTglLahir(tgl_dipilih,bulan,tahun){
	opt_tanggal_lahir	= "<select id='opt_tgl_lahir'>";
	
	if(((bulan%2==0 && bulan<8) || (bulan%2>0 && bulan>=8)) && bulan!=2){
		tgl_max=30;
	}
	else if(bulan!=2){
		tgl_max=31;
	}
	else{
		if(tahun%4!=0){
			tgl_max=28;
		}
		else{
			tgl_max=29;
		}
	}
	
	for(tgl=1;tgl<=tgl_max;tgl++){
		temp_tgl	= "0"+tgl;
		
		if(Right(temp_tgl,2)!=Right("0" + tgl_dipilih,2)){selected="";}else{selected="selected";}
		opt_tanggal_lahir	+= "<option value='"+tgl+"' "+selected+">"+tgl+"</option>";
	}
			
	opt_tanggal_lahir	+= "</select>";
	
	document.getElementById('span_tgl_lahir').innerHTML=opt_tanggal_lahir;
}

function setSisaKursi(){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
	/*id_jurusan		= document.getElementById('id_jurusan_aktif').value
	tgl_berangkat = document.getElementById('p_tgl_user').value;
  jam_berangkat	= document.getElementById('jam_berangkat_aktif').value; 	
		
	new Ajax.Updater("rewrite_sisa_kursi","reservasi.php?sid="+SID, 
	{
	      asynchronous: true,
	      method: "get",
	      parameters: "mode=sisa_kursi_next&id_jurusan="+id_jurusan+"&tgl_berangkat=" +tgl_berangkat+"&jam_berangkat="+jam_berangkat,
	      onLoading: function(request){
	      },
	      onComplete: function(request) {
	      },
	      onSuccess: function(request){
					
	      },
	      onFailure: function(request) 
	      {
	         assignError(request.responseText);
	      }
	}) */
}

function hitungTotalBiayaOperasional(){
	
	biaya_sopir	= document.getElementById('biaya_sopir').value*1;
	biaya_tol		= document.getElementById('biaya_tol').value*1;
	biaya_parkir= document.getElementById('biaya_parkir').value*1;
	biaya_bbm		= document.getElementById('biaya_bbm').value*1;
	
	total_biaya	= biaya_sopir + biaya_tol + biaya_parkir + biaya_bbm;
	
	document.getElementById('biaya_total').innerHTML	= FormatUang(total_biaya,".");
}

function showInputVoucher(){
	dialog_pembayaran.hide();
	document.getElementById("input_kode_voucher").value	= "";
	dialog_voucher.show();
}

function bayarByVoucher(kode_voucher){
	
	if(kode_voucher==''){
		alert("Anda belum memasukkan kode voucher");
		return false;
	}
	
	tgl  				= document.getElementById('p_tgl_user').value;
	id_jurusan	= document.getElementById('id_jurusan_aktif').value;
	
	if(document.getElementById('no_tiket')){
		no_tiket	= document.getElementById('no_tiket').value;
	}
	else{
		no_tiket	= document.getElementById('no_tiket_goshow').value
	}
	
	new Ajax.Request("reservasi.php?sid="+SID, 
  {
    asynchronous: true,
    method: "post",
    parameters: 
			"kode_voucher="+kode_voucher+"&tanggal="+tgl+"&id_jurusan="+id_jurusan+"&no_tiket="+no_tiket+
			"&mode=bayar_by_voucher",
    onLoading: function(request) 
    {
			Element.show('progress_kursi');
    },
    onComplete: function(request) 
    {
			document.getElementById("input_kode_voucher").value="";
    },
    onSuccess: function(request) 
    {
			eval(request.responseText);
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function showDebitMember(){
	kode_booking=(!document.getElementById('kode_booking'))?document.getElementById('kode_booking_go_show').value:document.getElementById('kode_booking').value;
	StartCustomeSize("reservasi.member.debit.php?sid="+SID+"&kodebooking="+kode_booking,"600","650");
	dialog_pembayaran.hide();
}

// global
var dialog_SPJ,dialog_ubah_tiket,btn_SPJ_OK,btn_SPJ_Cancel,msg;

function init(e) {
  // inisialisasi variabel
	SID =document.getElementById("hdn_SID").value;
	harga_minimum_paket = document.getElementById("hdn_harga_minimum_paket").value;
	
	//control dialog box SPJ
	dialog_SPJ = dojo.widget.byId("dialog_SPJ");
  btn_SPJ_OK = document.getElementById("dialog_SPJ_btn_OK");
  btn_SPJ_Cancel = document.getElementById("dialog_SPJ_btn_Cancel");
  //dialog_SPJ.setCloseControl(btn_SPJ_OK);
  dialog_SPJ.setCloseControl(btn_SPJ_Cancel);
	
	//control dialog box discount
	dialog_discount = dojo.widget.byId("dialog_discount");
  btn_discount_OK = document.getElementById("dialog_discount_btn_OK");
  btn_discount_Cancel = document.getElementById("dialog_discount_btn_Cancel");
  //dialog_discount.setCloseControl(btn_discount_OK);
  dialog_discount.setCloseControl(btn_discount_Cancel);
	
	//control dialog cetak ulang tiket
	dialog_cetak_ulang_tiket		 = dojo.widget.byId("dialog_cetak_ulang_tiket");
  btn_cetak_ulang_tiket_Cancel = document.getElementById("dialog_cetak_ulang_tiket_btn_Cancel");
  dialog_cetak_ulang_tiket.setCloseControl(btn_cetak_ulang_tiket_Cancel);
	
	//control dialog cetak ulang tiket OTP
	dialog_cetak_ulang_tiket_otp			= dojo.widget.byId("dialog_cetak_ulang_tiket_otp");
  btn_cetak_ulang_tiket_Cancel_otp 	= document.getElementById("dialog_cetak_ulang_tiket_btn_Cancel_otp");
  dialog_cetak_ulang_tiket_otp.setCloseControl(btn_cetak_ulang_tiket_Cancel_otp);
	

	//control dialog box koreksi asuransi
	dialog_koreksi_asuransi = dojo.widget.byId("dialog_koreksi_asuransi");
  btn_koreksi_asuransi_OK = document.getElementById("dialog_koreksi_asuransi_btn_OK");
  btn_koreksi_asuransi_Cancel = document.getElementById("dialog_koreksi_asuransi_btn_Cancel");
  dialog_koreksi_asuransi.setCloseControl(btn_koreksi_asuransi_Cancel);
	
	//control dialog cari keberangkatan
	dialog_cari_jadwal = dojo.widget.byId("dialog_cari_keberangkatan");
  btn_cari_jadwal_Cancel = document.getElementById("dialog_cari_jadwal_btn_Cancel");
  dialog_cari_jadwal.setCloseControl(btn_cari_jadwal_Cancel);
	
	//control dialog cari paket
	dialog_cari_paket = dojo.widget.byId("dialog_cari_paket");
  btn_cari_paket_Cancel = document.getElementById("dialog_cari_paket_btn_Cancel");
  dialog_cari_paket.setCloseControl(btn_cari_paket_Cancel);
	
	//control dialog paket
	dialog_paket 						= dojo.widget.byId("dialog_isian_paket");
  dlg_paket_button_ok 		= document.getElementById("dlg_paket_button_ok");
  dlg_paket_button_cancel = document.getElementById("dlg_paket_button_cancel");
	
	//control dialog ambil paket
	dialog_ambil_paket 						= dojo.widget.byId("dialog_ambil_paket");
  dlg_ambil_paket_button_ok 		= document.getElementById("dlg_ambil_paket_button_ok");
  dlg_ambil_paket_button_cancel = document.getElementById("dlg_ambil_paket_button_cancel");
	
	//control dialog pembayaran
	dialog_pembayaran = dojo.widget.byId("dialog_pembayaran");
  btn_pembayaran_OK = document.getElementById("dialog_pembayaran_btn_OK");
  btn_pembayaran_Cancel = document.getElementById("dialog_pembayaran_btn_Cancel");
  dialog_pembayaran.setCloseControl(btn_pembayaran_Cancel);
	
	//control dialog box batal
	dialog_pembatalan = dojo.widget.byId("dialog_batal");
  dialog_pembatalan_btn_ok = document.getElementById("dialog_batal_btn_ok");
  dialog_pembatalan_btn_cancel = document.getElementById("dialog_batal_btn_Cancel");
  dialog_pembatalan.setCloseControl(dialog_pembatalan_btn_cancel);
	
	//control dialog box input voucher
	dialog_voucher = dojo.widget.byId("dialog_input_voucher");
  dialog_voucher_btn_ok = document.getElementById("dialog_input_voucher_btn_ok");
  dialog_voucher_btn_cancel = document.getElementById("dialog_input_voucher_btn_ok_cancel");
  dialog_voucher.setCloseControl(dialog_voucher_btn_cancel);
	
	getPengumuman();
	timerLayoutKursi();
	timerPengumuman();
	timerPaket();
}

dojo.addOnLoad(init);

var jdwl,tgl;

//TIMER LAYOUT KURSI
var interval_timer_layout	= 10; //detik
var interval_halt_layout = 300; //detik
var count_timer_layout_kursi	= interval_timer_layout;

function timerLayoutKursi(){
	if (count_timer_layout_kursi<1) {
		//reset count down
		count_timer_layout_kursi = interval_timer_layout;
		
		//merefresh layout mobil
		
		if(document.getElementById("rewritemobil").innerHTML!=""){
			getUpdateMobil();
		}	
	}
	
	count_timer_layout_kursi-=1;
	setTimeout("timerLayoutKursi()",1000);
	//document.title	= count_timer_layout_kursi;
}
//END TIMER LAYOUT KURSI

//TIMER PENGUMUMAN
var interval_timer_pengumuman	= 60; //detik
var count_timer_pengumuman		= interval_timer_pengumuman;

function timerPengumuman(){
	if (count_timer_pengumuman<1) {
		//reset count down
		count_timer_pengumuman = interval_timer_pengumuman;
		
		//merefresh layout mobil
		getPengumuman();
			
	}
	
	count_timer_pengumuman-=1;
	setTimeout("timerPengumuman()",1000);
}
//END TIMER PENGUMUMAN

//TIMER PAKET
var interval_timer_paket	= 10; //detik
var interval_halt_paket = 300; //detik
var count_timer_paket		= interval_timer_paket;

function timerPaket(){
	if (count_timer_paket<1) {
		//reset count down
		count_timer_paket = interval_timer_paket;
		
		//merefresh layout mobil
		if(document.getElementById("rewritepaket").innerHTML!=""){
			getUpdatePaket();
		}
			
	}
	
	count_timer_paket-=1;
	setTimeout("timerPaket()",1000);
}
//END TIMER PAKET

//-->