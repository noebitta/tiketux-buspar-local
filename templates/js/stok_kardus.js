	var TambahStok,JenisKardus,dlg_jenis_kardus_button_close;

	function init(e) 
	{
	  // inisialisasi variabel

		TambahStok  = dojo.widget.byId("dialog_TambahStok");
		JenisKardus = dojo.widget.byId("dialog_JenisKardus");
		StokOpname  = dojo.widget.byId("dialog_StokOpname");
		LogHistory  = dojo.widget.byId("dialog_LogHistory");
	
	}

	dojo.addOnLoad(init);


	function ShowTambahStok(kode)
	{

		new Ajax.Updater("tambah_stok_kardus","pengaturan_stok_kardus.php?sid="+SID, 
		{
		    asynchronous: true,
		    method: "get",
		    parameters: "mode=ShowTambahStok"+"&KodeCabang="+kode,
		    onLoading: function(request) 
		    {
				//Element.show('progress_stok_kardus');
		    },
		    onComplete: function(request) 
		    {
				
		    },
		    onSuccess: function(request) 
		    {		
				//Element.hide('progress_stok_kardus');
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})
		TambahStok.show();  
		
	}

	function TambahStokKardus(kode)
	{
		var TambahStok  	= document.getElementById('TambahStok'+kode).value;
		var StokAsal    	= document.getElementById('StokAsal'+kode).value;
		var KodeCabang  	= document.getElementById('KodeCabang').value;


		if(TambahStok == "")
		{
			alert('Gagal menambah data, Data tidak terisi dengan benar!');
			exit;
		}

		new Ajax.Request("pengaturan_stok_kardus.php?sid="+SID, 
	  	{
		    asynchronous: true,
		    method: "get",
		    parameters: "IdJenisKardus="+kode+

		    			"&KodeCabang="+KodeCabang+
		    			"&TambahStok="+TambahStok+
		    			"&StokAsal="+StokAsal+
		    			"&mode=TambahStokKardus",
		    onLoading: function(request) 
		    {
			},
		    onComplete: function(request) 
		    {
		    	//Element.hide('progress_stok_kardus');
			},
		    onSuccess: function(request) 
		    {
		    	ShowTambahStok(KodeCabang);	
		    	alert('Tambah Data Berhasil!');
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  

	}

	function ShowStokOpname(kode)
	{
		new Ajax.Updater("stok_opname_kardus","pengaturan_stok_kardus.php?sid="+SID, 
		{
		    asynchronous: true,
		    method: "get",
		    parameters: "mode=ShowStokOpname"+"&KodeCabang="+kode,
		    onLoading: function(request) 
		    {
				Element.show('progress_opname_kardus');
		    },
		    onComplete: function(request) 
		    {
				Element.hide('progress_opname_kardus');
		    },
		    onSuccess: function(request) 
		    {		
				
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  
		StokOpname.show();

	}

	function StokOpnameKardus(kode)
	{
		var StokOpname  = document.getElementById('StokOpname'+kode).value;
		var StokAsal    = document.getElementById('StokAsal'+kode).value;
		var Keterangan  = document.getElementById('Keterangan'+kode).value;
		var KodeCabang  = document.getElementById('KodeCabang').value;

		if(TambahStok == "")
		{
			alert('Gagal menambah data, Data tidak terisi dengan benar!');
			exit;
		}

		new Ajax.Request("pengaturan_stok_kardus.php?sid="+SID, 
	  	{
		    asynchronous: true,
		    method: "get",
		    parameters: "IdStokKardus="+kode+
		    			"&KodeCabang="+KodeCabang+
		    			"&StokOpname="+StokOpname+
		    			"&StokAsal="+StokAsal+
		    			"&Keterangan="+Keterangan+
		    			"&mode=StokOpnameKardus",
		    onLoading: function(request) 
		    {
			},
		    onComplete: function(request) 
		    {
			},
		    onSuccess: function(request) 
		    {
		    	ShowStokOpname(KodeCabang);
		    	alert('Stok Opname Berhasil!');	
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  

	}



	function ShowJenisKardus()
	{
		
		new Ajax.Updater("jenis_kardus","pengaturan_stok_kardus.php?sid="+SID, 
		{
		    asynchronous: true,
		    method: "get",
		    parameters: "mode=JenisKardus",
		    onLoading: function(request) 
		    {
				Element.show('progress_jenis_kardus');
		    },
		    onComplete: function(request) 
		    {
				Element.hide('progress_jenis_kardus');
		    },
		    onSuccess: function(request) 
		    {		
				
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  
		JenisKardus.show();
		
	}

	function TambahJenisKardus()
	{
		IdJenisKardus= document.getElementById('IdJenisKardus').value;
		JenisKardus  = document.getElementById('NamaJenisKardus').value;
		Keterangan   = document.getElementById('Keterangan').value; 	

		if(JenisKardus == "")
		{
			alert('Gagal menambah data, Data tidak terisi dengan benar!');
			exit;
		}

		new Ajax.Request("pengaturan_stok_kardus.php?sid="+SID, 
	  	{
		    asynchronous: true,
		    method: "get",
		    parameters: "IdJenisKardus="+IdJenisKardus+"&JenisKardus="+JenisKardus+"&Keterangan="+Keterangan+"&mode=TambahJenisKardus",
		    onLoading: function(request) 
		    {
			},
		    onComplete: function(request) 
		    {
			},
		    onSuccess: function(request) 
		    {
		    	
		    	alert('Tambah Data Berhasil!');	
		    	ShowJenisKardus();		
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  

	}

	function HapusJenisKardus(kode)
	{
		if(confirm("Data ini berhubungan dengan data stok kardus, Apakah anda yakin akan menghapus data ini?"))
		{
			new Ajax.Request("pengaturan_stok_kardus.php?sid="+SID, 
		  	{
			    asynchronous: true,
			    method: "get",
			    parameters: "IdJenisKardus="+kode+"&mode=HapusJenisKardus",
			    onLoading: function(request) 
			    {
				},
			    onComplete: function(request) 
			    {
				},
			    onSuccess: function(request) 
			    {

					alert('Hapus Data Berhasil!');	
					ShowJenisKardus();	
				},
			    onFailure: function(request) 
			    {
			       
			    }
			})  
		}
			
	}

	function EditJenisKardus(kode)
	{
		JenisKardus  = document.getElementById('NamaJenisKardus'+kode).value;
		Keterangan   = document.getElementById('Keterangan'+kode).value; 	
		new Ajax.Request("pengaturan_stok_kardus.php?sid="+SID, 
	  	{
		    asynchronous: true,
		    method: "get",
		    parameters: "IdJenisKardus="+kode+"&JenisKardus="+JenisKardus+"&Keterangan="+Keterangan+"&mode=EditJenisKardus",
		    onLoading: function(request) 
		    {
			},
		    onComplete: function(request) 
		    {
			},
		    onSuccess: function(request) 
		    {

				alert('Edit Data Berhasil!');	
				ShowJenisKardus();			
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  

	}

	function ShowLogHistory(kode)
	{
		new Ajax.Updater("log_history_kardus","pengaturan_stok_kardus.php?sid="+SID, 
		{
		    asynchronous: true,
		    method: "get",
		    parameters: "mode=ShowLogHistory"+"&KodeCabang="+kode,
		    onLoading: function(request) 
		    {
				Element.show('progress_log_kardus');
		    },
		    onComplete: function(request) 
		    {
				Element.hide('progress_log_kardus');
		    },
		    onSuccess: function(request) 
		    {		
				
			},
		    onFailure: function(request) 
		    {
		       
		    }
		})  
		LogHistory.show();

	}
