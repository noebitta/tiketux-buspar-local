function findPosY(obj) {
  var curtop = 0;

  if(obj.offsetParent) {
    while(1) {

      curtop += obj.offsetTop;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;

    }
  }
  else if(obj.y) {
    curtop += obj.y;
  }

  return curtop;
}

function findPosX(obj) {
  var curleft = 0;
	
  if(obj.offsetParent) {
    while(1) {
      curleft += obj.offsetLeft;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;
    }
  } else if(obj.x) {
    curleft += obj.x;
  }
	
  obj.style.position = "static";

  return curleft;
}

function cekValueNoTelp(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function validasiNoTelp(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function validasiAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function echeck(str){

	var at="@"
	var dot="."
	var lat=str.indexOf(at)
	var lstr=str.length
	var ldot=str.indexOf(dot)
	
	if (str.indexOf(at)==-1){
	  return false
	}

	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
	  return false
	}

	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
	   return false
	}

	if (str.indexOf(at,(lat+1))!=-1){
	   return false
	}

	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
	   return false
	}

	if (str.indexOf(dot,(lat+2))==-1){
	   return false
	}
	
	if (str.indexOf(" ")!=-1){
	   return false
	}

 	return true;
}

function submitPilihanMenu(menu_dipilih){
	SID 	= document.getElementById("hdn_SID").value;

	if(menu_dipilih=='top_menu_operasional'){
		document.form_pilih_menu.action ="./menu_operasional.php";
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_reservasi'){
		document.form_pilih_menu.action ="./menu_lap_reservasi.php";
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_lap_keuangan'){
		document.form_pilih_menu.action ="./menu_lap_keuangan.php";
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_tiketux'){
		document.form_pilih_menu.action ="./menu_tiketux.php";
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_pengelolaan_member'){
		document.form_pilih_menu.action ="./menu_pengelolaan_member.php";
		document.form_pilih_menu.submit();
	}
	else if(menu_dipilih=='top_menu_pengaturan'){
		document.form_pilih_menu.action ="./menu_pengaturan.php";
		document.form_pilih_menu.submit();
	}
}

function setMenuOver(menu_dipilih){

	/*if(menu_dipilih.className!= "top_menu_on_select"){
		menu_dipilih.setAttribute("class", "top_menu_on_focus");
	}*/
}

function setMenuOut(menu_dipilih){

  /*if(menu_dipilih.className!= "top_menu_on_select"){
		menu_dipilih.setAttribute("class", "top_menu_lost_focus");
	}*/
}

function openNewPage(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function formatAngka(num) {
	num = num.toString().replace(/\$|\,/g,'');
	
	if(isNaN(num)) num = "0";
	
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	
	if(cents<10) cents = "0" + cents;
	
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+
		num.substring(num.length-(4*i+3));
	
	return (((sign)?'':'-') + num);
}

function formatAngkaKalkulasi(str){
	hasil	=str.toString().replace(/\./g,'');
	return hasil;
}

function ubahPassword(passlama,passbaru,konfpassbaru){

  var valid=true;

  valid = validasiInput(passlama) && valid;
  valid = validasiInput(passbaru) && valid;
  valid = validasiInput(konfpassbaru) && valid;

  if(!valid){
    return false;
  }

  if(passbaru.value!=konfpassbaru.value){
		alert("Konfirmasi Password Baru anda tidak sama dengan Password Baru yang anda masukkan!");
		return false;
	}
	
	new Ajax.Request("auth.php?sid="+SID,{
			asynchronous: true,
			method: "get",
			parameters: "mode=3&passlama="+passlama.value+"&passbaru="+passbaru.value,
			onLoading: function(request){
				
			},
	    onComplete: function(request){
			},
			onSuccess: function(request){

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            alert("Password telah BERHASIL diubah!");
            popupwrapper.hide();
          }
          else {
            passlama.value="";
            passbaru.value="";
            konfpassbaru.value=""
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          passlama.value="";
          passbaru.value="";
          konfpassbaru.value=""
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }

			},
			onFailure: function(request) 
			{
				
			}
		})
}

function showUbahPassword(){
  setPopup(400,180);

  popupwrapper	= dojo.widget.byId("popupcontainer");

  new Ajax.Updater("popupcontent","auth.php",{
    asynchronous: true,
    method: "post",
    parameters: "mode=2",
    onLoading: function(request) {
    },
    onComplete: function(request) {
    },
    onSuccess: function(request) {
      popupwrapper.show();
    },
    onFailure: function(request) {
      assignError(request.responseText);
    }
  })
}

function strClearence(str){
	string_output	= str.replace(/&/gi,"%26");
	return string_output;
}

function setLayoutTable(tableheader,tablecontent,wrappercontent){

  var tablewidth=0;
  var row=0;
  var i=0;
  var colwidth=0;

  var ihead1= 0,ihead2=0;
  var colspanhead=0;
  var icolspan=0;
  var minwidthcol=60;
  var maxwidthcol=400;

  while (tablecontent.rows[row].cells[i]) {

    if (tablecontent.rows[row].cells[i].offsetWidth < minwidthcol) {
      colwidth = minwidthcol;
    } else if (tablecontent.rows[row].cells[i].offsetWidth <= maxwidthcol) {
      colwidth = tablecontent.rows[row].cells[i].offsetWidth;
    } else {
      tablecontent.rows[row].cells[i].style.whiteSpace = "normal";
      colwidth = maxwidthcol;
    }

    colspanhead = tableheader.rows[0].cells[ihead1].colSpan;

    if (colspanhead == 1) {

      if (tableheader.rows[0].cells[ihead1].offsetWidth > colwidth && tableheader.rows[0].cells[ihead1].offsetWidth <= maxwidthcol) {
        colwidth = tableheader.rows[0].cells[ihead1].offsetWidth;
      }
      else if (tableheader.rows[0].cells[ihead1].offsetWidth > maxwidthcol) {
        tableheader.rows[0].cells[ihead1].style.whiteSpace = "normal";
        colwidth = maxwidthcol;
      }

      tableheader.rows[0].cells[ihead1].style.width = colwidth;

      ihead1++;
    }
    else {
      tableheader.rows[1].cells[ihead2].style.whiteSpace = "nowrap";

      if (tableheader.rows[1].cells[ihead2].offsetWidth > colwidth && tableheader.rows[1].cells[ihead2].offsetWidth <= maxwidthcol) {
        colwidth = tableheader.rows[1].cells[ihead2].offsetWidth;
      }
      else if (tableheader.rows[1].cells[ihead2].offsetWidth > maxwidthcol) {
        tableheader.rows[1].cells[ihead2].style.whiteSpace = "normal";
        colwidth = maxwidthcol;
      }

      tableheader.rows[1].cells[ihead2].width = colwidth;

      icolspan++;

      if (icolspan >= colspanhead) {
        icolspan = 0;
        ihead1++;
      }

      ihead2++;

    }

    tablecontent.rows[row].cells[i].width = colwidth;
    tablewidth += Number(colwidth);
    i++;
  }

  tablewidth  +=tablewidth*0.1;

  if(ihead2==0){
      wrappercontent.className="wrapperinnertable";
  }
  else{
    wrappercontent.className="wrapperinnertable wrapperinnertablespan";
  }

  wrappercontent.style.width=tablewidth;
  tableheader.style.width=tablewidth;
  tablecontent.style.width=tablewidth;

}

function sortDataTable(colid,submitform){
  document.getElementById("orderby").value=colid;
  document.getElementById("sort").value=1-document.getElementById("sort").value;
  submitform.submit();
}

function setClassArrowSort(colid,sort,table,row=0){
  setsort = (sort==0)?"asc":"desc";
  table.rows[row].cells[colid].innerHTML += " <span class='arrowsort"+setsort+"'></span>";

}

function pagingGotoPage(idxpage,submitform){
  document.getElementById("idxpage").value=idxpage;
  submitform.submit();
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function StartCustomeSize(page,width,height) {
	width=width==""?800:width;
	height=height==""?600:height;
	
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes,width="+width+"px,height="+height+"px");
}

function setPopup(lebar,tinggi,showbuttonclose=1){
  document.getElementById("popupcontent").style.width=lebar+"px";
  document.getElementById("popupcontent").style.height=tinggi+"px";

  if(showbuttonclose) {
    document.getElementById("popupbuttonclose").style.display = "inline-block";
  }
  else{
    document.getElementById("popupbuttonclose").style.display="none";
  }
}

function showLoading(){
  setPopup(400,50,0);
  document.getElementById("popupcontent").innerHTML="<br><center>silahkan tunggu, sedang memroses permintaan anda...</center>";
  popupwrapper.show();
}

function validasiInputanAngka(evt){

  var theEvent = evt || window.event;

  var key = theEvent.keyCode || theEvent.which;

  key = String.fromCharCode(key);

  var regex = /[0-9\.]/;

  if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
    || [evt.keyCode||evt.which]==37 || [evt.keyCode]==39 || [evt.keyCode||evt.which]==116)  return true;

  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    theEvent.preventDefault();
  }
}

function validCharInput(evt){

  var theEvent = evt || window.event;

  var key = theEvent.keyCode || theEvent.which;

  key = String.fromCharCode(key);

  var regex = /[a-zA-Z0-9_\.\- ]/;

  if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
    || [evt.keyCode||evt.which]==37 || [evt.keyCode]==39 || [evt.keyCode||evt.which]==116)  return true;

  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    theEvent.preventDefault();
  }
}

function addEvent(obj, evType, fn) {
  if (obj.addEventListener) {
    obj.addEventListener(evType, fn, false);
    return true;
  } else if (obj.attachEvent) {
    var r = obj.attachEvent("on" + evType, fn);
    return r;
  } else {
    alert("Handler could not be attached");
  }
}

function validasiInput(obj,elm=null){

  var elmtarget=null;
  var classnamenormal=null;

  if(obj.value==""){

    if(elm===null){
      elmtarget = obj;
      thisevt   = "focus";
    }
    else{
      elmtarget = elm;
      thisevt   = "click";
    }

    evtexist=elmtarget.onclick;
    classnamenormal=elmtarget.className;
    elmtarget.onclick=null;
    addEvent(elmtarget,thisevt,evtexist);
    addEvent(elmtarget,thisevt,function(){elmtarget.className=classnamenormal;});
    elmtarget.className= elmtarget.className + " error";

    return false;
  }

  return true;
}

function formatUang(uang,separator){
  len_uang = String(uang).length;
  return_val='';
  for (i=len_uang;i>=0;i--){
    if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

    return_val =String(uang).substring(i,i-1)+return_val;
  }

  return return_val;
}

function comboListFilter(targetelement,targetid,elmcontainer,anchortop,uri,filter,parameters="",istampilkansemua=false,show=true){

  new Ajax.Request(uri,{
    asynchronous: true,
    method: "post",
    parameters: parameters+"&filter="+filter,
    onLoading: function(request){
    },
    onComplete: function(request){
    },
    onSuccess: function(request){

      if(!isJson(request.responseText)) {
        alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        return false;
      }

      var stringshow      = "";
      var group           = "";
      var idx;
      var strlistdatatemp = "";
      var listdata;

      var strlistdata = request.responseText;

      var tempheight=0;

      if(strlistdata!="null"){

        strlistdatatemp = strlistdata.replace(/'/g,"\"");

        listdata = JSON.parse(strlistdatatemp);

        if(Object.prototype.toString.call(listdata)!=='[object Array]'){
          exit;
        }

        jumdata = listdata.length;

        for(idx=0;idx<jumdata;idx++){
          if(group!=listdata[idx]["group"]) {
            stringshow  += " <div class='resvcombolistgroup'>"+listdata[idx]["group"]+"</div>";
            group        = listdata[idx]["group"];
            tempheight  +=15;
          }

          textditampilkan     = listdata[idx]["text"];
          textditampilkanshort= textditampilkan.substring(0,20);

          if(listdata[idx]["id"]==document.getElementById(targetid).value){
            document.getElementById(targetelement.id).innerHTML=textditampilkanshort;
            document.getElementById(targetid).value=listdata[idx]["id"];
          }

          stringonclick="document.getElementById('"+targetid+"').value='"+listdata[idx]["id"]+"';document.getElementById('"+targetelement.id+"').innerHTML='"+textditampilkanshort+"';document.getElementById('"+elmcontainer.id+"').style.display='none';";
          stringshow  += "<div class=\"resvcombolist\" onclick=\""+stringonclick+"\" onmouseover=\"iscombolistfocus=true;\" onmouseout=\"iscombolistfocus=false;\">"+listdata[idx]["text"]+"</div>";
          tempheight  +=16;
        }
      }
      else{
        stringshow  +="<span style=\"width:150px;display: inline-block;text-align: center;padding-top: 5px;color: #808080;font-size: 11px;\"><i>-tidak ada data-</i></span><br>"
        tempheight  +=18;
      }

      var showtampilkansemua="";

      if(istampilkansemua){
        stringonclick="document.getElementById('"+targetid+"').value='';document.getElementById('"+targetelement.id+"').innerHTML='-tampilkan semua-';document.getElementById('"+elmcontainer.id+"').style.display='none';";
        showtampilkansemua  = "<div class=\"resvcombolist\" onclick=\""+stringonclick+"\" onmouseover=\"iscombolistfocus=true;\" onmouseout=\"iscombolistfocus=false;\">-tampilkan semua-</div>"
        if(document.getElementById(targetid).value=="") {
          document.getElementById(targetelement.id).innerHTML = "-tampilkan semua-";
          document.getElementById(targetid).value = "";
        }
        tempheight += 17;
      }

      document.getElementById("listcontent").innerHTML=showtampilkansemua+stringshow;
      document.getElementById("listcontent").style.height=(tempheight>170?"170px":tempheight);
      elmcontainer.style.display=(show?"block":"none");

      if(anchortop+document.getElementById("listcontent").offsetHeight<window.innerHeight-40) {
        elmcontainer.style.top=anchortop;
      }
      else{
        elmcontainer.style.top=anchortop-document.getElementById("listcontent").offsetHeight-65;
      }

      combolistfilter.focus();

    },
    onFailure: function(request){
      alert('Error !!! Cannot Save');
      assignError(request.responseText);
    }
  });
}

var iscombolistfocus=false;

function setComboList(targetelement,targetid,uri,parameters="",istampilkansemua=false,show=false,ispopup=false){

  if(ispopup) {
    elmrect   = targetelement.getBoundingClientRect();

    elmtop = elmrect.top+17;
    elmleft= elmrect.left;

    elmcontainer  = document.getElementById("combolistcontainerpopup");
    elmcontainer.style.position = "fixed";
  }
  else{
    elmposition = getPosElement(targetelement);
    elmtop      = elmposition.top+17;
    elmleft     = elmposition.left;

    elmcontainer  = document.getElementById("combolistcontainer");
    elmcontainer.style.position = "absolute";
  }

  elmcontainer.style.top  = elmtop;
  elmcontainer.style.left = elmleft;
  elmcontainer.innerHTML  = "<br><input type=\"text\" id=\"combolistfilter\" name=\"combolistfilter\" value=\"\" onblur=\"if(!iscombolistfocus){document.getElementById('"+elmcontainer.id+"').style.display='none';}\" onkeyup=\"comboListFilter(document.getElementById('"+targetelement.id+"'),'"+targetid+"',document.getElementById('"+elmcontainer.id+"'),"+elmtop+",'"+uri+"',this.value,'"+parameters+"',"+istampilkansemua+");\"/><div id=\"listcontent\"></div>";

  document.getElementById("combolistfilter").value="";

  comboListFilter(targetelement,targetid,elmcontainer,elmtop,uri,"",parameters,istampilkansemua,show);

}

function getPosElement(obj){
  var curtop = 0;
  var curleft = 0;

  if(obj.offsetParent) {
    while(1) {

      curtop  += obj.offsetTop;
      curleft  += obj.offsetLeft;

      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;

    }
  }
  else if(obj.x) {
    curtop += obj.y;
    curleft += obj.x;
  }

  return {
    top: curtop,
    left: curleft
  };
}

var waktucountshownotif=5;//detik
var counttmrshownotif=waktucountshownotif;

function timerShowNotif(elmid){

  if (counttmrshownotif==1) {
    counttmrshownotif=waktucountshownotif;
    timerShowNotif(elmid);
    document.getElementById(elmid).style.display="none";
  }
  else {
    counttmrshownotif-=1;
    setTimeout("timerShowNotif('"+elmid+"')",1000);
  }

}

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function setPopupNotif(objpopup,lebar,tinggi,pesan,button=""){

  document.getElementById("popupnotifcontent").style.width=lebar+"px";
  document.getElementById("popupnotifcontent").style.height=tinggi+"px";
  document.getElementById("popupnotifbuttonclose").style.display = "inline-block";

  popupnotifbuttonok.style.display="none";
  popupnotifbuttoncancel.style.display="none";
  popupnotifbuttonya.style.display="none";
  popupnotifbuttontidak.style.display="none";

  document.getElementById("popupnotifpesan").innerHTML = "<br>"+pesan;

  if(button=="") {
    popupnotifbuttonclose.onclick =function(){popupnotif.hide();};
  }

  if(Object.prototype.toString.call(button)=='[object Object]'){

    objkey  = Object.keys(button);

    for(i=0;i<objkey.length;i++){

      if(objkey[i]=="OK"){
        popupnotifbuttonok.style.display="inline-block";
        popupnotifbuttonok.onclick=button["OK"];
      }

      if(objkey[i]=="CANCEL"){
        popupnotifbuttoncancel.style.display="inline-block";
        popupnotifbuttoncancel.onclick=button["CANCEL"];
      }

      if(objkey[i]=="YA"){
        popupnotifbuttonya.style.display="inline-block";
        popupnotifbuttonya.onclick=button["YA"];
      }

      if(objkey[i]=="TIDAK"){
        popupnotifbuttontidak.style.display="inline-block";
        popupnotifbuttontidak.onclick=button["TIDAK"];
      }

      if(objkey[i]=="CLOSE"){
        popupnotifbuttonclose.onclick=button["CLOSE"];
      }

    }
  }

  objpopup.show();

}

var SID="";