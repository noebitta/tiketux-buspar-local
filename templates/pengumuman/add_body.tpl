<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('judul_invalid');
	Element.hide('pengumuman_invalid');
	
	kode			= document.getElementById('kode');
	judul			= document.getElementById('judul');
	pengumuman= document.getElementById('pengumuman');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(judul.value==''){
		valid=false;
		Element.show('judul_invalid');
	}
	
	if(pengumuman.value==''){
		valid=false;
		Element.show('pengumuman_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_PENGUMUMAN_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0">
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Pengumuman</td>
		</tr>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='400'>
				<table width='600'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <input type="hidden" name="id_pengumuman" value="{ID_PENGUMUMAN}">
			      <input type="hidden" name="kode_old" value="{KODE_OLD}">
						<td width='200'><u>Kode Pengumuman</u></td><td width='5'>:</td>
						<td>
							<input type="text" id="kode" name="kode" value="{KODE}" maxlength=50 onChange="Element.hide('kode_invalid');">
							<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Judul</u></td><td>:</td>
						<td>
							<input type="text" id="judul" name="judul" value="{JUDUL_PENGUMUMAN}" maxlength=100 onChange="Element.hide('judul_invalid');">
							<span id='judul_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td valign='top'><u>Pengumuman</u></td>
						<td  valign='top'>:</td>
						<td>
							<textarea name="pengumuman" id="pengumuman" cols="30" rows="3"  onChange="Element.hide('pengumuman_invalid');">{PENGUMUMAN}</textarea>
							<span id='pengumuman_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
			      <td>Pembuat Pengumuman</td><td>:</td>
						<td>
							<b>{PEMBUAT}<b>
						</td>
			    </tr>
					<tr>
			      <td>Waktu Pembuatan</td><td>:</td>
						<td>
							<b>{WAKTU_BUAT}<b>
						</td>
			    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>