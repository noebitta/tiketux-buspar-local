<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">TOP UP PETTY CASH</td>
                    <td class="bannernormal" align="right" valign="middle">
                        <input type="hidden" name="KAS" id="KAS" value="{KAS}">
                        {SALDO}
                    </td>
                </tr>
            </table>
            <!-- END HEADER-->
        </td>
        <!-- BODY -->
        {PESAN}
        <table class="border" width="100%">
            <BR>
            <thead>
            <tr>
                <th width="30">No</th>
                <th width="100">CABANG</th>
                <th width="150">PEMBUAT</th>
                <th width="150">TANGGAL BUAT</th>
                <th width="150">JENIS BIAYA</th>
                <th width="100">JUMLAH</th>
                <th width="150">PENERIMA</th>
                <th width="150">KETERANGAN</th>
                <th width="150">TANGGAL CETAK</th>
                <th width="150">PENCETAK</th>
                <th width="150">ACTION</th>
            </tr>
            </thead>
            <tbody>
            <!-- BEGIN ROW -->
            <tr class="even">
                <td><font size="3">{ROW.No}</font> </td>
                <td><font size="3">{ROW.KodeCabang}</font></td>
                <td><font size="3">{ROW.Pembuat}</font></td>
                <td align="center"><font size="2">{ROW.TglBuat}</font></td>
                <td><div><font size="3">{ROW.JenisBiaya}</font></div></td>
                <td><div><font size="3">{ROW.Jml}</font></div></td>
                <td><div><font size="3">{ROW.Penerima}</font></div></td>
                <td><div><font size="3">{ROW.Keterangan}</font></div></td>
                <td><div align="center"><font size="2">{ROW.TglCetak}</font></div></td>
                <td><font size="3">{ROW.Pencetak}</font></td>
                <td align="center">{ROW.TOPUP}</font></td>
            </tr>
            <!-- END ROW -->
            </tbody>
        </table>
        <!--END BODY-->
    </tr>
    <!--PAGING-->
    <table width='100%'>
        <tr>
            <td align='right' width='100%'>
                {PAGING}
            </td>
        </tr>
    </table>
