<script type="text/javascript">

  var arrmatrixlayout;

  function selectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;

      if(chk=document.getElementById(str_var)){
        chk.checked=true;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function deselectAll(){

    i=1;
    loop=true;
    record_dipilih="";
    do{
      str_var='checked_'+i;
      if(chk=document.getElementById(str_var)){
        chk.checked=false;
      }
      else{
        loop=false;
      }
      i++;
    }while(loop);

  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function showDialogTambah(){

    arrmatrixlayout = new Object();

    setPopup(600,400);

    new Ajax.Updater("popupcontent","pengaturan.kendaraan.layout.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1",
      onLoading: function(request) {
      },
      onComplete: function(request) {
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    });

    popupwrapper.show();
  }

  function showDialogUbah(idlayout){

    if(idlayout==""){
      return false;
    }

    arrmatrixlayout = new Object();

    setPopup(600,400);

    new Ajax.Updater("popupcontent","pengaturan.kendaraan.layout.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2&idlayout="+idlayout,
      onLoading: function(request) {
      },
      onComplete: function(request) {
        renderLayout(dlgjumlahbaris.value,dlgjumlahkolom.value,dlgstringjsonlayout.value);
      },
      onSuccess: function(request) {
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    });

    popupwrapper.show();
  }

  function simpan(){
    var valid=true;

    valid = validasiInput(dlgkodelayout) && valid;
    valid = validasiInput(dlgjumlahbaris) && valid;
    valid = validasiInput(dlgjumlahkolom) && valid;

    if(!valid){
      return false;
    }

    var temparray = new Object();

    for(ibaris=0;ibaris<dlgjumlahbaris.value;ibaris++){
      for(ikolom=0;ikolom<dlgjumlahkolom.value;ikolom++){
        temparray[ibaris+"_"+ikolom] = arrmatrixlayout[ibaris+"_"+ikolom];
      }
    }

    stringjson = JSON.stringify(temparray);

    new Ajax.Request("pengaturan.kendaraan.layout.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=1.1"+
      "&idlayout="+dlgidlayout.value+
      "&kodelayout="+dlgkodelayout.value+
      "&jumlahbaris="+dlgjumlahbaris.value+
      "&jumlahkolom="+dlgjumlahkolom.value+
      "&kapasitas="+dlgkapasitas.value+
      "&petakursi="+stringjson,
      onLoading: function(request){
      },
      onComplete: function(request){
      },
      onSuccess: function(request){
        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            if(dlgidlayout.value=="") {
              objbutton = new Object();
              popupwrapper.hide();
              objbutton["CLOSE"] = function(){popupnotif.hide();showDialogTambah();};
              objbutton["OK"] = function(){popupnotif.hide();showDialogTambah();};
              setPopupNotif(popupnotif,400,120,"Template Layout BERHASIL DITAMBAHKAN",objbutton);
              popupbuttonclose.onclick=null;
              addEvent(popupbuttonclose,"click",function(){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();});
            }
            else{
              objbutton = new Object();
              popupwrapper.hide();
              objbutton["CLOSE"] = function(){popupnotif.hide();popupwrapper.show();};
              objbutton["OK"] = function(){popupnotif.hide();popupwrapper.show();};
              setPopupNotif(popupnotif,400,120,"Template Layout BERHASIL Diubah",objbutton);
              popupbuttonclose.onclick=null;
              addEvent(popupbuttonclose,"click",function(){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();});
            }
          }
          else {
            alert("Gagal:" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }

      },
      onFailure: function(request){
        alert('Error');
        assignError(request.responseText);
      }
    });
  }

  function hapusData(id=''){

    if(id!=''){
      list_dipilih="'"+id+"'";
    }
    else{
      i=1;
      loop=true;
      list_dipilih="";
      do{
        str_var='checked_'+i;

        if(chk=document.getElementById(str_var)){
          if(chk.checked){
            if(list_dipilih==""){
              list_dipilih +="'"+chk.value+"'";
            }
            else{
              list_dipilih +=",'"+chk.value+"'";
            }
          }
        }
        else{
          loop=false;
        }
        i++;
      }while(loop);
    }

    if(list_dipilih==""){
      alert("Anda belum memilih data yang akan dihapus!");
      return false;
    }

    if(confirm("Apakah anda yakin akan menghapus data ini?")){

      new Ajax.Request("pengaturan.kendaraan.layout.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=3&listid="+list_dipilih,
        onLoading: function(request){
        },
        onComplete: function(request){
        },
        onSuccess: function(request){
          if(isJson(request.responseText)) {
            var result;
            result = JSON.parse(request.responseText);

            if (result["status"] == "OK") {
              objbutton = new Object();
              popupwrapper.hide();
              objbutton["CLOSE"] = function (){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();};
              objbutton["OK"] = function (){document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;formdata.submit();};
              setPopupNotif(popupnotif, 400, 120, "DATA BERHASIL DIHAPUS", objbutton);
            }
            else {
              alert("Gagal:" + result["pesan"]);
            }
          }
          else{
            alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
          }

        },
        onFailure: function(request){
        }
      })
    }

    return false;

  }

  function renderLayout(baris=0,kolom=0,petakursi=""){

    if(baris<=0 || kolom<=0){
      return false;
    }

    if(petakursi!="") {
      tempstring=petakursi;
      tempstring=tempstring.replace(/\'/g,'"');

      if(!isJson(tempstring)){
        return false;
      }

      arrmatrixlayout = JSON.parse(tempstring);

    }

    tablelayout.innerHTML= "";

    for(ibaris=0;ibaris<baris;ibaris++){
      var row = tablelayout.insertRow(ibaris);

      for(ikolom=0;ikolom<kolom;ikolom++){
        var cell  = row.insertCell(ikolom);

        if(arrmatrixlayout[ibaris+"_"+ikolom]){
          switch(arrmatrixlayout[ibaris+"_"+ikolom]["status"]){
            case "x":
              classrender = "renderlayoutkosong";
              displaylabel= "none";
              labelvalue  = "";
              break;
            case "p":
              classrender = "renderlayoutkursi";
              displaylabel= "block";
              labelvalue  = arrmatrixlayout[ibaris+"_"+ikolom]["label"];
              break;
            case "s":
              classrender = "renderlayoutsopir";
              displaylabel= "none";
              labelvalue  = "";
              break;
          }
        }
        else{
          classrender = "renderlayoutkosong";
          displaylabel= "none";
          labelvalue  = "";
          arrmatrixlayout[ibaris+"_"+ikolom] = {status:'x',label:''};
        }

        cell.innerHTML="<div id='elmmatrix_"+ibaris+"_"+ikolom+"' class='"+classrender+"' style='text-align: center; padding-top: 5px;padding-left:5px;' onclick='setMatrixLayout("+ibaris+","+ikolom+")'><input type='text' id='label_"+ibaris+"_"+ikolom+"' value='"+labelvalue+"' onkeypress='validCharInput(event);' placeholder='No.Kursi' style='display:"+displaylabel+";width:70px;text-align:center;' onchange='arrmatrixlayout[\""+ibaris+"_"+ikolom+"\"][\"label\"] = this.value;' /></div>";

      }
    }

  }

  function setMatrixLayout(baris,kolom){

    elmdiv  = document.getElementById("elmmatrix_"+baris+"_"+kolom);
    elmlabel= document.getElementById("label_"+baris+"_"+kolom);

    arrmatrixlayout[baris+"_"+kolom]["status"]=seldisplay.value;
    arrmatrixlayout[baris+"_"+kolom]["label"] = "";

    switch(seldisplay.value){
      case 'x':
        elmdiv.className      = "renderlayoutkosong";
        elmlabel.style.display= "none";
        break;

      case 'p':
        elmdiv.className = "renderlayoutkursi";
        elmlabel.style.display= "block";
        break;

      case 's':
        elmdiv.className = "renderlayoutsopir";
        elmlabel.style.display= "none";
        break;
    }

    jumlahkursi=0;

    for(ibaris=0;ibaris<dlgjumlahbaris.value;ibaris++){
      for(ikolom=0;ikolom<dlgjumlahkolom.value;ikolom++){
        jumlahkursi =jumlahkursi + (arrmatrixlayout[ibaris+"_"+ikolom]["status"]=="p"?1:0);
      }
    }

    dlgkapasitas.value=jumlahkursi;

  }

  function init(e){
    popupwrapper	= dojo.widget.byId("popupcontainer");
    popupnotif	  = dojo.widget.byId("popupnotifcontainer");
    document.getElementById("wrappertablecontent").scrollTop={SCROLL_VALUE};
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">
  <input type="hidden" id="scrollvalue" name="scrollvalue" value="0">
  <input type="hidden" id="modeshowdialogtambah" name="modeshowdialogtambah" value="0">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Template Layout Kendaraan</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="showDialogTambah();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->
    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="return hapusData();" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
  Data Layout<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th></th>
      <th>No</th>
      <th onclick="setOrder(0)">Kode Layout</th>
      <th onclick="setOrder(1)">Jumlah Baris</th>
      <th onclick="setOrder(2)">Jumlah Kolom</th>
      <th onclick="setOrder(3)">Kapasitas</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"><input type="checkbox" id="checked_{ROW.idx}" name="checked_{ROW.idlayout}" value="{ROW.idlayout}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="showDialogUbah('{ROW.idlayout}');" title="ubah data" ></span>&nbsp;
          <!-- END ACT_EDIT -->

          <!-- BEGIN ACT_DEL -->
          <span class="b_delete" onclick="hapusData('{ROW.idlayout}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.kodelayout}</td>
        <td align="center">{ROW.jumlahbaris}</td>
        <td align="center">{ROW.jumlahkolom}</td>
        <td align="center">{ROW.kapasitas}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+3,sort.value,document.getElementById("tableheader"))
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>