<span class="popuptitle">{JUDUL}</span>
<center>
  <div class="wrapperpopup">
    <input type="hidden" id="dlgidlayout" value="{ID_LAYOUT}"/>
    <input type="hidden" id="dlgstringjsonlayout" value="{JSON_LAYOUT}"/>
    <span style="float: left;width: 48%; display: inline-block;">
      <span class="popuplabel">Kode Layout</span><span class="popupinput"><input type="text" id="dlgkodelayout" onkeypress="validCharInput(event);" onfocus="this.style.background='white';" value="{KODE_LAYOUT}" maxlength="15" style="text-transform: uppercase;"/></span><br><br>
      <span class="popuplabel">Jumlah Baris</span><span class="popupinput"><input type="text" id="dlgjumlahbaris" onkeypress="validasiAngka(event);" onfocus="this.style.background='white';" onchange="renderLayout(this.value,dlgjumlahkolom.value);" value="{JUMLAH_BARIS}" size="5" maxlength="2"/></span><br><br>
      <span class="popuplabel">Jumlah Kolom</span><span class="popupinput"><input type="text" id="dlgjumlahkolom" onkeypress="validasiAngka(event);" onfocus="this.style.background='white';" onchange="renderLayout(dlgjumlahbaris.value,this.value);" value="{JUMLAH_KOLOM}" size="5" maxlength="2"/></span><br><br>
      <span class="popuplabel">Kapasitas</span><span class="popupinput"><span><input id="dlgkapasitas" type="text" size="5" readonly value="{KAPASITAS}"></span></span><br><br>
      <span class="flatbutton" onclick="simpan();" id="buttonproses">SIMPAN</span>
    </span>
    <span style="float: right;width: 48%; display: inline-block;">
      <b>Render Layout</b><br>
      <select id="seldisplay"><option value="x">-kosong-</option><option value="p">Penumpang</option><option value="s">Sopir</option></select><br>
      ----DEPAN----<br>
      <div style="overflow: auto;height:320px;text-align: center;">
        <span id="dlgrenderlayout" style="display: inline-block;text-align: center;">
          <table id="tablelayout"></table>
        </span>
      </div>
      ==BELAKANG==
    </span>
  </div>
</center>