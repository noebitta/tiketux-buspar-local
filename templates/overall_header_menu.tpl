<div id="wrapperoverallmenu">
  <form name='form_pilih_menu' method='post'>
    <input id='hdn_SID' name='hdn_SID' type='hidden' value='{SID}' />

    <!-- BEGIN menu200 -->
    <span id='top_menu_operasional' class='{menu200.class}' onClick="submitPilihanMenu(this.id);return false;">Operasional</span>
    <!-- END menu200 -->

    <!-- BEGIN menu300 -->
    <span id='top_menu_lap_reservasi' class='{menu300.class}' onClick="submitPilihanMenu(this.id);return false;">Lap. Reservasi</span>
    <!-- END menu300 -->

    <!-- BEGIN menu400 -->
    <span id='top_menu_lap_keuangan' class='{menu400.class}' onClick="submitPilihanMenu(this.id);return false;">Keuangan</span>
    <!-- END menu400 -->

    <!-- BEGIN menu500 -->
    <span id='top_menu_tiketux' class='{menu500.class}' onClick="submitPilihanMenu(this.id);return false;">Tiketux</span>
    <!-- END menu500 -->

    <!-- BEGIN menu600 -->
    <span id='top_menu_pengelolaan_member' class='{menu600.class}' onClick="submitPilihanMenu(this.id);return false;">Member</span>
    <!-- END menu600 -->

    <!-- BEGIN menu700 -->
    <span id='top_menu_pengaturan' class='{menu700.class}' onClick="submitPilihanMenu(this.id);return false;">Pengaturan</span>
    <!-- END menu700 -->
  </form>
</div>