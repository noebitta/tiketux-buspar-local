<script language="JavaScript">

function UbahPassword(){
	passlama=document.getElementById('passlama').value;
	passbaru=document.getElementById('passbaru').value;
	konfpassbaru=document.getElementById('konfpassbaru').value;
	
	if(passlama!='' && passbaru!='' && konfpassbaru!='')
	{
	
		new Ajax.Request("ubah_password.php?sid={SID}", 
		{
			asynchronous: true,
		  method: "get",
		  parameters: "passlama=" + passlama + "&passbaru="+ passbaru + "&konfpassbaru="+ konfpassbaru +"&mode=ubah",
			onLoading: function(request) 
		  {
		            
		  },
		  onComplete: function(request) 
		  {
				
				if(request.responseText=='sukses'){
					alert("Password anda telah BERHASIL DIUBAH!");
				}
				else{
					alert("Password lama yang anda masukkan tidak benar, atau password baru anda tidak sesuai dengan konfirmasi passowrd baru.");
				}
				
				document.getElementById('passlama').value = "";
				document.getElementById('passbaru').value = "";
				document.getElementById('konfpassbaru').value = "";
				
		  },
		  onFailure: function(request) 
		  { 
				assignError(request.responseText); 
		  }
		});
	}
	else{
		alert("Input anda belum lengkap!");
	}
}

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr class='banner' height=40>
		<td align='center' valign='middle' class="bannerjudul">&nbsp;Ubah Password</td>
	</tr>		
	<tr><td height=30></td></tr>
	<tr>
		<td class="whiter" valign="middle" align="center">
			<table width='40%'>
					<tr>
						<td>Masukkan password lama anda</td>
						<td width='3%'>:</td>
						<td><input type='password' size='20' id='passlama' name='passlama' value='' /></td>
					</tr>
					<tr>
						<td>Masukkan password baru anda</p></td>
						<td width='3%'>:</td>
						<td><input type='password' size='20' id='passbaru' name='passbaru' value='' /></td>
					</tr>
					<tr>
						<td>Ulangi password baru anda</td>
						<td width='3%'>:</td>
						<td><input type='password' size='20' id='konfpassbaru' name='konfpassbaru' value='' /></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td ><input type='button' name='ubah' value='ubah' onclick="UbahPassword()"></td>
					</tr>
				</table>
		</td>
	</tr>
</table>