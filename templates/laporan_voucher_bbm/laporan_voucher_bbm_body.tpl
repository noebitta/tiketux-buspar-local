<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");

  	function showDialogBuatVoucher(){
		/*nilaivoucher.value="";
		keterangan.value="";
		jumlahcetak.value="";*/
		dlg_buat_voucher.show();
	}

	function validasiLiter(evt){
		var theEvent = evt || window.event;
		
		var key = theEvent.keyCode || theEvent.which;
		
		key = String.fromCharCode(key);
		
		var regex = /[0-9]/;
		
		if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
			[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
		
		if( !regex.test(key) ) {
			theEvent.returnValue = false;
			theEvent.preventDefault();
		}
	}

	function buatVoucher(){
		
		is_valid=true;
		
		/*if(no_spj.value==""){
			no_spj.style.background = 'red';
			is_valid = false;
		}*/
		
		if(tgl_berangkat.value==""){
			tgl_berangkat.style.background = 'red';
			is_valid = false;
		}
		
		if(opt_cabang_asal.value==""){
			opt_cabang_asal.style.background = 'red';
			is_valid = false;
		}

		if(opt_tujuan.value==""){
			opt_tujuan.style.background = 'red';
			is_valid = false;
		}

		if(opt_jadwal.value==""){
			opt_jadwal.style.background = 'red';
			is_valid = false;
		}

		if(mobil.value==""){
			mobil.style.background = 'red';
			is_valid = false;
		}

		if(dlgcbosopir.value==""){
			dlgcbosopir.style.background = 'red';
			is_valid = false;
		}

		if(kilometer.value==""){
			kilometer.style.background = 'red';
			is_valid = false;
		}

		if(jml_liter.value==""){
			jml_liter.style.background = 'red';
			is_valid = false;
		}
		
		if(!is_valid){
			exit;
		}
		
		new Ajax.Request("laporan_voucher_bbm.php?sid={SID}",{
			asynchronous: true,
			method: "get",
			parameters: "mode=add"+
				/*"&no_spj="+no_spj.value+*/
				"&tgl_berangkat="+tgl_berangkat.value+
				"&kode_jadwal="+opt_jadwal.value+
				"&id_jurusan="+opt_tujuan.value+
				"&mobil="+mobil.value+
				"&sopir="+dlgcbosopir.value+
				"&kilometer="+kilometer.value+
				/*"&jenis_bbm="+jenis_bbm.value+*/
				"&jml_liter="+jml_liter.value+
				"&kode_spbu="+kode_spbu.value,
			onLoading: function(request){
				progressbar.show();
			},
			onComplete: function(request){
				progressbar.hide();
				dlg_buat_voucher.hide();
			},
			onSuccess: function(request){
				eval(request.responseText);
			},
			onFailure: function(request){
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		});
	}
	
	function getUpdateTujuan(asal){
			// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
			
			if(document.getElementById('rewrite_tujuan')){
				document.getElementById('rewrite_tujuan').innerHTML = "";
			}
			
			new Ajax.Updater("rewrite_tujuan","laporan_voucher_bbm.php?sid={SID}", 
			{
					asynchronous: true,
					method: "get",
					parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
					onLoading: function(request) 
					{
							Element.show('loading_tujuan');
					},
					onComplete: function(request) 
					{
							Element.hide('loading_tujuan');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			});   
	}

	function getUpdateJadwal(id_jurusan){
	
		tgl=document.getElementById('tgl_berangkat').value;
		//document.getElementById('p_jadwal').value="";
		
		// mengupdate JAM sesuai dengan RUTE yang dipilih
		document.getElementById('opt_tujuan').value = id_jurusan;
		
		new Ajax.Updater("rewrite_jadwal","laporan_voucher_bbm.php?sid="+SID, 
		{
			asynchronous: true,
			method: "get",
			parameters: "tgl=" + tgl + "&id_jurusan=" + id_jurusan +"&mode=get_jadwal",
			onLoading: function(request) 
			{
				document.getElementById('rewrite_jadwal').innerHTML="";	
				Element.show('progress_jam');
			},
				onComplete: function(request) 
			{
				Element.hide('progress_jam');
				Element.show('rewrite_jadwal');
			},                
			onFailure: function(request) 
			{ 
				assignError(request.responseText);
			}
		});
	}

	function getKendaraan(){
		
		new Ajax.Updater("rewrite_kendaraan","laporan_voucher_bbm.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=get_mobil",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_kendaraan');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}

	function getDaftarSopir(){
			
			
			new Ajax.Updater("dlgdaftarsopircbosopir","laporan_voucher_bbm.php?sid={SID}", 
			{
					asynchronous: true,
					method: "get",
					parameters: "mode=get_sopir",
					onLoading: function(request) 
					{
							//dlg_daftar_sopir.show();
							Element.show('dlgdaftarsopirloading');
					},
					onComplete: function(request) 
					{
							//document.getElementById("dlgdaftarsopirkodejadwal").innerHTML	= kodejadwal;
							Element.hide('dlgdaftarsopirloading');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			}); 
	}
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	function init(e){
		dlg_buat_voucher						= dojo.widget.byId("dialogbuatvoucher");
		dlg_buat_voucher_btn_cancel = document.getElementById("dialogbuatvouchercancel");


        dlg_edit_voucher			= dojo.widget.byId("dialogeditvoucher");
        dlg_edit_voucher_btn_cancel = document.getElementById("dialogeditvouchercancel");

        //setSortId();
	}
	
	dojo.addOnLoad(init);
	getKendaraan();
	getDaftarSopir();

	function approve(kode){

      if(confirm("Apakah anda yakin akan melakukan aksi ini?")){



          new Ajax.Request("laporan_voucher_bbm.php?sid={SID}",{
              asynchronous: true,
              method: "get",
              parameters: "mode=approve&no_spj="+kode,
              onLoading: function(request)
              {
              },
              onComplete: function(request)
              {

              },
              onSuccess: function(request)
              {

                  if(request.responseText==1){
                      window.location.reload();
                  }
                  else{
                      alert("Terjadi kegagalan!");
                  }
              },
              onFailure: function(request)
              {
              }
          })
      }

      return false;

  	}

  function reprint(voucher){
      if(confirm("Apakah anda yakin akan melakukan aksi ini?")){



          new Ajax.Request("laporan_voucher_bbm.php?sid={SID}",{
              asynchronous: true,
              method: "get",
              parameters: "mode=reprint&voucher="+voucher,
              onLoading: function(request)
              {
              },
              onComplete: function(request)
              {

              },
              onSuccess: function(request)
              {

                  if(request.responseText==1){
                      window.location.reload();
                  }
                  else{
                      alert("Terjadi kegagalan!");
                  }
              },
              onFailure: function(request)
              {
              }
          })
      }

      return false;
  }

  function showDialogEditVoucher(spj){
      //no_spj_edit.value = spj;

      new Ajax.Updater("dialogeditvoucher","laporan_voucher_bbm.php?sid={SID}", {
          asynchronous: true,
          method: "get",

          parameters: "mode=edit_voucher&no_spj="+spj,
          onLoading: function(request){
              progressbar.show();
          },
          onComplete: function(request){
              progressbar.hide();
          },
          onFailure: function(request){
              assignError(request.responseText);
          }
      });

      dlg_edit_voucher.show();
  }

  function updateVoucher(){
      is_valid=true;


      if(kilometer_edit.value==""){
          kilometer_edit.style.background = 'red';
          is_valid = false;
      }

      if(jml_liter_edit.value==""){
          jml_liter_edit.style.background = 'red';
          is_valid = false;
      }

      if(!is_valid){
          exit;
      }


      new Ajax.Request("laporan_voucher_bbm.php?sid={SID}",{
          asynchronous: true,
          method: "get",
          parameters: "mode=update_voucher"+
          "&no_spj_edit="+no_spj_edit.value+
              /*"&tgl_berangkat="+tgl_berangkat.value+
               "&kode_jadwal="+opt_jadwal.value+
               "&id_jurusan="+opt_tujuan.value+
               "&mobil="+mobil.value+
               "&sopir="+dlgcbosopir.value+*/
          "&kilometer_edit="+kilometer_edit.value+
          "&jenis_bbm_edit="+jenis_bbm_edit.value+
          "&jml_liter_edit="+jml_liter_edit.value+
          "&kode_spbu_edit="+kode_spbu_edit.value,
          onLoading: function(request){
              progressbar.show();
          },
          onComplete: function(request){
              progressbar.hide();
              dlg_buat_voucher.hide();
          },
          onSuccess: function(request){
              eval(request.responseText);
          },
          onFailure: function(request){
              alert('Error !!! Cannot Save');
              assignError(request.responseText);
          }
      });
  }
</script>

<!--dialog Create Voucher-->
<div dojoType="dialog" id="dialogbuatvoucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width="400">
<tr>
	<td bgcolor='ffffff' align='center'>
		<table>
			<tr><td colspan='2'><h2>Buat Voucher</h2></td></tr>
			<!-- <tr>
				<td align='right'>Manifest :</td>
				<td>
					<input id="no_spj" name="no_spj" type="text" onfocus="this.style.background='white'"/>
				</td>
			</tr> -->		
			<tr>
				<td align='right'>Tgl Berangkat:</td>
				<td>
					<input id="tgl_berangkat" name="tgl_berangkat" type="text" readonly='yes' size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_BERANGKAT}">
				</td>
			</tr>
			<tr>
				<td align='right'>Cabang Asal:</td>
				<td>
					<select name='opt_cabang_asal' id='opt_cabang_asal' onChange="getUpdateTujuan(this.value)" onfocus="this.style.background='white'">{CABANG_ASAL}</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Cabang Tujuan:</td>
				<td>
					<div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span>
				</td>
			</tr>
			<tr>
				<td align='right'>Jadwal:</td>
				<td>
					<div id="rewrite_jadwal"></div>
				</td>
			</tr>
			<tr>
				<td align='right'>Nama SPBU:</td>
				<td>
					<select id='kode_spbu'>
						<option value='1'>KM 57</option>
						<option value='2'>KM 72</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>Kendaraan:</td>
				<td>
					<div id='rewrite_kendaraan'></div>
				</td>
			</tr>
			<tr>
				<td align='right'>Sopir:</td>
				<td>
					<span id="dlgdaftarsopircbosopir"></span><span id='dlgdaftarsopirloading' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span>
				</td>
			</tr>
			<tr>
				<td align='right'>Kilometer:</td>
				<td><input id="kilometer" name="kilometer" type="text" onfocus="this.style.background='white'" size="5" onkeypress="validasiLiter(event)" /></td>
			</tr>
			<!--<tr>
				<td align='right'>Jenis BBM:</td>
				<td>
					<select id='jenis_bbm'>
						<option value='1'>SOLAR</option>
						<option value='2'>PREMIUM</option>
					</select>
				</td>
			</tr>-->
			<tr>
				<td align='right'>Jumlah Liter:</td>
				<td><input id="jml_liter" name="jml_liter" type="text" onfocus="this.style.background='white'" size="5" onkeypress="validasiLiter(event)" /></td>
			</tr>
			<!-- <tr>
				<td align='right'>Harga:</td>
				<td>
					{HARGA_LITER} / Liter
				</td>
			</tr> -->
		</table>
		<span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialogbuatvouchercancel" value="&nbsp;Cancel&nbsp;" onClick="dlg_buat_voucher.hide();"> 
		<input type="button" onclick="buatVoucher();" id="dialogbuatvoucherproses" value="&nbsp;Proses&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog create voucher-->

<!--dialog Edit Voucher-->
<div dojoType="dialog" id="dialogeditvoucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
</div>
<!--END dialog edit voucher-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<!--HEADER-->
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Checker SPBU</td>
			<td align='right' valign='middle'>
				<form action="{ACTION_CARI}" method="post">
				<table>
					<tr><td class='bannernormal'>
						COUNTER:&nbsp;
						<select name='opt_cabang' id='opt_cabang' onfocus="this.style.background='white'">{OPT_CABANG}</select>
						SPBU:&nbsp;
						<select id='spbu' name='spbu'>
							<option value="">-semua-</option>
							<!-- BEGIN OPT_SPBU -->
							<option value="{OPT_SPBU.value}" {OPT_SPBU.selected}>{OPT_SPBU.nama}</option>
							<!-- END OPT_SPBU -->
						</select>
						&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
						&nbsp;Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" />&nbsp;	
						<input type="submit" value="cari" />&nbsp;
						<input type='hidden' name='mode' value='0'/>
					</td></tr>
				</table>
				</form>
			</td>
		</tr>
		<tr>
			<td colspan=2 align='center' valign='middle'>
				<table>
					<tr>
						<td>
							<!--<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;-->
						</td><!--<td bgcolor='D0D0D0'>--></td>
						<td>
							<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align='left' valign='bottom' colspan=3>
			{SUMMARY}
			</td>
		</tr>
		<tr><td colspan=2 align='right'>{PAGING}</td></tr>
	</table>
	<!-- END HEADER-->
	<a href='#' onClick="showDialogBuatVoucher();">Buat Voucher</a>
	<table class="border" width='100%' >
  <tr>
     <th width=30>No</th>
		 <th width=70 ><a class="th" >Tgl</a></th>
		 <th width=100><a class="th" >#Voucher</a></th>
		 <th width=100><a class="th" >#Manifet</a></th>
		 <th width=50 ><a class="th" >#Body</a></th>
		 <th width=50 ><a class="th" >#NoPolisi</a></th>
		 <th width=100><a class="th" >Sopir</a></th>
		 <th width=100><a class="th" >#Jadwal</a></th>
		 <!--<th width=50><a class="th" id="sort7" href='#'>BBM</a></th>-->
		 <th width=50 ><a class="th">Liter</a></th>
		 <th width=100><a class="th" >Biaya</a></th>
		 <th width=100><a class="th" >SPBU</a></th>
		 <th width=100><a class="th" >Petugas</a></th>
		 <th width=100><a class="th" >WaktuCatat</a></th>
      <th width=100><a class="th" >Reprint</a></th>
        <th width=100><a class="th" >Action</a></th>
		 <th width=70><a class="th" >Tgl Approve</a></th>
		 <!--<th width=100><a class="th" id="sort14" href='#'>Petugas Approve</a></th>-->
		 <th width=70>Action</th>
   </tr>
   <!-- BEGIN ROW -->
   <tr class="{ROW.odd}">
     <td align="center">{ROW.no}</td>
		 <td align="center">{ROW.tanggal}</td>
		 <td align="center">{ROW.voucher}</td>
		 <td align="center">{ROW.nospj}</td>
     <td align="center">{ROW.body}</td>
     <td align="center">{ROW.nopol}</td>
		 <td align="center">{ROW.sopir}</td>
		 <td align="center">{ROW.jadwal}</td>
		 <!--<td align="center">{ROW.bbm}</td>-->
		 <td align="right">{ROW.liter}</td>
		 <td align="right">{ROW.biaya}</td>
		 <td align="center">{ROW.sbpu}</td>
		 <td align="center">{ROW.petugas}</td>
		 <td align="center">{ROW.waktucatat}</td>
       <td align="center">{ROW.reprint}</td>
		 <!-- <td align="center" style="background: {ROW.bgstatus};">{ROW.status}</td> -->
       <td align="center"><div title="Create Voucher BBM"><a href='#' onClick="{ROW.act}"> <img src="{TPL}/images/b_manifest.png"></a></div></td></td>
		 <td align="center">{ROW.tglapprove}</td>
		 <!--<td align="center">{ROW.petugasapprove}</td>-->
		 <!-- <td align="center">Edit</td> -->
		 <td align="center" style="background: {ROW.bgstatus};">{ROW.status}</td>
   </tr>
   <!-- END ROW -->
  </table>
	<table width='100%'>
		<tr>
			<td align='right' width='100%'>
				{PAGING}
			</td>
		</tr>
		<tr>
			<td align='left' valign='bottom' colspan=3>
			{SUMMARY}
			</td>
		</tr>
	</table>
 </td>
</tr>
</table>