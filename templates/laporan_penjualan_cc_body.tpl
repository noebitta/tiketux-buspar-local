<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function init(e) {
        // inisialisasi variabel

        //control dialog daftar cso dan counter
        dlg_daftar_counter				= dojo.widget.byId("dlgdaftarcounter");
        dlg_daftar_counter_cancel	    = document.getElementById("dlgdaftarcounterbuttoncancel");
        dlg_daftar_counter.setCloseControl(dlg_daftar_counter_cancel);

    }

    dojo.addOnLoad(init);

    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }

    function setForm(notiket, penumpang, kursi) {
        document.getElementById("notiket").innerHTML = notiket;
        document.getElementById("tiket_hidden").value = notiket;
        document.getElementById("penumpang").innerHTML = penumpang;
        document.getElementById("kursi").innerHTML = kursi;

        new Ajax.Updater("daftar_counter","laporan_penjualan_cc.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "post",
                    parameters: "mode=setdaftarcounter" +
                    "&notiket=" + notiket,
                    onLoading: function(request)
                    {
                        Element.show('loading_counter');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_counter');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });

        new Ajax.Updater("daftar_cso","laporan_penjualan_cc.php?sid={SID}",
                {
                    asynchronous: true,
                    method: "post",
                    parameters: "mode=setdaftarcso" +
                    "&notiket=" + notiket,
                    onLoading: function(request)
                    {
                        Element.show('loading_cso');
                    },
                    onComplete: function(request)
                    {
                        Element.hide('loading_cso');
                    },
                    onFailure: function(request)
                    {
                        assignError(request.responseText);
                    }
                });
    }

    function updateTiket() {
        notiket = document.getElementById("tiket_hidden").value;
        counter = document.getElementById("counter").value;
        cso     = document.getElementById("cso").value;

        new Ajax.Request("laporan_penjualan_cc.php?sid={SID}",{
            asynchronous: true,
            method: "post",
            parameters: "mode=update"+
            "&tiket="+notiket+
            "&counter="+counter+
            "&cso="+cso,
            onLoading: function(request){
                progressbar.show();
            },
            onComplete: function(request){
                console.log(request.responseText);
                if(request.responseText == 1){
                    alert("Penjualan Berhasil di Update");
                    document.forms["formcari"].submit();
                }else{
                    alert("Terjadi Kesalahan");
                    console.log(request.responseText);
                }
            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }

</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <form action="{ACTION_CARI}" method="post" id="formcari">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr class='banner' height=40>
                        <td align='center' valign='middle' class="bannerjudul">&nbsp;LAPORAN PENJUALAN CALL CENTER</td>
                        <td align='right' valign='middle'>
                            <table>
                                <tr><td class='bannernormal'>
                                        &nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                                        &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                                        &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                                        <input type="submit" value="cari" />&nbsp;
                                    </td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
    <tr>
        <td>
            {PAGING}
        </td>
    </tr>
    <tr>
        <td>
            <table class="border" width="100%">
                <tr>
                    <th>No</th>
                    <th>Penumpang</th>
                    <th>No Tiket</th>
                    <th>Berangkat</th>
                    <th>Jadwal</th>
                    <th>Kursi</th>
                    <th>Counter</th>
                    <th>Petugas</th>
                    <th>Action</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td align="center">{ROW.no}</td>
                    <td align="center">{ROW.nama}</td>
                    <td align="center"><font size=3><b>{ROW.tiket}</b></font></td>
                    <td align="center">{ROW.tanggal}</td>
                    <td align="center">{ROW.jadwal}</td>
                    <td align="center">{ROW.kursi}</td>
                    <td align="center">{ROW.cabang}</td>
                    <td align="center">{ROW.petugas}</td>
                    <td align="center">
                        <input type="button" value="..." onclick="dlg_daftar_counter.show(); setForm('{ROW.tiket}','{ROW.nama}','{ROW.kursi}')">
                    </td>
                </tr>
                <!-- END ROW -->
            </table>
        </td>
    </tr>
</table>


<!--BEGIN dialog daftar cso dan counter-->
<div dojoType="dialog" id="dlgdaftarcounter" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
    <table width="400" cellpadding="0" cellspacing="0" >
        <tr><td align="center" valign="middle" style="color: white;font-size: 16px;margin-top: 0px;">EDIT PENJUALAN COUNTER</td></tr>
        <tr>
            <td style="background-color: white;">
                <table width="100%">
                    <tr>
                        <td width="100">Penumpang</td>
                        <td width="1">:</td>
                        <td><b><span id="penumpang"></span></b></td>
                    </tr>
                    <tr>
                        <td width="100">Tiket</td>
                        <td width="1">:</td>
                        <td>
                            <input type="hidden" id="tiket_hidden">
                            <b><span id="notiket"></span></b>
                        </td>
                    </tr>
                    <tr>
                        <td width="100">Kursi</td>
                        <td width="1">:</td>
                        <td><b><span id="kursi"></span></b></td>
                    </tr>
                    <tr>
                        <td>Counter</td>
                        <td width="1">:</td>
                        <td><b></b><span id="daftar_counter"></span><span id='loading_counter' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></b></td></tr>
                    <tr>
                    <tr>
                        <td>CSO</td>
                        <td width="1">:</td>
                        <td><b></b><span id="daftar_cso"></span><span id='loading_cso' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></b></td></tr>
                    <tr>
                        <td colspan="3" align="center">
                            <br><input type="button" id="dlgdaftarcounterbuttonsimpan" value="Simpan" onclick="updateTiket();">&nbsp;
                            <input type="button" id="dlgdaftarcounterbuttoncancel" value="Batal">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
</div>
<!--END dialog cso dan counter-->