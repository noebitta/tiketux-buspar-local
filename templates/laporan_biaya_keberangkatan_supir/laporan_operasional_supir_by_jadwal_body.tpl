<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

</script>

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr class='banner' height=40>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap CGS By Cabang Tugas</td>
                    <td colspan=2 align='right' class="bannernormal" valign='middle'>
                        <form action="{URL}" method="post">
                            Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
                            S/d <input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
                            Cari <input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
                            <input type="submit" value="cari" />&nbsp;
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 align='center' valign='middle'>
                        <table width="100%">
                            <tr>
                                <td width="100%" align="right">
                                    {PAGING}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- END HEADER-->

            <table class="border" width='100%' >
                <tr>
                    <th width=20>No</th>
                    <th width=100>Kode Sopir</th>
                    <th width=200>Supir</th>
                    <th width=100>Cabang</th>
                    <th width=100>No Rekening</th>
                    <th width=200>Jurusan</th>
                    <th width=100>Total Trip</th>
                    <th width=100>Biaya</th>
                    <th width=100>Total CGS</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td ><div align="center"><font size=3 >{ROW.no}</font></div></td>
                    <td ><div align="left"><font size=2 >{ROW.kodesopir}</font></div></td>
                    <td ><div align="left"><font size=2 >{ROW.Supir}</font></div></td>
                    <td ><div align="left"><font size=2 >{ROW.Tugas}</font></div></td>
                    <td ><div align="right"><font size=2 >{ROW.Norek}</font></div></td>
                    <td ><div align="left"><font size=2>{ROW.Cabang}</font></div></td>
                    <td ><div align="center"><font size=2>{ROW.Trip}</font></div></td>
                    <td ><div align="center"><font size=2 >{ROW.Biaya}</font></div></td>
                    <td><div align="center"><font size=2>{ROW.TotalCGS}</font></div></td>
                </tr>
                <!-- END ROW -->
            </table>

            <table width='100%' cellspacing="0">
                <tr>
                    <td colspan=2 align='center' valign='middle'>
                        <table width="100%">
                            <tr>
                                <td width="50%" align="right">
                                </td>
                                <td width="50%" align="right">
                                    {PAGING}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>