<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
	
	function ubahStatus(id){
	
			new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=ubahstatus&id="+id,
			 onLoading: function(request) 
			 {
				Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
				
			 },
			 onSuccess: function(request) 
			 {			
				Element.hide('loading_proses');
				if(request.responseText==1){
					document.forms["formcari"].submit();
				}else if(request.responseText==2) {
					alert("Anda tidak memiliki akses untuk mengubah data ini!");
				}else{
					alert("Masih ada penumpang di jadwal tersebut, silahkan pindahkan terlebih dahulu penumpang dijadwal tersebut!");
				}
			},
			 onFailure: function(request) 
			 {
			 }
			});  
		
		return false;
			
	}
	
	function updateMobil(){
		
		tgl					= document.getElementById("tanggal").value;
		kode_jadwal	= document.getElementById("dlgdaftarmobilkodejadwal").innerHTML;
		kode_kendaraan = document.getElementById("dlgcbomobil").value;
		aktif				= 1;
		
		new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=update&submode=0&tgl="+tgl+"&kode_jadwal="+kode_jadwal+"&kodekendaraaan="+kode_kendaraan+"&aktif="+aktif,
			 onLoading: function(request) 
			 {
					Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
			 },
			 onSuccess: function(request) 
			 {			
					Element.hide('loading_proses');
					my_return = request.responseText;
					
					if(my_return==1){
						document.forms["formcari"].submit();
					}else if(my_return==2){
						alert("Anda tidak memiliki akses untuk mengubah data ini!");exit;
			 		}else {
						alert("Terjadi kegagalan");exit;
					}
					
			},
			onFailure: function(request) 
			{
			}
		});  
	
	}
	
	function updateSopir(){
		
		tgl					= document.getElementById("tanggal").value;
		kode_jadwal	= document.getElementById("dlgdaftarsopirkodejadwal").innerHTML;
		kode_sopir	= document.getElementById("dlgcbosopir").value;
		aktif				= 1;
		
		new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=update&submode=1&tgl="+tgl+"&kode_jadwal="+kode_jadwal+"&kode_sopir="+kode_sopir+"&aktif="+aktif,
			 onLoading: function(request) 
			 {
					Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
			 },
			 onSuccess: function(request) 
			 {			
					Element.hide('loading_proses');
					my_return = request.responseText;
					
					if(my_return==1){
						document.forms["formcari"].submit();
					}else if(my_return==2){
						alert("Anda tidak memiliki akses untuk mengubah data ini!");exit;
					}else {
						alert("Terjadi kegagalan");exit;
					}
					
			},
			onFailure: function(request) 
			{
			}
		});  
	
	}
	
	function simpan(tgl,kode_jadwal,aktif){
		
		new Ajax.Request("pengaturan_penjadwalan_kendaraan.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=update&submode=2&tgl="+tgl+"&kode_jadwal="+kode_jadwal+"&aktif="+aktif,
			 onLoading: function(request) 
			 {
					Element.show('loading_proses');
			 },
			 onComplete: function(request) 
			 {
			 },
			 onSuccess: function(request) 
			 {			
					Element.hide('loading_proses');
					my_return = request.responseText;
					
					if(my_return==1){
						document.forms["formcari"].submit();
					}
					else {
						alert("Masih ada penumpang di jadwal tersebut, silahkan pindahkan terlebih dahulu penumpang dijadwal tersebut!");
					}
					
			},
			onFailure: function(request) 
			{
			}
		});  
	
	}
	
	function getUpdateTujuan(asal){
			// fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
			
			if(document.getElementById('rewrite_tujuan')){
				document.getElementById('rewrite_tujuan').innerHTML = "";
			}
			
			new Ajax.Updater("rewrite_tujuan","pengaturan_penjadwalan_kendaraan.php?sid={SID}", 
			{
					asynchronous: true,
					method: "get",
					parameters: "asal=" + asal + "&jurusan={ID_JURUSAN}&mode=get_tujuan",
					onLoading: function(request) 
					{
							Element.show('loading_tujuan');
					},
					onComplete: function(request) 
					{
							Element.hide('loading_tujuan');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			});   
	}
	
	function setDaftarMobil(mobil,kodejadwal,tanggal,aktif){
			
			new Ajax.Updater("dlgdaftarmobilcbomobil","pengaturan_penjadwalan_kendaraan.php?sid={SID}", 
			{
					asynchronous: true,
					method: "post",
					parameters: "mode=setdaftarmobil" +
						"&mobil=" + mobil+
						"&kodejadwal=" + kodejadwal +
						"&tanggal=" + tanggal +
						"&aktif=" + aktif,
					onLoading: function(request) 
					{
							dlg_daftar_mobil.show();
							Element.show('dlgdaftarmobilloading');
					},
					onComplete: function(request) 
					{
							document.getElementById("dlgdaftarmobilkodejadwal").innerHTML	= kodejadwal;
							Element.hide('dlgdaftarmobilloading');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			});   
	}
	
	function setDaftarSopir(sopir,kodejadwal,tanggal,aktif){
			
			
			new Ajax.Updater("dlgdaftarsopircbosopir","pengaturan_penjadwalan_kendaraan.php?sid={SID}", 
			{
					asynchronous: true,
					method: "post",
					parameters: "mode=setdaftarsopir" +
						"&sopir=" + sopir +
						"&kodejadwal=" + kodejadwal +
						"&tanggal=" + tanggal +
						"&aktif=" + aktif,
					onLoading: function(request) 
					{
							dlg_daftar_sopir.show();
							Element.show('dlgdaftarsopirloading');
					},
					onComplete: function(request) 
					{
							document.getElementById("dlgdaftarsopirkodejadwal").innerHTML	= kodejadwal;
							Element.hide('dlgdaftarsopirloading');
					},
					onFailure: function(request) 
					{ 
						assignError(request.responseText); 
					}
			}); 
	}
	
	// komponen khusus dojo 
	dojo.require("dojo.widget.Dialog");
	
	function init(e) {
		// inisialisasi variabel
		
		//control dialog daftar mobil
		dlg_daftar_mobil				= dojo.widget.byId("dlgdaftarmobil");
		dlg_daftar_mobil_cancel	= document.getElementById("dlgdaftarmobilbuttoncancel");
		dlg_daftar_mobil.setCloseControl(dlg_daftar_mobil_cancel);
		
		//control dialog daftar sopir
		dlg_daftar_sopir				= dojo.widget.byId("dlgdaftarsopir");
		dlg_daftar_sopir_cancel	= document.getElementById("dlgdaftarsopirbuttoncancel");
		dlg_daftar_sopir.setCloseControl(dlg_daftar_sopir_cancel);
		
	}
	
	dojo.addOnLoad(init);

</script>

<!--BEGIN dialog daftar mobil-->
<div dojoType="dialog" id="dlgdaftarmobil" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="400" cellpadding="0" cellspacing="0" >
	<tr><td align="center" valign="middle" style="color: white;font-size: 16px;margin-top: 0px;">DAFTAR MOBIL</td></tr>
	<tr>
		<td style="background-color: white;">
			<table width="100%">
				<tr><td width="100">Kode Jadwal</td><td width="1">:</td><td><b></b><span id="dlgdaftarmobilkodejadwal"></span></b></td></tr>
				<tr><td>Mobil</td><td width="1">:</td><td><b></b><span id="dlgdaftarmobilcbomobil"></span><span id='dlgdaftarmobilloading' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></b></td></tr>
				<tr><td colspan="3" align="center"><br><input type="button" id="dlgdaftarmobilbuttonsimpan" value="Simpan" onclick="updateMobil();">&nbsp;<input type="button" id="dlgdaftarmobilbuttoncancel" value="Batal"></td></tr>
			</table>
		</td>
	</tr>
</table>
<br>
</div>
<!--END dialog daftar mobil-->

<!--BEGIN dialog daftar sopir-->
<div dojoType="dialog" id="dlgdaftarsopir" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="400" cellpadding="0" cellspacing="0" >
	<tr><td align="center" valign="middle" style="color: white;font-size: 16px;margin-top: 0px;">DAFTAR SOPIR</td></tr>
	<tr>
		<td style="background-color: white;">
			<table width="100%">
				<tr><td width="100">Kode Jadwal</td><td width="1">:</td><td><b></b><span id="dlgdaftarsopirkodejadwal"></span></b></td></tr>
				<tr><td>Sopir</td><td width="1">:</td><td><b></b><span id="dlgdaftarsopircbosopir"></span><span id='dlgdaftarsopirloading' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='000000' size=2>sedang memposes...</font></span></b></td></tr>
				<tr><td colspan="3" align="center"><br><input type="button" id="dlgdaftarsopirbuttonsimpan" value="Simpan" onclick="updateSopir();">&nbsp;<input type="button" id="dlgdaftarsopirbuttoncancel" value="Batal"></td></tr>
			</table>
		</td>
	</tr>
</table>
<br>
</div>
<!--END dialog daftar sopir-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<form action="{ACTION_CARI}" method="post" name="formcari">
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Penjadwalan Kendaraan</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<table width='500'>
						<tr><td width=100 class='bannernormal'>Tanggal</td><td width=5>:</td><td><input readonly="yes"  id="tanggal" name="tanggal" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TANGGAL}"></td></tr>
						<tr><td width=100 class='bannernormal'>Cabang Asal</td><td width=5>:</td><td><select name='opt_cabang_asal' id='opt_cabang_asal' onChange="getUpdateTujuan(this.value)">{CABANG_ASAL}</select></td></tr>
						<tr><td class='bannernormal'>Cabang Tujuan</td><td>:</td><td><div id='rewrite_tujuan'></div><span id='loading_tujuan' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='white' size=2>sedang memposes...</font></span></td></tr>
						<tr><td colspan=3 align='center' class='bannernormal'><input type="submit" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" /></td></tr>
					</table>
				</td>
			</tr>
		</table>
		<script type="text/javascript">
			getUpdateTujuan(opt_cabang_asal.value);
		</script>
		</form>
		<table width='1150'>
		<tr><td colspan=9 align='center' height=20><span id='loading_proses' style='display:none;'><img src="{TPL}images/loading.gif"/><font color='909090' size=2>sedang memposes...</font></span></td></tr>
    <tr>
			<td width='100%' align='right' colspan=9 valign='bottom'>{PAGING}</td>
		</tr>
		<tr>
       <th width=20>No</th>
			 <th width=50>Jam</th>
			 <th width=30>Kode Jadwal</th>
			 <th width=100>Kendaraan</th>
			 <th width=100>Sopir</th>
			 <th width=20>Kursi</th>
			 <th width=50>Status</th>
			 <th width=150>Remark</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td align="right">{ROW.no}</td>
			 <td align="center"><font size=4><b>{ROW.jam}</b></font></td>
       <td align="left">{ROW.kode}</td>
			 <td align="left">{ROW.nopol}</td>
			 <td align="left">{ROW.kode_sopir}</td>
			 <td align="right">{ROW.kursi}</td>
			 <td align="center">{ROW.status}</td>
			 <td align="left">{ROW.remark}</td>
     </tr>  
     <!-- END ROW -->
		 {NO_DATA}
    </table>
    <table width='1000'>
			<tr>
				<td width='100%' align='right'>{PAGING}</td>
			</tr>
		</table>
 </td>
</tr>
</table>