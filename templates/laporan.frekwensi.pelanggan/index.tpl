<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function setData(bulan){
		tahun	=document.getElementById('tahun').value;
		
		window.location='{URL}'+'&bulan='+bulan+'&tahun='+tahun;
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","laporan_keuangan_tiketux.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		
		/*if(document.getElementById('rewrite_tujuan')){
			document.getElementById('rewrite_tujuan').innerHTML = "";
    }*/
		
		new Ajax.Updater("rewrite_tujuan","laporan_keuangan_tiketux.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	getUpdateAsal("{KOTA}");
	getUpdateTujuan("{ASAL}");
	
</script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<!--HEADER-->
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Keberangkatan Grup</td>
			<td align='right' valign='middle' class="bannercari" style="color: white;padding-top: 10px;">
				<form action="{ACTION_CARI}" method="post">
					Tahun&nbsp;<select id="tahun" name="tahun"><option value="{TAHUN_1}" {SEL_TAHUN1}>{TAHUN_1}</option><option value="{TAHUN_2}" {SEL_TAHUN2}>{TAHUN_2}</option><option value="{TAHUN_3}" {SEL_TAHUN3}>{TAHUN_3}</option></select>&nbsp;
					<input name="btn_cari" type="submit" value="cari" />
				</form>
			</td>
		</tr>
		<tr>
			<td colspan=2 align='center' valign='middle'>
				<table>
					<tr>
						<!--<td>
							<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
						</td><td bgcolor='D0D0D0'></td>-->
						<td>
							<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<!-- END HEADER-->
	<table class="border" width='100%' >
  <tr>
		 <th width=100>Grup</th>
		 <th width=100>Jan</th>
		 <th width=100>Feb</th>
		 <th width=100>Mar</th>
		 <th width=100>Apr</th>
		 <th width=100>Mei</th>
		 <th width=100>Jun</th>
		 <th width=100>Jul</th>
		 <th width=100>Agu</th>
		 <th width=100>Sep</th>
		 <th width=100>Okt</th>
		 <th width=100>Nov</th>
		 <th width=100>Des</th>
   </tr>
   <!-- BEGIN ROW -->
	 <tr class="{ROW.odd}">
     <td align="center">{ROW.grup}</td>
		 <td align="right">{ROW.jan}</td>
		 <td align="right">{ROW.feb}</td>
		 <td align="right">{ROW.mar}</td>
		 <td align="right">{ROW.apr}</td>
		 <td align="right">{ROW.mei}</td>
		 <td align="right">{ROW.jun}</td>
		 <td align="right">{ROW.jul}</td>
		 <td align="right">{ROW.agu}</td>
		 <td align="right">{ROW.sep}</td>
		 <td align="right">{ROW.okt}</td>
		 <td align="right">{ROW.nov}</td>
		 <td align="right">{ROW.des}</td>
   </tr>
   <!-- END ROW -->
  </table>
 </td>
</tr>
</table>