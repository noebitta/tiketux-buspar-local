<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    function init(e){
        //control dialog
        dialog_biaya			= dojo.widget.byId("dialogbiayatambahan");
        dlg_biaya_cancel        = document.getElementById("dialogcancel");
    }
    dojo.addOnLoad(init);
    function ShowDialog(){
        dialog_biaya.show();
    }
    function InsertBiaya(){
        saldo = document.getElementById("KAS").value;
        pengeluaran = document.getElementById("jumlah").value;
        if(jenis.value == ""){
            alert("Jenis Biaya Tidak Boleh Kosong");
        }else if(jumlah.value == ""){
            alert("Jumlah Biaya Tidak Boleh Kosong");
        }else if(penerima.value == ""){
            alert("Penerima Biaya Tidak Boleh Kosong");
        }else if(ket.value == ""){
            alert("Keterangan Tidak Boleh Kosong");
        }else if(parseInt(pengeluaran) > parseInt(saldo)) {
            alert("Error!!! Pengeluaran Lebih Besar Dari Pada Saldo");
        }else{
            new Ajax.Request("biaya_petty_cash.php?sid={SID}",{
                asynchronous: true,
                method: "get",
                parameters: "mode=InsertData"+
                "&jenis="+jenis.value+
                "&jumlah="+jumlah.value+
                "&penerima="+penerima.value+
                "&keterangan="+ket.value,
                onLoading: function(request){
                    progressbar.show();
                },
                onComplete: function(request){
                    progressbar.hide();
                    dialog_biaya.hide();
                    console.log("oncomplete");
                    document.getElementById("myForm").reset();
                },
                onSuccess: function(request){
                    eval(request.responseText);

                },
                onFailure: function(request){
                    alert('Error !!! Cannot Save');
                    assignError(request.responseText);
                }
            });
        }
    }
    function Cetak(id, jumlah){
        saldo = document.getElementById("KAS").value;
        if(parseInt(jumlah) > parseInt(saldo)){
            alert("Error!!! Pengeluaran Lebih Besar Dari Pada Saldo");
        }else{
            StartCustomeSize('biaya_petty_cash_cetak.php?sid={SID}&id='+id,'600','650');
        }
    }
</script>
<!--dialog biaya tambahan-->
<div dojoType="dialog" id="dialogbiayatambahan" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
    <form onsubmit="false" id="myForm">
        <table width="500">
            <tr>
                <td bgcolor='ffffff' align='center'>
                    <table>
                        <tr><td colspan='2' align="center"><h2>Petty Cash</h2></td></tr>
                        <input type="hidden" name="mode" value="InsertData">
                        <tr><td align="right">Jenis Biaya: </td><td>
                                <select id="jenis" name="jenis">
                                    {BIAYA}
                                </select>
                            </td>
                        </tr>
                        <tr><td align="right">Jumlah : </td><td><input type="text" name="jumlah" id="jumlah"></td></tr>
                        <tr><td align="right">Penerima : </td><td><input type="text" name="penerima" id="penerima"></td></tr>
                        <tr><td align="right">Keterangan : </td><td><textarea name="ket" id="ket" rows="4"></textarea></td></tr>
                    </table>
                    <span id='progressbar' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <br>
                    <input type="button" id="dialogcancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_biaya.hide();">
                    <input type="button" onclick="InsertBiaya()"  id="dialogbiayatambahanproses" value="&nbsp;Proses&nbsp;">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--END dialog biaya tambahan-->

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">PETTY CASH</td>
                    <td class="bannernormal" align="right" valign="middle">
                        <input type="hidden" name="KAS" id="KAS" value="{KAS}">
                       {SALDO}
                        <br>
                        <form method="post" action="">
                            &nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                            &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                            &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;
                            <input type="submit" value="cari" />&nbsp;
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 align='left' valign='middle'>
                        <table>
                            <tr>
                                <td >
                                    <input class="tombol" value="Tambah Biaya" id="tambah_biaya" type="button" onclick="ShowDialog()">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- END HEADER-->
        </td>
        <!-- BODY -->
        <table class="border" width="100%">
            <thead>
            <tr>
                <th width="30">No</th>
                <th width="100">CABANG</th>
                <th width="150">PEMBUAT</th>
                <th width="150">TANGGAL BUAT</th>
                <th width="150">JENIS BIAYA</th>
                <th width="100">JUMLAH</th>
                <th width="150">PENERIMA</th>
                <th width="150">KETERANGAN</th>
                <th width="150">TANGGAL CETAK</th>
                <th width="250" colspan="2">Status</th>
                <th width="150">RELEASER</th>
                <th width="150">PENCETAK</th>
            </tr>
            </thead>
            <tbody>
            <!-- BEGIN ROW -->
            <tr class="{ROW.baris}">
                <td>{ROW.No}</td>
                <td >{ROW.KodeCabang}</td>
                <td>{ROW.Pembuat}</td>
                <td><div align="center">{ROW.TglBuat}</div></td>
                <td><div>{ROW.JenisBiaya}</div></td>
                <td><div align="right">{ROW.Jml}</div></td>
                <td><div>{ROW.Penerima}</div></td>
                <td><div>{ROW.Keterangan}</div></td>
                <td><div align="center">{ROW.TglCetak}</div></td>
                <td>{ROW.Status}</td>
                <td><a href="{ROW.Link}" target="{ROW.target}" onclick="{ROW.Onclik}"><img src='{TPL}images/{ROW.Icon}' /></a></td>
                <td>{ROW.Releaser}</td>
                <td>{ROW.Pencetak}</td>
            </tr>
            <!-- END ROW -->
            </tbody>
        </table>
        <!--END BODY-->
    </tr>
</table>