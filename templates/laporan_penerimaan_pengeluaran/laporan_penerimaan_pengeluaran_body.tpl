<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
    // komponen khusus dojo
    dojo.require("dojo.widget.Dialog");

    function init(e) {
        // inisialisasi variabel

        //control dialog daftar cso dan counter
        dialog_realisasi				= dojo.widget.byId("dialog_realisasi");
        dialog_realisasi_cancel         = document.getElementById("cancel");
        dialog_realisasi.setCloseControl(dialog_realisasi_cancel);

    }

    dojo.addOnLoad(init);

    function getData(manifest, trip, jam) {
        document.getElementById("data_manifest").value = manifest;
        document.getElementById("manifest").innerHTML = manifest;
        document.getElementById("jurusan").innerHTML = trip;
        document.getElementById("jam").innerHTML = jam;
    }

    function updateMNF() {
        new Ajax.Request("new_lppc.php?sid={SID}",{
            asynchronous: true,
            method: "post",
            parameters: "mode=manifest_realisasi"+
            "&data_manifest="+data_manifest.value+
            "&biaya="+realisasi.value,
            onLoading: function(request){
                progressbar.show();
            },
            onComplete: function(request){
                console.log(request.responseText);
                if(request.responseText == 1){
                    alert("Realisasi Berhasil disimpan");
                    document.forms["formcari"].submit();
                }else{
                    alert("Terjadi Kesalahan");
                    console.log(request.responseText);
                }
            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }

    document.getElementById('{CABANG}').selected = true;
    var cabang = document.getElementById('cabangasal');
    cabang.value = '{CABANG}';


    function Start(page) {
        OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
    }


    function SimpanModal(){
        setoran = document.getElementById('setoran').value;
        saldo   = document.getElementById('saldo').value;
        modal   = document.getElementById('modal_awal').value;
        id      = document.getElementById('IDMODAL').value;
        cabang  = document.getElementById('cabangasal').value;
        new Ajax.Request("laporan_penerimaan_pengeluaran.php?sid={SID}",{
            asynchronous: true,
            method: "get",
            parameters: "mode=validasi"+
            "&setoran="+setoran+
            "&saldo="+saldo+
            "&modal="+modal+
            "&cabang="+cabang+
            "&id="+id,
            onLoading: function(request){
                progressbar.show();
            },
            onComplete: function(request){
                console.log('ajax success');
            },
            onSuccess: function(request){
                eval(request.responseText);

            },
            onFailure: function(request){
                alert('Error !!! Cannot Save');
                assignError(request.responseText);
            }
        });
    }
</script>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
            <!--HEADER-->
            <table width='100%' cellspacing="0">
                <tr height=40 class='banner'>
                    <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Penerimaan Pengeluaran Counter</td>
                    <td class="bannerjudul">&nbsp;</td>
                    <td align="right" valign='middle' class="bannernormal">
                        <br>
                        <form method="post" action="{ACTION_CARI}"  id="formcari">
                            <table>
                                <tr>
                                    <td style="{SECRET}">
                                        <font color="white">Cabang :</font> <select id='cabangasal' name='cabang' >{SELECT}</select>
                                    </td>
                                    <td>
                                        <font color="white">Tanggal:</font> <input readonly="yes"  id="awal" name="awal" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{AWAL}" size=10>
                                    </td>
                                    <td>
                                        <input type="submit" value="Cari">
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;<input type="button" class="tombol" value="Validasi" onclick="SimpanModal()" style="{SECRET}">
                    </td>
                    <td align='center' valign='middle'>
                        <table>
                            <tr>
                                <td align="center">
                                    <a href='#' onClick="{EXCEL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right">
                        <input type="hidden" id="IDMODAL" name="IDMODAL" value="{IDMODAL}">
                        <input type="hidden" id="modal_awal" name="modal_awal" value="{MODAL}">
                        <font size="3"><b>MODAL : Rp. {MODAL}</b></font>
                    </td>
                </tr>
            </table>
            <!--END HEADER-->
            <!--BODY-->
            <table  class="border" width='100%'>
                <tr>
                    <td rowspan="3" valign="middle" align="center" bgcolor="#708090" ><font size=3 color="white"><b>No</b></font></td>
                    <td rowspan="3" valign="middle" bgcolor="#708090" align="center"><font size=3 color="white"><b>Keterangan</b></font></td>
                    <td colspan="7" align="center" bgcolor="#708090"><font size=3 color="white"><b>Penjualan</b></font></td>
                    <td colspan="2" align="center" bgcolor="#708090"><font size=3 color="white"><b>Pengeluaran</b></font></td>
                <tr>
                <tr>
                    <td align="center" bgcolor="#708090"><font color="white"><b>CASH</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>EDC</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>VOUCHER</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>ELEVANIA</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>MEMBER</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>TRANSFER</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>SYSTEM</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>LPOC</b></font></td>
                    <td align="center" bgcolor="#708090"><font color="white"><b>OTHER</b></font></td>
                </tr>
                <tr class="even">
                    <td align="center"></font></td>
                    <td><font size=3><b>TIKET</b></font></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>
                <!-- BEGIN TIKET -->
                <tr class="{TIKET.odd}">
                    <td align="center"><font size=2><b>{TIKET.no}</b></font></td>
                    <td><font size=2>{TIKET.nospj} / {TIKET.jurusan} {TIKET.jam} {TIKET.mobil} / {TIKET.sopir}  ( {TIKET.jml_tiket} )</font></td>
                    <td align="right">{TIKET.tunai}</td>
                    <td align="right">{TIKET.edc}</td>
                    <td align="right">{TIKET.voucher}</td>
                    <td align="right">{TIKET.elevania}</td>
                    <td align="right">{TIKET.member}</td>
                    <td align="right">{TIKET.transfer}</td>
                    <td align="right">{TIKET.tiketux}</td>
                    <td align="right">{TIKET.lpoc}</td>
                    <td align="right"></td>
                </tr>
                <!-- END TIKET -->

                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td align="center"><font size=2><b>{ROW.no}</b></font></td>
                    <td><font size=2 color="red">{ROW.tiket} / {ROW.KodeJurusan} {ROW.JamBerangkat} {ROW.TglBerangkat} / {ROW.penumpang}</font></td>
                    <td align="right">{ROW.Tunai}</td>
                    <td align="right">{ROW.EDC}</td>
                    <td align="right">{ROW.VOUCHER}</td>
                    <td align="right">{ROW.ELEVANIA}</td>
                    <td align="right">{ROW.MEMBER}</td>
                    <td align="right">{ROW.TRANSFER}</td>
                    <td align="right">{ROW.Sistem}</td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>
                <!-- END ROW -->

                <!-- BEGIN MEMBER -->
                <tr class="{MEMBER.odd}">
                    <td align="center"><font size=2><b>{MEMBER.no}</b></font></td>
                    <td><font size=2 color="red">{MEMBER.keterangan}</font></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right">{MEMBER.jumlah}</td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>
                <!-- END MEMBER -->

                <tr class="even">
                    <td align="center"></font></td>
                    <td><font size=3><b>PAKET</b></font></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>

                <!-- BEGIN PAKET -->
                <tr class="{PAKET.odd}">
                    <td align="center"><font size=2><b>{PAKET.no}</b></font></td>
                    <td><font size=2>{PAKET.nospj} / {PAKET.jurusan}  {PAKET.jam} {PAKET.mobil} / {PAKET.sopir} ({PAKET.jml_paket})</font></td>
                    <td align="right">{PAKET.paket_tunai}</td>
                    <td align="right">{PAKET.paket_edc}</td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>
                <!-- END PAKET -->

                <!-- BEGIN PAKET_PLUS -->
                <tr class="{PAKET_PLUS.odd}">
                    <td align="center"><font size=2><b>{PAKET_PLUS.no}</b></font></td>
                    <td><font size=2  color="red">{PAKET_PLUS.paket} / {PAKET_PLUS.KodeJurusan} {PAKET_PLUS.JamBerangkat} {PAKET_PLUS.TglBerangkat}/ Pengirim {PAKET_PLUS.pengirim} / Penerima {PAKET_PLUS.penerima}</font></td>
                    <td align="right">{PAKET_PLUS.Tunai}</td>
                    <td align="right">{PAKET_PLUS.EDC}</td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>
                <!-- END PAKET_PLUS -->


                <tr class="even">
                    <td align="center"></font></td>
                    <td><font size=3><b>BIAYA TAMBAHAN</b></font></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                </tr>

                <!-- BEGIN ROWBIAYA -->
                <tr class="{ROWBIAYA.odd}">
                    <td align="center"><font size=2><b>{ROWBIAYA.no}</b></font></td>
                    <td><font size=2>{ROWBIAYA.Penerima} / {ROWBIAYA.Jurusan} / {ROWBIAYA.Keterangan}</font></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"><font size=2></font></td>
                    <td align="right"><font size=2></font></td>
                    <td align="right"><font size=2>{ROWBIAYA.Jumlah}</font></td>
                </tr>
                <!-- END ROWBIAYA -->

                <tr  bgcolor='ffff00'>
                    <td colspan="2" align="left"><font size=3><b>&nbsp;&nbsp;&nbsp;Total</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_CASH}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_EDC}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_VOUCHER}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_ELEVANIA}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_MEMBER}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_TRANSFER}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_SISTEM}</b></font></td>
                    <td align="right"><font size=3><b>{TOTAL_LPOC}</b></font></td>
                    <td align="right"><font size=3><b>{BLAIN}</b></font></td>
                </tr>
            </table>
            <!--END BODY-->
            <table align="right" width="250">
                <tr>
                    <td>
                        <input type="hidden" name="setoran" id="setoran" value="{SETORAN}">
                        <p align="right"><font size="3"><b>SETORAN : Rp. {SETORAN}</b></font></p>
                    </td>
                </tr>
                <tr>
                    <td>
                       <hr>
                    </td>
                </tr>
                <tr>
                    <Td>
                        <input type="hidden" value="{SALDO}" id="saldo" name="saldo">
                        <p align="right"><font size="3"><b>SALDO : Rp. {SALDO}</b></font></p>
                    </Td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!--BEGIN dialog -->
<div dojoType="dialog" id="dialog_realisasi" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
    <table width="400" cellpadding="0" cellspacing="0" >
        <tr><td align="center" valign="middle" style="color: white;font-size: 16px;margin-top: 0px;">Realisasi Biaya Tol</td></tr>
        <tr>
            <td style="background-color: white;">
                <table width="100%">
                    <tr>
                        <td width="100">Manifest</td>
                        <td width="1">:</td>
                        <td><input type="hidden" id="data_manifest">
                            <b><span id="manifest"></span></b></td>
                    </tr>
                    <tr>
                        <td width="100">Jurusan</td>
                        <td width="1">:</td>
                        <td>
                            <b><span id="jurusan"></span></b>
                        </td>
                    </tr>
                    <tr>
                        <td width="100">Jam</td>
                        <td width="1">:</td>
                        <td><b><span id="jam"></span></b></td>
                    </tr>
                    <tr>
                        <td>Realisasi</td>
                        <td width="1">:</td>
                        <td>
                            <input type="text" name="realisasi" id="realisasi">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <br><input type="button" value="Simpan" onclick="updateMNF();">&nbsp;
                            <input type="button" value="Batal" id="cancel">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
</div>
<!--END dialog -->
