<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">

var kode;

function cekValTahun(tahun){
	cek_value=tahun*0;
	
	if(cek_value!=0){
		return false;
	}
	
	if((tahun*1)<1920 || (tahun*1>2050)){
		return false;
	}
	else{
		return true;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_kendaraan_invalid');
	Element.hide('no_polisi_invalid');
	Element.hide('km_invalid');
	Element.hide('tahun_pembuatan_invalid');
	
	kode_kendaraan		= document.getElementById('kode_kendaraan');
	no_polisi					= document.getElementById('no_polisi');
	no_stnk						= document.getElementById('no_stnk');
	km								= document.getElementById('km');
	tahun_pembuatan		= document.getElementById('tahun_pembuatan');
	
	if(kode_kendaraan.value==''){
		valid=false;
		Element.show('kode_kendaraan_invalid');
	}
	
	if(no_polisi.value==''){
		valid=false;
		Element.show('no_polisi_invalid');
	}
	
	if(!cekValTahun(tahun_pembuatan.value)){	
		valid=false;
		Element.show('tahun_pembuatan_invalid');
	}
	
	cek_value=km.value*0;
	
	if(cek_value!=0){
		valid=false;
		Element.show('km_invalid');
	}
	
	if(km.value==''){
		km.value=0;
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data_mobil" action="{U_MOBIL_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Mobil</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='400'>
				<table width='400'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
						<td colspan=3><h3><u>Data Umum Kendaraan</u></h3>
						</td>
					</tr>
					<tr>
						<input type="hidden" name="kode_kendaraan_old" value="{KODE_KENDARAAN_OLD}">
						<td width='200'><u>Kode Kendaraan</u></td><td width='5'>:</td>
						<td colspan="2"><input type="text" id="kode_kendaraan" name="kode_kendaraan" value="{KODE_KENDARAAN}" maxlength=15 onChange="Element.hide('kode_kendaraan_invalid');"><span id='kode_kendaraan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    	</tr>
					<tr>
			      		<input type="hidden" name="no_polisi_old" value="{NO_POLISI_OLD}">
						<td width='200'><u>Nomor Polisi</u></td><td width='5'>:</td>
						<td colspan="2"><input type="text" id="no_polisi" name="no_polisi" value="{NO_POLISI}" maxlength=15 onChange="Element.hide('no_polisi_invalid');"><span id='no_polisi_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    	</tr>
					<tr>
			      		<td>Jenis</td>
						<td>:</td>
						<td colspan="2"><input type="text" name="jenis" value="{JENIS}" maxlength=50></td>
			    	</tr>
					<tr>
			      		<td>Merek</td>
						<td>:</td>
						<td colspan="2"><input type="text" name="merek" value="{MEREK}" maxlength=50></td>
			    	</tr>
					<tr>
			      		<td><u>Tahun pembuatan</u></td>
						<td>:</td>
						<td colspan="2"><input type="text" id="tahun_pembuatan" name="tahun_pembuatan" value="{TAHUN_PEMBUATAN}" maxlength=4 onChange="Element.hide('tahun_pembuatan_invalid');"><span id='tahun_pembuatan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
			    	</tr>
					<tr>
			      		<td>Warna</td>
						<td>:</td>
						<td colspan="2"><input type="text" name="warna" value="{WARNA}" maxlength=50></td>
			    	</tr>
					<tr>
			      		<td><u>Jum. Kursi</u></td>
						<td>:</td>
						<td colspan="2"><select id="jumlah_kursi" name="jumlah_kursi">{KURSI}</select></td>
			    	</tr>
					<tr>
						<td><u>Kelengkapan</u></td>
						<td>:</td>
						<td colspan="2">
							<input type="checkbox" name="dongkrak" id="dongkrak" value="Dongkrak" {DONGKRAK}>Dongkrak
							<input type="checkbox" name="ban_serep" id="ban_serep" value="Ban Serep" {BANSEREP}>Ban Serep<br>
							<input type="checkbox" name="kunci_roda" id="kunci_roda" value="Kunci Roda" {KUNCIRODA}>Kunci Roda
							<input type="checkbox" name="p3k" id="p3k" value="P3K" {P3K}>Kotak PPPK
							<input type="checkbox" name="apar" id="apar" value="Apar" {APAR}>Apar
						</td>
					</tr>
					<tr>
					  	<td><u>Sopir 1</u></td>
						<td>:</td>
						<td colspan="2"><select id='sopir1' name='sopir1'>{SOPIR1}</select></td>
					</tr>
					<tr>
					  	<td>Sopir 2</td>
						<td>:</td>
						<td colspan="2"><select id='sopir2' name='sopir2'>{SOPIR2}</select></td>
					</tr>
					</tr>
					<tr>
						<td>Sopir 3</td>
						<td>:</td>
						<td colspan="2"><select id='sopir3' name='sopir3'>{SOPIR3}</select></td>
					</tr>
					<tr>
						<td>Status</td>
						<td>:</td>
						<td>
							<select id="DT" name="DT">
								<option value=0 {DT_0}>DAYTRANS</option>
								<option value=1 {DT_1}>WHITE HORSE</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan=3 height=40 valign='bottom'><h3><u>Data surat-surat kendaraan</u></h3></td>
					</tr>
					<tr>
						<td>No Rangka</td>
						<td>:</td>
						<td><input type="text" name="no_rangka" value="{NO_RANGKA}" maxlength=50></td>
					</tr>
					<tr>
						<td>No Mesin</td>
						<td>:</td>
						<td><input type="text" id="no_mesin" name="no_mesin" value="{NO_MESIN}" maxlength=50></td>
					</tr>
					<tr>
						<td>No STNK</td>
						<td>:</td>
						<td><input type="text" id='no_stnk' name="no_stnk" value="{NO_STNK}" maxlength=50 ></td>
						<td><input readonly="yes" value="{STNK}" id="tempo_stnk" name="tempo_stnk" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" ></td>
					</tr>
					<tr>
						<td>No BPKB</td>
						<td>:</td>
						<td><input type="text" name="no_bpkb" value="{NO_BPKB}" maxlength=50></td>
						<td><input readonly="yes" value="{PAJAK}" id="tempo_pajak" name="tempo_pajak" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)"></td>
					</tr>
					<tr>
						<td>No KIR</td>
						<td>:</td>
						<td><input type="text" name="nokir" value="{NOKIR}"></td>
						<td><input readonly="yes" value="{KIR}" id="kir" name="kir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)"></td>
					</tr>
					<tr>
						<td>No SIPA</td><td>:</td>
						<td><input type="text" name="nosipa" value="{NOSIPA}"></td>
						<td><input type="text" name="sipa" id="sipa" value="{SIPA}" readonly="yes" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)"></td>
					</tr>
					<tr>
						<Td>No KP</Td>
						<td>:</td>
						<td><input type="text" name="nokp" value="{NOKP}"></td>
						<td><input type="text" name="kp" id="kp" value="{KP}" readonly="yes" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)"></td>
					</tr>
				</table>
			</td>
			<td width=1 bgcolor='D0D0D0'></td>
			<td align='center' valign='top'>
				<table width='400'>   
					<tr>
						<td colspan=3><h3><u>Data Operasional</u></h3></td>
					</tr>
					<tr>
			      		<td>KM terakhir</td>
						<td>:</td>
						<td><input type="text" id="km" name="km" value="{KM}" maxlength=10 onChange="Element.hide('km_invalid');"/><span id='km_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span></td>
					</tr>
					<tr>
						<td>Kepemilikan Cabang</td>
						<td>:</td>
						<td>
							<select id='cabang' name='cabang'>
								{OPT_CABANG}
							</select>
						</td>
					</tr>
					<tr>
					  	<td>Status Aktif</td>
						<td>:</td>
						<td>
							<select id="aktif" name="aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  	<input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  	<input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>