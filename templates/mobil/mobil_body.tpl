<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
			
		new Ajax.Request("pengaturan_mobil.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_mobil="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode,status_terakhir){
	
	if(status_terakhir==1){
		var remark=prompt("Masukkan alasan unit kendaraan di nonaktifkan");	
		
		if (remark.length==0){
			alert("Anda belum memasukkan alasan unit kendaraan di nonaktifkan");
			return false;
		}
	}
	else{
		remark="";
	}
	
	new Ajax.Request("pengaturan_mobil.php?sid={SID}",{
	 asynchronous: true,
	 method: "get",
	 parameters: "mode=ubahstatus&kode_kendaraan="+kode+"&status="+status_terakhir+"&remark="+remark,
	 onLoading: function(request) 
	 {
	 },
	 onComplete: function(request) 
	 {
		
	 },
	 onSuccess: function(request) 
	 {			
		window.location.reload();
	},
	 onFailure: function(request) 
	 {
	 }
	});  
	
	return false;
		
}
	
</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Mobil</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;<input type="submit" value="cari" />&nbsp;
					</form>
				</td>
			</tr>
			<tr>
				<td width='30%' align='left'>
					<!-- BEGIN operasi -->
					<a href="{U_MOBIL_ADD}">[+]Tambah Mobil</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Mobil</a>
					<!-- END operasi -->
				</td>
				<td width='70%' align='right'>
					<!-- BEGIN operasi -->
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;
					<!-- END operasi -->
					{PAGING}
				</td>
			</tr>
		</table>
	 <br>
	 <table width="100%" class="border">
		 <tr>
			 <th colspan="8">Jatuh Tempo Minggu Ini</th>
		 </tr>
		 <tr>
			 <th width=30>No</th>
			 <th width=100>Kode</th>
			 <th width=100>Plat</th>
			 <th width=100>Tanggal STNK</th>
			 <th width=100>Tanggal Pajak</th>
			 <th width=100>Tanggal KIR</th>
			 <th width=100>Tanggal SIPA</th>
			 <th width=100>Tanggal KP</th>
		 </tr>
		 <!-- BEGIN ROWALERT -->
		 <tr class="even">
			 <td align="center" valign="top"><font size="4" ><b>{ROWALERT.no}</b></font></td>
			 <td align="left" valign="top"><font size="4" ><b>{ROWALERT.body}</b></font></td>
			 <td align="left" valign="top"><font size="4" ><b>{ROWALERT.kendaraan}</b></font></td>
			 {ROWALERT.stnk}
			 {ROWALERT.pajak}
			 {ROWALERT.kir}
			 {ROWALERT.sipa}
			 {ROWALERT.kp}
		 </tr>
		 <!-- END ROWALERT -->
	 </table>
	 <br>
	 <table width='100%' class="border">
    <tr>
       <th width=30></th>
       <th width=30>No</th>
			 <th width=50>Kode</th>
			 <th width=50>Plat</th>
			 <th width="100">Tgl STNK</th>
			 <th width="100">Tgl Pajak</th>
			 <th width="100">Tgl KIR</th>
		<th width="100">Tgl SIPA</th>
		<th width="100">Tgl KP</th>
			 <th width=150>Merek & Jenis</th>
			 <th width=70>Kursi</th>
			 <th width=200>Sopir</th>
			 <th width=100>Aktif</th>
			 <th width=150>Remark</th>
			 <th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="center">{ROW.check}</div></td>
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.kode}</div></td>
       <td><div align="left">{ROW.plat}</div></td>
		 <td><div align="left">{ROW.stnk}</div></td>
		 <td><div align="left">{ROW.pajak}</div></td>
		 <td><div align="left">{ROW.kir}</div></td>
		 <td><div align="left">{ROW.sipa}</div></td>
		 <td><div align="left">{ROW.kp}</div></td>
			 <td><div align="left">{ROW.merek}</div></td>
       <td><div align="right">{ROW.kapasitas}</div></td>
			 <td><div align="left">{ROW.sopir}</div></td>
       <td><div align="center">{ROW.aktif}</div></td>
			 <td><div align="left">{ROW.remark}</div></td>
       <td><div align="center">{ROW.action}</div></td>
     </tr>  
     <!-- END ROW -->
		 {NO_DATA}
    </table>
    <table width='100%'>
			<tr>
				<td width='30%' align='left'>
					<!-- BEGIN operasi -->
					<a href="{U_MOBIL_ADD}">[+]Tambah Mobil</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Mobil</a>
					<!-- END operasi -->
				</td>
				<td width='70%' align='right'>
					<!-- BEGIN operasi -->
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;
					<!-- END operasi -->
					{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>