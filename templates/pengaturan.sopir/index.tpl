<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">

    function selectAll(){

        i=1;
        loop=true;
        record_dipilih="";
        do{
            str_var='checked_'+i;

            if(chk=document.getElementById(str_var)){
                chk.checked=true;
            }
            else{
                loop=false;
            }
            i++;
        }while(loop);

    }

    function deselectAll(){

        i=1;
        loop=true;
        record_dipilih="";
        do{
            str_var='checked_'+i;
            if(chk=document.getElementById(str_var)){
                chk.checked=false;
            }
            else{
                loop=false;
            }
            i++;
        }while(loop);

    }

    function simpan(){

        var inputvalid=true;

        inputvalid=validasiInput(dlgnrp) && inputvalid;
        inputvalid=validasiInput(dlgnama) && inputvalid;
        inputvalid=validasiInput(dlgalamat) && inputvalid;
        inputvalid=validasiInput(dlgnohp) && inputvalid;
        inputvalid=validasiInput(dlgnosim) && inputvalid;
        inputvalid=validasiInput(dlgjenissim) && inputvalid;
        inputvalid=validasiInput(dlgsimexpired) && inputvalid;
        inputvalid=validasiInput(dlgkotalahir) && inputvalid;
        inputvalid=validasiInput(dlgtgllahir) && inputvalid;
        inputvalid=validasiInput(dlgnorek) && inputvalid;
        inputvalid=validasiInput(dlgnamabank) && inputvalid;
        inputvalid=validasiInput(dlgcabangbank) && inputvalid;
        inputvalid=validasiInput(dlgcabangasal,dlgselcabangasal) && inputvalid;

        if(!inputvalid) {
            return;
        }
        new Ajax.Request("pengaturan.sopir.php", {
            asynchronous: true,
            method: "post",
            parameters: "mode=1.1"+
            "&idsopir="+dlgidsopir.value+
            "&nrp="+dlgnrp.value+
            "&nama="+dlgnama.value+
            "&alamat="+dlgalamat.value+
            "&nohp="+dlgnohp.value+
            "&nosim="+dlgnosim.value+
            "&jenissim="+dlgjenissim.value+
            "&simexpired="+dlgsimexpired.value+
            "&kotalahir="+dlgkotalahir.value+
            "&tgllahir="+dlgtgllahir.value+
            "&rekeningbank="+dlgnorek.value+
            "&namabank="+dlgnamabank.value+
            "&cabangbank="+dlgcabangbank.value+
            "&outletkerja="+dlgcabangasal.value+
            "&isreguler="+dlgstatusreguler.value+
            "&isaktif="+dlgstatusaktif.value,
            onSuccess: function(request){
                if(isJson(request.responseText)){
                    var result;
                    result = JSON.parse(request.responseText);

                    if(result["status"]=="OK") {
                        formdata.submit();
                    }
                    else{
                        alert("Gagal:"+result["error"]);
                    }
                } else {
                    alert("error:"+request.responseText);
                }


            },
            onFailure: function(request){
                alert('Error');
                assignError(request.responseText);
            }
        })
    }

    function setOrder(colid){
        sortDataTable(colid,document.getElementById("formdata"));
    }

    function showDialogTambah(){
        setPopup(500,560);

        new Ajax.Updater("popupcontent","pengaturan.sopir.php",{
            asynchronous: true,
            method: "post",
            parameters: "mode=1",
            onLoading: function(request) {
            },
            onComplete: function(request) {
                toggleStatusAktif(true);
                toggleStatusReguler(true);
            },
            onSuccess: function(request) {
            },
            onFailure: function(request) {
                assignError(request.responseText);
            }
        });

        popupwrapper.show();
    }

    function showDialogUbah(idsopir){
        setPopup(500,560);

        new Ajax.Updater("popupcontent","pengaturan.sopir.php",{
            asynchronous: true,
            method: "post",
            parameters: "mode=4&idsopir="+idsopir,
            onLoading: function(request) {
                idsopir.value  = idsopir;
            },
            onComplete: function(request) {
                setComboList(document.getElementById('dlgselcabangasal'),'dlgcabangasal','pengaturan.sopir.php','mode=2',false,false,true)
                toggleStatusAktif(true);
                toggleStatusReguler(true);
            },
            onSuccess: function(request) {
            },
            onFailure: function(request) {
                assignError(request.responseText);
            }
        })

        popupwrapper.show();
    }

    function hapusData(idsopir=''){

        if(idsopir!=''){
            list_dipilih=idsopir;
        }
        else {
            i = 1;
            loop = true;
            list_dipilih = "";
            do {
                str_var = 'checked_' + i;

                if (chk = document.getElementById(str_var)) {
                    if (chk.checked) {
                        if (list_dipilih == "") {
                            list_dipilih += chk.value;
                        }
                        else {
                            list_dipilih += "," + chk.value;
                        }
                    }
                }
                else {
                    loop = false;
                }
                i++;
            } while (loop);
        }
        if(list_dipilih==""){

            alert("Anda belum memilih data yang akan dihapus!");
            return false;
        }
        if(confirm("Apakah anda yakin akan menghapus data ini?")){

            new Ajax.Request("pengaturan.sopir.php",{
                asynchronous: true,
                method: "post",
                parameters: "mode=3&idsopir="+list_dipilih,
                onLoading: function(request)
                {
                },
                onComplete: function(request)
                {

                },
                onSuccess: function(request)
                {
                    window.location.reload();
                    deselectAll();
                    alert(request.responseText);
                },
                onFailure: function(request)
                {
                }
            })
        }

        return false;

    }

    function toggleStatusAktif(inittoggle=false){

        if(!inittoggle){
            dlgstatusaktif.value  = 1 - dlgstatusaktif.value;
        }

        if(dlgstatusaktif.value==1){
            dlgbtnstatusaktif.className='crudgreen';
            dlgbtnstatusaktif.innerHTML='AKTIF';
        }else{
            dlgbtnstatusaktif.className='crud';
            dlgbtnstatusaktif.innerHTML='NON-AKTIF';
        }
    }

    function ubahStatusAktif(idsopir){

        new Ajax.Request("pengaturan.sopir.php",{
            asynchronous: true,
            method: "post",
            parameters: "mode=5&idsopir="+idsopir,
            onLoading: function(request){
            },
            onComplete: function(request) {
            },
            onSuccess: function(request) {

                if(isJson(request.responseText)) {
                    var result;
                    result = JSON.parse(request.responseText);

                    if (result["status"] == "OK") {
                        document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;
                        document.getElementById("formdata").submit();
                    }
                    else {
                        alert("Gagal:" + result["pesan"]);
                    }
                }
                else{
                    alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
                }

            },
            onFailure: function(request)
            {
            }
        });
        return false;
    }

    function toggleStatusReguler(inittoggle=false){

        if(!inittoggle){
            dlgstatusreguler.value  = 1 - dlgstatusreguler.value;
        }

        if(dlgstatusreguler.value==1){
            dlgbtnstatusreguler.className='crudgreen';
            dlgbtnstatusreguler.innerHTML='REGULER';
        }else{
            dlgbtnstatusreguler.className='crud';
            dlgbtnstatusreguler.innerHTML='BACKUP';
        }
    }

    function ubahStatusReguler(idsopir){

        new Ajax.Request("pengaturan.sopir.php",{
            asynchronous: true,
            method: "post",
            parameters: "mode=6&idsopir="+idsopir,
            onLoading: function(request){
            },
            onComplete: function(request) {
            },
            onSuccess: function(request) {

                if(isJson(request.responseText)) {
                    var result;
                    result = JSON.parse(request.responseText);

                    if (result["status"] == "OK") {
                        document.getElementById("scrollvalue").value = document.getElementById("wrappertablecontent").scrollTop;
                        document.getElementById("formdata").submit();
                    }
                    else {
                        alert("Gagal:" + result["pesan"]);
                    }
                }
                else{
                    alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
                }

            },
            onFailure: function(request)
            {
            }
        });
        return false;
    }

    function exportExcel(){
        document.getElementById("formdata").target = "_blank";
        document.getElementById("formdata").action = "{U_EXPORT_EXCEL}";
        document.getElementById("formdata").submit();
        document.getElementById("formdata").target = "";
        document.getElementById("formdata").action = "{ACTION_CARI}";
    }

    function init(e){
        popupwrapper	= dojo.widget.byId("popupcontainer");
        document.getElementById("wrappertablecontent").scrollTop={SCROLL_VALUE};

    }

    dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">
  <input type="hidden" id="scrollvalue" name="scrollvalue" value="0">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">{JUDUL_HALAMAN}</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Cari <input type="text" id="cari" name="cari" value="{CARI}"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
    <!-- BEGIN CRUD_ADD -->
    <a href="" onclick="showDialogTambah();return false;" class="crud">tambah</a>
    <!-- END CRUD_ADD -->
    <!-- BEGIN CRUD_DEL -->
    <a href="" onClick="return hapusData();" class="crud">hapus</a>
    <!-- END CRUD_DEL -->
    <!-- BEGIN EXPORT -->
    <a href="" onClick="exportExcel();return false;" class="crud">Export ke MS-Excel</a>
    <!-- END EXPORT -->
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
      {PAGING}
  </div>
</form>

<br>
<hr>
<div class="tabletitle">
    {JUDUL_TABEL}<br>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th rowspan="2"><input type="checkbox" onclick="if(this.checked){selectAll()}else{deselectAll()};"/></th>
      <th rowspan="2"></th>
      <th rowspan="2">No</th>
      <th onclick="setOrder(0)" align="left" rowspan="2">Nama</th>
      <th onclick="setOrder(1)" rowspan="2">NRP</th>
      <th onclick="setOrder(2)" rowspan="2">No.HP</th>
      <th onclick="setOrder(3)" rowspan="2">Alamat</th>
      <th onclick="setOrder(4)" rowspan="2">No.SIM</th>
      <th onclick="setOrder(5)" rowspan="2">Jenis SIM</th>
      <th onclick="setOrder(6)" rowspan="2">SIM Expired</th>
      <th onclick="setOrder(7)" rowspan="2">Kota Lahir</th>
      <th onclick="setOrder(8)" rowspan="2">Tgl Lahir</th>
      <th colspan="3">Bank</th>
      <th onclick="setOrder(12)" rowspan="2">Outlet Kerja</th>
      <th onclick="setOrder(13)" rowspan="2">Status</th>
      <th onclick="setOrder(14)" rowspan="2">Aktif</th>
    </tr>
    <tr>
      <th onclick="setOrder(9)" >Rekening</th>
      <th onclick="setOrder(10)" >Nama Bank</th>
      <th onclick="setOrder(11)" >Cabang</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center"><input type="checkbox" id="checked_{ROW.idx}" name="checked_{ROW.idsopir}" value="{ROW.idsopir}"/></td>
        <td align="center">
          <!-- BEGIN ACT_EDIT -->
          <span class="b_edit" onclick="showDialogUbah('{ROW.idsopir}');" title="ubah data" ></span>&nbsp;
          <!-- END ACT_EDIT -->

          <!-- BEGIN ACT_DEL -->
          <span class="b_delete" onclick="hapusData('{ROW.idsopir}');" title="hapus data" ></span>
          <!-- END ACT_DEL -->
        </td>
        <td align="center">{ROW.no}</td>
        <td align="center">{ROW.nama}</td>
        <td align="center">{ROW.nrp}</td>
        <td align="center">{ROW.nohp}</td>
        <td align="center">{ROW.alamat}</td>
        <td align="center">{ROW.nosim}</td>
        <td align="center">{ROW.jenissim}</td>
        <td align="center">{ROW.simexpired}</td>
        <td align="center">{ROW.kotalahir}</td>
        <td align="center">{ROW.tgllahir}</td>
        <td align="center">{ROW.rekeningbank}</td>
        <td align="center">{ROW.namabank}</td>
        <td align="center">{ROW.cabangbank}</td>
        <td align="center">{ROW.outletkerja}</td>
        <td align="center">
          <a href="" onclick="ubahStatusReguler('{ROW.idsopir}');return false;" class="{ROW.classcrudreguler}">&nbsp;{ROW.reguler}&nbsp;</a>
        </td>
        <td align="center">
          <a href="" onclick="ubahStatusAktif('{ROW.idsopir}');return false;" class="{ROW.classcrudaktif}">&nbsp;{ROW.aktif}&nbsp;</a>
        </td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
    setClassArrowSort(orderby.value*1+3,sort.value,document.getElementById("tableheader"))
    setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>