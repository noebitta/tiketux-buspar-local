<span class="popuptitle">{JUDUL}</span>
<center>
  <div class="wrapperpopup">
    <input type="hidden" id="dlgidsopir" value="{ID_SOPIR}"/>
    <input type="hidden" id="dlgcabangasal" value="{CABANG_ASAL}"/>
    <input type="hidden" id="dlgstatusreguler" value="{STATUS_REGULER}"/>
    <input type="hidden" id="dlgstatusaktif" value="{STATUS_AKTIF}"/>
    <span class="popuplabel">NRP</span><span class="popupinput"><input type="text" id="dlgnrp" name="dlgnrp" value="{NRP}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);" style="text-transform: uppercase;" maxlength="10" /></span><br><br>
    <span class="popuplabel">Nama</span><span class="popupinput"><input type="text" id="dlgnama" name="dlgnama" value="{NAMA}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);" style="text-transform: uppercase;" maxlength="25" /></span><br><br>
    <span class="popuplabel">Alamat</span><span class="popupinput"><input type="text" id="dlgalamat" name="dlgalamat" value="{ALAMAT}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);" style="text-transform: uppercase;" /></span><br><br>
    <span class="popuplabel">NO. HP</span><span class="popupinput"><input type="text" id="dlgnohp" name="dlgnohp" value="{NO_HP}" onfocus="this.style.background='white';" onkeypress="validasiAngka(event);" maxlength="12" /></span><br><br>
    <span class="popuplabel">No. SIM</span><span class="popupinput"><input type="text" id="dlgnosim" name="dlgnosim" value="{NO_SIM}" onfocus="this.style.background='white';" onkeypress="validasiAngka(event);" maxlength="15" /></span><br><br>
    <span class="popuplabel">Jenis SIM</span><span class="popupinput"><input type="text" id="dlgjenissim" name="dlgjenissim" value="{JENIS_SIM}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);" style="text-transform: uppercase;" maxlength="2" /></span><br><br>
    <span class="popuplabel">SIM Expired</span><span class="popupinput"><input readonly="yes" class="drop_calendar" id="dlgsimexpired" name="dlgsimexpired" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{SIM_EXPIRED}"></span><br><br>
    <span class="popuplabel">Kota Lahir</span><span class="popupinput"><input type="text" id="dlgkotalahir" name="dlgkotalahir" value="{KOTA_LAHIR}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);" style="text-transform: uppercase;" maxlength="15" /></span><br><br>
    <span class="popuplabel">Tanggal Lahir</span><span class="popupinput"><input readonly="yes" class="drop_calendar" id="dlgtgllahir" name="dlgtgllahir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_LAHIR}"></span><br><br>
    <span class="popuplabel">Rekening Bank</span><span class="popupinput"><input type="text" id="dlgnorek" name="dlgnorek" value="{NO_REK}" onfocus="this.style.background='white';" onkeypress="validasiAngka(event);" maxlength="15" /></span><br><br>
    <span class="popuplabel">Nama Bank</span><span class="popupinput"><input type="text" id="dlgnamabank" name="dlgnamabank" value="{NAMA_BANK}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);" style="text-transform: uppercase;" maxlength="5" /></span><br><br>
    <span class="popuplabel">Cabang Bank</span><span class="popupinput"><input type="text" id="dlgcabangbank" name="dlgcabangbank" value="{CABANG_BANK}" onfocus="this.style.background='white';" onkeypress="validCharInput(event);"style="text-transform: uppercase;" maxlength="15" /></span><br><br>
    <span class="popuplabel">Outlet Kerja</span><span class="popupinput"><span id="dlgselcabangasal" class="combolistselection" onClick="setComboList(this,'dlgcabangasal','pengaturan.sopir.php','mode=2',false,true,true);">&nbsp;</span></span><br><br>
      <span class="popuplabel">Status Reguler</span><span class="popupinput"><a href="" id="dlgbtnstatusreguler" onclick="toggleStatusReguler();return false;">&nbsp;BACKUP&nbsp;</a></span></span><br><br>
    <span class="popuplabel">Status Aktif</span><span class="popupinput"><a href="" id="dlgbtnstatusaktif" onclick="toggleStatusAktif();return false;">&nbsp;AKTIF&nbsp;</a></span></span><br><br>
    <span class="flatbutton" onclick="simpan();" id="buttonproses">SIMPAN</span>
  </div>
</center>