<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('kode_invalid');
	Element.hide('harga_tiket_invalid');
	Element.hide('harga_tiket_tuslah_invalid');
	Element.hide('biaya_sopir_invalid');
	Element.hide('biaya_tol_invalid');
	Element.hide('biaya_parkir_invalid');
	Element.hide('biaya_bbm_invalid');
	Element.hide('komisi_penumpang_sopir_invalid');
	Element.hide('komisi_penumpang_cso_invalid');
	Element.hide('komisi_paket_sopir_invalid');
	Element.hide('komisi_paket_cso_invalid');
	
	kode									= document.getElementById('kode');
	harga_tiket						= document.getElementById('harga_tiket');
	harga_tiket_tuslah		= document.getElementById('harga_tiket_tuslah');
	biaya_sopir						= document.getElementById('biaya_sopir');
	biaya_tol							= document.getElementById('biaya_tol');
	biaya_parkir					= document.getElementById('biaya_parkir');
	biaya_bbm							= document.getElementById('biaya_bbm');
	komisi_penumpang_sopir= document.getElementById('komisi_penumpang_sopir');
	komisi_penumpang_cso	= document.getElementById('komisi_penumpang_cso');
	komisi_paket_sopir		= document.getElementById('komisi_paket_sopir');
	komisi_paket_cso			= document.getElementById('komisi_paket_cso');
		
	if(kode.value==''){
		valid=false;
		Element.show('kode_invalid');
	}
	
	if(!cekValue(harga_tiket.value) || harga_tiket.value==''){	
		valid=false;
		Element.show('harga_tiket_invalid');
	}
	
	if(!cekValue(harga_tiket_tuslah.value) || harga_tiket_tuslah.value==''){	
		valid=false;
		Element.show('harga_tiket_tuslah_invalid');
	}
	
	if(!cekValue(biaya_sopir.value)){	
		valid=false;
		Element.show('biaya_sopir_invalid');
	}
	
	if(!cekValue(biaya_tol.value)){	
		valid=false;
		Element.show('biaya_tol_invalid');
	}
	
	if(!cekValue(biaya_parkir.value)){	
		valid=false;
		Element.show('biaya_parkir_invalid');
	}
	
	if(!cekValue(biaya_bbm.value)){	
		valid=false;
		Element.show('biaya_bbm_invalid');
	}
	
	if(!cekValue(komisi_penumpang_sopir.value)){	
		valid=false;
		Element.show('komisi_penumpang_sopir_invalid');
	}
	
	if(!cekValue(komisi_penumpang_cso.value)){	
		valid=false;
		Element.show('komisi_penumpang_cso_invalid');
	}
	
	if(!cekValue(komisi_paket_sopir.value)){	
		valid=false;
		Element.show('komisi_paket_sopir_invalid');
	}
	
	if(!cekValue(komisi_paket_cso.value)){	
		valid=false;
		Element.show('komisi_paket_cso_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

function lockUnlockHarga(nilai){
	if(nilai==0 || nilai==2){
		document.getElementById("harga_tiket").disabled=false;
		document.getElementById("harga_tiket_tuslah").disabled=false;
	}
	else{
		document.getElementById("harga_tiket").disabled=true;
		document.getElementById("harga_tiket").value=0;
		document.getElementById("harga_tiket_tuslah").disabled=true;
		document.getElementById("harga_tiket_tuslah").value=0;
	}
}

</script>

<form name="frm_data_jurusan" action="{U_JURUSAN_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Jurusan</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='500'>
				<table width='500'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <input type="hidden" name="id_jurusan" value="{ID_JURUSAN}">
			      <input type="hidden" name="kode_jurusan_old" value="{KODE_JURUSAN_OLD}">
						<td width='200'><u>Kode Jurusan</u></td><td width='5'>:</td>
						<td width='300'>
							<input type="text" id="kode" name="kode" value="{KODE_JURUSAN}" maxlength=10 onChange="Element.hide('kode_invalid');">
							<span id='kode_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
						<td>Asal</td><td>:</td>
						<td>
							<select id='asal' name='asal'>
								{OPT_ASAL}
							</select>
						</td>
					</tr>
					<tr>
						<td>Tujuan</td><td>:</td>
						<td>
							<select id='tujuan' name='tujuan'>
								{OPT_TUJUAN}
							</select>
						</td>
					</tr>
					<tr>
			      <td><u>Harga Tiket</u></td><td>:</td>
						<td>
							<input type="text" id="harga_tiket" name="harga_tiket" value="{HARGA_TIKET}" maxlength=8 onChange="Element.hide('harga_tiket_invalid');">
							<span id='harga_tiket_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td><u>Harga Tiket Tuslah</u></td><td>:</td>
						<td>
							<input type="text" id="harga_tiket_tuslah" name="harga_tiket_tuslah" value="{HARGA_TIKET_TUSLAH}" maxlength=8 onChange="Element.hide('harga_tiket_tuslah_invalid');">
							<span id='harga_tiket_tuslah_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Status Aktif</td><td>:</td>
						<td>
							<select id="flag_aktif" name="flag_aktif">
								<option value=1 {AKTIF_1}>AKTIF</option>
								<option value=0 {AKTIF_0}>TIDAK AKTIF</option>
							</select>
						</td>
			    </tr> 
					<tr>
			      <td>Jenis Jurusan</td><td>:</td>
						<td>
							<select id="flag_jenis" name="flag_jenis">
								<option value=1 {JENIS_1}>LUAR KOTA</option>
								<option value=0 {JENIS_0}>DALAM KOTA</option>
							</select>
						</td>
			    </tr>
					<tr>
			      <td>Operasional untuk</td><td>:</td>
						<td>
							<select id="flag_op_jurusan" name="flag_op_jurusan" onChange="lockUnlockHarga(this.value);">
								<option value=0 {OP_0}>Travel</option>
								<option value=1 {OP_1}>Paket</option>
								<option value=2 {OP_2}>Travel & Paket</option>
							</select>
						</td>
			    </tr> 					
					<tr><td colspan='3'><hr></td></tr>
					<tr><td colspan=3><h3>Biaya-biaya:</h3></td></tr>
					<tr>
			      <td>Biaya Sopir</td><td>:</td>
						<td>
							<input type="text" id="biaya_sopir" name="biaya_sopir" value="{BIAYA_SOPIR}" maxlength=8 onChange="Element.hide('biaya_sopir_invalid');">
							<span id='biaya_sopir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
							<input type='checkbox' id='flag_biaya_sopir_kumulatif' name='flag_biaya_sopir_kumulatif' {FLAG_BIAYA_SOPIR_KUMULATIF} /><span class='noticeMessage'>Dibayar kumulatif</span>
						</td>
			    </tr>
					<tr>
			      <td>Biaya Tol</td><td>:</td>
						<td>
							<input type="text" id="biaya_tol" name="biaya_tol" value="{BIAYA_TOL}" maxlength=8 onChange="Element.hide('biaya_tol_invalid');">
							<span id='biaya_tol_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Biaya Parkir</td><td>:</td>
						<td>
							<input type="text" id="biaya_parkir" name="biaya_parkir" value="{BIAYA_PARKIR}" maxlength=8 onChange="Element.hide('biaya_parkir_invalid');">
							<span id='biaya_parkir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Biaya BBM</td><td>:</td>
						<td>
							<input type="text" id="biaya_bbm" name="biaya_bbm" value="{BIAYA_BBM}" maxlength=8 onChange="Element.hide('biaya_bbm_invalid');">
							<span id='biaya_bbm_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
							<input type='checkbox' id='flag_biaya_voucher_bbm' name='flag_biaya_voucher_bbm' {FLAG_BIAYA_VOUCHER_BBM} /><span class='noticeMessage'>Voucher BBM</span>
						</td>
			    </tr>
					<!-- BEGIN LISTBBM -->
					<tr>
						<td>Layout {LISTBBM.idlayout}</td><td>:</td>
						<td>
							<input type="text" id="biayabbm{LISTBBM.idlayout}" name="biayabbm{LISTBBM.idlayout}" value="{LISTBBM.literbbm}" maxlength="10" size="5" onkeypress='validasiAngka(event);'style="text-align: right;"> Liter
							<input type="text" id="biayabbmext{LISTBBM.idlayout}" name="biayabbmext{LISTBBM.idlayout}" value="{LISTBBM.literbbm2}" maxlength="10" size="5" onkeypress='validasiAngka(event);'style="text-align: right;"> Liter
						</td>
					</tr>
					<!-- END LISTBBM -->
					<tr><td colspan='3'><hr></td></tr>
					<tr>
						<td>Lokasi Pengisian BBM</td><td>:</td>
						<td>
							<select id="spbu" name="spbu">
								<option value=1 {SPBU_1}>SPBU 57</option>
								<option value=2 {SPBU_2}>SPBU 72</option>
							</select>
						</td>
					</tr>
					<!--<tr><td colspan='3'><hr></td></tr>
					<tr><td colspan=3><h3>Komisi-komisi:</h3></td></tr>
					<tr>
			      <td>Komisi Sopir/penumpang</td><td>:</td>
						<td>
							<input type="text" id="komisi_penumpang_sopir" name="komisi_penumpang_sopir" value="{KOMISI_PENUMPANG_SOPIR}" maxlength=8 onChange="Element.hide('komisi_penumpang_sopir_invalid');">
							<span id='komisi_penumpang_sopir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Komisi CSO/penumpang</td><td>:</td>
						<td>
							<input type="text" id="komisi_penumpang_cso" name="komisi_penumpang_cso" value="{KOMISI_PENUMPANG_CSO}" maxlength=8 onChange="Element.hide('komisi_penumpang_cso_invalid');">
							<span id='komisi_penumpang_cso_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Komisi Sopir/Paket</td><td>:</td>
						<td>
							<input type="text" id="komisi_paket_sopir" name="komisi_paket_sopir" value="{KOMISI_PAKET_SOPIR}" maxlength=8 onChange="Element.hide('komisi_paket_sopir_invalid');">
							<span id='komisi_paket_sopir_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Komisi CSO/Paket</td><td>:</td>
						<td>
							<input type="text" id="komisi_paket_cso" name="komisi_paket_cso" value="{KOMISI_PAKET_CSO}" maxlength=8 onChange="Element.hide('komisi_paket_cso_invalid');">
							<span id='komisi_paket_cso_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>-->
				</table>
			</td>
			<td width=1 bgcolor='D0D0D0'></td>
			<td align='center' valign='top' width='500'>
				<table width='500'>
					<tr><td colspan=3><h3>Harga Kirim Paket:</h3></td></tr>
					<tr>
			      <td valign='top'>Dokumen</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_1_kilo_pertama" name="harga_paket_1_kilo_pertama" value="{HARGA_PAKET_1_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
							Rp.&nbsp;<input type="text" id="harga_paket_1_kilo_berikut" name="harga_paket_1_kilo_berikut" value="{HARGA_PAKET_1_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Xtra Small</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_2_kilo_pertama" name="harga_paket_2_kilo_pertama" value="{HARGA_PAKET_2_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
							Rp.&nbsp;<input type="text" id="harga_paket_2_kilo_berikut" name="harga_paket_2_kilo_berikut" value="{HARGA_PAKET_2_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Small</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_3_kilo_pertama" name="harga_paket_3_kilo_pertama" value="{HARGA_PAKET_3_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
							Rp.&nbsp;<input type="text" id="harga_paket_3_kilo_berikut" name="harga_paket_3_kilo_berikut" value="{HARGA_PAKET_3_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Medium</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_4_kilo_pertama" name="harga_paket_4_kilo_pertama" value="{HARGA_PAKET_4_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
							Rp.&nbsp;<input type="text" id="harga_paket_4_kilo_berikut" name="harga_paket_4_kilo_berikut" value="{HARGA_PAKET_4_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Large</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_5_kilo_pertama" name="harga_paket_5_kilo_pertama" value="{HARGA_PAKET_5_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
							Rp.&nbsp;<input type="text" id="harga_paket_5_kilo_berikut" name="harga_paket_5_kilo_berikut" value="{HARGA_PAKET_5_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya<hr>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Paket Xtra Large</td><td  valign='top'>:</td>
						<td valign='top'>
							Rp.&nbsp;<input type="text" id="harga_paket_6_kilo_pertama" name="harga_paket_6_kilo_pertama" value="{HARGA_PAKET_6_KILO_PERTAMA}" maxlength='8' size='12'>/Kg pertama<br>
							Rp.&nbsp;<input type="text" id="harga_paket_6_kilo_berikut" name="harga_paket_6_kilo_berikut" value="{HARGA_PAKET_6_KILO_BERIKUT}" maxlength='8' size='12'>/Kg berikutnya
						</td>
			    </tr>
					<!--<tr><td colspan=3><br><h3>Kode-kode Akun:</h3></td></tr>
					<tr>
						<td width='200'>Kode Akun Pendapatan Penumpang</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_pendapatan_penumpang" name="kode_akun_pendapatan_penumpang" value="{KODE_AKUN_PENDAPATAN_PENUMPANG}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Pendapatan Paket</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_pendapatan_paket" name="kode_akun_pendapatan_paket" value="{KODE_AKUN_PENDAPATAN_PAKET}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Biaya Sopir</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_sopir" name="kode_akun_biaya_sopir" value="{KODE_AKUN_BIAYA_SOPIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Biaya Tol</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_tol" name="kode_akun_biaya_tol" value="{KODE_AKUN_BIAYA_TOL}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='300'>Kode Akun Biaya Parkir</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_parkir" name="kode_akun_biaya_parkir" value="{KODE_AKUN_BIAYA_PARKIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='300'>Kode Akun Biaya BBM</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_biaya_bbm" name="kode_akun_biaya_bbm" value="{KODE_AKUN_BIAYA_BBM}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi Sopir/Penumpang</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_penumpang_sopir" name="kode_akun_komisi_penumpang_sopir" value="{KODE_AKUN_KOMISI_PENUMPANG_SOPIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi CSO/Penumpang</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_penumpang_cso" name="kode_akun_komisi_penumpang_cso" value="{KODE_AKUN_KOMISI_PENUMPANG_CSO}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi Sopir/Paket</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_paket_sopir" name="kode_akun_komisi_paket_sopir" value="{KODE_AKUN_KOMISI_PAKET_SOPIR}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Komisi CSO/Paket</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_komisi_paket_cso" name="kode_akun_komisi_paket_cso" value="{KODE_AKUN_KOMISI_PAKET_CSO}" maxlength=20>
						</td>
					</tr>
					<tr>
						<td width='200'>Kode Akun Charge</td><td width='5'>:</td>
						<td>
							<input type="text" id="kode_akun_charge" name="kode_akun_charge" value="{KODE_AKUN_CHARGE}" maxlength=20>
						</td>
					</tr>-->
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
				<input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
				<input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>           
	</table>
	</td>
</tr>
</table>
</form>