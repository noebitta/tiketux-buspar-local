<script type="text/javascript">
    filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>

<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="whiter" valign="middle" align="left">
                <!--HEADER-->
                <table width='100%' cellspacing="0">
                    <tr height=40 class='banner'>
                        <td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Rekap Route</td>
                        <td class="bannernormal" align="right">
                           <form action="{URL}" method="post">
                               Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
                               &nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
                               <input type="submit" value="cari" />
                           </form>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
                        </td>
                    </tr>
                </table>
        </td>
    </tr>
    <tr>
        <td>
            <br>
            <table width="100%" class="border">
                <tbody>
                <tr>
                    <th rowspan="2" width="20">No</th>
                    <th rowspan="2" width="200">COUNTER</th>
                    <th colspan="4">ROUTE</th>
                    <th colspan="4">RINCIAN</th>
                </tr>
                <tr>
                    <th>Jakarta</th>
                    <th colspan="2" >Bandung</th>
                    <th>SHORT NAME</th>
                    <th>Pax</th>
                    <th>Paket</th>
                    <th>Trip</th>
                    <th>Unit</th>
                </tr>
                <!-- BEGIN ROW -->
                <tr class="{ROW.odd}">
                    <td>{ROW.no}</td>
                    <td><b>{ROW.counter}</b></td>
                    <td>{ROW.jakarta}</td>
                    <td>{ROW.bandung}</td>
                    <td>{ROW.pasteur}</td>
                    <td>{ROW.shortname}</td>
                    <td><div align="right"><b>{ROW.pax}</b></div></td>
                    <td><div align="right"><b>{ROW.paket}</b></div></td>
                    <td><div align="right"><b>{ROW.trip}</b></div></td>
                    <td><div align="right"><b>{ROW.unit}</b></div></td>
                </tr>
                <!-- END ROW -->
                <tr style="background-color: rosybrown">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><div align="right"><b>{SUM_PAX}</b></div></td>
                    <td><div align="right"><b>{SUM_PAKET}</b></div></td>
                    <td><div align="right"><b>{SUM_TRIP}</b></div></td>
                    <td><div align="right"><b>{SUM_UNIT}</b></div></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>