<style type="text/css">
  .headertitle{
    display: inline-block;
    font-weight: bold;
    width: 120px;
  }
  
  .headerfield{
    display: inline-block;
    width: 280px;
  }
</style>

<input type='hidden' value='{JAM_BERANGKAT}' id='jam_berangkat_aktif' />
<input type='hidden' readonly='yes' value='{NO_SPJ}' name='txt_spj' id='txt_spj' />
<input type='hidden' readonly='yes' value='{NO_POLISI}' name='plat' id='plat' />
<input type='hidden' value='{NO_UNIT}' name='hide_mobil_sekarang' id='hide_mobil_sekarang'></input>
<input type='hidden' value='{JUMLAH_KURSI}' name='hide_layout_kursi' id='hide_layout_kursi'></input>
<input type='hidden' name='hide_nama_sopir_sekarang' id='hide_nama_sopir_sekarang' value='{KODE_SOPIR}'>

<div style="font-size: 13px; width:95%;height:90px; text-align: left;padding-left: 10px;">
  <span class="headertitle">#Manifest</span><span class="headerfield">:{NO_SPJ}</span><br>
  <span class="headertitle">#Kendaraan</span><span class="headerfield">:{DATA_UNIT}</span><br>
  <span class="headertitle">Harga</span><span class="headerfield">:Rp.{HARGA_TIKET}</span><br>
  <span class="headertitle">Daftar Tunggu</span><span class="headerfield">:<a style="color: red;font-size: 12px;" href="" onClick="{ACTION_DAFTAR_TUNGGU};return false;">{DAFTAR_TUNGGU} Orang</a></span><br>
  <span style="float:right;padding-right: 10px;"><input type='button' value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Refresh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' onclick='getUpdateMobil();' /></span>
</div>
<div style="font-size: 13px; text-align: center;width:400px;">
  <span style="display: inline-block;height: 20px;width: 400px;background: #d0d0d0;border-radius: 20px 20px 0px 0px;text-align: left;">
    <span style="color:#606060;font-weight: bold;font-size: 15px;padding-left: 150px;">Layout Kursi</span>
    <img id='progress_kursi' style="display:none;" src='./templates/images/loading.gif' />
  </span><br>
  <span>{LAYOUT_KURSI}</span>
  <span style="display: inline-block;height: 10px;width: 400px;background: #d0d0d0;border-radius: 0px 0px 12px 12px;"></span>
</div><br>