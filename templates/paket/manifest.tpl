<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script>
function printWindow() {
	bV = parseInt(navigator.appVersion);
	if (bV >= 4) window.print();
}
</script>

<body class="tiket">
  <div style="width: 400px;text-align: center;">
    <span style="font-size: 18px">{NAMA_PERUSAHAAN}</span><br>
    <span style="font-size: 12px">{ALAMAT_PERUSAHAAN}</span><br>
    <span style="font-size: 12px">{TELP_PERUSAHAAN}</span><br>
    <span style="font-size: 16px">-----------------------------</span><br>
    <span style="font-size: 16px">MANIFEST PAKET</span><br>
    <!-- BEGIN IS_COPY -->
    <span style="font-size: 14px;">COPY COPY COPY COPY COPY COPY</span><br>
    <!-- END IS_COPY -->
    <img src="{QR_CODE}"><br>
    <span style="font-size: 18px">{NO_MANIFEST}</span><br><br>
    <span style="font-size: 16px">-----KEBERANGKATAN------</span><br>
    <span class="resilabel">Unit:</span>&nbsp;<span class="residatafield">{KODE_UNIT} ({NO_PLAT})</span>
    <span class="resilabel">Driver:</span>&nbsp;<span class="residatafield">{DRIVER} ({KODE_DRIVER})</span>
    <span class="resilabel">Tgl:</span>&nbsp;<span class="residatafield">{TGL_BERANGKAT}</span>
    <span class="resilabel">Asal:</span>&nbsp;<span class="residatafield">{ASAL}</span>
    <span class="resilabel">Tujuan:</span>&nbsp;<span class="residatafield">{TUJUAN}</span>
    <span class="resilabel">Jam:</span>&nbsp;<span class="residatafield">{JAM_BERANGKAT}</span>
    <span class="resilabel">Jadwal:</span>&nbsp;<span class="residatafield">{KODE_JADWAL}</span>
    <br><br>
    <span style="font-size: 16px">-------DAFTAR BARANG---------</span><br>
    <!-- BEGIN LIST_BARANG -->
    <!-- BEGIN TRANSIT -->
    <span style="font-size: 16px">##TRANSIT KE {LIST_BARANG.TRANSIT.tujuanakhir}##</span><br>
    <!-- END TRANSIT -->
    <span class="resilabel">Resi:</span>&nbsp;<span class="residatafield">{LIST_BARANG.noresi}</span>
    <span class="resilabel">Layanan:</span>&nbsp;<span class="residatafield">{LIST_BARANG.layanan}</span>
    <span class="resilabel">Pengirim:</span>&nbsp;<span class="residatafield">{LIST_BARANG.pengirim}</span>
    <span class="resilabel">Penerima:</span>&nbsp;<span class="residatafield">{LIST_BARANG.penerima}</span>
    <span style="font-size: 16px">-----------------------------</span><br>
    <!-- END LIST_BARANG -->
    <span class="resilabel">Waktu Cetak:</span>&nbsp;<span class="residatafield">{WAKTU_CETAK}</span>
    <span class="resilabel">oleh:</span>&nbsp;<span class="residatafield">{PETUGAS}</span>
    <!-- BEGIN PENCETAK_ULANG -->
    <br>
    <span style="font-size: 16px">------------------------</span><br>
    <span style="font-size: 16px">Telah dicetak ulang</span><br>
    <span class="resilabel">Waktu Cetak Ulang:</span>&nbsp;<span class="residatafield">{PENCETAK_ULANG.waktucetak}</span>
    <span class="resilabel">oleh:</span>&nbsp;<span class="residatafield">{PENCETAK_ULANG.petugas}</span>
    <!-- END PENCETAK_ULANG -->
    <br><br>
    <span style="font-size: 16px">------------potong-------------</span><br>
    <span style="font-size: 16px">BUKTI SERAH TERIMA BARANG</span><br><br>
    <span style="font-size: 18px">{NO_MANIFEST}</span><br><br>
    <span style="font-size: 16px">-----KEBERANGKATAN------</span><br>
    <span class="resilabel">Unit:</span>&nbsp;<span class="residatafield">{KODE_UNIT} ({NO_PLAT})</span>
    <span class="resilabel">Driver:</span>&nbsp;<span class="residatafield">{DRIVER} ({KODE_DRIVER})</span>
    <span class="resilabel">Tgl:</span>&nbsp;<span class="residatafield">{TGL_BERANGKAT}</span>
    <span class="resilabel">Asal:</span>&nbsp;<span class="residatafield">{ASAL}</span>
    <span class="resilabel">Tujuan:</span>&nbsp;<span class="residatafield">{TUJUAN}</span>
    <span class="resilabel">Jam:</span>&nbsp;<span class="residatafield">{JAM_BERANGKAT}</span>
    <span class="resilabel">Jadwal:</span>&nbsp;<span class="residatafield">{KODE_JADWAL}</span>
    <br><br>
    <span style="font-size: 16px">-------DAFTAR BARANG---------</span><br>
    <!-- BEGIN LIST_BARANG -->
    <!-- BEGIN TRANSIT -->
    <span style="font-size: 16px">##TRANSIT KE {LIST_BARANG.TRANSIT.tujuanakhir}##</span><br>
    <!-- END TRANSIT -->
    <span class="resilabel">Resi:</span>&nbsp;<span class="residatafield">{LIST_BARANG.noresi}</span>
    <span class="resilabel">Layanan:</span>&nbsp;<span class="residatafield">{LIST_BARANG.layanan}</span>
    <span class="resilabel">Pengirim:</span>&nbsp;<span class="residatafield">{LIST_BARANG.pengirim}</span>
    <span class="resilabel">Penerima:</span>&nbsp;<span class="residatafield">{LIST_BARANG.penerima}</span>
    <span style="font-size: 16px">-----------------------------</span><br>
    <!-- END LIST_BARANG -->
    <span style="font-size: 14px">saya driver yang bertandatangan dibawah ini menyatakan telah menerima penyerahan barang dari "{PETUGAS}" dan bertanggung jawab terhadap barang-barang yang telah saya terima</span><br>
    <span style="font-size: 14px">Tanda tangan<br><br><br><br><br><br><br><br></span><br>
    <span style="font-size: 12px">{DRIVER} ({KODE_DRIVER})</span><br>
  </div>

  <script language="javascript">
    //printWindow();
    //window.close();
  </script>

</body>
</html>