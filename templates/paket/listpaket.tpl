<div class="resvsectioncard">
  <span class="resvsectiontitle">Data Keberangkatan</span>
  <div class="resvsectioncontent">
    <span class="resvisidatalabel">MANIFEST PAKET</span><span class="resvdisidatafield">:{NO_MANIFEST_PAKET}</span>
    <span class="resvisidatalabel">MANIFEST INDUK</span><span class="resvdisidatafield">:{NO_MANIFEST_INDUK}</span>
    <span class="resvisidatalabel">UNIT</span><span class="resvdisidatafield">:{NO_UNIT} ({NO_PLAT})</span>
    <span class="resvisidatalabel">DRIVER</span><span class="resvdisidatafield">:{DRIVER} {NIK}</span>
    <span class="resvisidatalabel">TGL.BERANGKAT</span><span class="resvdisidatafield">:<b>{TGL_BERANGKAT}</b></span>
    <span class="resvisidatalabel">JADWAL</span><span class="resvdisidatafield">:<b>{KODE_JADWAL}</b></span>
    <!-- BEGIN ALERT_JADWALOFF -->
    <br><br><span style="text-transform: uppercase;font-size: 16px;color: red;">{ALERT_JADWALOFF.pesan}</span><br><br>
    <!-- END ALERT_JADWALOFF -->
    <!-- BEGIN BTN_MANIFEST -->
    <span class="flatbutton" onClick="{BTN_MANIFEST.action}"  style="padding-top: 3px;width: 150px;height: 20px;margin-top:10px;margin-bottom: 10px;">CETAK MANIFEST</span>
    <!-- END BTN_MANIFEST -->
    <!-- BEGIN BTN_MUTASI -->
    <span class="flatbutton" onClick="prosesMutasiPaket();"  style="padding-top: 3px;width: 150px;height: 20px;margin-top:10px;margin-bottom: 10px;">MUTASIKAN KE SINI</span>
    <!-- END BTN_MUTASI -->
  </div>
</div>
<!-- BEGIN SHOW_HEADER_LAYOUT -->
<input type="hidden" value='OK' id="jadwalok"/>
<div class="resvsectioncard">
  <span class="resvsectiontitle">Daftar Paket</span>
  <div class="resvsectioncontent">
    Total Paket: {TOTAL_PAKET} | Total Omzet: Rp.{OMZET_PAKET}
  </div>
</div>
<!-- END SHOW_HEADER_LAYOUT -->
<div id="resvpaketcontainerbarang">
  <!-- BEGIN DAFTAR_BARANG -->
  <div class="resvsectioncardlistpaket" onclick="showDetailPaket('{DAFTAR_BARANG.resi}')" style="cursor: pointer; width: 98%;padding-bottom: 10px;">
    <span class="resvsectiontitle">{DAFTAR_BARANG.resi}</span>
    <div class="resvsectioncontent" style="text-align: left;">
      <h3 style="float:left;font-size: 20px;padding-left: 5px;">{DAFTAR_BARANG.no}</h3>
      <div style='color:blue;font-size:12px;font-weight:bold;float:right;text-transform:uppercase;padding-right:10px;'>{DAFTAR_BARANG.poinberangkat}</div>
      <div class="resvcargolisttitle">{DAFTAR_BARANG.jenislayanan}</div>
      <div style='color:red;font-size:12px;font-weight:bold;float:right;text-transform:uppercase;padding:10px;'>{DAFTAR_BARANG.transit}</div>
      <div class="resvcargolistdata">Pengirim: {DAFTAR_BARANG.nama_pengirim}</div>
      <div class="resvcargolistdata">Penerima: {DAFTAR_BARANG.nama_penerima}</div>
      <div class="resvcargolistpax"><span style="display: inline-block;color:green;">Berat: {DAFTAR_BARANG.berat} Kg </span>{DAFTAR_BARANG.is_dijemput} {DAFTAR_BARANG.is_diantar}</div>
      <div style="display: inline-block;width:100%;">
        <span style="display: inline-block;float: left;color: green;font-size:12px;font-weight:bold;padding-left: 30px;text-transform: uppercase;">{DAFTAR_BARANG.cara_bayar}</span>
        <span style="font-size:11px;font-weight:bold;text-align:right;padding-right: 10px;float: right;display: inline-block;color: #707070">Harga Paket Rp. {DAFTAR_BARANG.harga}</span><br>
        <div style="font-size:11px;font-weight:bold;text-align:right;padding-right: 10px;float: right;display: inline-block;color: #707070">Diskon Rp. {DAFTAR_BARANG.diskon}</div><br>
        <div style="color:green;font-size:12px;font-weight:bold;text-align:right;padding-right: 10px;float: right;display: inline-block;">Biaya Rp. {DAFTAR_BARANG.biaya}</div>
      </div>
    </div>
  </div>
  <!-- END DAFTAR_BARANG -->
</div>
