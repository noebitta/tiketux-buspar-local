<span class="popuptitle">Pencarian paket</span>
<center>
  <div class="tabletitle">Data Paket</div>
  <div class="wrappertabledata" style="height: 360px;">
    <table class="flat" id="tableheaderbrowse">
      <tr>
        <th>No</th>
        <th>Tgl.Berangkat</th>
        <th>Jam Berangkat</th>
        <th>Asal</th>
        <th>Tujuan</span></th>
        <th>#Jadwal</th>
        <th>#Unit</th>
        <th>Waktu Cetak</th>
      </tr>
    </table>
    <div class="wrapperinnertable" id="wrappertablecontentbrowse" style="height: 330px;">
      <table class="flat" id="tablecontentbrowse">
        <!-- BEGIN ROW -->
        <tr class="{ROW.odd}">
          <td align="center">{ROW.no}</td>
          <td align="center">{ROW.tglberangkat}</td>
          <td align="center">{ROW.jamberangkat}</td>
          <td align="center">{ROW.cabangasal}</td>
          <td align="center">{ROW.cabangtujuan}</td>
          <td align="center">{ROW.kodejadwal}</td>
          <td align="center">{ROW.nopol}</td>
          <td align="center">{ROW.waktucetak}</td>
        </tr>
        <!-- END ROW -->
      </table>
    </div>
  </div>

</center>

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
  <form onsubmit="return false;">
    <table width='800'>
      <tr><td><h1>Paket</h1></td></tr>
      <tr>
        <td bgcolor='ffffff' height=300 valign='top' align='center'>
          <div id="rewrite_ambil_paket"></div>
          <span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <br>
          <input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
          <input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
        </td>
      </tr>
    </table>
  </form>
</div>
<!--END dialog ambil paket-->