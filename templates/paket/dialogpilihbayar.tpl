<span class="popuptitle">{OPERATION} VOUCHER</span>
<center>
  <div class="wrapperpopup" style="width: 500px;height: 210px;">
    <span class="popuplabel">Kode Voucher</span><span class="popupinput"><textarea id="kodevoucher" name="kodevoucher" rows="5" cols="30" readonly>{KODE_VOUCHER}</textarea></span><br><br>
    <span class="popuplabel">Alasan Pembatalan</span><span class="popupinput"><textarea id="alasan" name="alasan" onfocus="this.style.background='white';" rows="2" cols="30"></textarea></span><br><br>
    <span class="flatbutton" onclick="batalkan();" id="buttonbatal">BATALKAN</span>
  </div>
</center>

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
  <form onsubmit="return false;">
    <table>
      <tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
      <tr>
        <td align='center'>
          <table bgcolor='white' width='100%'>
            <tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
            <tr><td>
                <input type='hidden' id='kode_booking_go_show' value='' />
                <table width='100%'>
                  <tr>
                    <td width='50%' align='center' valign='middle'>
                      <a href="" onClick="CetakTiket(0);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_tunai.png" /></a>
                      <br />
                      <a href="" onClick="CetakTiket(0);return false;"><span class="genmed">Tunai</span></a>
                    </td>
                    <td width='50%' align='center' valign='middle'>
                      <!--
							<a href="" onClick="CetakTiket(1);return false;"><img src="{TPL}images/icon_debitcard.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(1);return false;"><span class="genmed">Debit Card</span></a>
							-->
                      <a href="" onClick="CetakTiket(5);return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_edc.png" /></a>
                      <br />
                      <a href="" onClick="CetakTiket(5);return false;"><span class="genmed">EDC</span></a>
                    </td>
                  </tr>
                  <tr>
                    <!--
						<td align='center' valign='middle'>
							<a href="" onClick="CetakTiket(2);return false;"><img src="{TPL}images/icon_mastercard.png" /></a>
							<br />
							<a href="" onClick="CetakTiket(2);return false;"><span class="genmed">Credit Card</span></a>
						</td>
						-->
                    <td align='center' valign='middle'>
                      <a href="" onClick="showInputVoucher();return false;"><img src="{TPL}images/icon_pembayaran/ic_jp_voucher.png" /></a>
                      <br />
                      <a href="" onClick="showInputVoucher();return false;"><span class="genmed">Voucher</span></a>
                    </td>
                  </tr>
                </table>
              </td></tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center">
          <input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
        </td>
      </tr>
    </table>
  </form>
</div>
<!--END dialog Pilih Pembayaran-->