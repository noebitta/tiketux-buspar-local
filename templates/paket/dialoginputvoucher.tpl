<span class="popuptitle">{OPERATION} VOUCHER</span>
<center>
  <div class="wrapperpopup" style="width: 500px;height: 210px;">
    <span class="popuplabel">Kode Voucher</span><span class="popupinput"><textarea id="kodevoucher" name="kodevoucher" rows="5" cols="30" readonly>{KODE_VOUCHER}</textarea></span><br><br>
    <span class="popuplabel">Alasan Pembatalan</span><span class="popupinput"><textarea id="alasan" name="alasan" onfocus="this.style.background='white';" rows="2" cols="30"></textarea></span><br><br>
    <span class="flatbutton" onclick="batalkan();" id="buttonbatal">BATALKAN</span>
  </div>
</center>

<!--dialog Input Kode Voucher-->
<div dojoType="dialog" id="dialog_input_voucher" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
  <form onsubmit="return false;">
    <table>
      <tr><td><h2>Voucher</h2></td></tr>
      <tr>
        <td align='center'>
          <table bgcolor='white' width='100%'>
            <tr height=30><td colspan=3>Silahkan masukkan KODE VOUCHER</td></tr>
            <tr height=30><td>Kode Voucher</td><td>:</td><td><input type='password' id='input_kode_voucher'/></td></tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center">
          <input type="button" id="dialog_input_voucher_btn_ok_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
          <input type="button" onclick="bayarByVoucher(input_kode_voucher.value);" id="dialog_input_voucher_btn_ok" value="   Bayar   ">
        </td>
      </tr>
    </table>
  </form>
</div>
<!--END dialog Input Kode Voucher-->