<span class="popuptitle">Pencarian Paket</span>
<center>
  <div class="wrappertabledata" style="height: 250px;">
    <!-- BEGIN NO_DATA -->
    <span class="pesan"> tidak ada data yang ditemukan</span>
    <!-- END NO_DATA -->
    <!-- BEGIN TABLE_HEADER -->
    <table class="flat" id="tableheaderbrowse">
      <tr>
        <th>No</th>
        <th>Resi</th>
        <th>Tgl.Berangkat</th>
        <th>Jam Berangkat</th>
        <th>Carier</th>
        <th>Tujuan</th>
        <th>Status</th>
        <th>Remark</th>
      </tr>
    </table>
    <!-- END TABLE_HEADER -->
    <div class="wrapperinnertable" id="wrappertablecontentbrowse" style="height: 220px;">
      <table class="flat" id="tablecontentbrowse">
        <!-- BEGIN ROW -->
        <tr class="{ROW.odd}">
          <td align="center">{ROW.no}</td>
          <td align="center"><a href="" onclick="showDetailPaket('{ROW.noresi}');return false;" class="crud">&nbsp;{ROW.noresi}&nbsp;</a></td>
          <td align="center">{ROW.tglberangkat}</td>
          <td align="center">{ROW.jamberangkat}</td>
          <td align="center">{ROW.jadwal}</td>
          <td align="center">{ROW.tujuan}</td>
          <td align="center">{ROW.status}</td>
          <td align="center">{ROW.remark}</td>
        </tr>
        <!-- END ROW -->
      </table>
    </div>
  </div>

</center>