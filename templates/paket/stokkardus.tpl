<span class="popuptitle">Stok Kardus Cabang: {CABANG}</span>
<div style="overflow:auto; overflow-x:hidden;width: 100%;position: absolute;top: 0px;left: 0px;text-align: center;">
  <center>
    <br>
    <div class="wrappertabledata">
      <table class="flat" id="tableheaderdialog">
        <tr>
          <th>No</th>
          <th>Jenis Kardus</th>
          <th>Jumlah Stok</th>
        </tr>
      </table>
      <div class="wrapperinnertable" id="wrappertablecontentdialog">
        <table class="flat" id="tablecontentdialog">
          <!-- BEGIN ROW -->
          <tr class="{ROW.class}">
            <td align="center">{ROW.no}</td>
            <td align="center">{ROW.jeniskardus}</td>
            <td align="center">{ROW.jumlahstok}</td>
          </tr>
          <!-- END ROW -->
        </table>
      </div>
    </div>
  </center>
</div>