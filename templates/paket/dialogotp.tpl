<span class="popuptitle">{DLG_TITLE}</span>
<center>
  <div class="wrapperpopup" style="width: 500px;height: {DLG_HEIGHT}px;">
    <span style="font-size: 14px;font-family: 'trebuchet ms';color: #555555;width: 100%;text-align: center;">{DLG_KETERANGAN}</span><br><br>
    <!-- BEGIN INPUT_OTP -->
    <span class="popuplabel">Kode OTP</span><span class="popupinput"><input type="password" id="otpdlg" onkeypress="validasiAngka(event);" onfocus="this.style.background='white';" size="8" maxlength="6" style="font-size: 20px;text-align: center"/></span><br><br>
    <!-- END INPUT_OTP -->
    <!-- BEGIN INPUT_ALASAN -->
    <span class="popuplabel">Masukkan Alasan</span><span class="popupinput"><textarea id="alasandlg" onkeypress="validCharInput(event);" onfocus="this.style.background='white';" rows="5" cols="30"></textarea></span><br><br>
    <!-- END INPUT_ALASAN -->
    <span class="flatbutton" onclick="{DLG_ACTION}" id="buttonproses">{DLG_BUTTON}</span>
  </div>
</center>