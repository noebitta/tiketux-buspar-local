<span class="popuptitle">{OPERATION} VOUCHER</span>
<center>
  <div class="wrapperpopup" style="width: 500px;height: 210px;">
    <span class="popuplabel">Kode Voucher</span><span class="popupinput"><textarea id="kodevoucher" name="kodevoucher" rows="5" cols="30" readonly>{KODE_VOUCHER}</textarea></span><br><br>
    <span class="popuplabel">Alasan Pembatalan</span><span class="popupinput"><textarea id="alasan" name="alasan" onfocus="this.style.background='white';" rows="2" cols="30"></textarea></span><br><br>
    <span class="flatbutton" onclick="batalkan();" id="buttonbatal">BATALKAN</span>
  </div>
</center>

<!--dialog Cek Stok Kardus -->
<div dojoType="dialog" id="dialog_CekStokKardus" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
  <form onsubmit="return false;">
    <table width='300'>
      <tr><td><h1>Stok Kardus</h1></td></tr>
      <tr>
        <td bgcolor='ffffff' height=300 valign='top' align='center'>
          <div id="cek_stok_kardus"></div>
          <span id='progress_cek_stok_kardus' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <br>
          <input type="button" onClick="Show_CekStok.hide()" value="&nbsp;&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;">
        </td>
      </tr>
    </table>
  </form>
</div>
<!--END dialog Cek Stok Kardus -->