<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script>
function printWindow() {
	bV = parseInt(navigator.appVersion);
	if (bV >= 4) window.print();
}
</script>

<body class="tiket">
  <div style="width: 400px;text-align: center;">
    <span style="font-size: 14px">--copy untuk pengirim--</span><br>
    <span style="font-size: 18px">{NAMA_PERUSAHAAN}</span><br>
    <span style="font-size: 12px">{ALAMAT_PERUSAHAAN}</span><br>
    <span style="font-size: 12px">{TELP_PERUSAHAAN}</span><br>
    <span style="font-size: 16px">------------------------</span><br>
    <span style="font-size: 16px">RESI PAKET</span><br>
    <!-- BEGIN IS_COPY -->
    <span style="font-size: 14px;">COPY COPY COPY COPY COPY COPY</span><br>
    <!-- END IS_COPY -->
    <img src="{QR_CODE}"><br>
    <span style="font-size: 18px">{NO_RESI}</span><br><br>
    <span style="font-size: 16px">-----KEBERANGKATAN------</span><br>
    <span class="resilabel">Tgl:</span>&nbsp;<span class="residatafield">{TGL_BERANGKAT}</span>
    <span class="resilabel">Asal:</span>&nbsp;<span class="residatafield">{ASAL}</span>
    <span class="resilabel">Tujuan:</span>&nbsp;<span class="residatafield">{TUJUAN}</span>
    <span class="resilabel">Jam:</span>&nbsp;<span class="residatafield">{JAM_BERANGKAT}</span>
    <span class="resilabel">Jadwal:</span>&nbsp;<span class="residatafield">{KODE_JADWAL}</span>
    <br><br>
    <span style="font-size: 16px">-------PENGIRIM---------</span><br>
    <span class="resilabel">Telp:</span>&nbsp;<span class="residatafield">{TELP_PENGIRIM}</span>
    <span class="resilabel">Nama:</span>&nbsp;<span class="residatafield">{NAMA_PENGIRIM}</span>
    <span class="resilabel">Alamat:</span>&nbsp;<span class="residatafield">{ALAMAT_PENGIRIM}</span>
    <span class="resilabel">Dijemput:</span>&nbsp;<span class="residatafield">{IS_DIJEMPUT}</span>
    <br><br>
    <span style="font-size: 16px">-------PENERIMA---------</span><br>
    <span class="resilabel">Telp:</span>&nbsp;<span class="residatafield">{TELP_PENERIMA}</span>
    <span class="resilabel">Nama:</span>&nbsp;<span class="residatafield">{NAMA_PENERIMA}</span>
    <span class="resilabel">Alamat:</span>&nbsp;<span class="residatafield">{ALAMAT_PENERIMA}</span>
    <span class="resilabel">Diantar</span>&nbsp;<span class="residatafield">{IS_DIANTAR}</span>
    <br><br>
    <span style="font-size: 16px">---------BARANG---------</span><br>
    <span class="resilabel">Berat:</span>&nbsp;<span class="residatafield">{BERAT}&nbsp;Kg</span>
    <span class="resilabel">Layanan:</span>&nbsp;<span class="residatafield">{LAYANAN}</span>
    <span class="resilabel">Kardus:</span>&nbsp;<span class="residatafield">{JENIS_KARDUS}</span>
    <span class="resilabel">Keterangan:</span>&nbsp;<span class="residatafield">{KETERANGAN}</span>
    <span class="resilabel">Ket.Disc:</span>&nbsp;<span class="residatafield">{KET_DISCOUNT}</span>
    <br><br>
    <span style="font-size: 16px">----------BIAYA---------</span><br>
    <span class="resilabel">Harga:</span>&nbsp;<span class="residatafield"><span style="width: 50%;display: inline-block;text-align: right;">Rp.{HARGA}</span></span>
    <span class="resilabel">Discount:</span>&nbsp;<span class="residatafield"><span style="width: 50%;display: inline-block;text-align: right;">-Rp.{DISCOUNT}</span></span>
    <span class="resilabel">Chg.Jemput:</span>&nbsp;<span class="residatafield"><span style="width: 50%;display: inline-block;text-align: right;">Rp.{CHARGE_JEMPUT}</span></span>
    <span class="resilabel">Chg.Antar</span>&nbsp;<span class="residatafield"><span style="width: 50%;display: inline-block;text-align: right;">Rp.{CHARGE_ANTAR}</span></span><br>
    <span style="font-size: 16px">------------------------</span><br>
    <span class="resilabel">Bayar:</span>&nbsp;<span class="residatafield"><span style="width: 50%;display: inline-block;text-align: right;">Rp.{TOTAL_BAYAR}</span></span>
    <br>
    <span style="font-size: 16px">------------------------</span><br>
    <span style="font-size: 16px">{WAKTU_TRANSAKSI}</span><br>
    <span style="font-size: 16px">Oleh:{PETUGAS}</span><br><br><br>
    <span style="font-size: 12px">{WEBSITE}</span><br>
    <span style="font-size: 12px">{KETERANGAN_RESI}</span><br>
    <br>
    <span style="font-size: 16px">-------------------potong---------------------</span><br><br>
    <span style="font-size: 14px">--label untuk barang--</span><br>
    <span style="font-size: 18px">{NAMA_PERUSAHAAN}</span><br>
    <span style="font-size: 12px">{ALAMAT_PERUSAHAAN}</span><br>
    <span style="font-size: 12px">{TELP_PERUSAHAAN}</span><br>
    <span style="font-size: 16px">------------------------</span><br>
    <!-- BEGIN IS_COPY -->
    <span style="font-size: 14px;">COPY COPY COPY COPY COPY COPY</span><br>
    <!-- END IS_COPY -->
    <img src="{QR_CODE}"><br>
    <span style="font-size: 18px">{NO_RESI}</span><br><br>
    <!-- BEGIN TRANSIT -->
    <span style="font-size: 20px">[--TRANSIT--]</span><br><br>
    <!-- END TRANSIT -->
    <span style="font-size: 24px;">{TUJUAN}</span><br>
    <span class="resilabel">Tgl:</span>&nbsp;<span class="residatafield">{TGL_BERANGKAT}</span>
    <span class="resilabel">Asal:</span>&nbsp;<span class="residatafield">{ASAL}</span>
    <span class="resilabel">Jam:</span>&nbsp;<span class="residatafield">{JAM_BERANGKAT}</span>
    <span class="resilabel">Kode:</span>&nbsp;<span class="residatafield">{KODE_JADWAL}</span>
    <br><br>
    <span style="font-size: 16px">-------PENGIRIM---------</span><br>
    <span class="resilabel">Nama:</span>&nbsp;<span class="residatafield">{NAMA_PENGIRIM}</span>
    <span class="resilabel">Alamat:</span>&nbsp;<span class="residatafield">{ALAMAT_PENGIRIM}</span>
    <span class="resilabel">Dijemput:</span>&nbsp;<span class="residatafield">{IS_DIJEMPUT}</span>
    <br><br>
    <span style="font-size: 16px">-------PENERIMA---------</span><br>
    <span class="resilabel">Nama:</span>&nbsp;<span class="residatafield">{NAMA_PENERIMA}</span>
    <span class="resilabel">Alamat:</span>&nbsp;<span class="residatafield">{ALAMAT_PENERIMA}</span>
    <span class="resilabel">Diantar:</span>&nbsp;<span class="residatafield">{IS_DIANTAR}</span>
    <br>
    <span style="font-size: 16px">------------------------</span><br>
    <span style="font-size: 16px">{WAKTU_TRANSAKSI}</span><br>
    <span style="font-size: 16px">Oleh:{PETUGAS}</span><br>

  </div>

  <script language="javascript">
    printWindow();
    window.close();
  </script>

</body>
</html>