<span class="popuptitle">Daftar Paket Manifest {NO_MANIFEST_INDUK}</span>
<input type="hidden" id="nomanifestindukdipilih" value="{NO_MANIFEST_INDUK}"/>
<div id="bodyoverlay" class="resvsectionoverlay" style="display: none;"><span style="font-size: 14px;font-family: 'trebuchet ms';text-transform: uppercase;padding-top: 170px;display: inline-block;">sedang memroses...</span> </div>
<div style="overflow:auto; overflow-x:hidden;height: 400px;width: 100%;position: absolute;top: 0px;left: 0px;text-align: center;">
  <!-- BEGIN MANIFEST -->
  <br>
  <div class="wrappertabledata" style="height: 200px;">
    <span style="display: inline-block;float:left;margin-left: 10px;text-transform: uppercase;"> <a href="#" onclick="checkinPaket({MANIFEST.idx},'{MANIFEST.nomanifestpaket}');return false;" class="crud">&nbsp;Proses Check In&nbsp;</a>&nbsp;Manifest Paket:{MANIFEST.nomanifestpaket}</span>
    <table class="flat" id="tableheaderbrowse{MANIFEST.idx}">
      <tr>
        <th rowspan="2"></th>
        <th rowspan="2">No</th>
        <th rowspan="2">Resi</th>
        <th rowspan="2">Tujuan Akhir</th>
        <th rowspan="2">Status</th>
        <th colspan="3">Pengirim</th>
        <th colspan="3">Penerima</th>
        <th colspan="3">Barang</th>
        <th rowspan="2">Remark</th>
      </tr>
      <tr>
        <th>Nama</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Nama</th>
        <th>Telp</th>
        <th>Alamat</th>
        <th>Berat</th>
        <th>Layanan</th>
        <th>Keterangan</th>
      </tr>
    </table>
    <div class="wrapperinnertable" id="wrappertablecontentbrowse{MANIFEST.idx}" style="height: 220px;">
      <table class="flat" id="tablecontentbrowse{MANIFEST.idx}">
        <!-- BEGIN ROW -->
        <tr class="{MANIFEST.ROW.class}" onclick="{MANIFEST.ROW.act};return false;" style="cursor: pointer;">
          <td align="center">
            <!-- BEGIN BTN_BATALCHECK -->
            <a href="" onclick="checkinPaket('{MANIFEST.ROW.noresi}',{MANIFEST.ROW.no});return false;" class="crud">&nbsp;Belum Check In&nbsp;</a>
            <!-- END BTN_BATALCHECK -->
            <span id="showchkmnf{MANIFEST.idx}row{MANIFEST.ROW.no}" class="{MANIFEST.ROW.classchk}"><input type="hidden" id="chkmnf{MANIFEST.idx}row{MANIFEST.ROW.no}" value="0"/></span></td>
          <td align="center">{MANIFEST.ROW.no}</td>
          <td align="center">{MANIFEST.ROW.noresi}</td>
          <td align="center">{MANIFEST.ROW.tujuanakhir}</td>
          <td align="center">{MANIFEST.ROW.status}</td>
          <td align="center">{MANIFEST.ROW.namapengirim}</td>
          <td align="center">{MANIFEST.ROW.telppengirim}</td>
          <td align="center">{MANIFEST.ROW.alamatpengirim}</td>
          <td align="center">{MANIFEST.ROW.namapenerima}</td>
          <td align="center">{MANIFEST.ROW.telppenerima}</td>
          <td align="center">{MANIFEST.ROW.alamatpenerima}</td>
          <td align="center">{MANIFEST.ROW.berat} Kg</td>
          <td align="center">{MANIFEST.ROW.layanan}</td>
          <td align="center">{MANIFEST.ROW.keterangan}</td>
          <td align="center">{MANIFEST.ROW.remark}</td>
        </tr>
        <!-- END ROW -->
      </table>
    </div>
  </div>
  <!-- END MANIFEST -->
</div>