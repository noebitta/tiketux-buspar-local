<span class="popuptitle">Detail Paket {NO_RESI}</span>
<div style="overflow:auto; overflow-x:hidden;height: 100%;width: 100%;position: absolute;top: 0px;left: 0px;text-align: center;">
  <div style="float:left;display: inline-block;width:50%;">
    <br>
    <!--<h3 style="text-align: center;">[DATA PENGIRIM]</h3>-->
    <span class="resvisidatalabel">Telp.Pengirim</span>&nbsp;<span class="resvdisidatafield">{TELP_PENGIRIM}</span>
    <span class="resvisidatalabel">Nama Pengirim</span>&nbsp;<span class="resvdisidatafield">{NAMA_PENGIRIM}</span>
    <span class="resvisidatalabel">Alamat Pengirim</span>&nbsp;<span class="resvdisidatafield">{ALAMAT_PENGIRIM}</span>
    <span class="resvisidatalabel" style="vertical-align: middle;">Paket Dijemput</span>&nbsp;<span class="resvdisidatafield">{IS_DIJEMPUT}</span>
    <br><br>
    <!--<h3 style="text-align: center;">[DATA PAKET]</h3>-->
    <span class="resvisidatalabel">Berat</span>&nbsp;<span class="resvdisidatafield">{BERAT}&nbsp;Kg</span>
    <span class="resvisidatalabel">Layanan</span>&nbsp;<span class="resvdisidatafield">{LAYANAN}</span>
    <span class="resvisidatalabel">Jenis Kardus</span>&nbsp;<span class="resvdisidatafield">{JENIS_KARDUS}</span>
    <span class="resvisidatalabel">Keterangan</span>&nbsp;<span class="resvdisidatafield">{KETERANGAN}</span>
    <span class="resvisidatalabel">Keterangan Discount</span>&nbsp;<span class="resvdisidatafield">{KET_DISCOUNT}</span>
    <br><br>
    <!--<h3 style="text-align: center;">[DATA PENGIRIMAN]</h3>-->
    <span class="resvisidatalabel">Dari Cabang</span>&nbsp;<span class="resvdisidatafield">{ASAL}</span>
    <span class="resvisidatalabel">Ke Cabang</span>&nbsp;<span class="resvdisidatafield">{TUJUAN}</span>
    <span class="resvisidatalabel">Tgl.Berangkat</span>&nbsp;<span class="resvdisidatafield">{TGL_BERANGKAT}</span>
    <span class="resvisidatalabel">Kode Carier</span>&nbsp;<span class="resvdisidatafield">{JADWAL}</span>
    <!-- BEGIN PAKET_TRANSIT -->
    <span style="display: inline-block;background: red;color:white;font-size: 12px;padding-left: 5px;padding-right: 5px;">PAKET INI ADALAH PAKET TRANSIT</span>
    <!-- END PAKET_TRANSIT -->
  </div>
  <div style="float:right;display: inline-block;width:50%;">
    <br>
    <!--<h3 style="text-align: center;">[DATA PENERIMA]</h3>-->
    <span class="resvisidatalabel">Telp.Penerima</span>&nbsp;<span class="resvdisidatafield">{TELP_PENERIMA}</span>
    <span class="resvisidatalabel">Nama Penerima</span>&nbsp;<span class="resvdisidatafield">{NAMA_PENERIMA}</span>
    <span class="resvisidatalabel">Alamat Penerima</span>&nbsp;<span class="resvdisidatafield">{ALAMAT_PENERIMA}</span>
    <span class="resvisidatalabel" style="vertical-align: middle;">Paket Diantar</span>&nbsp;<span class="resvdisidatafield">{IS_DIANTAR}</span>
    <br><br>
    <!--<h3 style="text-align: center;">[DATA HARGA]</h3>-->
    <span class="resvisidatalabel">Harga</span>&nbsp;<span class="resvdisidatafield"><span style="width: 50%;display: inline-block;text-align: right;">Rp.{HARGA}</span></span>
    <span class="resvisidatalabel">Discount</span>&nbsp;<span class="resvdisidatafield"><span style="width: 50%;display: inline-block;text-align: right;">-Rp.{DISCOUNT}</span></span>
    <span class="resvisidatalabel">Charge Jemput</span>&nbsp;<span class="resvdisidatafield"><span style="width: 50%;display: inline-block;text-align: right;">Rp.{CHARGE_JEMPUT}</span></span>
    <span class="resvisidatalabel">Charge Antar</span>&nbsp;<span class="resvdisidatafield"><span style="width: 50%;display: inline-block;text-align: right;border-bottom: 1px solid;">Rp.{CHARGE_ANTAR}</span></span>
    <span class="resvisidatalabel">Total Bayar</span>&nbsp;<span class="resvdisidatafield"><span style="width: 50%;display: inline-block;text-align: right;color: green;">Rp.{TOTAL_BAYAR}</span></span>
    <br><br>
    <!--<h3 style="text-align: center;">[INFO DATA]</h3>-->
    <span class="resvisidatalabel">Waktu Transaksi</span>&nbsp;<span class="resvdisidatafield">{WAKTU_TRANSAKSI}</span>
    <span class="resvisidatalabel">Petugas</span>&nbsp;<span class="resvdisidatafield">{PETUGAS}</span>
    <!-- BEGIN IS_DALAM_PROSES -->
    <br><br>
    <span style="display: inline-block;background: grey;color:white;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">DALAM PROSES</span>
    <!-- END IS_DALAM_PROSES -->
    <!-- BEGIN IS_BERANGKAT -->
    <br><br>
    <span style="display: inline-block;background: blue;color:white;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">BERANGKAT</span><br>
    <span class="resvisidatalabel">Waktu Berangkat</span>&nbsp;<span class="resvdisidatafield">{IS_BERANGKAT.waktuberangkat}</span>
    <!-- END IS_BERANGKAT -->
    <!-- BEGIN IS_TRANSIT -->
    <br><br>
    <span style="display: inline-block;background: yellow;color:black;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">TRANSIT</span>
    <!-- END IS_TRANSIT -->
    <!-- BEGIN IS_SAMPAI -->
    <br><br>
    <span style="display: inline-block;background: green;color:white;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">SAMPAI</span><br>
    <span class="resvisidatalabel">Waktu Sampai</span>&nbsp;<span class="resvdisidatafield">{IS_SAMPAI.waktusampai}</span>
    {IS_SAMPAI.keterangan}
    <!-- END IS_SAMPAI -->
    <!-- BEGIN IS_DIAMBIL -->
    <br><br>
    <span style="display: inline-block;background: white;color:black;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">SUDAH DIAMBIL</span><br>
    <span class="resvisidatalabel">Diambil oleh</span>&nbsp;<span class="resvdisidatafield">{IS_DIAMBIL.nama}</span>
    <span class="resvisidatalabel">KTP Pengambil</span>&nbsp;<span class="resvdisidatafield">{IS_DIAMBIL.ktp}</span>
    <span class="resvisidatalabel">Telp.Pengambil</span>&nbsp;<span class="resvdisidatafield">{IS_DIAMBIL.telp}</span>
    <span class="resvisidatalabel">Waktu Pengambilan</span>&nbsp;<span class="resvdisidatafield">{IS_DIAMBIL.waktuambil}</span>
    <!-- END IS_DIAMBIL -->
    <!-- BEGIN IS_DIKIRIM_KURIR -->
    <br><br>
    <span style="display: inline-block;background: purple;color:white;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">KURIR</span><br>
    <span class="resvisidatalabel">Kurir</span>&nbsp;<span class="resvdisidatafield">{IS_DIKIRIM_KURIR.kurir}</span>
    <span class="resvisidatalabel">Waktu Kirim</span>&nbsp;<span class="resvdisidatafield">{IS_DIKIRIM_KURIR.waktukirim}</span>
    <!-- END IS_DIKIRIM_KURIR -->
    <!-- BEGIN IS_DITERIMA -->
    <br><br>
    <span style="display: inline-block;background: white;color:black;font-size: 18px;font-weight: bold;padding: 10px 10px 10px 10px;border: 1px solid black;">SUDAH DITERIMA</span><br>
    <span class="resvisidatalabel">Diterima oleh</span>&nbsp;<span class="resvdisidatafield">{IS_DITERIMA.nama}</span>
    <span class="resvisidatalabel">Telp.Penerima</span>&nbsp;<span class="resvdisidatafield">{IS_DITERIMA.telp}</span>
    <span class="resvisidatalabel">Waktu Update Data</span>&nbsp;<span class="resvdisidatafield">{IS_DITERIMA.waktuterima}</span>
    <!-- END IS_DITERIMA -->
  </div>
  <div style="display: inline-block; width: 100%;">
    <br>
    <!-- BEGIN BTN_TERIMA_PAKET -->
    <span class="flatbutton" onClick="{BTN_TERIMA_PAKET.action}">PAKET DITERIMA</span>
    <!-- END BTN_TERIMA_PAKET -->
    <!-- BEGIN BTN_AMBIL_PAKET -->
    <span class="flatbutton" onClick="{BTN_AMBIL_PAKET.action}">PAKET DIAMBIL</span>
    <!-- END BTN_AMBIL_PAKET -->
    <!-- BEGIN BTN_MUTASI -->
    <span class="flatbutton" onClick="{BTN_MUTASI.action}">MUTASI</span>
    <!-- END BTN_MUTASI -->
    <!-- BEGIN BTN_BATAL -->
    <span class="flatbutton" onClick="{BTN_BATAL.action}">BATALKAN</span>
    <!-- END BTN_BATAL -->
    <!-- BEGIN BTN_CETAK -->
    <span class="flatbutton" onclick="{BTN_CETAK.action}" id="buttonbatal">CETAK RESI</span>
    <!-- END BTN_CETAK -->
  </div>
</div>




