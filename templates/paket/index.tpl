<link rel="stylesheet" href="{TPL}/kalender/kalender.css" type="text/css" />
<script language="JavaScript"  src="{TPL}kalender/kalender.js"></script>
<script type="text/javascript">filePath = '{TPL}js/dropdowncalendar/images/';</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript"  src="{TPL}js/paket.js"></script>
<form id="formsubmit" method="post" action="" target="newwindow">
  <input type="hidden" value="" id="noresidipilih" name="noresidipilih"/>
  <input type="hidden" value="" id="tujuandipilih" name="tujuandipilih"/>
  <input type="hidden" value="" id="otp" name="otp"/>
</form>

<form id="formsubmitmanifest" method="post" action="" target="newwindow">
  <input type="hidden" value="" id="submittglberangkat" name="submittglberangkat"/>
  <input type="hidden" value="" id="submitjadwal" name="submitjadwal"/>
  <input type="hidden" value="" id="otpmanifest" name="otpmanifest"/>
</form>

<input type="hidden" value="0" id="flagmutasipaket" />
<input type="hidden" value="" id="tglberangkat" />
<input type="hidden" value="" id="kotadipilih" />
<input type="hidden" value="" id="asaldipilih" />
<input type="hidden" value="" id="jadwaldipilih" />
<input type="hidden" value="" id="kotadipilihid" />
<input type="hidden" value="" id="asaldipilihid" />
<input type="hidden" value="" id="tujuandipilihid" />
<input type="hidden" value="" id="jadwaldipilihid" />
<input type="hidden" value="" id="cabangtujuanakhirid" />
<input id='hargakgpertama1' type='hidden' value='0'/>
<input id='hargakgpertama2' type='hidden' value='0'/>
<input id='hargakgpertama3' type='hidden' value='0'/>
<input id='hargakgpertama4' type='hidden' value='0'/>
<input id='hargakgpertama5' type='hidden' value='0'/>
<input id='hargakgpertama6' type='hidden' value='0'/>
<input id='hargakgberikutnya1' type='hidden' value='0'/>
<input id='hargakgberikutnya2' type='hidden' value='0'/>
<input id='hargakgberikutnya3' type='hidden' value='0'/>
<input id='hargakgberikutnya4' type='hidden' value='0'/>
<input id='hargakgberikutnya5' type='hidden' value='0'/>
<input id='hargakgberikutnya6' type='hidden' value='0'/>
<input id='chargejemput' type='hidden' value='{CHARGE_JEMPUT}'/>
<input id='chargeantar' type='hidden' value='{CHARGE_ANTAR}'/>

<!-- SHOW PESAN BARU -->
<span class="resvnotif" style="display: none;">Anda Memiliki <span id="jumlahpesanbaru"></span> pesan baru. Klik untuk membuka</span>
<span id="resvmutasimodeon" style="display: none;">anda sedang berada dalam mode mutasi! <a href="" onclick="batalkanModeMutasi();return false;" class="crud">&nbsp;Batalkan Mode Mutasi&nbsp;</a></span>

<div id="wrapperpaketbody" style="position: relative">
  <div id="bodyoverlay" class="resvsectionoverlayload" style="display: none;"></div>
  <div id="wrapperpaketheader" style="box-shadow: 0px 5px 5px grey;height: 50px;">
    <span style="padding-left: 10px;padding-top:10px;display:inline-block; float: left;"><input name="inputcari" id="inputcari" type="text" placeholder="nomor resi/ telp pelanggan" style="width: 210px;text-align: center;text-transform: uppercase;height: 23px;font-size: 13px" onclick="this.style.background='white';" onkeypress="validCharInput(event);">&nbsp; <span class="flatbutton" onclick="cariDataPaket(inputcari.value);"  style="padding-top: 3px;width: 70px;height: 20px;">CARI</span></span>
    <span style="padding-right: 4px;display:inline-block; float: right;">
      <div class="menubarbutton"><a href="#" onclick="showStokKardus();return false" class="menubar"><div class="menubaricon">Stok Kardus</div></a></div>
      <div class="menubarbutton"><a href="#" onclick="Start('{U_BIAYA_PETTY_CASH}');return false" class="menubar"><div class="menubaricon">Petty Cash</div></a></div>
      <div class="menubarbutton"><a href="#" onclick="Start('{U_BIAYA_TAMBAHAN}');return false" class="menubar"><div class="menubaricon">Biaya Tambahan</div></a></div>
      <div class="menubarbutton"><a href="#" onclick="Start('{U_LAPORAN_UANG}');return false" class="menubar"><div class="menubaricon">Rekap Uang</div></a></div>
      <div class="menubarbutton"><a href="#" onclick="Start('{U_LAPORAN_PENJUALAN}');return false" class="menubar"><div class="menubaricon">Rekap Penjualan</div></a></div>
    </span>
  </div>

  <div style="padding-top: 20px;"></div>
  <div id="wrapperpaketinput">
    <div class="resvsection" style="width:23%;">
      <div class="resvsectioncard" style="height: 190px;">
        <div id="showkalender" style="height:170px;padding-top:"></div>
      </div>

      <div class="resvsectioncard">
        <span class="resvsectiontitle">Kota Asal</span>
        <div id="resvshowkota">
          <!-- BEGIN OPT_KOTA -->
          <div class="resvlistpilihan" id="kotaasal{OPT_KOTA.idx}" onclick="setCabangBerangkat('{OPT_KOTA.kota}',this.id);">{OPT_KOTA.kota}</div><br>
          <!-- END OPT_KOTA -->
        </div>
      </div>

      <div class="resvsectioncard">

        <span class="resvsectiontitle">Point Asal</span>
        <div class="resvsectioncontent">
          <div id="resvshowpointasal">pilih kota</div>
          <div id="asaloverlay" class="resvsectionoverlayload" style="display: none;"></div>
        </div>
      </div>

      <div class="resvsectioncard">
        <span class="resvsectiontitle">Point Tujuan</span>
        <div class="resvsectioncontent">
          <div id="resvshowpointtujuan">
            pilih point asal
          </div>
          <div id="tujuanoverlay" class="resvsectionoverlayload" style="display: none;"></div>
        </div>
      </div>

      <div class="resvsectioncard">
        <span class="resvsectiontitle">Jadwal</span>
        <div class="resvsectioncontent">
          <div id="resvshowjadwal">
            pilih point tujuan
          </div>
          <div id="jadwaloverlay" class="resvsectionoverlayload" style="display: none;"></div>
        </div>
      </div>
    </div>

    <div class="resvsection" style="width:37%;">
      <div class="resvsectioncontent">
        <div id="showlayout"></div>
        <div id="layoutoverlay" class="resvsectionoverlayload" style="display: none;"></div>
      </div>
    </div>

    <div class="resvsection" style="width:39%;">

      <div class="resvsectioncontent">

        <div id="inputoverlay" class="resvsectionoverlay" style="display: none;"></div>

        <div class="resvsectioncard" style="padding-bottom: 10px">
          <span class="resvsectiontitle" id="sectiondatapengirim">Data Pengirim</span>
          <span class="resvisidatalabel">Telp.Pengirim</span><span class="resvdisidatafield">&nbsp;<input id="telppengirim" type="text" maxlength="15" onkeypress="validasiNoTelp(event);" onFocus="this.style.background='white';" onBlur="cariDataPelanggan(this.value,1)"/></span>
          <span class="resvisidatalabel">Nama Pengirim</span><span class="resvdisidatafield">&nbsp;<input id="namapengirim" type="text" maxlength="100" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"/></span>
          <span class="resvisidatalabel">Alamat Pengirim</span><span class="resvdisidatafield">&nbsp;<textarea id="alamatpengirim" rows="3" rcols="40" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"></textarea></span>
          <span class="resvisidatalabel" style="vertical-align: middle;">Paket Dijemput</span><span class="resvdisidatafield"><input id="paketdijemput" type="checkbox" style="width: 20px;height: 20px;" onclick="hitungHargaPaket();"/></span>
        </div>

        <div class="resvsectioncard" style="padding-bottom: 10px">
          <span class="resvsectiontitle" id="sectiondatapenerima">Data Penerima</span>
          <span class="resvisidatalabel">Telp.Penerima</span><span class="resvdisidatafield">&nbsp;<input id="telppenerima" type="text" maxlength="15" onkeypress="validasiNoTelp(event);" onFocus="this.style.background='white';" onBlur="cariDataPelanggan(this.value,0)"/></span>
          <span class="resvisidatalabel">Nama Penerima</span><span class="resvdisidatafield">&nbsp;<input id="namapenerima" type="text" maxlength="100" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"/></span>
          <span class="resvisidatalabel">Alamat Penerima</span><span class="resvdisidatafield">&nbsp;<textarea id="alamatpenerima" rows="3" rcols="40" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"></textarea></span>
          <span class="resvisidatalabel" style="vertical-align: middle;">Paket Diantar</span><span class="resvdisidatafield"><input id="paketdiantar" type="checkbox" style="width: 20px;height: 20px;" onclick="hitungHargaPaket();"/></span>
        </div>

        <div class="resvsectioncard" style="padding-bottom: 10px">

          <span class="resvsectiontitle">Data Paket</span>
          <span class="resvisidatalabel">Cabang Tujuan Akhir</span><span class="resvdisidatafield">&nbsp;<span id="cabangtujuanakhir" class="combolistselection" onClick="setComboList(this,'cabangtujuanakhirid','paket.php','mode=14&kota='+kotatujuan.value,false,true)">&nbsp;</span></span>
          <span class="resvisidatalabel">Berat</span><span class="resvdisidatafield">&nbsp;<input id="beratpaket" type="text" maxlength="4" size="5" style="text-align: center;" onkeypress="validasiInputanAngka(event);" onFocus="this.style.background='white';" onBlur="hitungHargaPaket();"/>&nbsp;Kg</span>
          <span class="resvisidatalabel">Layanan</span><span class="resvdisidatafield">&nbsp;<select id='layananpaket' onchange="jeniskardus.selectedIndex=this.selectedIndex;hitungHargaPaket();">
                                                                                                <!-- BEGIN LIST_LAYANAN -->
                                                                                                <option value="{LIST_LAYANAN.value}">{LIST_LAYANAN.text}</option>
                                                                                                <!-- END LIST_LAYANAN -->
                                                                                              </select>
          </span>
          <span class="resvisidatalabel">Harga</span><span class="resvdisidatafield">&nbsp;Rp.<span id='showhargapaket'>0</span></span>
          <span class="resvisidatalabel">Jenis Kardus</span><span class="resvdisidatafield">&nbsp;<select id='jeniskardus'>
                                                                                                    <!-- BEGIN LIST_KARDUS -->
                                                                                                    <option value="{LIST_KARDUS.value}">{LIST_KARDUS.text}</option>
                                                                                                    <!-- END LIST_KARDUS -->
                                                                                                    <option value="0">-tanpa kardus-</option>
                                                                                                  </select>
          </span>
          <span class="resvisidatalabel">Cara Pembayaran</span><span class="resvdisidatafield">&nbsp;<select id='carabayar'>
                                                                                                        <option value="0">Tunai</option>
                                                                                                        <option value="2">Bayar di Tujuan</option>
                                                                                                      </select>
          </span>
          <span class="resvisidatalabel">Keterangan</span><span class="resvdisidatafield">&nbsp;<textarea id="keteranganpaket" rows="3" rcols="40" onkeypress="validCharInput(event);" onFocus="this.style.background='white';" style="text-transform: uppercase;"></textarea></span>
        </div>
        <div class="resvsectioncard" style="padding-bottom: 10px">
          <br>
          <span class="resvisidatalabel">Kode Voucher (jika ada)</span><span class="resvdisidatafield">&nbsp;<input id="kodevoucher" type="text" maxlength="100" onkeypress="validCharInput(event);" onFocus="this.style.background='white';"/></span><br><br>
          <span class="flatbutton" onClick="resetInput();"  style="padding-top: 3px;width: 70px;height: 20px;">RESET</span>&nbsp;
          <span class="flatbutton" onClick="showDialogPembayaran();"  style="padding-top: 3px;width: 70px;height: 20px;">SIMPAN</span>
        </div>
      </div>
    </div>
  </div>
</div>
