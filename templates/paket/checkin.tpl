<script type="text/javascript">
  filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">

  function checkinPaket(idmanifest,nomanifest){

    if(confirm("Anda akan melakukan proses CHECK-IN paket, klik OK untuk melanjutkan proses ini!")){

      //bodyoverlay.style.display="block";

      var idxrow=1;
      var loop=true;
      var listresi="";

      var tabledata = document.getElementById("tablecontentbrowse"+idmanifest);

      do{
        elmchk = document.getElementById("chkmnf"+idmanifest+"row"+idxrow);

        if(elmchk){
          if(elmchk.value==1){
            noresi    = "'"+tabledata.rows[idxrow-1].cells[2].innerHTML+"'";
            listresi  += listresi!=""?","+noresi:noresi;
          }
        }
        else{
          loop=false;
        }

        idxrow++;
      }while(loop);

      new Ajax.Request("paket.checkin.php",{
        asynchronous: true,
        method: "post",
        parameters: "mode=3&listresi="+listresi+"&nomanifest="+nomanifest,
        onLoading: function(request){
        },
        onComplete: function(request){
        },
        onSuccess: function(request){

          if(isJson(request.responseText)) {
            var result;
            result = JSON.parse(request.responseText);

            if (result["status"] == "OK") {
              browsePaket(nomanifestindukdipilih.value);
            }
            else {
              alert("Gagal:" + result["pesan"]);
            }
          }
          else{
            alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
          }
        },
        onFailure: function(request){
          alert('Error !!! Cannot Save');
          assignError(request.responseText);
        }
      });

    }

  }

  function checkinPaketByBarcode(){

    if(!validasiInput(resipaketscan)){
      return false;
    }

    new Ajax.Request("paket.checkin.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=5&noresi="+resipaketscan.value,
      onLoading: function(request){
      },
      onComplete: function(request){
      },
      onSuccess: function(request){

        if(isJson(request.responseText)) {
          var result;
          result = JSON.parse(request.responseText);

          if (result["status"] == "OK") {
            resipaketscan.value = "";
            resipaketscan.focus();
            document.getElementById("notifberhasil").style.display = "block";
            timerShowNotif("notifberhasil");
          }
          else {
            resipaketscan.value = "";
            resipaketscan.focus();
            alert(result["status"] + ":" + result["pesan"]);
          }
        }
        else{
          alert("TERJADI KEGAGALAN DALAM PROSES("+request.responseText+")");
        }
      },
      onFailure: function(request){
        alert('Error !!! Cannot Save');
        assignError(request.responseText);
      }
    });


  }

  function selectRow(elmrow,idmanifest,idrow){

    elmchk      = document.getElementById("chkmnf"+idmanifest+"row"+idrow);
    elmshowchk  = document.getElementById("showchkmnf"+idmanifest+"row"+idrow);

    elmchk.value= 1-elmchk.value;

    if(elmchk.value==0){
      elmshowchk.className  = "b_chk";
      elmrow.className      = ((elmrow.cells[1].innerHTML*1)%2==0?"even":"odd");
    }
    else{
      elmshowchk.className  = "b_ok";
      elmrow.className      = "yellowcell";
    }

  }

  function browsePaket(nomanifest){

    setPopup(800,400);

    new Ajax.Updater("popupcontent","paket.checkin.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=2"+
      "&nomanifest="+nomanifest,
      onLoading: function(request) {
      },
      onComplete: function(request) {
        idx=0;
        while(document.getElementById("tableheaderbrowse"+idx)) {
          setLayoutTable(document.getElementById("tableheaderbrowse"+idx), document.getElementById("tablecontentbrowse"+idx), document.getElementById("wrappertablecontentbrowse"+idx));
          idx++;
        }
      },
      onSuccess: function(request) {
        popupwrapper.show();
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })
  }

  function showDialogScanBarcode(){

    setPopup(500,150);

    new Ajax.Updater("popupcontent","paket.checkin.php",{
      asynchronous: true,
      method: "post",
      parameters: "mode=4",
      onLoading: function(request) {
      },
      onComplete: function(request) {
        document.getElementById("resipaketscan").focus();
      },
      onSuccess: function(request) {
        popupwrapper.show();
      },
      onFailure: function(request) {
        assignError(request.responseText);
      }
    })
  }

  function setOrder(colid){
    sortDataTable(colid,document.getElementById("formdata"));
  }

  function init(e){
     popupwrapper	= dojo.widget.byId("popupcontainer");

    setComboList(document.getElementById("seltujuan"),"tujuan","paket.checkin.php","mode=1",true,false);
  }

  dojo.addOnLoad(init);


</script>

<form action="{ACTION_CARI}" method="post" id="formdata" name="formdata">
  <input type="hidden" id="id" name="id" value="">
  <input type="hidden" id="tujuan" name="tujuan" value="{FIL_TUJUAN}">
  <input type="hidden" id="mode" name="mode" value="">
  <input type="hidden" id="idxpage" name="idxpage" value="{IDX_PAGE}">
  <input type="hidden" id="orderby" name="orderby" value="{ORDER}">
  <input type="hidden" id="sort" name="sort" value="{SORT}">

  <div class="wrapperheader" style="height:50px;">
    <div class="headertittle">Check-in Paket</div>
    <div class="headerfilter" style="padding-top: 7px;">
      Tujuan <span id="seltujuan" class="combolistselection" onClick="setComboList(this,'tujuan','paket.checkin.php','mode=1',true,true);">&nbsp;</span>
      Periode:<input readonly="yes" class="drop_calendar" id="filtglawal" name="filtglawal" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
      s.d.<input readonly="yes" class="drop_calendar" id="filtglakhir" name="filtglakhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
      Cari <input type="text" id="cari" name="cari" value="{CARI}" onkeypress="validCharInput(event);" placeholder="manifest/kode unit"/>
      <input type="submit" value="cari" onclick="idxpage.value=0;"/>&nbsp;
    </div>
  </div>
  <br>
  <div style="float: left;padding-bottom: 2px;padding-left: 10px;">
   <!-- CRUD -->
    <span class="flatbutton" onClick="showDialogScanBarcode();">SCAN BARCODE</span>
  </div>
  <div style="float: right;padding-bottom: 2px;padding-right: 10px;">
    {PAGING}
  </div>
</form>
<br><br>
<hr>
<div class="tabletitle">
  Daftar Manifest Paket<br>
  <span style="display: inline-block;float: left;padding-left: 10px;font-size: 12px;text-transform: none;"><span style="display: inline-block;width:20px; height: 10px;" class="greencell"></span>: Lengkap&nbsp;|&nbsp;<span style="display: inline-block;width:20px; height: 10px;" class="yellowcell"></span>: Belum Lengkap</span>
</div>
<div class="wrappertabledata">
  <!-- BEGIN TABLE_HEADER -->
  <table class="flat" id="tableheader">
    <tr>
      <th>No</th>
      <th onclick="setOrder(0)">Manifest Induk</th>
      <th onclick="setOrder(1)">Manifest Paket</th>
      <th onclick="setOrder(2)">Jumlah Manifest Paket</th>
      <th onclick="setOrder(3)">Tgl.Berangkat</th>
      <th onclick="setOrder(4)">Asal</th>
      <th onclick="setOrder(5)">Tujuan</th>
      <th onclick="setOrder(6)">Kode Jadwal</th>
      <th onclick="setOrder(7)">Jam Berangkat</th>
      <th onclick="setOrder(8)">Termanifest</th>
      <th onclick="setOrder(9)">Belum Manifest</th>
      <th onclick="setOrder(10)">Sampai</th>
      <th onclick="setOrder(11)">Waktu Manifest</th>
      <th onclick="setOrder(12)">Kode Unit</th>
      <th onclick="setOrder(13)">No.Plat</th>
      <th onclick="setOrder(14)">Kode Driver</th>
      <th onclick="setOrder(15)">Driver</th>
    </tr>
  </table>
  <!-- END TABLE_HEADER -->

  <!-- BEGIN NO_DATA -->
  <span class="pesan"> tidak ada data yang ditemukan</span>
  <!-- END NO_DATA -->

  <div class="wrapperinnertable" id="wrappertablecontent">
    <table class="flat" id="tablecontent">
      <!-- BEGIN ROW -->
      <tr class="{ROW.odd}">
        <td align="center" class="{ROW.class}">{ROW.no}</td>
        <td align="center"><a href="" onclick="browsePaket('{ROW.nomanifestinduk}');return false;" class="crud">&nbsp;{ROW.nomanifestinduk}&nbsp;</a></td>
        <td align="center">{ROW.nomanifestpaket}</td>
        <td align="center">{ROW.jumlahmanifestpaket}</td>
        <td align="center">{ROW.tglberangkat}</td>
        <td align="center">{ROW.asal}</td>
        <td align="center">{ROW.tujuan}</td>
        <td align="center">{ROW.kodejadwal}</td>
        <td align="center">{ROW.jamberangkat}</td>
        <td align="center">{ROW.pakettermanifest}</td>
        <td align="center">{ROW.paketbelumtermanifest}</td>
        <td align="center" class="{ROW.class}">{ROW.paketditerima}</td>
        <td align="center">{ROW.waktucetakmanifest}</td>
        <td align="center">{ROW.kodeunit}</td>
        <td align="center">{ROW.noplat}</td>
        <td align="center">{ROW.kodedriver}</td>
        <td align="center">{ROW.driver}</td>
      </tr>
      <!-- END ROW -->
    </table>
  </div>
</div>

<script language="javascript">
  setClassArrowSort(orderby.value*1+1,sort.value,document.getElementById("tableheader"));
  setLayoutTable(document.getElementById("tableheader"),document.getElementById("tablecontent"),document.getElementById("wrappertablecontent"));
</script>