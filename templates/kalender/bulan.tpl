<span class="resvsectiontitle">{TITLE}</span>
<input type="hidden" id="tanggaldipilih" name="tanggaldipilih" value="{TANGGAL}"/>
<input type="hidden" id="bulandipilih" name="bulandipilih" value="{BULAN}"/>
<input type="hidden" id="tahundipilih" name="tahundipilih" value="{TAHUN}"/>
<table border="1" cellspacing="0" cellpadding="0" class="borderflat">
  <tr>
    <td colspan="7" align="center">
      <span style="font-family: 'trebuchet ms';text-transform: uppercase;font-weight: bold;display: inline-block;padding-left: 23px;">Bulan</span><span style="display: inline-block;float: right;width: 20px;background: #da251d;cursor:pointer;color: white;" onClick="setKalender(tanggaldipilih.value,bulandipilih.value,tahundipilih.value);">X</span>
    </td>
  </tr>
  <tr>
    <td class="bulan{SEL1}" onclick="pilihBulan(1);">Jan</td>
    <td class="bulan{SEL2}" onclick="pilihBulan(2);">Feb</td>
    <td class="bulan{SEL3}" onclick="pilihBulan(3);">Mar</td>
  </tr>
  <tr>
    <td class="bulan{SEL4}" onclick="pilihBulan(4);">Apr</td>
    <td class="bulan{SEL5}" onclick="pilihBulan(5);">Mei</td>
    <td class="bulan{SEL6}" onclick="pilihBulan(6);">Jun</td>
  </tr>
  <tr>
    <td class="bulan{SEL7}" onclick="pilihBulan(7);">Jul</td>
    <td class="bulan{SEL8}" onclick="pilihBulan(8);">Agu</td>
    <td class="bulan{SEL9}" onclick="pilihBulan(9);">Sep</td>
  </tr>
  <tr>
    <td class="bulan{SEL10}" onclick="pilihBulan(10);">Okt</td>
    <td class="bulan{SEL11}" onclick="pilihBulan(11);">Nov</td>
    <td class="bulan{SEL12}" onclick="pilihBulan(12);">Des</td>
  </tr>
</table>