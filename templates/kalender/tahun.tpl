<span class="resvsectiontitle">{TITLE}</span>
<input type="hidden" id="tanggaldipilih" name="tanggaldipilih" value="{TANGGAL}"/>
<input type="hidden" id="bulandipilih" name="bulandipilih" value="{BULAN}"/>
<input type="hidden" id="tahundipilih" name="tahundipilih" value="{TAHUN}"/>
<table border="1" cellspacing="0" cellpadding="0" class="borderflat">
  <tr>
    <td colspan="7" align="center">
      <span style="font-family: 'trebuchet ms';text-transform: uppercase;font-weight: bold;display: inline-block;padding-left: 23px;">Tahun</span><span style="display: inline-block;float: right;width: 20px;background: #da251d;cursor:pointer;color: white;" onClick="setKalender(tanggaldipilih.value,bulandipilih.value,tahundipilih.value);">X</span>
    </td>
  </tr>
  <!-- BEGIN LIST_TAHUN -->
  <tr>
    <td class="tahun{LIST_TAHUN.se1}" onclick="pilihTahun({LIST_TAHUN.1});">{LIST_TAHUN.1}</td>
    <td class="tahun{LIST_TAHUN.sel2}" onclick="pilihTahun({LIST_TAHUN.2});">{LIST_TAHUN.2}</td>
    <td class="tahun{LIST_TAHUN.sel3}" onclick="pilihTahun({LIST_TAHUN.3});">{LIST_TAHUN.3}</td>
  </tr>
  <!-- END LIST_TAHUN -->
</table>