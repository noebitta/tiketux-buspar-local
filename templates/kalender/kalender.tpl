<span class="resvsectiontitle" style="margin-bottom: 7px;">{TITLE}</span>
<input type="hidden" id="tanggaldipilih" name="tanggaldipilih" value="{TANGGAL}"/>
<input type="hidden" id="bulandipilih" name="bulandipilih" value="{BULAN}"/>
<input type="hidden" id="tahundipilih" name="tahundipilih" value="{TAHUN}"/>

<table border="1" cellspacing="0" cellpadding="0" class="borderflat">
  <tr>
    <td colspan="7" align="center">
      <span class="kalbulan" onclick="setBulan(tanggaldipilih.value,{BULAN},tahundipilih.value);">{BULAN_STR}</span>&nbsp;
      <span class="kaltahun" onclick="setTahun(tanggaldipilih.value,bulandipilih.value,{TAHUN});">{TAHUN}</span>
    </td>
  </tr>
  <tr>
    <td class="kalday">Min</td>
    <td class="kalday">Sen</td>
    <td class="kalday">Sel</td>
    <td class="kalday">Rab</td>
    <td class="kalday">Kam</td>
    <td class="kalday">Jum</td>
    <td class="kalday">Sab</td>
  </tr>
  <!-- BEGIN WEEK -->
  <tr>
    <td class="kaldate{WEEK.sel1}" onclick="{WEEK.dsel1};">{WEEK.d1}</td>
    <td class="kaldate{WEEK.sel2}" onclick="{WEEK.dsel2};">{WEEK.d2}</td>
    <td class="kaldate{WEEK.sel3}" onclick="{WEEK.dsel3};">{WEEK.d3}</td>
    <td class="kaldate{WEEK.sel4}" onclick="{WEEK.dsel4};">{WEEK.d4}</td>
    <td class="kaldate{WEEK.sel5}" onclick="{WEEK.dsel5};">{WEEK.d5}</td>
    <td class="kaldate{WEEK.sel6}" onclick="{WEEK.dsel6};">{WEEK.d6}</td>
    <td class="kaldate{WEEK.sel7}" onclick="{WEEK.dsel7};">{WEEK.d7}</td>
  </tr>
  <!-- END WEEK -->
  <tr>
    <td colspan="7" align="center">
      <span class="kaltoday" onclick="pilihTanggal('{TGL_SEKARANG}')">Hari Ini: {HARI_INI}</span>
    </td>
  </tr>
</table>