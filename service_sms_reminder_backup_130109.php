<?php

// STANDARD
define('FRAMEWORK', true);
chdir("/var/www/html/dayax/");

$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLogSms.php');

$pass   = $HTTP_GET_VARS['pass'];
$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!="" && $pass!="hanyauntukmuindonesiaku"){ 
  exit;
}
//#############################################################################

$Jurusan	= new Jurusan();
$LogSMS		= new LogSMS();

//MENGAMBIL DATA KURSI YANG SUDAH MEMBOOKING
$sql = 
	"SELECT
		TglBerangkat, JamBerangkat, KodeJadwal,
		Nama, Telp, IdJurusan,
		GROUP_CONCAT(CAST(NomorKursi AS CHAR) SEPARATOR ',') AS NomorKursi, KodeBooking
	FROM tbl_reservasi
	WHERE FlagBatal!=1
		AND TIMEDIFF(CONCAT(TglBerangkat,' ',JamBerangkat),Now())>='00:00:00'
		AND TIMEDIFF(CONCAT(TglBerangkat,' ',JamBerangkat),Now())<='00:$sms_config[range_reminder]:00'
		AND NoSPJ=''
		AND (UCASE(Nama)!='KOSONG' OR UCASE(Nama)!='BLOK') 
		AND FlagSendSMSReminder!=1
	GROUP BY KodeBooking,Telp";
  			
if (!$result = $db->sql_query($sql)){
	die_error("Err: $this->ID_FILE".__LINE__);
}

$list_kode_booking	= "";
$list_response			= "";

$data_sms	= array();

while($row=$db->sql_fetchrow($result)){
	
	if(substr($row['Telp'],0,2)=="08"){
		
		//$list_kode_booking	.="'$row[KodeBooking]',";
		
		//mengambil asal dan tujuan
		$data_jurusan	= $Jurusan->ambilDataDetail($row['IdJurusan']);
		$asal		= substr($data_jurusan['NamaCabangAsal'],0,10);
		$tujuan	= substr($data_jurusan['NamaCabangTujuan'],0,10);
		
		$list_no_kursi_dipilih	= substr($list_no_kursi_dipilih,0,-1);
		$list_no_tiket	= substr($list_no_tiket,0,-1);
		
		/*$isi_pesan	= 
			"Sdr/i. ".strtoupper(substr($row['Nama'],0,10))." TELAH MEMBOOKING BIMOTRANS UNTUK $asal-$tujuan TGL ".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." $row[JamBerangkat] SEAT NO:$row[NomorKursi] MOHON DATANG 15 MENIT SEBELUM KEBERANGKATAN";
		*/
		
		//PESAN BIMO PINDAH ALAMAT
		$isi_pesan	= 
			"Anda membooking BIMOTRANS u/ tgl ".dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." $row[JamBerangkat] seat:$row[NomorKursi]. Mulai tgl 3 jan 13,loket GIANT Pamulang pindah ke PASAR KITA Pamulang Kota,blok K.19/5.021 99297300";
		
		$id_operator	= substr($row['Telp'],0,4);
		$pengirim	= (in_array($id_operator,$HEADER_NO_NON_ALPHA_SENDER))?$sms_config['pengirim2']:$sms_config['pengirim'];
		
		$telepon	= "62".substr($row['Telp'],1);
		$parameter= "username=$sms_config[user]&password=$sms_config[password]&type=0&dlr=1&destination=$telepon&source=$pengirim&message=$isi_pesan";	
		$response	= sendHttpPost($sms_config['url'],$parameter);	
		
		$list_response	.= $row['Telp']." | ".$response." | Send by:".$pengirim."<br>";
		
		$data_pnp['KodeBooking']		= $row['KodeBooking'];
		$data_pnp['Telp']						= $row['Telp'];
		$data_pnp['Nama']						= $row['Nama'];
		$data_pnp['TipePengiriman']	= 1;
		$data_pnp['IsiSMS']					= $isi_pesan;
		$data_pnp['Response']					= substr($response,0,4);
		
		$data_sms[]	= $data_pnp;
		
	}
	
}

//mengisi ke tabel log SMS
for($idx=0;$idx<count($data_sms);$idx++){
	if($data_pnp['Response']=='1701'){
		$LogSMS->tambah(
			$data_sms[$idx]['Telp'], $data_sms[$idx]['Nama'], $data_sms[$idx]['TipePengiriman'], 
			$data_sms[$idx]['KodeBooking'], $data_sms[$idx]['IsiSMS']);
		
		$list_kode_booking	.="'".$data_sms[$idx]['KodeBooking']."',";
	}
}

$list_kode_booking	= substr($list_kode_booking,0,-1);

if($list_kode_booking!=''){
	//UPDATE FLAG SEND SMS REMINDER
	$sql = 
		"UPDATE tbl_reservasi
			SET FlagSendSMSReminder=1, WaktuKirimSMSReminder=NOW()
		WHERE KodeBooking IN($list_kode_booking)";
	  			
	if (!$result = $db->sql_query($sql)){
		die_error("Err: ID_FILE".__LINE__);
	}
}

$keterangan	= $list_response!=""?$list_response:"List Kosong";
	
echo($keterangan);

?>