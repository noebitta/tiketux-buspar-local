<?php
// HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

//INCLUDE
include($adp_root_path . 'ClassAuth.php');

// SESSION
$id_page = 0;

//INSTANCE
$Auth     = new Auth();

//SECURITY#######################################################################
$username = getVariabel("user");
$token    = getVariabel("token");

$token_compare  = $Auth->createToken($username,$user_ip);

if($token!=$token_compare){
  //jika token tidak sama, hacking attempt
  die_error("Anda tidak memiliki akses kedalam sistem ini!");
}
//#############################################################################

// PARAMETER
$mode 		= getVariabel('mode');

//INIT
$mode     = $mode==""?0:$mode;

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //SHOW DIALOG DAFTARKAN KOMPUTER

    showDialogDaftarkanKomputer();

    break;

  case 1:
    //SET COMBO CABANG

    $cari     = getVariabel('filter');

    setListCabang($cari);

    break;

  case 2:
    //DAFTARKAN KOMPUTER
    $nama_komputer= getVariabel('namakomputer');
    $fingerprint  = getVariabel('hostuuid');
    $cabang       = getVariabel('cabang');

    daftarkanKomputer($nama_komputer,$fingerprint,$cabang,$username);

    break;

  case 3:
    //PROSES CHECK IN
    $list_resi   = getVariabel('listresi');
    $no_manifest = getVariabel('nomanifest');

    prosesCheckIn($list_resi,$no_manifest);

    break;

  case 4:
    //SHOW DIALOG SCAN BARCODE

    showDialogScanBarcode();

    break;

  case 5:
    //PROSES CHECK IN BY BARCODE

    $no_resi = getVariabel('noresi');

    prosesCheckInByBarcode($no_resi);

    break;

}

//METHODES & PROCESS ========================================================================================================================================================================

function showDialogDaftarkanKomputer(){

  global $template,$config,$username,$token;

  $template->set_filenames(array('body' => 'login.daftarkankomputer.tpl'));

  $template->assign_vars(array(
      "SITENAME" => $config["site_name"],
      "USER"     => $username,
      "TOKEN"    => $token
    )
  );

  $template->pparse('body');

}

function setListCabang($cari=""){
  global $db,$Auth;

  $result = $Auth->setComboListCabang($cari);

  while($row=$db->sql_fetchrow($result)){
    $list_cabang[]=array("group"=>$row["Kota"],"id"=>$row["KodeCabang"],"text"=>$row["Nama"]);
  }

  $ret_val  =json_encode($list_cabang);

  echo($ret_val);
}

function daftarkanKomputer($nama_komputer,$fingerprint,$cabang,$username){
  //INCLUDE
  global $Auth;

  if($nama_komputer=="" || $fingerprint=="" || $cabang==""){
    //VALIDASI INPUT
    die_error("Anda tidak memiliki akses ke sistem ini!");
  }

  if($Auth->daftarkanKomputer($fingerprint,$cabang,strtoupper($nama_komputer),$username)){
    redirect("./");
  }
  else{
    die_error("Komputer sudah pernah didaftarkan!");
  }

} //daftarkanKomputer

?>