<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassReservasi.php');


//SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);

// LIST
$template->set_filenames(array('body' => 'antrian_body.tpl'));

$kondisi =	$cari==""?"":
    " AND a.KodeBooking LIKE '%$cari%'";

$cabang  = $userdata['KodeCabang'];

$sql = "SELECT a.NoAntrian, a.TglAntrian,r.KodeBooking, r.TglBerangkat, r.JamBerangkat, r.Nama, r.KodeJadwal, r.NoTiket,
              f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(r.IdJurusan)) AS ASAL,
              f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(r.IdJurusan)) AS TUJUAN,
              GROUP_CONCAT(NomorKursi) AS Kursi
        FROM
        tbl_reservasi r
        JOIN tbl_kios_antrian a ON r.KodeBooking = a.KodeBooking
        WHERE
        (a.TglAntrian ='$tanggal_mulai_mysql') AND
        a.KodeCabang = '$cabang' AND r.CetakTiket != 1 AND r.FlagBatal != 1 $kondisi
        GROUP BY a.KodeBooking
        ORDER BY NoAntrian";

if(!$result = $db->sql_query($sql)){
    echo $sql;
    die("Error : ".__LINE__);
}

$idx = 0;
while ($row = $db->sql_fetchrow($result)){
    $odd = 'odd';

    if (($idx % 2) == 0) {
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'       =>$odd,
            'no'        =>$idx+1,
            'no_antrian'=>$row['NoAntrian'],
            'tgl_antri' =>date_format(date_create($row['TglAntrian']),'d/m/Y'),
            'jurusan'   =>$row['ASAL']." - ".$row['TUJUAN'],
            'kode_booking'=>$row['KodeBooking'],
            'tgl_berangkat'=>$row['TglBerangkat'],
            'format_tgl_berangkat'=>date_format(date_create($row['TglBerangkat']),'d/m/Y'),
            'jam_berangkat'=>$row['JamBerangkat'],
            'kursi'     => $row['Kursi'],
            'nama'      =>$row['Nama'],
            'kode_jadwal'=>$row['KodeJadwal'],
            'no_tiket'  => $row['NoTiket']
        )
    );

    $idx++;
}

if($userdata['user_level'] == $USER_LEVEL_INDEX["ADMIN"] || $userdata['user_level'] == $USER_LEVEL_INDEX["MANAJEMEN"] || $userdata['user_level'] == $USER_LEVEL_INDEX["MANAJER"] || $userdata['user_level'] == $USER_LEVEL_INDEX["CALL_CENTER"] || $userdata['user_level'] == $USER_LEVEL_INDEX["SPV_RESERVASI"]){
    $template->assign_block_vars('show_dialog_pembayaran_transfer',array());
}

$template->assign_vars(array(
    'BCRUMP'    		=> setBcrump($id_page),
    'ACTION_CARI'		=> append_sid('antrian.'.$phpEx),
    'TGL_AWAL'			=> $tanggal_mulai,
    'TXT_CARI'			=> $cari,
    )
);
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>

