<?php
/**
 * Pembayaran Indomaret.
 *
 * Last updated: 1/20/16, 9:47 AM
 *
 * @author Sopyan Adicandra Ramandani <sopyan@3trust.com>
 *
 */
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'class.Agen.php');
include($adp_root_path . 'Class.SignatureGenerator.php');
include($adp_root_path . 'ClassLogIndomaret.php');

global $config;
$date           = date("Y-m-d H:i:s");
$module         = 'Request Payment Code';
$action         = 'POST';
$url            = "/request_paymentcode.php";
$user_id        = "0";

$agen           = new Agen();
$LogIndomaret   = new LogIndomaret();
// dev
$authKey 		= $config['key'];
$secretKey 		= $config['secret'];
// live
/*$authKey 		= '84405b1b8166cfb2cc15e502b0ab5be2054f01591';
$secretKey 		= 'c030dfedc50556b255df7eef5d55c959054f0159e';*/
$merchant		= 'DAYTRANS';

$kode_booking 	= $HTTP_GET_VARS['kode_booking'];
$digit          = 6;
$otp 			= rand(pow(10, $digit-1), pow(10, $digit)-1);
$result         = ambilDataReservasi($kode_booking);

if($result){
    while($row = $db->sql_fetchrow($result)){
        $kode_booking   = $row['KodeBooking'];
        $orderAmount    = $row['TotalHarga'];
        $orderTime 	    = $row['WaktuPesan'];
        $user_name      = $row['Nama'];
        $orderUserPhone = $row['Telp'];
        $booking_name   = $merchant.'-'.$user_name;
        $digit          = 6 ;
        $otp            = rand(pow(10, $digit-1), pow(10, $digit)-1);
        $user_email     = '';

        $sigGenerator		= new SignatureGenerator();
        $auth 				= $authKey. '&' . $secretKey;
        $keyId				= $sigGenerator->genKeyId($auth);

        $genSignature		= $sigGenerator->genSignature($merchant, $kode_booking, $orderTime, $orderAmount, $orderUserPhone, $keyId);
        $requestPayment     = $agen->payIndomaret($kode_booking, $booking_name, $orderAmount, $user_name, $orderUserPhone, $user_email, $merchant, $genSignature, $orderTime, $otp);
        $objResquestPayment = json_decode($requestPayment);

        if($objResquestPayment->tiketux->status == 'OK'){
            $payment_code = $objResquestPayment->tiketux->results->payment_code;
            $expired_time = $objResquestPayment->tiketux->results->payment_expired_time;

            //Update Payment Code
            $updatePaymentCode = updatePaymentCodeByKodeBooking($kode_booking,$payment_code,$expired_time,$genSignature,$otp);
            if($updatePaymentCode){
                // Write Finpay Log
                //$module         = 'Reservasi';
                //$action         = 'POST';
                $url            = $objResquestPayment->tiketux->url;
                $status         = $objResquestPayment->tiketux->status;
                $time           = $objResquestPayment->tiketux->time;
                $data           = 'Sukses: Update Payment Code Kode Booking : '.$kode_booking. ' PaymentCode : '.$payment_code;
                //$user_id        = $userdata['userid'];

                $LogIndomaret->writeLog($time,$module,$action,$url,$status,$data,$user_id);
                //echo $kode_booking.",".$time.",".$status."<br />";
                echo 1;
            }else{
                // Write Finpay Log
                //$module         = 'Reservasi';
                //$action         = 'POST';
                $url            = $objResquestPayment->tiketux->url;
                $status         = 'ZERO_RESULT';
                $data           = 'Error: Gagal Update Payment Code Kode Booking : '.$kode_booking;
                //$user_id        = $userdata['userid'];

                $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
                echo $kode_booking.",".$date.",".$status."\n";
                echo 2;
            }

        }else{
            // Write Finpay Log
            //$module         = 'Reservasi';
            //$action         = 'POST';
            $url            = $objResquestPayment->tiketux->url;
            $status         = $objResquestPayment->tiketux->status;
            $error 			= $objResquestPayment->tiketux->error;
            $data           = $requestPayment;
            //$user_id        = $userdata['userid'];

            $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
            echo 3;
        }
    }

}else{
    $status     = "ZERO_RESULT";
    $data       = "Error : Data Not Found!";
    $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
    die("0");
}


function ambilDataReservasi($kode_booking){
    global $db;

    $sql =
        "SELECT *, SUM(Total) as TotalHarga FROM tbl_reservasi WHERE KodeBooking='$kode_booking' AND PaymentCode='' AND CetakTiket = 0 AND FlagBatal != 1 GROUP BY KodeBooking";

    if (!$result = $db->sql_query($sql)){
        die_error("Err:".__LINE__);
    }

    return $result;
}

function updatePaymentCodeByKodeBooking($kode_booking,$payment_code,$expired_time,$genSignature,$otp){

    global $db;

    $sql = "UPDATE tbl_reservasi SET JenisPembayaran=7, PaymentCode='$payment_code',PaymentCodeExpiredTime='$expired_time', OTP='$otp'
                WHERE KodeBooking='$kode_booking';";

    if(!$db->sql_query($sql)){
        //die_error("Err $this->ID_FILE".__LINE__);
    }

    return true;
}