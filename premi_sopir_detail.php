<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
// SESSION
$id_page = 999;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

$Sopir	    = new Sopir();
$Jurusan    = new Jurusan();
$Jadwal     = new Jadwal();

// PARAMETER
$mode           = isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
$kode_sopir     = isset($HTTP_GET_VARS['kode_sopir'])? $HTTP_GET_VARS['kode_sopir'] : $HTTP_POST_VARS['kode_sopir'];
$nama           = isset($HTTP_GET_VARS['nama'])? $HTTP_GET_VARS['nama'] : $HTTP_POST_VARS['nama'];
$hari           = isset($HTTP_GET_VARS['hari'])? $HTTP_GET_VARS['hari'] : $HTTP_POST_VARS['hari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

if($mode == "UbahKehadiran"){
    $id = $HTTP_GET_VARS['idJadwal'];
    $keterangan = $HTTP_GET_VARS['keterangan'];
    $status = $HTTP_GET_VARS['kehadiran'];

    $sql = "UPDATE tbl_penjadwalan_sopir SET Keterangan = '$keterangan', StatusKehadiran = $status WHERE IdJadwal = $id";
    if(!$db->sql_query($sql)){
        echo mysql_error();
    }else{
        echo (1);
    }

    exit;
}

function getDataSPJ($kodejadwal,$tanggal)
{
    global $db;

    $sql = "SELECT NoSPJ, KodeDriver, f_sopir_get_nama_by_id(KodeDriver) AS Supir, f_kendaraan_ambil_nopol_by_kode(NoPolisi) AS Nopol,
                  TglBerangkat, JamBerangkat, f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
                  f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan
            FROM tbl_spj 
            WHERE KodeJadwal = '$kodejadwal' AND DATE(TglBerangkat) = '$tanggal'";

    if(!$result = $db->sql_query($sql)){
        die("Error : ".__LINE__);
    }

    return $db->sql_fetchrow($result);
}

$kondisi = "WHERE TglBerangkat BETWEEN '$tanggal_mulai' AND '$tanggal_akhir' AND KodeDriver = '$kode_sopir'";

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"IdPenjadwalanKendaraan","tbl_penjadwalan_kendaraan","&kode_sopir=$kode_sopir&nama=$nama&hari=$hari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir",$kondisi,"premi_sopir_detail.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================


$sql = "SELECT KodeJadwal, KodeDriver, TglBerangkat, JamBerangkat, StatusAktif,
        f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) AS Asal,
        f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) AS Tujuan,
        f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) AS Nopol
        FROM tbl_penjadwalan_kendaraan
        $kondisi
        ORDER BY TglBerangkat, JamBerangkat";


if($result = $db->sql_query($sql)){
    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){

        $odd ='odd';

        if (($i % 2)==0){
            $odd = 'even';
        }

        $data_spj = getDataSPJ($row['KodeJadwal'],$row['TglBerangkat']);

        if($data_spj['NoSPJ'] != ""){
            if($row['KodeDriver'] != $data_spj['KodeDriver']){
                $keterangan = "Diganti Sopir Lain";
            }else{
                $keterangan = "Hadir";
            }
            $berangkat = date_format(date_create($data_spj['JamBerangkat']),'H:i')." ".$data_spj['Asal']." - ".$data_spj['Tujuan']." ".$data_spj['Supir']." ".$data_spj['Nopol'];

        }else{
            $keterangan = "Belum Berangkat";
            $berangkat  = "";
        }


        if($row['StatusAktif'] != 1){
            $odd = 'red';
            $keterangan = 'Jadwal Di Tutup';
        }


        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'           =>$odd,
                'no'            =>$i,
                'tanggal'       =>translateDay(date_format(date_create($row['TglBerangkat']),'l')).", ".date_format(date_create($row['TglBerangkat']),'d-m-Y'),
                'jadwal'        =>date_format(date_create($row['JamBerangkat']),'H:i')." ".$row['Asal']." - ".$row['Tujuan']." ".$row['Nopol'],
                'berangkat'     =>$berangkat,
                'keterangan'    =>$keterangan
            )
        );

        $i++;

    }
}else{
    die("Error :".__LINE__);
}

// FILE BODY
$template->set_filenames(array('body' => 'premi_sopir_detail_body.tpl'));

$template->assign_vars(array(
        'BCRUMP'    	  =>setBcrump($id_page),
        'NamaSopir'     => $nama,
        'TotalKerja'    => $hari,
        'PAGING'		    => $paging,
    )
);


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
