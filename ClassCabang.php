<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Cabang{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Cabang(){
		$this->ID_FILE="C-CBG";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){

		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/

		//kamus
		global $db;

		$sql =
			"SELECT f_cabang_periksa_duplikasi('$kode') AS jumlah_data";

		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		}
		else{
			die_error("Err:$this->ID_FILE".__LINE__);
		}

		return $ditemukan;

	}//  END periksaDuplikasi

    function ambilData($order_by,$sort_by=0,$idx_page="",$cari=""){

        //kamus
        global $db, $VIEW_PER_PAGE;

        //UNTUK SORTING
        $koloms = array("KodeCabang","Nama","Alamat","Kota","Telp","Fax","Aktif");

        $sort     = ($sort_by==0)?"ASC":"DESC";

        $order		= ($order_by!=="" && $order_by!==null)?" ORDER BY $koloms[$order_by] $sort":'';
        $set_limit= ($idx_page!=="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

        $set_kondisi  = "WHERE 1".($cari==""?"":" AND (Nama LIKE '%$cari%' OR KodeCabang LIKE '$cari%' OR KodeCabang LIKE '%$cari%' OR Alamat LIKE '$cari%'  OR Kota LIKE '%$cari%' OR Telp LIKE '%$cari%'  OR Fax LIKE '%$cari%')");

        $sql  =
            "SELECT SQL_CALC_FOUND_ROWS
        IdCabang,KodeCabang,Nama,
        Alamat,Kota,Telp,Fax,
        IF(IsAktif=1,'AKTIF','NON-AKTIF') AS StatusAktif
      FROM tbl_md_cabang
      $set_kondisi
      $order
	    $set_limit";

    if (!$result = $db->sql_query($sql)){
        echo("Err: $this->ID_FILE:".__LINE__);exit;
    }

    return $result;

    }//  END ambilData

    function hapus($id_cabang){

        //kamus
        global $db;

        //MENGHAPUS DATA KEDALAM DATABASE
        $sql =
            "DELETE FROM tbl_md_cabang
            WHERE IdCabang IN($id_cabang);";

        if (!$db->sql_query($sql)) {
            echo("Err:s $this->ID_FILE" . __LINE__);
            exit;
        }
    }

    function ubah($IdCabang,$KodeCabang,$Nama,$Alamat,$Kota,$Telp,$Fax,$IsAktif){
        global $db;

        $sql  =
            "UPDATE tbl_md_cabang SET
        KodeCabang='$KodeCabang',
        Nama='$Nama',
        Alamat='$Alamat',
        Kota='$Kota',
        Telp='$Telp',
        Fax='$Fax',
        IsAktif='$IsAktif'
      WHERE IdCabang=$IdCabang";

        if (!$db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__);exit;
        }
    } //ubah

//    function hapus($id_cabang){
//
//        /*
//        ID	: 005
//        IS	: data Cabang sudah ada dalam database
//        FS	:Data Cabang dihapus
//        */
//
//        //kamus
//        global $db;
//
//        //MENGHAPUS DATA KEDALAM DATABASE
//        $sql =
//            "DELETE FROM tbl_md_cabang
//			WHERE IdCabang IN($id_cabang);";
//
//        if (!$db->sql_query($sql)){
//            return false;
//            die_error("Err:$this->ID_FILE".__LINE__);
//        }
//        return true;
//    }//end hapus

    function tambah($KodeCabang,$Nama,$Alamat,$Kota,$Telp,$Fax,$IsAktif){

        /*
        ID	: 002
        IS	: data cabang belum ada dalam database
        FS	:Data cabang baru telah disimpan dalam database
        */

        //kamus
        global $db;

        //MENAMBAHKAN DATA KEDALAM DATABASE
        //"CALL sp_cabang_tambah ('$kode','$nama','$alamat','$kota','$telp','$fax',$flag_agen)";
        $sql ="INSERT INTO tbl_md_cabang SET
        KodeCabang='$KodeCabang',
        Nama='$Nama',
        Alamat='$Alamat',
        Kota='$Kota',
        Telp='$Telp',
        Fax='$Fax',
        IsAktif='$IsAktif'";

        if (!$db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__);exit;
        }

        return true;
    }

    function ambilDetailData($id_cabang){
        global $db;

        $sql  = "SELECT IdCabang,KodeCabang,Nama,
                Alamat,Kota,Telp,Fax,IsAktif FROM tbl_md_cabang WHERE IdCabang='$id_cabang'";

        if (!$result = $db->sql_query($sql,TRUE)){
            echo("Err:$this->ID_FILE ".__LINE__);exit;
        }

        $row=$db->sql_fetchrow($result);

        return $row;
    } //ambilDetailData

    function setComboCabang($kota=""){

        /*
        Desc	:Mengembalikan data Cabang sesuai dengan kriteria yang dicari
        */

        //kamus
        global $db;

        if($kota!=""){
            $kondisi_kota	= " WHERE Kota='$kota'";
            $order_by			= " Nama ";
        }
        else{
            $kondisi_kota	= " WHERE 1";
            $order_by			= " Kota,Nama ";
        }

        $sql =
            "SELECT KodeCabang,Nama,Kota
			FROM tbl_md_cabang
			$kondisi_kota AND FlagAgen!=1
			ORDER BY $order_by;";

        if ($result = $db->sql_query($sql)){
            return $result;
        }
        else{
            //die_error("Gagal $this->ID_FILE 003");
            echo("Err: $this->ID_FILE". __LINE__);
        }

    }//  END ambilData

    function ambilCabang($kondisi){


        //kamus
        global $db;

        $sql =
            "SELECT *
			FROM tbl_md_cabang
			$kondisi";

        if ($result = $db->sql_query($sql)){
            return $result;
        }
        else{
            //die_error("Gagal $this->ID_FILE 003");
            echo("Err:". __LINE__);
        }

    }//  END ambilCabang

    function setInterfaceComboCabangByKota($kota="",$cabang_dipilih="",$cabang_default=""){
        //SET COMBO cabang
        global $db;

        //return "<option>$cabang_dipilih</option>";

        if($cabang_default==""){
            $opt_cabang="<option value='' >silahkan pilih...</option>";
            $kondisi_tambahan="";
        }
        else{
            $opt_cabang="silahkan pilih...";
            $kondisi_tambahan=" AND KodeCabang='$cabang_default'";
        }

        if($kota==""){
            $kondisi_tambahan .="";
        }
        else{
            $kondisi_tambahan .=" AND Kota='$kota'";
        }

        $sql =
            "SELECT KodeCabang,Nama,Kota
				FROM tbl_md_cabang
				WHERE 1 $kondisi_tambahan
				ORDER BY Kota,Nama;";

        if (!$result = $db->sql_query($sql)){
            echo("Err:$this->ID_FILE". __LINE__);
            exit;
        }

        if($result){

            $kota="";

            while ($row = $db->sql_fetchrow($result)){
                $selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";

                if($kota!=$row['Kota']){

                    if($kota!=""){
                        $opt_cabang .= "</optgroup>";
                    }

                    $kota=$row['Kota'];
                    $opt_cabang .="<optgroup label='$kota'>";
                }

                $opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] ($row[KodeCabang])</option>";
            }
        }
        else{
            echo("Error :".__LINE__);exit;
        }
        return $opt_cabang;
        //END setInterfaceComboCabang
    }

    function ubahStatusAktif($id_cabang){

        //kamus
        global $db;

        $sql =
            "UPDATE tbl_md_cabang SET
        IsAktif=1-IsAktif
      WHERE IdCabang='$id_cabang';";

        if (!$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        return true;
    }//end ubahStatusAktif

    function setInterfaceComboCabang($cabang_dipilih="",$cabang_default=""){
        //SET COMBO cabang
        global $db;

        if($cabang_default==""){
            $opt_cabang="<option value='' >silahkan pilih...</option>";
            $kondisi_tambahan="";
        }
        else{
            $opt_cabang="";
            $kondisi_tambahan="WHERE KodeCabang='$cabang_default'";
        }

        $sql =
            "SELECT KodeCabang,Nama,Kota
				FROM tbl_md_cabang
				$kondisi_tambahan
				ORDER BY Kota,Nama;";

        if (!$result = $db->sql_query($sql)){
            echo("Err: $this->ID_FILE". __LINE__);
            exit;
        }

        if($result){

            $kota="";

            while ($row = $db->sql_fetchrow($result)){
                $selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";

                if($kota!=$row['Kota']){

                    if($kota!=""){
                        $opt_cabang .= "</optgroup>";
                    }

                    $kota=$row['Kota'];
                    $opt_cabang .="<optgroup label='$kota'>";
                }

                $opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] ($row[KodeCabang])</option>";
            }
        }
        else{
            echo("Error :".__LINE__);exit;
        }
        return $opt_cabang;
        //END setInterfaceComboCabang
    }

    function setInterfaceComboCabangTujuan($cabang_asal="",$cabang_dipilih=""){
        //SET COMBO cabang
        global $db;

        $opt_cabang="<option value='' >silahkan pilih...</option>";

        if($cabang_asal==""){
            return $opt_cabang;
        }

        $sql =
            "SELECT KodeCabangTujuan,Nama,Kota
				FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON KodeCabangTujuan=KodeCabang
				WHERE KodeCabangAsal='$cabang_asal'
				ORDER BY Kota,Nama;";

        if (!$result = $db->sql_query($sql)){
            echo("Err: $this->ID_FILE". __LINE__);
            exit;
        }

        if($result){

            $kota="";

            while ($row = $db->sql_fetchrow($result)){
                $selected	=($cabang_dipilih!=$row['KodeCabangTujuan'])?"":"selected";

                if($kota!=$row['Kota']){

                    if($kota!=""){
                        $opt_cabang .= "</optgroup>";
                    }

                    $kota=$row['Kota'];
                    $opt_cabang .="<optgroup label='$kota'>";
                }

                $opt_cabang .="<option value='$row[KodeCabangTujuan]' $selected>$row[Nama] ($row[KodeCabangTujuan])</option>";
            }
        }
        else{
            echo("Error :".__LINE__);exit;
        }
        return $opt_cabang;
        //END setInterfaceComboCabang
    }

    function isCabangPusat($kode_cabang){
        //SET COMBO cabang
        global $db;

        $sql =
            "SELECT COUNT(1)
				FROM tbl_md_cabang
				WHERE KodeCabang='$kode_cabang';";

        if (!$result = $db->sql_query($sql)){
            echo("Err: $this->ID_FILE". __LINE__);
            exit;
        }

        $row = $db->sql_fetchrow($result);


        return $row[0]>=1?true:false;
        //END isCabangPusat
    }

    function setComboListCabang($cari="",$cabang_asal=""){

        //kamus
        global $db;

        $cabang_asal=="";
            $sql =
                "SELECT KodeCabang,Nama,Kota
			  FROM tbl_md_cabang
			  WHERE (Nama LIKE '%$cari%' OR KodeCabang LIKE '$cari%') AND FlagAgen!=1
			  ORDER BY Kota,Nama;";


        if (!$result = $db->sql_query($sql)){
            echo("Err: $this->ID_FILE". __LINE__);exit;
        }

        return $result;

    }//  END setComboListCabang


}
?>