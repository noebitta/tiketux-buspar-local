<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$id_page = 703;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################


$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];

$tanggal = ($tanggal == "")?date('d-m-Y'):$tanggal;
$tanggal_mysql	= FormatMySQLDateToTgl($tanggal);

$mode = ($mode != "")?$mode:"explorer";

$Jurusan= new Jurusan();
$Jadwal = new Jadwal();
$Cabang	= new Cabang();
$Jadwal	= new Jadwal();
$Sopir	= new Sopir();
$Mobil	= new Mobil();

function setComboCabangAsal($cabang_dipilih){
    //SET COMBO cabang
    global $db;
    global $Cabang;

    $result=$Cabang->ambilData("","Nama,Kota","ASC");
    $opt_cabang="";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
            $opt_cabang .='<option value="'.$row['KodeCabang'].'" '.$selected.'>'.$row['Nama'].' ('.$row['Kota'].') ('.$row['KodeCabang'].')</option>';
        }
    }
    else{
        echo("Error :".__LINE__);exit;
    }
    return $opt_cabang;
    //END SET COMBO CABANG
}
function setComboCabangTujuan($cabang_asal,$idjurusan){
    //SET COMBO cabang
    global $db;
    $sql =
        "SELECT IdJurusan, KodeJurusan, KodeCabangTujuan, KodeCabangAsal, f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan
			WHERE KodeCabangAsal LIKE '$cabang_asal' ORDER BY NamaCabangTujuan";

    if (!$result = $db->sql_query($sql)){
        echo("Error : ".__LINE__);
    }
    $opt_cabang="<option value=''>(none)</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($idjurusan!=$row['IdJurusan'])?"":"selected";
            $opt_cabang .='<option value="'.$row['IdJurusan'].'" '.$selected.'>'.$row['NamaCabangTujuan'].' ('.$row['KodeJurusan'].')</option>';
        }
    }
    else{
        die("Error :".__LINE__);
    }
    return $opt_cabang;
    //END SET COMBO CABANG
}
function setComboJam($id_jurusan,$kodejadwal){
    //SET COMBO JAM KEBERANGKATAN
    global $db;
    $sql = "SELECT KodeJadwal, JamBerangkat, IdJurusan FROM tbl_md_jadwal WHERE IdJurusan = '$id_jurusan'";
    if(!$result = $db->sql_query($sql)){
        die("Error :".__LINE__);
    }
    $opt_cabang="<option value=''>(none)</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ($kodejadwal != $row['KodeJadwal'])?"":"selected";
            $opt_cabang .="<option value='$row[KodeJadwal]' $selected>$row[JamBerangkat] ($row[KodeJadwal])</option>";
        }
    }
    else{
        echo(mysql_error());exit;
    }
    return $opt_cabang;
}
function setComboSopir($kode_sopir_dipilih){
    //SET COMBO SOPIR
    global $db;
    global $Sopir;

    $result=$Sopir->ambilData("","Nama,Alamat","ASC");
    $opt_sopir="<option value=''>- silahkan pilih sopir  -</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
            $opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
        }
    }
    else{
        echo("Error :".__LINE__);exit;
    }
    return $opt_sopir;
    //END SET COMBO SOPIR
}
function setComboMobil($kode_kendaraan){
    //SET COMBO MOBIL
    global $db;
    global $Mobil;

    $result=$Mobil->ambilDataForComboBox();
    $selected_default	= ($kode_kendaraan!="")?"":"selected";

    $opt_mobil="<option value='' $selected_default>- silahkan pilih kendaraan  -</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
            $opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[JumlahKursi] Seat $row[Merek] $row[Jenis]</option>";
        }
    }
    else{
        echo("err :".__LINE__);exit;
    }

    return $opt_mobil;
    //END SET COMBO MOBIL
}
switch ($mode){
    case "get_tujuan":
        $cabang_asal		= $HTTP_GET_VARS['asal'];
        $opt_cabang_tujuan=
            '<select name="opt_tujuan[]" onChange="getUpdateJam(this.value,no)">'.
            setComboCabangTujuan($cabang_asal,"")
            .'</select>';

        echo($opt_cabang_tujuan);
    exit;
    case "get_jam":
        $id_jurusan = $HTTP_GET_VARS['id_jurusan'];
        $opt_jam_berangkat=
            '<select  name="jam[]">'.
            setComboJam($id_jurusan,"")
            .'</select>';
        echo($opt_jam_berangkat);
    exit;
    case "setSopir":
        $kode_group = $HTTP_GET_VARS['kodeGroup'];
        $kode_sopir = $HTTP_GET_VARS['kodeSopir'];

        $sql = "UPDATE tbl_group_jadwal SET KodeSopir = '$kode_sopir' WHERE kode_group = $kode_group";
        if(!$db->sql_query($sql)){
            echo("alert('Gagal Menyimpan Sopir');");
            exit;
        };
        echo ("window.location.reload();");
    exit;
    case "setMobil":
        $kode_group = $HTTP_GET_VARS['kodeGroup'];
        $kode_kendaraan = $HTTP_GET_VARS['kodeMobil'];

        $sql = "UPDATE tbl_group_jadwal SET KodeKendaraan = '$kode_kendaraan' WHERE kode_group = $kode_group";
        if(!$db->sql_query($sql)){
            echo("alert('Gagal Menyimpan Mobil');");
            exit;
        };
        echo ("window.location.reload();");
    exit;
    case "setTitle":
        $kode_group = $HTTP_GET_VARS['kodeGroup'];
        $kode_judul = $HTTP_GET_VARS['kodeTitle'];

        $sql = "UPDATE tbl_group_jadwal SET title_group = '$kode_judul' WHERE kode_group = $kode_group";
        if(!$db->sql_query($sql)){
            echo("alert('Gagal Menyimpan Title');");
            exit;
        };
        echo ("window.location.reload();");
        exit;
    case "save":

        $jml_jadwal = $HTTP_POST_VARS['jml_jadwal'];

        $sql = "SELECT MAX(id_jadwal) AS KODE FROM tbl_group_jadwal";
        if (!$Kode = $db->sql_query($sql)){
            echo("Err: ".__LINE__."<br>");
        }
        $result = $db->sql_fetchrow($Kode);
        $KodeBiaya = $result[0]+1;

        for($i=0; $i<$jml_jadwal; $i++){
            $IdJurusan  = $HTTP_POST_VARS["opt_tujuan"][$i];
            $KodeJadwal = $HTTP_POST_VARS["jam"][$i];

            $sql = "INSERT INTO tbl_group_jadwal (kode_group,IdJurusan,KodeJadwal)
                    VALUES ($KodeBiaya,$IdJurusan,'$KodeJadwal')";

            if(!$db->sql_query($sql)){
                die("Error : ".__LINE__);
            };
        }

        redirect(append_sid('grouping_jadwal.'.$phpEx));

        break;
    case "hapus":
        $kode_group = $HTTP_GET_VARS['kodeGroup'];

        $sql = "DELETE FROM tbl_group_jadwal WHERE kode_group = $kode_group";
        if(!$db->sql_query($sql)){
            echo("alert('Gagal Menyimpan Mobil');");
            exit;
        };
        echo ("window.location.reload();");
    exit;
    case "edit":

        $template->set_filenames(array('body' => 'jadwal/edit_group.tpl'));

        $kode_group = $HTTP_GET_VARS['kode_group'];

        if($pesan==1){
            $pesan="<font color='green' size=3>Data Berhasil Diubah</font>";
            $bgcolor_pesan="98e46f";
        }
        
        $sql="SELECT id_jadwal, kode_group, KodeJadwal, tbl_group_jadwal.IdJurusan, tbl_md_jurusan.KodeCabangAsal, 
                    tbl_md_jurusan.KodeCabangTujuan
              FROM tbl_group_jadwal 
              JOIN tbl_md_jurusan ON tbl_md_jurusan.IdJurusan = tbl_group_jadwal.IdJurusan
              WHERE kode_group = '$kode_group'";
        if(!$result = $db->sql_query($sql)){
            die('Error : '.__LINE__);
        }


        $i = 1;
        while($row = $db->sql_fetchrow($result)){

            $jadwal = '<tr id="jadwal'.$i.'">
                            <input type="hidden" name="id_jadwal[]" value="'.$row['id_jadwal'].'">
                            <td align="right">Asal :</td>
                            <td align="left">
                                    <select onchange="getUpdateTujuan(this.value,'.$i.')" id="opt_asal" name="opt_asal[]">
                                    <option value="">Pilih</option>
                                    '.setComboCabangAsal($row['KodeCabangAsal']).'
                                    </select>
                            </td>
                            <td align="right">Tujuan :</td>
                            <td align="left">
                                <div id="rewrite_tujuan'.$i.'">
                                    <select name="opt_tujuan[]" onChange="getUpdateJam(this.value,no)">'.
                                    setComboCabangTujuan($row['KodeCabangAsal'],$row['IdJurusan'])
                                    .'</select>
                                </div>
                            </td>
                            <td align="right">Jam :</td>
                            <td align="left">
                                <div id="rewrite_jam'.$i.'">
                                <select  name="jam[]">
                                 '.setComboJam($row['IdJurusan'],$row['KodeJadwal']).'
                                </select>
                                </div>
                            </td>
                            <td align="left">
                                <input class="tombol" value="Hapus" onclick="hapus('.$i.')" type="button">
                            </td>
                        </tr>';

            $template->
                assign_block_vars(
                    'ROW',
                    array(
                        'jadwal' => $jadwal,
                    )
                );
            $i++;

        }

        $template->assign_vars(array(
                'BCRUMP'=>setBcrump($id_page),
                'URL'   => append_sid('grouping_jadwal.'.$phpEx.'?mode=update'),
                'JUDUL'	=>'Edit Data Jadwal',
                'ASAL'  => setComboCabangAsal(""),
                'PESAN'			=> $pesan,
                'BGCOLOR_PESAN'	=> $bgcolor_pesan,
                'jml_jadwal'=>$i-1,
                'kode_group'=>$kode_group
            )
        );

        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');

    exit;
    case "update":

        $kode_group = $HTTP_POST_VARS['kode_group'];

        $list_id_jadwal = implode(',',$HTTP_POST_VARS['id_jadwal']);
        $sql = "DELETE FROM tbl_group_jadwal WHERE kode_group = $kode_group AND id_jadwal NOT IN ($list_id_jadwal);";
        if(!$db->sql_query($sql)){
            die("Error : ".__LINE__);
        }

        $jml_jadwal = $HTTP_POST_VARS['jml_jadwal'];

        for($i=0; $i<$jml_jadwal; $i++){
            $IdJadwal   = $HTTP_POST_VARS['id_jadwal'][$i];
            $IdJurusan  = $HTTP_POST_VARS["opt_tujuan"][$i];
            $KodeJadwal = $HTTP_POST_VARS["jam"][$i];

            if($IdJadwal != ""){
                $sql = "UPDATE tbl_group_jadwal SET IdJurusan = $IdJurusan, KodeJadwal = '$KodeJadwal' WHERE id_jadwal = $IdJadwal";

                if(!$db->sql_query($sql)){
                    die("Error : ".__LINE__);
                };
            }else{
                $sql = "INSERT INTO tbl_group_jadwal (kode_group,IdJurusan,KodeJadwal)
                        VALUES ($kode_group,$IdJurusan,'$KodeJadwal')";

                if(!$db->sql_query($sql)){
                    die("Error : ".__LINE__);
                };
            }

        }

        redirect(append_sid('grouping_jadwal.'.$phpEx));
    exit;
    case "add" :
        $pesan = $HTTP_GET_VARS['pesan'];

        if($pesan==1){
            $pesan="<font color='green' size=3>Data Berhasil Disimpan</font>";
            $bgcolor_pesan="98e46f";
        }

        $template->set_filenames(array('body' => 'jadwal/add_group.tpl'));
        $template->assign_vars(array(
                'BCRUMP'=>setBcrump($id_page),
                'URL'   => append_sid('grouping_jadwal.'.$phpEx.'?mode=save'),
                'JUDUL'	=>'Tambah Data Jadwal',
                'ASAL'  => setComboCabangAsal(""),
                'PESAN'			=> $pesan,
                'BGCOLOR_PESAN'	=> $bgcolor_pesan,
            )
        );
    break;
    case "explorer" :
        // LIST
        $template->set_filenames(array('body' => 'jadwal/group_jadwal.tpl'));

        $kondisi	=($cari=="")?"":
            " WHERE tbl_group_jadwal.KodeJadwal LIKE '%$cari%'";

        $sql = "SELECT kode_group,
                    GROUP_CONCAT(
                        tbl_group_jadwal.KodeJadwal
                        ORDER BY
                            id_jadwal
                    ) AS Jadwal, title_group
                FROM
                    tbl_group_jadwal
                   $kondisi
                GROUP BY
                    kode_group";

        if(!$result = $db->sql_query($sql)){
            echo mysql_error();
            die("Error : ".__LINE__);
        }

        $i = 0;
        while($row = $db->sql_fetchrow($result)){

            $list_jadwal = "";

            $arrayJadwal = explode(',',$row['Jadwal']);

            foreach ($arrayJadwal as $item){

                $data_jadwal = $Jadwal->ambilDataDetail($item);
                $jamberangkat = $data_jadwal['JamBerangkat'];
                $idjurusan = $data_jadwal['IdJurusan'];

                $data_jurusan = $Jurusan->ambilNamaJurusanByIdJurusan($idjurusan);

                $asal = $data_jurusan['Asal'];
                $tujuan = $data_jurusan['Tujuan'];

                $list_jadwal .= $asal." - ".$tujuan." : ".$jamberangkat.",";
            }

            $list_jadwal = rtrim($list_jadwal,',');

            $odd ='odd';

            if (($i % 2)==0){
                $odd = 'even';
            }

            $template->
            assign_block_vars(
                'ROW',
                array(
                    'odd'=>$odd,
                    'no'=>$i+1,
                    'group'=>str_replace(',',' => ',$list_jadwal),
                    'kode_group'=>$row['kode_group'],
                    'title'=>$row['title_group'],
                    'EDIT'=>append_sid('grouping_jadwal.'.$phpEx.'?mode=edit&kode_group='.$row['kode_group'])
                )
            );
            $i++;
        }

        $template->assign_vars(array(
                'BCRUMP'    		    =>setBcrump($id_page),
                'U_GROUP_JADWAL_ADD'=> append_sid('grouping_jadwal.'.$phpEx.'?mode=add'),
                'ACTION_CARI'		    => append_sid('grouping_jadwal.'.$phpEx),
                'TXT_CARI'		      => $cari,
                'SOPIR'             => setComboSopir(""),
                'MOBIL'             => setComboMobil(""),
            )
        );
    break;
}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>