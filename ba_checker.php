<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$idx_page = 220;
$userdata = session_pagestart($user_ip,$idx_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
//$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
//$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
//$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];


// LIST
$template->set_filenames(array('body' => 'daftar_manifest/ba_checker_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

//$Cabang	= new Cabang();
//
//switch($mode){
//	case 'getasal':
//		
//		echo "
//			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
//				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
//			</select>";
//		
//	exit;
//		
//	case 'gettujuan':
//		echo "
//			<select name='tujuan' id='tujuan' >
//				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
//			</select>";
//	exit;
//}

$kondisi =	$cari==""?"":
	" AND (KodeDriver LIKE '%$cari%'
		OR Driver LIKE '%$cari%'
		OR JamBerangkat LIKE '%$cari%' 
		OR KodeJurusan LIKE '%$cari%'
		OR NoPolisi LIKE '%$cari%'
		OR NamaChecker LIKE '%$cari%')";

//$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'":"";
//$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'":"";
//$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'":"";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat,Jamberangkat":$sort_by;


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoBA","tbl_ba_check ts",
"&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"ba_checker.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	=
	"SELECT 
		KodeDriver,Driver,
		NoBA,TglBA,KodeJurusan,
		TglBerangkat,JamBerangkat,
		NoPolisi,
		NamaChecker,JumlahPenumpangCheck,JumlahPaketCheck, 
		CatatanTambahanCheck,PathFoto
	FROM tbl_ba_check ts
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

//echo $sql;
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if ($row['PathFoto']) {
		$foto = "<a href='http://daytrans-stg.tiketux.com/apichecker/".$row['PathFoto']."' target='_blank' >Foto</a>";
	}
	else{
		$foto = "--";
	}

	//if($row['IsTerlambat']==1){
	//	$flagterlambat = "red";
	//	$lama_terlambat	= substr($row['Keterlambatan'],0,8);
	//}
	//else{
	//	$flagterlambat	= "";
	//	$lama_terlambat	= "";
	//}

	$sqlBbm	 = "SELECT COUNT(Id) as jml, JumlahLiter, NamaSPBU, TglDicatat, NamaPetugas,Kilometer,IsApprove FROM tbl_voucher_bbm  WHERE NoSPJ = '".$row['NoBA']."'";

	if(!$resBbm = $db->sql_query($sqlBbm)){
		echo("Err:".__LINE__);exit;
	}
	$rwBbm   = $db->sql_fetchrow($resBbm);
	
        
        
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'jadwal'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
				'kodejurusan'=>$row['KodeJurusan'],
				'tgl_ba'=>$row['TglBA'],
				'noba'=>$row['NoBA'],
				'sopir'=>$row['Driver'],
                'foto'=>$foto,
				'mobil'=>$row['NoPolisi'],
				'literbbm'=> number_format($rwBbm['JumlahLiter'],2,'.',','),
				'checker'=>$row['NamaChecker'],
				'penumpangcheck'=>number_format($row['JumlahPenumpangCheck'],0,",","."),
				'paketcheck'=>number_format($row['JumlahPaketCheck'],0,",","."),
				'bgpnpchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangCheck']==$row['JumlahPenumpang']?"#c0ff53":"red"),
				'bgpktchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPaketCheck']==$row['JumlahPaket']?"#c0ff53":"red"),
                                'catatan'=>$row['CatatanTambahanCheck'],				
			)
		);
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&cari=".$cari;
$script_cetak_excel="Start('ba_checker_export_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('ba_checker.'.$phpEx),
	'PAGING'		=> $paging,
	'CETAK_XL'		=> $script_cetak_excel,
	'TGL_AWAL'		=> $tanggal_mulai,
	'TGL_AKHIR'		=> $tanggal_akhir,
	'OPT_KOTA'		=> setComboKota($kota),
	'TXT_CARI'		=> $cari,
	'KOTA'		 	=> $kota,
	'ASAL'			=> $asal,
	'TUJUAN'		=> $tujuan,
	'A_SORT_1'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=TglBerangkat,JamBerangkat'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan berdasarkan Jadwal ($order_invert)",
	'A_SORT_2'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=KodeJurusan'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan berdasarkan KodeJurusan ($order_invert)",
	'A_SORT_3'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=TglBA'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan berdasarkan Tanggal BA ($order_invert)",
	'A_SORT_4'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=NoBA'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan berdasarkan Nomor BA ($order_invert)",
	'A_SORT_5'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=Driver'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan berdasarkan Sopir($order_invert)",
	'A_SORT_6'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=NoPolisi'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan berdasarkan Mobil ($order_invert)",
	'A_SORT_7'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=NamaChecker'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan berdasarkan Nama Checker ($order_invert)",
	'A_SORT_8'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=JumlahPenumpangCheck'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan berdasarkan Jumlah Penumpang ($order_invert)",
	'A_SORT_9'		=> append_sid('ba_checker.'.$phpEx.'?sort_by=JumlahPaketCheck'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan berdasarkan Jumlah Paket ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
