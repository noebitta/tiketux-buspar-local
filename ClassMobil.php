<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern

if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true);
	exit;
}

//#############################################################################

class Mobil{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Mobil(){
		$this->ID_FILE="C-KND";
	}
	
	//BODY
	
	function periksaDuplikasi($kode_kendaraan){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_kendaraan_periksa_duplikasi('$kode_kendaraan') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function periksaDuplikasiNoPol($no_polisi){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_kendaraan_periksa_duplikasi_by_nopol('$no_polisi') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	
	function tambah(
		$kode_kendaraan, $kode_cabang, $no_polisi,
        $jenis,$merek, $tahun, $warna,
        $jumlah_kursi, $kode_sopir1, $kode_sopir2,$kode_sopir3,
        $no_STNK, $no_BPKB, $no_rangka,
        $no_mesin, $kilometer_akhir, $flag_aktif, $kelengkapan,
        $tempo_pajak, $tempo_stnk, $nokir, $kir, $nosipa, $sipa, $nokp, $kp, $status_dt){
	  
		/*
		ID	: 002
		IS	: data mobil belum ada dalam database
		FS	:Data mobil baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
//		$sql =
//			"CALL sp_kendaraan_tambah(
//				'$kode_kendaraan', '$kode_cabang', '$no_polisi',
//				'$jenis','$merek', '$tahun', '$warna',
//				'$jumlah_kursi', '$kode_sopir1', '$kode_sopir2',
//				'$no_STNK', '$no_BPKB', '$no_rangka',
//				'$no_mesin', '$kilometer_akhir', $flag_aktif,'$kelengkapan','$tempo_pajak','$tempo_stnk');";
		$sql= " INSERT INTO tbl_md_kendaraan (
				KodeKendaraan, KodeCabang, NoPolisi,
				Jenis,Merek, Tahun, Warna,
				JumlahKursi, KodeSopir1, KodeSopir2, KodeSopir3,
				NoSTNK, NoBPKB, NoRangka,
				NoMesin, KilometerAkhir, FlagAktif, Kelengkapan, TglPajak, TglStnk, Kir, Kp, Sipa, NoKir, TglKp, TglSipa, IsWH)
			  	VALUES('$kode_kendaraan', '$kode_cabang', '$no_polisi',
				'$jenis','$merek', '$tahun', '$warna',
				'$jumlah_kursi', '$kode_sopir1', '$kode_sopir2','$kode_sopir3',
				'$no_STNK', '$no_BPKB', '$no_rangka',
				'$no_mesin', '$kilometer_akhir', $flag_aktif,'$kelengkapan','$tempo_pajak','$tempo_stnk','$kir','$nokp','$nosipa','$nokir','$kp','$sipa',$status_dt)";
		if (!$db->sql_query($sql)){
			//die_error("Err: $this->ID_FILE".__LINE__);
			die(mysql_error());
		}
		
		return true;
	}
		
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_md_kendaraan
			WHERE 
				KodeKendaraan LIKE '$pencari' 
				OR NoPolisi LIKE '$pencari' 
				OR Jenis LIKE '%$pencari%' 
				OR Merek LIKE '%$pencari%'
				OR Tahun LIKE '$pencari'
				OR NoSTNK LIKE '$pencari'
				OR NoMesin LIKE '$pencari'
				OR NoBPKB LIKE '$pencari'
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ubah(
		$kode_kendaraan_old,
		$kode_kendaraan, $kode_cabang, $no_polisi,
    $jenis,$merek, $tahun, $warna,
    $jumlah_kursi, $kode_sopir1, $kode_sopir2, $kode_sopir3,
    $no_STNK, $no_BPKB, $no_rangka,
    $no_mesin, $kilometer_akhir, $flag_aktif,$kelengkapan, $tempo_pajak, $tempo_stnk, $nokir, $kir, $nosipa, $sipa, $nokp, $kp, $status_dt){
	  
		/*
		ID	: 004
		IS	: data kendaraan sudah ada dalam database
		FS	:Data kendaraan diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
//		$sql =
//			"CALL sp_kendaraan_ubah(
//				'$kode_kendaraan_old',
//				'$kode_kendaraan', '$kode_cabang', '$no_polisi',
//				'$jenis','$merek', '$tahun', '$warna',
//				'$jumlah_kursi', '$kode_sopir1', '$kode_sopir2',
//				'$no_STNK', '$no_BPKB', '$no_rangka',
//				'$no_mesin', '$kilometer_akhir', $flag_aktif, '$kelengkapan', '$tempo_pajak','$tempo_stnk');";
		$sql = "UPDATE tbl_md_kendaraan SET
				KodeKendaraan='$kode_kendaraan', KodeCabang='$kode_cabang', NoPolisi='$no_polisi',
				Jenis='$jenis',Merek='$merek', Tahun='$tahun', Warna='$warna',
				JumlahKursi='$jumlah_kursi', KodeSopir1='$kode_sopir1', KodeSopir2='$kode_sopir2', KodeSopir3 ='$kode_sopir3',
				NoSTNK='$no_STNK', NoBPKB='$no_BPKB', NoRangka='$no_rangka',
				NoMesin='$no_mesin', KilometerAkhir='$kilometer_akhir', FlagAktif=$flag_aktif,
				Kelengkapan='$kelengkapan', TglPajak='$tempo_pajak', TglStnk='$tempo_stnk', Kir = '$kir', Kp = '$nokp', Sipa ='$nosipa',
				NoKir='$nokir', TglSipa='$sipa', TglKp='$kp', IsWH = $status_dt
			  	WHERE KodeKendaraan = '$kode_kendaraan_old'";
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_mobil){
	  
		/*
		ID	: 005
		IS	: data member sudah ada dalam database
		FS	:Data member dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_kendaraan
			WHERE KodeKendaraan IN($list_mobil);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ubahStatusAktif($kode_kendaraan,$remark=""){
	  
		/*
		ID	: 006
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql =
			"UPDATE tbl_md_kendaraan SET
				FlagAktif=1-FlagAktif,Remark='$remark'
			WHERE KodeKendaraan='$kode_kendaraan';";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function ambilDataDetail($kode_kendaraan){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_kendaraan
			WHERE KodeKendaraan='$kode_kendaraan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData

	function getArrayLayout(){
		global $db;

		$sql	= "SELECT * FROM tbl_md_kendaraan_layout ORDER BY LayoutBody";

		if (!$result = $db->sql_query($sql)){
		//	die_error("Err: $this->ID_FILE".__LINE__);
		die(mysql_error());
		}

		while($row=$db->sql_fetchrow($result)){
			$body_array["$row[0]"]	= $row[1];
		}

		return $body_array;
	}

	function layoutBodyDecode($string_body){

		$decode_array	= explode(";",$string_body);

		foreach($decode_array as $array_value){
			$array_dump = explode("=",$array_value);

			if($array_dump[0]!=""){
				$body_array["$array_dump[0]"]=$array_dump[1];
			}
		}

		return $body_array;
	}
	
	function ambilDataForComboBox(){
		
		/*
		ID	:008
		Desc	:Mengembalikan datakendaraan sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				KodeKendaraan, Jenis, Merek,NoPolisi,JumlahKursi
			FROM tbl_md_kendaraan
			WHERE FlagAktif=1 ORDER BY KodeKendaraan";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
}
?>