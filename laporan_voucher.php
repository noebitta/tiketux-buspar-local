<?php
//
// PENGATURAN / MASTER
//
//echo "x";
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassVoucher.php');
include($adp_root_path . 'ClassReservasi.php');

$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$Voucher	  = new Voucher();
$Reservasi  = new Reservasi();

// SESSION
$id_page = 308;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$is_tgl_digunakan	= isset($HTTP_GET_VARS['istgldigunakan'])? $HTTP_GET_VARS['istgldigunakan'] : $HTTP_POST_VARS['istgldigunakan'];
$fil_status			= isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status'];
$kota  				= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$tanggal_mulai  	= isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  	= isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$is_today			= isset($HTTP_GET_VARS['istoday'])? $HTTP_GET_VARS['istoday'] : $HTTP_POST_VARS['istoday'];
$is_today			= $is_today!=""?$is_today:0;

//PREPARASI

$Cabang	= new Cabang();

function setComboCabangTujuan($cabang_asal){
	//SET COMBO cabang
	global $db;
	
	$Jurusan = new Jurusan();
			
	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="<option value=''>silahkan pilih...</otpion>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_asal!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

switch($mode){
	case 'gettujuan':
		$cabang_asal	= $HTTP_GET_VARS['asal'];
		echo("<select id='jurusan' name='jurusan'>".setComboCabangTujuan($cabang_asal)."</select>");
		exit;
	
	case 'buatvoucher':
		
		$kodevoucher = $Voucher->setVoucher(
                            $HTTP_GET_VARS['jurusan'],$userdata['user_id'],FormatTglToMySQLDate($HTTP_GET_VARS['expireddate']),
                            $HTTP_GET_VARS['nilaivoucher'],$HTTP_GET_VARS['ishargatetap'],$HTTP_GET_VARS['isbolehweekend'],
                            $HTTP_GET_VARS['keterangan'],$HTTP_GET_VARS['jumlahcetak'],FormatTglToMySQLDate($HTTP_GET_VARS['startdate']),
                            strtoupper($HTTP_GET_VARS['prefix']),$HTTP_GET_VARS['hargajual']);

        echo("alert('Voucher telah berhasil dibuat!');window.location.reload();");

		exit;

    case 'setOnline':
        $kode_voucher = $HTTP_GET_VARS['kode_voucher'];
        $data_voucher = $Voucher->getDataVoucher($kode_voucher);

        $result = $Voucher->sendVoucher($kode_voucher,$data_voucher['Keterangan'],$data_voucher['NilaiVoucher'],$data_voucher['ExpiredDate'],$data_voucher['IdJurusan'],$data_voucher['IsBolehWeekEnd'],$data_voucher['IsHargaTetap']);

        $result = json_decode($result);

        if($result->tiketux->status == "OK"){
            $sql = "UPDATE tbl_voucher SET isOnline = 1 WHERE KodeVoucher = '$kode_voucher'";
            $db->sql_query($sql);
            echo(1);
        }else{
            echo($result->tiketux->error);
        }
    exit;
}

//Mengambil data user
$sql	= 
	"SELECT user_id,nama FROM tbl_user ORDER BY user_id";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_user = array();

while($row = $db->sql_fetchrow($result)){
	$data_user[$row['user_id']]	= $row['nama'];
}

	
$cari	= $HTTP_POST_VARS["txt_cari"];

$kondisi_cari	= $cari==""?"":" AND (KodeVoucher LIKE '%$cari%'
														OR tr.Nama LIKE '%$cari%'
														OR tr.telp LIKE '%$cari%'
														OR tv.NoTiket LIKE '%$cari%'
														OR tv.Keterangan LIKE '%$cari%'
														OR tv.KodeJadwal LIKE '%$cari%')";

if($kota!=""){
	$kondisi_cabang	.= " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tv.IdJurusan))='$kota'";
}

if($fil_status!=""){
	switch($fil_status){
		case "0"	:
			//belum terpakai
			$kondisi_status	= " AND (tv.NoTiket IS NULL AND ExpiredDate>DATE(NOW())) ";
			break;
		case "1"	:
			//terpakai
			$kondisi_status	= " AND tv.NoTiket IS NOT NULL ";
			break;
		case "2"	:
			//expired
			$kondisi_status	= " AND (tv.NoTiket IS NULL AND ExpiredDate<=DATE(NOW())) ";
			break;
	}
}


$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$tbl_reservasi	= $is_today?"tbl_reservasi":"tbl_reservasi";

$kondisi_tanggal	= " AND (".($is_tgl_digunakan==0?"DATE(WaktuCetak)":"DATE(WaktuDigunakan)")." BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')";

$kondisi	= $kondisi_cari.$kondisi_cabang.$kondisi_status.$kondisi_tanggal;
	
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$order	=($order=='')?"ASC":$order;
$sort_by =($sort_by=='')?($is_tgl_digunakan==0?"tv.WaktuCetak":"tv.WaktuDigunakan"):$sort_by;

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeVoucher",
		"(tbl_voucher tv LEFT JOIN $tbl_reservasi tr ON tv.NoTiket=tr.NoTiket)",
		"&kota=$kota&status=$fil_status&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
		"WHERE 1 $kondisi","laporan_voucher.php",
		$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT
		tv.*,tr.Nama,tr.Telp,
		IF(tv.NoTiket IS NULL,IF(ExpiredDate>DATE(NOW()),0,2),1) AS Status
	FROM (tbl_voucher tv LEFT JOIN $tbl_reservasi tr ON tv.NoTiket=tr.NoTiket)
	WHERE 1 $kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=0;


//PLOT DATA
while($row = $db->sql_fetchrow($result)){
	
	$i++;
	
	switch($row['Status']){
		case 0:
			//belum terpakai
			$odd = $i % 2==0?'odd':'even';
			$status	= "Open";
			break;
		
		case 1:
			//terpakai
			$odd	= "green";
			$status	= "TERPAKAI";
			break;
		
		case 2:
			$odd	= "red";
			$status	= "EXPIRED";
			break;
		
	}
	
	$jenis_voucher	= $row['IsHargaTetap']?"Harga ":"Diskon ";
	if($row['NilaiVoucher'] < 1 && $row['NilaiVoucher'] > 0){
		$nilai = ($row['NilaiVoucher']*100).'%';
	}else{
		$nilai = number_format($row['NilaiVoucher'],0,',','.');
	}
	$keterangan			= (!$row['IsReturn'])?"":"Voucher Return<br>$row[CabangBerangkat] - $row[CabangTujuan]<br>Tiket Berangkat <br> $row[NoTiketBerangkat]";
	$keterangan			.=$row['IsBolehWeekEnd']?"":"Tidak dapat digunakan pada saat week end";

    if($row['isOnline'] == 0 && $row['IsReturn'] == 1){
        $online = "<font color='#8b0000'><b>Offline</b></font>";
    }elseif($row['isOnline'] == 0 && $row['IsReturn'] == 0){
        if($row['Status'] != 0){
            $online = "<font color='#8b0000'><b>---</b></font>";
        }else{
            $online = "<a onclick='setOnline(\"$row[KodeVoucher]\")'><font color='#00008b'><b>Set Online</b></font></a>";
        }
    }else{
        $online = "<font color='#006400'><b>Online</b></font>";
    }

	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'kode_voucher'=>$row['KodeVoucher'],
				'waktu_buat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetak'])),
				'dibuat_oleh'=>$data_user[$row['PetugasPencetak']],
				'waktu_pakai'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuDigunakan'])),
				'no_tiket'=>$row['NoTiket'],
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'telp'=>$row['Telp'],
				'nilai'=>$jenis_voucher.' '.$nilai,
				'cso'=>$data_user[$row['PetugasPengguna']],
				'mulai_berlaku'=>dateparse(FormatMySQLDateToTgl($row['BerlakuMulai'])),
				'expired'=>dateparse(FormatMySQLDateToTgl($row['ExpiredDate'])),
				'keterangan'=>"<b>".$row['Keterangan']."</b><br>".$keterangan,
				'status'=>$status,
                'online'=>$online,
                'hargajual'=>number_format($row['HargaJual'],0,',','.'),
			)
		);
		
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota."&status=".$fil_status."&istgldigunakan=".$is_tgl_digunakan."&cari=".$cari.
										"&sort_by=".$sort_by."&order=".$order."";
													
$script_cetak_excel="Start('laporan_voucher_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&status=$fil_status&asal=$asal&tujuan=$tujuan&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

$temp_var		= "opt_status".$fil_status;
$$temp_var	= "selected";

$temp_var		= "is_tgl_digunakan".$is_tgl_digunakan;
$$temp_var	= "selected";

$sql	= 
	"SELECT
		COUNT(KodeVoucher) AS TotalVoucher,
		SUM(IF(tv.NoTiket IS NULL AND ExpiredDate>DATE(NOW()),1,0)) AS TotalBelumTerpakai,
		SUM(IF(tv.NoTiket IS NULL AND ExpiredDate <=DATE(NOW()),1,0)) AS TotalExpired,
		SUM(NilaiVoucher) AS TotalRupiahVoucher,
		SUM(IF(tv.NoTiket IS NULL AND ExpiredDate>DATE(NOW()),NilaiVoucher,0)) AS TotalRupiahBelumTerpakai,
		SUM(IF(tv.NoTiket IS NULL AND ExpiredDate <=DATE(NOW()),NIlaiVoucher,0)) AS TotalRupiahExpired
	FROM (tbl_voucher tv
		LEFT JOIN $tbl_reservasi tr ON tv.NoTiket=tr.NoTiket)
		WHERE 1 $kondisi";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_total = $db->sql_fetchrow($result);

//$kota	= $kota!=""?$kota:"JAKARTA";

$template->set_filenames(array('body' => 'laporan.voucher/index.tpl')); 

$page_title	= "Laporan Voucher";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_voucher.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'ISTGLDIGUNAKAN0'=>$is_tgl_digunakan0,
	'ISTGLDIGUNAKAN1'=>$is_tgl_digunakan1,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> setComboKota($kota),
	'OPT_ASAL'			=> $Cabang->setInterfaceComboCabangByKota(),
	'TGL_BERLAKU'		=> dateD_M_Y(),
	'TGL_EXPIRED'		=> dateD_M_Y(),
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'STATUS'				=> $opt_status,
	'STATUS0'				=> $opt_status0,
	'STATUS1'				=> $opt_status1,
	'STATUS2'				=> $opt_status2,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=KodeVoucher'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan kode voucher($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=WaktuCetak'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Waktu cetak($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=WaktuDigunakan'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan waktu penggunaan ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=NoTiket'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan nomor tiket($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan kode jadwal($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan nama penumpang($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=Telp'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan nomor telepon($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=NilaiVoucher'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan nilai voucher($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=ExpiredDate'.$parameter_sorting),
	'TIPS_SORT_9'	=> "Urutkan expired date ($order_invert)",
	'A_SORT_10'			=> append_sid('laporan_voucher.'.$phpEx.'?sort_by=Status'.$parameter_sorting),
	'TIPS_SORT_10'	=> "Urutkan status ($order_invert)",
	'TOTAL_VOUCHER'	=> number_format($data_total['TotalVoucher'],0,",","."),
	'TOTAL_BELUM_DIGUNAKAN'=> number_format($data_total['TotalBelumTerpakai'],0,",","."),
	'TOTAL_DIGUNAKAN'=> number_format($data_total['TotalVoucher']-$data_total['TotalBelumTerpakai']-$data_total['TotalExpired'],0,",","."),
	'TOTAL_EXPIRED'	=> number_format($data_total['TotalExpired'],0,",","."),
	'TOTAL_RUPIAH_VOUCHER'	=> "Rp. ".number_format($data_total['TotalRupiahVoucher'],0,",","."),
	'TOTAL_RUPIAH_BELUM_DIGUNAKAN'=> "Rp. ".number_format($data_total['TotalRupiahBelumTerpakai'],0,",","."),
	'TOTAL_RUPIAH_DIGUNAKAN'=> "Rp. ".number_format($data_total['TotalRupiahVoucher']-$data_total['TotalRupiahBelumTerpakai']-$data_total['TotalRupiahExpired'],0,",","."),
	'TOTAL_RUPIAH_EXPIRED'	=> "Rp. ".number_format($data_total['TotalRupiahExpired'],0,",",".")
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>