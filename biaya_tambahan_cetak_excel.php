<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"],$USER_LEVEL_INDEX["CSO"],$USER_LEVEL_INDEX["CSO_PAKET"]))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

$Cabang =new Cabang();

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$cari  					= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$kodecabang				= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (tbl_biaya_tambahan.KodeCabang LIKE '$cari%'
		OR NamaPencetak LIKE '%$cari%'
		OR NamaPenerima LIKE '%$cari%'
		OR NamaPembuat LIKE '$cari%'
		OR JenisBiaya LIKE '$cari%'
		OR Keterangan LIKE '$cari%'
		OR Jumlah LIKE '$cari%'
		OR NamaReleaser LIKE '%$cari%')";
	
$order	=($order=='')?"ASC":$order;

$sort_by =($sort_by=='')?"NamaPenerima":$sort_by;

//MENGAMBIL DATA-DATA MASTER CABANG
$sql=
	"SELECT tbl_biaya_tambahan.*, tbl_md_kendaraan.NoPolisi, tbl_md_cabang.Nama,
		GROUP_CONCAT(tbl_biaya_tambahan.id_biaya_tambahan ORDER BY id_biaya_tambahan ASC) AS id_biaya,
		GROUP_CONCAT(tbl_biaya_tambahan.JenisBiaya ORDER BY id_biaya_tambahan ASC) AS jenis,
		GROUP_CONCAT(tbl_biaya_tambahan.Jumlah ORDER BY id_biaya_tambahan) AS jumlahBiaya,
		GROUP_CONCAT(tbl_biaya_tambahan.Realisasi1 ORDER BY id_biaya_tambahan) AS Realisasi1,
	    GROUP_CONCAT(tbl_biaya_tambahan.Realisasi2 ORDER BY id_biaya_tambahan) AS Realisasi2,
	    GROUP_CONCAT(tbl_biaya_tambahan.Selisih ORDER BY id_biaya_tambahan) AS Selisih
	FROM tbl_biaya_tambahan
        JOIN tbl_md_kendaraan ON tbl_md_kendaraan.KodeKendaraan=tbl_biaya_tambahan.KodeKendaraan
        JOIN tbl_md_cabang ON tbl_md_cabang.KodeCabang = tbl_biaya_tambahan.KodeCabang
	$kondisi_cari AND DATE(TglBuat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND tbl_biaya_tambahan.KodeCabang = '$kodecabang'
	AND StatusCetak=1
	GROUP BY OTPCode,NamaPenerima";

if (!$result = $db->sql_query($sql)){
	die(mysql_error());
	echo("Err:".__LINE__);exit;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result)){

	$temp_array[$idx]['TglCetak']					= $row['TglCetak'];
	$temp_array[$idx]['NoPol']						= $row['NoPolisi'];
	$temp_array[$idx]['NamaPenerima']				= $row['NamaPenerima'];
	$temp_array[$idx]['KodeKendaraan']				= $row['KodeKendaraan'];
	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['KodeJurusan']				= $row['KodeJurusan'];
	$temp_array[$idx]['Keterangan']					= $row['Keterangan'];
	$temp_array[$idx]['jenis']						= $row['jenis'];
	$temp_array[$idx]['jumlah_biaya']				= $row['jumlahBiaya'];
	$temp_array[$idx]['Realisasi1']					= explode(",",$row['Realisasi1']);
	$temp_array[$idx]['Realisasi2']					= explode(",",$row['Realisasi2']);
	$temp_array[$idx]['Selisih']					= explode(",",$row['Selisih']);

	$temp_idx+=$idx;
	$idx++;
}

$a = 0;

for($a;$a <=$temp_idx;$a++){
	$jenis 		= explode(",",$temp_array[$a]['jenis']);
	$biaya 		= explode(",",$temp_array[$a]['jumlah_biaya']);
	foreach($jenis as $k=>$j){
		if($j == 'BBM'){
			$temp_array[$a]['BBM']								= $biaya[$k];

		}elseif($j == 'Tol'){
			$temp_array[$a]['Tol']								= $biaya[$k];

		}elseif($j == 'Service'){
			$temp_array[$a]['Service']								= $biaya[$k];

		}elseif($j == 'Storing'){
			$temp_array[$a]['Storing']								= $biaya[$k];

		}elseif($j == 'Tilang/Derek'){
			$temp_array[$a]['Tilang']								= $biaya[$k];

		}elseif($j == 'Order Fee'){
			$temp_array[$a]['Order']								= $biaya[$k];

		}elseif($j == 'Stay Fee'){
			$temp_array[$a]['Stay']								= $biaya[$k];

		}elseif($j == 'Stand By'){
			$temp_array[$a]['Stand']								= $biaya[$k];
		}
	}
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');

$objPHPExcel->getActiveSheet()->mergeCells('A4:A5');
$objPHPExcel->getActiveSheet()->mergeCells('B4:B5');
$objPHPExcel->getActiveSheet()->mergeCells('C4:C5');
$objPHPExcel->getActiveSheet()->mergeCells('D4:D5');
$objPHPExcel->getActiveSheet()->mergeCells('E4:G4');
$objPHPExcel->getActiveSheet()->mergeCells('H4:M4');
$objPHPExcel->getActiveSheet()->mergeCells('N4:N5');
$objPHPExcel->getActiveSheet()->mergeCells('O4:O5');
$objPHPExcel->getActiveSheet()->mergeCells('P4:P5');
$objPHPExcel->getActiveSheet()->mergeCells('Q4:Q5');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'REALISASI BIAYA OPERASIONAL '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A2', 'POOL DEPLU');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'No Polisi');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'ANALITICAL ACCOUNTING');
$objPHPExcel->getActiveSheet()->setCellValue('E5', 'NO BODY');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F5', 'COUNTER');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G5', 'ROUTE');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'BIAYA');
$objPHPExcel->getActiveSheet()->setCellValue('H5', 'TOLL');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I5', 'BBM');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J5', 'SERVICE');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K5', 'TILANG DEREK');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L5', 'STAND AND STAY');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M5', 'ORDER FEE');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N4', 'KETERANGAN');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
$objPHPExcel->getActiveSheet()->setCellValue('O4', 'REALISASI 1');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
$objPHPExcel->getActiveSheet()->setCellValue('P4', 'REALISASI 2');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
$objPHPExcel->getActiveSheet()->setCellValue('Q4', 'SELISIH');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);

$idx=0;
$idx_row=6;
$sum = 0;
while ($idx<count($temp_array)){
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['TglCetak']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['NoPol']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['NamaPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['KodeKendaraan']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['KodeJurusan']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, ($temp_array[$idx]['Tol'] ? $temp_array[$idx]['Tol'] : 0));
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, ($temp_array[$idx]['BBM'] ? $temp_array[$idx]['BBM'] : 0));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, ($temp_array[$idx]['Service'] ? $temp_array[$idx]['Service'] : 0));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, ($temp_array[$idx]['Tilang'] ? $temp_array[$idx]['Tilang'] : 0) + ($temp_array[$idx]['Storing'] ? $temp_array[$idx]['Storing'] : 0));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, (($temp_array[$idx]['Stand'] ? $temp_array[$idx]['Stand'] : 0) + ($temp_array[$idx]['Stay'] ? $temp_array[$idx]['Stay'] : 0)));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, ($temp_array[$idx]['Order'] ? $temp_array[$idx]['Order'] : 0));
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $temp_array[$idx]['Keterangan']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, array_sum($temp_array[$idx]['Realisasi1']));
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, array_sum($temp_array[$idx]['Realisasi2']));
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, array_sum($temp_array[$idx]['Selisih']));

	/*selisih diambil dari perhitungan langsung bukan dari tabel
	if(array_sum($temp_array[$idx]['Realisasi2']) != 0){
		$selisih = array_sum($biaya) - array_sum($temp_array[$idx]['Realisasi2']);
	}elseif(array_sum($temp_array[$idx]['Realisasi2']) == 0 && array_sum($temp_array[$idx]['Realisasi1']) == 0){
		$selisih = 0;
	}else{
		$selisih = array_sum($biaya) - array_sum ($temp_array[$idx]['Realisasi1']);
	}
	*/
	

	$sum += ($temp_array[$idx]['Tol'] ? $temp_array[$idx]['Tol'] : 0) + ($temp_array[$idx]['BBM'] ? $temp_array[$idx]['BBM'] : 0) + ($temp_array[$idx]['Service'] ? $temp_array[$idx]['Service'] : 0)
			+ ($temp_array[$idx]['Tilang'] ? $temp_array[$idx]['Tilang'] : 0) + ($temp_array[$idx]['Storing'] ? $temp_array[$idx]['Storing'] : 0) + ($temp_array[$idx]['Stand'] ? $temp_array[$idx]['Stand'] : 0)
			+ ($temp_array[$idx]['Stay'] ? $temp_array[$idx]['Stay'] : 0) + ($temp_array[$idx]['Order'] ? $temp_array[$idx]['Order'] : 0);
	$idx_row++;
	$idx++;
}
$temp_idx=$idx_row-1;

$idx_row++;

$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row,'=ROWS(E6:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H6:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I6:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J6:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K6:K'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, '=SUM(L6:L'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, '=SUM(M6:M'.$temp_idx.')');

$objPHPExcel->getActiveSheet()->setCellValue('C'.($idx_row+3),'MENYERAHKAN');
$objPHPExcel->getActiveSheet()->setCellValue('C'.($idx_row+6),'RIZKY');
$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+2),'Total');
$objPHPExcel->getActiveSheet()->setCellValue('H'.($idx_row+2),$sum);

$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+3),'MENERIMA');
$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+6),'AYU');
$objPHPExcel->getActiveSheet()->setCellValue('M'.($idx_row+3),'MENGETAHUI');
$objPHPExcel->getActiveSheet()->setCellValue('M'.($idx_row+6),'REZA');

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

for($col = 'A'; $col !== 'R'; $col++) {
	$objPHPExcel->getActiveSheet()->getStyle($col.'4:'.$col.$idx_row)->applyFromArray($styleArray);
}

$gaya = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	),
	'font'  => array(
		'bold'  => false,
		'color' => array('rgb' => '000'),
		'size'  => 12,
		'name'  => 'Arial'
	),
);

for($col = 'A'; $col !== 'R'; $col++) {
	$objPHPExcel->getActiveSheet()->getStyle($col.'4:'.$col.'5')->applyFromArray($gaya);
}

$style = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
	'font'  => array(
		'bold'  => false,
		'color' => array('rgb' => '000'),
		'size'  => 12,
		'name'  => 'Arial'
	),
);

$objPHPExcel->getDefaultStyle()->applyFromArray($style);
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Realisasi Biaya Operasional Tgl '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
}

?>
