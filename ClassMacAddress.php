<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  //redirect('index.'.$phpEx,true);
	exit;
}
//#############################################################################

class MacAddress{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function MacAddress(){
		$this->ID_FILE="C-MAD";
	}
	
	//BODY
	
	function periksaDuplikasi($mac_address){

		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS jumlah_data FROM tbl_mac_address WHERE MacAddress='$mac_address'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);

    $ditemukan = ($row[0]<=0)?false:true;
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah($mac_address,$kode_cabang,$nama_komputer,$is_aktif=1){

		//kamus
		global $db;
		global $userdata;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
				"INSERT INTO tbl_mac_address(MacAddress,KodeCabang,NamaKomputer,AddBy,AddByName,WaktuTambah,IsAktif)
				VALUES('$mac_address','$kode_cabang','$nama_komputer',$userdata[user_id],'$userdata[username]',NOW(),$is_aktif);";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah($id,$mac_address,$kode_cabang,$nama_komputer,$is_aktif,$allow_cso){
		
		//kamus
		global $db;
		
		//MENGUBAH DATA DI DATABASE
		$sql =
			"UPDATE tbl_mac_address 
			SET MacAddress='$mac_address',KodeCabang='$kode_cabang',NamaKomputer='$nama_komputer',
			  IsAktif='$is_aktif',AllowCSO='$allow_cso'
			WHERE Id=$id;";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list){
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_mac_address
			WHERE Id IN($list);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($id){

		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_mac_address
			WHERE Id=$id;";
		
		if (!$result = $db->sql_query($sql,TRUE)){
			die_error("Err:$this->ID_FILE ".__LINE__);
		}

		$row=$db->sql_fetchrow($result);
		return $row;
		
	}//  END ambilDataDetail

}
?>