<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KASIR"],$USER_LEVEL_INDEX["CSO"],$USER_LEVEL_INDEX["CSO_PAKET"]))){
    die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
$id = isset($HTTP_GET_VARS['id'])? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];
$tglprint       = date('Y-m-d H:i:s');
$namapencetak   = $userdata['nama'];
$idpencetak     = $userdata['user_id'];

function cekDuplikasiOTP($otp){
    global $db;
    $sql = "SELECT COUNT(OTPCode) AS jumlah_data FROM tbl_biaya_tambahan WHERE OTPCode = '$otp'";

    if ($result = $db->sql_query($sql)){
        $row = $db->sql_fetchrow($result);
    }
    else{
        die_error(mysql_error());
    }

    return $row['jumlah_data'];
}
//ambil data

$sql = "SELECT tbl_biaya_tambahan.*, pembuat.nama as pembuat, releaser.nama as releaser,
            f_cabang_get_name_by_kode(KodeCabangAsal) as asal, 
            f_cabang_get_name_by_kode(KodeCabangTujuan) as tujuan,
            GROUP_CONCAT(tbl_biaya_tambahan.JenisBiaya ORDER BY id_biaya_tambahan) AS jenis,
	        GROUP_CONCAT(tbl_biaya_tambahan.Jumlah ORDER BY id_biaya_tambahan) AS jumlahBiaya
        FROM tbl_biaya_tambahan
        JOIN tbl_user as pembuat ON pembuat.user_id = tbl_biaya_tambahan.IdPembuat
        LEFT JOIN tbl_user as releaser ON releaser.user_id = tbl_biaya_tambahan.IdReleaser
        JOIN tbl_md_jurusan ON tbl_md_jurusan.KodeJurusan = tbl_biaya_tambahan.KodeJurusan
        WHERE OTPCode IN ($id)
        GROUP BY tbl_biaya_tambahan.TglBuat";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

                $KodeCabang=$row['KodeCabang'];
                $Pembuat   =$row['pembuat'];
                $Releaser  =$row['releaser'];
                $Penerima  =$row['NamaPenerima'];
                $JenisBiaya = explode(",",$row['jenis']);
                $Keterangan =$row['Keterangan'];
                $Jml        =explode(",",$row['jumlahBiaya']);
                $TglBuat    =date_format(date_create($row['TglBuat']),'d-m-Y H:i:s');
                $jurusan    = $row['asal'].' - '.$row['tujuan'];
$mark = "";
if($row['StatusCetak'] == 1){
        $mark = '<tr>
                <td colspan="2" align="center"><B>>>DUPLIKAT<<</B></td>
                </tr>
                <tr>
                    <td colspan="2"><hr></td>
                </tr>';
}

function generateString($length)
{
    $charset = "0123456789";
    $key ="";
    for($i=0; $i<$length; $i++)
    {
        $key .= $charset[(mt_rand(0,(strlen($charset)-1)))];
    }

    return $key;
}

do{
    $OTP        = generateString(7);
    //JIKA OTP BELUM PERNAH DIGUNAKAN MAKA KELUAR DARI LOOPING
    if(cekDuplikasiOTP($OTP) == 0){
        $i=2;
    }
}while($i<2);
//update pencetak DAN KODE OTP AGAR TIDAK DIGUNAKAN LAGI
$sql ="UPDATE tbl_biaya_tambahan SET TglCetak='$tglprint', IdPencetak='$idpencetak', NamaPencetak='$namapencetak', StatusCetak=1, OTPCode='$OTP'  WHERE OTPCode IN ($id)";
if (!$db->sql_query($sql)){
    die(mysql_error());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cetak Biaya Tambahan</title>
    <style type="text/css">
        body {margin: 0; padding: 0; font-family: 'Calibri'; font-size: 16px;}
        html {margin: 0; padding: 0;}
        hr {border:1px dashed #ccc; margin: 2px 0}
        .text-lowercase {text-transform: lowercase;}
    </style>
    <script type="text/javascript">
        function printWindow() {
            bV = parseInt(navigator.appVersion);
            if (bV >= 4) window.print();
            window.opener.refresh_page();
        }
    </script>
</head>
<body width="200px">
        <table cellpadding="0" width="200px">
            <tr>
                <td colspan="2"><hr></td>
            </tr>
            <tr>
                <td colspan="2" align="center">DAYTRANS</td>
            </tr>
            <tr>
                <td colspan="2" align="center">Biaya Tambahan</td>
            </tr>
            <!-- ./header -->
            <tr>
                <td colspan="2">==========================</td>
            </tr>
            <?php echo $mark;?>
            <tr>
                <td>Releaser</td>
                <td align="right"><?php echo $Releaser;?></td>
            </tr>
            <tr>
                <td>Tanggal Buat</td>
                <td align="right"><?php echo $TglBuat;?></td>
            </tr>

            <tr>
                <td>Pembuat</td>
                <td align="right"><?php echo $Pembuat;?></td>
            </tr>

            <tr>
                <td>Pecetak</td>
                <td align="right"><?php echo $namapencetak;?></td>
            </tr>
            <tr>
                <td>Penerima</td>
                <td align="right"><?php echo $Penerima;?></td>
            </tr>
            <tr>
                <td>Kode Cabang</td>
                <td align="right"><?php echo $KodeCabang;?></td>
            </tr>
            <tr>
                <td>Jurusan</td>
                <td align="right"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo $jurusan; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">==========================</td>
            </tr>
            <tr>
            <?php
            foreach($JenisBiaya as $k=>$JB){
                echo "<tr>";
                echo "<td>".$JB."</td>";
                echo "<td align='right'>".number_format($Jml[$k],0,',','.')."</td>";
                echo "</tr>";

                $total += $Jml[$k];
            }
            ?>
            <tr>
                <td>Total</td>
                <td align="right"><?php echo number_format($total,0,',','.');?></td>
            </tr>
            <tr>
                <td>Keterangan</td>
                <td align="right"><?php echo $Keterangan;?></td>
            </tr>
            <tr>
                <td colspan="2">==========================</td>
            </tr>
            <tr>
                <td>Tanggal Cetak</td>
                <td align="right"><?php echo $tglprint;?></td>
            </tr>
            <tr>
                <td colspan="2">==========================</td>
            </tr>
            <tr>
                <td colspan="2" align="center">Tanda Tangan Bukti</td>
            </tr>
            <tr>
                <td height="75" valign="top">Pembuat</td>
                <td height="75" valign="top" align="right">Penerima</td>
            </tr>
            <tr>
                <td><?php echo $Pembuat;?></td>
                <td align="right"><?php echo $Penerima;?></td>
            </tr>
        </table>
       <br><br>
</body>
<script type="text/javascript">
    printWindow();
    window.close();
</script>
</html>