<?php
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//require_once dirname(__FILE__) . '/phpGrid_Lite/conf.php';

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$cabang = $HTTP_GET_VARS['cabang'];
$awal   = date('Y-m-d', strtotime($HTTP_GET_VARS['awal']));
$akhir  = date('Y-m-d', strtotime($HTTP_GET_VARS['akhir']));

$periode = $awal.' - '.$akhir;

$objPHPExcel = new PHPExcel();
$objPHPExcel->createSheet();

//HEADER
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("HEADER");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DATE');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'TYPE');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'SOURCE_DOCUMENT');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'REFERRENCE');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'CURRENCY_ID');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

//DISTRIBUTION
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle("DISTRIBUTION");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DATE');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'KREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DISTRIBUTION_REFERRENCE');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SEQUENCE_LINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

//DIMENTION
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()->setTitle("DIMENTION");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'KREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ASSIGN_ID');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TRX_DIMENSION_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DISTRIBUTION_REFERENCE');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SEQUENCE_LINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

//DIMENTIONCODE
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(3);
$objPHPExcel->getActiveSheet()->setTitle("DIMENTIONCODE");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'KREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ASSIGN_ID');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TRX_DIMENSION_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DISTRIBUTION_REFERENCE');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SEQUENCE_LINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

$i              = 1;
$cellNumber     = 2;
$cellNumber2    = 2;
$cellNumber3    = 2;
$cellNumber4    = 2;
$lastGLNumber   = 1;
$type           = 0;
$sourceDocument = 'IMGJ';
$currency       = 'IDR';
$trxDimension   = 'BODY';
$seqLine        = 100;
$dataArray      = array();
$arrayType      = array('TIKET','PAKET');
$arrayDimensionCode = array('BODY','COUNTER','ROUTE');

$sql            = "SELECT * FROM tbl_md_cabang WHERE KodeCabang = '$cabang'";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

while($row = $db->sql_fetchrow($result)){
    $listCOA['TIKET']['DEBIT'] = $row['COADisposisiSalesTiketGross'];
    $listCOA['TIKET']['KREDIT'] = $row['COASalesTiketGross'];
    $listCOA['PAKET']['DEBIT']  = $row['COADisposisiSalesPaketGross'];
    $listCOA['PAKET']['KREDIT'] = $row['COASalesPaketGross'];
}

//Penjualan tiket
$sql            = "SELECT TglBerangkat, SUM(SubTotal) as TotalPenjualan FROM tbl_reservasi WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) = '$cabang' AND (TglBerangkat BETWEEN '$awal' AND '$akhir') AND CetakTiket = 1 AND FlagBatal = 0 GROUP BY TglBerangkat";

/*if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}*/

$dataPenjualan = array();

while($row = $db->sql_fetchrow($result)){
    $dataPenjualan[$row['TglBerangkat']]['TIKET']['TOTAL'] = $row['TotalPenjualan'];
}

//penjualan paket
$sql            = "SELECT TglBerangkat, SUM(HargaPaket) as TotalPenjualan FROM tbl_paket WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) = '$cabang' AND (TglBerangkat BETWEEN '$awal' AND '$akhir') AND CetakTiket = 1 AND FlagBatal = 0 GROUP BY TglBerangkat";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

while($row = $db->sql_fetchrow($result)){
    $dataPenjualan[$row['TglBerangkat']]['PAKET']['TOTAL'] = $row['TotalPenjualan'];
}

//ambil data jadwal per tanggal
$sql            = "SELECT TglBerangkat, KodeJadwal FROM tbl_reservasi WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) = '$cabang' AND (TglBerangkat BETWEEN '$awal' AND '$akhir') AND CetakTiket = 1 AND FlagBatal = 0 GROUP BY CONCAT(TglBerangkat,KodeJadwal) ORDER BY TglBerangkat,KodeJadwal";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

$dataJadwal = array();

while($row = $db->sql_fetchrow($result)){
    $dataJadwal[$row['TglBerangkat']][] = $row['KodeJadwal'];
}

//penjualan tiket per jadwal
$sql            = "SELECT TglBerangkat, KodeJadwal,KodeKendaraan,f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) as Nopol, SUM(SubTotal) as TotalPenjualan FROM tbl_reservasi WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) = '$cabang' AND (TglBerangkat BETWEEN '$awal' AND '$akhir') AND CetakTiket = 1 AND FlagBatal = 0 GROUP BY CONCAT(TglBerangkat,KodeJadwal) ORDER BY TglBerangkat,KodeJadwal";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

while($row = $db->sql_fetchrow($result)){
    $dataPenjualan[$row['TglBerangkat']]['TIKET']['PERJADWAL'][$row['KodeJadwal']]['BODY'] = $row['KodeKendaraan'];
    $dataPenjualan[$row['TglBerangkat']]['TIKET']['PERJADWAL'][$row['KodeJadwal']]['NOPOL'] = $row['Nopol'];
    $dataPenjualan[$row['TglBerangkat']]['TIKET']['PERJADWAL'][$row['KodeJadwal']]['TOTAL'] = number_format($row['TotalPenjualan'],2,'.','');
}

//penjualan paket per jadwal
$sql            = "SELECT TglBerangkat,KodeJadwal,KodeKendaraan,f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) as Nopol, SUM(HargaPaket) as TotalPenjualan FROM tbl_paket WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) = '$cabang' AND (TglBerangkat BETWEEN '$awal' AND '$akhir') AND CetakTiket = 1 AND FlagBatal = 0 GROUP BY CONCAT(TglBerangkat,KodeJadwal) ORDER BY TglBerangkat,KodeJadwal";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

while($row = $db->sql_fetchrow($result)){
    $dataPenjualan[$row['TglBerangkat']]['PAKET']['PERJADWAL'][$row['KodeJadwal']]['BODY'] = $row['KodeKendaraan'];
    $dataPenjualan[$row['TglBerangkat']]['PAKET']['PERJADWAL'][$row['KodeJadwal']]['NOPOL'] = $row['Nopol'];
    $dataPenjualan[$row['TglBerangkat']]['PAKET']['PERJADWAL'][$row['KodeJadwal']]['TOTAL'] = number_format($row['TotalPenjualan'],2,'.','');
}

//echo json_encode($dataJadwal);

while($awal <= $akhir){
    foreach($arrayType as $type){
        $referrence = 'Penjl.'.$type.' C-'.$cabang.'/'.date('d/m/y', strtotime($awal));
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber, $cabang.'-'.$type.date('dm',strtotime($awal)));
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber, $i);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber, $lastGLNumber);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber, $i);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber, date('m/d/y',strtotime($awal)));
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber, 0);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber, $sourceDocument);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber, $referrence);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber, $currency);

        //DISTRIBUTION
        foreach($listCOA[$type] as $key => $coa){
            $objPHPExcel->setActiveSheetIndex(1);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber2, $cabang.'-'.$type.date('dm',strtotime($awal)));
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber2, $i);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber2, $lastGLNumber);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber2, $i);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber2, date('m/d/y',strtotime($awal)));
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber2, $coa);
            switch($key){
                case 'DEBIT':
                    $debit  = ($dataPenjualan[$awal][$type]['TOTAL'])?$dataPenjualan[$awal][$type]['TOTAL']:0;
                    $kredit = 0;
                    break;
                case 'KREDIT':
                    $debit  = 0;
                    $kredit = ($dataPenjualan[$awal][$type]['TOTAL'])?$dataPenjualan[$awal][$type]['TOTAL']:0;
                    break;
            }
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber2, $debit);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber2, $kredit);

            //DIMENTION
            if($key == 'KREDIT'){
                if($dataJadwal[$awal]){
                    $assignId2      = 1;
                    foreach ($arrayDimensionCode as $dimensionCodeType) {
                        $assignId       = 1;
                        foreach ($dataJadwal[$awal] as $jadwal) {
                            if($dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL'] > 0){
                                switch($dimensionCodeType){
                                    case 'BODY'     :
                                        $trxDimensionAlphaNumeric   = $dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['BODY'];
                                        break;
                                    case 'COUNTER'  :
                                        $trxDimensionAlphaNumeric   = $cabang;
                                        break;
                                    case 'ROUTE'    :
                                        $trxDimensionAlphaNumeric   = substr($jadwal,0,7);
                                        break;

                                }

                                if($dimensionCodeType == 'BODY'){
                                    $referrence = 'Penjl.'.$type.' '.$dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['NOPOL'].'/'.substr($jadwal,0,7).'/'.$cabang;
                                    $objPHPExcel->setActiveSheetIndex(2);
                                    $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber3, $i);
                                    $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber3, $lastGLNumber);
                                    $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber3, $i);
                                    $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber3, $coa);
                                    $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber3, $debit);
                                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber3, $kredit);
                                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber3, $assignId);
                                    $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber3, $dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']?$dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']:0);
                                    $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber3, $referrence);
                                    $objPHPExcel->getActiveSheet()->setCellValue('J'.$cellNumber3, $seqLine);

                                    $cellNumber3++;
                                    $assignId++;
                                }


                                $referrence = 'Penjl.'.$type.' '.$dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['NOPOL'].'/'.substr($jadwal,0,7).'/'.$cabang;
                                $objPHPExcel->setActiveSheetIndex(3);
                                $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber4, $i);
                                $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber4, $lastGLNumber);
                                $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber4, $i);
                                $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber4, $coa);
                                $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber4, $debit);
                                $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber4, $kredit);
                                $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber4, $assignId2);
                                $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber4, $dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']?$dataPenjualan[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']:0);
                                $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber4, $referrence);
                                $objPHPExcel->getActiveSheet()->setCellValue('J'.$cellNumber4, $seqLine);

                                $cellNumber4++;
                                $assignId2++;
                            }
                        }
                    }
                }

            }

            $cellNumber2++;
            $seqLine= $seqLine+100;
        }

        $i++;
        $cellNumber++;
    }
    $awal = date ("Y-m-d", strtotime("+1 day", strtotime($awal)));
}
ob_end_clean();
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Export Untuk GP Penjualan Cabang '.$cabang.' Periode '.$periode.'.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');