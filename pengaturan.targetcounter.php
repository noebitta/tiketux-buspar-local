<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassTargetCabang.php');

// SESSION
$id_page = 711;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= isset($HTTP_GET_VARS['start'])? intval($HTTP_GET_VARS['start']) : 0;
$bulan          = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun          = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];
$cari   		= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$page		    = isset($HTTP_GET_VARS['page'])? $HTTP_GET_VARS['page'] : $HTTP_POST_VARS['page'];

$TargetCabang = new TargetCabang();
$cabang       = new Cabang();

$mode           = $mode==""?"exp":$mode;
$bulan          = $bulan==""?date("m"):$bulan;
$bulan          = (int)$bulan;
$tahun          = $tahun==""?date("Y"):$tahun;
$page           = ($page!='')?$page:0;

switch($mode){
    case 'reset':
        /*ACTION RESET TARGET PER BULAN*/
        $TargetCabang->resetTarget($bulan,$tahun,'RESETED: '.date("H:i:s d-m-Y").' BY: '.$userdata['nama']);
        exit;
    case 'simpan':
        /*ACTION SIMPAN PENGUBAHAN TARGET*/

        $i=$page*$VIEW_PER_PAGE;
        do{
            $i++;
            $KodeCabang             = $HTTP_POST_VARS["KodeCabang$i"];
            $target_beforepax       = $HTTP_POST_VARS["targetbeforepax$i"];
            $target_inputpax        = $HTTP_POST_VARS["targetinputpax$i"];
            $target_beforepaket     = $HTTP_POST_VARS["targetbeforepaket$i"];
            $target_inputpaket      = $HTTP_POST_VARS["targetinputpaket$i"];

            if($KodeCabang!="" && ($target_beforepax!=$target_inputpax || $target_beforepaket!= $target_inputpaket)){
                /*JIKA ADA PERUBAHAN DATA , MAKA AKAN DISIMPAN*/
                $TargetCabang->UbahTarget($bulan,$tahun,$KodeCabang,$target_inputpax,$target_inputpaket,'UPDATE: '.date("H:i:s d-m-Y").' BY: '.$userdata['nama']);
              }

        }while($i<($page+1)*$VIEW_PER_PAGE && $KodeCabang!="");
    case 'exp':
        /*VIEW MODE*/

        $kondisi	=($cari=="")?"":" AND (KodeCabang LIKE '%$cari%' OR Nama LIKE '%$cari' OR Alamat LIKE '%$cari' OR Kota LIKE '%$cari' OR Telp LIKE '%$cari%')";


        /*MENGAMBIL DATA TARGET CABANG DARI TABEL TARGET*/
        $sql ="SELECT KodeCabang,TargetPax,TargetPaxTercapai,TargetPaket,TargetPaketTercapai,Log
                FROM tbl_md_cabang_target
                WHERE BulanTarget='$bulan' AND TahunTarget='$tahun'
                ORDER BY KodeCabang ASC";

        if (!$result = $db->sql_query($sql)){
            //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
            echo("Error:".__LINE__);exit;
        }

        while($row=$db->sql_fetchrow($result)){
            $data_target[$row['KodeCabang']]['TargetPax']          = $row['TargetPax'];
            $data_target[$row['KodeCabang']]['TargetPaxTercapai']  = $row['TargetPaxTercapai'];
            $data_target[$row['KodeCabang']]['TargetPaket']        = $row['TargetPaket'];
            $data_target[$row['KodeCabang']]['TargetPaketTercapai']= $row['TargetPaketTercapai'];
            $data_target[$row['KodeCabang']]['Log']                = $row['Log'];
        }

        /*MENGAMBIL JUMLAH TIKET YANG DIBELI COUNTER DIJAKARTA */
        $sql="SELECT tbl_reservasi.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta, Kota
                FROM tbl_reservasi
                JOIN tbl_md_cabang ON tbl_reservasi.KodeCabang = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = $bulan AND YEAR(TglBerangkat) = $tahun AND CetakTiket = 1
                AND FlagBatal != 1 AND Kota = 'JAKARTA'
                GROUP BY tbl_reservasi.KodeCabang";

        if (!$result = $db->sql_query($sql)){
            //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
            echo("Error:".mysql_error());exit;
        }
        while($row=$db->sql_fetchrow($result)){
            $tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'] = $row['JmlFromJakarta'];
        }

        /*MENGAMBIL JUMLAH PAKET DARI COUNTER JAKARTA*/
        $sql="SELECT tbl_paket.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta, Kota
                FROM tbl_paket
                JOIN tbl_md_cabang ON tbl_paket.KodeCabang = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = $bulan AND YEAR(TglBerangkat) = $tahun AND CetakTiket = 1
                AND FlagBatal != 1 AND Kota = 'JAKARTA'
                GROUP BY tbl_paket.KodeCabang";

        if (!$result = $db->sql_query($sql)){
            //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
            echo("Error:".mysql_error());exit;
        }
        while($row=$db->sql_fetchrow($result)){
            $paket_from_jakarta[$row['KodeCabang']]['FromJakarta'] = $row['JmlFromJakarta'];
        }

        /*MENGAMBIL JUMLAH TIKET YANG TUJUANYA KE JAKARTA*/
        $sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan AS KodeCabang, Kota
                FROM tbl_reservasi
                JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
                JOIN tbl_md_cabang ON tbl_md_jurusan.KodeCabangTujuan = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = '$bulan' AND YEAR(TglBerangkat) = '$tahun'
                AND FlagBatal != 1 AND CetakTiket = 1
                AND Kota = 'JAKARTA'
                GROUP BY KodeCabangTujuan";
        if (!$result = $db->sql_query($sql)){
            //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
            echo("Error:".__LINE__);exit;
        }
        while($row=$db->sql_fetchrow($result)){
            $tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'] = $row['JmlFromBandungToJakarta'];
        }

        /*MENGAMBIL JUMLAH PAKET YANG TUJUANYA KE JAKARTA*/
        $sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan AS KodeCabang, Kota
                FROM tbl_paket
                JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
                JOIN tbl_md_cabang ON tbl_md_jurusan.KodeCabangTujuan = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = '$bulan' AND YEAR(TglBerangkat) = '$tahun'
                AND FlagBatal != 1 AND CetakTiket = 1
                AND Kota = 'JAKARTA'
                GROUP BY KodeCabangTujuan";
        if (!$result = $db->sql_query($sql)){
            //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
            echo("Error:".__LINE__);exit;
        }
        while($row=$db->sql_fetchrow($result)){
            $paket_to_jakarta[$row['KodeCabang']]['ToJakarta'] = $row['JmlFromBandungToJakarta'];
        }

        /*MENGAMBIL DATA CABANG */
        $sql ="SELECT *
               FROM tbl_md_cabang
               WHERE Kota = 'JAKARTA' $kondisi
               ORDER BY Nama ASC";

        if (!$result = $db->sql_query($sql)){
            //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
            echo("Error:".__LINE__);exit;
            //die(mysql_error());
        }

        $i = $idx_page*$VIEW_PER_PAGE+1;
        while ($row = $db->sql_fetchrow($result)){
            $odd ='odd';

            if (($i % 2)==0){
                $odd = 'even';
            }
                if($data_target[$row['KodeCabang']]['TargetPax']){
                    $jakarta = ($tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'] == "")?0:$tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'];
                    $bandung = ($tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'] == "")?0:$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'];
                    if($jakarta == 0 && $bandung == 0){
                        $capaian = 0;
                    }else{
                        $capaian = (($jakarta+$bandung)/$data_target[$row['KodeCabang']]['TargetPax'])*100;
                        $capaian = round($capaian,2);
                    }
                }else{
                    $capaian = 0;
                }

                if($data_target[$row['KodeCabang']]['TargetPaket']){
                    if($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'] == 0){
                        $capaianpaket = 0;
                    }else{
                        $capaianpaket = ($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'])/$data_target[$row['KodeCabang']]['TargetPaket']*100;
                        $capaianpaket = round($capaianpaket,2);
                    }
                }else{
                    $capaianpaket = 0;
                }

            $template->
            assign_block_vars(
                'ROW',
                array(
                    'odd'         =>$odd,
                    'check'       =>$check,
                    'no'          =>$i,
                    'KodeCabang'  =>$row['KodeCabang'],
                    'Cabang'      =>$row['Nama'],
                    'Kota'        =>$row['Kota'],
                    'targetpax'   =>($data_target[$row['KodeCabang']]['TargetPax']!=""?$data_target[$row['KodeCabang']]['TargetPax']:0),
                    'realpax'     =>number_format($tiket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'],0,",","."),
                    'capaianpax'  =>$capaian,
                    'targetpaket' =>($data_target[$row['KodeCabang']]['TargetPaket']!=""?$data_target[$row['KodeCabang']]['TargetPaket']:0),
                    'realpaket'   =>number_format($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'],0,",","."),
                    'capaianpaket'=>$capaianpaket,
                )
            );

            $i++;
        }

        if($i-1<=0){
            $template->assign_block_vars("NO_DATA",array());
        }

        //paramter sorting
        $order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
        $parameter_sorting	= "&page=$page&cari=$cari&order=$order_invert";

        /*LOOP TAHUN PENCARIAN*/
        $thn_now  = date("Y");
        for($thn=$thn_now-2;$thn<=$thn_now+1;$thn++){
            $template->assign_block_vars(
                "TAHUN",array(
                "value"     => $thn
            ));
        }

        $parameter_cetak = "&bulan=".$bulan."&tahun=".$tahun;
        $script_excel = "Start('pengaturan.targetcounter_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
        $page_title = "Target User";
        $template->set_filenames(array('body' => 'pengaturan.targetcabang/index.tpl'));

        $template->assign_vars(array(
                'BCRUMP'    		=>setBcrump($id_page),
                'ACTION_CARI'		=> append_sid('pengaturan.targetcounter.'.$phpEx),
                'ACTION_SAVE'		=> append_sid('pengaturan.targetcounter.'.$phpEx),
                'MODE'          => "simpan",
                'BULAN'         => $bulan,
                'TAHUN'         => $tahun,
                'PAGE'          => $page,
                'CARI'			    => $cari,
                'CETAK_XL'      => $script_excel
            )
        );

        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
}
?>