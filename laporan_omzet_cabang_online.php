<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 504;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

$Cabang =new Cabang();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$type  			= isset($HTTP_GET_VARS['berdasarkan'])? $HTTP_GET_VARS['berdasarkan'] : $HTTP_POST_VARS['berdasarkan'];
$username		= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_cabang_online/laporan_omzet_cabang_online_body.tpl'));

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari2	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

//kondisi_cari update
$kondisi_cari   =($cari=="")?"IS NOT NULL ":
        " IN (SELECT KodeCabang
                FROM tbl_md_cabang
                WHERE (KodeCabang LIKE '$cari%'
                OR Nama LIKE '%$cari%'
                OR Kota LIKE '%$cari%'
                OR Telp LIKE '$cari%'
                OR Alamat LIKE '%$cari%'))";


if($userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;


switch($type){
	case 1 :
		$date_filter = 'TglBerangkat';
		break;
	case 2 :
		$date_filter = 'WaktuCetakTiket';
		break;
	default :
		$date_filter = 'WaktuCetakTiket';
		break;
}

//MENGAMBIL DATA-DATA MASTER CABANG
$sql_cabang=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari2";

//echo $sql_cabang;
if (!$result_laporan = $db->sql_query($sql_cabang)){
	echo("Err:".__LINE__);exit;
}


//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND ($date_filter BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) IN (SELECT KodeCabang
		FROM tbl_md_cabang
		$kondisi_cari)
	GROUP BY KodeCabang
	ORDER BY KodeCabang";

//sementara
$sql=
        "SELECT
          f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
          (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
          -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
          +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
        FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
          AND (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
        WHERE
                f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan)
		$kondisi_cari
        GROUP BY KodeCabang
        ORDER BY KodeCabang";
//echo $sql;

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['KodeCabang']]	= $row['JadwalDibuka'];
}


//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(IF(PetugasPenjual=0 AND FlagBatal!=1,NoTiket,NULL)),0) AS TotalPenumpangO,
		IS_NULL(SUM(IF(PetugasPenjual=0 AND FlagBatal!=1,Total,0)),0) AS TotalPenjualanTiket
	FROM tbl_reservasi
	WHERE (DATE($date_filter) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND CetakTiket = 1
		$kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

$total_m	= 0;
$total_r	= 0;
$total_pp   = 0;
$total_ppcc = 0;
$total_pppcc = 0;
$total_t 	= 0;
$total_s 	= 0;
$total_p 	= 0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']				= $row['KodeCabang'];
	$temp_array[$idx]['Nama']					= $row['Nama'];
	$temp_array[$idx]['Alamat']					= $row['Alamat'];
	$temp_array[$idx]['Kota']					= $row['Kota'];
	$temp_array[$idx]['Telp']					= $row['Telp'];
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$temp_array[$idx]['PenjualanPenumpangM']	= $data_tiket_total[$row['KodeCabang']]['PenjualanPenumpangM'];
	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$temp_array[$idx]['PenjualanPenumpangR']	= $data_tiket_total[$row['KodeCabang']]['PenjualanPenumpangR'];
    $temp_array[$idx]['TotalPenumpangPP']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangPP'];
    $temp_array[$idx]['PenjualanPenumpangPP']	= $data_tiket_total[$row['KodeCabang']]['PenjualanPenumpangPP'];
    $temp_array[$idx]['TotalPenumpangPPCC']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangPPCC'];
	$temp_array[$idx]['TotalTiket']				= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
	$temp_array[$idx]['TotalPenjualanTiket']	= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalPenjualanTiketPPCC']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanPPCC'];
	$temp_array[$idx]['Komisi']					= ($data_tiket_total[$row['KodeCabang']]['TotalPenumpangO']*5000);
	$temp_array[$idx]['KomisiPPCC']				= ($data_tiket_total[$row['KodeCabang']]['TotalPenumpangPPCC']*2500);

	$total_t	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
	$total_p	+= ($data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket']);
	$total_s	+= ($data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'])-(($data_tiket_total[$row['KodeCabang']]['TotalPenumpangO']*5000));

	$idx++;
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}

	//total tiket
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	$total_penjualan_tiket 				= $temp_array[$idx]['TotalPenjualanTiket']+$temp_array[$idx]['TotalPenjualanTiketPPCC'];
	$total_komisi		 				= $temp_array[$idx]['Komisi'];
	$total_komisippcc	 				= $temp_array[$idx]['KomisiPPCC'];
	$total_setor 						= $total_penjualan_tiket - ($total_komisi + $total_komisippcc);

	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'kode_cabang'=>$temp_array[$idx]['KodeCabang'],
				'cabang'=>$temp_array[$idx]['Nama'],
				'alamat'=>$temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Kota'],
				'total_penumpang_m'=>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
				'total_penjualan_m'=>number_format($temp_array[$idx]['PenjualanPenumpangM'],0,",","."),
				'total_penumpang_r'=>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
				'total_penjualan_r'=>number_format($temp_array[$idx]['PenjualanPenumpangR'],0,",","."),
                'total_penumpang_pp'=>number_format($temp_array[$idx]['TotalPenumpangPP'],0,",","."),
                'total_penjualan_pp'=>number_format($temp_array[$idx]['PenjualanPenumpangPP'],0,",","."),
                'total_penumpang_ppcc'=>number_format($temp_array[$idx]['TotalPenumpangPPCC'],0,",","."),
                'total_penjualan_ppcc'=>number_format($temp_array[$idx]['TotalPenjualanTiketPPCC'],0,",","."),
                'total_penumpang_u'=>number_format($total_tiket - ($temp_array[$idx]['TotalPenumpangPPCC']+$temp_array[$idx]['TotalPenumpangPP']+$temp_array[$idx]['TotalPenumpangR']+$temp_array[$idx]['TotalPenumpangM']),0,",","."),
				'total_penjualan_u'=>number_format($total_penjualan_tiket - ($temp_array[$idx]['TotalPenjualanTiketPPCC']+$temp_array[$idx]['PenjualanPenumpangPP']+$temp_array[$idx]['PenjualanPenumpangR']+$temp_array[$idx]['PenjualanPenumpangM']),0,",","."),
				'total_penumpang'=>number_format($total_tiket,0,",","."),
				'total_penjualan_tiket'=>number_format($total_penjualan_tiket,0,",","."),
				'total_setor'=>number_format($total_setor,0,",","."),
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tglmulai=".$tanggal_mulai."&tglakhir=".$tanggal_akhir."&berdasarkan=".$type."&txt_cari=".$cari;
											
$script_cetak_excel="Start('laporan_omzet_cabang_online_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$page_title	= "Penjualan Cabang";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_omzet_cabang_online.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'TOTAL_M'				=> number_format($total_m,0,",","."),
	'TOTAL_R'				=> number_format($total_r,0,",","."),
    'TOTAL_PP'				=> number_format($total_pp,0,",","."),
    'TOTAL_PPCC'				=> number_format($total_ppcc,0,",","."),
	'TOTAL_T'				=> number_format($total_t,0,",","."),
	'TOTAL_SETOR'				=> number_format($total_s,0,",","."),
	'TOTAL_PENJUALAN'				=> number_format($total_p,0,",","."),
	'CETAK_XL'			=> $script_cetak_excel,
	)
);
	     
				
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
