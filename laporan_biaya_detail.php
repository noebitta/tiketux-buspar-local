<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 713;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_KASIR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

function setComboCabang($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	$Cabang	= new Cabang();
			
	$result=$Cabang->ambilData("","Nama,Area","ASC");
	
	$selected = ($cabang_dipilih=="")?"selected":"";
	
	$opt_cabang="<option value=0 $selected>- semua cabang-</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['Kode'])?"":"selected";
			$opt_cabang .="<option value='$row[Kode]' $selected>$row[Nama] $row[Area] ($row[Kode])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cabang  				= isset($HTTP_GET_VARS['opt_cabang'])? $HTTP_GET_VARS['opt_cabang'] : $HTTP_POST_VARS['opt_cabang'];
$jenis_biaya		= isset($HTTP_GET_VARS['opt_jenis_biaya'])? $HTTP_GET_VARS['opt_jenis_biaya'] : $HTTP_POST_VARS['opt_jenis_biaya'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

// LIST
$template->set_filenames(array('body' => 'laporan_biaya/laporan_biaya_detail_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$kondisi	= "WHERE (TglTransaksi BETWEEN CONVERT(datetime,'$tanggal_mulai',105) AND CONVERT(datetime,'$tanggal_akhir',105))";

$kondisi_cabang	=($cabang==0 || $cabang=="")?"":
	" AND KodeCabang = '$cabang' ";

$kondisi_jb	=($jenis_biaya=="")?"":
	" AND FlagJenisBiaya = $jenis_biaya ";

$kondisi_cari	=($cari=="")?"":
	" AND (NoSPJ LIKE '%$cari'
		OR NoPolisi LIKE '%$cari%')";
	
$kondisi	= $kondisi.$kondisi_cabang.$kondisi_jb.$kondisi_cari;

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"TglPesan,JamPesan":$sort_by;

			
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingDataWithJoin($idx_page,"IDBiayaOP","TbBiayaOP",
	"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&opt_cabang=$cabang&opt_jenis_biaya=$jenis_biaya",
	$kondisi,"laporan_biaya_detail.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		IdBiayaOP,NoSPJ,FlagJenisBiaya,NoPolisi,dbo.f_SopirGetNamaByKode(KodeSopir) AS NamaSopir,Jumlah,
		dbo.f_KodeVoucherGetByNoSPJ(NoSPJ) AS KodeVoucher,
		CONVERT(varchar(12),TglTransaksi,106) as TglTransaksi,
		dbo.f_CabangGetNameByKode(KodeCabang) AS Cabang,
		dbo.f_UserGetNameById(IdPetugas) AS Kasir,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangAsalByIdJurusan(IdJurusan)) Asal,
		dbo.f_CabangGetNameByKode(dbo.f_JadwalAmbilKodeCabangTujuanByIdJurusan(IdJurusan)) Tujuan
		
	FROM 
		TbBiayaOP
	$kondisi
	ORDER BY TglTransaksi,NoSPJ,IdBiayaOP LIMIT $idx_awal_record,$VIEW_PER_PAGE";	
	//ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";	


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
	
	$tgl_sebelumnya	="";
	
	$flag_odd	=1;
	
	$total_biaya	=	array(0,0,0,0);
	
  while ($row = $db->sql_fetchrow($result)){
		
		if($tgl_sebelumnya==""){
			$tgl_ditampilkan	= $row['TglTransaksi'];
			$tgl_sebelumnya		= $tgl_ditampilkan; 
		}
		elseif($tgl_sebelumnya==$row['TglTransaksi']){
			$tgl_ditampilkan	= "";
		}
		else{
			$tgl_ditampilkan	= $row['TglTransaksi'];
			$tgl_sebelumnya		= $tgl_ditampilkan; 
			$flag_odd	= 1-$flag_odd;
		}
		
		$kode_voucher	=($row['FlagJenisBiaya']!=$LIST_KODE_BIAYA['bbm'])?"":"<br>VC:".$row['KodeVoucher'];
		
		$odd ='odd';
		
		if (($flag_odd % 2)==0){
			$odd = 'even';
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'tgl_transaksi'=>$tgl_ditampilkan,
					'no_pol'=>$row['NoPolisi'],
					'sopir'=>$row['NamaSopir'],
					'jurusan'=>$row['Asal']."-".$row['Tujuan'],
					'no_spj'=>$row['NoSPJ'],
					'jenis_biaya'=>$LIST_BIAYA[$row['FlagJenisBiaya']].$kode_voucher,
					'jumlah'=>number_format($row['Jumlah'],0,",","."),
					'cabang'=>$row['Cabang'],
					'kasir'=>$row['Kasir']
				)
			);
		
		$total_biaya[$row['FlagJenisBiaya']] +=$row['Jumlah'];
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Error :$sql".__LINE__);exit;
} 

//SUMMARY

	$summary = "
		<table width=1210>
			<tr><td colspan=4><h2>SUMMARY</h2></td></tr>
			<tr><td width='240'>
				<table width='240' class='border'>
					<tr><th colspan=3 ><font size=3>Jasa Sopir</font></th></tr>
					<tr>
						<td width=100>Total</td><td width=50>: Rp.</td><td width=150 align='right'>".number_format($total_biaya[0],0,",",".")."</td>
					</tr>
				</table>
			</td>
			<td width=1 ></td>
			<td width='240'>
				<table width='240' class='border'>
					<tr><th colspan=3><font size=3>Tol</font></th></tr>
					<tr>
						<td width=100>Total</td><td width=50>: Rp.</td><td width=150 align='right'>".number_format($total_biaya[1],0,",",".")."</td>
					</tr>
				</table>
			</td>
			<td width=1 ></td>
			<td width='240'>
				<table width='240' class='border'>
					<tr><th colspan=3><font size=3>Parkir</font></th></tr>
					<tr>
						<td width=100>Total</td><td width=50>: Rp.</td><td width=150 align='right'>".number_format($total_biaya[4],0,",",".")."</td>
					</tr>
				</table>
			</td>
			<td width=1></td>
			<td width='240'>
				<table width='240' class='border'>
					<tr><th colspan=3><font size=3>Fee Driver</font></th></tr>
					<tr>
						<td width=100>Total</td><td width=50>: Rp.</td><td width=150 align='right'>".number_format($total_biaya[2],0,",",".")."</td>
					</tr>		
				</table>
			</td>
			<td width=1></td>
			<td width='240'>
				<table width='240' class='border'>
					<tr><th colspan=3><font size=3>BBM</font></th></tr>
					<tr>
						<td width=100>Total</td><td width=50>: Rp.</td><td width=150 align='right'>".number_format($total_biaya[3],0,",",".")."</td>
					</tr>
				</table>
			</td></tr>
		</table>
	";


$temp_var_jb		= "jb".$jenis_biaya;
$$temp_var_jb		= "selected";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$jenis_biaya.
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."&p7=".$cabang;
	
$script_cetak_pdf="Start('laporan_biaya_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_biaya_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
	'BCRUMP'    		=> setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_biaya_detail.'.$phpEx),
	'OPT_CABANG'		=> setComboCabang($cabang),
	'JENIS_BIAYA_'	=>$jb,
	'JENIS_BIAYA_0'	=>$jb0,
	'JENIS_BIAYA_1'	=>$jb1,
	'JENIS_BIAYA_2'	=>$jb2,
	'JENIS_BIAYA_3'	=>$jb3,
	'JENIS_BIAYA_4'	=>$jb4,
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>