<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KASIR"],$USER_LEVEL_INDEX["CSO"],$USER_LEVEL_INDEX["CSO_PAKET"]))){
    die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
$id = isset($HTTP_GET_VARS['id'])? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];
$tglprint = date('Y-m-d H:i:s');
$namapencetak = $userdata['nama'];
$idpencetak = $userdata['user_id'];
//update pencetak
$sql ="UPDATE tbl_biaya_petty_cash SET TglCetak='$tglprint', IdPencetak='$idpencetak', NamaPencetak='$namapencetak' WHERE IdBiayaPettyCash = $id";
if (!$db->sql_query($sql)){
    die(mysql_error());
}
//ambil data
$sql = "SELECT *
        FROM tbl_biaya_petty_cash
        WHERE IdBiayaPettyCash = $id";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}
//
$row = $db->sql_fetchrow($result);
$KodeCabang = $row['KodeCabang'];
$Pembuat    = $row['NamaPembuat'];
$Releaser   = $row['NamaReleaser'];
$Penerima   = $row['NamaPenerima'];
$JenisBiaya = $row['JenisBiaya'];
$Keterangan = $row['Keterangan'];
$Pecetak    = $row['NamaPencetak'];
$Jml        = $row['Jumlah'];
$TglBuat    = date_format(date_create($row['TglBuat']),'d-m-Y H:i:s');
$TglCetak   = date_format(date_create($row['TglCetak']),'d-m-Y H:i:s');

// ambil saldo sekarang
$sql ="SELECT SaldoPettyCash FROM tbl_md_cabang WHERE KodeCabang= '$KodeCabang'";
if(!$result = $db->sql_query($sql)){
    die(mysql_error());
}
$kas = $db->sql_fetchrow($result);
$saldosekarang = $kas['SaldoPettyCash'];
$saldoakhir = $saldosekarang - $Jml;
// kurangi saldo petty cash cabang yang bersangkutan
$sql = "UPDATE tbl_md_cabang SET SaldoPettyCash = $saldoakhir WHERE KodeCabang='$KodeCabang'";
if(!$result = $db->sql_query($sql)){
    die(mysql_error());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cetak Biaya Tambahan</title>
    <style type="text/css">
        body {margin: 0; padding: 0; font-family: 'Calibri'; font-size: 16px;}
        html {margin: 0; padding: 0;}
        table.container {min-width: 230px; max-width: 230px; border-collapse: collapse;}
        /*table.container tr td {padding-left: 10px;}*/
        hr {border:1px dashed #ccc; margin: 2px 0}
        .text-lowercase {text-transform: lowercase;}
    </style>
</head>
<body>
<?php for($i=1; $i<=2; $i++){?>
    <table class="container" cellpadding="0" >
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td colspan="2" align="center">DAYTRANS</td>
        </tr>
        <tr>
            <td colspan="2" align="center">Petty Cash</td>
        </tr>
        <!-- ./header -->
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><b>Slip <?php echo $i;?></b></td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td valign="top">Releaser</td>
            <td align="right"><?php echo $Releaser;?></td>
        </tr>
        <tr>
            <td valign="top">Tanggal Buat</td>
            <td align="right"><?php echo $TglBuat;?></td>
        </tr>

        <tr>
            <td valign="top">Pembuat</td>
            <td align="right"><?php echo $Pembuat;?></td>
        </tr>

        <tr>
            <td valign="top">Pecetak</td>
            <td align="right"><?php echo $Pecetak;?></td>
        </tr>
        <tr>
            <td valign="top">Penerima</td>
            <td align="right"><?php echo $Penerima;?></td>
        </tr>
        <tr>
            <td>Kode Cabang</td>
            <td align="right"><?php echo $KodeCabang;?></td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td>Jenis Biaya </td>
            <td align="right"><?php echo $JenisBiaya;?></td>
        </tr>
        <tr>
            <td>Jumlah</td>
            <td align="right"><?php echo number_format($Jml,0,',','.');?></td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td align="right"><?php echo $Keterangan;?></td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td>Tanggal Cetak</td>
            <td align="right"><?php echo $TglCetak;?></td>
        </tr>
        <tr>
            <td colspan="2"><hr></td>
        </tr>
        <tr>
            <td colspan="2" align="center">Tanda Tangan Bukti</td>
        </tr>
        <tr>
            <td height="75" valign="top">Pembuat</td>
            <td height="75" valign="top" align="right">Penerima</td>
        </tr>
        <tr>
            <td><?php echo $Pembuat;?></td>
            <td align="right"><?php echo $Penerima;?></td>
        </tr>
    </table>
    <br><br>
<?php } ?>
</body>
</html>