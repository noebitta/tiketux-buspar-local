<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class TargetUser extends User{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function TargetUser(){
		$this->ID_FILE="C-TGU";
	}
	
	//BODY
	function ubahTarget($BulanTarget,$TahunTarget,$UserId,$NamaUser,$Target,$Log){
	  
		/*UPDATE DATA TARGET PER USER*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_user_target
			SET 
				Target='$Target',
				Log=CONCAT(IF(Log IS NULL,'',Log),';','$Log')
			WHERE UserId='$UserId' AND BulanTarget='$BulanTarget' AND TahunTarget='$TahunTarget';";
		
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}

    if($db->sql_affectedrows()<=0){
      /*BELUM PERNAH DI SET TARGET, MAKA AKAN DITAMBAHKAN TARGET KE DATABASE*/

      $sql =
        "INSERT INTO tbl_user_target(
          UserId,NamaUser,BulanTarget,
          TahunTarget,Target,TargetTercapai,
          Log)
			VALUES(
				'$UserId','$NamaUser','$BulanTarget',
				'$TahunTarget','$Target',0,
				'$Log');";

      if (!$db->sql_query($sql)){
        echo("Err: $this->ID_FILE".__LINE__);exit;
      }
    }

		return true;
	}//end ubahTarget

  function getTargetUserNow($UserId){
    global $db;

    $sql  = "SELECT * FROM tbl_user_target WHERE UserId='$UserId' AND BulanTarget=MONTH(NOW()) AND TahunTarget=YEAR(NOW()) LIMIT 0,1";

    if(!$result=$db->sql_query($sql)){
      echo("Err:".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }

  function addTransaksi($jumlah_transaksi){
    global $db;
    global $userdata;

    $jumlah_transaksi = $jumlah_transaksi!=""?$jumlah_transaksi:0;

    $sql =
      "UPDATE tbl_user_target
      SET
				TargetTercapai=TargetTercapai+$jumlah_transaksi
			WHERE UserId='$userdata[user_id]' AND BulanTarget=MONTH(NOW()) AND TahunTarget=YEAR(NOW())";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function resetTarget($BulanTarget,$TahunTarget,$Log){

    /*MERESET DATA TARGET SEMUA USER MENJADI 0*/

    //kamus
    global $db;

    $sql =
      "UPDATE tbl_user_target
			SET
				Target='0',
				Log=CONCAT(IF(Log IS NULL,'',Log),';','$Log')
			WHERE BulanTarget='$BulanTarget' AND TahunTarget='$TahunTarget';";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }//end resetTarget
}
?>