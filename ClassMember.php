<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Member{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Member(){
		$this->ID_FILE="C-MBR";
	}
	
	//BODY
	
	function kodeValidasi($id_member,$no_ktp,$deposit,$point){
		return md5($deposit.$point.$id_member.$no_ktp);
	}
	
	function generateIdMember(){
		$temp	= array("1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		
		return $y.$rnd1.$m.$rnd2.$d.$j.$mn;
	}
	
	function periksaDuplikasi($id_member,$email,$handphone){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika id_member tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT f_member_periksa_duplikasi('$id_member','$email','$handphone') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti id_member sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	
	function periksaDuplikasiKartu($nomor_kartu){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika id_member tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) as JumlahData FROM tbl_md_member WHERE NoSeriKartu='$nomor_kartu'";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		$row = $db->sql_fetchrow($result);
		$ditemukan = ($row['JumlahData']<=0)?false:true;
	
		
		return $ditemukan;
		
	}//  END periksaDuplikasiKartu
	
	function tambah(
		$id_member,$nama, $jenis_kelamin,
	  $kategori_member,$tempat_lahir,$tgl_lahir,
	  $no_ktp,$tgl_registrasi,$alamat,
	  $kota,$kode_pos,$telp,
	  $handphone,$email, $pekerjaan,
	  $point,$expired_date, $id_kartu, 
	  $no_seri_kartu,$kata_sandi,$flag_aktif,
		$cabang_daftar){
		  
		/*
		IS	: data member belum ada dalam database
		FS	:Data member baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_md_member (
				IdMember, Nama, JenisKelamin,
				KategoriMember, TempatLahir, TglLahir,
				NoKTP, TglRegistrasi, Alamat,
				Kota, KodePos, Telp,
				Handphone, Email, Pekerjaan,
				Point, ExpiredDate, IdKartu,
				NoSeriKartu, KataSandi, FlagAktif,
				CabangDaftar,WaktuPendaftaran,PetugasPendaftar,
				NamaPetugasDaftar)
			VALUES(
				'$id_member','$nama','$jenis_kelamin',
				'$kategori_member','$tempat_lahir','$tgl_lahir',
				'$no_ktp','$tgl_registrasi','$alamat',
				'$kota','$kode_pos','$telp',
				'$handphone','$email','$pekerjaan',
				'$point','$expired_date','$id_kartu',
				'$no_seri_kartu',md5('$kata_sandi'),'$flag_aktif',
				'$cabang_daftar',NOW(),'$userdata[user_id]',
				'$userdata[nama]');";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah(
		$id_member,$nama, $jenis_kelamin,
	  $kategori_member,$tempat_lahir,$tgl_lahir,
	  $no_ktp,$tgl_registrasi,$alamat,
	  $kota,$kode_pos,$telp,
	  $handphone,$email, $pekerjaan,
	  $point,$expired_date, $id_kartu, 
	  $no_seri_kartu,$kata_sandi,$flag_aktif,
		$cabang_daftar){
	  
		/*
		IS	: data member sudah ada dalam database
		FS	:Data member diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"CALL sp_member_ubah(
				'$id_member', '$nama', '$jenis_kelamin',
			  '$kategori_member','$tempat_lahir','$tgl_lahir',
			  '$no_ktp','$tgl_registrasi','$alamat',
			  '$kota','$kode_pos','$telp',
			  '$handphone','$email','$pekerjaan',
			  '$point','$expired_date','$id_kartu', 
			  '$no_seri_kartu','$kata_sandi','$flag_aktif',
				'$cabang_daftar');";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_member){
	  
		/*
		IS	: data sopir sudah ada dalam database
		FS	:Data sopir dihapus
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA
		$sql =
			"DELETE FROM tbl_md_member
			WHERE IdMember IN($list_member);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ubahStatusAktif($id_member){
	  
		/*
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql ="CALL sp_member_ubah_status_aktif('$id_member');";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function ambilData($id_member){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *,DATEDIFF(ExpiredDate,NOW()) AS MasaBerlaku
			FROM tbl_md_member
			WHERE IdMember='$id_member';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function registerKartu($id_member,$id_kartu){
	  
		/*
		ID	: 008
		IS	: data member sudah ada dalam database
		FS	: kartu teregister
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return "false otoritas";
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		$id_kartu	= md5($id_kartu);
		
		$sql = 
			"SELECT 
				COUNT(id_member) as jumlah_data
			FROM $this->TABEL1
			WHERE 
				id_kartu='$id_kartu' AND NOT id_member='$id_member'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti id_member sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 008-01");
		}
		
		if(!$ditemukan){
			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"UPDATE $this->TABEL1
				SET 
					id_kartu='$id_kartu',
					diubah_oleh='$useraktif',tgl_diubah={fn NOW()}
				WHERE id_member='$id_member';";
									
			if (!$db->sql_query($sql)){
				die_error("Gagal $this->ID_FILE 008-02");
			}
			
			return "true";
		}
		else{
			return "false duplikasi";
		}
		
	}//end registerKartu
	
	function ambilDataByIdKartu($id_kartu){
		
		/*
		ID	:009
		Desc	:Mengembalikan data member berdasarkan id kartu
		ket	: ID_KARTU YANG MASUK ADALAH ID KARTU YANG SUDAH DI MD5
		*/
		
		//kamus
		global $db;
		
		$id_kartu=trim($id_kartu);
		
		$sql = 
			"SELECT 
				id_member,nama,jenis_kelamin,tempat_lahir,
				CONVERT(CHAR(10),tgl_lahir,103) as tgl_lahir,no_ktp,CONVERT(CHAR(10),tgl_registrasi,103) as tgl_registrasi,
				alamat,kota,kode_pos,telp_rumah,handphone,email,
				status_member,saldo,point,CONVERT(CHAR(25),waktu_transaksi_terakhir,103) as waktu_transaksi_terakhir,
				jumlah_transaksi_terakhir,expired_date,id_kartu,
				diubah_oleh,CONVERT(CHAR(10),tgl_diubah,103) as tgl_diubah,kode_validasi,kategori_member
			FROM $this->TABEL1
			WHERE id_kartu='$id_kartu';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 009");//.$sql);
		}
		
	}//  END ambilDataByIdKartu
	
	function registerKartuBaru($no_seri_kartu,$id_kartu){
	  
		/*
		ID	: 010
		IS	: data kartu belum ada
		FS	: kartu teregister
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak valid atau jika id member duplikasi
			return "false otoritas";
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		$id_kartu	= md5($id_kartu);
		
		$sql = 
			"SELECT 
				COUNT(id_kartu) as jumlah_data
			FROM $this->TABEL1
			WHERE 
				id_kartu='$id_kartu'";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti id_member sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 010-01");
		}
		
		if(!$ditemukan){
			$id_member	= "MBR".dateYMD().rand(100,999);
			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"INSERT INTO $this->TABEL1(
					id_member,no_seri_kartu,id_kartu,status_member,
					diubah_oleh,tgl_diubah
				)
				VALUES(
					'$id_member','$no_seri_kartu','$id_kartu',0,
					'$useraktif',{fn NOW()}
				)";
									
			if (!$db->sql_query($sql)){
				die_error("Gagal $this->ID_FILE 010-02");
			}
			
			return "true";
		}
		else{
			return "false duplikasi";
		}
		
	}//end registerKartu
	
	function ambilDataKartuBelumAktif($order_by,$asc){
		
		/*
		ID	:011
		Desc	:Mengembalikan kartu yang belum aktif
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT 
				id_member,no_seri_kartu,
				diubah_oleh,CONVERT(CHAR(10),tgl_diubah,103) as tgl_diubah
			FROM $this->TABEL1
			WHERE 
				status_member=0 AND nama IS NULL
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 011");
		}
		
	}//  END ambilDataKartuBelumAktif
	
	function ambilDataByNoSeriKartu($no_seri_kartu){
		
		/*
		ID	:012
		Desc	:Mengembalikan data member berdasarkan no seri kartu
		*/
		
		//kamus
		global $db;

		$sql = 
			"SELECT 
				id_member,nama,jenis_kelamin,tempat_lahir,
				CONVERT(CHAR(10),tgl_lahir,103) as tgl_lahir,no_ktp,CONVERT(CHAR(10),tgl_registrasi,103) as tgl_registrasi,
				alamat,kota,kode_pos,telp_rumah,handphone,email,
				status_member,saldo,point,CONVERT(CHAR(25),waktu_transaksi_terakhir,103) as waktu_transaksi_terakhir,
				jumlah_transaksi_terakhir,expired_date,id_kartu,
				diubah_oleh,CONVERT(CHAR(10),tgl_diubah,103) as tgl_diubah,kode_validasi,kategori_member
			FROM $this->TABEL1
			WHERE no_seri_kartu = '$no_seri_kartu';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 012");
		}
		
	}//  END ambilDataByNoSeriKartu
	
	function ubahPassword($id_member,$password){
	  
		/*
		ID	: 013
		IS	: data member sudah ada dalam database
		FS	:password member diubah
		*/
		
		//kamus
		global $db;

		
		$password=md5($password);
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE $this->TABEL1
			SET kata_sandi='$password'
			WHERE id_member='$id_member';";
								
		if (!$db->sql_query($sql)){
			die_error("Gagal $this->ID_FILE 013 ");
		}
		
		return true;
	} //ubahPassword
	
	function verifikasiPassword($id_member,$password){
		
		/*
		ID	:014
		Desc	:Mengembalikan true jika verfikasi password benar
		*/
		
		//kamus
		global $db;

		$sql = 
			"SELECT kata_sandi
			FROM $this->TABEL1
			WHERE id_member = '$id_member';";
		
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			
			if(md5($password)==trim($row['kata_sandi'])){
				return true;
			}
			else{
				return false;
			}
		} 
		else{
			die_error("Gagal $this->ID_FILE 014 ");
		}
		
	}//  END verifikasiPassword
	
	
//[TOP UP ALL ABOUT]===================================

  function isDuplikasiKodeRef($kode_referensi){
    global $db;

    $sql = "SELECT COUNT(1) FROM tbl_member_deposit_topup_log WHERE KodeReferensi='$kode_referensi'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE ".__LINE__);
    }

    $row=$db->sql_fetchrow($result);

    return ($row[0]<=0?false:true);
  }

	function generateKodeReferensi(){
		global $db;
		
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		
		$is_duplikasi = false;

		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$kode_referensi	= "DTA".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s.$rnd3;
		
			$is_duplikasi = $this->isDuplikasiKodeRef($kode_referensi);

		}while($is_duplikasi);
			
		
		
		return $kode_referensi;
	}
	
	function ambilPaketTopup($is_aktif="%"){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_member_topup_paket
			WHERE IsAktif LIKE '$is_aktif'";
				
		if(!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE ".__LINE__);exit;
		}
		return $result;
	}//  END ambilPaketTopup
	
	function ambilDetailPaketTopup($id_paket){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_member_topup_paket
			WHERE Id='$id_paket'";
				
		if(!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE ".__LINE__);exit;
		}
		
		return $db->sql_fetchrow($result);
	}//  END ambilDetailPaketTopup
	
	function ambilPaketTopupByKondisi($kondisi){
		
		//kamus
		global $db;
		
		$id_kartu=trim($id_kartu);
		
		$sql = 
			"SELECT *
			FROM tbl_member_topup_paket
			WHERE 1 AND $kondisi";
				
		if(!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE ".__LINE__);exit;
		}
		return $result;
	}//  END ambilPaketTopup

	function topUpMember(
		$IdMember,$KodeReferensi,$JumlahRupiah,
		$JumlahPoin,$PetugasTopUp,$NamaPetugasTopUp,
		$kode_token){

		global $db;

		//MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "INSERT INTO tbl_member_deposit_topup_log (
        IdMember,KodeReferensi,WaktuTransaksi,
        JumlahRupiah,JumlahPoin,PetugasTopUp,
        NamaPetugasTopUp,KodeToken)
      VALUES('$IdMember','$KodeReferensi',NOW(),
        '$JumlahRupiah','$JumlahPoin','$PetugasTopUp',
        '$NamaPetugasTopUp','$kode_token');";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return true;
	}

  function ambilDetailLogTopUp($kode_referensi){

    //kamus
    global $db;

    $sql =
      "SELECT *
			FROM tbl_member_deposit_topup_log
			WHERE KodeReferensi='$kode_referensi'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;
  }//  END ambilDetailLogTopUp

  function isSudahProsesKodeRef($kode_referensi){
    global $db;

    $sql = "SELECT COUNT(1) FROM tbl_member_deposit_topup_log WHERE KodeReferensi='$kode_referensi' AND IsVerified=1";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE ".__LINE__);
    }

    $row=$db->sql_fetchrow($result);

    return ($row[0]<=0?false:true);
  }

  function verifikasiTopUp(
    $IdMember,$KodeReferensi,$JumlahRupiah,
    $JumlahPoin,$PetugasTopUp,$NamaPetugasTopUp,$kode_token){

    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "UPDATE tbl_member_deposit_topup_log
      SET IsVerified=1,WaktuVerified=NOW()
      WHERE KodeReferensi='$KodeReferensi' AND IdMember='$IdMember' AND KodeToken='$kode_token';";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $sql= "CALL sp_member_deposit_topup('$IdMember','$KodeReferensi','$JumlahRupiah','$JumlahPoin','$PetugasTopUp','$NamaPetugasTopUp','$kode_token');";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

	function ambilDataByKondisi($kondisi){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_member
			WHERE 1 AND $kondisi;";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function generateToken(
		$operation,$kode_referensi,$kode_member,
		$nomor_kartu,$jumlah,$user_id,$token_secret){
		
		$tgl_transaksi	= date("Y-m-d");
		
		$return_val	= md5($operation."!".$kode_referensi."@".$kode_member."#".$nomor_kartu."$".$jumlah."%".$user_id."^".$token_secret."&".$tgl_transaksi);
		
		return $return_val;
	}
	
	function ambilLogTransaksiTopup($kode_referensi){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT tmdtl.*,tmm.Nama,tmm.SaldoDeposit
			FROM tbl_member_deposit_topup_log tmdtl INNER JOIN tbl_md_member tmm ON tmdtl.IdMember=tmm.IdMember
			WHERE KodeReferensi='$kode_referensi';";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		
		return $row;
		
	}//  END ambilLogTransaksiTopup
	
	function ambilJumlahTiketDibeli($kode_booking){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS JumlahTiket
			FROM tbl_reservasi
			WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 AND CetakTiket!=1;";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		
		return $row[0];
		
	}//  END ambilLogTransaksiTopup

  function ambilPromoMember($kode_jadwal,$tgl_berangkat){

    /*
    ID	:002
    Desc	:Mengembalikan nilai discount
    */

    //kamus
    global $db;

    $sql_jam_berangkat	= "(SELECT JamBerangkat FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal')";

    $sql_id_jurusan	= "f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')";

    $sql_kode_cabang_asal	="f_jurusan_get_kode_cabang_asal_by_jurusan($sql_id_jurusan)";
    $sql_kode_cabang_tujuan	="f_jurusan_get_kode_cabang_tujuan_by_jurusan($sql_id_jurusan)";

    $sql =
      "SELECT JumlahDiscount,FlagDiscount,JumlahPoint,NamaPromo,KodePromo
			FROM tbl_md_promo
			WHERE
				IF(KodeCabangAsal!='',KodeCabangAsal LIKE $sql_kode_cabang_asal,1)
				AND IF(KodeCabangTujuan!='',KodeCabangTujuan LIKE $sql_kode_cabang_tujuan,1)
				AND (CONCAT('$tgl_berangkat',' ',$sql_jam_berangkat) BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir))
				AND FlagAktif=1
				AND FlagTargetPromo<2
			ORDER BY LevelPromo DESC LIMIT 0,1;";

    if ($result = $db->sql_query($sql)){
      $row=$db->sql_fetchrow($result);
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return $row;

  }//  END ambilDiscountDanPoint

  function ambilTotalPembelian($kode_booking){

    /*
    ID	:007
    Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
    */

    //kamus
    global $db;

    $sql =
      "SELECT SUM(Total) AS TotalBayar,COUNT(NoTiket) AS JumlahTiket,SUM(HargaTiket) AS TotalHarga,
        GROUP_CONCAT(DISTINCT KodeJadwal SEPARATOR ',') AS GrupKodeJadwal,
        GROUP_CONCAT(DATE_FORMAT(TglBerangkat,'%Y-%m-%d') SEPARATOR ',') AS GrupTglBerangkat
			FROM tbl_reservasi
			WHERE KodeBooking='$kode_booking' AND FlagBatal!=1 AND CetakTiket!=1;";

    if (!$result = $db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    $row=$db->sql_fetchrow($result);

    $sub_total          = $row["TotalHarga"];
    $total_bayar        = $row["TotalBayar"];
    $jumlah_tiket       = $row["JumlahTiket"];
    $grup_kodejadwal    = explode(",",$row["GrupKodeJadwal"]);
    $grup_tgl_berangkat = explode(",",$row["GrupTglBerangkat"]);
    $kode_jadwal        = $grup_kodejadwal[0];
    $tgl_berangkat      = $grup_tgl_berangkat[0];

    //MEMERIKSA DISCOUNT BERDASARKAN PROMO YANG BERLAKU
    $data_promo	= $this->ambilPromoMember($kode_jadwal,$tgl_berangkat);
    if($data_promo["FlagDiscount"]==0) {
      $discount = ($data_promo['JumlahDiscount'] > 1) ? $data_promo['JumlahDiscount'] * $jumlah_tiket : $data_promo['JumlahDiscount'] * $sub_total;

      $discount   = $discount<$sub_total?$discount:$sub_total;
      $total_bayar      -=$discount;
    }
    else{
      $total_bayar  = $data_promo['JumlahDiscount'] ;
    }

    return $total_bayar;

  }//  END ambilTotalPembelian

  function preDebitMember(
    $IdMember,$KodeReferensi,$JumlahRupiah,
    $JumlahPoin,$PetugasDebit,$NamaPetugasDebit,
    $kode_token){

    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "INSERT INTO tbl_member_deposit_debit_log (
        IdMember,KodeReferensi,WaktuTransaksi,
        JumlahRupiah,JumlahPoin,PetugasDebit,
        NamaPetugasDebit,KodeToken)
      VALUES('$IdMember','$KodeReferensi',NOW(),
        '$JumlahRupiah','$JumlahPoin','$PetugasDebit',
        '$NamaPetugasDebit','$kode_token');";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return true;
  }

  function ambilDetailLogDebit($kode_referensi){

    //kamus
    global $db;

    $sql =
      "SELECT *
			FROM tbl_member_deposit_debit_log
			WHERE KodeReferensi='$kode_referensi'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;
  }//  END ambilDetailLogDebit

  function verifikasiDebit(
    $IdMember,$KodeReferensi,$KodeBooking,
    $CabangTransaksi,$CSO,$KodeToken){

    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql=
      "UPDATE tbl_member_deposit_debit_log
      SET IsVerified=1,WaktuVerified=NOW()
      WHERE KodeReferensi='$KodeReferensi' AND IdMember='$IdMember' AND KodeToken='$KodeToken';";

    if (!$db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $harga_poin = $this->ambilHargaPoin();
    $this->debitMember($IdMember,$KodeReferensi,$KodeBooking,$CabangTransaksi,$CSO,$harga_poin,$KodeToken);

    return true;
  }

	function debitMember(
		$IdMember,$KodeReferensi,$KodeBooking,
		$CabangTransaksi,$CSO,$HargaPoin,
    $Signature){
		
		global $db;


		//p_IdMember VARCHAR(30),p_KodeReferensi VARCHAR(30),p_KodeBooking VARCHAR(50),
		//p_KodeJadwal VARCHAR(30),p_TglBerangkat DATE,p_Keterangan TEXT,p_CSO INTEGER,p_CabangTransaksi VARCHAR(30),p_Signature VARCHAR(100)
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		//Mengambil detail keberangkatan
		$sql	=
			"SELECT GROUP_CONCAT(Nama SEPARATOR ',') AS NamaPenumpang,
			  KodeJadwal,TglBerangkat,
			  GROUP_CONCAT(DISTINCT HargaTiket SEPARATOR ',') AS GrupHargaTiket
			 FROM tbl_reservasi WHERE KodeBooking='$KodeBooking' GROUP BY KodeBooking";
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}
		
		$data_tiket = $db->sql_fetchrow($result);
		
		//mengambil jadwal utama
		$sql	=
			"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$data_tiket[KodeJadwal]';";
		
		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}

    //MENGAMBIL PROMO JIKA ADA
    $data_promo = $this->ambilPromoMember($data_tiket["KodeJadwal"],$data_tiket["TglBerangkat"]);

    if($data_promo["KodePromo"]!=""){
      $grup_harga_tiket = explode(",",$data_tiket["GrupHargaTiket"]);
      $harga_tiket = $grup_harga_tiket[0];

      if($data_promo["FlagDiscount"]==0) {
        $discount = ($data_promo['JumlahDiscount'] > 1) ? $data_promo['JumlahDiscount'] : $data_promo['JumlahDiscount'] * $harga_tiket;
        $discount = $discount < $harga_tiket ? $discount : $harga_tiket;
      }
      else{
        $discount = $harga_tiket-$data_promo['JumlahDiscount'];
      }
    }

    $discount = $discount<=0?0:$discount;

		$data_jadwal	= $db->sql_fetchrow($result);
		$kode_jadwal	= $data_jadwal[0];
		
		$sql	=
			"CALL sp_member_deposit_trx(
				'$IdMember','$KodeReferensi','$KodeBooking',
				'$kode_jadwal','$data_tiket[TglBerangkat]','Pembelian $KodeBooking $data_tiket[KodeJadwal] $data_tiket[TglBerangkat] Pnp:$data_tiket[NamaPenumpang]',
				'$CSO','$CabangTransaksi','$HargaPoin','$discount','$data_promo[KodePromo]','$Signature')";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);

		}
		
		return true;
	}
	
	function ambilSaldo($IdMember){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT SaldoDeposit
			FROM tbl_md_member
			WHERE IdMember='$IdMember';";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		$row=$db->sql_fetchrow($result);
		return $row[0];
		
	}//  END ambilSaldo

  function ambilHargaPoin(){

    global $db;

    $sql =
      "SELECT NilaiParameter
      FROM tbl_pengaturan_parameter
      WHERE NamaParameter LIKE 'MEMBER_RP2POINT'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $row=$db->sql_fetchrow($result);
    return $row[0];

  }//  END ambilHargaPoin
}
?>