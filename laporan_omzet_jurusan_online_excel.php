<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');

//SESSION
$id_page = 505;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){
    die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$Cabang     = new Cabang();
$Jurusan    = new Jurusan();

//PARAMETER
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$type  			= isset($HTTP_GET_VARS['berdasarkan'])? $HTTP_GET_VARS['berdasarkan'] : $HTTP_POST_VARS['berdasarkan'];

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}
$tanggal_mulai	        = ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	        = ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari2	=($cari=="")?"WHERE 1 ":
    " WHERE (KodeJurusan LIKE '$cari%'
		OR KodeCabangAsal LIKE '%$cari%'
		OR KodeCabangTujuan LIKE '%$cari%')";

if($userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])){
    $kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";
    $kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";
}

switch($type){
    case 1 :
        $date_filter = 'TglBerangkat';
        break;
    case 2 :
        $date_filter = 'WaktuCetakTiket';
        break;
    default :
        $date_filter = 'WaktuCetakTiket';
        break;
}

//MENGAMBIL DATA-DATA MASTER Jurusan
$sql_jurusan= "SELECT KodeJurusan, f_cabang_get_name_by_kode(KodeCabangAsal) AS KodeCabangAsal, f_cabang_get_name_by_kode(KodeCabangTujuan) AS KodeCabangTujuan
	           FROM tbl_md_jurusan
	           $kondisi_cari2
	           ORDER BY KodeJurusan ASC";


if (!$result_laporan = $db->sql_query($sql_jurusan)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}

//DATA PENJUALAN TIKET
$sql	= "SELECT
            KodeJurusan , KodeCabangAsal, KodeCabangTujuan,
            IS_NULL(COUNT(IF(PetugasPenjual=0 AND FlagBatal!=1,NoTiket,NULL)),0) AS TotalPenumpangO,
            IS_NULL(SUM(IF(PetugasPenjual=0 AND FlagBatal!=1,Total,0)),0) AS TotalPenjualanTiket
	        FROM tbl_reservasi JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
	        WHERE (DATE($date_filter) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND CetakTiket = 1
	        $kondisi_cabang
	        GROUP BY KodeJurusan ORDER BY KodeJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}
while ($row = $db->sql_fetchrow($result)){
    $data_tiket_total[$row['KodeJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

$total_t 	= 0;
$total_s 	= 0;
$total_p 	= 0;

while ($row = $db->sql_fetchrow($result_laporan)){
    $temp_array[$idx]['KodeJurusan']			= $row['KodeJurusan'];
    $temp_array[$idx]['CabangAsal']				= $row['KodeCabangAsal'];
    $temp_array[$idx]['CabangTujuan']			= $row['KodeCabangTujuan'];
    $temp_array[$idx]['TotalTiket']				= $data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO'];
    $temp_array[$idx]['TotalPenjualanTiket']	= $data_tiket_total[$row['KodeJurusan']]['TotalPenjualanTiket'];
    $temp_array[$idx]['Komisi']					= ($data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO']*5000);

    $total_t	+= $data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO'];
    $total_p	+= ($data_tiket_total[$row['KodeJurusan']]['TotalPenjualanTiket']);
    $total_s	+= ($data_tiket_total[$row['KodeJurusan']]['TotalPenjualanTiket'])-(($data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO']*5000));

    $idx++;
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);

//judul
$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Jurusan Online dari'.$tanggal_mulai." sampai dengan ".$tanggal_akhir);

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Jurusan');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Cabang Asal');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Cabang Tujuan');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Penjualan Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Total Setor');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

//BODY
$idx_row = 4;
$i = 0;
while($i < count($temp_array)){

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $temp_array[$i]['KodeJurusan']);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$i]['CabangAsal']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$i]['CabangTujuan']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$i]['TotalTiket']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$i]['TotalPenjualanTiket']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$i]['Komisi']);

    $idx_row++;
    $i++;
}

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':C'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Jumlah');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $total_p);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $total_t);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $total_s);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
if ($idx_row>0){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Omzet Jurusan Online dari '.$tanggal_mulai." sampai ".$tanggal_akhir);
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
?>