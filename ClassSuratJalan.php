<?php
$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!="" && !$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true);
	exit; 
}
//#############################################################################

class SuratJalan{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function SuratJalan(){
		$this->ID_FILE="C-SJ";
	}
	
	//BODY
	
	function generateNoSuratJalan(){
		
		/*
		Desc	:mengenearate nomor surat jalan dan ditambahkan algoritma agar nomor tidak bentrok
		*/
		
		//kamus
		global $db;
	
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
	
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
	
	
		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$no_sj	= "SJ".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
		
			$sql = "SELECT COUNT(1) FROM tbl_surat_jalan WHERE NoSJ='$no_sj'";
						
			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE ".__LINE__);
			}
			
			$row=$db->sql_fetchrow($result);
		
		}while($row[0]>0);
			
		
		return $no_sj;
		
	}//  END generateNoSuratJalan
	
	function setComboPool($pool_dipilih = ""){
	
		$opt	="";
		
		$daftar_pool	= array("RAWA BOKOR","CIHAMPELAS");
		
		for($idx=0;$idx<count($daftar_pool);$idx++){
			
			$selected=($pool_dipilih!=$idx)?"":"selected";
			$opt .="<option $selected value=$idx>$daftar_pool[$idx]</option>";
		}
		
		return $opt;
	}
	
	function setComboMobil($kode_kendaraan){
		//SET COMBO MOBIL
		global $db;
		global $Mobil;
				
		$result=$Mobil->ambilDataForComboBox();
		$selected_default	= ($kode_kendaraan!="")?"":"selected";
	
		$opt_mobil="<option value='' $selected_default>- silahkan pilih kendaraan  -</option>";
		
		if($result){
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
				$opt_mobil .="<option value='$row[KodeKendaraan]|$row[NoPolisi]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[JumlahKursi] Seat $row[Merek] $row[Jenis]</option>";
			}
		}
		else{
			echo("err :".__LINE__);exit;
		}		
		
		return $opt_mobil;
	}//END SET COMBO MOBIL
	
	function setComboSopir($kode_sopir_dipilih){
		//SET COMBO SOPIR
		global $db;
		global $Sopir;
				
		$result=$Sopir->ambilData("","Nama,Alamat","ASC");
		
		$selected_default	= ($kode_sopir_dipilih!="")?"":"selected";
		$opt_sopir="<option value='' $selected_default>- silahkan pilih sopir  -</option>";
			
		if($result){
			while ($row = $db->sql_fetchrow($result)){
				$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
				$opt_sopir .="<option value='$row[KodeSopir]|$row[Nama]' $selected>$row[Nama] ($row[KodeSopir])</option>";
			}
		}
		else{
			echo("Err :".__LINE__);exit;
		}		
		return $opt_sopir;
	}//END SET COMBO SOPIR
	
	function tambahHeader(
		$tgl_sj,$waktu_sj,$id_pool,
		$id_petugas,$nama_petugas,$no_body,
		$no_polisi,$kode_sopir,$nama_sopir,
		$jumlah_trip){
	  
		/*
		desc: menyimpan data header surat jalan serta mengambalikan (return) no sj yang berhasil disimpan
		*/
		
		//kamus
		global $db;
		
		//mengenerate nomor surat jalan
		$no_sj	= $this->generateNoSuratJalan();
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$sql	=
			"INSERT INTO tbl_surat_jalan(
				NoSJ,TglSJ,WaktuSJ,
				WaktuCetak,IdPool,IdPetugas,
				NamaPetugas,NoBody,NoPolisi,
				KodeSopir,NamaSopir,JumlahTrip)
			VALUES(
				'$no_sj','$tgl_sj','$waktu_sj',
				NOW(),'$id_pool','$id_petugas',
				'$nama_petugas','$no_body','$no_polisi',
				'$kode_sopir','$nama_sopir','$jumlah_trip')";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $no_sj;
	}
	
	function tambahBiaya(
		$no_sj,$jenis_biaya,$coa,
		$id_jurusan,$kode_jurusan,$nama_biaya,
		$jumlah){
	  
		/*
		desc: menyimpan data detail biaya per surat jalan
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$sql	=
			"INSERT INTO tbl_surat_jalan_biaya(
				NoSJ,JenisBiaya,COA,
				IdJurusan,KodeJurusan,NamaBiaya,
				Jumlah)
			VALUES(
				'$no_sj','$jenis_biaya','$coa',
				'$id_jurusan','$kode_jurusan','$nama_biaya',
				'$jumlah')";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $sql$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapusHeader($no_sj){
	  
		/*
		desc: menghapus header surat jalan yang juga menghapus detail biaya surat jalan
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA DARI DATABASE
		$sql =
			"DELETE FROM tbl_surat_jalan
			WHERE NoSJ IN($no_sj);";
					
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
			return false;
		}
		
		//MENGHAPUS DATA DARI DATABASE
		$sql =
			"DELETE FROM tbl_surat_jalan_biaya
			WHERE NoSJ IN($no_sj);";
					
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
			return false;
		}
		
		return true;
	}//end hapusHeader
	
	function batalkanSuratJalan($no_sj,$pembatal,$nama_pembatal){
	  
		/*
		desc: memberikan flag batal pada header surat jalan
		*/
		
		//kamus
		global $db;
		
		//MENGUBAH STATUS ISBATAL=1
		$sql =
			"UPDATE tbl_surat_jalan
			SET IsBatal=1,Pembatal='$pembatal',NamaPembatal='$nama_pembatal',WaktuBatal=NOW()
			WHERE NoSJ='$no_sj';";
					
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
			return false;
		}
		
		return true;
	}//end hapusHeader
	
}
?>