<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPayoutCGS.php');

// SESSION
$id_page = 408;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################


require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER

$cari           = isset($HTTP_POST_VARS["cari"])?$HTTP_POST_VARS["cari"]:$HTTP_GET_VARS["cari"];
$siklus         = isset($HTTP_POST_VARS['siklus'])? $HTTP_POST_VARS['siklus'] : $HTTP_GET_VARS['siklus'];
$fil_bln        = isset($HTTP_POST_VARS['bulan'])?$HTTP_POST_VARS['bulan']:$HTTP_GET_VARS['bulan'];
$fill_thn       = isset($HTTP_POST_VARS['tahun'])?$HTTP_POST_VARS['tahun']:$HTTP_GET_VARS['tahun'];
$order_by       = isset($HTTP_POST_VARS['orderby'])?$HTTP_POST_VARS['orderby']:$HTTP_GET_VARS['orderby'];
$sort           = isset($HTTP_POST_VARS['sort'])?$HTTP_POST_VARS['sort']:$HTTP_GET_VARS['sort'];

$PayoutCGS  = new PayOutCGS();

if($siklus==0){
  $tgl_awal   = 26;
  $tgl_akhir  = 10;
  $bulan_awal = ($fil_bln>1?$fil_bln-1:12);
  $tahun_awal = $fill_thn-($fil_bln>1?0:1);
  $bulan_akhir= $fil_bln;
  $tahun_akhir= $fill_thn;
}else{
  $tgl_awal   = 11;
  $tgl_akhir  = 25;
  $bulan_awal = $fil_bln;
  $tahun_awal = $fill_thn;
  $bulan_akhir= $fil_bln;
  $tahun_akhir= $fill_thn;
}

$periode_awal   = dateparse($tgl_awal."-".$bulan_awal."-".$tahun_awal);
$periode_akhir  = dateparse($tgl_akhir."-".$bulan_akhir."-".$tahun_akhir);

$styleborder = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);


//EXPORT KE MS-EXCEL

$i=1;
$col_map  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
            "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ");

$objPHPExcel = new PHPExcel();

// SHEET 1
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("Sheet1");

//HEADER
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap CGS Periode '.$periode_awal." s.d. ".$periode_akhir);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Kode Sopir');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Nama Sopir');
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Total Trip');
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total CGS');
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jurusan');
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'No.Rekening');
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Bank');
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Cabang Bank');
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Remark');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getStyle('F')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->freezePane('D4');

$result = $PayoutCGS->getDataCGS($siklus,$fil_bln,$fill_thn,$order_by,$sort,"",$cari);

$baris = 3;
$no = 0;

while($data = $db->sql_fetchrow($result)){
  $no++;
  $baris++;
  $idx_col=0;

  $list_jurusan = "";

  if($data["Jurusan"]!="") {
    $arr_jurusan = explode(",", $data["Jurusan"]);

    $arr_count_jurusan = array_count_values($arr_jurusan);

    foreach ($arr_count_jurusan as $key => $value) {
      $list_jurusan .= $key . ":" . $value . "| ";
    }

    $list_jurusan = substr($list_jurusan, 0, -2);
  }

  $remark = $data["IdPayout"]==""?"":"Payout:".dateparseWithTime(FormatMySQLDateToTglWithTime($data["WaktuPayout"]))."| Via:".($data["PayoutVia"]==0?"Cash":"Transfer")."| #Ref:".$data["KodeReferensi"]."| Oleh:".$data["NamaPetugas"];

  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$no);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["KodeSopir"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["Nama"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["TotalTrip"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["TotalCGS"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$list_jurusan);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["NoRek"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["NamaBank"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["CabangBank"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$remark);$idx_col++;

  //$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);

}

$objPHPExcel->getActiveSheet()->getStyle("A3:J".($baris+1))->applyFromArray($styleborder);

$objPHPExcel->getActiveSheet()->mergeCells("A".($baris+1).":C".($baris+1));
$objPHPExcel->getActiveSheet()->setCellValue("A".($baris+1),"TOTAL");
// total trip
$objPHPExcel->getActiveSheet()->setCellValue("D".($baris+1),"=SUM(D4:D$baris)");
//Total CGS
$objPHPExcel->getActiveSheet()->setCellValue("E".($baris+1),"=SUM(E4:E$baris)");

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Rekap CGS Periode '.$periode_awal." s.d. ".$periode_akhir.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>
