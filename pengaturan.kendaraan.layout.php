<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 717;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassLayoutKendaraan.php');

// PARAMETER
$mode 			= getVariabel("mode");

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST
    $cari               = getVariabel("cari");
    $idx_page           = getVariabel("idxpage");
    $order_by           = getVariabel("orderby");
    $sort               = getVariabel("sort");
    $scroll_value       = getVariabel("scrollvalue");

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));
    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    break;

  case 1:
    //SHOW DIALOG TAMBAH DATA
    showDialogTambah();

    break;

  case 1.1:
    //SIMPAN DATA
    $id_layout        = getVariabel("idlayout");
    $kode_layout      = getVariabel("kodelayout");
    $jumlah_baris     = getVariabel("jumlahbaris");
    $jumlah_kolom     = getVariabel("jumlahkolom");
    $kapasitas        = getVariabel("kapasitas");
    $peta_kursi       = str_replace('\\"',"\'",getVariabel("petakursi"));

    echo(prosesSimpan(strtoupper($kode_layout),$jumlah_baris,$jumlah_kolom,$kapasitas,$peta_kursi,$id_layout));

    break;

  case 2:
    //SHOW DATA
    $id_layout      = getVariabel("idlayout");

    showDialogUbah($id_layout);

    break;

  case 3:
    //HAPUS DATA
    $list_id      = getVariabel("listid");

    $list_id = str_replace("\'","'",$list_id);

    echo(hapusData($list_id));

    break;
}

//METHODES & PROCESSES ========================================================================================================================================================================

function showData(){

  global $db,$cari,$order_by,$sort,$idx_page,$template,$VIEW_PER_PAGE,$userdata,$id_page,$scroll_value,$show_dialog_tambah;

  $LayoutKendaraan = new LayoutKendaraan();

  $template->set_filenames(array('body' => 'pengaturan.kendaraan.layout/index.tpl'));

  //cek permission untuk CRUD
  $Permission = new Permission();

  if($Permission->isPermitted($userdata["user_level"],$id_page.".1")){
    $template->assign_block_vars("CRUD_ADD",array());
    $template->assign_block_vars("CRUD_DEL",array());
  }

  $ret_val = $LayoutKendaraan->ambilData($order_by,$sort,$idx_page,$cari);

  if($ret_val["status"]!="OK"){
    echo($ret_val["pesan"]);
  }

  $result = $ret_val["data"];

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd = ($no%2)==0?"even":"odd";

    $template->assign_block_vars(
      'ROW',array(
        'odd'             => $odd,
        'idx'             => $no,
        'no'              => $idx_page*$VIEW_PER_PAGE+$no,
        'idlayout'        => $row["IdLayout"],
        'kodelayout'      => $row["KodeLayout"],
        'jumlahbaris'     => $row["JumlahBaris"],
        'jumlahkolom'     => $row["JumlahKolom"],
        'kapasitas'       => $row["Kapasitas"]
      )
    );

    //ACTION
    $template->assign_block_vars("ROW.ACT_EDIT", array());
    $template->assign_block_vars("ROW.ACT_DEL", array());

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	        => basename(__FILE__),
      'CARI'              => $cari,
      'ORDER'             => $order_by,
      'SORT'              => $sort,
      'IDX_PAGE'          => $idx_page,
      'PAGING'            => $paging,
      'SCROLL_VALUE'      => ($scroll_value==""?0:$scroll_value)
    )
  );

  $template->pparse('body');

} //showData

function showDialogTambah(){
  global $template;

  $template->set_filenames(array('body' => 'pengaturan.kendaraan.layout/detail.tpl'));

  $template->assign_vars(array(
      "JUDUL"	            => "Tambah Template Layout Kendaraan"
    )
  );

  $template->pparse('body');
}//showDialogTambah

function showDialogUbah($id_layout){
  global $template;

  $LayoutKendaraan = new LayoutKendaraan();

  $ret_val  = $LayoutKendaraan->ambilDetailData($id_layout);

  if($ret_val["status"]!="OK"){
    echo($ret_val["pesan"]);
  }

  $data   = $ret_val["data"];

  $template->set_filenames(array('body' => 'pengaturan.kendaraan.layout/detail.tpl'));

  $template->assign_vars(array(
      "JUDUL"	        => "Ubah Template Layout Kendaraan",
      "ID_LAYOUT"	    => $id_layout,
      "KODE_LAYOUT" 	=> $data["KodeLayout"],
      "JUMLAH_BARIS"	=> $data["JumlahBaris"],
      "JUMLAH_KOLOM"	=> $data["JumlahKolom"],
      "KAPASITAS"	    => $data["Kapasitas"],
      "JSON_LAYOUT"	  => $data["PetaKursi"]
    )
  );

  $template->pparse('body');
}//showDialogUbah

function prosesSimpan($kode_layout,$jumlah_baris,$jumlah_kolom,$kapasitas,$peta_kursi,$id_layout=""){

  $LayoutKendaraan  = new LayoutKendaraan();

  if($id_layout==""){
    //TAMBAH DATA BARU
    return json_encode($LayoutKendaraan->tambah($kode_layout,$jumlah_baris,$jumlah_kolom,$kapasitas,$peta_kursi));
  }
  else{
    //MENGUBAH DATA
    return json_encode($LayoutKendaraan->ubah($id_layout,$kode_layout,$jumlah_baris,$jumlah_kolom,$kapasitas,$peta_kursi));
  }

}

function hapusData($list_id){
  $LayoutKendaraan = new LayoutKendaraan();

  $return = $LayoutKendaraan->hapus($list_id);

  return json_encode($return);

} //hapusData
?>