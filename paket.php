<?php
//HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 223;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassStokKardus.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassVoucherPaket.php');
include($adp_root_path . 'ClassPelanggan.php');

// PARAMETER
$mode 			= getVariabel('mode');

//INIT
$mode         = $mode==""?0:$mode;
$charge_jemput= 10000;
$charge_antar = 10000;

//CREATE INSTANCE
$Paket			  = new Paket();
$StokKardus	  = new StokKardus();
$Jadwal		    = new Jadwal();
$Jurusan		  = new Jurusan();
$VoucherPaket	= new VoucherPaket();
$StokKardus	  = new StokKardus();

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    paketHome();
    include($adp_root_path . 'includes/page_tail.php');

    exit;

  case 1:
    //SET PILIHAN POINT ASAL
    $kota_asal 			= getVariabel('kota');

    setPointAsal($kota_asal);

    exit;

  case 2:
    //SET PILIHAN POINT TUJUAN
    $cabang_asal 			= getVariabel('cabangasal');

    setPointTujuan($cabang_asal);

    exit;

  case 3:
    //SET PILIHAN JADWAL
    $tgl_berangkat  = getVariabel('tanggal');
    $jurusan 			  = getVariabel('jurusan');

    setJadwal($tgl_berangkat,$jurusan);

    exit;

  case 4:
    //SET LAYOUT PAKET
    $tgl_berangkat  = getVariabel('tanggal');
    $kode_jadwal 	  = getVariabel('jadwal');
    $is_mutasi_mode = getVariabel('ismutasimode');

    setLayout($tgl_berangkat,$kode_jadwal,$is_mutasi_mode);

    break;

  case 5:
    //SET POPUP PILIH PEMBAYARAN
    $kode_voucher   = getVariabel('kodevoucher');
    $tgl_berangkat  = getVariabel('tglberangkat');
    $jurusan        = getVariabel('jurusan');
    $kode_jadwal    = getVariabel('kodejadwal');
    $layanan        = getVariabel('layanan');
    $jenis_kardus   = getVariabel('jeniskardus');
    $berat          = getVariabel('berat');
    $berat          = getVariabel('berat');
    $is_dijemput    = getVariabel('isdijemput');
    $is_diantar     = getVariabel('isdiantar');

    setPopupPilihanPembayaran($tgl_berangkat,$jurusan,$kode_jadwal,$layanan,$jenis_kardus,$berat,$is_dijemput,$is_diantar,$kode_voucher);

    break;

  case 6:
    //AMBIL DAFTAR HARGA PAKET
    $jurusan   = getVariabel('jurusan');

    setHargaPaket($jurusan);

    break;

  case 7:
    //PROSES SIMPAN TRANSAKSI

    $parameter["kode_voucher"]        = getVariabel('kodevoucher');
    $parameter["tgl_berangkat"]       = getVariabel('tglberangkat');
    $parameter["jurusan"]             = getVariabel('jurusan');
    $parameter["kode_jadwal"]         = getVariabel('kodejadwal');
    $parameter["telp_pengirim"]       = getVariabel('telppengirim');
    $parameter["nama_pengirim"]       = getVariabel('namapengirim');
    $parameter["alamat_pengirim"]     = getVariabel('alamatpengirim');
    $parameter["paket_dijemput"]      = getVariabel('isdijemput');
    $parameter["telp_penerima"]       = getVariabel('telppenerima');
    $parameter["nama_penerima"]       = getVariabel('nama_penerima');
    $parameter["alamat_penerima"]     = getVariabel('alamatpenerima');
    $parameter["paket_diantar"]       = getVariabel('isdiantar');
    $parameter["berat"]               = getVariabel('berat');
    $parameter["layanan"]             = getVariabel('layanan');
    $parameter["jenis_kardus"]        = getVariabel('jeniskardus');
    $parameter["cara_bayar"]          = getVariabel('carabayar');
    $parameter["keterangan"]          = getVariabel('keterangan');
    $parameter["jenis_pembayaran"]    = getVariabel('jenisbayar');
    $parameter["cabang_tujuan_akhir"] = getVariabel('tujuanakhir');

    simpanTransaksi($parameter);

    break;

  case 8:
    //SHOW DETAIL PAKET

    $no_resi   = getVariabel('noresi');

    getDetailPaket($no_resi);

    break;

  case 9:
    //CARI PAKET
    $cari   = getVariabel('cari');

    cariPaketPelanggan($cari);

    break;

  case 10:
    //SET DIALOG REQUEST CETAK ULANG RESI

    $no_resi   = getVariabel('noresi');

    setDialogReqOTPCetakUlang($no_resi);

    break;

  case 10.1:
    //SEND REQUEST OTP CETAK
    $no_resi   = getVariabel('noresi');
    $alasan    = getVariabel('alasan');

    sendRequestOTPCetakUlang($no_resi,$alasan);

    break;

  case 10.2:
    //SET DIALOG INPUT OTP CETAK ULANG

    $no_resi   = getVariabel('noresi');

    setDialogOTPCetakUlang($no_resi);

    break;

  case 10.3:
    //VERIFIKASI OTP CETAK ULANG
    $no_resi   = getVariabel('noresi');
    $otp       = getVariabel('otp');

    verifikasiOTPCetakUlang($no_resi,$otp);

    break;

  case 11:
    //SET DIALOG REQUEST PEMBATALAN

    $no_resi   = getVariabel('noresi');

    setDialogReqOTPBatal($no_resi);

    break;

  case 11.1:
    //SEND REQUEST OTP BATAL
    $no_resi   = getVariabel('noresi');
    $alasan    = getVariabel('alasan');

    sendRequestOTPBatal($no_resi,$alasan);

    break;

  case 11.2:
    //SET DIALOG INPUT OTP PEMBATALAN

    $no_resi   = getVariabel('noresi');

    setDialogOTPBatal($no_resi);

    break;

  case 11.3:
    //VERIFIKASI OTP PEMBATALAN
    $no_resi   = getVariabel('noresi');
    $otp       = getVariabel('otp');

    prosesBatalkanTransaksi($no_resi,$otp);

    break;

  case 12:
    //SET DIALOG REQUEST MUTASI

    $no_resi   = getVariabel('noresi');

    setDialogReqOTPMutasi($no_resi);

    break;

  case 12.1:
    //SEND REQUEST OTP MUTASI
    $no_resi   = getVariabel('noresi');
    $alasan    = getVariabel('alasan');

    sendRequestOTPMutasi($no_resi,$alasan);

    break;

  case 12.2:
    //SET DIALOG INPUT OTP MUTASI

    $no_resi   = getVariabel('noresi');

    setDialogOTPMutasi($no_resi);

    break;

  case 12.3:
    //VERIFY OTP MUTASI
    $no_resi      = getVariabel('noresi');
    $otp          = getVariabel('otp');

    verifikasiOTPMutasi($no_resi,$otp);

    break;

  case 12.4:
    //PROSES MUTASI
    $no_resi      = getVariabel('noresi');
    $jurusan      = getVariabel('jurusan');
    $tgl_berangkat= getVariabel('tanggal');
    $kode_jadwal  = getVariabel('jadwal');
    $otp          = getVariabel('otp');
    $alasan       = getVariabel('alasan');

    prosesMutasi($no_resi,$jurusan,$tgl_berangkat,$kode_jadwal,$otp);

    break;

  case 13:
    //MENGAMBIL DATA PELANGGAN BERDASARKAN NOMOR TELEPON
    $no_telp   = getVariabel('notelp');

    getDataPelanggan($no_telp);

    break;

  case 14:
    //SET COMBO TUJUAN
    $cari   = getVariabel('filter');
    $kota   = getVariabel('kota');

    setListCabangTujuanAkhir($cari,$kota);

    break;

  case 15:
    //SET DIALOG REQUEST CETAK ULANG MANIFEST

    $no_manifest   = getVariabel('nomanifest');

    setDialogReqOTPCetakUlangManifest($no_manifest);

    break;

  case 15.1:
    //SEND REQUEST OTP CETAK MANIFEST
    $no_manifest= getVariabel('nomanifest');
    $alasan     = getVariabel('alasan');

    sendRequestOTPCetakUlangManifest($no_manifest,$alasan);

    break;

  case 15.2:
    //SET DIALOG INPUT OTP CETAK ULANG MANIFEST

    $no_manifest   = getVariabel('nomanifest');

    setDialogOTPCetakUlangManifest($no_manifest);

    break;

  case 15.3:
    //VERIFIKASI OTP CETAK ULANG MANIFEST
    $no_manifest  = getVariabel('nomanifest');
    $otp          = getVariabel('otp');

    verifikasiOTPCetakUlangManifest($no_manifest,$otp);

    break;

  case 16:
    //SHOW DIALOG AMBIL PAKET
    $no_resi  = getVariabel('noresi');
    $is_otp   = getVariabel('isotp');

    setDialogAmbilPaket($no_resi,$is_otp);

    break;

  case 16.1:
    //SHOW DIALOG AMBIL PAKET
    $no_resi        = getVariabel('noresi');
    $nama_pengambil = getVariabel('namapengambil');
    $telp_pengambil = getVariabel('telppengambil');
    $ktp_pengambil  = getVariabel('ktppengambil');
    $otp            = getVariabel('otp');

    ambilPaket($no_resi,$nama_pengambil,$telp_pengambil,$ktp_pengambil,$otp);

    break;

  case 17:
    //SHOW DIALOG PENERIMAAN PAKET
    $no_resi  = getVariabel('noresi');

    setDialogTerimaPaket($no_resi);

    break;

  case 17.1:
    //SHOW DIALOG TERIMA PAKET
    $no_resi          = getVariabel('noresi');
    $nama_penerima    = getVariabel('namapenerima');
    $telp_penerima    = getVariabel('telppenerima');

    terimaPaket($no_resi,$nama_penerima,$telp_penerima);

    break;

  case 18:
    //SHOW STOK KARDUS

    showStokKardus();

    break;

}

//METHODES & PROCESS ========================================================================================================================================================================

function paketHome(){
  global $db,$template,$id_page,$Paket,$config,$StokKardus,$charge_jemput,$charge_antar;

  $template->set_filenames(array('body' => 'paket/index.tpl'));

  $template->assign_vars (
    array(
      'BCRUMP'    					=> setBcrump($id_page),
      'OPT_KOTA'						=> setComboKota("BANDUNG"),
      'U_BIAYA_PETTY_CASH'	=> "biaya_petty_cash.php",
      'U_BIAYA_TAMBAHAN'		=> "biaya_tambahan.php",
      'U_LAPORAN_UANG'			=> "laporan_rekap_uang_user.php",
      'U_LAPORAN_PENJUALAN'	=> "laporan_penjualan_paket_user.php",
      'CHARGE_JEMPUT'       => $charge_jemput,
      'CHARGE_ANTAR'        => $charge_antar
    )
  );

  //MENGAMBIL LIST KOTA KEBERANGKATAN
  $result  = $Paket->getListKota();
  $i=0;
  while($row=$db->sql_fetchrow($result)){
    $template->assign_block_vars("OPT_KOTA", array(
      "kota" => $row["NamaKota"],
      "idx"  => $i
    ));
    $i++;
  }

  //LIST STOK KARDUS
  $result  = $StokKardus->getListJenisKardus();
  while($row=$db->sql_fetchrow($result)){
    $template->assign_block_vars("LIST_KARDUS", array(
      "value" => $row["IdJenisKardus"],
      "text"  => $row["NamaJenisKardus"]
    ));
    $i++;
  }

  foreach($config["layanan_paket"] as $key=>$value){
    $template->assign_block_vars("LIST_LAYANAN",array(
      "value" => $key,
      "text"  => $value
    ));
  }

  $template->pparse('body');
}

function setPointAsal($kota_asal){
  global $db,$Paket;

  $result = $Paket->getListPointAsal($kota_asal);

  $ret_str  ="";

  $i=0;
  while($row=$db->sql_fetchrow($result)){
    $ret_str  .= "<div class=\"resvlistpilihan\" id=\"pointasal$i\" onclick=\"setCabangTujuan('".$row["KodeCabang"]."',this.id);\">".$row["Nama"]."</div><br>";
    $i++;
  }

  if($i<=0){
    $ret_str  = "<span style='background:yellow;display:inline-block;width: 100%;text-transform: uppercase'>Tidak ada keberangkatan dari kota ini</span>";
  }

  echo($ret_str);

}

function setPointTujuan($cabang_asal){
  global $db,$Paket;

  $result = $Paket->getListPointTujuan($cabang_asal);

  $ret_str  ="";

  $i    = 0;
  $kota = "";
  while($row=$db->sql_fetchrow($result)){
    if($kota!=$row["Kota"]){
      $ret_str .= "<span style='font-weight: bold;font-size: 14px;border-bottom: 1px solid #cccccc;width:100%;display: inline-block;'>".$row["Kota"]."</span>";
      $kota     = $row["Kota"];
    }
    $ret_str  .= "<div class=\"resvlistpilihan\" id=\"pointtujuan$i\" onclick=\"setJadwal('".$row["IdJurusan"]."',this.id);\">".$row["Tujuan"]." (".$row["KodeJurusan"].")</div><br>";
    $i++;
  }

  if($i<=0){
    $ret_str  = "<span style='background:yellow;display:inline-block;width: 100%;text-transform: uppercase'>Tidak ada tujuan ke point ini</span>";
  }

  echo($ret_str);

}

function setJadwal($tgl_berangkat,$jurusan){
  global $db,$Jadwal,$Permission,$userdata,$Paket;

  $result = $Jadwal->setListJadwalReservasi(FormatTglToMySQLDate($tgl_berangkat),$jurusan);

  $ret_str  ="";

  $i            = 0;
  $jam_berangkat= "";

  while($row=$db->sql_fetchrow($result)){
    if($jam_berangkat!=$row["JamBerangkat"]){
      $ret_str .= "<span style='font-weight: bold;font-size: 14px;border-bottom: 1px solid #cccccc;width:100%;display: inline-block;'>".$row["JamBerangkat"]."</span>";
      $jam_berangkat     = $row["JamBerangkat"];
    }

    if($row["JadwalDioperasikan"] && $row["VerifyWaktu"]) {
      $action        = "onclick=\"setLayout('$tgl_berangkat','".$row["KodeJadwal"]."',this.id);\"";
      $status_jadwal = "";
      $class_off     = "";
    }
    elseif(!$row["JadwalDioperasikan"]){
      $action        = !$Permission->isPermitted($userdata["user_level"],201.1)?"":"onclick=\"setLayout('$tgl_berangkat','".$row["KodeJadwal"]."',this.id);\"";
      $status_jadwal = " (OFF)";
      $class_off      = "off";
    }
    elseif(!$row["VerifyWaktu"]){
      $action        = !$Permission->isPermitted($userdata["user_level"],201.1)?"":"onclick=\"setLayout('$tgl_berangkat','".$row["KodeJadwal"]."',this.id);\"";
      $status_jadwal = " (PASSED)";
      $class_off      = "off";
    }

    $ret_str  .= "<div class=\"resvlistpilihan$class_off\" id=\"jadwal$i\" $action>".$row["KodeJadwal"].$status_jadwal."</div><br>";
    $i++;
  }

  if($i<=0){
    $ret_str  = "<span style='background:yellow;display:inline-block;width: 100%;text-transform: uppercase'>Tidak ada jadwal keberangkatan</span>";
  }

  $data_cabang_tujuan = $Paket->getDataCabangTujuan($jurusan);

  $ret_str    = "<input type=\"hidden\" id=\"kotatujuan\" value=\"$data_cabang_tujuan[Kota]\"/><input type=\"hidden\" id=\"namacabangtujuan\" value=\"$data_cabang_tujuan[Nama]\"/><input type=\"hidden\" id=\"kodecabangtujuan\" value=\"$data_cabang_tujuan[KodeCabang]\"/>".$ret_str;

  echo($ret_str);

}

function setLayout($tgl_berangkat,$kode_jadwal,$is_mutasi_mode=0){
  global $db,$config,$template,$Paket,$Jadwal,$Permission,$userdata,$id_page;

  $cara_bayar = array("tunai","langganan","bayar ditujuan");

  $template->set_filenames(array('body' => 'paket/listpaket.tpl'));

  //CEK STATUS JADWAL
  $data_status_jadwal  = $Jadwal->getStatusJadwal($kode_jadwal,FormatTglToMySQLDate($tgl_berangkat));

  if($Permission->isPermitted($userdata["user_level"],201.1) || ($data_status_jadwal["JadwalDioperasikan"] && $data_status_jadwal["VerifyWaktu"])){
    //JIKA MEMILIKI AKSES ATAUPUN TANGGAL KEBERANGKATAN LEBIH BESAR ATAU SAMA DENGAN TGL SEKARANG

    if(!$data_status_jadwal["JadwalDioperasikan"]) {
      $template->assign_block_vars("ALERT_JADWALOFF", array("pesan"=>"jadwal ini tidak dioperasikan!"));
    }

    $template->assign_block_vars("SHOW_HEADER_LAYOUT", array());

    $data_summary = $Paket->ambilSummary(FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal);

    $jadwal_utama = $data_summary["KodeJadwalUtama"]==""?"":" (Induk:".$data_summary["KodeJadwalUtama"].")";

    $template->assign_vars (
      array(
        'NO_MANIFEST_PAKET'   => ($data_summary["NoManifestPaket"]==""?"<span style='text-weight:bold;color:red;'>-BELUM MANIFEST-</span>":"<b>".$data_summary["NoManifestPaket"]."</b><br>&nbsp;&nbsp;".dateparseWithTime(FormatMySQLDateToTglWithTime($data_summary["WaktuCetakManifestPaket"]))),
        'NO_MANIFEST_INDUK'   => ($data_summary["NoManifestInduk"]==""?"<b>-BELUM MANIFEST-</b>":"<b>".$data_summary["NoManifestInduk"]."</b><br>&nbsp;&nbsp;".dateparseWithTime(FormatMySQLDateToTglWithTime($data_summary["WaktuCetakManifestInduk"]))),
        'NO_UNIT'    			    => ($data_summary["KodeKendaraan"]==""?"-N/A-":"<b>".$data_summary["KodeKendaraan"]."</b>"),
        'NO_PLAT'    			    => ($data_summary["NoPolisi"]==""?"":$data_summary["NoPolisi"]),
        'DRIVER'    			    => ($data_summary["Driver"]==""?"-":"<b>".$data_summary["Driver"]."</b>"),
        'NIK'    			        => ($data_summary["KodeDriver"]==""?"":"(".$data_summary["KodeDriver"].")"),
        'TGL_BERANGKAT'    		=> dateparse($tgl_berangkat),
        'KODE_JADWAL'    		  => $kode_jadwal.$jadwal_utama,
        'TOTAL_PAKET'         => $data_summary["TotalPaket"],
        'OMZET_PAKET'         => "Rp.".number_format($data_summary["TotalOmzet"],0,",",".")
      )
    );

    //MENGAMBIL SEMUA PAKET DARI CABANG YANG TERKONEKSI DENGAN CABANG INI
    //MENGAMBIL KODE JADWAL UTAMA
    $data_jadwal  = $Jadwal->ambilDataDetail($kode_jadwal);
    $kode_jadwal_utama = !$data_jadwal["FlagSubJadwal"]?$kode_jadwal:$data_jadwal["KodeJadwalUtama"];

    $res_list_jadwal_dan_pickup  = $Jadwal->ambilListJadwalDanPickUpPaket($kode_jadwal_utama);

    $i=1;

    while($row_jadwal=$db->sql_fetchrow($res_list_jadwal_dan_pickup)){

      $result = $Paket->ambilListPaket(FormatTglToMySQLDate($tgl_berangkat),$row_jadwal["KodeJadwal"]);

      while($row=$db->sql_fetchrow($result)) {
        $template->assign_block_vars(
          'DAFTAR_BARANG', array(
            'no'            => $i,
            'resi'          => $row["NoResi"],
            'jenislayanan'  => $config["layanan_paket"][$row["Layanan"]],
            'poinberangkat' => ($kode_jadwal==$row_jadwal["KodeJadwal"]?"paket ada di cabang ini":"paket di cabang ".$row_jadwal["NamaCabangAsal"]),
            'transit'       => (!$row["IsPaketTransit"]?"":"transit ke ".$row["NamaCabangTujuanAkhir"]),
            'nama_pengirim' => substr($row['NamaPengirim'], 0, 15),
            'nama_penerima' => substr($row['NamaPenerima'], 0, 15),
            'berat'         => $row["Berat"],
            'is_dijemput'   => ($row["IsDijemput"]?"| DIJEMPUT":""),
            'is_diantar'    => ($row["IsDiantar"]?"| DIANTAR":""),
            'cara_bayar'    => $cara_bayar[$row["CaraPembayaran"]],
            'harga'         => number_format($row['HargaSebelumDiscount']+$row["ChargeJemput"]+$row["ChargeAntar"], 0, ",", "."),
            'diskon'        => number_format($row['Discount'], 0, ",", "."),
            'biaya'         => number_format($row['HargaPaket']+$row["ChargeJemput"]+$row["ChargeAntar"], 0, ",", ".")
          )
        );
        $i++;
      }
    }

    if(!$is_mutasi_mode) {
      if(($data_summary["NoManifestInduk"]!="" && $data_summary["NoManifestPaket"]=="") || $Permission->isPermitted($userdata["user_level"],$id_page.".4")) {
        $template->assign_block_vars("BTN_MANIFEST", array("action"=>"cetakManifest();"));
      }
      elseif($data_summary["NoManifestInduk"]!=""){

        //CETAK ULANG MANIFEST
        $data_manifest  = $Paket->ambilDataManifest($data_summary["NoManifestPaket"]);

        if(!$data_manifest["RequestCetakUlang"]) {
          $act_cetak_ulang = "showDialogReqOTPCetakManifest('$data_summary[NoManifestPaket]');";
        }
        else{
          $act_cetak_ulang = "showDialogOTPCetakManifest('$data_summary[NoManifestPaket]');";
        }

        //SHOW BUTTON
        $template->assign_block_vars("BTN_MANIFEST", array("action"=>$act_cetak_ulang));
      }
    }
    else{
      $template->assign_block_vars("BTN_MUTASI", array());
    }
  }
  else{
    $template->assign_block_vars("ALERT_JADWALOFF",array("pesan"=>"jadwal ini tidak dioperasikan, atau anda tidak memiliki akses terhadap jadwal ini!"));
  }

  $template->pparse('body');
}

function setPopupPilihanPembayaran($tgl_berangkat,$jurusan,$kode_jadwal,$layanan,$jenis_kardus,$berat,$is_dijemput,$is_diantar,$kode_voucher=""){
  global $template,$VoucherPaket,$charge_jemput,$charge_antar,$StokKardus,$userdata,$config;

  $template->set_filenames(array('body' => 'paket/dialogpembayaran.tpl'));

  //CEK STOK KARDUS
  if($jenis_kardus!=0){
    //MEMERIKSA STOK
    $stok_kardus  = $StokKardus->ambilStokKardus($userdata["KodeCabang"],$jenis_kardus);
    if($stok_kardus<=0){
      //STOK KARDUS KOSONG
      $template->assign_block_vars("DLG_KETERANGAN",array("teks"=>"STOK KARDUS TIDAK MENCUKUPI"));
      $template->pparse('body');
      exit;
    }

    if($stok_kardus<=$config["stok_kardus_min"]){
      $ket_stok_kardus_kritis = "<br><span style='color:red;font-weight: bold;'>'perhatian! Stok kardus tersisa: $stok_kardus lagi'</span>";
    }
  }


  $ada_pembayaran=true;

  //CEK VOUCHER
  if($kode_voucher!="") {
    //JIKA KODE VOUCHER DIINPUT
    $status_voucher = $VoucherPaket->verifyVoucher($kode_voucher,FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal);

    if($status_voucher["status"]){ //status voucher valid
      $harga_paket  = hitungHargaPaket($jurusan,$layanan,$berat)+($is_dijemput?$charge_jemput:0)+($is_diantar?$charge_antar:0); //mengambil harga paket

      if(!$status_voucher["IsHargaTetap"]){
        $harga_setelah_diskon = $harga_paket-($status_voucher["NilaiVoucher"]>1?$status_voucher["NilaiVoucher"]:$harga_paket*$status_voucher["NilaiVoucher"]);

        if($harga_setelah_diskon>0) {
          $keterangan = $status_voucher["keterangan"]." Jumlah yang harus dibayar adalah Rp.".number_format($harga_setelah_diskon,0,",",".");
        }
        else{
          $ada_pembayaran=false;
          $keterangan = "Pembayaran menggunakan <i>free voucher</i>, klik 'PROSES' untuk menyelesaikan transaksi ini";
          $template->assign_block_vars("DLG_LANJUTKAN",array());
        }
      }
      else{
        //VOUCHER JENISNYA ADALAH PEMBAYARAN DENGAN SATU NOMINAL
        $keterangan  = $status_voucher["keterangan"];
      }

    }
    else{
      $ada_pembayaran= false;
      $keterangan    = $status_voucher["keterangan"];
    }

  }

  $template->assign_block_vars("DLG_KETERANGAN",array("teks"=>$keterangan.$ket_stok_kardus_kritis));

  if($ada_pembayaran) {
    $template->assign_block_vars("DLG_BAYAR_TUNAI", array());
    $template->assign_block_vars("DLG_BAYAR_EDC", array());
  }

  $template->pparse('body');
}

function hitungHargaPaket($jurusan,$layanan,$berat){
  global $Paket;

  $daftar_harga = $Paket->ambilDaftarHarga($jurusan);

  $harga_kilo_pertama = $daftar_harga["HargaPaket".$layanan."KiloPertama"];
  $harga_kilo_berikut = $daftar_harga["HargaPaket".$layanan."KiloBerikut"];

  return $harga_kilo_pertama+((($berat-1>=0)?$berat-1:0)*$harga_kilo_berikut);
}

function setHargaPaket($jurusan){
  global $Paket;

  $daftar_harga = $Paket->ambilDaftarHarga($jurusan);

  $str_return = "";

  for($i=1;$i<=6;$i++){
    $harga_kilo_pertama = $daftar_harga["HargaPaket".$i."KiloPertama"];
    $harga_kilo_berikut = $daftar_harga["HargaPaket".$i."KiloBerikut"];

    $str_return .=
      "hargakgpertama".$i.".value=".$harga_kilo_pertama.";
      hargakgberikutnya".$i.".value=".$harga_kilo_berikut.";";
  }

  echo($str_return);
}

function simpanTransaksi($parameter){
  global $Paket,$Jadwal,$Jurusan,$VoucherPaket,$userdata,$charge_jemput,$charge_antar,$StokKardus,$Permission;

  $Pelanggan  = new Pelanggan();

  foreach($parameter as $key=>$value){
    //mengexplode paramter dikirim menjadi variabel
    $$key = $value;
  }

  //CEK STATUS JADWAL
  $data_jadwal  = $Jadwal->getStatusJadwal($kode_jadwal,FormatTglToMySQLDate($tgl_berangkat));

  if(!$Permission->isPermitted($userdata["user_level"],201.1) && !($data_jadwal["JadwalDioperasikan"] && $data_jadwal["VerifyWaktu"])) {
    //JIKA TIDAK MEMILIKI AKSES ATAUPUN TANGGAL KEBERANGKATAN SUDAH LALU ATAU JADWAL DI CLOSE

    $ret_val["status"]  = "GAGAL";
    $ret_val["pesan"]   = "AKSES DITOLAK:".__LINE__;
    echo(json_encode($ret_val));exit;

  }

    //CEK STOK KARDUS
  if($jenis_kardus!=0){
    //MEMERIKSA STOK
    if($StokKardus->ambilStokKardus($userdata["KodeCabang"],$jenis_kardus)<=0){
      //STOK KARDUS KOSONG
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Stok kardus tidak mencukupi";
      return $ret_val;
    }
  }

  $harga_sebelum_diskon  = hitungHargaPaket($jurusan,$layanan,$berat); //mengambil harga paket
  $harga_paket           = $harga_sebelum_diskon;

  if($kode_voucher!="") {

    //JIKA KODE VOUCHER DIINPUT
    $status_voucher = $VoucherPaket->verifyVoucher($kode_voucher,FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal);

    if($status_voucher["status"]){ //status voucher valid

      if(!$status_voucher["IsHargaTetap"]){
        $discount    = $status_voucher["NilaiVoucher"]>1?$status_voucher["NilaiVoucher"]:$harga_sebelum_diskon*$status_voucher["NilaiVoucher"];
        $harga_paket = $harga_sebelum_diskon-$discount;

        if($harga_paket<0){
          $harga_paket      = 0;
          $discount         = $harga_sebelum_diskon;
          $jenis_pembayaran = 3;
        }
      }
      else{
        //VOUCHER JENISNYA ADALAH PEMBAYARAN DENGAN SATU NOMINAL
        $harga_paket  = $status_voucher["NilaiVoucher"];
        $discount     = $harga_sebelum_diskon-$harga_paket;
      }

      $jenis_discount   = "VOUCHER:$kode_voucher";
    }
    else{
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = $status_voucher["keterangan"];
      echo(json_encode($ret_val));exit;
    }
  }

  $data_jadwal  = $Jadwal->ambilDataDetail($kode_jadwal);

  //MEMERIKSA APAKAH PAKET ADALAH TRANSIT
  $data_jurusan = $Jurusan->ambilDataDetail($jurusan);

  if($cabang_tujuan_akhir==$data_jurusan["KodeCabangTujuan"]) {
    $is_paket_transit = 0;
  }
  else{
    $is_paket_transit = 1;
  }

  $no_resi = $Paket->registerPaket($userdata["KodeCabang"],$kode_jadwal,$jurusan,
    FormatTglToMySQLDate($tgl_berangkat),$data_jadwal["JamBerangkat"],strtoupper($nama_pengirim),
    strtoupper($alamat_pengirim),$telp_pengirim,strtoupper($nama_penerima),
    strtoupper($alamat_penerima),$telp_penerima,$discount,
    $jenis_discount,$harga_sebelum_diskon,$harga_paket,
    (!$paket_dijemput?0:$charge_jemput),(!$paket_diantar?0:$charge_antar),1,
    $berat,strtoupper($keterangan),"",
    $userdata["user_id"],"","",
    "","",$jenis_pembayaran,
    $cara_bayar,"",$layanan,$userdata["user_id"],$jenis_kardus,$paket_dijemput,$paket_diantar,
    $cabang_tujuan_akhir,$is_paket_transit);

  if($is_paket_transit){
    //JIKA PAKET TRANSIT, DIINPUT KE TABEL TRANSIT
    $Paket->registerPaketTransit($no_resi);
  }

  if($no_resi!=""){
    if($kode_voucher!="") {
      //PROSES PAKAI VOUCHER
      $VoucherPaket->pakaiVoucher($kode_voucher,$userdata["user_id"],$no_resi);
    }

    //MENGURANGI STOK KARDUS
    $StokKardus->kurangiStok($jenis_kardus,$userdata["KodeCabang"]);

    $ret_val["status"]  = "OK";
    $ret_val["pesan"]   = $no_resi;

    //INPUT DATA PELANGGAN 1
    $Pelanggan->tambah($telp_pengirim,$nama_pengirim,$alamat_pengirim,$jurusan);

    //INPUT DATA PELANGGAN 2
    $Pelanggan->tambah($telp_penerima,$nama_penerima,$alamat_penerima,$jurusan);

  }
  else{
    $ret_val["status"]  = "GAGAL";
    $ret_val["pesan"]   = "gagal menyimpan data";
  }

  echo(json_encode($ret_val));

}

function getDetailPaket($no_resi){
  global $template,$Paket,$config,$StokKardus,$Permission,$userdata,$id_page;

  $data_paket = $Paket->ambilDetailDataPaket($no_resi);

  $data_kardus= $StokKardus->getDetailDataKardus($data_paket["JenisKardus"]);

  $template->set_filenames(array('body' => 'paket/detailpaket.tpl'));

  //ACTION CETAK ULANG RESI
  if(!$Permission->isPermitted($userdata["user_level"],$id_page.".1")) {
    //CETAK ULANG RESI
    if(!$data_paket["RequestCetakUlang"]) {
      $act_cetak_ulang = "showDialogReqOTPCetak('$data_paket[NoResi]');";
    }
    else{
      $act_cetak_ulang = "showDialogOTPCetak('$data_paket[NoResi]');";
    }
    //SHOW BUTTON
    $template->assign_block_vars("BTN_CETAK",array("action"=>$act_cetak_ulang));
  }
  else{
    //SHOW BUTTON
    $template->assign_block_vars("BTN_CETAK",array("action"=>"cetakResi('$data_paket[NoResi]');"));
  }

  //ACTION PEMBATALAN
  if(!$Permission->isPermitted($userdata["user_level"],$id_page.".2")) {
    //PEMBATALAN TRANSAKSI

    if(!$data_paket["CetakManifest"] && !$data_paket["IsTransit"] && !$data_paket["IsSampai"]) {
      if (!$data_paket["RequestBatal"]) {
        $act_pembatalan = "showDialogReqOTPBatal('$data_paket[NoResi]');";
      } else {
        $act_pembatalan = "showDialogOTPBatal('$data_paket[NoResi]');";
      }
      //SHOW BUTTON
      $template->assign_block_vars("BTN_BATAL",array("action"=>$act_pembatalan));
    }
  }
  else{
    //SHOW BUTTON
    $template->assign_block_vars("BTN_BATAL",array("action"=>"batalkanPaket('$data_paket[NoResi]');"));
  }

  //ACTION MUTASI
  if(!$Permission->isPermitted($userdata["user_level"],$id_page.".3")) {
    //MUTASI PAKET

    if(!$data_paket["CetakManifest"] && !$data_paket["IsTransit"] && !$data_paket["IsSampai"]) {
      if (!$data_paket["RequestMutasi"]) {
        $act_mutasi = "showDialogReqOTPMutasi('$data_paket[NoResi]');";
      } else {
        $act_mutasi = "showDialogOTPMutasi('$data_paket[NoResi]');";
      }
      //SHOW BUTTON
      $template->assign_block_vars("BTN_MUTASI",array("action"=>$act_mutasi));
    }
  }
  else{
    //SHOW BUTTON
    $template->assign_block_vars("BTN_MUTASI",array("action"=>"mutasiPaket('$data_paket[NoResi]');"));
  }

  if($data_paket["StatusDiambil"]){ //PAKET SUDAH DIAMBIL ATAU SUDAH DITERIMA
    if(!$data_paket["IsDiantar"]) {
      $template->assign_block_vars("IS_DIAMBIL", array(
        "nama"      => $data_paket["NamaPengambil"],
        "ktp"       => $data_paket["NoKTPPengambil"],
        "telp"      => $data_paket["TelpPengambil"],
        "waktuambil"=> FormatMySQLDateToTglWithTime($data_paket["WaktuPengambilan"])
      ));
    }
    else{
      $template->assign_block_vars("IS_DITERIMA", array(
        "nama"        => $data_paket["NamaPengambil"],
        "telp"        => $data_paket["TelpPengambil"],
        "waktuterima" => FormatMySQLDateToTglWithTime($data_paket["WaktuPengambilan"])
      ));
    }
  }
  elseif($data_paket["IsSampai"] && $data_paket["IsDiantar"] && $data_paket["WaktuAntar"]!=""){ //PAKET YANG SEDANG DALAM PENGANTARAN KURIR
    $template->assign_block_vars("BTN_TERIMA_PAKET", array("action" => "showDialogTerimaPaket('$no_resi');"));

    $template->assign_block_vars("IS_DIKIRIM_KURIR",array(
      "kurir"     => $data_paket["KurirAntar"],
      "waktukirim"=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_paket["WaktuAntar"]))
    ));
  }
  elseif($data_paket["IsSampai"]){ //PAKET SUDAH SAMPAI
    if(!$data_paket["IsDiantar"]) { //BARANG DIAMBIL DI LOKET
      $is_otp = $data_paket["OTPPengambilan"] != "" ? 1 : 0;
      $template->assign_block_vars("BTN_AMBIL_PAKET", array("action" => "showDialogAmbilPaket('$no_resi',$is_otp);"));
      $keterangan_sampai  = "";
    }
    else{
      $keterangan_sampai  = "<span class='yellowcell'>Menunggu proses pengiriman oleh kurir ke penerima paket</span>";
    }

    $template->assign_block_vars("IS_SAMPAI",array(
      "waktusampai" => dateparseWithTime(FormatMySQLDateToTglWithTime($data_paket["WaktuSampai"])),
      "keterangan"  => $keterangan_sampai
    ));

  }
  elseif($data_paket["IsTransit"]){ //PAKET TRANSIT
    $status = "Paket sedang transit pada ".dateparseWithTime(FormatMySQLDateToTglWithTime($data_paket["WaktuTransit"]));
    $template->assign_block_vars("IS_TRANSIT",array());
  }
  elseif($data_paket["CetakManifest"]){ //PAKET SUDAH DIBERANGKATKAN
    $template->assign_block_vars("IS_BERANGKAT",array(
      "waktuberangkat"  => dateparseWithTime(FormatMySQLDateToTglWithTime($data_paket["WaktuCetakManifest"]))
    ));
  }
  else{ //PAKET MASIH DALAM PROSES, BELUM DIKIRIMKAN
    $template->assign_block_vars("IS_DALAM_PROSES",array());
  }


  $template->assign_vars(array(
    "NO_RESI"         => $data_paket["NoResi"],
    "TGL_BERANGKAT"   => dateparse(FormatMySQLDateToTgl($data_paket["TglBerangkat"])),
    "ASAL"            => $data_paket["Asal"],
    "TUJUAN"          => $data_paket["Tujuan"],
    "JADWAL"          => $data_paket["KodeJadwal"],
    "TELP_PENGIRIM"   => $data_paket["TelpPengirim"],
    "NAMA_PENGIRIM"   => $data_paket["NamaPengirim"],
    "ALAMAT_PENGIRIM" => $data_paket["AlamatPengirim"],
    "IS_DIJEMPUT"     => $data_paket["IsDijemput"]?"<span style='color:green;'>YA</span>":"<span style='color:red;'>TIDAK</span>",
    "TELP_PENERIMA"   => $data_paket["TelpPenerima"],
    "NAMA_PENERIMA"   => $data_paket["NamaPenerima"],
    "ALAMAT_PENERIMA" => $data_paket["AlamatPenerima"],
    "IS_DIANTAR"      => $data_paket["IsDiantar"]?"<span style='color:green;'>YA</span>":"<span style='color:red;'>TIDAK</span>",
    "BERAT"           => $data_paket["Berat"],
    "LAYANAN"         => $config["layanan_paket"][$data_paket["Layanan"]],
    "JENIS_KARDUS"    => $data_kardus["NamaJenisKardus"],
    "KETERANGAN"      => $data_paket["KeteranganPaket"],
    "KET_DISCOUNT"    => ($data_paket["JenisDiscount"]==""?"-tanpa diskon-":$data_paket["JenisDiscount"]),
    "HARGA"           => number_format($data_paket["HargaSebelumDiscount"],0,",","."),
    "DISCOUNT"        => number_format($data_paket["Discount"],0,",","."),
    "CHARGE_JEMPUT"   => number_format($data_paket["ChargeJemput"],0,",","."),
    "CHARGE_ANTAR"    => number_format($data_paket["ChargeAntar"],0,",","."),
    "TOTAL_BAYAR"     => number_format($data_paket["HargaPaket"]+$data_paket["ChargeJemput"]+$data_paket["ChargeAntar"],0,",","."),
    "WAKTU_TRANSAKSI" => dateparseWithTime(FormatMySQLDateToTglWithTime($data_paket["WaktuCetakTiket"])),
    "PETUGAS"         => $data_paket["NamaPetugas"],
  ));

  if($data_paket["CabangTujuanAkhir"]!=$data_paket["KodeCabangTujuan"]){
    $template->assign_block_vars("PAKET_TRANSIT",array());
  }

  $template->pparse('body');
}

function setDialogReqOTPCetakUlang($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "Request OTP Cetak Ulang",
    "DLG_KETERANGAN"  => "Anda tidak memiliki otoritas untuk melakukan cetak ulang resi!<br>Jika anda ingin melakukan cetak ulang resi, silahkan melakukan request OTP dengan menekan tombol \"Request OTP\" dengan memberikan alasan request anda.",
    "DLG_ACTION"      => "requestOTPCetak('$no_resi');",
    "DLG_BUTTON"      => "Request OTP",
    "DLG_HEIGHT"      => "220"
  ));

  $template->assign_block_vars("INPUT_ALASAN",array());

  $template->pparse('body');
}

function sendRequestOTPCetakUlang($no_resi,$alasan){
  global $Paket;

  $ret_val["status"]  = "OK";

  //AKAN DIBUATKAN REQUEST CETAK ULANG RESI
  $Paket->setRequestCetakUlang($no_resi,$alasan);

  echo(json_encode($ret_val));

}

function setDialogOTPCetakUlang($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "OTP Cetak Ulang",
    "DLG_KETERANGAN"  => "Silahkan masukkan kode OTP untuk melakukan cetak ulang resi.",
    "DLG_ACTION"      => "cetakResi('$no_resi',1)",
    "DLG_BUTTON"      => "cetak ulang",
    "DLG_HEIGHT"      => "170"
  ));

  $template->assign_block_vars("INPUT_OTP",array());

  $template->pparse('body');
}

function verifikasiOTPCetakUlang($no_resi,$otp){
  global $Paket;

  $ret_val["status"]  = "OK";

  $ret_val["pesan"] = $Paket->verifyOTPCetakUlang($no_resi,$otp)?"APPROVED":"REJECTED";

  echo(json_encode($ret_val));

}

function setDialogReqOTPBatal($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "Request OTP Pembatalan",
    "DLG_KETERANGAN"  => "Anda tidak memiliki otoritas untuk melakukan PEMBATALAN!<br>Jika anda ingin melakukan PEMBATALAN, silahkan melakukan request OTP dengan menekan tombol \"Request OTP\" dengan memberikan alasan request anda.",
    "DLG_ACTION"      => "requestOTPBatal('$no_resi');",
    "DLG_BUTTON"      => "Request OTP",
    "DLG_HEIGHT"      => "220"
  ));

  $template->assign_block_vars("INPUT_ALASAN",array());

  $template->pparse('body');
}

function sendRequestOTPBatal($no_resi,$alasan){
  global $Paket;

  $ret_val["status"]  = "OK";

  //AKAN DIBUATKAN REQUEST CETAK ULANG RESI
  $Paket->setRequestBatal($no_resi,$alasan);

  echo(json_encode($ret_val));

}

function setDialogOTPBatal($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "OTP Pembatalan",
    "DLG_KETERANGAN"  => "Silahkan masukkan kode OTP untuk melakukan PEMBATALAN.",
    "DLG_ACTION"      => "batalkanPaket('$no_resi')",
    "DLG_BUTTON"      => "Batalkan Transaksi",
    "DLG_HEIGHT"      => "170"
  ));

  $template->assign_block_vars("INPUT_OTP",array());

  $template->pparse('body');
}

function prosesBatalkanTransaksi($no_resi,$otp=""){
  global $Paket,$Permission,$id_page,$userdata;

  $ret_val["status"]  = "OK";

  if(!$Permission->isPermitted($userdata["user_level"],$id_page.".2")) { //PAGE ID 223.2 UNTUK OTORISASI PROSES PEMBATALAN PAKET
    if($Paket->verifyOTPBatal($no_resi,$otp)){
      //OTORISASI DITERIMA
      $Paket->batalkanResi($no_resi);
      $ret_val["pesan"] = "APPROVED";
    }
    else{
      $ret_val["pesan"] = "REJECTED";
    }
  }
  else{
    $Paket->batalkanResi($no_resi);
    $ret_val["pesan"] = "APPROVED";
  }

  echo(json_encode($ret_val));

}

function setDialogReqOTPMutasi($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "Request OTP Mutasi Paket",
    "DLG_KETERANGAN"  => "Anda tidak memiliki otoritas untuk melakukan MUTASI!<br>Jika anda ingin melakukan MUTASI, silahkan melakukan request OTP dengan menekan tombol \"Request OTP\" dengan memberikan alasan request anda.",
    "DLG_ACTION"      => "requestOTPMutasi('$no_resi');",
    "DLG_BUTTON"      => "Request OTP",
    "DLG_HEIGHT"      => "220"
  ));

  $template->assign_block_vars("INPUT_ALASAN",array());

  $template->pparse('body');
}

function sendRequestOTPMutasi($no_resi,$alasan){
  global $Paket;

  $ret_val["status"]  = "OK";

  //AKAN DIBUATKAN REQUEST CETAK ULANG RESI
  $Paket->setRequestMutasi($no_resi,$alasan);

  echo(json_encode($ret_val));

}

function cekOtorisasiMutasi($no_resi,$role,$otp="",$alasan=""){
  global $Paket,$Permission,$id_page;

  $ret_val["status"]  = "OK";

  if(!$Permission->isPermitted($role,$id_page.".3")) { //PAGE ID 223.3 UNTUK OTORISASI PROSES MUTASI PAKET
    if($otp==""){
      //JIKA TIDAK MEMILIKI OTORISASI, AKAN DIBUATKAN REQUEST PEMBATALAN
      $Paket->setRequestMutasi($no_resi,$alasan);
      $ret_val["pesan"] = "NEED_OTORISASI";
    }
    else{
      if($Paket->verifyOTPMutasi($no_resi,$otp)){
        //OTORISASI DITERIMA
        $ret_val["pesan"] = "APPROVED";
      }
      else{
        $ret_val["pesan"] = "REJECTED";
      }
    }
  }
  else{
    $ret_val["pesan"] = "APPROVED";
  }

  echo(json_encode($ret_val));

}

function setDialogOTPMutasi($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "OTP Mutasi",
    "DLG_KETERANGAN"  => "Silahkan masukkan kode OTP untuk melakukan MUTASI.",
    "DLG_ACTION"      => "mutasiPaket('$no_resi')",
    "DLG_BUTTON"      => "Mutasikan Paket",
    "DLG_HEIGHT"      => "170"
  ));

  $template->assign_block_vars("INPUT_OTP",array());

  $template->pparse('body');
}

function verifikasiOTPMutasi($no_resi,$otp){
  global $Paket;

  $ret_val["status"]  = "OK";

  $ret_val["pesan"] = $Paket->verifyOTPMutasi($no_resi,$otp)?"APPROVED":"REJECTED";

  echo(json_encode($ret_val));

}

function prosesMutasi($no_resi,$jurusan,$tgl_berangkat,$kode_jadwal,$otp){
  global $Paket,$Permission,$Jadwal,$id_page,$userdata;

  if($Permission->isPermitted($userdata["user_level"],$id_page.".3") || $Paket->verifyOTPMutasi($no_resi,$otp)) { //PAGE ID 223.3 UNTUK OTORISASI PROSES MUTASI PAKET
    //OTORISASI DITERIMA
    $data_jadwal  = $Jadwal->ambilDataDetail($kode_jadwal);

    if($Paket->mutasikanResi($no_resi,$jurusan,FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal,$data_jadwal["JamBerangkat"])) {
      $ret_val["status"] = "OK";
      $ret_val["pesan"] = "DONE";
    }
    else{
      $ret_val["status"] = "ERROR";
      $ret_val["pesan"]  = "tidak boleh mutasi ke jadwal yang sudah cetak manifest atau yang sudah lalu";
    }
  }
  else{
    $ret_val["status"] = "OK";
    $ret_val["pesan"]  = "REJECTED";
  }

  echo(json_encode($ret_val));

}

function getDataPelanggan($no_telp){

  $Pelanggan  = new Pelanggan();

  $data_pelanggan = $Pelanggan->ambilDataDetail($no_telp);

  $ret_val["notelp"]  = $data_pelanggan["NoTelp"];
  $ret_val["nama"]    = $data_pelanggan["Nama"];
  $ret_val["alamat"]  = $data_pelanggan["Alamat"];

  echo(json_encode($ret_val));


}

function setListCabangTujuanAkhir($cari,$kota){
  global $db;

  $Cabang = new Cabang();


  $result = $Cabang->setComboListCabang($cari,$kota);

  while($row=$db->sql_fetchrow($result)){
    $list_cabang[]=array("group"=>"","id"=>$row["KodeCabang"],"text"=>$row["Nama"]);
  }

  $ret_val  = json_encode($list_cabang);
  echo($ret_val);
}

function cariPaketPelanggan($cari){
  global $Paket,$template;

  $template->set_filenames(array('body' => 'paket/hasilcari.tpl'));

  $data_ditemukan = $Paket->cariPaketPelanggan($cari);

  $i=0;

  foreach($data_ditemukan as $row) {

    $i++;

    if ($row["IsTransit"]) {
      $status = "TRANSIT";
      $remark = "Transit pada:" . dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuTransit"]));
    } elseif ($row["StatusDiambil"] == 1) {
      $status = "SUDAH DIAMBIL";
      $remark = "diambil oleh:" . $row["NamaPengambil"] . ",pada:" . dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuPengambilan"])) . ",diberikan oleh:" . $row["NamaPetugasPemberi"];
    } elseif ($row["IsSampai"]) {
      $status = "SAMPAI";
      $remark = "sudah sampai di tujuan pada " . dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuSampai"]));
    } elseif ($row["IsDispatch"] == 1) {
      $status = "DISPATCH DARI TRANSIT";
      $remark = "diberangkatkan dari transit pada:" . dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuDispatch"]));
    } elseif ($row["CetakManifest"]) {
      $status = "DIBERANGKATKAN";
      $remark = "berangkat pada:" . dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuCetakManifest"]));
    } else {
      $status = "ON PROCESS";
      $remark = "sedang dalam proses";
    }

    $template->assign_block_vars("ROW", array(
      "no" => $i,
      "odd" => ($i % 2 == 0 ? "even" : "odd"),
      "noresi" => $row["NoResi"],
      "tglberangkat" => dateparse(FormatMySQLDateToTgl($row["TglBerangkat"])),
      "jamberangkat" => $row["JamBerangkat"],
      "jadwal" => $row["KodeJadwal"],
      "tujuan" => $row["Tujuan"],
      "status" => $status,
      "remark" => $remark
    ));

  }

  if($i>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->pparse('body');

}

function setDialogReqOTPCetakUlangManifest($no_manifest){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "Request OTP Cetak Ulang Manifest",
    "DLG_KETERANGAN"  => "Anda tidak memiliki otoritas untuk melakukan cetak ulang manifest!<br>Jika anda ingin melakukan cetak ulang manifest, silahkan melakukan request OTP dengan menekan tombol \"Request OTP\" dengan memberikan alasan request anda.",
    "DLG_ACTION"      => "requestOTPCetakManifest('$no_manifest');",
    "DLG_BUTTON"      => "Request OTP",
    "DLG_HEIGHT"      => "220"
  ));

  $template->assign_block_vars("INPUT_ALASAN",array());

  $template->pparse('body');
}

function sendRequestOTPCetakUlangManifest($no_manifest,$alasan){
  global $Paket;

  $ret_val["status"]  = "OK";

  //AKAN DIBUATKAN REQUEST CETAK ULANG RESI
  $Paket->setRequestCetakUlangManifest($no_manifest,$alasan);

  echo(json_encode($ret_val));

}

function setDialogOTPCetakUlangManifest($no_manifest){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogotp.tpl'));

  $template->assign_vars(array(
    "DLG_TITLE"       => "OTP Cetak Ulang Manifest",
    "DLG_KETERANGAN"  => "Silahkan masukkan kode OTP untuk melakukan cetak ulang manifest.",
    "DLG_ACTION"      => "cetakManifest('$no_manifest',1)",
    "DLG_BUTTON"      => "cetak ulang",
    "DLG_HEIGHT"      => "170"
  ));

  $template->assign_block_vars("INPUT_OTP",array());

  $template->pparse('body');
}

function verifikasiOTPCetakUlangManifest($no_manifest,$otp){
  global $Paket;

  $ret_val["status"]  = "OK";

  $ret_val["pesan"] = $Paket->verifyOTPCetakUlangManifest($no_manifest,$otp)?"APPROVED":"REJECTED";

  echo(json_encode($ret_val));

}

function setDialogAmbilPaket($no_resi,$is_otp=0){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogambilpaket.tpl'));

  $template->assign_vars(array(
    "NO_RESI"         => $no_resi
  ));

  if($is_otp){
    $template->assign_block_vars("OTP_PENGAMBILAN",array());
  }

  $template->pparse('body');
}

function ambilPaket($no_resi,$nama_pengambil,$telp_pengambil,$ktp_pengambil,$otp=""){
  global $Paket;

  echo(json_encode($Paket->prosesPengambilaBarang($no_resi,$nama_pengambil,$telp_pengambil,$ktp_pengambil,$otp)));

}

function setDialogTerimaPaket($no_resi){
  global $template;

  $template->set_filenames(array('body' => 'paket/dialogterimapaket.tpl'));

  $template->assign_vars(array(
    "NO_RESI"         => $no_resi
  ));

  $template->pparse('body');
}

function terimaPaket($no_resi,$nama_penerima,$telp_penerima){
  global $Paket;

  echo(json_encode($Paket->prosesPenerimaanBarang($no_resi,$nama_penerima,$telp_penerima)));

}

function showStokKardus(){
  global $template,$userdata,$StokKardus;

  $Cabang = new Cabang();

  $template->set_filenames(array('body' => 'paket/stokkardus.tpl'));

  $data_cabang  = $Cabang->ambilDataDetail($userdata["KodeCabang"]);

  $template->assign_vars(array(
    "CABANG"         => $data_cabang["Nama"]
  ));

  $data_arr_stok  = $StokKardus->ambilStokKardus($userdata["KodeCabang"]);

  $i=0;

  foreach($data_arr_stok as $key=>$value){
    $i++;

    $template->assign_block_vars("ROW",array(
      "class"       => ($i%2==0?"even":"odd"),
      "no"          => $i,
      "jeniskardus" => $value["NamaJenisKardus"],
      "jumlahstok"  => $value["Stok"]
    ));
  }

  $template->pparse('body');
}
?>