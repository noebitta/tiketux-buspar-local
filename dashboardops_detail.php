<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 214
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX["KASIR"]))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJadwal.php');


$Cabang	= new Cabang();
$Jadwal = new Jadwal();

//METHODS
function getDataSPJ($tgl_berangkat,$kode_jadwal){
	global $db;
	global $Jadwal;

	$sql=
		"SELECT 
			NoSPJ,KodeDriver,Driver,NoPolisi
		FROM tbl_spj
		WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl_berangkat'";

	if (!$result= $db->sql_query($sql)){
			echo("Error:".__LINE__);exit;
	}
	
	return $db->sql_fetchrow($result);
	
}

// PARAMETER
$id_jurusan		= isset($HTTP_GET_VARS['idjurusan'])? $HTTP_GET_VARS['idjurusan'] : $HTTP_POST_VARS['idjurusan'];
$tanggal  		= isset($HTTP_GET_VARS['tgl'])? $HTTP_GET_VARS['tgl'] : $HTTP_POST_VARS['tgl'];
$is_arrival		= isset($HTTP_GET_VARS['isarrival'])? $HTTP_GET_VARS['isarrival'] : $HTTP_POST_VARS['isarrival'];


$template->set_filenames(array("body" => "dashboardops/detail.tpl")); 

$tanggal			= ($tanggal!='')?$tanggal:dateD_M_Y();
$tanggal_mysql= FormatTglToMySQLDate($tanggal);



//MENGAMBIL JUMLAH PENUMPANG BERANGKAT
$sql_sub 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr2.KodeJadwal)=tr1.KodeJadwal
		AND tr1.KodeJadwal!=tr2.KodeJadwal
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";

$sql_sub2 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)=tr2.KodeJadwal
		AND tr1.KodeJadwal!=tr2.KodeJadwal
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";

$sql_sub3 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr2.KodeJadwal)=tr1.KodeJadwal
		AND tr1.KodeJadwal!=tr2.KodeJadwal AND CetakTiket=1
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";

$sql_sub4 	=
	"(SELECT
		IS_NULL(COUNT(NoTiket),0)
	FROM tbl_reservasi tr2
	WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)=tr2.KodeJadwal
		AND tr1.KodeJadwal!=tr2.KodeJadwal AND CetakTiket=1
		AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";
		
$sql	=
	"SELECT 
		KodeJadwal,
		IS_NULL(COUNT(NoTiket),0) AS PenumpangBook,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS PenumpangConfirm,
		$sql_sub AS BookTransit1,
		$sql_sub2 AS BookTransit2,
		$sql_sub3 AS ConfirmTransit1,
		$sql_sub4 AS ConfirmTransit2
	FROM tbl_reservasi tr1
	WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1
	AND  IdJurusan=$id_jurusan
	GROUP BY KodeJadwal;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_penumpang[$row["KodeJadwal"]]	= $row;
}

//MENGAMBIL JUMLAH PAKET BERANGKAT
$sql	=
	"SELECT 
		KodeJadwal,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS JumlahPaket
	FROM tbl_paket
	WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1
	AND  IdJurusan=$id_jurusan AND CetakTiket=1
	GROUP BY KodeJadwal;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket[$row["KodeJadwal"]]=$row["JumlahPaket"];
}

//KEBERANGKATAN DARI CABANG BERANGKAT
$sql=
	"SELECT
		tmj.KodeJadwal,tmj.KodeJadwalUtama,tmj.JamBerangkat,IdPenjadwalan,tpk.StatusAktif,NoPolisi,KodeKendaraan,
		KodeDriver,NamaDriver,
		IS_NULL(tpk.StatusAktif,1) AS StatusPenjadwalan,FlagSubJadwal
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'
	WHERE tmj.IdJurusan=$id_jurusan AND (FlagAktif=1 OR tpk.StatusAktif=1) 
	ORDER BY tmj.JamBerangkat";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=0;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row["FlagSubJadwal"]!=1){
		$kode_jadwal	= $row["KodeJadwal"];
		$nama_driver	= $row["NamaDriver"];
		$kendaraan 		= $row["KodeKendaraan"];
	}
	else{
		//JIKA JADWAL ADALAH SUB JADWAL, MENGAMBIL NAMA DAN KODE KENDARAAN DARI JADWAL UTAMA 
		$kode_jadwal	= $row["KodeJadwalUtama"];

		$sql=
			"SELECT KodeKendaraan,NamaDriver
			FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'
			WHERE tmj.KodeJadwal='$kode_jadwal' AND (FlagAktif=1 OR tpk.StatusAktif=1) ";
			
		if(!$result_sub_jadwal = $db->sql_query($sql)){
			echo("Err:".__LINE__);exit;
		}

		$row_sub_jadwal	= $db->sql_fetchrow($result_sub_jadwal);
		
		$nama_driver	= $row_sub_jadwal["NamaDriver"];
		$kendaraan 		= $row_sub_jadwal["KodeKendaraan"];

	}

	$data_spj	= getDataSPJ($tanggal_mysql,$kode_jadwal);
	
	$keterangan	= "<font style='color:green;'>BUKA</font>";

	if($data_spj["NoSPJ"]!=""){
		$nama_driver	= $data_spj["Driver"];
		$kendaraan 		= $data_spj["NoPolisi"];
		$keterangan		= "Manifest";
	}

	if($row['StatusPenjadwalan']==0){
		$odd				= "red";
		$keterangan	= "<b>TUTUP</b>";
	}
	
	$template->
		assign_block_vars(
			'ROW1',
			array(
				'odd'=>$odd,
				'time'=>$row['JamBerangkat'],
				'cgs'=>$nama_driver,
				'body'=>$kendaraan,
				'p'=>number_format($data_paket[$row["KodeJadwal"]],0,",","."),
				'b'=>number_format($data_penumpang[$row["KodeJadwal"]]['PenumpangBook'],0,",","."),
				't'=>number_format($data_penumpang[$row["KodeJadwal"]]['BookTransit1']+$data_penumpang[$row["KodeJadwal"]]['BookTransit2'],0,",","."),
				'c'=>number_format($data_penumpang[$row["KodeJadwal"]]['PenumpangConfirm']+$data_penumpang[$row["KodeJadwal"]]['ConfirmTransit1']+$data_penumpang[$row["KodeJadwal"]]['ConfirmTransit2'],0,",","."),
				'keterangan'=>$keterangan
			)
		);
		
		$i++;
}


//MENGAMBIL NAMA CABANG TUJUAN
$sql = 
	"SELECT 
		f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabang,
		KodeCabangAsal,KodeCabangTujuan
	FROM tbl_md_jurusan WHERE IdJurusan=$id_jurusan";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);
$tujuan 	= $row[0];
$cabang_asal	= $row["KodeCabangAsal"];
$cabang_tujuan= $row["KodeCabangTujuan"];

//====KEDATANGAN================================================================================================

//MENGAMBIL JADWAL SEBALIKNYA
$sql = 
	"SELECT IdJurusan
	FROM tbl_md_jurusan WHERE KodeCabangAsal='$cabang_tujuan' AND KodeCabangTujuan='$cabang_asal'
	ORDER BY IdJurusan
	LIMIT 0,1";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);
$id_jurusan	= $row[0];

if($id_jurusan!=""){

	//MENGAMBIL JUMLAH PENUMPANG DATANG
	$sql_sub 	=
		"(SELECT
			IS_NULL(COUNT(NoTiket),0)
		FROM tbl_reservasi tr2
		WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr2.KodeJadwal)=tr1.KodeJadwal
			AND tr1.KodeJadwal!=tr2.KodeJadwal
			AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";
	
	$sql_sub2 	=
		"(SELECT
			IS_NULL(COUNT(NoTiket),0)
		FROM tbl_reservasi tr2
		WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)=tr2.KodeJadwal
			AND tr1.KodeJadwal!=tr2.KodeJadwal
			AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";
	
	$sql_sub3 	=
		"(SELECT
			IS_NULL(COUNT(NoTiket),0)
		FROM tbl_reservasi tr2
		WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr2.KodeJadwal)=tr1.KodeJadwal
			AND tr1.KodeJadwal!=tr2.KodeJadwal AND CetakTiket=1
			AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";
	
	$sql_sub4 	=
		"(SELECT
			IS_NULL(COUNT(NoTiket),0)
		FROM tbl_reservasi tr2
		WHERE f_jadwal_ambil_kodeutama_by_kodejadwal(tr1.KodeJadwal)=tr2.KodeJadwal
			AND tr1.KodeJadwal!=tr2.KodeJadwal AND CetakTiket=1
			AND TglBerangkat='$tanggal_mysql' AND FlagBatal!=1)";
			
	$sql	=
		"SELECT 
			KodeJadwal,
			IS_NULL(COUNT(NoTiket),0) AS PenumpangBook,
			IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS PenumpangConfirm,
			$sql_sub AS BookTransit1,
			$sql_sub2 AS BookTransit2,
			$sql_sub3 AS ConfirmTransit1,
			$sql_sub4 AS ConfirmTransit2
		FROM tbl_reservasi tr1
		WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1
		AND  IdJurusan=$id_jurusan
		GROUP BY KodeJadwal;";


	if(!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}

	while ($row = $db->sql_fetchrow($result)){
		$data_penumpang[$row["KodeJadwal"]]	= $row;
	}

	//MENGAMBIL JUMLAH PAKET DATANG
	$sql	=
		"SELECT 
			KodeJadwal,
			IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS JumlahPaket
		FROM tbl_paket
		WHERE TglBerangkat='$tanggal_mysql' AND FlagBatal!=1
		AND  IdJurusan=$id_jurusan AND CetakTiket=1
		GROUP BY KodeJadwal;";

	if(!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}

	while ($row = $db->sql_fetchrow($result)){
		$data_paket[$row["KodeJadwal"]]=$row["JumlahPaket"];
	}

	//KEBERANGKATAN DARI CABANG DATANG
	$sql=
		"SELECT
			tmj.KodeJadwal,tmj.KodeJadwalUtama,tmj.JamBerangkat,IdPenjadwalan,tpk.StatusAktif,NoPolisi,KodeKendaraan,
			KodeDriver,NamaDriver,
			IS_NULL(tpk.StatusAktif,1) AS StatusPenjadwalan,FlagSubJadwal
		FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'
		WHERE tmj.IdJurusan=$id_jurusan AND (FlagAktif=1 OR tpk.StatusAktif=1)  
		ORDER BY tmj.JamBerangkat";
		
	if(!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}

	$i=1;

	while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
			
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		if($row["FlagSubJadwal"]!=1){
			$kode_jadwal	= $row["KodeJadwal"];
			$nama_driver	= $row["NamaDriver"];
			$kendaraan 		= $row["KodeKendaraan"];
		}
		else{
			//JIKA JADWAL ADALAH SUB JADWAL, MENGAMBIL NAMA DAN KODE KENDARAAN DARI JADWAL UTAMA 
			$kode_jadwal	= $row["KodeJadwalUtama"];

			$sql=
				"SELECT KodeKendaraan,NamaDriver
				FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat='$tanggal_mysql'
				WHERE tmj.KodeJadwal='$row[KodeJadwal]' AND (FlagAktif=1 OR tpk.StatusAktif=1) ";
				
			if(!$result_sub_jadwal = $db->sql_query($sql)){
				echo("Err:".__LINE__);exit;
			}

			$row_sub_jadwal	= $db->sql_fetchrow($result_sub_jadwal);
			
			$nama_driver	= $row_sub_jadwal["NamaDriver"];
			$kendaraan 		= $row_sub_jadwal["KodeKendaraan"];

		}

		$data_spj	= getDataSPJ($tanggal_mysql,$kode_jadwal);
		
		$keterangan	= "<font style='color:green;'>BUKA</font>";

		if($data_spj["NoSPJ"]==""){
			$nama_driver	= $row["NamaDriver"];
			$kendaraan 		= $row["KodeKendaraan"];
		}
		else{
			$nama_driver	= $data_spj["Driver"];
			$kendaraan 		= $data_spj["NoPolisi"];
			$keterangan		= "Manifest";
		}

		if($row['StatusPenjadwalan']==0){
			$odd				= "red";
			$keterangan	= "<b>TUTUP</b>";
		}
		
		$template->
			assign_block_vars(
				'ROW2',
				array(
					'odd'=>$odd,
					'time'=>$row['JamBerangkat'],
					'cgs'=>$nama_driver,
					'body'=>$kendaraan,
					'p'=>number_format($data_paket[$row["KodeJadwal"]],0,",","."),
					'b'=>number_format($data_penumpang[$row["KodeJadwal"]]['PenumpangBook'],0,",","."),
					't'=>number_format($data_penumpang[$row["KodeJadwal"]]['BookTransit1']+$data_penumpang[$row["KodeJadwal"]]['BookTransit2'],0,",","."),
					'c'=>number_format($data_penumpang[$row["KodeJadwal"]]['PenumpangConfirm']+$data_penumpang[$row["KodeJadwal"]]['ConfirmTransit1']+$data_penumpang[$row["KodeJadwal"]]['ConfirmTransit2'],0,",","."),
					'keterangan'=>$keterangan
				)
			);
			
		$i++;
	}


	//MENGAMBIL NAMA CABANG ASAL
	$sql = 
		"SELECT f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabang 
		FROM tbl_md_jurusan 
		WHERE IdJurusan=$id_jurusan";

	if(!$result = $db->sql_query($sql)){
		echo("Err:".__LINE__);exit;
	}

	$row = $db->sql_fetchrow($result);

}

$template->assign_vars(array(
	'TUJUAN'	=> $tujuan,
	'ASAL'		=> $row[0]
	)
);

$template->pparse('body');
?>