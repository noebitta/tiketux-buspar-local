<?php
// HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 228;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassCabang.php');

// PARAMETER
$mode 			      = getVariabel('mode');
$periode_awal     = getVariabel('filtglawal');
$periode_akhir    = getVariabel('filtglakhir');
$fil_tujuan       = getVariabel('tujuan');
$cabang_tujuan_id = getVariabel('cabangtujuanid');
$cari             = getVariabel('cari');
$idx_page         = getVariabel('idxpage');
$order_by         = getVariabel('orderby');
$sort             = getVariabel('sort');

//INSTANCE
$Paket      = new Paket();
$Cabang     = new Cabang();

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;
$order_by   = $order_by==""?1:$order_by;

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));
    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    break;

  case 1:
    //SET COMBO TUJUAN

    $cari         = getVariabel('filter');

    setListCabangTujuan($cari);

    break;

  case 2:
    //BROWSE PAKET BY MANIFEST

    $no_manifest = getVariabel('nomanifest');

    browsePaket($no_manifest);

    break;

  case 3:
    //PROSES CHECK IN
    $list_resi   = getVariabel('listresi');
    $no_manifest = getVariabel('nomanifest');

    prosesCheckIn($list_resi,$no_manifest);

    break;

  case 4:
    //SHOW DIALOG SCAN BARCODE

    showDialogScanBarcode();

    break;

  case 5:
    //PROSES CHECK IN BY BARCODE

    $no_resi = getVariabel('noresi');

    prosesCheckInByBarcode($no_resi);

    break;

}

//METHODES & PROCESS ========================================================================================================================================================================

function showData(){

  global $db,$periode_awal,$periode_akhir,$fil_tujuan,$cabang_tujuan_id,$cari,$order_by,$sort,$idx_page,$template,$Paket,$VIEW_PER_PAGE;

  $template->set_filenames(array('body' => 'paket/checkin.tpl'));

  $periode_awal   = $periode_awal==""?date("d-m-Y"):$periode_awal;
  $periode_akhir  = $periode_akhir==""?date("d-m-Y"):$periode_akhir;

  $filter         = "(DATE(tm.TglBerangkat) BETWEEN '".FormatTglToMySQLDate($periode_awal)."' AND '".FormatTglToMySQLDate($periode_akhir)."')".($fil_tujuan==""?"":" AND KodeCabangTujuan='$fil_tujuan'");

  $result = $Paket->ambilDaftarManifest($order_by,$sort,$idx_page,$cari,$filter);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd =($no%2)==0?"even":"odd";

    $class  = "";

    if($row["PaketSampai"]>0 && ($row["PaketTermanifest"]+$row["PaketBelumManifest"])>0){
      $class="yellowcell";
    }
    elseif($row["PaketTermanifest"]+$row["PaketBelumManifest"]==0){
      $class="greencell";
    }

    $template->assign_block_vars(
      'ROW',array(
        'odd'                   => $odd,
        'idx'                   => $no,
        'class'                 => $class,
        'no'                    => $idx_page*$VIEW_PER_PAGE+$no,
        'nomanifestinduk'       => $row["NoManifestInduk"],
        'nomanifestpaket'       => $row["ListManifestPaket"],
        'jumlahmanifestpaket'   => $row["JumlahManifestPaket"],
        'tglberangkat'          => dateparse(FormatMySQLDateToTgl($row["TglBerangkat"])),
        'asal'                  => $row["Asal"],
        'tujuan'                => $row["Tujuan"],
        'kodejadwal'            => $row["KodeJadwal"],
        'jamberangkat'          => $row["JamBerangkat"],
        'pakettermanifest'      => $row["PaketTermanifest"],
        'paketbelumtermanifest' => $row["PaketBelumManifest"],
        'paketditerima'         => $row["PaketSampai"],
        'waktucetakmanifest'    => dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuCetakManifest"])),
        'kodeunit'              => $row["KodeUnit"],
        'noplat'                => $row["NoPlat"],
        'kodedriver'            => $row["KodeDriver"],
        'driver'                => $row["Driver"],
      )
    );
  }


  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	        => basename(__FILE__),
      "TGL_AWAL"          => $periode_awal,
      "TGL_AKHIR"         => $periode_akhir,
      'CARI'              => $cari,
      'FIL_TUJUAN'        => $fil_tujuan,
      'CABANG_TUJUAN_ID'  => $cabang_tujuan_id,
      'ORDER'             => $order_by,
      'SORT'              => $sort,
      'IDX_PAGE'          => $idx_page,
      'PAGING'            => $paging
    )
  );

  $template->pparse('body');

}

function setListCabangTujuan($cari=""){
  global $db,$Cabang;

  $Cabang = new Cabang();

  $result = $Cabang->setComboListCabang($cari);

  while($row=$db->sql_fetchrow($result)){
    $list_cabang[]=array("group"=>$row["Kota"],"id"=>$row["KodeCabang"],"text"=>$row["Nama"]);
  }

  $ret_val  =json_encode($list_cabang);

  echo($ret_val);
}

function browsePaket($no_manifest){
  global $db,$Paket,$template;

  $template->set_filenames(array('body' => 'paket/checkinbrowse.tpl'));

  $template->assign_vars(array(
    "NO_MANIFEST_INDUK" => $no_manifest
  ));

  //MENGAMBIL DAFTAR MANIFEST PAKET BERDASARKAN MANIFEST INDUK
  $res_manifest = $Paket->ambilDaftarManifestPaketByManifestInduk($no_manifest);

  $idx_manifest = 0;

  while($data_manifest=$db->sql_fetchrow($res_manifest)){ //LOOPING MANIFEST PAKET
    $template->assign_block_vars("MANIFEST",array(
      "idx"             => $idx_manifest,
      "nomanifestpaket" => $data_manifest["NoManifest"]
    ));

    $parameters["TglBerangkat"] = $data_manifest["TglBerangkat"];
    $parameters["KodeJadwal"]   = $data_manifest["KodeJadwal"];

    $res_paket  = $Paket->ambilDaftarPaket($parameters);

    $no   = 0;

    while($data_paket=$db->sql_fetchrow($res_paket)){

      $no++;
      $class_row  = ($no % 2==0?"even":"odd");
      $class_chk  = "b_chk";

      if(!$data_paket["CetakManifest"]){
        $act      = "selectRow(this,$idx_manifest,$no)";
        $status   = "BELUM TERMANIFEST";
        $remark   = "barang ini belum masuk dalam manifest paket";
      }
      elseif(!$data_paket["IsSampai"]){
        $act    = "selectRow(this,$idx_manifest,$no)";
        $status = "BELUM CHECK-IN";
        $remark = "barang belum di check-in ke cabang ini";
      }
      else{
        $class_chk= "b_ok";
        $class_row= "greencell";
        $act      = "";
        $status   = "SAMPAI";
        $remark   = "Barang sudah sampai pada ".FormatMySQLDateToTglWithTime($data_paket["WaktuSampai"]);
      }

      //dibuat ada 3 manifest.row disini karena untuk mengejar performance, karena jika tidak, maka akan ada 3 if yang akan memperlambat proses
      $template->assign_block_vars("MANIFEST.ROW",array(
        "class"           => $class_row,
        "classchk"        => $class_chk,
        "no"              => $no,
        "noresi"          => $data_paket["NoResi"],
        "tujuanakhir"     => $data_paket["TujuanAkhir"],
        "namapengirim"    => $data_paket["NamaPengirim"],
        "telppengirim"    => $data_paket["TelpPengirim"],
        "alamatpengirim"  => $data_paket["AlamatPengirim"],
        "namapenerima"    => $data_paket["NamaPenerima"],
        "telppenerima"    => $data_paket["TelpPenerima"],
        "alamatpenerima"  => $data_paket["AlamatPenerima"],
        "berat"           => $data_paket["Berat"],
        "layanan"         => $data_paket["Layanan"],
        "keterangan"      => $data_paket["KeteranganPaket"],
        "remark"          => $remark,
        "status"          => $status,
        "act"             => $act
      ));

        //BELUM DIMANIFESTASIKAN, MUNCULKAN TOMBOL MANIFEST
        $template->assign_block_vars("MANIFEST.ROW.BTN_MANIFEST",array());
    }

    $idx_manifest++;
  }

  $template->pparse('body');


}

function prosesCheckIn($list_resi,$no_manifest){

  global $db,$Paket,$config;

  $list_resi  = str_replace("\\'","'",$list_resi);

  if($Paket->checkInPaket($list_resi,$no_manifest)){
    $ret_val["status"]  = "OK";
    $ret_val["pesan"]   = "";

    if($config["otp_paket"]){
      $res_paket  = $Paket->ambilListPaketKirimSMSOTP($list_resi);

      while($row=$db->sql_fetchrow($res_paket)){
        //ENGINE KIRIM SMS DISINI ***********************
      }
    }
  }
  else{
    $ret_val["status"]  = "GAGAL";
    $ret_val["pesan"]   = "terjadi kegagalan proses checkin!";
  }

  echo(json_encode($ret_val));

} //prosesCheckIn

function showDialogScanBarcode(){
  global $template;

  $template->set_filenames(array('body' => 'paket/checkin.scanbarcode.tpl'));

  $template->pparse('body');


}

function prosesCheckInByBarcode($no_resi){
  global $Paket,$config,$db;

  $ret_val=$Paket->checkInPaketByBarcode($no_resi);

  if($config["otp_paket"]){
    $res_paket  = $Paket->ambilListPaketKirimSMSOTP("'".$no_resi."'");

    $row=$db->sql_fetchrow($res_paket);
    //ENGINE KIRIM SMS DISINI ***********************
  }

  echo(json_encode($ret_val));

}

?>