<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
global $token;

if(!$userdata['session_logged_in'] && $token!=md5("hanyauntukmuindonesiaku")){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class Rekon{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
  var $inisial_fee_tiket;
  var $fee_sms;

	//CONSTRUCTOR
	function Rekon(){
		$this->ID_FILE="C-RKN";
    $this->inisial_fee_tiket= 500; //rupiah | inisialisasi fee per tiket
    $this->fee_sms= 100;//rupiah
	}
	
	//BODY
  function ambilDataRekonToDate($tgl_rekon){
    global $db;

    $sql=
      "SELECT SUM(JumlahSPJ) AS JumlahSPJ,SUM(JumlahPenumpang) AS JumlahPenumpang,SUM(JumlahTiketOnline) AS JumlahTiketOnline,SUM(JumlahTiketBatal) AS JumlahTiketBatal,
				SUM(JumlahFeeTiket) AS JumlahFeeTiket,SUM(JumlahPaket) AS JumlahPaket,SUM(JumlahFeePaket) AS JumlahFeePaket,
				SUM(JumlahSMS) AS JumlahSMS,SUM(JumlahFeeSMS) AS JumlahFeeSMS,SUM(TotalFee) AS TotalFee
			FROM tbl_rekon_data
			WHERE (TglRekonData BETWEEN '".substr($tgl_rekon,0,-2)."01' AND '$tgl_rekon')";

    if (!$result = $db->sql_query($sql)){
      echo("Err:".$this->ID_FILE.__LINE__);exit;
    }

    return $db->sql_fetchrow($result);
  }

	function ambilDataRekonPerBulan($bulan,$tahun){
		global $db;
		
		$sql=
			"SELECT IdRekonData,
				WEEKDAY(TglRekonData)+1 AS Hari,DAY(TglRekonData) AS Tanggal, 
				JumlahSPJ,JumlahPenumpang,JumlahTiketBatal,JumlahFeeTiket,JumlahPaket,JumlahFeePaket,
				JumlahSMS,JumlahFeeSMS,TotalFee,FlagDibayar,WaktuCatatBayar,
				TotalDiskonFee,TotalBayarFee,JumlahTiketOnline
			FROM tbl_rekon_data
			WHERE MONTH(TglRekonData)=$bulan AND YEAR(TglRekonData)=$tahun
			GROUP BY TglRekonData
			ORDER BY TglRekonData";
		
		$result_rekon = $db->sql_query($sql);	
		
		return $result_rekon;
	}
	
	function ambilTotalRekonTerhutang($bulan="",$tahun=""){
		global $db;
		
		$kondisi_bulan	= $bulan==""?"":"AND MONTH(TglRekonData)=$bulan";
		$kondisi_tahun	= $tahun==""?"":"AND YEAR(TglRekonData)=$tahun";
		
		$sql=
			"SELECT SUM(TotalBayarFee)
			FROM tbl_rekon_data
			WHERE FlagDibayar!=1 $kondisi_bulan $kondisi_tahun";
		
		$result_rekon = $db->sql_query($sql);	
		
		$row = $db->sql_fetchrow($result_rekon);
		
		if(sizeof($row)>0){
			return $row[0];	
		}
		else{
			return 0;
		}
		
	}
	
	function ambilListRekonTerhutang($tgl_akhir=""){
		global $db;
		
		$kondisi_tanggal	= $tgl_akhir==""?"":"AND TglRekonData<='$tgl_akhir'";
		
		$sql=
			"SELECT TglRekonData,JumlahPenumpang,JumlahFeeTiket
			FROM tbl_rekon_data
			WHERE FlagDibayar!=1 $kondisi_tanggal";
		
		return $db->sql_query($sql);	
	}
	
	function isRekonClosedByTgl($tgl){
		global $db;
		
		$sql=
			"SELECT COUNT(1)
			FROM tbl_rekon_data
			WHERE TglRekonData='$tgl'";
		
		$result_rekon = $db->sql_query($sql);	
		$row = $db->sql_fetchrow($result_rekon);
		
		if($row[0]>=1){
			return 1;
		}
		else{
			return 0;
		}
		
	}
	
	function hitungDiscountHariSabat($tgl_transaksi,$fee_tiket,$fee_paket){
		/*
		ID	: hitungDiscountHariSabat
		mengembalikan jumlah discount yang diberikan pada saat hari sabat
		*/
		
		//kamus
		global $db;
		global $userdata;
		global $config;
		
		$tgl_php	= strtotime($tgl_transaksi);
		
		if(date("N",$tgl_php)==5){
			//TRANSAKSI HARI JUMAT
			//FEE PENUMPANG
			$sql_penumpang=
				"SELECT 
					IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket
				FROM tbl_reservasi
				WHERE (WaktuCetakTiket BETWEEN '$tgl_transaksi 18:00:00' AND '$tgl_transaksi 23:59:59')  AND CetakTiket=1 AND FlagBatal!=1";
			
			//FEE PAKET
			$sql_paket=
				"SELECT 
					IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
					IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
				FROM tbl_paket
				WHERE (DATE(WaktuPesan) BETWEEN '$tgl_transaksi 18:00:00' AND '$tgl_transaksi 23:59:59')  AND CetakTiket=1 AND FlagBatal!=1";
		}
		else if(date("N",$tgl_php)==6){
			//TRANSAKSI HARI SABTU
			//FEE PENUMPANG
			$sql_penumpang=
				"SELECT 
					IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket
				FROM tbl_reservasi
				WHERE (WaktuCetakTiket BETWEEN '$tgl_transaksi 00:00:00' AND '$tgl_transaksi 17:59:59')  AND CetakTiket=1 AND FlagBatal!=1";
			
			//FEE PAKET
			$sql_paket=
				"SELECT 
					IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
					IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
				FROM tbl_paket
				WHERE (DATE(WaktuPesan) BETWEEN '$tgl_transaksi 00:00:00' AND '$tgl_transaksi 17:59:59')  AND CetakTiket=1 AND FlagBatal!=1";
		}
		else{
			return 0;
		}

		if (!$result = $db->sql_query($sql_penumpang)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}

		$data_penumpang = $db->sql_fetchrow($result);
		//END MENGAMBIL FEE PENUMPANG
			
		
		if (!$result = $db->sql_query($sql_paket)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}
		
		$data_paket	= $db->sql_fetchrow($result);
		//END MENGAMBIL FEE PAKET
		
		return $data_penumpang['TotalTiket']*$fee_tiket+$data_paket['TotalOmzetPaket']*$fee_paket;
	}

  function closeRekon($tgl_transaksi,$jumlah_penumpang,$jumlah_tiket_online,
                      $jumlah_tiket_batal,$jumlah_fee){

    /*
    ID	: closeRekon
    IS	: data rekon belum ada dalam database
    FS	: Data rekon baru telah disimpan dalam database
    */

    //kamus
    global $db;
    global $userdata;

    //CHECK
    $sql=
      "SELECT COUNT(1)
			FROM tbl_rekon_data
			WHERE TglRekonData='$tgl_transaksi'";

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
      echo("Error:".__LINE__);exit;
    }

    $row	= $db->sql_fetchrow($result);

    if($row[0]>0){
      return 0;
    }

    // SPJ
    $sql=
      "SELECT
				IS_NULL(COUNT(NoSPJ),0) AS TotalSPJ
			FROM tbl_spj
			WHERE DATE(TglBerangkat)='$tgl_transaksi' AND IsEkspedisi=0";

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
      echo("Error:".__LINE__);exit;
    }

    $data_spj = $db->sql_fetchrow($result);
    $pelaku	= $userdata['user_id']==''?0:$userdata['user_id'];

    //FEE SMS
    $sql=
      "SELECT
        SUM(JumlahSMSDikirim) AS TotalSMS
      FROM tbl_log_sms
      WHERE DATE(WaktuKirim)='$tgl_transaksi'";

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
      echo("Error:".__LINE__);exit;
    }

    $total_sms	        = 0;
    $total_fee_sms			= 0;

    while ($row = $db->sql_fetchrow($result)){
      $total_sms	    += $row["TotalSMS"];
      $total_fee_sms	+= $row["TotalSMS"]*$this->fee_sms;
    }

    $total_fee  = $jumlah_fee+$total_fee_sms;

    //END MENGAMBIL FEE SMS

    //MENAMBAHKAN DATA KEDALAM DATABASE

    //MENAMBAHKAN DATA KEDALAM DATABASE

    $sql =
      "INSERT INTO tbl_rekon_data
				(TglRekonData, PetugasRekonData, JumlahSPJ,
				JumlahPenumpang, JumlahTiketBatal, JumlahFeeTiket,
				JumlahPaket, JumlahFeePaket, JumlahSMS,
				JumlahFeeSMS, TotalFee, FlagDibayar,
				FeePerTiket, FeePerPaket, FeePerSMS,
				JumlahTiketOnline,TotalDiskonFee,TotalBayarFee)
			VALUES(
				'$tgl_transaksi',$pelaku,$data_spj[TotalSPJ],
				$jumlah_penumpang,$jumlah_tiket_batal,$jumlah_fee,
				0,0,$total_sms,
				$total_fee_sms,'$total_fee',0,
				'".$this->inisial_fee_tiket."',0,'".$this->fee_sms."',
				'$jumlah_tiket_online',0,'$total_fee')";

    if (!$db->sql_query($sql)){
      die_error("Err:$this->ID_FILE".__LINE__);
    }

    return $total_fee;
  }
	
	function bayarRekon($id_rekon_data){
	  
		/*
		ID	: bayarRekon
		IS	: data rekon sudah ada dalam database
		FS	: Data rekon telah diubah
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENGUPDATE STATUS BAYAR REKON DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_rekon_data SET
				WaktuCatatBayar=NOW(), PetugasCatatBayar=$userdata[user_id], FlagDibayar=1
			WHERE IdRekonData=$id_rekon_data";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function bayarRekonAll($id_rekon_data){
	  
		/*
		ID	: bayarRekon
		IS	: data rekon sudah ada dalam database
		FS	: Data rekon telah diubah
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENGUPDATE STATUS BAYAR REKON DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_rekon_data SET
				WaktuCatatBayar=NOW(), PetugasCatatBayar=$userdata[user_id], FlagDibayar=1
			WHERE IdRekonData IN($id_rekon_data)";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$sql $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilTotalBayarFee($tgl_transaksi,$fee_tiket,$fee_paket,$fee_sms){
	  
		/*
		ID	: closeRekon
		IS	: data rekon belum ada dalam database
		FS	: Data rekon baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		global $userdata;
		global $config;
		
		//FEE PENUMPANG
		$sql=
			"SELECT 
				IS_NULL(SUM(IF(FlagBatal!=1,1,0)),0) AS TotalTiket,
				IS_NULL(SUM(IF(FlagBatal=1,1,0)),0) AS TotalTiketBatal
			FROM tbl_reservasi
			WHERE DATE(TglBerangkat)='$tgl_transaksi' AND CetakTiket=1";

		if (!$result = $db->sql_query($sql)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}

		$data_penumpang = $db->sql_fetchrow($result);
		//END MENGAMBIL FEE PENUMPANG
			
		//FEE PAKET
		$sql=
			"SELECT 
				IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
				IS_NULL(SUM(HargaPaket),0) AS TotalOmzetPaket
			FROM tbl_paket
			WHERE DATE(WaktuPesan)='$tgl_transaksi' AND CetakTiket=1 AND FlagBatal!=1";

		if (!$result = $db->sql_query($sql)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}
		
		$data_paket	= $db->sql_fetchrow($result);
		//END MENGAMBIL FEE PAKET

		//FEE SMS
		$sql=
			"SELECT 
				IS_NULL(SUM(JumlahPesan),0) AS TotalSMS
			FROM tbl_log_sms
			WHERE DATE(WaktuKirim)='$tgl_transaksi'";

		if (!$result = $db->sql_query($sql)){
			//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		}

		$data_sms	= $db->sql_fetchrow($result);
		//END MENGAMBIL FEE SMS
		
		$total_fee	= $data_penumpang[TotalTiket]*$fee_tiket+$data_paket[TotalOmzetPaket]*$fee_paket;
		$total_fee_sms	=	$data_sms[TotalSMS]*$fee_sms;
		$pelaku	= $userdata['user_id']==''?0:$userdata['user_id'];
		
		$total_diskon_sabat	= $this->hitungDiscountHariSabat($tgl_transaksi,$fee_tiket,$fee_paket);
		
		if($total_fee-$total_diskon_sabat<=$config['limit_max_fee_rekon']){
			$total_bayar_fee	= $total_fee-$total_diskon_sabat;
			$total_bayar_fee	= $total_bayar_fee>0?$total_bayar_fee:0;
			
			$total_diskon_fee	= $total_diskon_sabat;
		}
		else{
			$total_bayar_fee	= $config['limit_max_fee_rekon']-$total_diskon_sabat;
			$total_bayar_fee	= $total_bayar_fee>0?$total_bayar_fee:0;
			
			$total_diskon_fee	= $total_fee-$total_bayar_fee;
		}
		
		$total_bayar_fee		= $total_bayar_fee+$total_fee_sms;
		
		return $total_bayar_fee;
	}

  function hitungFee($jumlah_tiket,$fee_tiket){
    if($fee_tiket<100){
      return $jumlah_tiket*100;
    }
    else if($jumlah_tiket/10000<1){
      return $jumlah_tiket*$fee_tiket;
    }
    else{
      return 10000*$fee_tiket + $this->hitungFee($jumlah_tiket-10000,$fee_tiket-100);
    }
  }
	
}
?>