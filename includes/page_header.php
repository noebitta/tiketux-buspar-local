<?php

if (!defined('FRAMEWORK')) {  die("Hacking attempt"); }

define('HEADER_INC', TRUE);

// benchmarking
function getmicrotime() 
{ 
    list($usec, $sec) = explode(" ", microtime()); 
    return ((float)$usec + (float)$sec); 
} 
$benchmark_time_start = getmicrotime();   

$do_gzip_compress = $config['zip'];

// gzip ga jalan di browser super jadul :P
if ($do_gzip_compress) 
{
	$phpver = phpversion();
	$useragent = (isset($HTTP_SERVER_VARS['HTTP_USER_AGENT'])) ? $HTTP_SERVER_VARS['HTTP_USER_AGENT'] : getenv('HTTP_USER_AGENT');
	if ( $phpver >= '4.0.4pl1' && ( strstr($useragent,'compatible') || strstr($useragent,'Gecko') ) )
	{
		if ( extension_loaded('zlib') )
		{
			ob_start('ob_gzhandler');
		}
	}
	else if ( $phpver > '4.0' )
	{
		if ( strstr($HTTP_SERVER_VARS['HTTP_ACCEPT_ENCODING'], 'gzip') )
		{
			if ( extension_loaded('zlib') )
			{
				$do_gzip_compress = TRUE;
				ob_start();
				ob_implicit_flush(0);
				header('Content-Encoding: gzip');
			}
		}
	}
}

// header page
$template->set_filenames(array('overall_header' => 'overall_header.tpl'));
 
//$adp_template_path = $adp_root_path .'templates/'. $config['template'].'/';

$adp_template_path = './templates/'. $config['template'].'/';

// predefined global vars, please change if required
$template->assign_vars(
array(
	// URL
  'U_HOME'        =>append_sid('main.'.$phpEx.''),
  'U_LOGOUT'      =>append_sid('auth.'.$phpEx.'?mode=1'),
  'U_HELP'   	 		=>append_sid('#'),    
  'CABANG_LOGIN'	=>$userdata['KodeCabang'],
  // variabel standar
  'USERNAME'             => $userdata['nama'], 
	'SITENAME'             => $config["site_name"],
	'SITE_DESCRIPTION'     => '',
	'PAGE_TITLE'           => $page_title,
	'CURRENT_TIME'         => date('d/M/Y h:m:s'),	
	'CURRENT_DATE'         => date('d-m-Y'),	
	'SID'                  => $userdata['session_id'],
	// lokasi
	'TPL'       => $adp_template_path, 
	'ROOT'      => $adp_root_path,
	// alias untuk lokasi
	'/'         => $adp_root_path,
	'@'         => $adp_template_path,
    )
);

if ($userdata['session_logged_in'])
{
  // kalo login, show the main-logo and menu
  $template->assign_block_vars('if_login',array());
}

if($interface_menu_utama){
	$template->assign_block_vars('if_menu_utama',array());
}

header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // now
// always modified
header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header ("Pragma: no-cache"); // HTTP/1.0

$template->pparse('overall_header');

?>