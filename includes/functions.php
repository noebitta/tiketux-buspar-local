<?php

function generateNoTiket($kode_jadwal){
	$nourut = rand(100,999);
	$no_tiket = "T".dateYMD().$kode_jadwal.$nourut;
	
	return $no_tiket;
}

function generateKodeBooking($kode_jadwal){
	return date("ymdhis").$kode_jadwal.rand(100,999);
}

function generateKodeBookingPenumpang(){
	global $db;
	
	$temp	= array("0",
		"1","2","3","4","5","6","7","8","9",
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S","T",
		"U","V","W","X","Y","Z",
		"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
		"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
		"U1","V1","W1","X1","Y1","Z1");
	
	$y		= $temp[date("y")*1];
	$m		= $temp[date("m")*1];
	$d		=	$temp[date("d")*1];
	$j		= $temp[date("j")*1];
	$mn		= $temp[date("i")*1];
	$s		= $temp[date("s")*1];
	
	
	do{
		$rnd1	= $temp[rand(1,61)];
		$rnd2	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		$rnd3	= $temp[rand(1,61)];
		
		$kode_booking	= "B".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s;
	
		$sql = "SELECT COUNT(1) FROM tbl_reservasi WHERE KodeBooking='$kode_booking'";
					
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
	
	}while($row[0]>0);
		
	
	
	return $kode_booking;
}

function generateNoTiketPenumpang(){
	$temp	= array("0",
		"1","2","3","4","5","6","7","8","9",
		"A","B","C","D","E","F","G","H","I","J",
		"K","L","M","N","O","P","Q","R","S","T",
		"U","V","W","X","Y","Z",
		"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
		"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
		"U1","V1","W1","X1","Y1","Z1");
	
	$y		= $temp[date("y")*1];
	$m		= $temp[date("m")*1];
	$d		=	$temp[date("d")*1];
	$j		= $temp[date("j")*1];
	$mn		= $temp[date("i")*1];
	$s		= $temp[date("s")*1];
	$rnd1	= $temp[rand(1,61)];
	$rnd2	= $temp[rand(1,61)];
	$rnd3	= $temp[rand(1,61)];
	$rnd3	= $temp[rand(1,61)];
	
	return "T".$y.$m.$d.$j.$mn.$s.$rnd1.$rnd2;
}

function setComboKota($kota_dipilih){
	//SET COMBO kota
	
	global $LIST_KOTA;
	
	$opt_kota="";
		
	for($idx=0;$idx<count($LIST_KOTA);$idx++){
			$selected	=($kota_dipilih!=$LIST_KOTA[$idx])?"":"selected";
			$opt_kota .="<option value='$LIST_KOTA[$idx]' $selected>$LIST_KOTA[$idx]</option>";
		}
			
	return $opt_kota;
	//END SET COMBO KOTA
}

function setComboKotaByTemplate($my_template,$string_parse,$kota_dipilih){
	//SET COMBO kota
	
	global $LIST_KOTA;
		
	for($idx=0;$idx<count($LIST_KOTA);$idx++){
		$selected	=($kota_dipilih!=$LIST_KOTA[$idx])?"":"selected";
		$my_template->assign_block_vars($string_parse,array('value'=>$LIST_KOTA[$idx],'nama'=>$LIST_KOTA[$idx],'selected'=>$selected));
	}
}

function setComboLayoutKursi($layout_dipilih){
	
	$temp_var		= "kursi_".$layout_dipilih;
	$$temp_var	= "selected";
	
	/*$opt_kursi="
		<option value=7 $kursi_7>7 kursi</option>
		<option value=9 $kursi_9>9 kursi</option>
		<option value=10 $kursi_10>10 kursi</option>
		<option value=11 $kursi_11>11 kursi</option>
		<option value=131 $kursi_131 >13A kursi</option>
		<option value=132 $kursi_132 >13B kursi</option>
		<option value=14 $kursi_14 >14 kursi</option>
		<option value=19 $kursi_19 >19 kursi</option>
		<option value=29 $kursi_29 >29 kursi</option>";*/
	
	$opt_kursi="
		<option value=8 $kursi_8>8 kursi</option>
		<option value=10 $kursi_10>10 kursi</option>
		<option value=101 $kursi_101>10 kursi Bandara</option>
		<option value=14 $kursi_14 >14 kursi</option>
		<option value=17 $kursi_17 >17 kursi</option>
		<option value=27 $kursi_27 >27 kursi</option>
		<option value=50 $kursi_50 >50 kursi</option>";
	
	return $opt_kursi;
}

function setComboUserLevel($level_dipilih){
	
	global $userdata;
	global $USER_LEVEL_INDEX;
	global $USER_LEVEL;
	
	$temp_var		= "level_".str_replace(".","_",$level_dipilih);
	$$temp_var	= "selected";
	
	$opt_user	= $userdata['user_level']!=$USER_LEVEL_INDEX['ADMIN']?"":"<option value='".$USER_LEVEL_INDEX['ADMIN']."' $level_0>Admin</option>";
	
	foreach($USER_LEVEL_INDEX as &$user_level){
		$idx_usr		= substr($user_level.".0",0,3);
		$temp_var		= "level_".str_replace(".","_",$idx_usr);
		$opt_user	.= $user_level>0?"<option value='".$user_level."' ".$$temp_var.">".$USER_LEVEL[$idx_usr]."</option>":"";
	}
	
	return $opt_user;
}

function getUserLevel($level_dipilih){
	
	global $LEVEL_ADMIN;
	global $LEVEL_MANAJEMEN;
	global $LEVEL_MANAJER;
	global $LEVEL_SUPERVISOR;
	global $LEVEL_CSO;
	global $LEVEL_CSO_PAKET;
	global $LEVEL_SCHEDULER;
	global $LEVEL_KASIR;
	global $LEVEL_KEUANGAN;
	global $LEVEL_CCARE;
	
	switch($level_dipilih){
		case $LEVEL_ADMIN:
			$user_level	= "Administrator";
			break;
		case $LEVEL_MANAJEMEN:
			$user_level	= "Manajemen";
			break;
		case$LEVEL_MANAJER:
			$user_level	= "Manajer";
			break;
		case$LEVEL_SUPERVISOR:
			$user_level	= "Supervisor";
			break;
		case $LEVEL_CSO:
			$user_level="CSO";
			break;
		case $LEVEL_CSO_PAKET:
			$user_level="CSO Paket";
			break;
		case $LEVEL_SCHEDULER:
			$user_level="Scheduler";
			break;
		case $LEVEL_KASIR:
			$user_level="Kasir";
			break;
		case $LEVEL_KEUANGAN:
			$user_level="Keuangan";
			break;
		case $LEVEL_CCARE:
			$user_level="Customer Care";
			break;
	}
	
	return $user_level;
}

// konversi 12.10.2007 ke 12/10/2007
function dateparse($tgl)
{  
  $temp_tgl	= explode("-",$tgl);
	
	$d = $temp_tgl[0];
  $m = $temp_tgl[1];
  $y = $temp_tgl[2];
	
	/*$d = substr($tgl,0,2);
		$m = substr($tgl,3,2);
		$y = substr($tgl,6,4);*/
	
  $bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
						'1' => 'jan',
  					'2' => 'feb',
  					'3' => 'mar',
  					'4' => 'apr',
  					'5' => 'may',
  					'6' => 'jun',
  					'7' => 'jul',
  					'8' => 'aug',
  					'9' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec'
  				);
  return $d.'-'.$bulan[$m].'-'.$y;
}

function dateparseWithTime($tgl)
{  
	$temp	= explode(" ",$tgl);
	
	$temp_tgl	= explode("-",$temp[0]);
	
  $d = $temp_tgl[0];
  $m = $temp_tgl[1];
  $y = $temp_tgl[2];
	
  $bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec'
  				);
  return $d.'-'.$bulan[$m].'-'.$y." ".$temp[1];  
}

function dateparseD_Y_M($tgl)
{  
	$temp_tgl	= explode("-",$tgl);
	$d = $temp_tgl[0];
  $m = $temp_tgl[1];
  $y = $temp_tgl[2];
	
	
	
  $bulan = array(
  					'01' => 'jan',
  					'02' => 'feb',
  					'03' => 'mar',
  					'04' => 'apr',
  					'05' => 'may',
  					'06' => 'jun',
  					'07' => 'jul',
  					'08' => 'aug',
  					'09' => 'sep',
  					'10' => 'oct',
  					'11' => 'nov',
  					'12' => 'dec',
						'1' => 'jan',
  					'2' => 'feb',
  					'3' => 'mar',
  					'4' => 'apr',
  					'5' => 'may',
  					'6' => 'jun',
  					'7' => 'jul',
  					'8' => 'aug',
  					'9' => 'sep'
  				);
  return $d.'-'.$bulan[$m].'-'.$y;  
}

// konversi  bulan
function BulanString($bln)
{  

  $bulan = array(
  					'01' => 'Januari',
  					'02' => 'Februari',
  					'03' => 'Maret',
  					'04' => 'April',
  					'05' => 'Mei',
  					'06' => 'Juni',
  					'07' => 'Juli',
  					'08' => 'Agustus',
  					'09' => 'September',
		            '1' => 'Januari',
  					'2' => 'Februari',
  					'3' => 'Maret',
  					'4' => 'April',
  					'5' => 'Mei',
  					'6' => 'Juni',
  					'7' => 'Juli',
  					'8' => 'Agustus',
  					'9' => 'September',
  					'10' => 'Oktober',
  					'11' => 'November',
  					'12' => 'Desember'
  				);
					
  return $bulan[$bln];  
}

function BulanStringShort($bln)
{  

  $bulan = array(
  					'01' => 'Jan',
  					'02' => 'Feb',
  					'03' => 'Mar',
  					'04' => 'Apr',
  					'05' => 'Mei',
  					'06' => 'Jun',
  					'07' => 'Jul',
  					'08' => 'Agu',
  					'09' => 'Sep',
						'1' => 'Jan',
  					'2' => 'Feb',
  					'3' => 'Mar',
  					'4' => 'Apr',
  					'5' => 'Mei',
  					'6' => 'Jun',
  					'7' => 'Jul',
  					'8' => 'Agu',
  					'9' => 'Sep',
  					'10' => 'Okt',
  					'11' => 'Nov',
  					'12' => 'Des'
  				);
					
  return $bulan[$bln];  
}

// konversi 12.10.2007 ke 12-oct-2007
function TanggalToDate($tgl)
{
	$d = substr($tgl,0,2);
  	$m = substr($tgl,3,2);
  	$y = substr($tgl,6,4);
  return $d.'/'.$m.'/'.$y;
}

// konversi 2008-12-30  ke 30-12-2008
function FormatMySQLDateToTgl($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[2];
  $m 			= $temp[1];
  $y 			= $temp[0];
  return substr('0'.$d,-2).'-'.substr('0'.$m,-2).'-'.$y;
}

// konversi 2008-12-30  ke 30-12-2008
function FormatMySQLDateToTglWithTime($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[2];
  $m 			= $temp[1];
  $y 			= $temp[0];
  return $d.'-'.$m.'-'.$y." ".substr($temp_tgl[1],0,5);
}


// konversi  30-12-2008 ke 2008-12-30
function FormatTglToMySQLDate($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[0];
  $m 			= $temp[1];
  $y 			= $temp[2];
	
  return $y.'-'.substr("0".$m,-2).'-'.substr("0".$d,-2)." ".substr($temp_tgl[1],0,5);;
}

// konversi 2008-12-30  ke 30-12-2008
function FormatMySQLDateToTglEng($tgl)
{
	$temp_tgl	= explode(" ",$tgl);
	
	$temp		= explode('-',$temp_tgl[0]);
	
	$d 			= $temp[2];
  $m 			= $temp[1];
  $y 			= $temp[0];
  return substr('0'.$m,-2).'/'.substr('0'.$d,-2).'/'.$y;
}

function dateNow($with_time=false){
		
		/*
		Desc	:Mengembalikan tanggal hari ini
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NOW() AS tgl_sekarang";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$temp	= explode(" ",$row['tgl_sekarang']);
			
			if(!$with_time){
				$return	= $temp[0];
			}
			else{
				$return = $row['tgl_sekarang'];
			}
			
			return $return;
		} 
		else{
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
}//  END dateNow

function dateNowD_MMM_Y(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NOW() AS tgl_sekarang";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$temp_tgl	= explode(" ",$row['tgl_sekarang']);
			
			$temp	= explode("-",$temp_tgl[0]);
			
			$d	= $temp[2];
			$m	=  BulanStringShort($temp[1]);
			$y	= $temp[0];
			
			return $d."-".$m."-".$y;
		} 
		else{
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
}//  END dateNow


function dateY_M_D(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini dengan format YYYYMMDD
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT DATE(NOW())";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
		return $row[0];
		
		
}//  END dateYMD

function dateD_M_Y(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini dengan format DDMMYYYY
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				DAY({fn NOW()}) as tgl, 
				MONTH({fn NOW()}) as bln,
				YEAR({fn NOW()}) as thn";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$tgl	= $row['tgl'];
			$bln 	= $row['bln'];
			$thn	= $row['thn'];
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
		$tgl	= ($tgl<10)?"0".$tgl:$tgl;
		$bln	= ($bln<10)?"0".$bln:$bln;
		
		return $tgl."-".$bln."-".$thn;
		
		
}//  END dateYMD

function dateYMD(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini dengan format YYYYMMDD
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				DAY({fn NOW()}) as tgl, 
				MONTH({fn NOW()}) as bln,
				YEAR({fn NOW()}) as thn";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$tgl	= $row['tgl'];
			$bln 	= $row['bln'];
			$thn	= substr($row['thn'],-2);
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
		$tgl	= ($tgl<10)?"0".$tgl:$tgl;
		$bln	= ($bln<10)?"0".$bln:$bln;
		
		return $thn.$bln.$tgl;
		
		
}//  END dateYMD

function getMaxDate($bln,$thn){
	$bulan = array(
  					'01' => 31,
  					'02' => 28,
  					'03' => 31,
  					'04' => 30,
  					'05' => 31,
  					'06' => 30,
  					'07' => 31,
  					'08' => 31,
  					'09' => 30,
						'1' => 31,
  					'2' => 28,
  					'3' => 31,
  					'4' => 30,
  					'5' => 31,
  					'6' => 30,
  					'7' => 31,
  					'8' => 31,
  					'9' => 30,
  					'10' => 31,
  					'11' => 30,
  					'12' => 31
  				);
	
	if($thn%4==0){
		$bulan['02']	= 29;
		$bulan['2']		= 29;
	}
	
	return $bulan[$bln];
}

function HariStringShort($hr)
{  

  /*$hari = array(
  					'01' => 'Min',
  					'02' => 'Sen',
  					'03' => 'Sel',
  					'04' => 'Rab',
  					'05' => 'Kam',
  					'06' => 'Jum',
  					'07' => 'Sab',
						'1' => 'Min',
  					'2' => 'Sen',
  					'3' => 'Sel',
  					'4' => 'Rab',
  					'5' => 'Kam',
  					'6' => 'Jum',
  					'7' => 'Sab',
  				);        */
					
	 $hari = array(
  					'07' => 'Min',
  					'01' => 'Sen',
  					'02' => 'Sel',
  					'03' => 'Rab',
  					'04' => 'Kam',
  					'05' => 'Jum',
  					'06' => 'Sab',
						'7' => 'Min',
  					'1' => 'Sen',
  					'2' => 'Sel',
  					'3' => 'Rab',
  					'4' => 'Kam',
  					'5' => 'Jum',
  					'6' => 'Sab',
  				);        
					
  return $hari[$hr];  
}

function translateDay($day){
    $hari = array(
        'Monday'    => 'Senin',
        'Tuesday'   => 'Selasa',
        'Wednesday' => 'Rabu',
        'Thursday'  => 'Kamis',
        'Friday'    => 'Jumat',
        'Saturday'  => 'Sabtu',
        'Sunday'    => 'Minggu',
    );

    return $hari[$day];
}

function now(){
		
		/*
		Desc	:Mengembalikan tanggal hari ini
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				CONVERT(CHAR(25),{fn NOW()},103) waktu_sekarang";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row['waktu_sekarang'];
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
	}//  END now
/*
function pagingData($idx_page,$nama_tabel,$cari,$kondisi,$nama_file,$view_per_page,$page_per_section,$idx_awal_record){
	global $db;
	global $idx_awal_record;
	
	//menghitung jumlah data
		$idx_page = ($idx_page!='')?$idx_page:0;
		$kondisi	=($kondisi!='')?$kondisi:'';
		
		$sql = "SELECT ISNULL(COUNT(*),0) AS JumData FROM $nama_tabel $kondisi";
		
		if ($result = $db->sql_query($sql)){
			$my_data = $db->sql_fetchrow($result);
			$jum_data=$my_data['JumData'];
			
			$jum_halaman=ceil($jum_data/$view_per_page);
		}
		else{
			$jum_data=0;
			$jum_halaman=0;
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
		}
		
		$jum_halaman_for_idx=($jum_halaman>0)?$jum_halaman-1:0;
		
		$idx_next=($idx_page+1<$jum_halaman_for_idx)?$idx_page+1:$jum_halaman_for_idx;
		$idx_prev=($idx_page-1>0)?$idx_page-1:0;
		
		$page_first 	=($idx_page!=$idx_prev)?"<a href='".append_sid("$nama_file?page=0&cari=$cari")."'>First</a>&nbsp;&nbsp;&nbsp;":"";
		$page_last 		=($idx_page!=$idx_next)?"&nbsp;&nbsp;&nbsp;<a href='".append_sid("$nama_file?page=$jum_halaman_for_idx&cari=$cari")."'>Last</a>":"";
		
		$page_next	= ($idx_page!=$idx_next)?
			"<a href='".append_sid("$nama_file.".$phpEx."?page=$idx_next&cari=$cari")."'>Next</a>":"";
			
		$page_prev 	=($idx_page!=$idx_prev)?
			"<a href='".append_sid("$nama_file.".$phpEx."?page=$idx_prev&cari=$cari")."'>Prev</a>":"";
		
		$section_idx_page="";
		
		$start_iterasi	=(floor(($idx_page)/$page_per_section)*$page_per_section)+1;
		
		$max_iterasi		=($start_iterasi+$page_per_section-1<$jum_halaman)?$start_iterasi+$page_per_section-1:$jum_halaman;
		
		if($start_iterasi>1){
			$temp_idx=$start_iterasi-2;
			$extend_section_prev	="<a href='".append_sid("$nama_file?page=$temp_idx&cari=$cari")."'>...</a>";
		}
		
		$extend_section_next=($max_iterasi<$jum_halaman)?
				"<a href='".append_sid("$nama_file?page=$max_iterasi&cari=$cari")."'>...</a>":"";
		
		for($idx=$start_iterasi;$idx<=$max_iterasi;$idx++){
			$temp_idx=$idx-1;
			$section_idx_page .=($temp_idx!=$idx_page)?"<a href='".append_sid("$nama_file?page=$temp_idx&cari=$cari")."'>&nbsp;<u>$idx</u>&nbsp;</a>":"&nbsp;[$idx]&nbsp;";
		}
		
		$section_idx_page =$extend_section_prev.$section_idx_page.$extend_section_next;
		
		$paging= "<a>Halaman:</a>&nbsp;".$page_first.$page_prev."&nbsp;&nbsp;".$section_idx_page."&nbsp;&nbsp;".$page_next.$page_last;
		
		$idx_awal_record	= $idx_page*$view_per_page;
		
		return $paging;
}*/

function pagingData($idx_page,$field_id,$nama_tabel,$parameter,$kondisi,$nama_file,$view_per_page,$page_per_section,$idx_awal_record){

	global $db;
	global $idx_awal_record;

	//menghitung jumlah data
	$idx_page = ($idx_page!='')?$idx_page:0;
	$kondisi	=($kondisi!='')?$kondisi:'';

	$field_id	= $field_id==""?"1":$field_id;

	if($nama_tabel == 'tbl_permissions'){
		$sql = "SELECT COUNT(*) AS JumData FROM $nama_tabel $kondisi GROUP BY page_id";

		if ($result = $db->sql_query($sql)){
			$a = 1;
			while($my_data = $db->sql_fetchrow($result)){
				$a++;
			}
			$jum_data=$a;

			$jum_halaman=ceil($jum_data/$view_per_page);
		}
		else{
			$jum_data=0;
			$jum_halaman=0;
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
		}
	}else{
		$sql = "SELECT COUNT($field_id) AS JumData FROM $nama_tabel $kondisi";

		if ($result = $db->sql_query($sql)){
			$my_data = $db->sql_fetchrow($result);
			$jum_data=$my_data['JumData'];

			$jum_halaman=ceil($jum_data/$view_per_page);
		}
		else{
			$jum_data=0;
			$jum_halaman=0;
			//die_error('Cannot Load mobil',__FILE__,__LINE__,$sql);
		}
	}

	$jum_halaman_for_idx=($jum_halaman>0)?$jum_halaman-1:0;

	$idx_next=($idx_page+1<$jum_halaman_for_idx)?$idx_page+1:$jum_halaman_for_idx;
	$idx_prev=($idx_page-1>0)?$idx_page-1:0;

	if($nama_file == 'pengaturan_page.php?mode=detail_page'){
		$page_first 	=($idx_page!=$idx_prev)?"<a href='".append_sid("$nama_file&page=0".$parameter)."'>First</a>&nbsp;&nbsp;&nbsp;":"";
		$page_last 		=($idx_page!=$idx_next)?"&nbsp;&nbsp;&nbsp;<a href='".append_sid("$nama_file&page=$jum_halaman_for_idx".$parameter)."'>Last</a>":"";

		$page_next	= ($idx_page!=$idx_next)?
			"<a href='".append_sid("$nama_file&page=$idx_next".$parameter)."'>Next</a>":"";
		$page_prev 	=($idx_page!=$idx_prev)?
			"<a href='".append_sid("$nama_file&page=$idx_prev".$parameter)."'>Prev</a>":"";

		$section_idx_page="";

		$start_iterasi	=(floor(($idx_page)/$page_per_section)*$page_per_section)+1;

		$max_iterasi		=($start_iterasi+$page_per_section-1<$jum_halaman)?$start_iterasi+$page_per_section-1:$jum_halaman;

		if($start_iterasi>1){
			$temp_idx=$start_iterasi-2;
			$extend_section_prev	="<a href='".append_sid("$nama_file&page=$temp_idx".$parameter)."'>...</a>";
		}

		$extend_section_next=($max_iterasi<$jum_halaman)?
			"<a href='".append_sid("$nama_file&page=$max_iterasi".$parameter)."'>...</a>":"";

		for($idx=$start_iterasi;$idx<=$max_iterasi;$idx++){
			$temp_idx=$idx-1;
			$section_idx_page .=($temp_idx!=$idx_page)?"<a href='".append_sid("$nama_file&page=$temp_idx".$parameter)."'>&nbsp;<u>$idx</u>&nbsp;</a>":"&nbsp;[$idx]&nbsp;";
		}
	}else{
		$page_first 	=($idx_page!=$idx_prev)?"<a href='".append_sid("$nama_file?page=0".$parameter)."'>First</a>&nbsp;&nbsp;&nbsp;":"";
		$page_last 		=($idx_page!=$idx_next)?"&nbsp;&nbsp;&nbsp;<a href='".append_sid("$nama_file?page=$jum_halaman_for_idx".$parameter)."'>Last</a>":"";

		$page_next	= ($idx_page!=$idx_next)?
			"<a href='".append_sid("$nama_file?page=$idx_next".$parameter)."'>Next</a>":"";

		$page_prev 	=($idx_page!=$idx_prev)?
			"<a href='".append_sid("$nama_file?page=$idx_prev".$parameter)."'>Prev</a>":"";

		$section_idx_page="";

		$start_iterasi	=(floor(($idx_page)/$page_per_section)*$page_per_section)+1;

		$max_iterasi		=($start_iterasi+$page_per_section-1<$jum_halaman)?$start_iterasi+$page_per_section-1:$jum_halaman;

		if($start_iterasi>1){
			$temp_idx=$start_iterasi-2;
			$extend_section_prev	="<a href='".append_sid("$nama_file?page=$temp_idx".$parameter)."'>...</a>";
		}

		$extend_section_next=($max_iterasi<$jum_halaman)?
			"<a href='".append_sid("$nama_file?page=$max_iterasi".$parameter)."'>...</a>":"";

		for($idx=$start_iterasi;$idx<=$max_iterasi;$idx++){
			$temp_idx=$idx-1;
			$section_idx_page .=($temp_idx!=$idx_page)?"<a href='".append_sid("$nama_file?page=$temp_idx".$parameter)."'>&nbsp;<u>$idx</u>&nbsp;</a>":"&nbsp;[$idx]&nbsp;";
		}
	}

	$section_idx_page =$extend_section_prev.$section_idx_page.$extend_section_next;

	$paging= "<a>Jumlah Data: </a><b>".number_format($jum_data,0,",",".")."</b> | <a>Halaman:</a>&nbsp;".$page_first.$page_prev."&nbsp;&nbsp;".$section_idx_page."&nbsp;&nbsp;".$page_next.$page_last;

	$idx_awal_record	= $idx_page*$view_per_page;
	return $paging;
}

function setPaging($idx_page,$submit_form){

  global $db,$VIEW_PER_PAGE,$PAGE_PER_SECTION;

  //menghitung jumlah data
  $idx_page = ($idx_page!='')?$idx_page:0;

  $sql = "SELECT FOUND_ROWS()";

  $result     = $db->sql_query($sql);
  $row        = $db->sql_fetchrow($result);
  $jum_data   = $row[0];
  $jum_halaman= ceil($jum_data/$VIEW_PER_PAGE);

  $jum_halaman_for_idx=($jum_halaman>0)?$jum_halaman-1:0;

  $idx_next=($idx_page+1<$jum_halaman_for_idx)?$idx_page+1:$jum_halaman_for_idx;
  $idx_prev=($idx_page-1>0)?$idx_page-1:0;

  $page_first 	= ($idx_page!=$idx_prev)?"<span class='pagepaging' onClick='pagingGotoPage(0,$submit_form);'>First</span>&nbsp;":"";
  $page_last 		= ($idx_page!=$idx_next)?"&nbsp;<span class='pagepaging' onClick='pagingGotoPage($jum_halaman_for_idx,$submit_form);'>Last</span>":"";
  $page_next	  = ($idx_page!=$idx_next)?"&nbsp;<span class='pagepaging' onClick='pagingGotoPage($idx_next,$submit_form);'>Next</span>&nbsp;":"";
  $page_prev 	  = ($idx_page!=$idx_prev)?"&nbsp;<span class='pagepaging' onClick='pagingGotoPage($idx_prev,$submit_form);'>Prev</span>&nbsp;":"";

  $section_idx_page="";

  $start_iterasi	=(floor(($idx_page)/$PAGE_PER_SECTION)*$PAGE_PER_SECTION)+1;

  $max_iterasi		=($start_iterasi+$PAGE_PER_SECTION-1<$jum_halaman)?$start_iterasi+$PAGE_PER_SECTION-1:$jum_halaman;

  if($start_iterasi>1){
    $temp_idx=$start_iterasi-2;
    $extend_section_prev	="&nbsp;<span class='pagepaging' onClick='pagingGotoPage($temp_idx,$submit_form);'>...</span>&nbsp;";
  }

  $extend_section_next=($max_iterasi<$jum_halaman)?"&nbsp;<span class='pagepaging' onClick='pagingGotoPage($max_iterasi,$submit_form);'>...</span>&nbsp;":"";

  for($idx=$start_iterasi;$idx<=$max_iterasi;$idx++){
    $temp_idx=$idx-1;
    $section_idx_page .=($temp_idx!=$idx_page)?"&nbsp;<span class='pagepaging' onClick='pagingGotoPage($temp_idx,$submit_form);'>$idx</span>&nbsp;":"&nbsp;$idx&nbsp;";
  }

  $section_idx_page =$extend_section_prev.$section_idx_page.$extend_section_next;

  $paging= "<a>Records: </a><b>".number_format($jum_data,0,",",".")."</b> | <a>Pages:</a>&nbsp;".$page_first.$page_prev."&nbsp;&nbsp;".$section_idx_page."&nbsp;&nbsp;".$page_next.$page_last;

  return $paging;
}

// Memparse Teks Biar ga Pake karakter ilegal yang bisa dipake SQL-Inject
function adp_clean($username)
{
  $username = substr(htmlspecialchars(str_replace("\'", "'", trim($username))), 0, 25);
  $username = adp_rtrim($username, "\\");
  $username = str_replace("'", "\'", $username);
  return $username;
}

// Right Trim
function adp_rtrim($str, $charlist = false)
{
	if ($charlist === false)
	{
		return rtrim($str);
	}
	$php_version = explode('.', PHP_VERSION);
	if ((int) $php_version[0] < 4 || ((int) $php_version[0] == 4 && (int) $php_version[1] < 1))
	{
		while ($str{strlen($str)-1} == $charlist)
		{
			$str = substr($str, 0, strlen($str)-1);
		}
	}
	else
	{
		$str = rtrim($str, $charlist);
	}
	return $str;
}

// Inisialisasi userdata
function init_userprefs($userdata)
{
	global $template, $adp_root_path;
	global $nav_links;
	global $config;
	global $db;
	
/*	Navigasi Untuk Menunya Mozilla
	// navigation links
	$nav_links['top'] = array (
		'url' => append_sid($adp_root_path . 'index.' . $phpEx),
		'title' => sprintf($lang['Forum_Index'], $board_config['sitename'])
	);
	$nav_links['search'] = array (
		'url' => append_sid($adp_root_path . 'search.' . $phpEx),
		'title' => $lang['Search']
	);
	$nav_links['help'] = array (
		'url' => append_sid($adp_root_path . 'faq.' . $phpEx),
		'title' => $lang['FAQ']
	);
	$nav_links['author'] = array (
		'url' => append_sid($adp_root_path . 'memberlist.' . $phpEx),
		'title' => $lang['Memberlist']
	);
*/	
	$theme = setup_style($config['template']); // siapin template
	
	//UPDATE WAKTU UPDATE TERAKHIR USER
	$sql	= "UPDATE tbl_user SET waktu_update_terakhir=NOW() WHERE user_id='$userdata[user_id]'";
	
	if (!$db->sql_query($sql)){
		echo("Err $this->ID_FILE".__LINE__);exit;
	}
	
	return;
}

// nyiapin template
function setup_style($style)
{
	global $template, $adp_root_path;
	// lokasi template kita
	$template_path = 'templates/' ;
	// nama templatenya
	$template_name = $style;
	// bikin objek template
	$template = new Template($adp_root_path . $template_path . $template_name);
}

// encoding ip
function encode_ip($dotquad_ip)
{
	$ip_sep = explode('.', $dotquad_ip);
	return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
}

// decoding ip
function decode_ip($int_ip)
{
	$hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
	return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

// bikin link buat paging
function generate_pagination($base_url, $num_items, $per_page, $start_item, $add_prevnext_text = TRUE)
{
	global $lang;
	$total_pages = ceil($num_items/$per_page);
	if ( $total_pages == 1 )
	{
		return '';
	}
	$on_page = floor($start_item / $per_page) + 1;
	$page_string = '';
	if ( $total_pages > 10 )
	{
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;

		for($i = 1; $i < $init_page_max + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
			if ( $i <  $init_page_max )
			{
				$page_string .= ", ";
			}
		}

		if ( $total_pages > 3 )
		{
			if ( $on_page > 1  && $on_page < $total_pages )
			{
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
				{
					$page_string .= ($i == $on_page) ? '<b>' . $i . '</b>' : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
					if ( $i <  $init_page_max + 1 )
					{
						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			}
			else
			{
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>'  : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
				if( $i <  $total_pages )
				{
					$page_string .= ", ";
				}
			}
		}
	}
	else
	{
		for($i = 1; $i < $total_pages + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : '<a href="' . append_sid($base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) ) . '">' . $i . '</a>';
			if ( $i <  $total_pages )
			{
				$page_string .= ', ';
			}
		}
	}

	if ( $add_prevnext_text )
	{
		if ( $on_page > 1 )
		{
			$page_string = ' <a href="' . append_sid($base_url . "&amp;start=" . ( ( $on_page - 2 ) * $per_page ) ) . '">' . $lang['Previous'] . '</a>&nbsp;&nbsp;' . $page_string;
		}

		if ( $on_page < $total_pages )
		{
			$page_string .= '&nbsp;&nbsp;<a href="' . append_sid($base_url . "&amp;start=" . ( $on_page * $per_page ) ) . '">' . $lang['Next'] . '</a>';
		}

	}

	$page_string = $lang['Goto_page'] . ' ' . $page_string;
	return $page_string;
}

// pregmatic quotation
function adp_preg_quote($str, $delimiter)
{
	$text = preg_quote($str);
	$text = str_replace($delimiter, '\\' . $delimiter, $text);
	return $text;
}

// realpath
function adp_realpath($path)
{
	global $adp_root_path;
	return (!@function_exists('realpath') || !@realpath($adp_root_path . 'includes/functions.php')) ? $path : @realpath($path);
}

// pesan error
function die_error($message, $line='', $file ='',$sql='')
{
  global $template,$config;
  global $adp_root_path;
  
  if (!defined('HEADER_INC')){
	  if (empty($template)){
		  $template = new Template($adp_root_path . 'templates/' . $config['template']);
	  }
  }

  if($file!=""){
    $error_file_line  = "[ ".$file.($line!=""?":".$line:"")." ]";
  }
  else{
    $error_file_line  = "";
  }

  $template->set_filenames(array('error' => 'error_body.tpl')); 
  $template->assign_vars(
    array(
      "SITENAME"  => $config["site_name"],
      "PESAN"     => $message ."<br/>".$error_file_line,
    ));

  $template->pparse('error');

  exit;
}

// pesan biasa yang menggunakan satu click here
function die_message($message, $back1='',$back2='',$message_picture='forbiden.png')
{
  global $template,$config;
  global $db;
  global $phpEx;
  global $adp_root_path;

  
  if (!defined('HEADER_INC'))
  {
	if ( empty($template) )
	{
		$template = new Template($adp_root_path . 'templates/' . $config['template']);
	}
	if ( empty($theme) )
	{
		$theme = setup_style($config['template']);
	}
	include($adp_root_path . 'includes/page_header.'.$phpEx);
  } 
  
  $template->set_filenames(array('error' => 'message_body.tpl')); 
  $template->assign_vars(
    array(
    'MESSAGE_TITLE'		=>'Message',
    'MESSAGE_CONTENT'	=>$message,
    'U_BACK1'				 	=> $back1,
    'U_BACK2' 				=> $back2,
    'MESSAGE_PICTURE' => $message_picture
    )
   );
  $template->pparse('error');
  
  if ( !defined('IN_ADMIN') )
  {
	//include($adp_root_path . 'includes/page_tail.'.$phpEx);
  }
  else
  {
	include($adp_root_path . 'admin/page_footer_admin.'.$phpEx);
  }   
  exit;
}


// redirection ke lokasi lain
function redirect($url)
{
	global $db,$config;
	if (!empty($db))
	{
		$db->sql_close(); // kalo mo redirect, close koneksi database kita
	}
	if (strstr(urldecode($url), "\n") || strstr(urldecode($url), "\r"))
	{	    
		die('Tried to redirect to potentially insecure url.'); // cek kalo ada usaha jahat ;p
	}
	$server_protocol = $config['protocol'];
	$server_name = ($config['script']!="")?$config['name'] . '/' . $config['script']:".";
	//$server_port = 80;
	$script_name = '';	
	$script_name = ($script_name == '') ? $script_name : './' . $script_name;
	// url yang mo diloncatin ;p
	$url = preg_replace('#^\/?(.*?)\/?$#', '/\1', trim($url));
	if (@preg_match('/Microsoft|WebSTAR|Xitami/', getenv('SERVER_SOFTWARE')))
	{
		header('Refresh: 0; URL=' . $server_protocol . $server_name . $script_name . $url);
		echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><meta http-equiv="refresh" content="0; url=' . $server_protocol . $server_name . $server_port . $script_name . $url . '"><title>Redirect</title></head><body><div align="center">If your browser does not support meta redirection please click <a href="' . $server_protocol . $server_name . $server_port . $script_name . $url . '">HERE</a> to be redirected</div></body></html>';
		exit;
	}	
	header('Location: ' . $server_protocol . $server_name . $script_name . $url);
	exit;
}

function array_orderby()
{
		//$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
		//$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
		
		$args	= array();
		
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}

//UNTUK PHP 5
/*function multiSortArray($data, $field) {
	if (!is_array($field)) {    //if the field is given as a string, assume ascending
		$field = array($field=>true);
  }

  usort($data, function($a, $b) use($field) {
		$retval = 0;
    foreach ($field as $fieldname=>$asc) {
			if ($retval == 0) {
				$retval = strnatcmp($a[$fieldname], $b[$fieldname]);
        if(!$asc) $retval *= -1;    //if
      }
    }
		return $retval;
  });
  return $data;
}*/

function sendHttpPost($url,$parameter){

  $length = strlen($parameter);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_GET, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded", "Content-length: $length"));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		
		return $response;
}

function idxHariToStringHari($idx_day){
	switch($idx_day){
		case 6:
			return "Minggu";
		case 0:
			return "Senin";
		case 1:
			return "Selasa";
		case 2:
			return "Rabu";
		case 3:
			return "Kamis";
		case 4:
			return "Jumat";
		case 5:
			return "Sabtu";
	}


}

function setHalamanMenu($id_page){
  global $adp_root_path;
  global $userdata;
  global $db;
  global $template;

  include_once($adp_root_path . 'ClassPermission.php');

  $Permission = new Permission();

  $res_group_menu  = $Permission->getGroupMenu($userdata["user_level"]);

  while($row=$db->sql_fetchrow($res_group_menu)){
    $section  = "menu".$row["PageId"];

    $class  = ($row["PageId"]!=$id_page)?"top_menu_lost_focus":"top_menu_on_select";

    $template->assign_block_vars($section,array("class"=>$class));
  }

  $template->set_filenames(array("topmenu"=> 'overall_header_menu.tpl'));
  $template->pparse("topmenu");

  $res_menu  = $Permission->getMenu($userdata["user_level"],$id_page);

  $i=0;

  while($row=$db->sql_fetchrow($res_menu)){
    $i++;

    if($i%4>0){
      $template->assign_block_vars("menu",array(
        "url"       => $row["NamaFile"],
        "namamenu"  => $row["NamaTampil"],
        "icon"      => $row["ImageIcon"]
      ));
    }
    else{
      $template->assign_block_vars("menu",array(
        "url"       => $row["NamaFile"],
        "namamenu"  => $row["NamaTampil"],
        "icon"      => $row["ImageIcon"],
        "br"        => "90px"
      ));
    }
  }

  $template->set_filenames(array("menu" => "menu.tpl"));
  $template->pparse("menu");

}

function setBcrump($id_page,$parameter=null){

  $data_page  = getGrupMenu($id_page);

  $bcrump = "<a href='" . $data_page["NamaFileGrup"] . "'>Home</a> <span class='bcrumparrow'></span> ";
  $bcrump.= "<a href='" . $data_page["NamaFile"] . "'>".$data_page["NamaTampilFile"]."</a> <span class='bcrumparrow'></span> ";

  if(sizeof($parameter)) {
    foreach ($parameter as $value) {
      $bcrump .= "<a href='" . append_sid($value["url"]) . "'>" . $value["label"] . "</a> <span class='bcrumparrow'></span> ";
    }
  }

  return substr($bcrump,0,-35);
}

function cleansingString($str){
  return trim(preg_replace("/[^A-Za-z0-9 ]/","",$str));
}

function gotoMainpage($IdRole){
  global $db;

  $sql =
    "SELECT NamaFile FROM tbl_permissions t INNER JOIN tbl_pages tp ON t.PageId=tp.PageId
      WHERE t.IdRole=$IdRole ORDER BY t.PageId LIMIT 0,1";

  if(!$result = $db->sql_query($sql)){
    echo("Err:FUNC".__LINE__);exit;
  }

  $row  = $db->sql_fetchrow($result);

  return $row["NamaFile"];
}

function getGrupMenu($PageId){
  global $db;

  $sql =
    "SELECT t1.NamaFile,t1.NamaTampil AS NamaTampilFile,t2.NamaFile AS NamaFileGrup, t2.NamaTampil AS NamaTampilGrup  FROM tbl_pages t1 INNER JOIN tbl_pages t2 ON t1.GroupPage=t2.PageId
      WHERE t1.PageId=$PageId";

  if(!$result = $db->sql_query($sql)){
    echo("Err: FUNC".__LINE__);exit;
  }

  $row  = $db->sql_fetchrow($result);

  return $row;
}

function getVariabel($nama_variabel){
  global $HTTP_POST_VARS,$HTTP_GET_VARS;

  return isset($HTTP_POST_VARS[$nama_variabel])?$HTTP_POST_VARS[$nama_variabel] : $HTTP_GET_VARS[$nama_variabel];
}

function verifySelisihWaktu($tgl,$jam_berangkat,$toleransi){
  global $db;

  $sql  = "SELECT IF(TIME_TO_SEC(TIMEDIFF(CONCAT('$tgl ','$jam_berangkat',':59'),NOW()))/60>=-$toleransi,1,0) AS VerifyWaktu";

  if(!$result=$db->sql_query($sql)){
    echo("Err: FUNC ".__LINE__);exit;
  }

  $row  = $db->sql_fetchrow($result);

  return $row[0];
}
?>