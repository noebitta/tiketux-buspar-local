<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    //redirect('index.'.$phpEx,true);
    exit;
}
//#############################################################################

class TargetCabang{
    //KAMUS GLOBAL
    var $ID_FILE; //ID Kelas

    //CONSTRUCTOR
    function TargetCabang(){
        $this->ID_FILE="C-TGC";
    }
    function addTransaksiPax($jumlah_transaksi_pax){
        global $db;
        global $userdata;

        $jumlah_transaksi_pax   = $jumlah_transaksi_pax!=""?$jumlah_transaksi_pax:0;

        $sql = "UPDATE tbl_md_cabang_target
                SET
				  TargetPaxTercapai   = TargetPaxTercapai+$jumlah_transaksi_pax
			    WHERE KodeCabang='$userdata[KodeCabang]' AND BulanTarget=MONTH(NOW()) AND TahunTarget=YEAR(NOW())";

        if (!$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        return true;
    }
    function addTransaksiPaket($jumlah_transaksi_paket){
        global $db;
        global $userdata;

        $jumlah_transaksi_paket = $jumlah_transaksi_paket!=""?$jumlah_transaksi_paket:0;

        $sql = "UPDATE tbl_md_cabang_target
                SET
				  TargetPaketTercapai = $jumlah_transaksi_paket + TargetPaketTercapai
			    WHERE KodeCabang='$userdata[KodeCabang]' AND BulanTarget=MONTH(NOW()) AND TahunTarget=YEAR(NOW())";

        if (!$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        return true;
    }
    function resetTarget($BulanTarget,$TahunTarget,$Log){

        /*MERESET DATA TARGET SEMUA CABANG MENJADI 0*/

        //kamus
        global $db;

        $sql =
            "UPDATE tbl_md_cabang_target
			SET
				TargetPax='0', TargetPaket='0',
				Log=CONCAT(IF(Log IS NULL,'',Log),';','$Log')
			WHERE BulanTarget='$BulanTarget' AND TahunTarget='$TahunTarget';";

        if (!$db->sql_query($sql)){
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        return true;
    }//end resetTarget
    function UbahTarget($BulanTarget,$TahunTarget,$KodeCabang,$Targetpax,$Targetpaket,$Log){
        //kamus
        global $db;

        //MENAMBAHKAN DATA KEDALAM DATABASE
        $sql =
            "UPDATE tbl_md_cabang_target
			SET
				TargetPax='$Targetpax',TargetPaket='$Targetpaket',
				Log=CONCAT(IF(Log IS NULL,'',Log),';','$Log')
			WHERE KodeCabang='$KodeCabang' AND BulanTarget='$BulanTarget' AND TahunTarget='$TahunTarget';";

        if (!$db->sql_query($sql)){
            //die(mysql_error());
            echo("Err: $this->ID_FILE".__LINE__);exit;
        }

        if($db->sql_affectedrows()<=0){
            /*BELUM PERNAH DI SET TARGET, MAKA AKAN DITAMBAHKAN TARGET KE DATABASE*/

            $sql =
                "INSERT INTO tbl_md_cabang_target(
                              KodeCabang,BulanTarget,
                              TahunTarget,TargetPax,TargetPaxTercapai,TargetPaket,TargetPaketTercapai,
                              Log)
                VALUES(
                    '$KodeCabang','$BulanTarget',
                    '$TahunTarget','$Targetpax',0,'$Targetpaket',0,
                    '$Log');";

            if (!$db->sql_query($sql)){
                //die(mysql_error());
                echo("Err: $this->ID_FILE".__LINE__);exit;
            }
        }

        return true;
    }
}
?>