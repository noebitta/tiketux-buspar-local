<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 601;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';


// PARAMETER
$perpage 		= $config['perpage'];

$tanggal_mulai  = isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$cari           = isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$id_member  	  = isset($HTTP_GET_VARS['idmember'])? $HTTP_GET_VARS['idmember'] : $HTTP_POST_VARS['idmember'];

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Transaksi Poin Member '.$id_member.' Periode'.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir));
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Waktu Transaksi');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', '#Reff');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Token');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Trx (poin)');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jenis Trx');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Poin');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$idx=0;

$sql=
  "SELECT *
	FROM tbl_member_deposit_trx_log
	WHERE
	  (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai' AND '$tanggal_akhir')
	  AND (KodeReferensi LIKE '$cari%'
		OR Keterangan LIKE '%$cari%')
		AND IdMember='$id_member'";

if (!$result = $db->sql_query($sql)){
  echo("Err:".__LINE__);
  exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
  $idx++;
  $idx_row=$idx+3;

  $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
  $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])));
  $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeReferensi']);
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Token']);
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['Keterangan']);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Jumlah']);
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['IsDebit']==1?"DB":"CR");
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['Saldo']);

  $total_debit	+= $row['IsDebit']==1?$row['Jumlah']:0;
  $total_kredit += $row['IsDebit']==0?$row['Jumlah']:0;

}
$temp_idx=$idx_row;
$idx_row++;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Trx Debit');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_debit);
$idx_row++;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Trx Kredit');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_kredit);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($idx>0){

  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Transaksi Poin Member Periode '.dateparse($tanggal_mulai).' s/d '.dateparse($tanggal_akhir).'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
}



?>
