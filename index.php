<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';

include($adp_root_path . 'common.php');

// TEMPLATE
$template->set_filenames(array('body' => 'main_body.tpl')); 
$template->assign_vars(array(
	'SITENAME'		=>$config["site_name"],
	));

// PARSE
$template->pparse('body');
?>