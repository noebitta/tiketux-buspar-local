<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
//SESSION  =
$id_page = 505;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){
    die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

$Cabang     = new Cabang();
$Jurusan    = new Jurusan();


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$type  			= isset($HTTP_GET_VARS['berdasarkan'])? $HTTP_GET_VARS['berdasarkan'] : $HTTP_POST_VARS['berdasarkan'];
$username		= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_jurusan_online/laporan_omzet_jurusan_online_body.tpl'));

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	        = ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	        = ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari2	=($cari=="")?"WHERE 1 ":
    " WHERE (KodeJurusan LIKE '$cari%'
		OR KodeCabangAsal LIKE '%$cari%'
		OR KodeCabangTujuan LIKE '%$cari%')";


if($userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])){
    $kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";
    $kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";
}
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeJurusan","tbl_md_jurusan",
    "&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
    $kondisi_cari,"laporan_omzet_jurusan_online.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

switch($type){
    case 1 :
        $date_filter = 'TglBerangkat';
        break;
    case 2 :
        $date_filter = 'WaktuCetakTiket';
        break;
    default :
        $date_filter = 'WaktuCetakTiket';
        break;
}

//MENGAMBIL DATA-DATA MASTER Jurusan
$sql_jurusan= "SELECT KodeJurusan, f_cabang_get_name_by_kode(KodeCabangAsal) AS KodeCabangAsal, f_cabang_get_name_by_kode(KodeCabangTujuan) AS KodeCabangTujuan
	           FROM tbl_md_jurusan
	           $kondisi_cari2
	           ORDER BY KodeJurusan ASC";


if (!$result_laporan = $db->sql_query($sql_jurusan)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}

//DATA PENJUALAN TIKET
$sql	= "SELECT
            KodeJurusan , KodeCabangAsal, KodeCabangTujuan,
            IS_NULL(COUNT(IF(PetugasPenjual=0 AND FlagBatal!=1,NoTiket,NULL)),0) AS TotalPenumpangO,
            IS_NULL(SUM(IF(PetugasPenjual=0 AND FlagBatal!=1,Total,0)),0) AS TotalPenjualanTiket
	        FROM tbl_reservasi JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
	        WHERE (DATE($date_filter) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND CetakTiket = 1
	        $kondisi_cabang
	        GROUP BY KodeJurusan ORDER BY KodeJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}
while ($row = $db->sql_fetchrow($result)){
    $data_tiket_total[$row['KodeJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

$total_t 	= 0;
$total_s 	= 0;
$total_p 	= 0;

while ($row = $db->sql_fetchrow($result_laporan)){
    $temp_array[$idx]['KodeJurusan']			= $row['KodeJurusan'];
    $temp_array[$idx]['CabangAsal']				= $row['KodeCabangAsal'];
    $temp_array[$idx]['CabangTujuan']			= $row['KodeCabangTujuan'];
    $temp_array[$idx]['TotalTiket']				= $data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO'];
    $temp_array[$idx]['TotalPenjualanTiket']	= $data_tiket_total[$row['KodeJurusan']]['TotalPenjualanTiket'];
    $temp_array[$idx]['Komisi']					= ($data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO']*5000);

    $total_t	+= $data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO'];
    $total_p	+= ($data_tiket_total[$row['KodeJurusan']]['TotalPenjualanTiket']);
    $total_s	+= ($data_tiket_total[$row['KodeJurusan']]['TotalPenjualanTiket'])-(($data_tiket_total[$row['KodeJurusan']]['TotalPenumpangO']*5000));

    $idx++;
}


$idx=$idx_awal_record;

while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
    //total tiket
    $total_tiket						= $temp_array[$idx]['TotalTiket'];
    $total_penjualan_tiket 				= $temp_array[$idx]['TotalPenjualanTiket'];
    $total_komisi		 				= $temp_array[$idx]['Komisi'];
    $total_setor 						= $total_penjualan_tiket - $total_komisi;

    $odd ='odd';

    if (($idx % 2)==0){
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'                   =>$odd,
            'no'                    =>$idx+1,
            'jurusan'               =>$temp_array[$idx]['KodeJurusan'],
            'cabangasal'            =>$temp_array[$idx]['CabangAsal'],
            'cabangtujuan'          =>$temp_array[$idx]['CabangTujuan'],
            'total_penumpang'       =>number_format($total_tiket,0,",","."),
            'total_penjualan_tiket' =>number_format($total_penjualan_tiket,0,",","."),
            'total_setor'           =>number_format($total_setor,0,",","."),
        )
    );

    $idx++;
}
$parameter_cetak = "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&berdasarkan=".$type."&txt_cari=".$cari;
$script_excel = "Start('laporan_omzet_jurusan_online_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
$page_title	= "Penjualan Per Jurusan";

$template->assign_vars(array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('laporan_omzet_jurusan_online.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'TGL_AWAL'			=> $tanggal_mulai,
        'TGL_AKHIR'			=> $tanggal_akhir,
        'PAGING'			=> $paging,
        'CETAK_XL'          => $script_excel
    )
);


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>