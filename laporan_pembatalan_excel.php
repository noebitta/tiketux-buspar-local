<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 210;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tgl_awal				= isset($HTTP_GET_VARS['tgl_awal'])? $HTTP_GET_VARS['tgl_awal'] : $HTTP_POST_VARS['tgl_awal']; 
$tgl_akhir			= isset($HTTP_GET_VARS['tgl_akhir'])? $HTTP_GET_VARS['tgl_akhir'] : $HTTP_POST_VARS['tgl_akhir']; 
$bulan					= isset($HTTP_GET_VARS['opt_bulan'])? $HTTP_GET_VARS['opt_bulan'] : $HTTP_POST_VARS['opt_bulan']; 
$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun']; 
$kota_asal			= isset($HTTP_GET_VARS['kota_asal'])? $HTTP_GET_VARS['kota_asal'] : $HTTP_POST_VARS['kota_asal']; 
$cabang_asal		= isset($HTTP_GET_VARS['opt_cabang_asal'])? $HTTP_GET_VARS['opt_cabang_asal'] : $HTTP_POST_VARS['opt_cabang_asal']; 
$cabang_tujuan	= isset($HTTP_GET_VARS['opt_tujuan'])? $HTTP_GET_VARS['opt_tujuan'] : $HTTP_POST_VARS['opt_tujuan']; 
$kode_jadwal		= isset($HTTP_GET_VARS['opt_jam'])? $HTTP_GET_VARS['opt_jam'] : $HTTP_POST_VARS['opt_jam']; 
$status					= isset($HTTP_GET_VARS['status'])? $HTTP_GET_VARS['status'] : $HTTP_POST_VARS['status']; 
$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari']; 

//pengaturan periode
if($tgl_awal>$tgl_akhir){
	$temp_tgl		= $tgl_akhir;
	$tgl_akhir	= $tgl_awal;
	$tgl_awal		= $temp_tgl;
}

$tahun	= ($tahun=="")?date("y"):$tahun;
$bulan	= ($bulan=="")?substr('0'.date("m"),-2):$bulan;

$tgl_maximum	= getMaxDate($bulan,'20'.$tahun);

if($tgl_awal==""){
	$tgl_awal	= date("d");
}
else if($tgl_awal>$tgl_maximum){
	$tgl_awal	= $tgl_maximum;
}

if($tgl_akhir==""){
	$tgl_akhir	= date("d");
}
else if($tgl_akhir>$tgl_maximum){
	$tgl_akhir	= $tgl_maximum;
}

$tgl_awal_mysql	= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_awal,-2);
$tgl_akhir_mysql= '20'.$tahun.'-'.$bulan.'-'.substr('0'.$tgl_akhir,-2);
//end pemgaturan periode

//pengaturan pemilihan tabel pencarian
$tbl_pencarian	= ($bulan>=substr('0'.date("m"),-2))?"tbl_reservasi":"tbl_reservasi";
//end pengatran pemilihan tabel pencarian

//mengambil list id jurusan by kota
$sql	= "SELECT IdJurusan FROM view_list_jurusan_by_kota_".$kota_asal." ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
} 

$list_id_jurusan ="";

while ($row = $db->sql_fetchrow($result)){
	$list_id_jurusan .=$row[0].",";
}
$list_id_jurusan	= substr($list_id_jurusan,0,-1);
//END mengambil list id jurusan by kota

$kondisi_status	= $status==''?'%':$status;

$kondisi	= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND FlagBatal=1 AND IdJurusan IN($list_id_jurusan) AND CetakTiket LIKE '$kondisi_status' ";
	
$kondisi	.= ($cari=="")?"":" AND (Nama LIKE '%$cari%' OR Telp LIKE '%$cari' OR NoTiket LIKE '%$cari' OR KodeBooking LIKE '%$cari')";

if($cabang_asal=='0'){
	$kondisi	.= "";
}
else{
	
	if($cabang_tujuan=='0'){
		$kondisi	.= " AND (SELECT KodeCabangAsal FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tr.IdJurusan) LIKE '$cabang_asal' ";
	}
	else{
		if($kode_jadwal=='0'){
			$kondisi	.= " AND IdJurusan LIKE '$cabang_tujuan' ";
		}
		else{
			$kondisi	.= " AND KodeJadwal LIKE '$kode_jadwal' ";
		}
	}
}	

$sql = 
	"SELECT *
	FROM $tbl_pencarian tr
	$kondisi 
	ORDER BY TglBerangkat,JamBerangkat,KodeJadwal,NomorKursi";

$idx_check=0;


if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load jadwal',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

$i = $idx_page*$VIEW_PER_PAGE+1;

//mengambil data User
$sql_user = 
	"SELECT user_id,nama,nrp FROM tbl_user";

if ($result_user = $db->sql_query($sql_user)){
  while ($row_user = $db->sql_fetchrow($result_user)){
		$data_user[$row_user['user_id']]	= $row_user['nama'].' ('.$row_user['nrp'].')';
  }
} 
//end mengambil data User

//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Pembatalan Periode '.$tgl_awal.' s/d '.$tgl_akhir.' Bulan:'.$bulan.' Tahun: 20'.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'No. Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Tgl. Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jam');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Kode Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'No. Kursi');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Waktu Pesan');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Penjual');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Waktu Batal');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Pembatal');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Status');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

$idx=0;
$idx_row=4;

while ($row = $db->sql_fetchrow($result)){
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])));
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['NomorKursi']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, "'".$row['Telp']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $data_user[$row['PetugasPenjual']]);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, dateparsewithtime(FormatMySQLDateToTglWithTime($row['WaktuPembatalan'])));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $data_user[$row['PetugasPembatalan']]);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, ($row['CetakTiket']==0)?"Book":"Bayar");	
	
	$idx_row ++;
	$idx++;
}
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Pembatalan Periode '.$tgl_awal.' s/d '.$tgl_akhir.' Bulan:'.$bulan.' Tahun: 20'.$tahun.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
  
?>
