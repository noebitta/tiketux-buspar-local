<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 406;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################
include($adp_root_path . 'ClassMobil.php');
$Mobil	= new Mobil();

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kota_dipilih 	= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$cabang 					= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$spbu 					= isset($HTTP_GET_VARS['spbu'])? $HTTP_GET_VARS['spbu'] : $HTTP_POST_VARS['spbu'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$kota_dipilih	= ($kota_dipilih!='')?$kota_dipilih:"JAKARTA";
		
$kondisi_cari	=($cari=="")?
	"":
	" AND (f_sopir_get_nama_by_id(tbo.KodeSopir) LIKE '$cari%'
					OR f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) LIKE '%$cari%'
					OR tbo.NoSPJ LIKE '%$cari%'
					OR f_user_get_nama_by_userid(IdPetugas) LIKE '%$cari%'
					OR CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) LIKE  '%$cari%'
				)";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat":$sort_by;

$sum_total	= 0;

//QUERY
$kondisi_spbu	= $spbu==""?"":" AND NamaSPBU='$spbu'";
$kondisi_cabang	= $cabang==""?"":" AND (SELECT KodeCabangAsal FROM tbl_md_jurusan j WHERE j.IdJurusan=vb.IdJurusan)='$cabang'";
$group_cabang	= $cabang==""?" GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(vb.IdJurusan)":"";

$kondisi	=
	"(vb.TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	AND (NoBody LIKE '$cari'
		OR vb.KodeJadwal LIKE '$cari%'
		OR NamaSopir LIKE '%$cari%'
		OR NamaPetugas LIKE '%$cari%')
	$kondisi_spbu
	$kondisi_cabang";

$order	=($order=='')?"ASC":$order;

$sort_by =($sort_by=='')?"vb.TglBerangkat":$sort_by;

$sql	=
	"SELECT vb.*,
		f_kendaraan_ambil_nopol_by_kode(NoBody) as NoPol,
		f_jurusan_get_kode_cabang_asal_by_jurusan(vb.IdJurusan) as KodeCabang,
		JumlahPenumpang as Tiket, JumlahPaket as Paket,
		JumlahPenumpangCheck as TiketFisik, JumlahPaketCheck as PaketFisik,
		CatatanTambahanCheck as Catatan
	FROM tbl_voucher_bbm vb
	LEFT JOIN tbl_user ON vb.PetugasApprove = tbl_user.user_id
	LEFT JOIN tbl_spj ON vb.NoSPJ = tbl_spj.NoSPJ
	WHERE $kondisi
	ORDER BY vb.$sort_by $order";

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->mergeCells('A2:R2');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:R3');
	$objPHPExcel->getActiveSheet()->mergeCells('A6:A7');
	$objPHPExcel->getActiveSheet()->mergeCells('B6:B7');
	$objPHPExcel->getActiveSheet()->mergeCells('C6:C7');
	$objPHPExcel->getActiveSheet()->mergeCells('D6:D7');
	$objPHPExcel->getActiveSheet()->mergeCells('E6:E7');
	$objPHPExcel->getActiveSheet()->mergeCells('F6:F7');
	$objPHPExcel->getActiveSheet()->mergeCells('G6:G7');
	$objPHPExcel->getActiveSheet()->mergeCells('H6:H7');
	$objPHPExcel->getActiveSheet()->mergeCells('I6:I7');
	$objPHPExcel->getActiveSheet()->mergeCells('J6:J7');
	$objPHPExcel->getActiveSheet()->mergeCells('K6:L6');
	$objPHPExcel->getActiveSheet()->mergeCells('M6:N6');
	$objPHPExcel->getActiveSheet()->mergeCells('O6:O7');
	$objPHPExcel->getActiveSheet()->mergeCells('P6:R6');
	$objPHPExcel->getActiveSheet()->mergeCells('S6:S7');
	$objPHPExcel->getActiveSheet()->mergeCells('T6:T7');
	$objPHPExcel->getActiveSheet()->mergeCells('U6:U7');
	$objPHPExcel->getActiveSheet()->mergeCells('V6:V7');
	$objPHPExcel->getActiveSheet()->mergeCells('W6:W7');

//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN CHECKER SPBU TIKETUX');
	$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Counter : '.$cabang);
	$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Tanggal : '.date('d-m-Y'));
	$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
	$objPHPExcel->getActiveSheet()->setCellValue('B6', 'SPBU');
	$objPHPExcel->getActiveSheet()->setCellValue('C6', 'VOUCHER');
	$objPHPExcel->getActiveSheet()->setCellValue('D6', 'MANIFES');
	$objPHPExcel->getActiveSheet()->setCellValue('E6', 'COUNTER');
	$objPHPExcel->getActiveSheet()->setCellValue('F6', 'JADWAL');
	$objPHPExcel->getActiveSheet()->setCellValue('G6', 'JAM MASUK');
	$objPHPExcel->getActiveSheet()->setCellValue('H6', 'NOPOL');
	$objPHPExcel->getActiveSheet()->setCellValue('I6', 'NO BODY');
	$objPHPExcel->getActiveSheet()->setCellValue('J6', 'DRIVER');
	$objPHPExcel->getActiveSheet()->setCellValue('K6', 'PAX');
	$objPHPExcel->getActiveSheet()->setCellValue('K7', 'TIKET');
	$objPHPExcel->getActiveSheet()->setCellValue('L7', 'FISIK');
	$objPHPExcel->getActiveSheet()->setCellValue('M6', 'PAKET');
	$objPHPExcel->getActiveSheet()->setCellValue('M7', 'TIKET');
	$objPHPExcel->getActiveSheet()->setCellValue('N7', 'FISIK');
	$objPHPExcel->getActiveSheet()->setCellValue('O6', 'KETERANGAN');
	$objPHPExcel->getActiveSheet()->setCellValue('P6', 'BBM');
	$objPHPExcel->getActiveSheet()->setCellValue('P7', 'KM');
	$objPHPExcel->getActiveSheet()->setCellValue('Q7', 'LITER');
	$objPHPExcel->getActiveSheet()->setCellValue('R7', 'RUPIAH');
	$objPHPExcel->getActiveSheet()->setCellValue('S6', 'REALISASI');
	$objPHPExcel->getActiveSheet()->setCellValue('T6', 'SELISIH');
	$objPHPExcel->getActiveSheet()->setCellValue('U6', 'CHECKER');
	$objPHPExcel->getActiveSheet()->setCellValue('V6', 'TGL APPROVAL');
	/*$objPHPExcel->getActiveSheet()->setCellValue('W6', 'ACTION');*/

	for($col = 'A'; $col !== 'W'; $col++) {

		$objPHPExcel->getActiveSheet()
			->getDefaultColumnDimension($col)
			->setWidth('20');
	}

	if ($result = $db->sql_query($sql)){
		$i = $idx_page*$VIEW_PER_PAGE+1;
		$no =1 ;
		while ($row = $db->sql_fetchrow($result)){

			$i++;
			$idx_row=$i+6;

			if($no == 1){
				if($idx_row != null){
					$first=$idx_row;
				}
			}

			$no++;

			if($row['IsApprove'] == 1){
				$ket = 'SUDAH DIAPPROVE';
			}else{
				$ket = 'BELUM DIAPPROVE';
			}

			//AMBIL KENDARAAN
			$msql = "SELECT * FROM tbl_md_kendaraan WHERE KodeKendaraan='$row[NoBody]'";
			$res = $db->sql_fetchrow($db->sql_query($msql));

			//AMBIL JURUSAN
			$jsql = "SELECT * FROM tbl_md_jurusan WHERE IdJurusan='$row[IdJurusan]'";
			$hasil = $db->sql_fetchrow($db->sql_query($jsql));
			$Liter = $hasil['LiterBBM'];

			$bbm_decode		= $Mobil->layoutBodyDecode($Liter);
			$literbbm = $bbm_decode[$res['JumlahKursi']];


			$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, ($i-1));
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NamaSPBU']);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $idx_row, $row['KodeVoucher'], PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NoSPJ']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['KodeCabang']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['KodeJadwal']);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['TglDicatat']);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NoPol']);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['NoBody']);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $row['NamaSopir']);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $row['Tiket']);
			$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['TiketFisik']);
			$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $row['Paket']);
			$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['PaketFisik']);
			$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $ket);
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $row['Kilometer']);
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $literbbm);
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, $row['JumlahBiaya']);
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, $row['JumlahLiter']);
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$idx_row, number_format((float)($literbbm-$row['JumlahLiter']),2,'.',''));
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$idx_row, $row['NamaPetugas']);
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$idx_row, $row['WaktuApprove']);

			$sum_total	+=$row['Jumlah'];

		}
	}
	else{
		//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
		echo("Error:".__LINE__);exit;
	}

	$temp_idx=$idx_row;

	$idx_row++;

	$objPHPExcel->getActiveSheet()->setCellValue('A'.($idx_row+1), 'TOTAL');
	$objPHPExcel->getActiveSheet()->setCellValue('B'.($idx_row+1), '=COUNTA(B8:B'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('C'.($idx_row+1), '=COUNTA(C8:C'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('D'.($idx_row+1), '=COUNTA(D8:D'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('E'.($idx_row+1), '=COUNTA(E8:E'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('F'.($idx_row+1), '=COUNTA(F8:F'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+1), '=COUNTA(G8:G'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('H'.($idx_row+1), '=COUNTA(H8:H'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('I'.($idx_row+1), '=COUNTA(I8:I'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('J'.($idx_row+1), '=COUNTA(J8:J'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('K'.($idx_row+1), '=SUM(K8:K'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('L'.($idx_row+1), '=SUM(L8:L'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('M'.($idx_row+1), '=SUM(M8:M'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('N'.($idx_row+1), '=SUM(N8:N'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('O'.($idx_row+1), '=COUNTA(O8:O'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('P'.($idx_row+1), '=SUM(P8:P'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.($idx_row+1), '=SUM(Q8:Q'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('R'.($idx_row+1), '=SUM(R8:R'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('S'.($idx_row+1), '=SUM(S8:S'.$temp_idx.')');
	$objPHPExcel->getActiveSheet()->setCellValue('T'.($idx_row+1), '=SUM(T8:T'.$temp_idx.')');

	$objPHPExcel->getActiveSheet()->setCellValue('B'.($idx_row+9), 'SHIFT 1');
	$objPHPExcel->getActiveSheet()->setCellValue('C'.($idx_row+9), 'MIDDLE');
	$objPHPExcel->getActiveSheet()->setCellValue('D'.($idx_row+9), 'SHIFT 2');

	$sql	=
		"SELECT tbl_user.user_id,tbl_user.nama
	FROM tbl_voucher_bbm vb
	LEFT JOIN tbl_user ON vb.IdPetugas = tbl_user.user_id
	LEFT JOIN tbl_spj ON vb.NoSPJ = tbl_spj.NoSPJ
	WHERE $kondisi
	GROUP BY tbl_user.nama
	ORDER BY vb.$sort_by $order";

	if($result = $db->sql_query($sql)){
		$is = ($idx_row+10);
		$a = 0;
		while ($row = $db->sql_fetchrow($result)){
			$index = chr(64 + ($a+2));

			$nama = $row['nama'];
			$rumus = '=COUNTIF(U'.$first.':U'.$idx_row.',"'.$nama.'")';

			$objPHPExcel->getActiveSheet()->setCellValue($index.$is, $nama);
			$objPHPExcel->getActiveSheet()->setCellValue($index.($is+1), $rumus);
			$a+=2;
		}

	}else{
		echo("Error:".__LINE__);exit;
	}

	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);

	for($col = 'A'; $col !== 'W'; $col++) {
		$objPHPExcel->getActiveSheet()->getStyle($col.'6:'.$col.($idx_row-1))->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('A'.($idx_row+1).':'.'V'.($idx_row+1))->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('B'.($idx_row+9).':'.'D'.($idx_row+11))->applyFromArray($styleArray);
	}

	$style = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		),
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000'),
			'size'  => 12,
			'name'  => 'Arial'
		),
	);

	$objPHPExcel->getDefaultStyle()->applyFromArray($style);

	$gaya = array(
		'alignment' => array(
			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		),
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000'),
			'size'  => 12,
			'name'  => 'Arial'
		),
	);

	for($col = 'A'; $col !== 'W'; $col++) {
		$objPHPExcel->getActiveSheet()->getStyle($col.($first-2).':'.$col.($first-2))->applyFromArray($gaya);
	}

	$header = array(
		'font'  => array(
			'bold'  => true,
			'color' => array('rgb' => '000'),
			'size'  => 18,
			'name'  => 'Arial'
		),
	);

	$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($header);

	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(60);

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Laporan checker SPBU Kota : '.$kota_dipilih.' per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir.'.xls"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');

?>