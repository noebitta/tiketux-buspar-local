<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class PayoutCGS{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function PayoutCGS(){
		$this->ID_FILE="C-CGS";
	}

  function isDuplikasi($kode){

    //kamus
    global $db;

    $sql =
      "SELECT COUNT(1) AS jumlah_data FROM tbl_payout_cgs WHERE KodeReferensi='$kode'";

    if (!$result = $db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);
    }

    $row = $db->sql_fetchrow($result);

    return ($row[0]>0?true:false);

  }//  END isDuplikasi

  function generateKodeReferensi($siklus,$bulan,$tahun,$kode_sopir){

    //kamus
    global $db;

    $tgl_pay  = $siklus==0?"15":"30";

    $kode_referensi = "CGS-".str_replace(array("/","-",".","_","'","+","="),array("","","","","","",""),$kode_sopir).substr($tahun,0,2).substr("0".$bulan,-2).$tgl_pay.rand(01,99);

    $try=0;

    while($try<=100 && $this->isDuplikasi($kode_referensi)){
      $try++;
    }

    $kode_referensi .=($try<100)?"":"X";


    return $kode_referensi;

  }//  END generateKodeReferensi

  function payout($KodeSopir,$Siklus,$Bulan,$Tahun,$PayoutVia,$JumlahPayout,$TotalTrip,$Jurusan,$PayoutBy,$NamaPetugas){

    //kamus
    global $db;

    //MENAMBAHKAN DATA KEDALAM DATABASE

    $KodeReferensi = $this->generateKodeReferensi($Siklus,$Bulan,$Tahun,$KodeSopir);

    $sql ="INSERT INTO tbl_payout_cgs
          SET
            KodeReferensi='$KodeReferensi',KodeSopir='$KodeSopir',Siklus='$Siklus',
            Bulan='$Bulan',Tahun='$Tahun',PayoutVia='$PayoutVia',
            JumlahPayout='$JumlahPayout',TotalTrip='$TotalTrip',Jurusan='$Jurusan',
            WaktuPayout=NOW(),PayoutBy='$PayoutBy',NamaPetugas='$NamaPetugas'";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);
    }

    $sql  = "SELECT LAST_INSERT_ID()";

    if (!$result=$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    return $row[0];
  }

  function getListTahunCGS(){

    /*
    Desc	:Mengembalikan array list tahun transaksi yang ada
    */

    //kamus
    global $db;

    $sql =
      "SELECT YEAR(MIN(TglBerangkat)) AS TahunMin,YEAR(MAX(TglBerangkat)) AS TahunMax FROM tbl_spj WHERE TglBerangkat!='0000-00-00 00:00:00'";

    if (!$result = $db->sql_query($sql)){
      echo("Err:$this->ID_FILE ".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    $tahun_min  = ($row["TahunMin"])!=""?$row["TahunMin"]:date("Y");
    $tahun_max  = ($row["TahunMax"])!=""?$row["TahunMax"]:$tahun_min;

    $list_tahun = array();

    for($thn=$tahun_min;$thn<=$tahun_max;$thn++){
      $list_tahun[] = $thn;
    }

    return $list_tahun;

  }//  END getListTahun

  function getDataCGS($siklus,$bulan,$tahun,$order_by,$sort_by=0,$idx_page="",$cari=""){

    //kamus
    global $db, $VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("KodeSopir","Nama","TotalTrip","TotalCGS","Jurusan","NoRek","NamaBank","CabangBank");

    $sort     = ($sort_by==0)?"ASC":"DESC";

    $order		= ($order_by!=="")?" ORDER BY $koloms[$order_by] $sort":'';
    $set_limit= ($idx_page!=="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    if($siklus==0){
      $tgl_awal   = 26;
      $tgl_akhir  = 10;
      $bulan_awal = ($bulan>1?$bulan-1:12);
      $tahun_awal = $tahun-($bulan>1?0:1);
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }else{
      $tgl_awal   = 11;
      $tgl_akhir  = 25;
      $bulan_awal = $bulan;
      $tahun_awal = $tahun;
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }

    $set_kondisi  = "WHERE tms.FlagAktif=1".($cari==""?"":" AND (Nama LIKE '%$cari%' OR tms.KodeSopir LIKE '%$cari%' OR NoRek LIKE '%$cari' OR NamaBank LIKE '%$cari%' OR HP LIKE '%$cari')");

    $sql  =
      "SELECT SQL_CALC_FOUND_ROWS tms.KodeSopir,Nama,NamaBank,NoRek,CabangBank,
        IF(KodeReferensi IS NULL,COUNT(NoSPJ),tpc.TotalTrip) AS TotalTrip,IF(KodeReferensi IS NULL,SUM(BiayaSopir),tpc.JumlahPayout) AS TotalCGS,
        IF(KodeReferensi IS NULL,GROUP_CONCAT(KodeJurusan ORDER BY KodeJurusan SEPARATOR ','),tpc.Jurusan) AS Jurusan,KodeReferensi,
        PayoutVia,WaktuPayout,NamaPetugas,tpc.Id AS IdPayout
      FROM (tbl_md_sopir tms LEFT JOIN
      	(tbl_spj ts INNER JOIN tbl_md_jurusan tmj ON ts.IdJurusan=tmj.IdJurusan AND (TglBerangkat BETWEEN '$tahun_awal-$bulan_awal-$tgl_awal' AND '$tahun_akhir-$bulan_akhir-$tgl_akhir') AND IsEkspedisi=0)
      	ON ts.KodeDriver=tms.KodeSopir) LEFT JOIN tbl_payout_cgs tpc ON tms.KodeSopir=tpc.KodeSopir AND Siklus=$siklus AND tpc.Bulan=$bulan AND tpc.Tahun=$tahun
        $set_kondisi
      GROUP BY tms.KodeSopir
      $order
	    $set_limit";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);
    }

    return $result;

  }//  END getDataCGS

  function browseManifestCGS($siklus,$bulan,$tahun,$kode_sopir){

    //kamus
    global $db, $VIEW_PER_PAGE;

    if($siklus==0){
      $tgl_awal   = 26;
      $tgl_akhir  = 10;
      $bulan_awal = ($bulan>1?$bulan-1:12);
      $tahun_awal = $tahun-($bulan>1?0:1);
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }else{
      $tgl_awal   = 11;
      $tgl_akhir  = 25;
      $bulan_awal = $bulan;
      $tahun_awal = $tahun;
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }

    $sql =
      "SELECT TglBerangkat,JamBerangkat,KodeCabangAsal,KodeCabangTujuan,KodeJadwal,NoPolisi,TglSPJ
      FROM tbl_spj ts INNER JOIN tbl_md_jurusan tmj ON ts.IdJurusan=tmj.IdJurusan AND (TglBerangkat BETWEEN '$tahun_awal-$bulan_awal-$tgl_awal' AND '$tahun_akhir-$bulan_akhir-$tgl_akhir') AND IsEkspedisi=0
      WHERE KodeDriver='$kode_sopir'
	    ORDER BY TglBerangkat,JamBerangkat,KodeCabangAsal,KodeCabangTujuan;";

    if (!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);
    }

    return $result;

  }//  END browseManifestCGS

  function hitungTotalCGS($siklus,$bulan,$tahun,$kode_sopir){

    //kamus
    global $db;

    if($siklus==0){
      $tgl_awal   = 26;
      $tgl_akhir  = 10;
      $bulan_awal = ($bulan>1?$bulan-1:12);
      $tahun_awal = $tahun-($bulan>1?0:1);
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }else{
      $tgl_awal   = 11;
      $tgl_akhir  = 25;
      $bulan_awal = $bulan;
      $tahun_awal = $tahun;
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }

    $set_kondisi  = "WHERE tms.FlagAktif=1 AND tms.KodeSopir='$kode_sopir'";

    $sql  =
      "SELECT tpc.Id AS IdPayout,GROUP_CONCAT(KodeJurusan ORDER BY KodeJurusan SEPARATOR ',') AS Jurusan,COUNT(NoSPJ) AS TotalTrip,SUM(BiayaSopir) AS TotalCGS
      FROM (tbl_md_sopir tms LEFT JOIN
      	(tbl_spj ts INNER JOIN tbl_md_jurusan tmj ON ts.IdJurusan=tmj.IdJurusan AND (TglBerangkat BETWEEN '$tahun_awal-$bulan_awal-$tgl_awal' AND '$tahun_akhir-$bulan_akhir-$tgl_akhir') AND IsEkspedisi=0)
      	ON ts.KodeDriver=tms.KodeSopir) LEFT JOIN tbl_payout_cgs tpc ON tms.KodeSopir=tpc.KodeSopir AND Siklus=$siklus AND tpc.Bulan=$bulan AND tpc.Tahun=$tahun
        $set_kondisi";

    if (!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }//  END hitungTotalCGS

  function getDataPayoutCGS($id){

    //kamus
    global $db;

    $sql  =
      "SELECT tpc.*,tms.Nama AS NamaSopir
      FROM tbl_payout_cgs tpc INNER JOIN tbl_md_sopir tms ON tpc.KodeSopir=tms.KodeSopir
      WHERE Id='$id'";

    if (!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }//  END getDataPayoutCGS

  function summaryCGS($siklus,$bulan,$tahun){

    //kamus
    global $db;

    if($siklus==0){
      $tgl_awal   = 26;
      $tgl_akhir  = 10;
      $bulan_awal = ($bulan>1?$bulan-1:12);
      $tahun_awal = $tahun-($bulan>1?0:1);
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }else{
      $tgl_awal   = 11;
      $tgl_akhir  = 25;
      $bulan_awal = $bulan;
      $tahun_awal = $tahun;
      $bulan_akhir= $bulan;
      $tahun_akhir= $tahun;
    }

    $set_kondisi  = "WHERE tms.FlagAktif=1";

    $sql  =
      "SELECT COUNT(NoSPJ) AS TotalTrip,SUM(BiayaSopir) AS TotalCGS
      FROM tbl_md_sopir tms LEFT JOIN
      	(tbl_spj ts INNER JOIN tbl_md_jurusan tmj ON ts.IdJurusan=tmj.IdJurusan AND (TglBerangkat BETWEEN '$tahun_awal-$bulan_awal-$tgl_awal' AND '$tahun_akhir-$bulan_akhir-$tgl_akhir') AND IsEkspedisi=0)
      	ON ts.KodeDriver=tms.KodeSopir
        $set_kondisi";

    if (!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    $return["TotalTrip"]  = $row["TotalTrip"];
    $return["TotalCGS"]  = $row["TotalCGS"];

    $sql  =
      "SELECT SUM(JumlahPayout) AS TotalPayout
      FROM tbl_payout_cgs
      WHERE Siklus=$siklus AND Bulan=$bulan AND Tahun=$tahun";

    if (!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    $return["TotalPayout"]  = $row["TotalPayout"];

    return $return;

  }//  END summaryCGS
}
?>