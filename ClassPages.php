<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']!=$config['role_admin']){
  //redirect('index.'.$phpEx,true);
	exit;
}
//#############################################################################

class Pages{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
  VAR $TABEL;

	//CONSTRUCTOR
	function Pages(){
		$this->ID_FILE  ="C-PAGE";
    $this->TABEL    = "tbl_pages";
	}

	//BODY

	function periksaDuplikasi($PageId){

		//kamus
		global $db;

		$sql =
			"SELECT COUNT(1) FROM $this->TABEL WHERE PageId=$PageId";

		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}

    $row  = $db->sql_fetchrow($result);

		return $row[0]>0?true:false;

	}//  END periksaDuplikasi

	function tambah($PageId,$NamaPage,$NamaTampil,$GroupPage,$Urutan,$NamaFile,$ImageIcon,$IsHide){

		//kamus
		global $db;

    $set_field   = $GroupPage!=""?",GroupPage='$GroupPage'":"";
    $set_field  .= $ImageIcon!=""?",ImageIcon='$ImageIcon'":"";

		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
      "INSERT INTO $this->TABEL SET
        PageId='$PageId',NamaPage='$NamaPage',NamaTampil='$NamaTampil',
        Urutan='$Urutan',NamaFile='$NamaFile',IsHide='$IsHide' $set_field;";

		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}

		return true;
	}

  function ubah($PageIdOld,$PageId,$NamaPage,$NamaTampil,$GroupPage,$Urutan,$NamaFile,$ImageIcon,$IsHide){

    //kamus
    global $db;

    $set_id_baru  = $PageIdOld==$PageId?"":",PageId=$PageId";

    $set_field   = $GroupPage!=""?",GroupPage='$GroupPage'":"";
    $set_field  .= $ImageIcon!=""?",ImageIcon='$ImageIcon'":"";

    $sql =
      "UPDATE $this->TABEL SET
        NamaPage='$NamaPage',NamaTampil='$NamaTampil',
        Urutan='$Urutan',NamaFile='$NamaFile',IsHide='$IsHide' $set_field
        $set_id_baru
      WHERE PageId='$PageIdOld'";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
    }

    if($set_id_baru!=""){
      $sql =
        "UPDATE tbl_permissions SET
        PageId='$PageId'
      WHERE PageId='$PageIdOld'";

      if (!$db->sql_query($sql)){
        echo("Err:$this->ID_FILE".__LINE__);exit;
      }
    }

    return true;
  }

  function hapus($PageId){

    //kamus
    global $db;

    //MENGHAPUS DATA KEDALAM DATABASE
    $sql =
      "DELETE FROM $this->TABEL
			WHERE PageId IN($PageId);";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
    }

    //MENGHAPUS DATA DARI TABEL PERMISSION
    $sql =
      "DELETE FROM tbl_permissions
			WHERE PageId IN($PageId);";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
    }

    return true;
  }//end hapus

  function ambilData($order_by,$sort_by=0,$idx_page="",$cari=""){

    //kamus
    global $db, $VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("PageId","NamaPage","NamaTampil","GroupPage","Urutan","NamaFile","ImageIcon","IsHide");

    $sort     = ($sort_by==0)?"ASC":"DESC";

    $order		= ($order_by!=="" && $order_by!==null)?" ORDER BY $koloms[$order_by] $sort":'';
    $set_limit= ($idx_page!=="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1".($cari==""?"":" AND (PageId LIKE '%$cari%' OR NamaPage LIKE '%$cari%' OR NamaTampil LIKE '%$cari' OR GroupPage LIKE '%$cari%' OR NamaFile LIKE '%$cari')");

    $sql  =
      "SELECT SQL_CALC_FOUND_ROWS PageId,NamaPage,NamaTampil,Urutan,NamaFile,ImageIcon,IsHide,GroupPage
      FROM $this->TABEL
      $set_kondisi
      $order
	    $set_limit";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);exit;
    }

    return $result;

  }//  END ambilData


	function ambilDataDetail($PageId){

		//kamus
		global $db;

		$sql =
			"SELECT *
			FROM $this->TABEL
			WHERE PageId='$PageId'";

		if (!$result = $db->sql_query($sql,TRUE)){
			echo("Err:$this->ID_FILE ".__LINE__);exit;
		}

    $row=$db->sql_fetchrow($result);

    return $row;

	}//  END ambilData

  function hideUnhide($PageId){

    //kamus
    global $db;

    $sql =
      "UPDATE $this->TABEL SET
        IsHide=1-IsHide
      WHERE PageId='$PageId'";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

}
?>