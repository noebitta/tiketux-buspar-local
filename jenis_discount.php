<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 2014;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 	= $config['perpage'];
$mode 		= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // $act    	= $HTTP_GET_VARS['act'];
$submode	= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   	= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$pesan    	= $HTTP_GET_VARS['pesan'];

//$mode 	= ($mode != '') ? $mode : 'set_awal';
//$pesan	= ($pesan =='') ? "<td colspan='9'></td>":"<td colspan='9' bgcolor='yellow'><h2>".$pesan."</h2></td>";

switch($mode){

//mengatur tampilan awal pada halaman laporan rute
default:

	if($HTTP_POST_VARS["txt_cari"]!=""){
		$cari=$HTTP_POST_VARS["txt_cari"];
	}
	else{
		$cari=$HTTP_GET_VARS["cari"];
	}

	$kondisi	=($cari=="")?"":
		" WHERE KodePromo LIKE '%$cari%'
				OR NamaPromo LIKE '%$cari%'";

	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging=pagingData($idx_page,"KodePromo","tbl_md_promo","",$kondisi,"pengaturan_promo.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================

	$sql = "SELECT *
					FROM	tbl_jenis_discount
					ORDER BY FlagLuarKota DESC,NamaDiscount ASC ";
	$idx_check=0;

	if ($result = $db->sql_query($sql)){
		$i = $idx_page*$VIEW_PER_PAGE+1;
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';

			if (($i % 2)==0){
				$odd = 'even';
			}

			if($row['FlagAktif']){
				$status="<a href='javascript:;' onClick='return ubahStatus(\"$row[IdDiscount]\")'>Aktif</a>";
			}
			else{
				$odd	= "red";
				$status="<a href='javascript:;' onClick='return ubahStatus(\"$row[IdDiscount]\")'>Tidak Aktif</a>";
			}

			$idx_check++;

			$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";

			$act 	="<a href='".append_sid('jenis_discount.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
			$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
			$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'check'=>$check,
					'no'=>$i,
					'nama_diskon'=>$row['NamaDiscount'],
					'kode_diskon'=>$row['KodeDiscount'],
					'jenis'=>($row['FlagLuarKota']==0?"Dalam Kota":"Luar Kota"),
					'besar_diskon'=>$row['JumlahDiscount']>1?number_format($row['JumlahDiscount'],0,",","."):($row['JumlahDiscount']*100)."%",
					'aktif'	=>$status,
					'action'=>$act
				)
			);

			$i++;

		}

	}else{
		die_error('Err:',__LINE__);
		//die_error('GAGAL mengambil data');
	}
		
	$template->set_filenames(array('body' => 'jenis_discount.tpl'));

	$template->assign_vars(array(
			'BCRUMP'    		=>setBcrump($id_page),
			'U_ADD'					=> append_sid('jenis_discount.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('jenis_discount.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
		)
	);
	include($adp_root_path . 'includes/page_header.php');
	$template->pparse('body');
	include($adp_root_path . 'includes/page_tail.php');
	
exit;

//TAMBAH DATA
	case 'add':
		// add
		if($userdata['user_level'] != 0){
			$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '101'
						AND user_level='$userdata[user_level]'";

			$result= $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
			}
		}

		$pesan = $HTTP_GET_VARS['pesan'];

		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Ditambah!</font>";
			$bgcolor_pesan="98e46f";
		}

		$template->set_filenames(array('body' => 'discount/add_body.tpl'));

		$template->assign_vars(array(
				'BCRUMP'		=>setBcrump($id_page),
				'JUDUL'		=>'Tambah Data Discount',
				'MODE'   	=> 'save',
				'SUB'    	=> '0',
				'PESAN'						=> $pesan,
				'BGCOLOR_PESAN'		=> $bgcolor_pesan,
				'U_ADD_ACT'	=> append_sid('jenis_discount.'.$phpEx)
			)
		);
		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
		exit;

	case 'save':

		$kode_diskon  		= str_replace(" ","",$HTTP_POST_VARS['kode_diskon']);
		$kode_diskon_old	= str_replace(" ","",$HTTP_POST_VARS['kode_diskon_old']);
		$nama_diskon   		= $HTTP_POST_VARS['nama_diskon'];
		$flag_discount		= $HTTP_POST_VARS['flagdiscount'];
		$discount			    = $HTTP_POST_VARS['discount'];
		$status_aktif 		= $HTTP_POST_VARS['aktif'];

		$terjadi_error=false;
		if($submode == 0){
			if($userdata['user_level'] != 0) {
				$sql = "SELECT page_id,user_level AS access
							FROM tbl_permissions
							WHERE page_id = '101'
							AND user_level='$userdata[user_level]'";
				$result = $db->sql_fetchrow($db->sql_query($sql));

				if($result == false){
					die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
				}
			}

			$judul    = "Tambah Data Discount";

			//MENAMBAHKAN DATA KEDALAM DATABASE
			$sql =
				"INSERT INTO tbl_jenis_discount
				(KodeDiscount, NamaDiscount,
				JumlahDiscount,FlagLuarKota, FlagAktif,IsHargaTetap)
			VALUES(
				'$kode_diskon', '$nama_diskon',
				'$discount',1,'$status_aktif','$flag_discount')";

			if (!$db->sql_query($sql)){
				die_error("Gagal");
				exit;
			}

			$pesan="<font color='green' size=3>Data Berhasil Ditambah!</font>";
			$bgcolor_pesan="98e46f";
		}else{
			if($userdata['user_level'] != 0) {
				$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '102'
						AND user_level='$userdata[user_level]'";
				$result = $db->sql_fetchrow($db->sql_query($sql));

				if($result == false){
					die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
				}
			}

			$judul="Ubah Data Discount";

			$sql =
				"UPDATE tbl_jenis_discount SET
				KodeDiscount='$kode_diskon', NamaDiscount='$nama_diskon',
				JumlahDiscount='$discount',
				IsHargaTetap='$flag_discount',
				FlagAktif='$status_aktif'
			WHERE KodeDiscount='$kode_diskon_old'";

			if (!$db->sql_query($sql)){
				die_error("Gagal");
				exit;
			}
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}

		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";

		$template->set_filenames(array('body' => 'discount/add_body.tpl'));

		$template->assign_vars(array(
				'BCRUMP'		                  =>setBcrump($id_page),
				'JUDUL'		                    =>$judul,
				'MODE'   	                    => 'save',
				'SUB'    	                    => $submode,
				'KODE_DISKON_OLD'             => $kode_diskon_old,
				'KODE_DISKON'                 => $kode_diskon,
				'NAMA_DISKON'                 => $nama_diskon,
				'DISCOUNT'			              => $discount,
				'FLAGDISCOUNT'.$flag_discount =>"selected",
				'AKTIF_1'			                => $status_aktif_1,
				'AKTIF_0'			                => $status_aktif_0,
				'PESAN'				                => $pesan,
				'BGCOLOR_PESAN'                => $bgcolor_pesan,
				'U_ADD_ACT'		                =>append_sid('jenis_discount.'.$phpEx)
			)
		);

		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
		exit;

	case 'edit':
		if($userdata['user_level'] != 0) {
			$sql = "SELECT page_id,user_level AS access
							FROM tbl_permissions
							WHERE page_id = '102'
							AND user_level='$userdata[user_level]'";
			$result = $db->sql_fetchrow($db->sql_query($sql));

			if($result == false){
				die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
			}
		}

		$id = $HTTP_GET_VARS['id'];
		$sql =
			"SELECT *
			FROM tbl_jenis_discount
			WHERE IdDiscount='$id';";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		}
		else{
			die_error("Gagal".__LINE__);
		}
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";

		$template->set_filenames(array('body' => 'discount/add_body.tpl'));

		$template->assign_vars(array(
				'BCRUMP'		                          =>setBcrump($id_page),
				'JUDUL'		                            => 'Ubah Data Discount',
				'MODE'   	                            => 'save',
				'SUB'    	                            => '1',
				'KODE_DISKON_OLD'                     => $row['KodeDiscount'],
				'KODE_DISKON'                         => $row['KodeDiscount'],
				'NAMA_DISKON'                         => $row['NamaDiscount'],
				'FLAGDISCOUNT'.$row["IsHargaTetap"]   => "selected",
				'DISCOUNT'			                      => $row['JumlahDiscount'],
				'AKTIF_1'			                        => $status_aktif_1,
				'AKTIF_0'			                        => $status_aktif_0,
				'PESAN'				                        => $pesan,
				'U_ADD_ACT'                           => append_sid('jenis_discount.'.$phpEx)
			)
		);
		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
		exit;

//UBAH STATUS==========================================================================================================
case 'ubah_status':
	if($userdata['user_level'] != 0) {
		$sql = "SELECT page_id,user_level AS access
						FROM tbl_permissions
						WHERE page_id = '102'
						AND user_level='$userdata[user_level]'";
		$result = $db->sql_fetchrow($db->sql_query($sql));

		if($result == false){
			echo 2;
			exit;
		}
	}

	$id_discount 			= str_replace("\'","'",$HTTP_GET_VARS['id_discount']);
	
	$sql ="CALL sp_jenis_discount_ubah_status_aktif('$id_discount')";
	echo 'aa';

	if (!$db->sql_query($sql)){
		return false;
		die_error("Err: $this->ID_FILE".__LINE__);
	}
	return true;
	
exit;

//HAPUS ACCOUNT==========================================================================================================
case 'hapus':
	if($userdata['user_level'] != 0) {
		$sql = "SELECT page_id,user_level AS access
							FROM tbl_permissions
							WHERE page_id = '103'
							AND user_level='$userdata[user_level]'";
		$result = $db->sql_fetchrow($db->sql_query($sql));

		if($result == false){
			echo(0);
			exit;
		}
	}

	$list_promo = str_replace("\'","'",$HTTP_GET_VARS['list_discount']);

	$sql =
		"DELETE FROM tbl_jenis_discount
			WHERE IdDiscount IN($list_promo);";

	if (!$db->sql_query($sql)){
		echo 3;
		die_error("ERR:".__LINE__.$sql);
	}

	
	echo(1);
	
exit;
}//switch mode


?>
