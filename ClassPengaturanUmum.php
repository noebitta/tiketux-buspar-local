<?php

class PengaturanUmum{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Pengumuman(){
		$this->ID_FILE="C-PU";
	}
	
	//BODY
	
	function periksaDuplikasi($kode){
		
		/*
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT f_pengumuman_periksa_duplikasi('$kode') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function ubahPesanDiTiket($pesan){		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_pu_ubah_pesan_tiket('$pesan')";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubahPengaturanUmum(
		$pesan_di_tiket,$alamat,$telp,$email,
		$website,$tgl_mulai_tuslah,$tgl_akhir_tuslah,
		$harga_paket_pertama_p,$harga_paket_selanjutnya_p,
		$harga_paket_pertama_ga,$harga_paket_selanjutnya_ga,
		$harga_paket_pertama_gd,$harga_paket_selanjutnya_gd,
		$harga_paket_pertama_s,$harga_paket_selanjutnya_s){		
		//kamus
		global $db;
		
		//MENGUBAH DATA KEDALAM DATABASE
		$sql = 
			"UPDATE tbl_pengaturan_umum SET 
				PesanDiTiket='$pesan_di_tiket',AlamatPerusahaan='$alamat',TelpPerusahaan='$telp',
				EmailPerusahaan='$email',WebSitePerusahaan='$website',
				TglMulaiTuslah='$tgl_mulai_tuslah',TglAkhirTuslah='$tgl_akhir_tuslah'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		
		//Mengubah Harga Paket Platinum
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_p',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_p'
			WHERE KodeLayanan='P'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE ".__LINE__);
		}
		
		//Mengubah Harga Paket Gold Antar
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_ga',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_ga'
			WHERE KodeLayanan='GA'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//Mengubah Harga Paket Gold Diambil
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_gd',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_gd'
			WHERE KodeLayanan='GD'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		//Mengubah Harga Paket silver
		$sql = 
			"UPDATE tbl_md_paket_daftar_layanan SET 
				HargaKiloPertama='$harga_paket_pertama_s',
				HargaKiloBerikutnya='$harga_paket_selanjutnya_s'
			WHERE KodeLayanan='S'";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData(){
		
		/*
		Desc	:Mengembalikan data pengaturan umum
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_pengaturan_umum
			ORDER BY IdPengaturanUmum DESC LIMIT 0,1;";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilHargaPaketMinimum(){
		
		/*
		Desc	:Mengembalikan harga minimum paket
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT HargaPaketMinimum
			FROM tbl_pengaturan_umum
			ORDER BY IdPengaturanUmum DESC LIMIT 0,1;";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilHargaPaketMinimum
	
	function ambilFeeTransaksi(){
		
		/*
		Desc	:Mengembalikan fee transaksi
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT FeeTiket,FeePaket,FeeSMS
			FROM tbl_pengaturan_umum
			ORDER BY IdPengaturanUmum DESC LIMIT 0,1;";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//ambilFeeTransaksi
	
	function ambilDataPerusahaan(){
		
		/*
		Desc	:Mengembalikan data perusahaan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT NamaPerusahaan,AlamatPerusahaan,TelpPerusahaan,EmailPerusahaan,WebSitePerusahaan
			FROM tbl_pengaturan_umum LIMIT 0,1";
				
		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);
			
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}

	}//  END ambilDataPerusahaan
	
	function ambilPesanUntukDiTiket(){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT PesanDiTiket
			FROM tbl_pengaturan_umum LIMIT 0,1";
				
		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);
			
			$pesan	=$row[0];
			
			return $pesan;
		} 
		else{
			//die_error("Err: $this->ID_FILE".__LINE__);
			return "-E-";
		}

	}//  END ambilPesanUntukDiTiket
	
	function ambilStatusBlokir(){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT FlagBlokir
			FROM tbl_pengaturan_umum LIMIT 0,1";
				
		if ($result = $db->sql_query($sql)){
			$row = $db->sql_fetchrow($result);
			
			$status_blokir	=$row[0];
			
			return $status_blokir;
		} 
		else{
			//die_error("Err: $this->ID_FILE".__LINE__);
			return "-E-";
		}

	}//  END ambilStatusBlokir
	
	function ubahStatusBlokir($status_blokir,$pelaku){
		$this->cekSession();
		
		//kamus
		global $db;
		global $LEVEL_ADMIN;
		
		$sql = 
			"UPDATE tbl_pengaturan_umum
			SET FlagBlokir='$status_blokir', PemblokirUnblokir='$pelaku', WaktuBlokirUnblokir=now()";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		if($status_blokir=='1'){
			$sqla = 
				"UPDATE tbl_user SET 
					status_online =0
				WHERE user_level>$LEVEL_ADMIN;";
			
			if (!$resulta = $db->sql_query($sqla)){
				die_error('Err: $this->ID_FILE'.__LINE__);//,__FILE__,__LINE__,$sqla);
			}
			
			$sqla = 
				"DELETE FROM tbl_sessions 
				WHERE session_user_id!=$pelaku";
			
			if (!$resulta = $db->sql_query($sqla)){
				die_error("Err: $this->ID_FILE".__LINE__);//,__FILE__,__LINE__,$sqla);
			}
		}
		
		return true;
		
	}//  END ubahStatusBlokir
	
	function cekSession(){
		global $userdata;
		global $token;
		if(!$userdata['session_logged_in'] && $token!=md5('hanyauntukmuindonesiaku')){ 
			redirect('index.php',true); 
		}
	}
	
	function ambilTglTuslah(){
		
		/*
		Desc	:Mengembalikan tanggal tuslah
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_pengaturan_parameter
			WHERE NamaParameter LIKE 'TGL_%';";
				
		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$data_return	= array();
		
		while ($row=$db->sql_fetchrow($result)){
			$field_name	= $row['NamaParameter'];
			$data_return[$field_name]	= $row['NilaiParameter'];
		}
		
		return $data_return;
		
	}//  END ambilData

  function ambilParameter($nama_parameter){
    //kamus
    global $db;

    $sql =
      "SELECT *
      FROM tbl_pengaturan_parameter
      WHERE NamaParameter LIKE '$nama_parameter'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

   return $result;
  }//  END ambilParameter

	function ambilParameterBatch($kondisi){

		/*
        Desc    :Mengembalikan tanggal tuslah
        */

		//kamus
		global $db;

		$sql =
			"SELECT * FROM tbl_pengaturan_parameter
      WHERE NamaParameter LIKE '$kondisi';";

		if(!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		return $result;

	}//  END ambilParameter

  function ambilDataPerusahaanKeteranganTiket(){
    global $db;

    $sql =
      "SELECT NamaParameter,NilaiParameter FROM tbl_pengaturan_parameter
      WHERE NamaParameter LIKE 'PERUSAHAAN_%' OR NamaParameter='TIKET_CATATAN'";

    if(!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    while($row=$db->sql_fetchrow($result)){
      $data_return[$row["NamaParameter"]]=$row["NilaiParameter"];
    }

    return $data_return;
  }

}
?>