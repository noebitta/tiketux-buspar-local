<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 401;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$username				= $userdata['username'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

// LIST
$template->set_filenames(array('body' => 'laporan_keuangan_cso/laporan_keuangan_cso_body.tpl'));

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}


$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"":
	" WHERE (username='$cari'
	  OR nama LIKE '%$cari%' 
		OR telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"nama":$sort_by;
			
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"user_id","tbl_user","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_keuangan_cso.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================
	
$sql=
	"SELECT 
	  user_id,nama,username,nrp,telp,hp,KodeCabang,f_cabang_get_name_by_kode(KodeCabang) AS cabang
	FROM 
	  tbl_user
	$kondisi_cari";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		PetugasCetakTiket,
		IS_NULL(SUM(IF(JenisPembayaran=0,SubTotal,0)),0) AS TotalTiketTunai, 
		IS_NULL(SUM(IF(JenisPembayaran=5,SubTotal,0)),0) AS TotalTiketEDC,
		IS_NULL(SUM(SubTotal),0) AS TotalUangTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount, 
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket 
	FROM tbl_reservasi
	WHERE (DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
		AND (FlagPesanan IS NULL OR FlagPesanan=0) 
	GROUP BY PetugasCetakTiket ORDER BY PetugasCetakTiket";

if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_total[$row['PetugasCetakTiket']]	= $row;
}


//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		PetugasPenjual,
		IS_NULL(SUM(IF(JenisPembayaran=0,HargaPaket,0)),0) AS TotalPaketTunai,
		IS_NULL(SUM(IF(JenisPembayaran=5,HargaPaket,0)),0) AS TotalPaketEDC,
		IS_NULL(SUM(HargaPaket),0) AS TotalUangPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(WaktuPesan) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND CetakTiket=1 AND FlagBatal!=1
	GROUP BY PetugasPenjual ORDER BY PetugasPenjual";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['PetugasPenjual']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){
	$temp_array[$idx]['user_id']					= $row['user_id'];
	$temp_array[$idx]['nama']							= $row['nama'];
	$temp_array[$idx]['username']					= $row['username'];
	$temp_array[$idx]['nrp']							= $row['nrp'];
	$temp_array[$idx]['telp']							= $row['telp'];
	$temp_array[$idx]['telp']							= $row['telp'];
	$temp_array[$idx]['hp']								= $row['hp'];
	$temp_array[$idx]['KodeCabang']				= $row['KodeCabang'];
	$temp_array[$idx]['cabang']						= $row['cabang'];
	$temp_array[$idx]['TotalTiketTunai']	= $data_total[$row['user_id']]['TotalTiketTunai'];
	$temp_array[$idx]['TotalTiketEDC']	= $data_total[$row['user_id']]['TotalTiketEDC'];
	$temp_array[$idx]['TotalUangTiket']		= $data_total[$row['user_id']]['TotalUangTiket'];
	$temp_array[$idx]['TotalDiscount']		= $data_total[$row['user_id']]['TotalDiscount'];
	$temp_array[$idx]['TotalTiket']				= $data_total[$row['user_id']]['TotalTiket'];
	$temp_array[$idx]['TotalPaketTunai']	= $data_paket_total[$row['user_id']]['TotalPaketTunai'];
	$temp_array[$idx]['TotalPaketEDC']	= $data_paket_total[$row['user_id']]['TotalPaketEDC'];
	$temp_array[$idx]['TotalUangPaket']		= $data_paket_total[$row['user_id']]['TotalUangPaket'];
	$temp_array[$idx]['TotalPaket']				= $data_paket_total[$row['user_id']]['TotalPaket'];
	$temp_array[$idx]['Total']						= $temp_array[$idx]['TotalUangTiket'] + $temp_array[$idx]['TotalUangPaket'] - $temp_array[$idx]['TotalDiscount'];
	
	$idx++;
}

if($order=='ASC'){
	//PHP Versi 5
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	
	//PHP Versi 4
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//PHP Versi 5
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	
	//PHP Versi 4
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

//PLOT DATA
$idx=$idx_awal_record;

while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_rekap_tiket_detail.'.$phpEx.'?p0='.$tanggal_mulai.'&p1='.$tanggal_akhir.'&p2='.$temp_array[$idx]['user_id'].'&p3=3')."\");return false'>Detail<a/>";
	
	//total tiket
	$total_tunai			= $temp_array[$idx]['TotalTiketTunai'];
	$total_edc			= $temp_array[$idx]['TotalTiketEDC'];
	$total_uang_tiket	= $temp_array[$idx]['TotalUangTiket'];
	$total_discount		= $temp_array[$idx]['TotalDiscount'];
	$total_tiket			= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_paket_tunai	= $temp_array[$idx]['TotalPaketTunai'];
	$total_paket_edc	= $temp_array[$idx]['TotalPaketEDC'];
	$total_uang_paket		= $temp_array[$idx]['TotalUangPaket'];
	$total_paket				= $temp_array[$idx]['TotalPaket'];
	
	//total
	$total							= $temp_array[$idx]['Total'];
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'nama'=>$temp_array[$idx]['nama'],
				'nrp'=>$temp_array[$idx]['nrp'],
				'cabang'=>$temp_array[$idx]['cabang']." (".$temp_array[$idx]['kode_cabang'].")",
				'tiket'=>number_format($total_tiket,0,",","."),
				'total_tunai'=>number_format($total_tunai,0,",","."),
				'total_edc'=>number_format($total_edc,0,",","."),
				'total_uang_tiket'=>number_format($total_uang_tiket,0,",","."),
				'paket'=>number_format($total_paket,0,",","."),
				'total_tunai_paket'=>number_format($total_paket_tunai,0,",","."),
				'total_edc_paket'=>number_format($total_paket_edc,0,",","."),
				'total_uang_paket'=>number_format($total_uang_paket,0,",","."),
				'discount'=>number_format($total_discount,0,",","."),
				'total'=>number_format($total,0,",","."),
				'act'=>$act
			)
		);
	
	$idx++;	
} 

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$cari."&p4=".$sort_by."&p5=".$order."";
	
$script_cetak_pdf="Start('laporan_keuangan_cso_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_keuangan_cso_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT


//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
	
$page_title	= "Keuangan CSO";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_keuangan_cso.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=nama'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Nama ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=nrp'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan NRP ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=cabang'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Cabang ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=TotalTiket'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan Jumlah jual tiket ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=TotalUangTiket'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Jumlah uang tiket ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=TotalPaket'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Jumlah paket ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=TotalUangPaket'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Jumlah uang paket ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=TotalDiscount'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Discount ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_keuangan_cso.'.$phpEx.'?sort_by=Total'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan Total ($order_invert)"

	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>