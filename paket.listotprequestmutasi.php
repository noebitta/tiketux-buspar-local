<?php
// HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 226;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassUser.php');

// PARAMETER
$mode 			  = getVariabel('mode');
$periode_awal = getVariabel('filtglawal');
$periode_akhir= getVariabel('filtglakhir');
$cari         = getVariabel('cari');
$idx_page     = getVariabel('idxpage');
$order_by     = getVariabel('orderby');
$sort         = getVariabel('sort');

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;
$order_by   = $order_by==""?1:$order_by;

$Paket      = new Paket();

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    break;

  case 1:
    //GET KODE OTP

    $no_resi  = getVariabel('noresi');

    getKodeOTP($no_resi);

    break;

}

//METHODES & PROCESS ========================================================================================================================================================================

function showData(){

  global $db,$periode_awal,$periode_akhir,$cari,$order_by,$sort,$idx_page,$template,$Paket,$VIEW_PER_PAGE;

  $template->set_filenames(array('body' => 'paket.listotprequestmutasi/index.tpl'));

  $periode_awal   = $periode_awal==""?date("d-m-Y"):$periode_awal;
  $periode_akhir  = $periode_akhir==""?date("d-m-Y"):$periode_akhir;

  $filter         = "(DATE(WaktuRequest) BETWEEN '".FormatTglToMySQLDate($periode_awal)."' AND '".FormatTglToMySQLDate($periode_akhir)."')";

  $result = $Paket->ambilDataRequestOTPMutasi($order_by,$sort,$idx_page,$cari,$filter);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd =($no%2)==0?"even":"odd";

    if($row["OTP"]=="") {
      $otp             = "";
      $waktu_otorisasi = "";
    }
    else{
      $otp            = $row["OTP"];
      $waktu_otorisasi= dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuApproved"]));
    }

    $template->assign_block_vars(
      'ROW',array(
        'odd'                 => $odd,
        'idx'                 => $no,
        'no'                  => $idx_page*$VIEW_PER_PAGE+$no,
        'noresi'              => $row["NoResi"],
        'tblberangkat'        => dateparse(FormatMySQLDateToTgl($row["TglBerangkat"])),
        'kodejadwal'          => $row["KodeJadwal"],
        'jamberangkat'        => $row["JamBerangkat"],
        'namapengirim'        => $row["NamaPengirim"],
        'namapenerima'        => $row["NamaPenerima"],
        'keterangan'          => $row["Keterangan"],
        'csorequester'        => $row["NamaPetugasRequest"],
        'wakturequest'        => dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuRequest"])),
        'alasan'              => $row["Alasan"],
        'otp'                 => $otp,
        'otorisasioleh'       => $row["NamaPetugasApproved"],
        'waktuotorisasi'      => $waktu_otorisasi
      )
    );

    //ACTION
    if($otp=="") {
      $template->assign_block_vars("ROW.ACT_GETOTP", array());
    }

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	=> basename(__FILE__),
      "TGL_AWAL"  => $periode_awal,
      "TGL_AKHIR" => $periode_akhir,
      'CARI'      => $cari,
      'ORDER'     => $order_by,
      'SORT'      => $sort,
      'IDX_PAGE'  => $idx_page,
      'PAGING'    => $paging
    )
  );

  $template->pparse('body');

}

function getKodeOTP($no_resi){
  global $userdata,$Paket;

  $otp  = $Paket->setOTPMutasi($no_resi);

  $ret_val["status"]      = "OK";
  $ret_val["otp"]         = $otp;
  $ret_val["approver"]    = $userdata["nama"];
  $ret_val["waktuapprove"]= dateparseWithTime(date("d-m-Y H:i:s"));

  echo(json_encode($ret_val));
}

?>