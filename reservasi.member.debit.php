<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassSMSGateway.php');
include($adp_root_path . 'ClassVoucher.php');
include($adp_root_path . 'ClassReservasi.php');
// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>");
}
//#############################################################################
// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$order			= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$Member	= new Member();
$Voucher	= new Voucher();
$Reservasi  	= new Reservasi();
$mode	= $mode==""?"show":$mode;

$kode_booking = isset($HTTP_GET_VARS['kodebooking'])? $HTTP_GET_VARS['kodebooking'] : $HTTP_POST_VARS['kodebooking'];
$jenis_discount	= $HTTP_GET_VARS['jenis_discount'];

switch($mode){
	case "show":

		//get id member dari ponta
		$sql = "SELECT IDPONTA FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";
		if(!$result = $db->sql_query($sql)){
			die("Error : ".__LINE__);
		}
		$row = $db->sql_fetchrow($result);
		$ponta = $row['IDPONTA'];

		$data_member		= $Member->ambilDataByKondisi("NoSeriKartu ='$ponta'");
		if($data_member['IdMember']==""){
			$template->assign_block_vars('PESAN',array('ISI'=>'---[ MEMBER TIDAK DITEMUKAN ]---'));
		}else{

		    // jika ada discount yg dipilih
            if($jenis_discount != ""){

                $sql = "SELECT HargaTiket, IdJurusan, NoTiket, KodeJadwal FROM tbl_reservasi WHERE KodeBooking = '$kode_booking' LIMIT 1";
                if(!$result = $db->sql_query($sql)){
                    die("Error : ".__LINE__);
                }
                $row = $db->sql_fetchrow($result);
                $harga_tiket = $row[0];
                $id_jurusan  = $row[1];
                $no_tiket    = $row[2];
                $kode_jadwal = $row[3];

                $data_discount	= $Reservasi->ambilDiscount($jenis_discount);
                $jenis_penumpang= $data_discount['KodeDiscount'];
                $jenis_discount	= $data_discount['NamaDiscount'];
                $nama_discount 	= "(".$data_discount['NamaDiscount'].")";
                $is_harga_tetap = $data_discount['IsHargaTetap'];
                $is_return		= $data_discount['IsReturn'];
                $listJurusan	= $data_discount['ListJurusan'];
                $id_discount    = $data_discount['IdDiscount'];
                if($is_harga_tetap!=1) {
                    if ($data_discount['JumlahDiscount'] > $harga_tiket) {
                        $besar_discount = $harga_tiket;
                    } else if ($data_discount['JumlahDiscount'] > 0 && $data_discount['JumlahDiscount'] <= 1) {
                        $besar_discount = $harga_tiket * $data_discount['JumlahDiscount'];
                    } else {
                        $besar_discount = $data_discount['JumlahDiscount'];
                    }
                }
                else{
                    $besar_discount = $harga_tiket-$data_discount['JumlahDiscount'];
                }

                $discount	= ($besar_discount<=$harga_tiket)?$besar_discount:$harga_tiket;

                $sub_total	= substr($data_discount['KodeDiscount'],0,1)!="R"?$harga_tiket:$harga_tiket+$Reservasi->getHargaTiketNormal($id_jurusan);
                $total_bayar= $sub_total-$discount;
                $bayar 		= $total_bayar;

                $harga_point      = $Member->ambilHargaPoin();
                $jumlah_poin_didebit  = $total_bayar/$harga_point;
                $saldo_poin =$Member->ambilSaldo($data_member['IdMember']);

                if($saldo_poin-$jumlah_poin_didebit >= 0){

                    // SALDO LEBIH BESAR DARI TOTAL PEMBELIAN MAKA UPDATE TBL RESERVASI
                    $sql = "UPDATE tbl_reservasi SET 
                                                IdDiscount = $id_discount, 
                                                Discount='$discount',
                                                SubTotal='$sub_total',
                                                Total='$total_bayar',
                                                JenisPenumpang ='$jenis_penumpang',
                                                JenisDiscount='$jenis_discount' 
                         WHERE KodeBooking='$kode_booking'";
                    if(!$result = $db->sql_query($sql)){
                        echo("Err :".__LINE__);
                        exit;
                    }

                    if($is_return == 1){
                        //penumpang return
                        $kode_voucher = $Voucher->setVoucherReturn($no_tiket, $id_jurusan, $kode_jadwal, $discount, $userdata['user_id'],1,$config['masa_berlaku_voucher_return']);
                    }

                }else{
                    //EXIT
                    $template->assign_block_vars('PESAN',array('ISI'=>'---[ POIN MEMBER TIDAK MENCUKUPI ]---'));
                    $template->set_filenames(array('body' => 'reservasi/reservasi.member.debit.tpl'));

                    include($adp_root_path . 'includes/page_header_detail.php');
                    $template->pparse('body');
                    include($adp_root_path . 'includes/page_tail.php');
                    exit;
                }

            }

			$harga_point      = $Member->ambilHargaPoin();
			$kode_ref = $Member->generateKodeReferensi()."DB";
			$total_bayar_rupiah	= $Member->ambilTotalPembelian($kode_booking);

			$jumlah_poin_didebit  = $total_bayar_rupiah/$harga_point;
			$kode_token	= rand(100000,999999);
			//memeriksa sisa saldo tiket member
			$saldo_poin=$Member->ambilSaldo($data_member['IdMember']);
			if($saldo_poin-$jumlah_poin_didebit>=0){
				$template->assign_block_vars(
					'MEMBER'
					,array(
					'DTA'=>$data_member['IdMember'],
					'NAMA'=>$data_member['Nama'],
					'HP'=>$data_member['Handphone'],
					'HP2'=>$data_member['Telp'],
					'EMAIL'=>$data_member['Email'],
					'JENISKELAMIN'=>($data_member['JenisKelamin']==0?"Laki-laki":"Perempuan"),
					'KOTA'=>$data_member['Kota'],
					'PEKERJAAN'=>$data_member['Pekerjaan'],
					'TGLBERLAKU'=>dateparse(FormatMySQLDateToTgl($data_member['ExpiredDate'])),
					'POIN'=>number_format($data_member['SaldoDeposit'],0,",","."),
					'JMLDEBIT'=>number_format($jumlah_poin_didebit,0,",","."),
					'KODE_REF'=>$kode_ref,
					'KODE_BOOKING'=>$kode_booking,
					)
				);

				if($Member->preDebitMember($data_member['IdMember'],$kode_ref,$total_bayar_rupiah,
					$jumlah_poin_didebit,$userdata['user_id'],$userdata['nama'],$kode_token)){

					$SMSGateway = new SMSGateway();
					$isi_pesan  = "Tn/Ny ".strtoupper(substr($data_member['Nama'],0,1)).substr($data_member['Nama'],1,14).", anda akan melakukan pembelian tiket Sebesar ".number_format($jumlah_poin_didebit,0,",",".")." Poin. Berikan kode ini pada petugas kami:".$kode_token;
					$SMSGateway->sendSMS($data_member['Handphone'],$isi_pesan);

				};
			}
			else{
				$template->assign_block_vars('PESAN',array('ISI'=>'---[ POIN MEMBER TIDAK MENCUKUPI ]---'));

			}
		}

		$template->set_filenames(array('body' => 'reservasi/reservasi.member.debit.tpl'));

		include($adp_root_path . 'includes/page_header_detail.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
	exit;

	case "getdatamember":
		// aksi menambah member

		$id_member  		= $HTTP_POST_VARS['idmember'];

		$data_member		= $Member->ambilDataByKondisi("IdMember='$id_member'");

		if($data_member['IdMember']==""){
			echo("proses_ok=true;alert('Data member tidak ditemukan!');");exit;
		}

    $harga_point      = $Member->ambilHargaPoin();

		$total_bayar_rupiah	= $Member->ambilTotalPembelian($kode_booking);

    $jumlah_poin_didebit  = $total_bayar_rupiah/$harga_point;

		//memeriksa sisa saldo tiket member
		$saldo_poin=$Member->ambilSaldo($id_member);

		if($saldo_poin-$jumlah_poin_didebit>=0){
      $return_val	=
        "beforeproses.style.display='none';
        detailmember.style.display='block';
        idmember.readOnly	= true;
        idmember.style.border='0px';
        proses_ok=true;
        nama.innerHTML='".$data_member['Nama']."';
        hp.innerHTML='".$data_member['Handphone']."';
        hp2.innerHTML='".$data_member['Telp']."';
        email.innerHTML='".$data_member['Email']."';
        jeniskelamin.innerHTML='".($data_member['JenisKelamin']==0?"Laki-laki":"Perempuan")."';
        kota.innerHTML='".$data_member['Kota']."';
        pekerjaan.innerHTML='".$data_member['Pekerjaan']."';
        tglberlaku.innerHTML='".dateparse(FormatMySQLDateToTgl($data_member['ExpiredDate']))."';
        saldo.innerHTML='".number_format($data_member['SaldoDeposit'],0,",",".")."';
        jumlahdebit.innerHTML='".number_format($jumlah_poin_didebit,0,",",".")."';";
    }
    else{
      $return_val = "proses_ok=true;pesan.style.display='block';pesan.innerHTML='---[ POIN MEMBER TIDAK MENCUKUPI ]---';";
		}

		echo($return_val);
	exit;

  	case "kirimtoken":

    $kode_ref		  = $HTTP_POST_VARS['koderef'];
    $id_member	  = $HTTP_POST_VARS['idmember'];

    $harga_point      = $Member->ambilHargaPoin();

    $total_bayar_rupiah	= $Member->ambilTotalPembelian($kode_booking);

    $jumlah_poin_didebit  = $total_bayar_rupiah/$harga_point;

    //periksa kode token
    $kode_token	= rand(100000,999999);

    if($Member->preDebitMember($id_member,$kode_ref,$total_bayar_rupiah,
      $jumlah_poin_didebit,$userdata['user_id'],$userdata['nama'],$kode_token)){
      //berhasil menambahkan data

      $data_member = $Member->ambilData($id_member);
      $hp = $data_member["Handphone"];

      //KIRIM TOKEN ke Pelanggan
      $SMSGateway = new SMSGateway();
      $isi_pesan  = "Tn/Ny ".strtoupper(substr($data_member['Nama'],0,1)).substr($data_member['Nama'],1,14).", anda akan melakukan pembelian tiket Sebesar ".number_format($jumlah_poin_didebit,0,",",".")." Poin. Berikan kode ini pada petugas kami:".$kode_token;
      $SMSGateway->sendSMS($hp,$isi_pesan);

      echo(99);

    }
    else{
      echo(0);
    }

    exit;

  	case "debit":

		$kode_ref	= $HTTP_POST_VARS['koderef'];
		$id_member	= $HTTP_POST_VARS['idmember'];
		$kode_token	= $HTTP_POST_VARS['kodetoken'];
		
    $data_debit = $Member->ambilDetailLogDebit($kode_ref);

    if($data_debit["ID"]==""){
      echo("status='ERROR';err=2;");
      exit;
    }

    if($data_debit["KodeToken"]!=$kode_token){
      echo("status='ERROR';err=1;");
      exit;
    }

    if($Member->verifikasiDebit(
      $id_member,$kode_ref,$kode_booking,
      $userdata["KodeCabang"],$data_debit["PetugasDebit"],$kode_token))
	{


      //berhasil menambahkan data

      $data_member = $Member->ambilData($id_member);
      $hp = $data_member["Handphone"];
	  $idPonta = $data_member['NoSeriKartu'];


		$LogDebit = $Member->ambilDetailLogDebit($kode_ref);
		$JmlRupiah = $LogDebit['JumlahRupiah'];
		$poinPonta = (5*$JmlRupiah)/100;
		$JmlTagihan= 1.2 * $poinPonta;

		//SIMPAN DATA TRANSAKSI DI TBL TRANSASKSI PONTA
        $sql = "SELECT NoTiket FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";
        if(!$result = $db->sql_query($sql)){
            echo("status='ERROR';err=0;");
            exit;
        }
        while ($row = $db->sql_fetchrow($result)){
            $sql = "INSERT INTO tbl_ponta_transaksi (IdCard,KodeBooking,NoTiket,WaktuTransaksi,JumlahTransaksi,JumlahPoin,JumlahTagihan)
				    VALUES('$idPonta','$kode_booking','$row[0]',NOW(),$JmlRupiah,$poinPonta,$JmlTagihan)";
            if(!$db->sql_query($sql)){
                echo("status='ERROR';err=".mysql_error().";");
                exit;
            }
        }


		/**UPDATE 29-03-2016
		 * SIMPAN NILAI TRANSAKSI MEMBER KE FIELD POINT
		 * NANTINYA JIKA POINT TELAH 500K (saat ini hanya 500k nanti bisa diset)MAKA AKAN CREATE VOUCHER DAN LANGSUNG KIRIM SMS VOUCHER NYA
		 */
		//OKE LET'S DO IT
		$sql = "UPDATE tbl_md_member SET Point = Point + $JmlRupiah WHERE IdMember = '$id_member'";
		if(!$db->sql_query($sql)){
			echo("status='ERROR';err=2;");
			exit;
		}



      //KIRIM TOKEN ke Pelanggan
      $SMSGateway = new SMSGateway();
      $isi_pesan  = "Tn/Ny ".strtoupper(substr($data_member['Nama'],0,1)).substr($data_member['Nama'],1,14).",Pembelian Berhasil! Poin anda berjumlah: ".number_format($data_member["SaldoDeposit"],0,",",".")." Poin";
      $SMSGateway->sendSMS($hp,$isi_pesan);


      echo("status='OK';");

    }
    else{
      echo("status='ERROR';err=9;");
    }


	exit;

	case "cetak":
		
		include($adp_root_path . 'ClassReservasi.php');
		include($adp_root_path . 'ClassPelanggan.php');
		include($adp_root_path . 'ClassJurusan.php');
		
		$Reservasi	= new Reservasi();
		$Pelanggan	= new Pelanggan();
		$Jurusan		= new Jurusan();
		
		$kode_ref		= $HTTP_GET_VARS['kodereferensi'];
		$no_tiket		= $HTTP_GET_VARS['notiket'];
		$is_duplicat= $HTTP_GET_VARS['isdup'];
		
		if($kode_booking!=""){
			$result	= $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
		}
		else{
			$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
		}
		
		if(!$result){
			echo("err:".__LINE__);exit;
		}
		
		$jum_tiket	= @mysql_num_rows($result);
	
		while ($row = $db->sql_fetchrow($result)){
			
			$array_jurusan	= $Jurusan->ambilNamaJurusanByIdJurusan($row['IdJurusan']);
			
			$no_tiket					= $row['NoTiket'];
			$kode_booking			= $row['KodeBooking'];
			$id_member				= $row['IdMember'];
			$nama							= $row['Nama'];
			$alamat						= $row['Alamat'];
			$telp							= $row['Telp'];
			$tanggal					= $row['TglBerangkat'];
			$jam							= $row['JamBerangkat'];
			$asal							= $array_jurusan['Asal'];
			$tujuan						= $array_jurusan['Tujuan'];
			$id_jurusan				= $row['IdJurusan'];
			$bayar						= $row['Total'];
			$nomor_kursi			= $row['NomorKursi'];
			$kode_jadwal			= $row['KodeJadwal'];
			$waktu_cetak_tiket= dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket']));
			
			if($is_duplicat!=1 || $is_duplicat==""){
				if(!$sudah_diperiksa){
					//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
					$Pelanggan	= new Pelanggan();
					
					if($Pelanggan->periksaDuplikasi($telp)){
						//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN
						
						$Pelanggan->ubah(
							$telp,$telp,
							$telp, $telp ,$nama,
							"", $tanggal,
							$userdata['user_id'], $kode_jadwal);
					}
					else{
						//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
						$Pelanggan->tambah(
							$telp, $telp ,$nama,
							$alamat, $tanggal, $tanggal,
							$userdata['user_id'], $kode_jadwal,1,
							0);
					}
					
					$sudah_diperiksa	= true;
					
					$Reservasi->updateFrekwensiMember($id_member,$tanggal);
					
				}
				
				$nama_cso	= $cso;
				
				$waktu_cetak_tiket=dateparseWithTime(FormatMySQLDateToTglWithTime(date("Y-m-d H:i")));
				
			}
			else{
				$nama_cso	= $User->ambilNamaUser($row['PetugasCetakTiket']);
				$keterangan_duplikat	= "****DUPLIKAT****";
			}
			
			$cso	= $nama_cso;
			
			$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
			
			//PESAN SPONSOR
			
			$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						/*'NAMA_PERUSAHAAN'		=>strtoupper($data_perusahaan[NamaPerusahaan]),*/
						'NO_TIKET'					=>$no_tiket,
						'DUPLIKAT'					=>$keterangan_duplikat,
						'JENIS_BAYAR'				=>$ket_jenis_pembayaran,
						'TGL_BERANGKAT'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
						'JURUSAN'						=>substr($asal,0,20)."-".substr($tujuan,0,20),
						'JAM_BERANGKAT'			=>$jam,
						'NAMA_PENUMPANG'		=>substr($nama,0,20),
						'NOMOR_KURSI'				=>$nomor_kursi,
						'POINT_MEMBER'			=>$show_point_member,
						'CSO'								=>substr($cso,0,20),
						'PESAN'							=>$pesan_sponsor,
						'WAKTU_CETAK'				=>$waktu_cetak_tiket
					)
				);
		}
		
		$data_member	= $Member->ambilData($id_member);
		
		$template->
			assign_block_vars(
				'MEMBER',
					array(
						'KODE_REFERENSI'			=>$kode_ref,
						'KODE_MEMBER'					=>$id_member,
						'NAMA_MEMBER'					=>$data_member['Nama'],
						'MASA_BERLAKU'				=>FormatMySQLDateToTglWithTime($data_member['ExpiredDate']),
						'JUMLAH_TIKET_DIBELI'	=>$jum_tiket,
						'SALDO'								=>number_format($data_member['SaldoDeposit'],0,",",".")
					)
		);
		
		$template->set_filenames(array('body' => 'reservasi/reservasi.member.tiket.tpl')); 
		$template->pparse('body');
	exit;

	case "potong_manual":
		$id_member = "GK4U1EE3";
		$kode_booking = "BGW14JSSZE1M";

		$data_debit = $Member->ambilDetailLogDebit($kode_ref);
		if($data_debit["ID"]==""){
			echo("status='ERROR';err=2;");
			exit;
		}

		if($data_debit["KodeToken"]!=$kode_token){
			echo("status='ERROR';err=1;");
			exit;
		}

		if($Member->verifikasiDebit(
			$id_member,$kode_ref,$kode_booking,
			$userdata["KodeCabang"],$data_debit["PetugasDebit"],$kode_token))
		{
			echo "berhasil dipotong";
		}else{
			echo "gagal dipotong";
		}
	exit;

	case "testing":

		$sql	=
			"SELECT GROUP_CONCAT(Nama SEPARATOR ',') AS NamaPenumpang,
			  KodeJadwal,TglBerangkat,
			  GROUP_CONCAT(DISTINCT HargaTiket SEPARATOR ',') AS GrupHargaTiket
			 FROM tbl_reservasi WHERE KodeBooking='$KodeBooking' GROUP BY KodeBooking";

		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}

		$data_tiket = $db->sql_fetchrow($result);

		//mengambil jadwal utama
		$sql	=
			"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$data_tiket[KodeJadwal]';";

		if (!$result = $db->sql_query($sql)){
			echo("Err :".__LINE__);
			exit;
		}

		//MENGAMBIL PROMO JIKA ADA
		$data_promo = $this->ambilPromoMember($data_tiket["KodeJadwal"],$data_tiket["TglBerangkat"]);

		if($data_promo["KodePromo"]!=""){
			$grup_harga_tiket = explode(",",$data_tiket["GrupHargaTiket"]);
			$harga_tiket = $grup_harga_tiket[0];

			if($data_promo["FlagDiscount"]==0) {
				$discount = ($data_promo['JumlahDiscount'] > 1) ? $data_promo['JumlahDiscount'] : $data_promo['JumlahDiscount'] * $harga_tiket;
				$discount = $discount < $harga_tiket ? $discount : $harga_tiket;
			}
			else{
				$discount = $harga_tiket-$data_promo['JumlahDiscount'];
			}
		}

		$discount = $discount<=0?0:$discount;

		$data_jadwal	= $db->sql_fetchrow($result);
		$kode_jadwal	= $data_jadwal[0];

		$sql	=
			"CALL sp_member_deposit_trx(
				'$IdMember','$KodeReferensi','$KodeBooking',
				'$kode_jadwal','$data_tiket[TglBerangkat]','Pembelian $KodeBooking $data_tiket[KodeJadwal] $data_tiket[TglBerangkat] Pnp:$data_tiket[NamaPenumpang]',
				'$CSO','$CabangTransaksi','$HargaPoin','$discount','$data_promo[KodePromo]','$Signature')";

		echo $sql;
		exit;
}

?>