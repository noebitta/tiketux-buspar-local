<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$id_page = 406;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
	redirect('index.'.$phpEx,true);
}
//#############################################################################

$interface_menu_utama=false;

$PengaturanUmum	= new PengaturanUmum();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];

$cari						= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$counter 					= isset($HTTP_GET_VARS['opt_cabang'])? $HTTP_GET_VARS['opt_cabang'] : $HTTP_POST_VARS['opt_cabang'];
$spbu 					= isset($HTTP_GET_VARS['spbu'])? $HTTP_GET_VARS['spbu'] : $HTTP_POST_VARS['spbu'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$kota  						= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];

$cabang_asal		= isset($HTTP_GET_VARS['opt_cabang_asal'])? $HTTP_GET_VARS['opt_cabang_asal'] : $HTTP_POST_VARS['opt_cabang_asal'];
$cabang_tujuan	= isset($HTTP_GET_VARS['opt_tujuan'])? $HTTP_GET_VARS['opt_tujuan'] : $HTTP_POST_VARS['opt_tujuan'];

$start	= $tanggal_mulai==''?true:false;
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Jurusan= new Jurusan();
$Cabang	= new Cabang();
$Sopir	= new Sopir();

function setComboCabangAsal($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;

	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboCabangTujuan($cabang_asal,$cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Jurusan;

	$result=$Jurusan->ambilDataByCabangAsal($cabang_asal);
	$opt_cabang="";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			$opt_cabang .="<option value='$row[IdJurusan]' $selected>$row[NamaCabangTujuan] ($row[KodeJurusan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}
	return $opt_cabang;
	//END SET COMBO CABANG
}

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;

	$Mobil = new Mobil();
	$result=$Mobil->ambilDataForComboBox();
	$selected_default	= ($kode_kendaraan!="")?"":"selected";

	$opt_mobil="<option value='' $selected_default>silahkan pilih...</option>";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan]</option>";
		}
	}
	else{
		echo("err :".__LINE__);exit;
	}

	return $opt_mobil;
	//END SET COMBO MOBIL
}

function setComboSopir($kode_sopir_dipilih){
	//SET COMBO SOPIR
	global $db;
	global $Sopir;

	$result=$Sopir->ambilData("","Nama,Alamat","ASC");

	$selected_default	= ($kode_sopir_dipilih!="")?"":"selected";
	$opt_sopir="<option value='' $selected_default>- silahkan pilih sopir  -</option>";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Err :".__LINE__);exit;
	}
	return $opt_sopir;
	//END SET COMBO SOPIR

}

function generateNoSPJ($kode_jadwal,$id_jurusan){
	global $db;

	$i=0;

	do{
		$no_spj= "MNF".dateYMD().substr($kode_jadwal,0,3).$id_jurusan.rand(1000,9999);

		$sql	= "SELECT COUNT(1) FROM tbl_spj WHERE NoSPJ='$no_spj'";

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$row=$db->sql_fetchrow($result);

		$i++;

	}while($row[0]>0 && $i<3);

	$no_spj	.=($row[0]>0 && $i>=3)?"X":"";

	return $no_spj;
}

switch ($mode) {
	case 'get_tujuan':
		$cabang_asal		= $HTTP_GET_VARS['asal'];
		$id_jurusan			= $HTTP_GET_VARS['jurusan'];

		$opt_cabang_tujuan=
			"<select id='opt_tujuan' name='opt_tujuan' onChange='getUpdateJadwal(this.value);'>".
			setComboCabangTujuan($cabang_asal,$id_jurusan)
			."</select>";

		echo($opt_cabang_tujuan);

		exit;
	case 'get_jadwal':
		$tgl 				= $HTTP_GET_VARS['tgl'];
		$id_jurusan 		= $HTTP_GET_VARS['id_jurusan'];

		$Jadwal	= new Jadwal();

		$result	= $Jadwal->setComboJadwal($tgl,$id_jurusan);

		$opt = "";

		if ($result){
			$opt = "<option>(none)</option>" . $opt;
			while ($row = $db->sql_fetchrow($result)){
				$opt .= "<option value='$row[0]'>$row[0]-$row[1]</option>";
			}
		}else{
			$opt .="<option selected=selected>Error</option>";
		}

		$opt_jadwal			= "<select id='opt_jadwal' name='opt_jadwal'>".$opt."</select>";

		echo ($opt_jadwal);
		exit;
	case 'get_mobil':
		echo "<select name='mobil' id='mobil'>"
			.setComboMobil($mobil_dipilih).
			"</select>";;
		exit;
	case 'get_sopir':
		$return			=
			"<select name='dlgcbosopir' id='dlgcbosopir'>"
			.setComboSopir($sopir_dipilih).
			"</select>";

		echo($return);

		exit;
	case 'add':
		$date 			= date('Y-m-d H:i:s');
		$tgl_berangkat	= FormatTglToMySQLDate($HTTP_GET_VARS['tgl_berangkat']);
		$kode_jadwal	= $HTTP_GET_VARS['kode_jadwal'];
		$no_spj			= generateNoSPJ($kode_jadwal,$id_jurusan);
		$id_jurusan 	= $HTTP_GET_VARS['id_jurusan'];
		$mobil 			= $HTTP_GET_VARS['mobil'];
		$sopir 			= $HTTP_GET_VARS['sopir'];
		$sql 			= "SELECT f_sopir_get_nama_by_id('$sopir') Sopir";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$nama_sopir		= $row['Sopir'];
		}
		$user_id 		= $userdata['user_id'];
		$sql 			= "SELECT f_user_get_nama_by_userid('$user_id') User";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$nama_user		= $row['User'];
		}
		$kilometer		= $HTTP_GET_VARS['kilometer'];
		$jenis_bbm		= $HTTP_GET_VARS['jenis_bbm'];
		$nama_bbm		= ($jenis_bbm == 1)?'SOLAR':'PREMIUM';
		$jml_liter		= $HTTP_GET_VARS['jml_liter'];
		$kode_spbu		= $HTTP_GET_VARS['kode_spbu'];
		switch ($kode_spbu) {
			case 1:
				$nama_spbu = 'KM 57';
				break;

			case 2:
				$nama_spbu = 'KM 72';
				break;
		}
		$sql 			= "SELECT NilaiParameter FROM tbl_pengaturan_parameter WHERE NamaParameter LIKE '%%$nama_bbm%%'";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$harga_bbm		= $row['NilaiParameter'];
		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		$kode_voucher   = $kode_spbu.date('YmdHis').rand(1,100);
		$digit          = 7 ;
		$secretKey      = rand(pow(10, $digit-1), pow(10, $digit)-1);

		$jml_biaya		= $harga_bbm*$jml_liter;

		$sql 			= "INSERT INTO tbl_voucher_bbm SET KodeVoucher = '$kode_voucher', NoSPJ = '$no_spj', TglBerangkat = '$tgl_berangkat',
        						KodeJadwal = '$kode_jadwal', IdJurusan = '$id_jurusan', NoBody = '$mobil', KodeSopir = '$sopir', NamaSopir = '$nama_sopir',
        						Kilometer = '$kilometer', JenisBBM = '$jenis_bbm', JumlahLiter = '$jml_liter', JumlahBiaya = '$jml_biaya', 
        						IdPetugas = '$user_id', NamaPetugas = '$nama_user', TglDicatat = '$date', KodeSPBU = '$kode_spbu', NamaSPBU = '$nama_spbu'";
		if($db->sql_query($sql)){
			echo("alert('Voucher telah berhasil dibuat!');window.location.reload();");
		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}

		exit;
    case 'edit_voucher' :
        $no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];

        $sql 		= "SELECT * FROM tbl_voucher_bbm WHERE NoSPJ = '$no_spj'";
        if(!$result = $db->sql_query($sql)){
            echo("Err:".__LINE__);exit;
        }

        $row = $db->sql_fetchrow($result);

        $temp_var_aktif = 'spbu_'.$row['KodeSPBU'];
        $$temp_var_aktif = 'selected';

        $temp_var_aktif = 'jenis_bbm_'.$row['JenisBBM'];
        $$temp_var_aktif = 'selected';

        $result = "<form onsubmit='return false;'>
						<table width='400'>
						<tr>
							<td bgcolor='ffffff' align='center'>
								<table>
									<tr><td colspan='2'><h2>Edit Voucher</h2></td></tr>
									<tr>
										<td align='right'>Manifest :</td>
										<td>
											<input id='no_spj_edit' name='no_spj_edit' type='text' onfocus='this.style.background='white'' value='".$row['NoSPJ']."' />
										</td>
									</tr>
									<tr>
										<td align='right'>Nama SPBU:</td>
										<td>
											<select id='kode_spbu_edit'>
												<option value='1' $spbu_1>SPBU 57</option>
												<option value='2' $spbu_2>SPBU 72</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align='right'>Kilometer:</td>
										<td><input id='kilometer_edit' name='kilometer_edit' type='text' onfocus='this.style.background='white'' size='5' onkeypress='validasiLiter(event)' value ='".$row['Kilometer']."'/></td>
									</tr>
									<tr>
										<td align='right'>Jenis BBM:</td>
										<td>
											<select id='jenis_bbm_edit'>
												<option value='1' $jenis_bbm_1>SOLAR</option>
												<option value='2' $jenis_bbm_2>PREMIUM</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align='right'>Jumlah Liter:</td>
										<td><input id='jml_liter_edit' name='jml_liter_edit' type='text' onfocus='this.style.background='white'' size='5' onkeypress='validasiLiter(event)' value='".$row['JumlahLiter']."'/></td>
									</tr>
								</table>
								<br>
							</td>
						</tr>
						<tr>
						   <td colspan='2' align='center'>
								<br>
								<input type='button' id='dialogeditvouchercancel' value='&nbsp;Cancel&nbsp;' onClick='dlg_edit_voucher.hide();'>
								<input type='button' onclick='updateVoucher();' id='dialogeditvoucherproses' value='&nbsp;Update&nbsp;'>
							 </td>
						</tr>
						</table>
						</form>";
        echo $result;
        exit;

    case 'update_voucher' :
        $date 		= date('Y-m-d H:i:s');
        $no_spj     = isset($HTTP_GET_VARS['no_spj_edit'])? $HTTP_GET_VARS['no_spj_edit'] : $HTTP_POST_VARS['no_spj_edit'];
        $kilometer	= $HTTP_GET_VARS['kilometer_edit'];
        $jenis_bbm  = $HTTP_GET_VARS['jenis_bbm_edit'];
        $jml_liter	= $HTTP_GET_VARS['jml_liter_edit'];
        $kode_spbu	= $HTTP_GET_VARS['kode_spbu_edit'];
        $nama_bbm	= ($jenis_bbm == 1)?'SOLAR':'PREMIUM';
        switch ($kode_spbu) {
            case 1:
                $nama_spbu = 'BBM_SPBU_57';
                break;

            case 2:
                $nama_spbu = 'BBM_SPBU_72';
                break;
        }
        $sql 			= "SELECT NilaiParameter FROM tbl_pengaturan_parameter WHERE NamaParameter LIKE '%%$nama_bbm%%'";
        if ($result = $db->sql_query($sql)){
            $row 			= $db->sql_fetchrow($result);
            $harga_bbm		= $row['NilaiParameter'];
        }else{
            die_error("ERR: $this->ID_FILE".__LINE__);
        }
        $digit          = 7 ;
        $secretKey      = rand(pow(10, $digit-1), pow(10, $digit)-1);

        $jml_biaya		= $harga_bbm*$jml_liter;

        $user_id 		= $userdata['user_id'];

        $sql 			= "UPDATE tbl_voucher_bbm SET
        						Kilometer = '$kilometer', JenisBBM = '$jenis_bbm', JumlahLiter = '$jml_liter', JumlahBiaya = '$jml_biaya',
        						PetugasEdit = '$user_id', WaktuEdit = '$date', KodeSPBU = '$kode_spbu', NamaSPBU = '$nama_spbu'
        					WHERE NoSPJ = '$no_spj'";
        if($db->sql_query($sql)){
            echo("alert('Voucher telah berhasil Di Ubah!');window.location.reload();");
        }else{
            die_error("ERR: $this->ID_FILE".__LINE__);
        }

        exit;
	case 'approve':
		$no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];
		$date 		= date("Y-m-d H:i:s");
		$user_id 	= $userdata['user_id'];
		$sql 		= "UPDATE tbl_voucher_bbm SET IsApprove = 1, WaktuApprove = '$date', PetugasApprove = '$user_id' 
							WHERE NoSPJ = '$no_spj';";
		if(!$db->sql_query($sql)){
			echo 0;
			die_error("ERR: $this->ID_FILE".__LINE__);
		}else{
			echo 1;
			exit;
		}
		break;
    case 'reprint':
        $voucher     = isset($HTTP_GET_VARS['voucher'])? $HTTP_GET_VARS['voucher'] : $HTTP_POST_VARS['voucher'];
        $sql 		= "UPDATE tbl_voucher_bbm SET RePrint = 1 WHERE KodeVoucher = '$voucher';";
        if(!$db->sql_query($sql)){
            echo 0;
            die_error("ERR: $this->ID_FILE".__LINE__);
        }else{
            echo 1;
            exit;
        }
        break;
}

$kondisi_spbu	= $spbu==""?"":" AND NamaSPBU='$spbu'";

$kondisi	=
	"(TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	AND (NoBody LIKE '$cari'
		OR KodeJadwal LIKE '$cari%'
		OR NamaSopir LIKE '%$cari%'
		OR NamaPetugas LIKE '%$cari%')
	$kondisi_spbu";

$order	=($order=='')?"ASC":$order;

$sort_by =($sort_by=='')?"TglBerangkat":$sort_by;

// LIST
$template->set_filenames(array('body' => 'laporan_voucher_bbm/laporan_voucher_bbm_body.tpl'));

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"1","tbl_voucher_bbm",
	"&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&spbu=".$spbu."&cari=".$cari."&sort_by=".$sort_by."&order=".$order,
	"WHERE ".$kondisi,"laporan_voucher_bbm.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//mengambil query voucher BBM

$kondisi_cabang	= $counter==""?"":" AND (SELECT KodeCabangAsal FROM tbl_md_jurusan j WHERE j.IdJurusan=tbl_voucher_bbm.IdJurusan)='$counter'";

$sql	=
	"SELECT *,tbl_user.user_id,tbl_user.nama,f_kendaraan_ambil_nopol_by_kode(NoBody) as NoPol
	FROM tbl_voucher_bbm LEFT JOIN tbl_user ON tbl_voucher_bbm.PetugasApprove = tbl_user.user_id
	WHERE $kondisi
	$kondisi_cabang
	ORDER BY TglDicatat DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
	while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';

		if (($i % 2)==0){
			$odd = 'even';
		}

		if($row['IsApprove'] == 1){
			$status 	= 'Sudah Approve';
			$bgApprove	= '#c0ff53';
		}else{
			$status 	= "<a href='#' onClick='return approve(\"".$row['NoSPJ']."\");'>Approve<a/>";
			$bgApprove	= '';
		}

        if($row['JumlahLiter'] == ""){
            $act = "";
        }else{
            $act = "showDialogEditVoucher('".$row['NoSPJ']."')";
        }

        if($row['RePrint'] == 1){
            $odd = 'green';
            $reprint = "OK";
        }else{
            $reprint = "<a href='#' onClick='return reprint(\"".$row['KodeVoucher']."\");'>Reprint<a/>";
        }

		/*$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'tanggal'=>dateparseD_Y_M(FormatMySQLDateToTgl($row['TglTransaksi'])),
					'jurusan'=>$row['Jurusan'],
					'jam'=>substr($row['JamBerangkat'],0,-3),
					'no_spj'=>$row['NoSPJ'],
					'kendaraan'=>$row['NoKendaraan'],
					'sopir'=>$row['NamaSopir'],
					'jumlah'=>number_format($row['Jumlah'],0,",","."),
				)
			);*/

		$template->assign_block_vars(
			'ROW',
			array(
				'odd'			=>$odd,
				'no'			=>$i,
				'tanggal'		=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),
				'voucher'		=>$row['KodeVoucher'],
				'nospj'			=>$row['NoSPJ'],
				'body'			=>$row['NoBody'],
				'nopol'			=>$row['NoPol'],
				'sopir'			=>$row['NamaSopir'],
				'jadwal'		=>$row['KodeJadwal'],
				/*'bbm'			=>($row['JenisBBM']==0?"PREMIUM":"SOLAR"),*/
				'liter'			=>number_format($row['JumlahLiter'],2,".",","),
				'biaya'			=>number_format($row['JumlahBiaya'],0,".",","),
				'sbpu'			=>$row['NamaSPBU'],
				'petugas'		=>$row['NamaPetugas'],
				'status'		=>$status,
				'bgstatus'		=>$bgApprove,
				'tglapprove'	=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuApprove'])),
				/*'petugasapprove'=>$row['nama'],*/
				'waktucatat'	=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglDicatat'])),
                'act'           =>$act,
                'reprint'       => $reprint
			)
		);

		$i++;
	}
}
else{
	//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&spbu=".$spbu.
	"&cabang=".$counter."&cari=".$cari."&sort_by=".$sort_by."&order=".$order."";

$script_cetak_pdf="Start('laporan_voucher_bbm_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";

$script_cetak_excel="Start('laporan_voucher_bbm_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT



//BEGIN KOMPONEN-KOMPONEN SORTING================================
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';

$parameter_sorting	=
	"&page=".$idx_page."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&spbu=".$spbu."&cari=".$cari."&order=".$order_invert;

$url_submit	= "laporan_voucher_bbm.php";

$array_sort	=
	"'".append_sid($url_submit).'&sort_by=TglBerangkat'.$parameter_sorting."',".
	"'".append_sid($url_submit.'?sort_by=KodeVoucher'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=NoSPJ'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=NoBody'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=NamaSopir'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=KodeJadwal'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=JenisBBM'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=JumlahLiter'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=JumlahBiaya'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=NamaSPBU'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=NamaPetugas'.$parameter_sorting)."',".
	"'".append_sid($url_submit.'?sort_by=TglDicatat'.$parameter_sorting)."'";

//END KOMPONEN-KOMPONEN SORTING================================

$page_title	= "Checker SPBU";

//Ambil total tagihan BBM

$sql	=
	"SELECT SUM(JumlahBiaya) AS TotalBiaya,SUM(JumlahLiter) AS TotalLiter,SUM(IF(PetugasApprove IS NULL,JumlahBiaya,NULL)) AS TotalBiayaBelumApp
	FROM tbl_voucher_bbm
	WHERE $kondisi";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$row = $db->sql_fetchrow($result);

$summary	=
	"<b>Total Tagihan	= Rp. ".number_format($row[0],0,",",".")."</b></br>
	 <b>Total Tagihan Belum Approve	= Rp. ".number_format($row[2],0,",",".")."</b>";

$template->assign_vars(array(

	/*'BCRUMP'    		=> '<a href="'.append_sid('menu_lap_keuangan.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan') .'">Home</a> | <a href="'.append_sid('laporan_voucher_bbm.'.$phpEx).'">Laporan Checker SPBU</a>',
	'ACTION_CARI'		=> append_sid('laporan_voucher_bbm.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'OPT_KOTA'			=> setComboKota($kota_dipilih),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'OPT_SORT'			=> $opt_sort_by,
	'OPT_ORDER'			=> $opt_order,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=tgltransaksi'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Tgl Transaksi ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=jurusan'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Jurusan ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=jamberangkat'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Jam Berangkat ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=nospj'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan No. SPJ ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=nokendaraan'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan No. Kendaraan ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=namasopir'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Nama Sopir ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=jumlah'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Jumlah ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_voucher_bbm.'.$phpEx.'?sort_by=pencetak'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Pencetak ($order_invert)",*/
		'BCRUMP'    		=>setBcrump($id_page),
		'ACTION_CARI'		=> append_sid('laporan_voucher_bbm.'.$phpEx),
		'ASAL'					=> $cabang_asal,
		'ID_JURUSAN'		=> $cabang_tujuan,
		'CABANG_ASAL'		=> $Cabang->setInterfaceComboCabang($cabang_asal),
		'OPT_CABANG'		=> $Cabang->setInterfaceComboCabang($counter),
		'CABANG_TUJUAN'		=> setComboCabangTujuan($cabang_asal,$cabang_tujuan),
		'CARI'					=> $cari,
		'TGL_AWAL'			=> $tanggal_mulai,
		'TGL_AKHIR'			=> $tanggal_akhir,
		'SUMMARY'				=> $summary,
		'PAGING'				=> $paging,
		'CETAK_PDF'			=> $script_cetak_pdf,
		'CETAK_XL'			=> $script_cetak_excel,
		'ARRAY_SORT'		=> $array_sort,
		'URL_ADD'			=> '<a href="'.append_sid('laporan_voucher_bbm.'.$phpEx.'?mode=add') .'">Tambah Data</a>',
	)
);

$result	= $PengaturanUmum->ambilParameterBatch("BBM_SPBU_%");

while($row=$db->sql_fetchrow($result)){
	$template->
	assign_block_vars(
		'OPT_SPBU',
		array(
			'value'		=> $row['NilaiParameter'],
			'nama'		=> $row['NilaiParameter'],
			'selected'=> ($spbu==$row['NilaiParameter']?"selected":"")
		)
	);
}


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>