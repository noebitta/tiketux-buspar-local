<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 601;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################
// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$Member	= new Member();
$Cabang	= new Cabang();

function setComboCabang($cabang_dipilih=""){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->setComboCabang();
	
	$opt_cabang="<option value='' >silahkan pilih...</option>";
		
	if($result){
		
		$kota="";
		
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			
			if($kota!=$row['Kota']){
					
				if($kota!=""){
					$opt_cabang .= "</optgroup>";
				}
					
				$kota=$row['Kota'];
				$opt_cabang .="<optgroup label='$kota'>";
			}
			
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}


	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$tgl_berlaku	= date("d-m-").(date("Y")+1);
		$tgl_lahir		= date("d-m-").(date("Y")-30);
		
		$template->set_filenames(array('body' => 'member/add_body.tpl'));

		$template->assign_vars(array(
		 'BCRUMP'		=>setBcrump($id_page),
		 'JUDUL'		=> 'Tambah Data Member',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'PESAN'				=> $pesan,
		 'BGCOLOR_PESAN'=> $bgcolor_pesan,
		 'U_ADD_ACT'		=> append_sid('pengaturan_member.'.$phpEx),
		 'TGL_LAHIR'		=> $tgl_lahir,
		 'TGL_BERLAKU'	=> $tgl_berlaku,
		 'TGL_REGISTRASI'	=> date("d-m-Y"),
		 'OPT_CABANG_DAFTAR'	=> setComboCabang()
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah member
		
		$id_member  		= str_replace(" ","",$HTTP_POST_VARS['id_member']);
		$no_seri_kartu	= $HTTP_POST_VARS['ponta'];
		$nama   				= $HTTP_POST_VARS['nama'];
		$jenis_kelamin  = $HTTP_POST_VARS['jenis_kelamin'];
		$kategori_member= $HTTP_POST_VARS['kategori_member'];
		$tempat_lahir   = $HTTP_POST_VARS['tempat_lahir'];
		$tgl_lahir   		= $HTTP_POST_VARS['tgl_lahir'];
		$noktp   				= $HTTP_POST_VARS['noktp'];
		$alamat   			= $HTTP_POST_VARS['alamat'];
		$kota   				= $HTTP_POST_VARS['kota'];
		$kode_pos   		= $HTTP_POST_VARS['kode_pos'];
		$tgl_registrasi = $HTTP_POST_VARS['tgl_registrasi'];
		$hp							= $HTTP_POST_VARS['hp'];
		$telp						= $HTTP_POST_VARS['telp'];
		$email					= $HTTP_POST_VARS['email'];
		$pekerjaan			= $HTTP_POST_VARS['pekerjaan'];
		$tgl_berlaku		= $HTTP_POST_VARS['tgl_berlaku'];
		$status_aktif 	= $HTTP_POST_VARS['aktif'];
		$cabang_daftar 	= $HTTP_POST_VARS['cabang_daftar'];

		$terjadi_error=false;
		
		if(($Member->periksaDuplikasi($id_member,$email,$hp) && $Member->periksaDuplikasiKartu($no_seri_kartu)) && $submode==0){
			$pesan="<font color='white' size=3>Member yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Member";

				if($Member-> tambah(
					$id_member,$nama, $jenis_kelamin,
				  $kategori_member,$tempat_lahir,FormatTglToMySQLDate($tgl_lahir),
				  $noktp,FormatTglToMySQLDate($tgl_registrasi),$alamat,
				  $kota,$kode_pos,$telp,
				  $hp,$email, $pekerjaan,
				  $point,FormatTglToMySQLDate($tgl_berlaku), $id_kartu, 
				  $no_seri_kartu,$kata_sandi,$status_aktif,
					$cabang_daftar,$ponta)){
					
					redirect(append_sid('pengaturan_member.'.$phpEx.'?mode=add&pesan=1',true));
					
				}
			}
			else{
				
				$judul="Ubah Data Member";

				if($Member->ubah(
					$id_member,$nama, $jenis_kelamin,
				  $kategori_member,$tempat_lahir,FormatTglToMySQLDate($tgl_lahir),
				  $noktp,FormatTglToMySQLDate($tgl_registrasi),$alamat,
				  $kota,$kode_pos,$telp,
				  $hp,$email, $pekerjaan,
				  $point,FormatTglToMySQLDate($tgl_berlaku), $id_kartu, 
				  $no_seri_kartu,$kata_sandi,$status_aktif,
					$cabang_daftar)){
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
					
				}
			}
			
		}
		
		$temp_var_aktif="jk_".$jenis_kelamin;
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="kategori_".$kategori_member;
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'member/add_body.tpl'));
		$template->assign_vars(array(
			 'BCRUMP'				=>setBcrump($id_page),
			 'JUDUL'				=> $judul,
			 'MODE'   			=> 'save',
			 'SUB'    			=> $submode,
			 'ID_MEMBER'		=> $id_member,
			 'PONTA'			=> $no_seri_kartu,
			 'READONLY'			=> "readonly",
			 'NAMA'    			=> $nama,
			 'JK_0'    			=> $jk_0,
			 'JK_1'    			=> $jk_1,
			 'KATEGORI_0'		=> $kategori_0,
			 'KATEGORI_1'		=> $kategori_1,
			 'TEMPAT_LAHIR'	=> $tempat_lahir,
			 'TGL_LAHIR' 		=> $tgl_lahir,
			 'NO_KTP'   		=> $noktp,
			 'ALAMAT'   		=> $alamat,
			 'KOTA'   			=> $kota,
			 'KODE_POS'   	=> $kode_pos,
			 'TGL_REGISTRASI'=> $tgl_registrasi,
			 'HP'						=> $hp,
			 'TELP'					=> $telp,
			 'EMAIL'				=> $email,
			 'PEKERJAAN'		=> $pekerjaan,
			 'TGL_BERLAKU'	=> $tgl_berlaku,
			 'OPT_CABANG_DAFTAR'	=> setComboCabang($cabang_daftar),
			 'AKTIF_1'			=> $status_aktif_1,
			 'AKTIF_0'			=> $status_aktif_0,
			 'PESAN'				=> $pesan,
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_ADD_ACT'=>append_sid('pengaturan_member.'.$phpEx)
			)
		);
	
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Member->ambilData($id);
		
		$temp_var_aktif="jk_".$row['JenisKelamin'];
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="kategori_".$row['KategoriMember'];
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'member/add_body.tpl'));

		$template->assign_vars(array(
			 'BCRUMP'		=>setBcrump($id_page),
			 'JUDUL'		=> 'Ubah Data Member',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'ID_MEMBER'		=> $id,
			 'PONTA'	=>$row['NoSeriKartu'],
			 'READONLY'			=> "readonly",
			 'NAMA'    			=> $row['Nama'],
			 'JK_0'    			=> $jk_0,
			 'JK_1'    			=> $jk_1,
			 'KATEGORI_0'		=> $kategori_0,
			 'KATEGORI_1'		=> $kategori_1,
			 'TEMPAT_LAHIR'	=> $row['TempatLahir'],
			 'TGL_LAHIR' 		=> FormatMySQLDateToTgl($row['TglLahir']),
			 'NO_KTP'   		=> $row['NoKTP'],
			 'ALAMAT'   		=> $row['Alamat'],
			 'KOTA'   			=> $row['Kota'],
			 'KODE_POS'   	=> $row['KodePos'],
			 'TGL_REGISTRASI'=> FormatMySQLDateToTgl($row['TglRegistrasi']),
			 'HP'						=> $row['Handphone'],
			 'TELP'					=> $row['Telp'],
			 'EMAIL'				=> $row['Email'],
			 'PEKERJAAN'		=> $row['Pekerjaan'],
			 'TGL_BERLAKU'	=> FormatMySQLDateToTgl($row['ExpiredDate']),
			 'OPT_CABANG_DAFTAR'	=> setComboCabang($row['CabangDaftar']),
			 'AKTIF_1'			=> $status_aktif_1,
			 'AKTIF_0'			=> $status_aktif_0,
			 'PESAN'				=> $pesan,
			 'U_ADD_ACT'=>append_sid('pengaturan_member.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus member
		$list_member = str_replace("\'","'",$HTTP_GET_VARS['list_member']);
		$Member->hapus($list_member);
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		// aksi hapus jadwal
		$id_member = str_replace("\'","'",$HTTP_GET_VARS['id_member']);
	
		$Member->ubahStatusAktif($id_member);
		
		exit;
	} 
	else {
		// LIST
		$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
		$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
		
		$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:"01-01-2010";
		$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
		$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
		$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
		
		$template->set_filenames(array('body' => 'member/member_body.tpl')); 
		
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";
		
		$kondisi	=($cari=="")?"":
			" AND (IdMember LIKE '%$cari%'
			 	OR NoSeriKartu LIKE '%$cari%' 
				OR Nama LIKE '%$cari%' 
				OR Alamat LIKE '%$cari%' 
				OR Handphone LIKE '%$cari%'
				OR Telp LIKE '%$cari%'
				OR Email LIKE '%$cari%'
				OR Pekerjaan LIKE '%$cari%')";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdMember","tbl_md_member","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order","WHERE 1 ".$kondisi,"pengaturan_member.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *,f_member_hitung_frekwensi(IdMember) AS Frekwensi
			FROM tbl_md_member 
			WHERE (TglRegistrasi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi 
			$kondisi_sort LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['FlagAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[IdMember]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[IdMember]\")'>Tidak Aktif</a>";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";

        $act 	="<a href='".append_sid('member.detailtransaksi.'.$phpEx.'?idmember='.$row[0])."' class='b_browse' title='detail transaksi'>&nbsp;</a>&nbsp;|&nbsp;";
				$act .="<a href='".append_sid('pengaturan_member.'.$phpEx.'?mode=edit&id='.$row[0])."' class='b_edit' title='ubah data'>&nbsp;</a>&nbsp;|&nbsp;";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");' class='b_delete' title='hapus data'>&nbsp;</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['Nama'],
							'id_member'=>$row['IdMember'],
                            'ponta'=>$row['NoSeriKartu'],
							'alamat'=>$row['Alamat'],
							'hp'=>$row['Handphone'],
							'email'=>$row['Email'],
							'pekerjaan'=>$row['Pekerjaan'],
							'tgl_daftar'=>dateparseD_Y_M(FormatMySQLDateToTgl($row['TglRegistrasi'])),
              'poin'=>number_format($row['SaldoDeposit'],0,",","."),
							'frekwensi'=>$row['Frekwensi'],
							'aktif'	=>$status,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<div class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></div>";
			}
		} 
		else{
			echo("Error :".__LINE__);exit;
		} 
		
		$order_invert	= ($order=='asc' || $order=='')?'desc':'asc';
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&cari=".$cari."&sort_by=".$sort_by."&order=".$order_invert;
			
		//$script_cetak_pdf="Start('pengaturan_member_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
														
		$script_cetak_excel="Start('pengaturan_member_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		//--END KOMPONEN UNTUK EXPORT
		
		$page_title	= "Data Member";

		$template->assign_vars(array(
			'BCRUMP'    		=>setBcrump($id_page),
			'U_MEMBER_ADD'	=> append_sid('pengaturan_member.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_member.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging,
			'A_SORT_BY_NAMA'			=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Nama&order='.$order_invert),
			'A_SORT_BY_KODE'			=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=IdMember&order='.$order_invert),
			'A_SORT_BY_ALAMAT'		=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Nama&order='.$order_invert),
			'A_SORT_BY_HP'				=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Handphone&order='.$order_invert),
			'A_SORT_BY_EMAIL'			=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Email&order='.$order_invert),
			'A_SORT_BY_PEKERJAAN'	=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Pekerjaan&order='.$order_invert),
			'A_SORT_TGL_DAFTAR'		=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=TglRegistrasi&order='.$order_invert),
			'A_SORT_POIN'		      =>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=SaldoDeposit&order='.$order_invert),
			'A_SORT_BY_FREKWENSI'	=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Frekwensi&order='.$order_invert),
			'A_SORT_BY_STATUS'		=>  append_sid('pengaturan_member.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=FlagAktif&order='.$order_invert),
			'CETAK_XL'						=> $script_cetak_excel
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>