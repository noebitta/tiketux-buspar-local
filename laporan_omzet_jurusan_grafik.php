<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include_once ($adp_root_path. 'chart/php-ofc-library/open_flash_chart_object.php');

// SESSION
$id_page = 303;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$username				= $userdata['username'];

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$bulan					= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$id_jurusan			= isset($HTTP_GET_VARS['id_jurusan'])? $HTTP_GET_VARS['id_jurusan'] : $HTTP_POST_VARS['id_jurusan'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$tahun					= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$temp_tanggal	= explode("-",$tanggal_mulai);

$bulan	=($bulan!='')?$bulan:$temp_tanggal[1];
$tahun	= ($tahun!='')?$tahun:$temp_tanggal[2];

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	//$list_bulan	.="<a href='".append_sid('laporan_omzet_jurusan_grafik.php'.$parameter)."'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
	$list_bulan	.="<a href='#' onClick='setGrafik($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}


// LIST
$template->set_filenames(array('body' => 'laporan_omzet_jurusan/laporan_omzet_jurusan_grafik_body.tpl')); 



if($bulan<=0){
	$bulan	= 1;
	$tahun	= $temp_tanggal[2]-1;
}

$parameter	= "?tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&id_jurusan=".$id_jurusan."&sort_by=".$sort_by."&order=".$order;

$page_title	= "Grafik Penjualan Jurusan";

$template->assign_vars(array(
	'BCRUMP'    						=>setBcrump($id_page),
	'LIST_BULAN'						=> "| ".$list_bulan,
	'TAHUN'									=> $tahun,
	'URL'										=> append_sid('laporan_omzet_jurusan_grafik.php'.$parameter),
	'DATA_GRAFIK_BULANAN_OMZET'		=> append_sid('laporan_omzet_jurusan_grafik_data.php').'%26mode=bulanan%26submode=omzet%26tgl=1-'.$bulan.'-'.$tahun.'%26id_jurusan='.$id_jurusan,
	'DATA_GRAFIK_TAHUNAN_OMZET'		=> append_sid('laporan_omzet_jurusan_grafik_data.php').'%26mode=tahunan%26submode=omzet%26tgl=1-'.$bulan.'-'.$tahun.'%26id_jurusan='.$id_jurusan,
	'DATA_GRAFIK_BULANAN_PROFIT_BIAYA'		=> append_sid('laporan_omzet_jurusan_grafik_data.php').'%26mode=bulanan%26submode=profit_biaya%26tgl=1-'.$bulan.'-'.$tahun.'%26id_jurusan='.$id_jurusan,
	'DATA_GRAFIK_TAHUNAN_PROFIT_BIAYA'		=> append_sid('laporan_omzet_jurusan_grafik_data.php').'%26mode=tahunan%26submode=profit_biaya%26tgl=1-'.$bulan.'-'.$tahun.'%26id_jurusan='.$id_jurusan,
	'DATA_GRAFIK_BULANAN_PRODUKTIFITAS'		=> append_sid('laporan_omzet_jurusan_grafik_data.php').'%26mode=bulanan%26submode=produktifitas%26tgl=1-'.$bulan.'-'.$tahun.'%26id_jurusan='.$id_jurusan,
	'DATA_GRAFIK_TAHUNAN_PRODUKTIFITAS'		=> append_sid('laporan_omzet_jurusan_grafik_data.php').'%26mode=tahunan%26submode=produktifitas%26tgl=1-'.$bulan.'-'.$tahun.'%26id_jurusan='.$id_jurusan
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>