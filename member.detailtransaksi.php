<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');

// SESSION
$id_page = 601;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tglawal'])? $HTTP_GET_VARS['tglawal'] : $HTTP_POST_VARS['tglawal'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$cari           = isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$id_member  	  = isset($HTTP_GET_VARS['idmember'])? $HTTP_GET_VARS['idmember'] : $HTTP_POST_VARS['idmember'];

// LIST
$template->set_filenames(array('body' => 'member/detailtransaksi.tpl'));

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);


$kondisi  = "WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND IdMember='$id_member'";

$kondisi	.=($cari=="")?"":
	" AND (KodeReferensi LIKE '%$cari'
		OR Keterangan LIKE '%$cari%')";
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"1","tbl_member_deposit_trx_log",
"&idmember=$id_member&cari=$cari&tglawal=$tanggal_mulai&tglakhir=$tanggal_akhir",
$kondisi,basename(__FILE__),$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT *
	FROM tbl_member_deposit_trx_log
	$kondisi";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//PLOT DATA
while($row=$db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
  }

  $jumlah  = $row["IsDebit"]?"<font color='red'>DB ".number_format($row["Jumlah"],0,",",".")."</font> ":"<font color='blue'>CR ".number_format($row["Jumlah"],0,",",".")."</font>";

	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'waktu_trx'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
				'kode_reff'=>$row['KodeReferensi'],
        'token'=>$row["Signature"],
				'keterangan'=>$row['Keterangan'],
				'jumlah'=>$jumlah,
				'saldo'=>number_format($row["Saldo"],0,",","."),
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&idmember=".$id_member."&tglawal=".$tanggal_mulai_mysql."&tglakhir=".$tanggal_akhir_mysql."&cari=".$cari;

$script_cetak_excel="Start('member.detailtransaksi.cetakexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$page_title	= "Transaksi Member";

$Member   = new Member();

$data_member  = $Member->ambilData($id_member);

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid(basename(__FILE__)),
  'ID_MEMBER'			=> $id_member,
	'CARI'			    => $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'DATA_MEMBER'		=> $data_member["Nama"]."(".$data_member["IdMember"].")",
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>