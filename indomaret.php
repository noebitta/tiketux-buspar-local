<?php

define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'class.RequestSignature.php');
include($adp_root_path . 'Class.SignatureGenerator.php');
include($adp_root_path . 'ClassLogIndomaret.php');

//INISIALISASI
global $config,$db;
$kode_booking 	= $HTTP_GET_VARS['kode_booking'];
$digit          = 6;
$otp 			= rand(pow(10, $digit-1), pow(10, $digit)-1);
$result         = ambilDataReservasi($kode_booking);
$user_id        = $userdata['userid'];
$date           = date('Y-m-d H:i:s');
$module         = 'Request Payment Code';
$action         = 'POST';
$url            = "/request_paymentcode.php";

$LogIndomaret   = new LogIndomaret();

if($result){
    $row = $db->sql_fetchrow($result);

    if($db->sql_numrows($result) < 1){
        $status     = "ZERO_RESULT";
        $data       = "Error : Sudah Pernah Request!";
        $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
        echo 3;
        exit;
    }

    $booking_name = $config['Merchant']."-".$row['Nama'];
    $user_name    = $row['Nama'];
    $user_phone   = $row['Telp'];
    $biaya_admin  = 2500;
    $total        = $row['Total']-0+$biaya_admin;
    $email        = empty($email) ? "info@tiketux.com" : $email;

    $sigGenerator		= new SignatureGenerator();
    $auth 				= $config['key']. '&' . $config['secret'];
    $keyId				= $sigGenerator->genKeyId($auth);

    $genSignature		= $sigGenerator->genSignature($config['Merchant'], $kode_booking, $row['WaktuPesan'], $total, $user_phone, $keyId);

    $reqSignature	= new RequestSignature();
    $params 		= array();

    //Api Parameter
    $params['auth_nonce']	  	= $reqSignature->createNonce(true);
    $params['auth_timestamp'] 	= time();
    $params['auth_client_id']	= $config['client_id'];

    $params['merchant'] 		= $config['Merchant'];
    $params['order_id'] 		= $kode_booking;
    $params['order_name'] 		= $booking_name;
    $params['order_user_name'] 	= $user_name;
    $params['order_user_phone'] = $user_phone;
    $params['order_user_email'] = $email;
    $params['order_time']	 	= $row['WaktuPesan'];
    $params['order_price']      = $row['HargaTiket'];
    $params['order_discount']   = 0;
    $params['order_admin_fee']  = $biaya_admin;
    $params['order_amount']	 	= $total;
    $params['return_url'] 		= $config['return_url_indomaret'];
    $params['signature'] 		= $genSignature;
    $params['otp']              = $otp;

    $accessToken				= $config['client_id'];
    $accessTokenSecret			= $config['client_secret'];

    if (!empty($accessToken)) {
        $params['auth_access_token'] = $accessToken;
        $key 	= $accessToken;
        $secret = $accessTokenSecret;
    } else {
        $key 	= $config['client_id'];
        $secret = $config['client_secret'];
    }

    $baseSignature = $reqSignature->createSignatureBase("POST", $config['api_url']."paymentmc/indomaret.json", $params);
    $signature     = $reqSignature->createSignature($baseSignature, $key, $secret);

    $pay = sendHttpPost($config['api_url'] ."paymentmc/indomaret.json",$reqSignature->normalizeParams($params).'&auth_signature=' .$signature);

    $objResquestPayment = json_decode($pay);

    if($objResquestPayment->tiketux->status == 'OK'){
        $payment_code = $objResquestPayment->tiketux->results->payment_code;
        $expired_time = $objResquestPayment->tiketux->results->payment_expired_time;

        //Update Payment Code
        $updatePaymentCode = updatePaymentCodeByKodeBooking($kode_booking,$payment_code,$expired_time,$genSignature,$otp);
        if($updatePaymentCode){
            // Write Finpay Log
            //$module         = 'Reservasi';
            //$action         = 'POST';
            $url            = $objResquestPayment->tiketux->url;
            $status         = $objResquestPayment->tiketux->status;
            $time           = $objResquestPayment->tiketux->time;
            $data           = 'Sukses: Update Payment Code Kode Booking : '.$kode_booking. ' PaymentCode : '.$payment_code;
            //$user_id        = $userdata['userid'];

            $LogIndomaret->writeLog($time,$module,$action,$url,$status,$data,$user_id);
            //echo $kode_booking.",".$time.",".$status."<br />";
            echo 1;
        }else{
            // Write Finpay Log
            //$module         = 'Reservasi';
            //$action         = 'POST';
            $url            = $objResquestPayment->tiketux->url;
            $status         = 'ZERO_RESULT';
            $data           = 'Error: Gagal Update Payment Code Kode Booking : '.$kode_booking;
            //$user_id        = $userdata['userid'];

            $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
            echo 2;
        }

    }
    else{
        // Write Finpay Log
        //$module         = 'Reservasi';
        //$action         = 'POST';
        $url            = $objResquestPayment->tiketux->url;
        $status         = $objResquestPayment->tiketux->status;
        $error 			= $objResquestPayment->tiketux->error;
        $data           = $pay;

        $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
        echo 3;
    }
}else{
    $status     = "ZERO_RESULT";
    $data       = "Error : Data Not Found!";
    $LogIndomaret->writeLog($date,$module,$action,$url,$status,$data,$user_id);
    echo 0;
}

function ambilDataReservasi($kode_booking){
    global $db;

    $sql =
        "SELECT *, SUM(Total) as TotalHarga FROM tbl_reservasi WHERE KodeBooking='$kode_booking' AND PaymentCode='' AND CetakTiket = 0 AND FlagBatal != 1 GROUP BY KodeBooking";

    if (!$result = $db->sql_query($sql)){
        $result =  false;
    }

    return $result;
}

function updatePaymentCodeByKodeBooking($kode_booking,$payment_code,$expired_time,$genSignature,$otp){

    global $db;

    $sql = "UPDATE tbl_reservasi SET JenisPembayaran=7, PaymentCode='$payment_code',PaymentCodeExpiredTime='$expired_time', OTP='$otp'
                WHERE KodeBooking='$kode_booking';";

    if(!$db->sql_query($sql)){
        //die_error("Err $this->ID_FILE".__LINE__);
    }

    return true;
}