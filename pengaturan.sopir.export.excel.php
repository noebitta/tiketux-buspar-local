<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$id_page = 705;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE
require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$cari       = getVariabel("cari");
$order_by   = getVariabel("orderby");
$sort       = getVariabel("sort");


//INIT
$tgl_sekarang = dateparse(date("d-m-Y"));

//PROCESS
$Sopir  = new Sopir();

$styleborder = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);


//EXPORT KE MS-EXCEL

$i=1;
$col_map  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
            "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ");

$objPHPExcel = new PHPExcel();

// SHEET 1
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("Sheet1");

//HEADER
$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Master Data Sopir per Tanggal '.$tgl_sekarang);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'NO');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'NAMA');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'NRP');
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'ALAMAT');
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'HP');
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'NO SIM');
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'JENIS SIM');
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'SIM EXPIRED');
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'KOTA LAHIR');
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'TANGGAL LAHIR');
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'NO REKENING');
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'NAMA BANK');
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'CABANG BANK');
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'KODE CABANG');
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'STATUS SOPIR');
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'STATUS AKTIF');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);



$result = $Sopir->ambilData($order_by,$sort,"",$cari);

$baris = 3;
$no = 0;

while($data = $db->sql_fetchrow($result)){
  $no++;
  $baris++;
  $idx_col=0;

  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$no);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["Nama"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["KodeSopir"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["Alamat"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["HP"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["NoSIM"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["JenisSIM"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,dateparse(FormatTglToMySQLDate($data['SIMExpired'])));$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["KotaLahir"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,dateparse(FormatTglToMySQLDate($data['TglLahir'])));$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["Norek"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["NamaBank"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["CabangBank"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["KodeCabang"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["StatusReguler"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["StatusAktif"]);$idx_col++;


  //$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);

}

$objPHPExcel->getActiveSheet()->getStyle("A3:P".($baris))->applyFromArray($styleborder);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Master Data Sopir per Tanggal '.$tgl_sekarang.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>
