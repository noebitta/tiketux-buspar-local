<?php
$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!="" && !$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true);
	exit; 
}
//#############################################################################

class Jurusan{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Jurusan(){
		$this->ID_FILE="C-JRS";
	}
	
	//BODY

	//data layout
	function dataLayout(){
		global $db;
		$sql =
			"SELECT IdLayout,KodeLayout FROM tbl_md_kendaraan_layout ORDER BY IdLayout";

		if ($result = $db->sql_query($sql)){
			return $result;
		}
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Error :".$this->ID_FILE. __LINE__);
		}
	}

	
	function periksaDuplikasi($kode_jurusan){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT f_jurusan_periksa_duplikasi('$kode_jurusan') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi


    function tambah1(
        $kode_jurusan,
        $kode_cabang_asal,
        $kode_cabang_tujuan,
        $estimasi_waktu_tempuh,
        $harga_tiket,
        $biaya_sopir,
        $biaya_tol,
        $biaya_tol_extra,
        $biaya_parkir,
        $bbm_liter,
        $bbm_liter_ext,
        $bbm_rupiah,
        $bbm_rupiah_ext,
        $flag_aktif,
        $flag_luar_kota,
        $harga_paket_kilo_pertama,
        $harga_paket_kilo_berikut,
        $flag_op_jurusan,
        $flag_biaya_sopir_kumulatif,
        $flag_biaya_voucher_bbm,
        $lokasi_spbu){

        /*
        ID	: 002
        IS	: data jurusan belum ada dalam database
        FS	:Data jurusan baru telah disimpan dalam database
        */

        //kamus
        global $db;

        //MENAMBAHKAN DATA KEDALAM DATABASE

        $sql	=
            "INSERT INTO tbl_md_jurusan (
				 KodeJurusan, KodeCabangAsal, KodeCabangTujuan, EstimasiWaktuTempuh, HargaTiket,
				 HargaPaketPertama,HargaPaketBerikut,
				 BiayaSopir,
				 BiayaTol,
				 BiayaTolExtra,
				 BiayaParkir,
				LiterBBM,LiterBBMExtra, RupiahBBM, RupiahBBMExtra, IsAktif,FlagLuarKota,
				 FlagOperasionalJurusan,IsBiayaSopirKumulatif,
				 IsBBMVoucher,SPBU)
			VALUES (
				'$kode_jurusan',
				'$kode_cabang_asal',
				'$kode_cabang_tujuan',
			    '$estimasi_waktu_tempuh',
			    '$harga_tiket',
			    '$harga_paket_kilo_pertama',
			    '$harga_paket_kilo_berikut',
			    '$biaya_sopir',
			    '$biaya_tol',
			    '$biaya_tol_extra',
			    '$biaya_parkir',
			    '$bbm_liter',
			    '$bbm_liter_ext',
			    '$bbm_rupiah',
			    '$bbm_rupiah_ext',
			    '$flag_aktif',
			    '$flag_luar_kota',
				'$flag_op_jurusan',
				'$flag_biaya_sopir_kumulatif',
				'$flag_biaya_voucher_bbm',
				'$lokasi_spbu')";

        if (!$db->sql_query($sql)){
            die_error("Err:$this->ID_FILE".__LINE__);
        }

        return true;
    }

	function  tambah2(
	    $kode_jurusan, $kode_cabang_asal,
        $kode_cabang_tujuan, $harga_tiket,
        $bbm_liter, $bbm_liter_ext,
        $bbm_rupiah, $bbm_rupiah_ext, $flag_aktif, $flag_op_jurusan, $biaya_sopir, $biaya_tol
    ){
		global $db;

		//MENAMBAHKAN DATA KEDALAM DATABASE

		$sql	= "INSERT INTO tbl_md_jurusan_test (KodeJurusan, KodeCabangAsal, KodeCabangTujuan,HargaTiket, LiterBBM, LiterBBMExtra, RupiahBBM, RupiahBBMExtra, IsAktif, FlagOperasionalJurusan, BiayaSopir, BiayaTol) VALUES ('$kode_jurusan','$kode_cabang_asal','$kode_cabang_tujuan', '$harga_tiket','$bbm_liter', '$bbm_liter_ext', '$bbm_rupiah', '$bbm_rupiah_ext','$flag_aktif','$flag_op_jurusan','$biaya_sopir','$biaya_tol')";
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}

		return true;
	}

	function tambah(
		$kode_jurusan,
        $kode_cabang_asal,
        $kode_cabang_tujuan,
		$harga_tiket,
		$kode_akun_pendapatan_penumpang,
        $kode_akun_pendapatan_paket,
		$kode_akun_charge,
        $kode_akun_biaya_sopir,$biaya_sopir,
        $kode_akun_biaya_tol,
		$biaya_tol,
        $kode_akun_biaya_parkir,
        $biaya_parkir,
		$kode_akun_biaya_bbm,
        $biaya_bbm,
		$kode_akun_komisi_penumpang_sopir,
		$komisi_penumpang_sopir,$kode_akun_komisi_penumpang_cso,$komisi_penumpang_cso,
		$kode_akun_komisi_paket_sopir,$komisi_paket_sopir,$kode_akun_komisi_paket_cso,
		$komisi_paket_cso,$flag_aktif,$flag_luar_kota,
		$harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
		$harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
		$harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
		$harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
		$harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
		$harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
		$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$liter_bbm,$liter_bbm2,$lokasi_spbu){
	  
		/*
		ID	: 002
		IS	: data jurusan belum ada dalam database
		FS	:Data jurusan baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$sql	=
			"INSERT INTO tbl_md_jurusan (
				 KodeJurusan, KodeCabangAsal, KodeCabangTujuan, HargaTiket,
				 HargaTiketTuslah, FlagTiketTuslah,KodeAkunPendapatanPenumpang, KodeAkunPendapatanPaket,
				 KodeAkunCharge, KodeAkunBiayaSopir, BiayaSopir, KodeAkunKomisiPenumpangSopir,
				 KomisiPenumpangSopir, KodeAkunKomisiPenumpangCSO, KomisiPenumpangCSO, KodeAkunKomisiPaketSopir,
				 KomisiPaketSopir, KodeAkunKomisiPaketCSO, KomisiPaketCSO,FlagAktif,
				 KodeAkunBiayaTol,BiayaTol,
				 KodeAkunBiayaParkir,BiayaParkir,
				 KodeAkunBiayaBBM,BiayaBBM,FlagLuarKota,
				 HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
				 HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
				 HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
				 HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
				 HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
				 HargaPaket6KiloPertama,HargaPaket6KiloBerikut,
				 FlagOperasionalJurusan,IsBiayaSopirKumulatif,
				 IsVoucherBBM,LiterBBM,LiterBBMExt,LokasiSPBU)
			VALUES (
				'$kode_jurusan', '$kode_cabang_asal','$kode_cabang_tujuan',
			  $harga_tiket,
			  '$kode_akun_pendapatan_penumpang',
			  '$kode_akun_pendapatan_paket',
			  '$kode_akun_charge',
			  '$kode_akun_biaya_sopir',
			  '$biaya_sopir',
			  '$kode_akun_biaya_tol',
			  '$biaya_tol',
			  '$kode_akun_biaya_parkir',
			  '$biaya_parkir',
				'$kode_akun_biaya_bbm',
			  '$biaya_bbm',
			  '$kode_akun_komisi_penumpang_sopir',
			  '$komisi_penumpang_sopir',
			  '$kode_akun_komisi_penumpang_cso',
			  '$komisi_penumpang_cso',
			  '$kode_akun_komisi_paket_sopir',
			  '$komisi_paket_sopir',
			  '$kode_akun_komisi_paket_cso',
			  '$komisi_paket_cso',
			  $flag_aktif,$flag_luar_kota,
				'$harga_paket_1_kilo_pertama','$harga_paket_1_kilo_berikut',
				'$harga_paket_2_kilo_pertama','$harga_paket_2_kilo_berikut',
				'$harga_paket_3_kilo_pertama','$harga_paket_3_kilo_berikut',
				'$harga_paket_4_kilo_pertama','$harga_paket_4_kilo_berikut',
				'$harga_paket_5_kilo_pertama','$harga_paket_5_kilo_berikut',
				'$harga_paket_6_kilo_pertama','$harga_paket_6_kilo_berikut',
				$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,'$liter_bbm','$liter_bbm2','$lokasi_spbu')";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *,f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabangAsal,f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan, IF(IsAktif=1, 'AKTIF', 'NON-AKTIF') As Aktif
			FROM tbl_md_jurusan
			WHERE 
				KodeJurusan LIKE '$pencari' 
				OR KodeCabangAsal LIKE '%$pencari%' 
				OR KodeCabangTujuan LIKE '%$pencari%'
			$order;";
				
		if ($result = $db->sql_query($sql)){

			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Error :".$this->ID_FILE. __LINE__);
		}
		
	}
	//  END ambilData
	
	function ubah(
        $id_jurusan,
        $kode,
        $asal,
        $tujuan,
        $estimasi_waktu_tempuh,
        $harga_tiket,
        $biaya_sopir,
        $biaya_tol,
        $biaya_tol_extra,
        $biaya_parkir,
        $bbm_liter,
        $bbm_liter_ext,
        $bbm_rupiah,
        $bbm_rupiah_ext,
        $flag_aktif,
        $flag_jenis,
        $harga_paket_kilo_pertama,
        $harga_paket_kilo_berikut,
        $flag_op_jurusan,
        $flag_biaya_sopir_kumulatif,
        $flag_biaya_voucher_bbm,
        $lokasi_spbu){
	  
		/*
		ID	: 004
		IS	: data jurusan sudah ada dalam database
		FS	:Data jurusan diubah 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE

		$sql = "UPDATE tbl_md_jurusan SET
				 KodeJurusan='$kode',
				 KodeCabangAsal='$asal',
				 KodeCabangTujuan='$tujuan',
				 EstimasiWaktuTempuh='$estimasi_waktu_tempuh',
				 HargaTiket='$harga_tiket',
				 HargaPaketPertama='$harga_paket_kilo_pertama',
				 HargaPaketBerikut='$harga_paket_kilo_berikut',
				 BiayaSopir='$biaya_sopir',
				 IsAktif='$flag_aktif',
				 BiayaTol='$biaya_tol',
				 BiayaTolExtra='$biaya_tol_extra',
				 BiayaParkir='$biaya_parkir',
				 LiterBBM='$bbm_liter',
				 LiterBBMExtra='$bbm_liter_ext',
				 RupiahBBM='$bbm_rupiah',
				 RupiahBBMExtra='$bbm_rupiah_ext',
				 FlagLuarKota='$flag_jenis',
				 
				 FlagOperasionalJurusan='$flag_op_jurusan',
				 IsBiayaSopirKumulatif='$flag_biaya_sopir_kumulatif',
				 IsBBMVoucher='$flag_biaya_voucher_bbm',
				 SPBU = '$lokasi_spbu'
			   WHERE IdJurusan=$id_jurusan;";

		if (!$db->sql_query($sql)){
		    die($sql);
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_jurusan){
	  
		/*
		ID	: 005
		IS	: data member sudah ada dalam database
		FS	:Data member dihapus
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_jurusan
			WHERE IdJurusan IN($list_jurusan);";

		if (!$db->sql_query($sql)){
			echo ("Err: $this->ID_FILE".__LINE__);
			return false;
			
		}
		
		return true;
	}//end hapus
	
	function ambilHargaTiket(){
		global $db;
		$sql = "SELECT HargaTiket AS harga FROM tbl_md_jurusan";
		
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		}
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
	}
	
	function ambilDataDetail($id_jurusan){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *,f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabangAsal,f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilData
	
	function ambilDataByKodeCabangAsal($cabang_asal){
		
		/*
		ID	:008
		Desc	:Mengembalikan data jurusan sesuai dengan kode asal
		*/
		
		//kamus
		global $db;
	
		$sql = 
			"SELECT IdJurusan, KodeJurusan, KodeCabangTujuan, f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan
			WHERE KodeCabangAsal LIKE '$cabang_asal' ORDER BY NamaCabangTujuan";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Error :".$this->ID_FILE. __LINE__);
		}
		
	}//  END ambilData

    function ubahStatusAktif($id_jurusan){

//kamus
        global $db;

        $sql =
            "UPDATE tbl_md_jurusan SET
IsAktif=1-IsAktif
WHERE IdJurusan='$id_jurusan';";

        if (!$db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__);exit;
        }

        return true;
    }//end ubahStatus
	
	function ubahStatusTuslah($id,$tuslah){
	  
		/*
		ID	: 010
		IS	: data jurusan sudah ada dalam database
		FS	: Status jurusan diubah 
		*/
		
		//kamus
		global $db;
		
		//MENGUBAH STATUS TUSLAH JURUSAN
		
		$sql =
			"CALL sp_jurusan_ubah_tuslah($id,$tuslah);";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubah tuslah
	
	function setComboJurusan($cabang_asal,$jenis_op_jurusan="0,2"){
		
		/*
		Desc	:Mengembalikan data Cabang sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$kondisi_cabang_asal	= ($cabang_asal!="")?" AND KodeCabangAsal='$cabang_asal' AND FlagAktif=1":"";
		
		$sql = 
			"SELECT IdJurusan, f_reservasi_cabang_get_name_by_kode(KodeCabangTujuan) AS Tujuan,KodeJurusan
			FROM tbl_md_jurusan
			WHERE FlagOperasionalJurusan IN($jenis_op_jurusan) $kondisi_cabang_asal
			ORDER BY Tujuan;";
			
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err: $this->ID_FILE". __LINE__);
		}
		
	}//  END ambilData
	
	function ambilDataByCabangAsal($cabang_asal){
		
		/*
		Desc	:Mengembalikan data cabang sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
	
		$sql = 
			"SELECT IdJurusan, KodeJurusan, KodeCabangTujuan, f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan
			FROM tbl_md_jurusan
			WHERE KodeCabangAsal LIKE '$cabang_asal' ORDER BY NamaCabangTujuan";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err:".$this->ID_FILE. __LINE__);
		}
		
	}//  END ambilData
	
	function ambilNamaJurusanByIdJurusan($id_jurusan){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT KodeCabangAsal,KodeCabangTujuan
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$kode_cabang_asal		= $row['KodeCabangAsal'];
			$kode_cabang_tujuan	= $row['KodeCabangTujuan'];
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
		$sql = 
			"SELECT Nama
			FROM tbl_md_cabang
			WHERE KodeCabang='$kode_cabang_asal';";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			$nama_cabang_asal		= $row['Nama'];
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
		$sql = 
			"SELECT Nama
			FROM tbl_md_cabang
			WHERE KodeCabang='$kode_cabang_tujuan';";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			$nama_cabang_tujuan		= $row['Nama'];
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
		$jurusan['Asal']		= $nama_cabang_asal;
		$jurusan['Tujuan']	= $nama_cabang_tujuan;
		
		return $jurusan;
		
	}//  END ambilNamaJurusanByIdJurusan

  function ambilBiaya($id_jurusan){

    //kamus
    global $db;

    $sql =
      "SELECT *
			FROM tbl_md_jurusan
			WHERE
				KodeJurusan='$id_jurusan';";

    if (!$result = $db->sql_query($sql)){
      echo("Error :".$this->ID_FILE. __LINE__);
    }

    $row=$db->sql_fetchrow($result);

    return $row;

  }//  END ambilBiaya


	function setComboListCabang($cari="",$cabang_asal=""){

		//kamus
		global $db;
		$kondisi_tambahan = $cabang_asal==""?"":" AND KodeCabang != '$cabang_asal'";
		$sql =
		 "SELECT KodeCabang,Nama,Kota
		  FROM tbl_md_cabang
		  WHERE (Nama LIKE '%$cari%' OR KodeCabang LIKE '$cari%') AND FlagAgen!=1 $kondisi_tambahan
		  ORDER BY Kota,Nama;";

		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE". __LINE__);exit;
		}

		return $result;

	}//
}
?>