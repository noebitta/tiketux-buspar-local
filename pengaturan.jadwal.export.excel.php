<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJadwal.php');

// SESSION
$id_page = 703;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE
require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$cari       = getVariabel("cari");
$order_by   = getVariabel("orderby");
$sort       = getVariabel("sort");


//INIT
$tgl_sekarang = dateparse(date("d-m-Y"));

//PROCESS
$Jadwal  = new Jadwal();

$styleborder = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);


//EXPORT KE MS-EXCEL

$i=1;
$col_map  = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
            "AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ");

$objPHPExcel = new PHPExcel();

// SHEET 1
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("Sheet1");

//HEADER
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Master Data Jadwal per Tanggal '.$tgl_sekarang);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'NO');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'KODE JADWAL');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'JENIS JADWAL');
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'JURUSAN');
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'JAM BERANGKAT');
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'LAYOUT');
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'AKTIF');
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'ONLINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

$objPHPExcel->getActiveSheet()->getStyle('F')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->freezePane('C4');

$result = $Jadwal->ambilData($order_by,$sort,"",$cari);

$baris = 3;
$no = 0;

while($data = $db->sql_fetchrow($result)){
  $no++;
  $baris++;
  $idx_col=0;

  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$no);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["KodeJadwal"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["JenisJadwal"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["Jurusan"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["JamBerangkat"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["JenisLayout"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["StatusAktif"]);$idx_col++;
  $objPHPExcel->getActiveSheet()->setCellValue($col_map[$idx_col].$baris,$data["StatusOnline"]);$idx_col++;

  //$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);

}

$objPHPExcel->getActiveSheet()->getStyle("A3:H".($baris))->applyFromArray($styleborder);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Master Data Jadwal per Tanggal '.$tgl_sekarang.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

?>
