<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 702;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Jurusan	= new Jurusan();
$Cabang		= new Cabang();

include($adp_root_path . 'ClassMobil.php');
$Mobil	= new Mobil();

function setComboCabang($cabang_dipilih){
	//SET COMBO jurusan
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO JURUSAN
}


	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}

		$data_layout	= $Mobil->getArrayLayout();

		foreach($data_layout as $layout_id	=> $layout_value){
			$template->assign_block_vars("LISTBBM",array(
					"idlayout"	=> $layout_id,
					"literbbm"	=> "")
			);
		}
		
		$template->set_filenames(array('body' => 'jurusan/add_body.tpl'));

		$template->assign_vars(array(
			'BCRUMP'		        =>setBcrump($id_page),
			'JUDUL'			        => 'Tambah Data Jurusan',
			'MODE'   		        => 'save',
			'SUB'    		        => '0',
			'OPT_ASAL'          => setComboCabang(''),
			'OPT_TUJUAN'        => setComboCabang(''),
			'PESAN'						  => $pesan,
		  'BGCOLOR_PESAN'		  => $bgcolor_pesan,
			'U_JURUSAN_ADD_ACT'	=> append_sid('pengaturan_jurusan.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		
		// aksi menambah jurusan
		$id_jurusan  											= $HTTP_POST_VARS['id_jurusan'];
		$kode  														= str_replace(" ","",$HTTP_POST_VARS['kode']);
		$kode_jurusan_old									= str_replace(" ","",$HTTP_POST_VARS['kode_jurusan_old']);
		$asal   													= $HTTP_POST_VARS['asal'];
		$tujuan   												= $HTTP_POST_VARS['tujuan'];
		$harga_tiket											= $HTTP_POST_VARS['harga_tiket'];
		$harga_tiket_tuslah 							= $HTTP_POST_VARS['harga_tiket_tuslah'];
		$harga_paket_1_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_1_kilo_pertama'];
		$harga_paket_1_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_1_kilo_berikut'];
		$harga_paket_2_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_2_kilo_pertama'];
		$harga_paket_2_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_2_kilo_berikut'];
		$harga_paket_3_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_3_kilo_pertama'];
		$harga_paket_3_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_3_kilo_berikut'];
		$harga_paket_4_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_4_kilo_pertama'];
		$harga_paket_4_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_4_kilo_berikut'];
		$harga_paket_5_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_5_kilo_pertama'];
		$harga_paket_5_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_5_kilo_berikut'];
		$harga_paket_6_kilo_pertama 			= $HTTP_POST_VARS['harga_paket_6_kilo_pertama'];
		$harga_paket_6_kilo_berikut 			= $HTTP_POST_VARS['harga_paket_6_kilo_berikut'];
		$flag_tuslah											= $HTTP_POST_VARS['flag_tuslah'];
		$flag_aktif												= $HTTP_POST_VARS['flag_aktif'];
		$flag_jenis												= $HTTP_POST_VARS['flag_jenis'];
		$flag_op_jurusan									= $HTTP_POST_VARS['flag_op_jurusan'];
		$biaya_sopir											= $HTTP_POST_VARS['biaya_sopir'];
		$flag_biaya_sopir_kumulatif				= $HTTP_POST_VARS['flag_biaya_sopir_kumulatif']!='on'?0:1;
		$biaya_tol												= $HTTP_POST_VARS['biaya_tol'];
		$biaya_parkir											= $HTTP_POST_VARS['biaya_parkir'];
		$biaya_bbm												= $HTTP_POST_VARS['biaya_bbm'];
		$flag_biaya_voucher_bbm						= $HTTP_POST_VARS['flag_biaya_voucher_bbm']!='on'?0:1;
		$komisi_penumpang_sopir						= $HTTP_POST_VARS['komisi_penumpang_sopir'];
		$komisi_penumpang_cso							= $HTTP_POST_VARS['komisi_penumpang_cso'];
		$komisi_paket_sopir								= $HTTP_POST_VARS['komisi_paket_sopir'];
		$komisi_paket_cso									= $HTTP_POST_VARS['komisi_paket_cso'];
		$kode_akun_pendapatan_penumpang		= $HTTP_POST_VARS['kode_akun_pendapatan_penumpang'];
		$kode_akun_pendapatan_paket				= $HTTP_POST_VARS['kode_akun_pendapatan_paket'];
		$kode_akun_biaya_sopir						= $HTTP_POST_VARS['kode_akun_biaya_sopir'];
		$kode_akun_biaya_tol							= $HTTP_POST_VARS['kode_akun_biaya_tol'];
		$kode_akun_biaya_parkir						= $HTTP_POST_VARS['kode_akun_biaya_parkir'];
		$kode_akun_biaya_bbm							= $HTTP_POST_VARS['kode_akun_biaya_bbm'];
		$kode_akun_komisi_penumpang_sopir	= $HTTP_POST_VARS['kode_akun_komisi_penumpang_sopir'];
		$kode_akun_komisi_penumpang_cso		= $HTTP_POST_VARS['kode_akun_komisi_penumpang_cso'];
		$kode_akun_komisi_paket_sopir			= $HTTP_POST_VARS['kode_akun_komisi_paket_sopir'];
		$kode_akun_komisi_paket_cso				= $HTTP_POST_VARS['kode_akun_komisi_paket_cso'];
		$kode_akun_charge									= $HTTP_POST_VARS['kode_akun_charge'];
		$lokasi_spbu                            = $HTTP_POST_VARS['spbu'];

		$terjadi_error=false;

		$data_layout	= $Mobil->getArrayLayout();
		
		if($Jurusan->periksaDuplikasi($kode) && $kode!=$kode_jurusan_old){
			$pesan="<font color='white' size=3>Kode jurusan yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($flag_op_jurusan==1){
				$harga_tiket=0;
				$harga_tiket_tuslah=0;
			}

			$liter_bbm	= "";

			foreach($data_layout as $layout_id	=> $layout_value){
				$liter_bbm	.= $layout_id."=".$HTTP_POST_VARS["biayabbm".$layout_id].";";
				$liter_bbm2	.= $layout_id."=".$HTTP_POST_VARS["biayabbmext".$layout_id].";";
			}
			
			if($submode==0){
				$judul="Tambah Data Jurusan";

				if($Jurusan->tambah(
					$kode,$asal,$tujuan,
					$harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
					$kode_akun_pendapatan_penumpang,$kode_akun_pendapatan_paket,
					$kode_akun_charge,$kode_akun_biaya_sopir,$biaya_sopir,$kode_akun_biaya_tol,
					$biaya_tol,$kode_akun_biaya_parkir,$biaya_parkir,
					$kode_akun_biaya_bbm,$biaya_bbm,$kode_akun_komisi_penumpang_sopir,
					$komisi_penumpang_sopir,$kode_akun_komisi_penumpang_cso,$komisi_penumpang_cso,
					$kode_akun_komisi_paket_sopir,$komisi_paket_sopir,$kode_akun_komisi_paket_cso,
					$komisi_paket_cso,$flag_aktif,$flag_jenis,
					$harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
					$harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
					$harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
					$harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
					$harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
					$harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
					$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$liter_bbm,$liter_bbm2,$lokasi_spbu)){
						
					redirect(append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
					
				}
			}
			else{
				
				$judul="Ubah Data Jurusan";

				if($Jurusan->ubah(
					$id_jurusan,
					$kode,$asal,$tujuan,
					$harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
					$kode_akun_pendapatan_penumpang,$kode_akun_pendapatan_paket,
					$kode_akun_charge,$kode_akun_biaya_sopir,$biaya_sopir,$kode_akun_biaya_tol,
					$biaya_tol,$kode_akun_biaya_parkir,$biaya_parkir,
					$kode_akun_biaya_bbm,$biaya_bbm,$kode_akun_komisi_penumpang_sopir,
					$komisi_penumpang_sopir,$kode_akun_komisi_penumpang_cso,$komisi_penumpang_cso,
					$kode_akun_komisi_paket_sopir,$komisi_paket_sopir,$kode_akun_komisi_paket_cso,
					$komisi_paket_cso,$flag_aktif,$flag_jenis,
					$harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
					$harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
					$harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
					$harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
					$harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
					$harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
					$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$liter_bbm,$liter_bbm2,$lokasi_spbu)){
						
					//redirect(append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add',true));
					//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jurusan.'.$phpEx.'?mode=edit&id='.$id_jurusan).'">Sini</a> Untuk Melanjutkan','');
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}
			
			//exit;
			
		}
		
		$temp_var_tuslah="tuslah_".$flag_tuslah;
		$$temp_var_tuslah="selected";
		
		$temp_var_aktif="aktif_".$flag_aktif;
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="jenis_".$flag_jenis;
		$$temp_var_aktif="selected";
		
		$temp_var_sub_jurusan	="sub_jurusan_".$flag_sub_jurusan;
		$$temp_var_sub_jurusan="selected";
		
		$temp_var_op_jurusan	="op_".$flag_op_jurusan;
		$$temp_var_op_jurusan	="selected";

		$temp_var_lokasi_spbu   ="spbu_".$lokasi_spbu;
		$$temp_var_lokasi_spbu  ="selected";

		foreach($data_layout as $layout_id	=> $layout_value){
			$template->assign_block_vars("LISTBBM",array(
					"idlayout"	=> $layout_id,
					"literbbm"	=> $HTTP_POST_VARS["biayabbm".$layout_id],
					"literbbm2"	=> $HTTP_POST_VARS["biayabbmext".$layout_id])
			);
		}
		
			$template->set_filenames(array('body' => 'jurusan/add_body.tpl')); 
			$template->assign_vars(array(
			 'BCRUMP'		                  =>setBcrump($id_page),
			 'JUDUL'		                  =>$judul,
			 'MODE'   	                  => 'save',
			 'SUB'    	                  => $submode,
			 'ID_JURUSAN'				          => $id_jurusan,
			 'KODE_JURUSAN_OLD'	          => $kode,
			 'KODE_JURUSAN'    	          => $kode,
			 'OPT_ASAL'    			          => setComboCabang($asal),
			 'OPT_TUJUAN'   		          => setComboCabang($tujuan),
			 'HARGA_TIKET'			          => $harga_tiket,
			 'HARGA_TIKET_TUSLAH'         => $harga_tiket_tuslah,
			 'HARGA_PAKET_1_KILO_PERTAMA'	=> $harga_paket_1_kilo_pertama,
			 'HARGA_PAKET_1_KILO_BERIKUT'	=> $harga_paket_1_kilo_berikut,
			 'HARGA_PAKET_2_KILO_PERTAMA' => $harga_paket_2_kilo_pertama,
			 'HARGA_PAKET_2_KILO_BERIKUT' => $harga_paket_2_kilo_berikut,
			 'HARGA_PAKET_3_KILO_PERTAMA' => $harga_paket_3_kilo_pertama,
			 'HARGA_PAKET_3_KILO_BERIKUT' => $harga_paket_3_kilo_berikut,
			 'HARGA_PAKET_4_KILO_PERTAMA'	=> $harga_paket_4_kilo_pertama,
			 'HARGA_PAKET_4_KILO_BERIKUT'	=> $harga_paket_4_kilo_berikut,
			 'HARGA_PAKET_5_KILO_PERTAMA'	=> $harga_paket_5_kilo_pertama,
			 'HARGA_PAKET_5_KILO_BERIKUT'	=> $harga_paket_5_kilo_berikut,
			 'HARGA_PAKET_6_KILO_PERTAMA'	=> $harga_paket_6_kilo_pertama,
			 'HARGA_PAKET_6_KILO_BERIKUT'	=> $harga_paket_6_kilo_berikut,
			 'TUSLAH_0'					          => $tuslah_0,
			 'TUSLAH_1'					          => $tuslah_1,
			 'AKTIF_0'					          => $aktif_0,
			 'AKTIF_1'					          => $aktif_1,
			 'JENIS_0'					          => $jenis_0,
			 'JENIS_1'					          => $jenis_1,
			 'OP_0'							          => $op_0,
			 'OP_1'							          => $op_1,
			 'OP_2'							          => $op_2,
			 'SPBU_1'                     => $spbu_1,
			 'SPBU_2'                     => $spbu_2,
			 'BIAYA_SOPIR'			          => $biaya_sopir,
			 'FLAG_BIAYA_SOPIR_KUMULATIF'	=> $flag_biaya_sopir_kumulatif!=1?"":"checked",
			 'BIAYA_TOL'				          => $biaya_tol,
			 'BIAYA_PARKIR'			          => $biaya_parkir,
			 'BIAYA_BBM'				          => $biaya_bbm,
			 'FLAG_BIAYA_VOUCHER_BBM'	    => $flag_biaya_voucher_bbm!=1?"":"checked",
			 'KOMISI_PENUMPANG_SOPIR'					=> $komisi_penumpang_sopir,
			 'KOMISI_PENUMPANG_CSO'						=> $komisi_penumpang_cso,
			 'KOMISI_PAKET_SOPIR'							=> $komisi_paket_sopir,
			 'KOMISI_PAKET_CSO'								=> $komisi_paket_cso,
			 'KODE_AKUN_PENDAPATAN_PENUMPANG'	=> $kode_akun_pendapatan_penumpang,
			 'KODE_AKUN_PENDAPATAN_PAKET'			=> $kode_akun_pendapatan_paket,
			 'KODE_AKUN_BIAYA_SOPIR'					=> $kode_akun_biaya_sopir,
			 'KODE_AKUN_BIAYA_TOL'						=> $kode_akun_biaya_tol,
			 'KODE_AKUN_BIAYA_PARKIR'					=> $kode_akun_biaya_parkir,
			 'KODE_AKUN_BIAYA_BBM'						=> $kode_akun_biaya_bbm,
			 'KODE_AKUN_KOMISI_PENUMPANG_SOPIR'=> $kode_akun_komisi_penumpang_sopir,
			 'KODE_AKUN_KOMISI_PENUMPANG_CSO'	=> $kode_akun_komisi_penumpang_cso,
			 'KODE_AKUN_KOMISI_PAKET_SOPIR'		=> $kode_akun_komisi_paket_sopir,
			 'KODE_AKUN_KOMISI_PAKET_CSO'			=> $kode_akun_komisi_paket_cso,
			 'KODE_AKUN_CHARGE'								=> $kode_akun_charge,
			 'BGCOLOR_PESAN'									=> $bgcolor_pesan,
			 'PESAN'						=> $pesan,
			 'U_JURUSAN_ADD_ACT'=>append_sid('pengaturan_jurusan.'.$phpEx)
			 )
			);
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Jurusan->ambilDataDetail($id);
		
		$temp_var_tuslah="tuslah_".$row['FlagTiketTuslah'];
		$$temp_var_tuslah="selected";
		
		$temp_var_aktif="aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";
		
		$temp_var_aktif="jenis_".$row['FlagLuarKota'];
		$$temp_var_aktif="selected";
		
		$temp_var_sub_jurusan	="sub_jurusan_".$row['FlagSubJurusan'];
		$$temp_var_sub_jurusan="selected";
		
		$temp_var_op_jurusan	="op_".$row['FlagOperasionalJurusan'];
		$$temp_var_op_jurusan	="selected";

		$temp_var_lokasi_spbu   ="spbu_".$row['LokasiSPBU'];
		$$temp_var_lokasi_spbu  ="selected";

		$data_layout	= $Mobil->getArrayLayout();

		$bbm_decode		= $Mobil->layoutBodyDecode($row["LiterBBM"]);
		$bbm2_decode		= $Mobil->layoutBodyDecode($row["LiterBBMExt"]);

		foreach($data_layout as $layout_id	=> $array_value){
			$template->assign_block_vars("LISTBBM",array(
					"idlayout"	=> $layout_id,
					"literbbm"	=> $bbm_decode["$layout_id"],
					"literbbm2"	=> $bbm2_decode["$layout_id"])
			);

		}
		
		$template->set_filenames(array('body' => 'jurusan/add_body.tpl'));

		$template->assign_vars(array(
			 'BCRUMP'		=>setBcrump($id_page),
			 'JUDUL'		=> 'Ubah Data Jurusan',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'ID_JURUSAN'				=> $id,
			 'KODE_JURUSAN_OLD'	=> $row['KodeJurusan'],
			 'KODE_JURUSAN'			=> $row['KodeJurusan'],
			 'OPT_ASAL'    			=> setComboCabang($row['KodeCabangAsal']),
			 'OPT_TUJUAN'   		=> setComboCabang($row['KodeCabangTujuan']),
			 'HARGA_TIKET'			=> $row['HargaTiket'],
			 'HARGA_TIKET_TUSLAH'=> $row['HargaTiketTuslah'],
			 'HARGA_PAKET_1_KILO_PERTAMA'	=> $row['HargaPaket1KiloPertama'],
			 'HARGA_PAKET_1_KILO_BERIKUT'	=> $row['HargaPaket1KiloBerikut'],
			 'HARGA_PAKET_2_KILO_PERTAMA'=> $row['HargaPaket2KiloPertama'],
			 'HARGA_PAKET_2_KILO_BERIKUT'=> $row['HargaPaket2KiloBerikut'],
			 'HARGA_PAKET_3_KILO_PERTAMA'=> $row['HargaPaket3KiloPertama'],
			 'HARGA_PAKET_3_KILO_BERIKUT'=> $row['HargaPaket3KiloBerikut'],
			 'HARGA_PAKET_4_KILO_PERTAMA'	=> $row['HargaPaket4KiloPertama'],
			 'HARGA_PAKET_4_KILO_BERIKUT'	=> $row['HargaPaket4KiloBerikut'],
			 'HARGA_PAKET_5_KILO_PERTAMA'	=> $row['HargaPaket5KiloPertama'],
			 'HARGA_PAKET_5_KILO_BERIKUT'	=> $row['HargaPaket5KiloBerikut'],
			 'HARGA_PAKET_6_KILO_PERTAMA'	=> $row['HargaPaket6KiloPertama'],
			 'HARGA_PAKET_6_KILO_BERIKUT'	=> $row['HargaPaket6KiloBerikut'],
			 'TUSLAH_0'					=> $tuslah_0,
			 'TUSLAH_1'					=> $tuslah_1,
			 'AKTIF_0'					=> $aktif_0,
			 'AKTIF_1'					=> $aktif_1,
			 'JENIS_0'					=> $jenis_0,
			 'JENIS_1'					=> $jenis_1,
			 'OP_0'							=> $op_0,
			 'OP_1'							=> $op_1,
			 'OP_2'							=> $op_2,
			 'SPBU_1'                       => $spbu_1,
			 'SPBU_2'                       => $spbu_2,
			 'BIAYA_SOPIR'			=> $row['BiayaSopir'],
			 'FLAG_BIAYA_SOPIR_KUMULATIF'	=> $row['IsBiayaSopirKumulatif']!=1?"":"checked",
			 'BIAYA_TOL'				=> $row['BiayaTol'],
			 'BIAYA_PARKIR'			=> $row['BiayaParkir'],
			 'BIAYA_BBM'				=> $row['BiayaBBM'],
			 'FLAG_BIAYA_VOUCHER_BBM'	=> $row['IsVoucherBBM']!=1?"":"checked",
			 'KOMISI_PENUMPANG_SOPIR'					=> $row['KomisiPenumpangSopir'],
			 'KOMISI_PENUMPANG_CSO'						=> $row['KomisiPenumpangCSO'],
			 'KOMISI_PAKET_SOPIR'							=> $row['KomisiPaketSopir'],
			 'KOMISI_PAKET_CSO'								=> $row['KomisiPaketCSO'],
			 'KODE_AKUN_PENDAPATAN_PENUMPANG'	=> $row['KodeAkunPendapatanPenumpang'],
			 'KODE_AKUN_PENDAPATAN_PAKET'			=> $row['KodeAkunPendapatanPaket'],
			 'KODE_AKUN_BIAYA_SOPIR'					=> $row['KodeAkunBiayaSopir'],
			 'KODE_AKUN_BIAYA_TOL'						=> $row['KodeAkunBiayaTol'],
			 'KODE_AKUN_BIAYA_PARKIR'					=> $row['KodeAkunBiayaParkir'],
			 'KODE_AKUN_BIAYA_BBM'						=> $row['KodeAkunBiayaBBM'],
			 'KODE_AKUN_KOMISI_PENUMPANG_SOPIR'=> $row['KodeAkunKomisiPenumpangSopir'],
			 'KODE_AKUN_KOMISI_PENUMPANG_CSO'	=> $row['KodeAkunKomisiPenumpangCSO'],
			 'KODE_AKUN_KOMISI_PAKET_SOPIR'		=> $row['KodeAkunKomisiPaketSopir'],
			 'KODE_AKUN_KOMISI_PAKET_CSO'			=> $row['KodeAkunKomisiPaketCSO'],
			 'KODE_AKUN_CHARGE'								=> $row['KodeAkunCharge'],
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_JURUSAN_ADD_ACT'=>append_sid('pengaturan_jurusan.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus jurusan
		$list_jurusan = str_replace("\'","'",$HTTP_GET_VARS['list_jurusan']);
		
		$Jurusan->hapus($list_jurusan);
		
		exit;
	} 
	else if ($mode=='ubahstatusaktif'){
		// aksi hapus jadwal
		$id_jurusan = $HTTP_GET_VARS['id'];
	
		$Jurusan->ubahStatusAktif($id_jurusan);
		
		exit;
	}
	else if ($mode=='ubahstatustuslah'){
		// aksi hapus jadwal
		/*$id_jurusan = $HTTP_GET_VARS['id'];
		$tuslah 		= $HTTP_GET_VARS['tuslah'];
	
		$Jurusan->ubahStatusTuslah($id_jurusan,$tuslah);
		*/
		exit;
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'jurusan/jurusan_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$temp_cari=str_replace("asal=","",$cari);
		if($temp_cari==$cari){
			$kondisi_asal = "";
		}
		else{
			$kondisi_asal = "AND f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$temp_cari%' ";
			$cari=$temp_cari;
		}
		
		$temp_cari=str_replace("tujuan=","",$cari);
		if($temp_cari==$cari){
			$kondisi_tujuan = "";
		}
		else{
			$kondisi_tujuan = "AND f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$temp_cari%' ";
			$cari=$temp_cari;
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE (KodeJurusan LIKE '%$cari%' 
				OR KodeCabangAsal LIKE '%$cari%' 
				OR KodeCabangTujuan LIKE '%$cari%'
				OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%' 
				OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%') 
				$kondisi_asal
				$kondisi_tujuan ";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"IdJurusan","tbl_md_jurusan","&cari=$cari",$kondisi,"pengaturan_jurusan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT *,f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabangAsal,f_cabang_get_name_by_kode(KodeCabangTujuan) AS NamaCabangTujuan,FlagOperasionalJurusan
			FROM tbl_md_jurusan $kondisi 
			ORDER BY KodeJurusan,NamaCabangAsal,NamaCabangTujuan LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  
			while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"$row[0]\"/>";
				
				if(!$row['FlagTiketTuslah']){
					$tuslah = "";
					$css_harga_normal	= "green";
					$css_harga_tuslah	= "";
				}
				else{
					$tuslah	= "<b>TUSLAH<b>";
					$css_harga_normal	= "";
					$css_harga_tuslah	= "green";
				}
				
				$harga_tiket_normal	= "<a href='#' onClick='ubahStatusTuslah($row[IdJurusan],0)'>".number_format($row['HargaTiket'],0,",",".")."</a>";
				$harga_tiket_tuslah	= "<a href='#' onClick='ubahStatusTuslah($row[IdJurusan],1)'>".number_format($row['HargaTiketTuslah'],0,",",".")."</a>";
				
				if($row['FlagAktif']){
					$aktif = "<a href='#' onClick='ubahStatusAktif($row[IdJurusan])'>Aktif</a>";
				}
				else{
					$aktif ="<a href='#' onClick='ubahStatusAktif($row[IdJurusan])'>Tidak Aktif</a>";
					$odd	='red';
				}
				
				$class_op_jurusan="";
				
				if($row['FlagOperasionalJurusan']==1){
					$class_op_jurusan	= "class='jurusan_paket'";
				}
				elseif($row['FlagOperasionalJurusan']==2){
					$class_op_jurusan	= "class='jurusan_paket_travel'";
				}
				
				
				$act 	="<a href='".append_sid('pengaturan_jurusan.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'class_op_jurusan'=>$class_op_jurusan,
							'kode'=>$row['KodeJurusan'],
							'asal'=>$row['NamaCabangAsal'],
							'tujuan'=>$row['NamaCabangTujuan'],
							'jenis'=>($row['FlagLuarKota']==1)?"LUAR KOTA":"DALAM KOTA",
							'harga_tiket'=>$harga_tiket_normal,
							'normal'=>$css_harga_normal,
							'harga_tiket_tuslah'=>$harga_tiket_tuslah,
							'harga_paket_1'=>number_format($row['HargaPaket1KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket1KiloBerikut'],0,",","."),
							'harga_paket_2'=>number_format($row['HargaPaket2KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket2KiloBerikut'],0,",","."),
							'harga_paket_3'=>number_format($row['HargaPaket3KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket3KiloBerikut'],0,",","."),
							'harga_paket_4'=>number_format($row['HargaPaket4KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket4KiloBerikut'],0,",","."),
							'harga_paket_5'=>number_format($row['HargaPaket5KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket5KiloBerikut'],0,",","."),
							'harga_paket_6'=>number_format($row['HargaPaket6KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket6KiloBerikut'],0,",","."),
							'tuslah'=>$css_harga_tuslah,
							'status_tuslah'=>$tuslah,
							'status_aktif'=>$aktif,
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=15 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
			
		} 
		else{
			//die_error('Cannot Load jurusan',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$page_title	= "Pengaturan Jurusan";

		$template->assign_vars(array(
			'BCRUMP'    		=>setBcrump($id_page),
			'U_JURUSAN_ADD'	=> append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_jurusan.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>