<?php
/**
 * Class Pembayaran Indomaret Log.
 *
 * Last updated: 1/20/16, 2:08 PM
 *
 * @author Sopyan Adicandra Ramandani <sopyan@3trust.com>
 *
 */

class LogIndomaret{
    var $ID_FILE; //ID Kelas
    var $TABEL1;
    var $WAKTU_BACKUP;

    function LogIndomaret(){
        $this->ID_FILE="C-LOGINDOMARET";
        $this->WAKTU_BACKUP = 2; //hari
    }

    function writeLog($date,$module,$action,$url,$status,$data,$user_id){
        global $db;

        $data = str_replace("'","|",$data);

        $sql = "INSERT INTO tbl_log_indomaret (date,module,action,url,status,data,user_id) VALUES ('$date','$module','$action','$url','$status','$data','$user_id');";
        if (!$db->sql_query($sql)){
            die_error("Err $this->ID_FILE".__LINE__);
        }

        return true;
    }
}