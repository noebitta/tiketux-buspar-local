<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################


class Jadwal{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas

	//CONSTRUCTOR
	function Jadwal(){
		$this->ID_FILE="C-JDW";
	}
	
	//BODY

  function isDuplikasi($kode_jadwal){

    //kamus
    global $db;

    $sql = "SELECT COUNT(1)  FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);
      exit;
    }

    $row  = $db->sql_fetchrow($result);

    return ($row[0]>0?true:false);

  }//  END isDuplikasi

  function ambilData($order_by,$sort_by=0,$idx_page="",$cari=""){

    //kamus
    global $db, $VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("KodeJadwal","JenisJadwal","Jurusan","JamBerangkat","JenisLayout","StatusAktif","StatusOnline");

    $sort     = ($sort_by==0)?"ASC":"DESC";

    $order		= ($order_by!=="" && $order_by!==null)?" ORDER BY $koloms[$order_by] $sort":" ORDER BY $koloms[0] ASC";
    $set_limit= ($idx_page!=="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1".($cari==""?"":" AND (KodeJadwal LIKE '%$cari%' OR JamBerangkat LIKE '$cari%' OR f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)) LIKE '%$cari%' OR f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)) LIKE '%$cari%')");

    $sql  =
      "SELECT SQL_CALC_FOUND_ROWS
        KodeJadwal,
        IF(IsSubJadwal=0,'INDUK',CONCAT('TRANSIT dari:',KodeJadwalutama)) AS JenisJadwal,
        CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan))) AS Jurusan,
        JamBerangkat,tmkl.KodeLayout AS JenisLayout,IF(IsAktif=1,'AKTIF','NON-AKTIF') AS StatusAktif, IF(IsOnline,'ONLINE','OFFLINE') AS StatusOnline
      FROM tbl_md_jadwal tmj LEFT JOIN tbl_md_kendaraan_layout tmkl ON tmj.JenisLayout=tmkl.IdLayout
      $set_kondisi
      $order
	    $set_limit";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);exit;
    }

    return $result;

  }//  END ambilData
	
	function tambah($KodeJadwal, $IdJurusan, $KodeCabangAsal,
                  $KodeCabangTujuan, $JamBerangkat,$JenisLayout,
                  $JumlahKursi,$IsSubJadwal,$KodeJadwalUtama,
                  $HariAktif,$IsAktif, $IsOnline){

		//kamus
		global $db;

    //CEK DUPLIKASI

    if($this->isDuplikasi($KodeJadwal)) {
      $ret_val["status"] = "GAGAL";
      $ret_val["pesan"]  = "KODE SUDAH TERDAFTAR";

      return $ret_val;
    }
		
		$sql  =
      "INSERT INTO tbl_md_jadwal SET
        KodeJadwal='$KodeJadwal', IdJurusan='$IdJurusan', JamBerangkat='$JamBerangkat',
        KodeCabangAsal='$KodeCabangAsal',KodeCabangTujuan='$KodeCabangTujuan',JenisLayout='$JenisLayout',
        JumlahKursi='$JumlahKursi',IsSubJadwal='$IsSubJadwal',KodeJadwalUtama='$KodeJadwalUtama',
        HariAktif='$HariAktif',IsAktif='$IsAktif',IsOnline='$IsOnline'";

    if(!$result=$db->sql_query($sql)){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "SQL ERROR";

      return $ret_val;
    }

    if($IsSubJadwal==1) {
      $this->updateViaJadwalUtama($KodeJadwalUtama);
    }

    $ret_val["status"]  = "OK";

    return $ret_val;
	}

	function ubah($KodeJadwal, $IdJurusan, $KodeCabangAsal,
                $KodeCabangTujuan,$JamBerangkat,$JenisLayout,
                $JumlahKursi,$IsSubJadwal,$KodeJadwalUtama,
                $HariAktif,$IsAktif,$IsOnline,
                $KodeJadwalOld){

		//kamus
		global $db;

    //CEK DUPLIKASI
    if($this->isDuplikasi($KodeJadwal) && $KodeJadwal!=$KodeJadwalOld) {
      $ret_val["status"] = "GAGAL";
      $ret_val["pesan"]  = "KODE SUDAH TERDAFTAR";

      return $ret_val;
    }

    $data_jadwal_old  = $this->ambilDataDetail($KodeJadwalOld);

    $KodeJadwalUtama  = $IsSubJadwal!=1?"":$KodeJadwalUtama;

    //UPDATE TABEL MASTER JADWAL
    $sql  =
      "UPDATE tbl_md_jadwal SET
        KodeJadwal='$KodeJadwal', IdJurusan='$IdJurusan', JamBerangkat='$JamBerangkat',
        KodeCabangAsal='$KodeCabangAsal',KodeCabangTujuan='$KodeCabangTujuan',JenisLayout='$JenisLayout',
        JumlahKursi='$JumlahKursi',IsSubJadwal='$IsSubJadwal',KodeJadwalUtama='$KodeJadwalUtama',
        HariAktif='$HariAktif',IsAktif='$IsAktif',IsOnline='$IsOnline'
      WHERE KodeJadwal='$KodeJadwalOld'";

    if(!$result=$db->sql_query($sql)){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "SQL ERROR ".__LINE__;

      return $ret_val;
    }

    if($KodeJadwal!=$KodeJadwalOld){
      //JIKA KODE JADWAL BERUBAH
      //UPDATE TABEL TABEL YANG TERELASI DENGAN TABEL MASTER JADWAL

      //UPDATE TABEL MASTER JADWAL UNTUK KODE JADWAL UTAMA
      $sql  =
        "UPDATE tbl_md_jadwal SET
        KodeJadwalUtama='$KodeJadwal'
      WHERE KodeJadwalUtama='$KodeJadwalOld' AND IsSubJadwal=1";

      if(!$result=$db->sql_query($sql)){
        $ret_val["status"]  = "GAGAL";
        $ret_val["pesan"]   = "SQL ERROR ".__LINE__;

        return $ret_val;
      }

      $list_tabel = array(
        array("tabel"=>"tbl_reservasi"),
        array("tabel"=>"tbl_paket"),
        array("tabel"=>"tbl_manifest"),
        array("tabel"=>"tbl_manifest_paket"),
        array("tabel"=>"tbl_manifest_paket_request_otp_cetak"),
        array("tabel"=>"tbl_md_harga_promo"),
        array("tabel"=>"tbl_md_promo"),
        array("tabel"=>"tbl_paket_request_otp_batal"),
        array("tabel"=>"tbl_paket_request_otp_cetak"),
        array("tabel"=>"tbl_paket_request_otp_mutasi"),
        array("tabel"=>"tbl_paket_transit"),
        array("tabel"=>"tbl_penjadwalan_kendaraan"),
        array("tabel"=>"tbl_penjadwalan_sopir"),
        array("tabel"=>"tbl_reservasi_waiting_list"),
        array("tabel"=>"tbl_voucher"),
        array("tabel"=>"tbl_voucher","field"=>"KodeJadwalBerangkat"),
        array("tabel"=>"tbl_voucher_bbm"),
        array("tabel"=>"tbl_voucher_paket","field"=>"KodeJadwalDigunakan"));

      foreach($list_tabel as $tabel){
        //UPDATE TABEL RESERVASI

        $field  = $tabel["field"]==""?"KodeJadwal":$tabel["field"];

        $sql  =
          "UPDATE $tabel[tabel] SET
            $field='$KodeJadwal'
          WHERE $field='$KodeJadwalOld'";

        if(!$result=$db->sql_query($sql)){
          $ret_val["status"]  = "GAGAL";
          $ret_val["pesan"]   = "SQL ERROR";

          return $ret_val;
        }

      }

    }

    //JIKA JADWAL YANG DIUBAH ADALAH JADWAL TRANSIT, MAKA AKAN MENGUPDATE KOLOM "VIA" DI JADWAL UTAMANYA
    if($IsSubJadwal==1){
      $this->updateViaJadwalUtama($KodeJadwalUtama);
    }
    else if($data_jadwal_old["IsSubJadwal"]==1){
      $this->updateViaJadwalUtama($data_jadwal_old["KodeJadwalUtama"]);
    }

    $ret_val["status"]  = "OK";

    return $ret_val;
	}

  function updateViaJadwalUtama($kode_jadwal_utama){

    /*
    MENGUPDATE KOLOM VIA DI TBL MD JADWAL
    */

    //kamus
    global $db;

    $data_jadwal = $this->ambilDataDetail($kode_jadwal_utama);

    $sql =
      "SELECT GROUP_CONCAT(DISTINCT KodeCabangAsal ORDER BY JamBerangkat SEPARATOR ',' ) AS GroupCabangAsal
      FROM tbl_md_jadwal
      WHERE KodeJadwalUtama='$kode_jadwal_utama'
      AND KodeCabangAsal!='$data_jadwal[KodeCabangAsal]' AND KodeCabangAsal!='$data_jadwal[KodeCabangTujuan]'
      GROUP BY KodeJadwalUtama";

    if (!$result = $db->sql_query($sql)) {
      echo("Err: $this->ID_FILE" . __LINE__);
      exit;
    }

    $row = $db->sql_fetchrow($result);

    //MENGUBAH DATA KEDALAM DATABASE
    $sql =
      "UPDATE tbl_md_jadwal SET
			Via='$row[0]'
		WHERE KodeJadwal = '$kode_jadwal_utama';";

    if (!$db->sql_query($sql)) {
      echo("Err: $this->ID_FILE" . __LINE__);
      exit;
    }

    return true;
  }

  function ambilDetailRute($kode_jadwal,$kode_jadwal_utama){

    /*
    Desc	:Mengembalikan data jadwal yang memiliki kodejadwalutama=$kode_jadwal
    */

    //kamus
    global $db;

    $sql =
      "SELECT
				GROUP_CONCAT(DISTINCT JamBerangkat SEPARATOR ',') AS GroupJamBerangkat,
				KodeCabangAsal,f_cabang_get_name_by_kode(KodeCabangAsal) AS NamaCabangAsal,
				GROUP_CONCAT(DISTINCT KodeJadwal SEPARATOR ',') AS GroupKodeJadwal
			FROM tbl_md_jadwal
			WHERE KodeJadwalUtama='$kode_jadwal_utama' AND KodeJadwal!='$kode_jadwal'
			GROUP BY KodeCabangAsal
			ORDER BY JamBerangkat ASC";

    if (!$result = $db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);exit;
    }

    return $result;
  }//  END ambilDetailRute


  function hapus($list_id){

		//kamus
	
		global $db,$userdata;
		
	  //MEMBACKUP JADWAL YANG DIHAPUS

    $sql="SELECT * FROM tbl_md_jadwal WHERE KodeJadwal IN ($list_id)";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    while($row=$db->sql_fetchrow($result)){
      $sql=
        "INSERT INTO tbl_md_jadwal_deleted SET
          KodeJadwal='$row[KodeJadwal]', IdJurusan='$row[IdJurusan]', JamBerangkat='$row[JamBerangkat]',
          JenisLayout='$row[JenisLayout]',JumlahKursi='$row[JumlahKursi]',IsSubJadwal='$row[IsSubJadwal]',
          KodeJadwalUtama='$row[KodeJadwalUtama]',HariAktif='$row[HariAktif]',IsAktif='$row[IsAktif]',
          IsOnline='$row[IsOnline]',DihapusOleh='$userdata[user_id]',NamaPenghapus='$userdata[nama]',
          WaktuHapus=NOW()";

      if (!$result=$db->sql_query($sql)){
        echo("Err: $this->ID_FILE".__LINE__);exit;
      }
    }

		//MENGHAPUS DATA DARI DATABASE
		$sql =
			"DELETE FROM tbl_md_jadwal
			WHERE KodeJadwal IN($list_id);";
								
		if (!$db->sql_query($sql)){
			return false;
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}

    $ret_val["status"]  = "OK";

		return $ret_val;
	}//end hapus
	
	function ubahStatusAktif($kode_jadwal){
		
		//kamus
		global $db;
		
		$sql =
      "UPDATE tbl_md_jadwal SET
        IsAktif=1-IsAktif
      WHERE KodeJadwal='$kode_jadwal';";
		
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}//end ubahStatus

	function ubahStatusOnline($kode_jadwal){

    //kamus
    global $db;

    $sql =
      "UPDATE tbl_md_jadwal SET
        IsOnline=1-IsOnline
      WHERE KodeJadwal='$kode_jadwal';";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
	}//end ubahStatusOnline
	
	function ambilDataDetail($kode_jadwal){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				KodeJadwal,JamBerangkat,IdJurusan,
				JumlahKursi,IsSubJadwal,KodeJadwalUtama,
				JenisLayout,HariAktif,IsAktif,
				IsOnline
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";		
		
		if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

		$row=$db->sql_fetchrow($result);

		$data_jadwal['KodeJadwal']			= $row['KodeJadwal'];
		$data_jadwal['JamBerangkat']		= $row['JamBerangkat'];
		$data_jadwal['IdJurusan']				= $row['IdJurusan'];
		$data_jadwal['JumlahKursi']			= $row['JumlahKursi'];
		$data_jadwal['IsSubJadwal']		  = $row['IsSubJadwal'];
		$data_jadwal['KodeJadwalUtama']	= $row['KodeJadwalUtama'];
		$data_jadwal['HariAktif']				= $row['HariAktif'];
		$data_jadwal['JenisLayout']			= $row['JenisLayout'];
		$data_jadwal['IsAktif']				  = $row['IsAktif'];
		$data_jadwal['IsOnline']				= $row['IsOnline'];

		//mengambil kodecabang asal dan tujuan
		$sql	=
		  "SELECT KodeCabangAsal,KodeCabangTujuan
		  FROM tbl_md_jurusan
		  WHERE IdJurusan='$row[IdJurusan]'";

		if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
		}

    $row=$db->sql_fetchrow($result);
    $data_jadwal['KodeCabangAsal']		= $row['KodeCabangAsal'];
    $data_jadwal['KodeCabangTujuan']	= $row['KodeCabangTujuan'];
			
		//mengambil nama cabang asal
		$sql	=
			"SELECT Nama
			FROM tbl_md_cabang
			WHERE KodeCabang='$data_jadwal[Asal]'";
			
		if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
		}

    $row=$db->sql_fetchrow($result);

    $data_jadwal['NamaAsal']	= $row['Nama'];
			
		//mengambil nama cabang tujuan
		$sql	=
			"SELECT Nama
		  FROM tbl_md_cabang
		  WHERE KodeCabang='$data_jadwal[Tujuan]'";

		if (!$result = $db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}

    $row=$db->sql_fetchrow($result);

    $data_jadwal['NamaTujuan']	= $row['Nama'];

    if($data_jadwal["IsSubJadwal"]){
      $data_jadwal_utama                    = $this->ambilDataDetail($data_jadwal["KodeJadwalUtama"]);
      $data_jadwal["IdJurusanUtama"]        = $data_jadwal_utama["IdJurusan"];
      $data_jadwal["KodeCabangAsalUtama"]   = $data_jadwal_utama["KodeCabangAsal"];
      $data_jadwal["KodeCabangTujuanUtama"] = $data_jadwal_utama["KodeCabangTujuan"];
      $data_jadwal["NamaAsalUtama"]         = $data_jadwal_utama["NamaAsal"];
      $data_jadwal["NamaTujuanUtama"]       = $data_jadwal_utama["NamaTujuan"];
    }

		return $data_jadwal;

		
	}//  END ambilData

  function setListJadwalReservasi($tgl,$id_jurusan){

    //kamus
    global $db,$config;

    $tgl_berangkat  = strtotime($tgl);

    $hari_aktif = date("w",$tgl_berangkat)+1;

    $sql =
      "SELECT tmj.KodeJadwal,tmj.JamBerangkat,IF(tpk.StatusAktif=1 OR (tpk.StatusAktif IS NULL AND tmj.IsAktif=1 AND HariAktif LIKE '%$hari_aktif%'),1,0) AS JadwalDioperasikan,IF(TIME_TO_SEC(TIMEDIFF(CONCAT('$tgl ',tmj.JamBerangkat,':59'),NOW()))/60>=-$config[toleransi_buka_jam_lalu],1,0) AS VerifyWaktu
			FROM tbl_md_jadwal tmj
				LEFT JOIN tbl_penjadwalan_kendaraan tpk
				ON tpk.KodeJadwal=tmj.KodeJadwal AND tpk.TglBerangkat='$tgl'
			WHERE tmj.IdJurusan=$id_jurusan
			ORDER BY tmj.JamBerangkat ASC,JadwalDioperasikan DESC,KodeJadwal ASC;";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE". __LINE__);exit;
    }

    return $result;

  }//  END setListJadwalReservasi

  function getStatusJadwal($kode_jadwal,$tgl){

    //kamus
    global $db,$config;

    $tgl_berangkat  = strtotime($tgl);

    $hari_aktif = date("w",$tgl_berangkat)+1;

    $sql =
      "SELECT tmj.KodeJadwal,tmj.JamBerangkat,IF(tpk.StatusAktif=1 OR (tpk.StatusAktif IS NULL AND tmj.IsAktif=1  AND HariAktif LIKE '%$hari_aktif%'),1,0) AS JadwalDioperasikan,IF(TIME_TO_SEC(TIMEDIFF(CONCAT('$tgl ',tmj.JamBerangkat,':59'),NOW()))/60>=-$config[toleransi_buka_jam_lalu],1,0) AS VerifyWaktu
			FROM tbl_md_jadwal tmj
				LEFT JOIN tbl_penjadwalan_kendaraan tpk
				ON tpk.KodeJadwal=tmj.KodeJadwal AND tpk.TglBerangkat='$tgl'
			WHERE tmj.KodeJadwal='$kode_jadwal';";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE". __LINE__);exit;
    }

    $data_jadwal  = $db->sql_fetchrow($result);

    $sql  =
      "SELECT KodeJadwal,KodeJadwalUtama,IsSubJadwal FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE". __LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    if($row["IsSubJadwal"]==1 && $data_jadwal["JadwalDioperasikan"]){
      $sql =
        "SELECT IF(tpk.StatusAktif=1 OR (tpk.StatusAktif IS NULL AND tmj.IsAktif=1  AND HariAktif LIKE '%$hari_aktif%'),1,0) AS JadwalDioperasikan
        FROM tbl_md_jadwal tmj
          LEFT JOIN tbl_penjadwalan_kendaraan tpk
          ON tpk.KodeJadwal=tmj.KodeJadwal AND tpk.TglBerangkat='$tgl'
        WHERE tmj.KodeJadwal='$row[KodeJadwalUtama]';";

      if(!$result = $db->sql_query($sql)){
        echo("Err: $this->ID_FILE". __LINE__);exit;
      }

      $data_jadwal_utama  = $db->sql_fetchrow($result);

      $data_jadwal["JadwalDioperasikan"]  = $data_jadwal_utama["JadwalDioperasikan"];
    }

    return $data_jadwal;

  }//  END getStatusJadwal

  function getJadwalReservasi($kode_jadwal,$tgl){

    //kamus
    global $db,$config;

    $tgl_berangkat  = strtotime($tgl);

    $hari_aktif = date("w",$tgl_berangkat)+1;

    $sql =
      "SELECT tmj.KodeJadwal,tmj.JamBerangkat,IF((tpk.StatusAktif IS NULL) OR (tpk.LayoutKursi IS NULL),tmj.JenisLayout,tpk.LayoutKursi) AS JenisLayout,
        IF(tpk.StatusAktif=1 OR (tpk.StatusAktif IS NULL AND tmj.IsAktif=1 AND HariAktif LIKE '%$hari_aktif%'),1,0) AS JadwalDioperasikan,IF(TIME_TO_SEC(TIMEDIFF(CONCAT('$tgl ',tmj.JamBerangkat,':59'),NOW()))/60>=-$config[toleransi_buka_jam_lalu],1,0) AS VerifyWaktu,
        KodeCabangAsal,KodeCabangTujuan,CONCAT(KodeCabangAsal,',',IF(Via='','',CONCAT(Via,',')),KodeCabangTujuan) AS Rute
			FROM tbl_md_jadwal tmj
				LEFT JOIN tbl_penjadwalan_kendaraan tpk
				ON tpk.KodeJadwal=tmj.KodeJadwal AND tpk.TglBerangkat='$tgl'
			WHERE tmj.KodeJadwal='$kode_jadwal'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE". __LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }//  END getJadwalReservasi

  //==========old function=============
	
	function setListJadwal($tgl,$id_jurusan){

		//kamus
		global $db;
		global $userdata;
		global $TOLERANSI_KEBERANGKATAN;
		global $LEVEL_CSO;

		if($id_jurusan=="") exit;

		if($tgl==""){
			$kondisi_status_aktif	= " AND FlagAktif=1 ";
		}
		else{
			$sql_penjadwalan	=
				"(SELECT StatusAktif
				FROM tbl_penjadwalan_kendaraan tpk
				WHERE tpk.KodeJadwal=tmj.KodeJadwal AND TglBerangkat LIKE '$tgl')";
			
			$kondisi_status_aktif	= " AND ($sql_penjadwalan=1 OR ($sql_penjadwalan IS NULL AND FlagAktif=1)) ";
		}
		
		//CEK USERLEVEL
		if($userdata['user_level']>=$LEVEL_CSO){
			$kondisi_user_level	= " AND  TIME_TO_SEC(TIMEDIFF(CONCAT('$tgl',' ',JamBerangkat,':00'),NOW()))>=-($TOLERANSI_KEBERANGKATAN*60) ";
		}
		
		
		$sql = 
			"SELECT KodeJadwal,JamBerangkat
			  FROM tbl_md_jadwal tmj
			  WHERE IdJurusan=$id_jurusan 
					$kondisi_status_aktif $kondisi_user_level
			  ORDER BY JamBerangkat;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			#echo("Err: $this->ID_FILE $sql". __LINE__);
		}
		
	}//  END ambilData
	
	function ambilListJadwalDanPickUp($kode_jadwal){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;

		$sql = 
			"SELECT KodeJadwal,IdJurusan
			FROM tbl_md_jadwal
			WHERE (KodeJadwalUtama='$kode_jadwal' AND FlagSubJadwal=1) OR KodeJadwal='$kode_jadwal';";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilListSubKodeJadwalByKodeJadwal

	function ambilListJadwalDanPickUpPaket($kode_jadwal){

		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/

		//kamus
		global $db;

		$sql =
			"SELECT KodeJadwal,tmj.IdJurusan,tmc.Nama AS NamaCabangAsal
			FROM (tbl_md_jadwal tmj LEFT JOIN tbl_md_jurusan tmjr ON tmj.IdJurusan=tmjr.IdJurusan)  LEFT JOIN tbl_md_cabang tmc ON tmc.KodeCabang=tmjr.KodeCabangAsal
			WHERE (KodeJadwalUtama='$kode_jadwal' AND IsSubJadwal=1) OR KodeJadwal='$kode_jadwal';";

		if ($result = $db->sql_query($sql)){
			return $result;
		}
		else{
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}

	}//  END ambilListSubKodeJadwalByKodeJadwal

	function setSisaKursiJadwalNext($tgl,$id_jurusan,$jam_berangkat){
		
		/*
		Desc	:Mengembalikan data jadwal
		*/
		
		//kamus
		global $db;
		global $config;
		global $SESSION_TIME_EXPIRED;
		
		$sql = 
			"SELECT KodeJadwal,StatusAktif,LayoutKursi
				FROM tbl_penjadwalan_kendaraan
				WHERE 
					IdJurusan='$id_jurusan' 
					AND TglBerangkat LIKE '$tgl' 
					AND JamBerangkat>'$jam_berangkat' 
					AND StatusAktif=1";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err".__LINE__);
		}
		
		while ($row = $db->sql_fetchrow($result)){
			$data_penjadwalan[$row['KodeJadwal']]['StatusAktif'] 	=  $row['StatusAktif'];
			$data_penjadwalan[$row['KodeJadwal']]['LayoutKursi'] 	=  $row['LayoutKursi'];
		}
		
		$sql = 
			"SELECT KodeJadwal,JamBerangkat,JumlahKursi
			  FROM tbl_md_jadwal
			  WHERE 
					IdJurusan='$id_jurusan'
					AND JamBerangkat>'$jam_berangkat'
					AND FlagAktif=1
					AND RIGHT(JamBerangkat,2)=RIGHT('$jam_berangkat',2)
			  ORDER BY JamBerangkat
				LIMIT 0,".$config['range_show_sisa_kursi'].";";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err".__LINE__);
		}
		
		$idx_jadwal	= 0;
		while ($row = $db->sql_fetchrow($result)){
			$list_kode_jadwal	.= "'".$row['KodeJadwal']."',"; 
			$data_jadwal[$idx_jadwal]['KodeJadwal']		= $row['KodeJadwal'];
			$data_jadwal[$idx_jadwal]['JamBerangkat']	= $row['JamBerangkat'];
			$data_jadwal[$idx_jadwal]['JumlahKursi']		= $data_penjadwalan[$row['KodeJadwal']]['LayoutKursi']==''?$row['JumlahKursi']-1:$data_penjadwalan[$row['KodeJadwal']]['LayoutKursi']-1;
			$idx_jadwal++;
		}
	 
		//mengambil jumlah kursi kosong
		
		$list_kode_jadwal	= substr($list_kode_jadwal,0,-1);
		
		if($list_kode_jadwal!=''){
			$sql = 
				"SELECT KodeJadwal,COUNT(1) AS JumlahDipesan
				FROM tbl_posisi_detail
				WHERE 
					KodeJadwal IN ($list_kode_jadwal) 
					AND TglBerangkat='$tgl'
					AND ((HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED 
					OR StatusKursi!=0)
				GROUP BY KodeJadwal";

			if (!$result = $db->sql_query($sql)){
				die_error("Err".__LINE__);
			}
			
			while ($row = $db->sql_fetchrow($result)){
				$data_jum_kursi[$row['KodeJadwal']]['JumlahDipesan']		= $row['JumlahDipesan'];
				$test++;
			}
		}
		
		$return	= "";
		
		for($idx=0; $idx<$idx_jadwal; $idx++){
			//list status kursi
			$row = $db->sql_fetchrow($result);
			
			if($data_penjadwalan[$row['KodeJadwal']]['StatusAktif']==1 || $data_penjadwalan[$row['KodeJadwal']]['StatusAktif']==''){
				$jumlah_sisa_kursi	= $data_jadwal[$idx]['JumlahKursi']-$data_jum_kursi[$data_jadwal[$idx]['KodeJadwal']]['JumlahDipesan'];
				$jumlah_sisa_kursi	= $jumlah_sisa_kursi>=0?$jumlah_sisa_kursi:0;
				$jumlah_sisa_kursi	= $jumlah_sisa_kursi>0?substr("0".$jumlah_sisa_kursi,-2)." Kursi":"<font color='red'>HABIS</font>";
				$return	.="<b>".$data_jadwal[$idx]['KodeJadwal']."</b> | ".$data_jadwal[$idx]['JamBerangkat'].": <b>".$jumlah_sisa_kursi."</b> <br>";
			}                                                                                                                                                                                                                                      
		}	 

		
		
		
		return $return;
		
	}//  END setSisaKursiJadwalNext

  function setComboListJadwal($id_jurusan,$cari="",$jam_berangkat=""){

    //kamus
    global $db;

    if($id_jurusan=="") exit;

    /*$kondisi_jam_berangkat  = "";

    if($jam_berangkat!=""){
      $kondisi_jam_berangkat  = " AND JamBerangkat<='$jam_berangkat'";
    }*/

    $sql =
      "SELECT KodeJadwal,JamBerangkat
			  FROM tbl_md_jadwal tmj
			  WHERE IdJurusan=$id_jurusan AND KodeJadwal LIKE '%$cari%' $kondisi_jam_berangkat
			  ORDER BY JamBerangkat;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE". __LINE__);exit;
    }

    return $result;

  }//  END setComboListJadwal

  private function catatLog($action,$keterangan){
    global $userdata;

    $log_baru = "'action=$action,keterangan=$keterangan,waktu=',NOW(),',oleh=$userdata[nama]#$userdata[user_id];'";

    return "CONCAT($log_baru,SUBSTRING(IF(Log IS NULL,'',Log),1,IF(LOCATE(';',Log,2000)=0,LENGTH(Log),LOCATE(';',Log,2000))))";
  }
}
?>