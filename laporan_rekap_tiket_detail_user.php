<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');
		
// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['p0'])? $HTTP_GET_VARS['p0'] : $HTTP_POST_VARS['p0'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$pencari				= isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$jenis_laporan	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];

// LIST
$template->set_filenames(array('body' => 'laporan_rekap_tiket_detail/laporan_rekap_tiket_detail_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Asuransi	= new Asuransi();

if($jenis_laporan==0){
	//laporan omzet cso
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND PetugasPenjual='$pencari'";
	
	$kondisi_waktu_paket	= $kondisi_waktu;
	$kondisi_laporan_paket= $kondisi_laporan;
}
elseif($jenis_laporan==1){
	//laporan omzet cabang	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$pencari'";
	
	$kondisi_waktu_paket	= $kondisi_waktu;
	$kondisi_laporan_paket= $kondisi_laporan;
}
elseif($jenis_laporan==2){
	//laporan omzet Jurusan	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND IdJurusan='$pencari'";
	
	$kondisi_waktu_paket	= $kondisi_waktu;
	$kondisi_laporan_paket= $kondisi_laporan;
}
elseif($jenis_laporan==3){
	//laporan rekap uang user	
	$kondisi_waktu		= "WaktuCetakTiket";
	$kondisi_laporan	= "AND PetugasCetakTiket='$pencari'";
	$kondisi_waktu_tambahan = "TglBuat";

	$kondisi_tambahan	= "AND IdPembuat='$pencari'";
	$kondisi_waktu_paket	= "WaktuPesan";
	$kondisi_laporan_paket= "AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual='$pencari',PetugasPemberi='$pencari')";
}
elseif($jenis_laporan==4){
	//laporan rekap cabang
	$kondisi_waktu		= "WaktuCetakTiket";
	$kondisi_laporan	= "AND KodeCabang='$pencari'";
	
	$kondisi_waktu_paket	= "WaktuPesan";
	$kondisi_laporan_paket= "AND KodeCabang='$pencari'";
}


$kondisi	= 
	"WHERE (DATE($kondisi_waktu) = '$tanggal_mulai_mysql') 
	AND CetakTiket=1 $kondisi_laporan";

$kondisi_paket	= 
	"WHERE (DATE($kondisi_waktu_paket) = '$tanggal_mulai_mysql') 
	AND CetakTiket=1 $kondisi_laporan_paket";

$kondisi_biaya	=
	"WHERE (DATE($kondisi_waktu_tambahan) = '$tanggal_mulai_mysql')
	AND StatusCetak=1 $kondisi_tambahan";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

$temp_tanggal_cari	= explode("-",$tanggal_mulai_mysql);
$tahun_cari		= $temp_tanggal_cari[0];
$bulan_cari		= $temp_tanggal_cari[1];

$tanggal_sekarang	= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang		= $temp_tanggal_sekarang[0];
$bulan_sekarang		= $temp_tanggal_sekarang[1];

if($tahun_cari==$tahun_sekarang && $bulan_cari==$bulan_sekarang){
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else{
	$tbl_reservasi	= "tbl_reservasi";
}

//QUERY TIKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		HargaTiket,SubTotal,Discount,Total,JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan,
		WaktuCetakTiket
	FROM 
		$tbl_reservasi
	$kondisi
	ORDER BY $sort_by $order";	


if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i = 1;//$idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$pesanan	= (!$row['FlagPesanan'])?"Go Show":"Pesanan";

	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Released";
		}
		else{
			$status	= "Not Released";
		}
		$keterangan=$row["JenisPembayaran"]<3?"":"Daytrans Addict";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	//data asuransi
	$data_asuransi			= $Asuransi->ambilDataDetailByNoTiket($row['NoTiket']);
	
	if($data_asuransi['IdAsuransi']!=""){
		$data_plan_asuransi	= $Asuransi->ambilDataDetail($data_asuransi['PlanAsuransi']);
		$plan_asuransi	= "<font color='green'><b>".$data_plan_asuransi['NamaPlan']."</b></font>";
		$besar_premi		= $data_asuransi['BesarPremi'];
	}
	else{
		$plan_asuransi	= "NO PLAN";
		$besar_premi		= 0;
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
				'no_tiket'=>$row['NoTiket'],
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'kode_jadwal'=>$row['KodeJadwal'],
				'nama'=>$row['Nama'],
				'no_kursi'=>$row['NomorKursi'],
				'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
				'discount'=>number_format($row['Discount'],0,",","."),
				'plan_asuransi'=>$plan_asuransi,
				'premi_asuransi'=>number_format($besar_premi,0,",","."),
				'total'=>number_format($row['Total']+$besar_premi,0,",","."),
				'tipe_discount'=>$row['JenisDiscount'],
				'cso'=>$row['NamaCSO'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
	
	$i++;
}

//QUERY TOPUP
$sql=
	"SELECT 
		WaktuTransaksi,KodeReferensi,tmdtl.IdMember,
		JumlahRupiah,JumlahPoin,Nama
	FROM 
		tbl_member_deposit_topup_log tmdtl INNER JOIN tbl_md_member tmm ON tmdtl.IdMember=tmm.IdMember
	WHERE DATE(WaktuTransaksi) = '$tanggal_mulai'
	ORDER BY WaktuTransaksi";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i = 1;//$idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$template->
		assign_block_vars(
			'ROWTOPUP',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_transaksi'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
				'kode_referensi'=>$row['KodeReferensi'],
				'id_member'=>$row['IdMember'],
				'nama'=>$row['Nama'],
				'rupiah'=>number_format($row['JumlahRupiah'],0,",","."),
				'tiket'=>number_format($row['JumlahTiket'],0,",",".")
			)
		);
	
	$i++;
}


//QUERY PAKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket
	$kondisi_paket
	ORDER BY $sort_by $order";	


if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['FlagBatal']!=1){
		if($row['CetakTiket']!=1){
			$odd	= "blue";
			$status	= "Book";
		}
		else{
			$status	= "OK";
		}
		$keterangan="";
	}
	else{
		$odd	= 'red';
		$status	="BATAL";
		$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
	}
	
	$template->
		assign_block_vars(
			'ROWPAKET',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
				'no_tiket'=>$row['NoTiket'],
				'kode_jadwal'=>$row['KodeJadwal'],
				'dari'=>$row['NamaPengirim'],
				'untuk'=>$row['NamaPenerima'],
				'harga_paket'=>number_format($row['HargaPaket'],0,",","."),
				'cso'=>$row['NamaCSO'],
				'status'=>$status,
				'ket'=>$keterangan
			)
		);
	
	$i++;
}

//QUERY TAMBAHAN
$sql=
	"SELECT *
		FROM tbl_biaya_tambahan
		WHERE TglCetak like '%$tanggal_mulai_mysql%'
		AND StatusCetak='1'
		AND IdPencetak='$userdata[user_id]'";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';

	if (($i % 2)==0){
		$odd = 'even';
	}

	if($row['StatusCetak']!=1){
		$odd	= "red";
		$status	= "Not Released";
	}
	else{
		$odd = "green";
		$status	= "Released";
	}

	$template->
	assign_block_vars(
		'ROWTAMBAHAN',
		array(
			'odd'=>$odd,
			'no'=>$i,
			'cabang'=>$row['KodeCabang'],
			'releaser'=>$row['NamaReleaser'],
			'pembuat'=>$row['NamaPembuat'],
			'penerima'=>$row['NamaPenerima'],
			'tgl_buat'=>$row['TglBuat'],
			'status'=>$status,
			'jenis_biaya'=>$row['JenisBiaya'],
			'jumlah'=>$row['Jumlah'],
			'ket'=>$row['Keterangan']
		)
	);

	$i++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p0=".$tanggal_mulai."&p1=".$tanggal_akhir."&p2=".$pencari."&p3=".$jenis_laporan."";
	
$script_cetak_pdf="Start('laporan_rekap_tiket_detail_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_rekap_tiket_detail_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'PENCARI'				=> $pencari,
	'JENIS_LAPORAN'	=> $jenis_laporan,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>