<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMacAddress.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 707;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$mac_id			= $HTTP_GET_VARS['mac_id']; 

$Cabang				= new Cabang();
$MacAddress 	= new MacAddress();

if($mac_id!=""){
	//CEK MACADDRESS
	$sql ="SELECT Id,KodeCabang, IsAktif
				FROM  tbl_mac_address
				WHERE MacAddress = '$mac_id' AND IsAktif=1";


	if ( (!$result = $db->sql_query($sql)) )
	{    
		echo("Err:".__LINE__);exit;
	}


	$data_mac_address = $db->sql_fetchrow($result);

	if($data_mac_address[0]==""){
		//MAC BELUM TERDAFTAR, AKAN DITANYAKAN APAKAH AKAN DIDAFTARKAN
		$mode 	= "add";
		$pesan 	= "<b>Mac Address belum terdaftar, silahkan lengkapi data dibawah ini, <br>atau tekan [KEMBALI] untuk membatalkan pendaftaran komputer.</b><hr>";
	}
}

if($mode=="") {
	// LIST
	$template->set_filenames(array('body' => 'pengaturan.macaddress/index.tpl')); 

	if($HTTP_POST_VARS["txt_cari"]!=""){
		$cari=$HTTP_POST_VARS["txt_cari"];
	}
	else{
		$cari=$HTTP_GET_VARS["cari"];
	}
	
	$kondisi	=($cari=="")?"":
		" WHERE KodeCabang LIKE '%$cari%' 
			OR MacAddress LIKE '%$cari%'";
	
	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging=pagingData($idx_page,"MacAddress","tbl_mac_address","&cari=$cari",$kondisi,"pengaturan_macaddress.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================
	
	$sql = 
		"SELECT *,f_cabang_get_name_by_kode(KodeCabang) AS NamaCabang
		FROM tbl_mac_address $kondisi 
		ORDER BY MacAddress LIMIT $idx_awal_record,$VIEW_PER_PAGE";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot Load cabang',__FILE__,__LINE__,$sql);
		echo("Err :".__LINE__);exit;
	}

	$idx_check=0;

	$i = $idx_page*$VIEW_PER_PAGE+1;

  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$idx_check++;
		
		$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[Id]'\"/>";
		
		$act 	="<a href='".append_sid('pengaturan_macaddress.'.$phpEx.'?mode=edit&id='.$row[0])."&page=".$idx_page."&cari=".$cari."'>Edit</a> + ";
		$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";

		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'		=>$odd,
					'check'	=>$check,
					'no'		=>$i,
					'mac_address'	=>$row['MacAddress'],
					'nama_komputer'	=>$row['NamaKomputer'],
					'cabang'	=>$row['NamaCabang'],
					'status'	=>($row['IsAktif']==1)?"AKTIF":"NONAKTIF",
					'action'=>$act
				)
			);
		
		$i++;
  }
	
	if($i-1<=0){
		$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
	}
	
	$page_title	= "Pengaturan Mac Address";

	$template->assign_vars(array(
		'BCRUMP'    		=>setBcrump($id_page),
		'U_ADD'					=> append_sid('pengaturan_macaddress.'.$phpEx.'?mode=add&page='.$idx_page.'&cari='.$cari),
		'ACTION_CARI'		=> append_sid('pengaturan_macaddress.'.$phpEx),
		'TXT_CARI'			=> $cari,
		'NO_DATA'				=> $no_data,
		'PAGING'				=> $paging
		)
	);
	
}      
elseif ($mode=='add'){
	// add 
	
	$pesan 	= $mac_id==""?$HTTP_GET_VARS['pesan']:$pesan;
	$page 	= $HTTP_GET_VARS['page'];
	$cari 	= $HTTP_GET_VARS['cari'];

	if($pesan==1){
		$pesan="<font color='green' size=3>Penyimpanan Berhasil!</font>";
		$bgcolor_pesan="98e46f";
	}
	
	$template->set_filenames(array('body' => 'pengaturan.macaddress/add.tpl'));

	$template->assign_vars(array(
	 'BCRUMP'						=>setBcrump($id_page),
	 'JUDUL'						=> 'Tambah Komputer',
	 'MODE'   					=> 'save',
	 'SUB'    					=> '0',
	 'PESAN'						=> $pesan,
	 'MAC_ADDRESS'			=> $mac_id,
	 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
	 'OPT_CABANG'				=> $Cabang->setInterfaceComboCabang(""),
	 'U_ADD_ACT'				=> append_sid('pengaturan_macaddress.'.$phpEx),
	 'U_BACK'						=> append_sid('pengaturan_macaddress.php?page='.$page.'&cari='.$cari),
	 'PAGE'							=> $page,
	 'CARI'							=> $cari
	 )
	);
} 
else if ($mode=='save'){
	//MENYIMPAN DATA
	
	$id 							= $HTTP_POST_VARS['id'];
	$mac_address 			= $HTTP_POST_VARS['mac_address'];
	$mac_address_old	= $HTTP_POST_VARS['mac_address_old'];
	$nama_komputer   	= $HTTP_POST_VARS['nama_komputer'];
	$cabang   				= $HTTP_POST_VARS['cabang'];
	$status						= $HTTP_POST_VARS['status'];
	$page 						= $HTTP_POST_VARS['page'];
	$cari 						= $HTTP_POST_VARS['cari'];

	$terjadi_error=false;
	
	if($MacAddress->periksaDuplikasi($mac_address) && $mac_address!=$mac_address_old){
		$pesan="<font color='white' size=3>Mac Address yang anda masukkan sudah terdaftar dalam sistem!</font>";
		$bgcolor_pesan="red";
	}
	else{
		
		if($submode==0){
			$judul="Tambah Data Komputer";
			$path	='<a href="'.append_sid('pengaturan_macaddress.'.$phpEx."?mode=add").'">Tambah Komputer</a> ';
			
			if($MacAddress->tambah($mac_address,$cabang,$nama_komputer)){
					
				redirect(append_sid('pengaturan_macaddress.'.$phpEx.'?mode=add&pesan=1',true));
				//die_message('<h2>Data cabang Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_macaddress.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
			}
		}
		else{
			
			$judul="Ubah Data Komputer";
			$path	='<a href="'.append_sid('pengaturan_macaddress.'.$phpEx."?mode=edit&id=$kode_old").'">Ubah Komputer</a> ';
			
			if($MacAddress->ubah($id,$mac_address,$cabang,$nama_komputer)){
					
					//redirect(append_sid('pengaturan_macaddress.'.$phpEx.'?mode=add',true));
					//die_message('<h2>Data cabang Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_macaddress.'.$phpEx.'?mode=edit&id='.$kode).'">Sini</a> Untuk Melanjutkan','');
					$pesan="<font color='green' size=3>Penyimpanan Berhasil!</font>";
					$bgcolor_pesan="98e46f";
			}
		}
		
		//exit;
		
	}

	$temp_var	= "selected_status".$status;
	$$temp_var= "selected";
	
	$template->set_filenames(array('body' => 'pengaturan.macaddress/add.tpl'));

	$template->assign_vars(array(
	 'BCRUMP'						=>setBcrump($id_page),
	 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
	 'PESAN'						=> $pesan,
	 'JUDUL'						=> $judul,
	 'MODE'   					=> 'save',
	 'SUB'    					=> $submode,
	 'ID'								=> $id,
	 'MAC_ADDRESS'			=> $mac_address,
	 'MAC_ADDRESS_OLD'	=> $mac_address_old,
	 'NAMA_KOMPUTER'		=> $nama_komputer,
	 'OPT_CABANG'				=> $Cabang->setInterfaceComboCabang($cabang),
	 'STATUS_1_SELECTED'=> $selected_status1,
	 'STATUS_0_SELECTED'=> $selected_status0,
	 'U_ADD_ACT'				=> append_sid('pengaturan_macaddress.'.$phpEx),
	 'U_BACK'						=> append_sid('pengaturan_macaddress.php?page='.$page.'&cari='.$cari),
	 'PAGE'							=> $page,
	 'CARI'							=> $cari
	 )
	);
	
	
} 
else if ($mode=='edit'){
	// edit
	
	$id 	= $HTTP_GET_VARS['id'];
	$page = $HTTP_GET_VARS['page'];
	$cari = $HTTP_GET_VARS['cari'];
	
	$row=$MacAddress->ambilDataDetail($id);
	
	$temp_var	= "selected_status".$row['IsAktif'];
	$$temp_var= "selected";

	$template->set_filenames(array('body' => 'pengaturan.macaddress/add.tpl'));

	$template->assign_vars(array(
		 'BCRUMP'						=>setBcrump($id_page),
		 'JUDUL'						=> 'Ubah Data Komputer',
		 'MODE'   					=> 'save',
		 'SUB'    					=> '1',
		 'ID'								=> $row['Id'],
		 'MAC_ADDRESS'			=> $row['MacAddress'],
		 'NAMA_KOMPUTER'		=> $row['NamaKomputer'],
		 'OPT_CABANG'				=> $Cabang->setInterfaceComboCabang($row['KodeCabang']),
		 'STATUS_1_SELECTED'=> $selected_status1,
		 'STATUS_0_SELECTED'=> $selected_status0,
		 'U_ADD_ACT'				=> append_sid('pengaturan_macaddress.'.$phpEx),
		 'U_BACK'						=> append_sid('pengaturan_macaddress.php?page='.$page.'&cari='.$cari),
		 'PAGE'							=> $page,
		 'CARI'							=> $cari
		 )
	);
} 
else if ($mode=='delete'){
	// aksi hapus cabang
	$list = str_replace("\'","'",$HTTP_GET_VARS['list']);
	//echo($list_cabang. " asli :".$HTTP_GET_VARS['list_cabang']);
	$MacAddress->hapus($list);
	
	exit;
} 

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>