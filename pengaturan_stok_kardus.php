<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassStokKardus.php');

// SESSION
$id_page = 708;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'])
{
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

	if($mode=='JenisKardus')
	{
		global $db;

		$StokKardus = new StokKardus();

		$sql = "select * from tbl_jenis_kardus";
		
		$template->set_filenames(array('body' => 'stok_kardus/jenis_kardus_body.tpl'));
		if ($result = $db->sql_query($sql))
		{
			$i = $idx_page*$VIEW_PER_PAGE+1;
		 	while ($row = $db->sql_fetchrow($result))
		 	{
			
				$act  ="<a  href='#' onclick='EditJenisKardus(\"$row[0]\");'>Edit</a> + ";
				$act .="<a  href='#' onclick='HapusJenisKardus(\"$row[0]\");'>Delete</a>";

				$template->
					assign_block_vars(
						'ROW',
						array(
							'no'=>$i,
							'ID_JENIS_KARDUS'=>$row['IdJenisKardus'],
							'NAMA_KARDUS'=>$row['NamaJenisKardus'],
							'KETERANGAN'=>$row['Keterangan'],
							'action'=>$act
						)
					);
				
				$i++;
		    }
		} 
		else
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 

		$template->assign_vars(array(
			'ID_GENERATE' => $StokKardus->latest_id(),
			)
		);

		$template->pparse('body');
	}
	elseif($mode=='TambahJenisKardus')
	{
		global $db;

		$StokKardus = new StokKardus();

		$IdJenisKardus = $HTTP_GET_VARS['IdJenisKardus'];
		$JenisKardus = $HTTP_GET_VARS['JenisKardus'];
		$Keterangan  = $HTTP_GET_VARS['Keterangan'];

		$sql = "insert into tbl_jenis_kardus(IdJenisKardus,NamaJenisKardus,Keterangan) 
				values('$IdJenisKardus','$JenisKardus','$Keterangan')";
		
		if (!$result = $db->sql_query($sql))
		{
			echo("Error:".__LINE__);exit;
		} 

		$StokKardus->generateKardus($IdJenisKardus);

	}
	elseif($mode=='EditJenisKardus')
	{
		global $db;
		$IdJenisKardus = $HTTP_GET_VARS['IdJenisKardus'];
		$JenisKardus = $HTTP_GET_VARS['JenisKardus'];
		$Keterangan  = $HTTP_GET_VARS['Keterangan'];

		$sql = "update tbl_jenis_kardus set NamaJenisKardus='$JenisKardus', Keterangan='$Keterangan' where IdJenisKardus='$IdJenisKardus'";
		
		if (!$result = $db->sql_query($sql))
		{
			echo("Error:".__LINE__);exit;
		} 
	}
	elseif($mode=='HapusJenisKardus')
	{
		global $db;

		$StokKardus = new StokKardus();

		$IdJenisKardus = $HTTP_GET_VARS['IdJenisKardus'];
		
		$sql = "delete from tbl_jenis_kardus where IdJenisKardus='$IdJenisKardus'";
		
		if (!$result = $db->sql_query($sql))
		{
			echo("Error:".__LINE__);exit;
		} 

		$StokKardus->destroyKardus($IdJenisKardus);
	}
	elseif($mode=='ShowTambahStok')
	{
		global $db;

		$KodeCabang = $HTTP_GET_VARS['KodeCabang'];
		
		$template->set_filenames(array('body' => 'stok_kardus/tambah_stok_kardus.tpl'));
		
		$sql = "
				SELECT tjk.*,tsk.JumlahStok
				FROM tbl_jenis_kardus tjk
				LEFT JOIN tbl_stok_kardus tsk
				ON tjk.IdJenisKardus=tsk.JenisKardus AND tsk.KodeCabang='$KodeCabang';";

		if ($result = $db->sql_query($sql))
		{
			$i = $idx_page*$VIEW_PER_PAGE+1;
		 	while ($row = $db->sql_fetchrow($result))
		 	{
				$template->
					assign_block_vars(
						'ROW',
						array(
							'no'=>$i,
							'ID_STOK_KARDUS'	=>$row['IdStokKardus'],
							'ID_JENIS_KARDUS'	=>$row['IdJenisKardus'],
							'NAMA_KARDUS'		=>$row['NamaJenisKardus'],
							'JUMLAH_STOK'		=>($row['JumlahStok']==null?'0':$row['JumlahStok']),
						)
					);
				$i++;
		    }
		} 
		else
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 

		$template->assign_vars(array(
			'KODE_CABANG' => $KodeCabang,
			)
		);

		$template->pparse('body');
	}
	elseif($mode=='TambahStokKardus')
	{
		global $db;

		$StokKardus = new StokKardus();

		$IdJenisKardus 	= $HTTP_GET_VARS['IdJenisKardus'];

		$KodeCabang 	= $HTTP_GET_VARS['KodeCabang'];
		$TambahStok 	= $HTTP_GET_VARS['TambahStok'];
		$StokAsal	 	= $HTTP_GET_VARS['StokAsal'];
		
		$query 		= "select * from tbl_stok_kardus where JenisKardus = '$IdJenisKardus' and KodeCabang = '$KodeCabang'";
		$result 	= $db->sql_query($query);
		$stok_data  = $db->sql_fetchrow($result);

		if($stok_data=="")
		{
			$sql = "insert into tbl_stok_kardus(KodeCabang,JenisKardus,JumlahStok) values('$KodeCabang','$IdJenisKardus','$TambahStok')";
			$db->sql_query($sql);
		}
		else
		{
			$sql  = "START TRANSACTION;";
			$sql2 =	"update tbl_stok_kardus set JumlahStok = JumlahStok + $TambahStok where JenisKardus = '$IdJenisKardus' and KodeCabang = '$KodeCabang';";
			$sql3 = "COMMIT;";
			
			$db->sql_query($sql);
			$db->sql_query($sql2);
			$db->sql_query($sql3);
		}


		$StokKardus->generateTambahLog($IdJenisKardus,$TambahStok,$KodeCabang,$StokAsal);

	}
	elseif($mode=='ShowStokOpname')
	{
		global $db;

		$KodeCabang = $HTTP_GET_VARS['KodeCabang'];
		
		$template->set_filenames(array('body' => 'stok_kardus/stok_opname_kardus.tpl'));
		
		$sql = "select * from tbl_stok_kardus,tbl_jenis_kardus where KodeCabang='$KodeCabang' and tbl_stok_kardus.JenisKardus = tbl_jenis_kardus.IdJenisKardus";
		if ($result = $db->sql_query($sql))
		{
			$i = $idx_page*$VIEW_PER_PAGE+1;
		 	while ($row = $db->sql_fetchrow($result))
		 	{
				$template->
					assign_block_vars(
						'ROW',
						array(
							'no'=>$i,
							'ID_STOK_KARDUS'	=>$row['IdStokKardus'],
							'ID_JENIS_KARDUS'	=>$row['IdJenisKardus'],
							'NAMA_KARDUS'		=>$row['NamaJenisKardus'],
							'JUMLAH_STOK'		=>$row['JumlahStok'],
						)
					);
				$i++;
		    }
		} 
		else
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 

		$template->assign_vars(array(
			'KODE_CABANG' => $KodeCabang,
			)
		);

		$template->pparse('body');
	}
	elseif($mode=='StokOpnameKardus')
	{
		global $db;

		$StokKardus = new StokKardus();

		$IdStokKardus 	= $HTTP_GET_VARS['IdStokKardus'];
		$KodeCabang 	= $HTTP_GET_VARS['KodeCabang'];
		$Keterangan 	= $HTTP_GET_VARS['Keterangan'];
		$StokOpname 	= $HTTP_GET_VARS['StokOpname'];
		$StokAsal	 	= $HTTP_GET_VARS['StokAsal'];
		
		$sql = "update tbl_stok_kardus set JumlahStok = $StokOpname where IdStokKardus = '$IdStokKardus'";
		
		if (!$result = $db->sql_query($sql))
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 

		$StokKardus->generateOpnameLog($IdStokKardus,$StokOpname,$KodeCabang,$StokAsal,$Keterangan);
	}
	elseif($mode=='ShowLogHistory')
	{
		global $db;

		$KodeCabang = $HTTP_GET_VARS['KodeCabang'];
		
		$template->set_filenames(array('body' => 'stok_kardus/log_history_kardus.tpl'));
		
		$sql = "select 
				tbl_log_kardus.*,
				tbl_jenis_kardus.NamaJenisKardus

				from tbl_log_kardus,tbl_stok_kardus,tbl_jenis_kardus 
				where 
					tbl_log_kardus.KodeCabang='$KodeCabang' 
				and 
					tbl_log_kardus.IdStokKardus = tbl_stok_kardus.IdStokKardus 
				and 
					tbl_stok_kardus.JenisKardus = tbl_jenis_kardus.IdJenisKardus
				order by 
					tbl_log_kardus.WaktuTransaksi desc";

		if ($result = $db->sql_query($sql))
		{
			$i = $idx_page*$VIEW_PER_PAGE+1;
		 	while ($row = $db->sql_fetchrow($result))
		 	{
				$template->
					assign_block_vars(
						'ROW',
						array(
							'no'=>$i,
							'ID_LOG'			=>$row['id_log_StokKardus'],
							'ID_STOK_KARDUS'	=>$row['IdStokKardus'],
							'JUMLAH_TRANSAKSI'	=>$row['JumlahTransaksi'],
							'DETAIL_TRANSAKSI'	=>$row['DetailTransaksi'],
							'WAKTU_TRANSAKSI'	=>$row['WaktuTransaksi'],
							'NAMA_KARDUS'		=>$row['NamaJenisKardus'],
							'STOK'				=>$row['Stok'],
						)
					);
				$i++;
		    }
		} 
		else
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 

		$template->pparse('body');
	}
	else
	{
		global $db;

		$StokKardus = new StokKardus();

		$sql = "select * from tbl_md_cabang order by Nama asc";

		if ($result = $db->sql_query($sql))
		{
		    $i = $idx_page*$VIEW_PER_PAGE+1;
		 	while ($row = $db->sql_fetchrow($result))
		 	{
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$act  = "<a href='#' onclick='ShowTambahStok(\"$row[0]\");'>Tambah Stok</a> + ";
				$act .= "<a href='#' onclick='ShowStokOpname(\"$row[0]\");'>Stok Opname</a> + ";
				$act .= "<a href='#' onclick='ShowLogHistory(\"$row[0]\");'>Log History</a>";

				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'no'=>$i,
							'kodecabang'=>$row['KodeCabang'],
							'namacabang'=>$row['Nama'],
							'kota'=>$row['Kota'],
							'stok'=> $StokKardus->getStok($row['KodeCabang']),

							'action'=>$act
						)
					);
				
				$i++;
		    }
		} 
		else
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 

		$page_title	= "Master Data Stok Kardus";
		
		//paramter sorting
		//$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
		//$parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";
		
		$template->set_filenames(array('body' => 'stok_kardus/kardus_body.tpl'));

    $template->assign_vars(array(
			'BCRUMP'    					=>setBcrump($id_page),
			'ACTION_CARI'					=> append_sid('pengaturan_user.'.$phpEx),
			'TXT_CARI'						=> $cari,
			'PAGING'						  => $paging,
			)
		);
	include($adp_root_path . 'includes/page_header.php');
	$template->pparse('body');
	include($adp_root_path . 'includes/page_tail.php');
	}


?>