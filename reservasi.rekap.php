<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$bulan	= ($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");

//LIST BULAN
$list_data_bulan = "";

for($idx_bln=1;$idx_bln<=12;$idx_bln++)
{
	$font_size	= 2;
	$font_color ='';
	
	if($bulan==$idx_bln)
	{
		$font_size = 4;
		$font_color='008609';
	}
	
	$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}

//memilih tabel yang akan diambil
$calendar 				= CAL_GREGORIAN;
$tanggal_sekarang		= dateNow(); 
$temp_tanggal_sekarang	= explode("-",$tanggal_sekarang);
$tahun_sekarang			= $temp_tanggal_sekarang[0];
$bulan_sekarang			= $temp_tanggal_sekarang[1];
$tgl_hari_sekarang		= $temp_tanggal_sekarang[2];

if($tahun==$tahun_sekarang && $bulan==$bulan_sekarang)
{
	//jika tahun dan bulan adalah bulan sekarang
	$tbl_reservasi	= "tbl_reservasi";
}
else
{
	$tbl_reservasi	= "tbl_reservasi";
}

//jumlah hari dalam 1 bulan
$jumlah_hari = cal_days_in_month($calendar,$bulan,$tahun);

for($i=1;$i<=$jumlah_hari;$i++)
{
	$list_tanggal .= "<td>$i</td>";
}

$sql_jurusan = 
	"SELECT 
		IdJurusan, 
		CONCAT(f_cabang_get_name_by_kode(KodeCabangAsal),' - ',f_cabang_get_name_by_kode(KodeCabangTujuan)) as Jurusan,
		f_cabang_get_kota_by_kode_cabang(KodeCabangAsal) as Kota  
	FROM 
		tbl_md_jurusan
	ORDER BY KOTA asc, Jurusan asc";

if($result_jurusan = $db->sql_query($sql_jurusan))
{
	$index = 1;
	while($data_jurusan = $db->sql_fetchrow($result_jurusan))
	{
		$total = 0;
		$list_data .= "<tr class='".($index % 2 == 0 ? 'odd' : 'even')."'>";

		$list_data .= "<td>".$data_jurusan['Jurusan']."</td>";
		$list_data .= "<td>".$data_jurusan['Kota']."</td>";

		for($i=1;$i<=$jumlah_hari;$i++)
		{
			$jumlah_reservasi = "";

			$sql = "
				SELECT 
					IdJurusan,
					WEEKDAY(WaktuCetakTiket)+1 AS Hari,
					DAY(WaktuCetakTiket) AS Tanggal,
					IS_NULL(COUNT(NoTiket),0) AS JumlahReservasi
				FROM 
					tbl_reservasi
				WHERE 
					MONTH(WaktuCetakTiket)= $bulan
				AND 
					YEAR(WaktuCetakTiket)= $tahun
				AND
					CetakTiket=1 
				AND 
					FlagBatal!=1 
				AND 
					JenisPembayaran = 0
				AND
					IdJurusan = $data_jurusan[IdJurusan]
				Group by Date(WaktuCetakTiket)
				order by (WaktuCetakTiket) ";
	
			if($result = $db->sql_query($sql))
			{
				$list_data .= "<td align=right>";
	
				while($row = $db->sql_fetchrow($result))
				{
					$jumlah_reservasi .= ($row['Tanggal'] == $i ? $row['JumlahReservasi'] : "");
				}
				$list_data .= ($jumlah_reservasi != "" ? "<b>".$jumlah_reservasi."</b>"  : "<font color='red'>0</font>" );
				$list_data .= "</td>";
			}
			else
			{
				echo("Error:".__LINE__);exit;
			}

			$total = $total + $jumlah_reservasi;
		}

		$list_data .= "<td align='right'><b>".$total."</b></td>";
		$list_data .= "</tr>";
		$index++;
	}
} 
else
{
	echo("Error:".__LINE__);exit;
}

for($i=1;$i<=$jumlah_hari;$i++)
{
	$jumlah_total = "";

	$sql = "
		SELECT 
			DAY(WaktuCetakTiket) AS Tanggal,
			IS_NULL(COUNT(NoTiket),0) AS JumlahReservasi
		FROM 
			tbl_reservasi
		WHERE 
			MONTH(WaktuCetakTiket)= $bulan
		AND 
			YEAR(WaktuCetakTiket)= $tahun
		AND
			CetakTiket=1 
		AND 
			FlagBatal!=1 
		AND 
			JenisPembayaran = 0
		Group by Date(WaktuCetakTiket)
		order by (WaktuCetakTiket)";

	if($result = $db->sql_query($sql))
	{
		$list_total .= "<td align=right>";

		while($row = $db->sql_fetchrow($result))
		{
			$jumlah_total .= ($row['Tanggal'] == $i ? $row['JumlahReservasi'] : "");
		}
		$list_total .= ($jumlah_total != "" ? "<b>".$jumlah_total."</b>"  : "<font color='red'>0</font>" );
		$list_total .= "</td>";
	}
	else
	{
		echo("Error:".__LINE__);exit;
	}
	$total_reservasi = $total_reservasi + ($jumlah_total != "" ? $jumlah_total : 0 );
}

$act 	= "<a href='#' onClick='Start(\"".append_sid('laporan_rekap_tiket_detail_user.php?p0='.$tanggal_mulai.'&p1='.$tanggal_akhir.'&p2='.$userdata['user_id'].'&p3=3')."\");return false'>Detail<a/>&nbsp;+&nbsp;";		

$act 	.= "<a href='#' onClick='Start(\"".append_sid("laporan_rekap_uang_user_cetak_struk.php?p0=$tahun-$bulan-".($idx_tgl+1))."\");return false'>Cetak<a/>";	

//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$bulan."&p2=".$tahun;
	
$script_cetak_pdf="Start('laporan_rekap_uang_user_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('reservasi_rekap_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

// LIST
$template->set_filenames(array('body' => 'reservasi/reservasi.rekap.tpl'));

$template->assign_vars(array(
	'BCRUMP'    						=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('laporan_rekap_uang_user.'.$phpEx).'">Laporan Rekap Uang</a>',
	'URL'								=> append_sid('reservasi.rekap.php'.$parameter),
	'LIST_BULAN'						=> "| ".$list_bulan,
	'LIST_TANGGAL'						=> $list_tanggal,
	'LIST_DATA'							=> $list_data,
	'LIST_TOTAL'						=> $list_total,
	'TOTAL_KESELURUHAN'					=> $total_reservasi,
	'RESERVASI_PERTANGGAL'				=> $data_pertanggal,
	'JUMLAH_HARI'						=> $jumlah_hari,
	'BULAN'								=> $bulan,
	'TAHUN'								=> $tahun,
	'U_laporan_rekap_uang_user_GRAFIK'	=> append_sid('laporan_rekap_uang_user_grafik.'.$phpEx).'&bulan='.$bulan.'&tahun='.$tahun,
	'CETAK_PDF'							=> $script_cetak_pdf,
	'CETAK_XL'							=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>