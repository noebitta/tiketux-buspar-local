<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 304;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$template->set_filenames(array('body' => 'laporan_omzet_jadwal/laporan_omzet_jadwal_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Cabang								= new Cabang();

if(in_array($userdata['user_level'],array($LEVEL_ADMIN))){
	$kondisi_cabang	= ($kode_cabang=="")?"":" AND KodeCabang='$kode_cabang'";
	$cabang_default	= "";
}
else{
	$kondisi_cabang	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]' ";
	$cabang_default	= $userdata['KodeCabang'];
}

//AMBIL DATA CABANG
//Cabang Asal
$data_cabang_asal		= $Cabang->ambilDataDetail($asal);
$data_cabang_tujuan	= $Cabang->ambilDataDetail($tujuan);

$keterangan_asal	= ($data_cabang_asal['Nama']=="")?"Semua Asal":"$data_cabang_asal[Nama] ($data_cabang_asal[KodeCabang]) $data_cabang_asal[Kota]"; 
$keterangan_tujuan= ($data_cabang_tujuan['Nama']=="")?"Semua Tujuan":"$data_cabang_tujuan[Nama] ($data_cabang_tujuan[KodeCabang]) $data_cabang_tujuan[Kota]"; 

if($asal!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'";
}

if($asal!="" && $tujuan!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'";
}


$sql=
	"SELECT 
		IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(IF(JenisPenumpang='U',1,0)),0) AS TotalPenumpangU,
		IS_NULL(SUM(IF(JenisPenumpang='M',1,0)),0) AS TotalPenumpangM,
		IS_NULL(SUM(IF(JenisPenumpang='K',1,0)),0) AS TotalPenumpangK,
		IS_NULL(SUM(IF(JenisPenumpang='KK',1,0)),0) AS TotalPenumpangKK,
		IS_NULL(SUM(IF(JenisPenumpang='G',1,0)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(DISTINCT(NoSPJ)),0) AS TotalBerangkat,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE  (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY Jam
	ORDER BY Jam ";

//EXPORT KE MS-EXCEL
	
	if ($result = $db->sql_query($sql)){
			
		$i=1;
		
		$objPHPExcel = new PHPExcel();          
	  $objPHPExcel->setActiveSheetIndex(0);  
	  $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
	  $objPHPExcel->getActiveSheet()->mergeCells('A2:K2');
	  
		//HEADER
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Per Jam periode '.$tanggal_mulai." s/d ".$tanggal_akhir." Asal:".$keterangan_asal." Tujuan:".$keterangan_tujuan);
	  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Jam');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Total Berangkat');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jum.Pnp Umum');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jum.Pnp Mahasiswa dll');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Jum.Pnp Khusus');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jum.Pnp Keluarga karyawan');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jum.Pnp Gratis');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jum.Total Pnp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jum.Pnp');
		$objPHPExcel->getActiveSheet()->getColumnDimension('H3')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Rata-rata.Pnp/Berangkat');
		$objPHPExcel->getActiveSheet()->getColumnDimension('I3')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Total Omzet');
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Total Disc.');
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		
		$row = $db->sql_fetchrow($result);
	
		$sum_berangkat			= 0;
		$sum_penumpang_u		= 0;
		$sum_penumpang_m		= 0;
		$sum_penumpang_k		= 0;
		$sum_penumpang_kk		= 0;
		$sum_penumpang_g		= 0;
		$sum_penumpang			= 0;
		$sum_omzet					= 0;
		$sum_discount				= 0;
		
		$idx_row	=4;
		
		for($idx_jam=0;$idx_jam<24;$idx_jam++){
		
			if($row['Jam']==$idx_jam){
				$total_berangkat		= $row['TotalBerangkat']; 
				$total_penumpang_u	= $row['TotalPenumpangU']; 
				$total_penumpang_m	= $row['TotalPenumpangM']; 
				$total_penumpang_k	= $row['TotalPenumpangK']; 
				$total_penumpang_kk	= $row['TotalPenumpangKK']; 
				$total_penumpang_g	= $row['TotalPenumpangG']; 
				$total_penumpang		= $row['TotalPenumpang']; 
				$total_omzet				= $row['TotalOmzet']; 
				$total_discount			= $row['TotalDiscount']; 
				$row = $db->sql_fetchrow($result);
			}
			else{
				$total_berangkat		= 0; 
				$total_penumpang_u	= 0; 
				$total_penumpang_m	= 0; 
				$total_penumpang_k	= 0; 
				$total_penumpang_kk	= 0; 
				$total_penumpang_g	= 0; 
				$total_penumpang		= 0; 
				$total_omzet				= 0; 
				$total_discount			= 0; 
			}
			
			$rata_penumpang_per_trip	=($total_berangkat>0)?$total_penumpang/$total_berangkat:0;
			
			$temp_row	= $idx_row+$idx_jam;
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$temp_row, $idx_jam);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$temp_row, $total_berangkat);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$temp_row, $total_penumpang_u);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$temp_row, $total_penumpang_m);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$temp_row, $total_penumpang_k);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$temp_row, $total_penumpang_kk);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$temp_row, $total_penumpang_g);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$temp_row, $total_penumpang);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$temp_row, $rata_penumpang_per_trip);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$temp_row, $total_omzet);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$temp_row, $total_discount);
			
			
		}
		
		$temp_idx			= $temp_row;		
		$temp_idx_2		= $temp_row-1;		
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$temp_idx, 'TOTAL');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$temp_idx, '=SUM(B'.$idx_row.':B'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$temp_idx, '=SUM(C'.$idx_row.':C'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$temp_idx, '=SUM(D'.$idx_row.':D'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$temp_idx, '=SUM(E'.$idx_row.':E'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$temp_idx, '=SUM(F'.$idx_row.':F'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$temp_idx, '=SUM(G'.$idx_row.':G'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$temp_idx, '=SUM(H'.$idx_row.':H'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$temp_idx, '=SUM(I'.$idx_row.':I'.$temp_idx_2.')');
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$temp_idx, '=H'.$temp_idx.'/B'.$temp_idx.')');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$temp_idx, '=SUM(K'.$idx_row.':K'.$temp_idx_2.')');

		
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 
	  
		if ($idx_jam>0){
			header('Content-Type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment;filename="Laporan Per Jam '.$tanggal_mulai." s/d ".$tanggal_akhir.'.xls"');
	    header('Cache-Control: max-age=0');

	    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	    $objWriter->save('php://output'); 
		}
	}
	else{
		die_error('Err:',__LINE__);
	}   
  
  
?>
