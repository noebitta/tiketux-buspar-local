<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 704;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#############################################################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//######################################################################################################################

//INCLUDE
include($adp_root_path . 'ClassCabang.php');

// PARAMETER
$mode 			= getVariabel("mode");

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

//ROUTER================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST
    $cari         = getVariabel("cari");
    $idx_page     = getVariabel("idxpage");
    $order_by     = getVariabel("orderby");
    $sort         = getVariabel("sort");
    $scroll_value = getVariabel("scrollvalue");

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));
    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    exit;

  case 1:
    //TAMBAH DATA
    showInputData();

    exit;

  case 2:
    //SHOW DATA
    $id_cabang         = getVariabel("idcabang");
    showDialogUbah($id_cabang);

    break;

  case 3:
    //SHOW DIALOG TAMBAH DATA
    showDialogTambah();

    exit;

  case 3.1:
      //SIMPAN DATA
      $id_cabang        = getVariabel("idcabang");
      $kode_cabang      = getVariabel("kodecabang");
      $nama             = getVariabel("nama");
      $alamat           = getVariabel("alamat");
      $kota             = getVariabel("kota");
      $telp             = getVariabel("telp");
      $fax              = getVariabel("fax");
      $is_aktif       = getVariabel("isaktif");

      echo(prosesSimpan(strtoupper($kode_cabang),(strtoupper($nama)),(strtoupper($alamat)),(strtoupper($kota)),$telp,$fax,$is_aktif,$id_cabang));

      exit;

  case 4:
    //HAPUS
    $id_cabang         = getVariabel("idcabang");
    echo $id_cabang;
    echo(hapusData($id_cabang));

    break;

  case 5:
    //UBAH STATUS AKTIF
    $id_cabang  = getVariabel("idcabang");

    ubahStatusAktif($id_cabang);

    break;

}

//METHODES & PROCESSES==================================================================================================

function showData(){

  global $db,$cari,$order_by,$sort,$idx_page,$template,$VIEW_PER_PAGE,$userdata,$id_page,$scroll_value;

  $Cabang = new Cabang();

  $template->set_filenames(array('body' => 'pengaturan.cabang/index.tpl'));


  //cek permission untuk CRUD
  $Permission = new Permission();

  if($Permission->isPermitted($userdata["user_level"],$id_page.".1")){
    $template->assign_block_vars("CRUD_ADD",array());
    $template->assign_block_vars("CRUD_DEL",array());
    $template->assign_block_vars("EXPORT",array());
  }

  $result = $Cabang->ambilData($order_by,$sort,$idx_page,$cari);

  //PAGING==============================================================================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==========================================================================================================

    $no = 0;

    while($row=$db->sql_fetchrow($result)){
        $no++;

        $odd = ($no%2)==0?"even":"odd";

        if($row["StatusAktif"]=="AKTIF"){
            $class_crud_aktif = "crudgreen";
        }
        else{
            $odd  = "red";
            $class_crud_aktif = "crud";
        }

    $template->assign_block_vars(
      'ROW',array(
        'odd'             => $odd,
        'idx'             => $no,
        'no'              => $idx_page*$VIEW_PER_PAGE+$no,
        'idcabang'        => $row['IdCabang'],
        'kode'            => $row["KodeCabang"],
        'nama'            => $row["Nama"],
        'alamat'          => $row["Alamat"],
        'kota'            => $row["Kota"],
        'telp'            => $row["Telp"],
        'fax'             => $row["Fax"],
        'aktif'           => $row["StatusAktif"],
        'classcrudaktif'  => $class_crud_aktif
      )
    );

    //ACTION
    $template->assign_block_vars("ROW.ACT_EDIT", array());
    $template->assign_block_vars("ROW.ACT_DEL", array());

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	    => basename(__FILE__),
      'CARI'          => $cari,
      'ORDER'         => $order_by,
      'SORT'          => $sort,
      'IDX_PAGE'      => $idx_page,
      'PAGING'        => $paging,
      'U_EXPORT_EXCEL'=> substr(basename(__FILE__),0,-4).".export.excel.php",
      'SCROLL_VALUE'  => ($scroll_value==""?0:$scroll_value),
      'JUDUL_HALAMAN' => "pengaturan cabang",
      'JUDUL_TABEL'   => "data cabang"
    )
  );

  $template->pparse('body');

} //showData

function ubahStatusAktif($id_cabang){
    $Cabang = new Cabang();

    if($Cabang->ubahStatusAktif($id_cabang)){
        $ret_val["status"]  = "OK";
    }
    else{
        $ret_val["status"]  = "GAGAL";
    }

    echo(json_encode($ret_val));
}//ubahStatusAktif

function showDialogTambah(){
  global $template;

  $template->set_filenames(array('body' => 'pengaturan.cabang/detail.tpl'));

  $template->assign_vars(array(
      "JUDUL"	          => "tambah data cabang",
      "STATUS_AKTIF"      => "0"
    )
  );

  $template->pparse('body');
}//showDialogTambah

function showDialogUbah($id_cabang){
    global $template;

    $Cabang = new Cabang();

    $template->set_filenames(array('body' => 'pengaturan.cabang/detail.tpl'));

    $data = $Cabang->ambilDetailData($id_cabang);

    $template->assign_vars(array(
            "JUDUL"	          => "ubah data cabang",
            'ID_CABANG'       => $data["IdCabang"],
            'KODE_CABANG'     => $data["KodeCabang"],
            'NAMA'            => $data["Nama"],
            'ALAMAT'          => $data["Alamat"],
            'KOTA'            => $data["Kota"],
            'TELP'            => $data["Telp"],
            'FAX'             => $data["Fax"],
            "STATUS_AKTIF"    => $data["IsAktif"]
        )
    );
    $template->pparse('body');
}//showDialogUbah

function prosesSimpan($kode_cabang,$nama,$alamat,$kota,$telp,$fax,$is_aktif,$id_cabang=""){

    $Cabang = new Cabang();

    if($id_cabang=="") {
        //proses tambah
        $id_cabang=$Cabang->tambah($kode_cabang,$nama,$alamat,$kota,$telp,$fax,$is_aktif);
    }
    else {
        $Cabang->ubah($id_cabang,$kode_cabang,$nama,$alamat,$kota,$telp,$fax,$is_aktif);
    }
    $return = array("status" => "OK", "idcabang" => $id_cabang);
    return json_encode($return);
}//prosesSimpan

function hapusData($id_cabang){
    $Cabang = new Cabang();

    $Cabang->hapus($id_cabang);

    $return = array("status" => "OK","id_cabang"=>$id_cabang);

    return json_encode($return);
}

?>