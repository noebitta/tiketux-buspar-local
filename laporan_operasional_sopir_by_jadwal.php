<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 408;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$periode        = isset($HTTP_GET_VARS['periode'])? $HTTP_GET_VARS['periode'] : $HTTP_POST_VARS['periode'];
$tanggal_mulai  = isset($HTTP_POST_VARS['tanggal_mulai'])?$HTTP_POST_VARS['tanggal_mulai']:$HTTP_GET_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_POST_VARS['tanggal_akhir'])?$HTTP_POST_VARS['tanggal_akhir']:$HTTP_GET_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}

$kondisi_cari	=($cari=="")?"":
    " WHERE KodeSopir LIKE '$cari%' 
		OR Nama LIKE '%$cari%' 
		OR Norek LIKE '%$cari%'
		OR HP LIKE '%$cari%'
		OR NoSIM LIKE '%$cari%'";

// LIST
$template->set_filenames(array('body' =>'laporan_biaya_keberangkatan_supir/laporan_operasional_supir_by_jadwal_body.tpl'));

// BIKIN TABEL VIEW UNTUK MENAMPUNG JUMLAH JALAN SUPIR PER CABANG
$sql = "CREATE OR REPLACE VIEW view_cgs_by_jadwal AS   
            SELECT
                KodeDriver,
                Driver,
                COUNT(DISTINCT(NoSPJ)) AS Total_Jalan,
                KodeCabangAsal,
                CONCAT_WS('-',f_cabang_get_name_by_kode(KodeCabangAsal),f_cabang_get_name_by_kode(KodeCabangTujuan)) AS Jurusan,
                IF(Kota = 'BANDUNG',f_cabang_get_name_by_kode(KodeCabangTujuan),f_cabang_get_name_by_kode(KodeCabangAsal)) AS CabangJakarta,
                tbl_spj.IdJurusan,
                IFNULL((SELECT BiayaSopir FROM tbl_md_jurusan WHERE KodeCabangAsal = tbl_md_sopir.KodeCabang ORDER BY BiayaSopir DESC LIMIT 1),0) BiayaSopir,
                COUNT(DISTINCT(NoSPJ)) * IFNULL((SELECT BiayaSopir FROM tbl_md_jurusan WHERE KodeCabangAsal = tbl_md_sopir.KodeCabang ORDER BY BiayaSopir DESC LIMIT 1),0) AS CGS
            FROM
                tbl_spj
            JOIN
                tbl_md_jurusan
            ON
                tbl_spj.IdJurusan = tbl_md_jurusan.IdJurusan
            JOIN 
                tbl_md_cabang 
            ON 
                tbl_md_cabang.KodeCabang = tbl_md_jurusan.KodeCabangAsal
            JOIN
				tbl_md_sopir
			ON	
				tbl_spj.KodeDriver = tbl_md_sopir.KodeSopir
            WHERE
                DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'
            GROUP BY
                KodeDriver,
                IdJurusan
            ORDER BY
                Driver";
if (! $db->sql_query($sql)){
    die("Error:".__LINE__);
}

$biaya_sopir = array();
//  QUERY AMBIL DATA JML JALAN SUPIR DARI TABEL VIEW DI ATAS
$sql = "SELECT KodeDriver, Driver,
            GROUP_CONCAT(Total_Jalan ORDER BY IdJurusan) AS JML,
            GROUP_CONCAT(BiayaSopir ORDER BY IdJurusan) AS BIAYA,
            GROUP_CONCAT(Jurusan ORDER BY IdJurusan) AS Jurusan,
            SUM(CGS) AS Total_CGS
            FROM view_cgs_by_jadwal
            GROUP BY KodeDriver
            ORDER BY Driver ASC";
if (!$biaya = $db->sql_query($sql)){
    die("Error:".__LINE__.mysql_error());
}
while($row = $db->sql_fetchrow($biaya)){
    $biaya_sopir[$row['KodeDriver']] = $row;
}


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"KodeSopir","tbl_md_sopir","&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&cari=$cari",$kondisi_cari,"laporan_operasional_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

// QUERY AMBIL LIST SUPIR
$sql="SELECT *, f_cabang_get_name_by_kode(KodeCabang) AS Cabang
          FROM tbl_md_sopir
          $kondisi_cari
          ORDER BY Nama ASC";
if (!$sopir = $db->sql_query($sql)){
    die("Error:".__LINE__);
}

//isi array temp laporan

$temp_array=array();

$idx=0;
$cgs="";
$total=0;
while ($row = $db->sql_fetchrow($sopir)){
    $temp_array[$idx]['KodeSopir']					= $row['KodeSopir'];
    $temp_array[$idx]['Nama']                       = $row['Nama'];
    $temp_array[$idx]['Norek']                      = $row['Norek'];
    $temp_array[$idx]['Cabang']                     = $row['Cabang'];
    $temp_array[$idx]['Trip']                       = str_replace(',','<br>',$biaya_sopir[$row['KodeSopir']]['JML']);
    $temp_array[$idx]['Jurusan']                    = str_replace(',','<br>',$biaya_sopir[$row['KodeSopir']]['Jurusan']);
    $temp_array[$idx]['Cgs']                        = number_format($biaya_sopir[$row['KodeSopir']]['Total_CGS'],0,',','.');

    $array_biaya = explode(',',$biaya_sopir[$row['KodeSopir']]['BIAYA']);
    $array_trip  = explode(',',$biaya_sopir[$row['KodeSopir']]['JML']);

    foreach ($array_biaya as $item => $value){
        if($value != ""){
            $cgs .= number_format($value,0,',','.').",";
        }else{
            $cgs .= $value.",";
        }


        //$total += $array_trip[$item]*$value;
    }

    $temp_array[$idx]['Biaya']  = str_replace(',','<br>',rtrim($cgs,","));
    //$temp_array[$idx]['Cgs']    = number_format($total,0,',','.');

    $idx++;
    unset($cgs);
    //unset($total);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
    $odd ='odd';

    if (($idx % 2)==0){
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'       =>$odd,
            'no'        =>$idx+1,
            'kodesopir' =>$temp_array[$idx]['KodeSopir'],
            'Supir'     =>$temp_array[$idx]['Nama'],
            'Norek'     =>$temp_array[$idx]['Norek'],
            'Tugas'     =>$temp_array[$idx]['Cabang'],
            'Cabang'    =>$temp_array[$idx]['Jurusan'],
            'Trip'      =>$temp_array[$idx]['Trip'],
            'Biaya'     =>$temp_array[$idx]['Biaya'],
            'TotalCGS'  =>$temp_array[$idx]['Cgs']
        )
    );
    $idx++;
}

$page_title	= "Laporan Operasional Sopir";

$template->assign_vars(array(
        'BCRUMP'    	  =>setBcrump($id_page),
        'URL'			      => append_sid('laporan_operasional_sopir_by_jadwal.'.$phpEx),
        'TGL_AWAL'		  => $tanggal_mulai,
        'TGL_AKHIR'		  => $tanggal_akhir,
        'SUM_BERANGKAT'	=> number_format($sum_berangkat,0,",","."),
        'SUM_BIAYA'     => number_format($sum_biaya,0,",","."),
        'PAGING'		    => $paging,
        'TXT_CARI'		  => $cari,
    )
);
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>