<?php
// HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 229;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassCabang.php');

// PARAMETER
$mode 			      = getVariabel('mode');
$periode_awal     = getVariabel('filtglawal');
$periode_akhir    = getVariabel('filtglakhir');
$fil_tujuan       = getVariabel('tujuan');
$cabang_tujuan_id = getVariabel('cabangtujuanid');
$cari             = getVariabel('cari');
$idx_page         = getVariabel('idxpage');
$order_by         = getVariabel('orderby');
$sort             = getVariabel('sort');

//INSTANCE
$Paket      = new Paket();
$Cabang     = new Cabang();

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;
$order_by   = $order_by==""?1:$order_by;

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));
    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    break;

  case 1:
    //SET COMBO TUJUAN

    $cari         = getVariabel('filter');

    setListCabangTujuan($cari);

    break;

  case 2:
    //SHOW DIALOG KURIR

    showDialogKurir();

    break;

  case 3:
    //PROSES ANTARAN

    $list_resi     = getVariabel('listresi');
    $nama_kurir    = getVariabel('namakurir');

    prosesAntar($list_resi,$nama_kurir);

    break;

}

//METHODES & PROCESS ========================================================================================================================================================================

function showData(){

  global $config,$db,$periode_awal,$periode_akhir,$fil_tujuan,$cari,$order_by,$sort,$idx_page,$template,$Paket,$VIEW_PER_PAGE;

  $template->set_filenames(array('body' => 'paket.antaran/index.tpl'));

  $periode_awal   = $periode_awal==""?date("d-m-Y"):$periode_awal;
  $periode_akhir  = $periode_akhir==""?date("d-m-Y"):$periode_akhir;

  $filter         = "(DATE(tp.TglBerangkat) BETWEEN '".FormatTglToMySQLDate($periode_awal)."' AND '".FormatTglToMySQLDate($periode_akhir)."')".($fil_tujuan==""?"":" AND CabangTujuanAkhir='$fil_tujuan'");

  $result = $Paket->ambilDaftarAntaranPaket($order_by,$sort,$idx_page,$cari,$filter);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd =($no%2)==0?"even":"odd";

    if($row["Status"]=="Belum Diantar"){
      $classrow = $odd;
      $remark   = "";
      $act      = "selectRow(this,$no)";
      $classchk = "b_chk";
    }
    else{
      $classrow = "greencell";
      $remark   = "Kurir: ".$row["KurirAntar"]." | Waktu antar:".dateparseWithTime(FormatMySQLDateToTglWithTime($row["WaktuAntar"]));
      $act      = "";
      $classchk = "b_ok";
    }

    $template->assign_block_vars(
      'ROW',array(
        'odd'           => $odd,
        'classchk'      => $classchk,
        'classrow'      => $classrow,
        'act'           => $act,
        'no'            => $idx_page*$VIEW_PER_PAGE+$no,
        'noresi'        => $row["NoResi"],
        'tglberangkat'  => dateparse(FormatMySQLDateToTgl($row["TglBerangkat"])),
        'kodejadwal'    => $row["KodeJadwal"],
        'tujuanakhir'   => $row["TujuanAkhir"],
        'status'        => $row["Status"],
        'namapengirim'  => $row["NamaPengirim"],
        'telppengirim'  => $row["TelpPengirim"],
        'alamatpengirim'=> $row["AlamatPengirim"],
        'namapenerima'  => $row["NamaPengirim"],
        'telppenerima'  => $row["TelpPengirim"],
        'alamatpenerima'=> $row["AlamatPenerima"],
        'berat'         => $row["Berat"],
        'layanan'       => $config["layanan_paket"][$row["Layanan"]],
        'keterangan'    => $row["KeteranganPaket"],
        'remark'        => $remark
      )
    );
  }


  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      "TGL_AWAL"          => $periode_awal,
      "TGL_AKHIR"         => $periode_akhir,
      'CARI'              => $cari,
      'FIL_TUJUAN'        => $fil_tujuan,
      'ORDER'             => $order_by,
      'SORT'              => $sort,
      'IDX_PAGE'          => $idx_page,
      'PAGING'            => $paging
    )
  );

  $template->pparse('body');

}

function setListCabangTujuan($cari=""){
  global $db,$Cabang;

  $Cabang = new Cabang();

  $result = $Cabang->setComboListCabang($cari);

  while($row=$db->sql_fetchrow($result)){
    $list_cabang[]=array("group"=>$row["Kota"],"id"=>$row["KodeCabang"],"text"=>$row["Nama"]);
  }

  $ret_val  =json_encode($list_cabang);

  echo($ret_val);
} //setListCabangTujuan

function showDialogKurir(){
  global $template;

  $template->set_filenames(array('body' => 'paket.antaran/dialogkurir.tpl'));

  $template->pparse('body');


} //showDialogKurir

function prosesAntar($list_resi,$nama_kurir){

  global $Paket;

  $list_resi  = str_replace("\\'","'",$list_resi);

  if($Paket->antarBarang($list_resi,$nama_kurir)){
    $ret_val["status"]  = "OK";
    $ret_val["pesan"]   = "";
  }
  else{
    $ret_val["status"]  = "GAGAL";
    $ret_val["pesan"]   = "terjadi kegagalan proses checkin!";
  }

  echo(json_encode($ret_val));

} //prosesAntar

?>