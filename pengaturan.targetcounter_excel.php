<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 711;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$bulan          = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun          = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$bulan          = $bulan==""?date("m"):$bulan;
$bulan          = (int)$bulan;
$tahun          = $tahun==""?date("Y"):$tahun;

/*MENGAMBIL DATA TARGET CABANG DARI TABEL TARGET*/
$sql ="SELECT KodeCabang,TargetPax,TargetPaxTercapai,TargetPaket,TargetPaketTercapai,Log
                FROM tbl_md_cabang_target
                WHERE BulanTarget='$bulan' AND TahunTarget='$tahun'
                ORDER BY KodeCabang ASC";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".__LINE__);exit;
}
while($row=$db->sql_fetchrow($result)){
    $data_target[$row['KodeCabang']]['TargetPax']          = $row['TargetPax'];
    $data_target[$row['KodeCabang']]['TargetPaxTercapai']  = $row['TargetPaxTercapai'];
    $data_target[$row['KodeCabang']]['TargetPaket']        = $row['TargetPaket'];
    $data_target[$row['KodeCabang']]['TargetPaketTercapai']= $row['TargetPaketTercapai'];
    $data_target[$row['KodeCabang']]['Log']                = $row['Log'];
}

/*MENGAMBIL JUMLAH TIKET YANG DIBELI COUNTER DIJAKARTA */
$sql="SELECT tbl_reservasi.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta, Kota
                FROM tbl_reservasi
                JOIN tbl_md_cabang ON tbl_reservasi.KodeCabang = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = $bulan AND YEAR(TglBerangkat) = $tahun AND CetakTiket = 1
                AND FlagBatal != 1 AND Kota = 'JAKARTA'
                GROUP BY tbl_reservasi.KodeCabang";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".mysql_error());exit;
}
while($row=$db->sql_fetchrow($result)){
    $tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'] = $row['JmlFromJakarta'];
}

/*MENGAMBIL JUMLAH PAKET DARI COUNTER JAKARTA*/
$sql="SELECT tbl_paket.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta, Kota
                FROM tbl_paket
                JOIN tbl_md_cabang ON tbl_paket.KodeCabang = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = $bulan AND YEAR(TglBerangkat) = $tahun AND CetakTiket = 1
                AND FlagBatal != 1 AND Kota = 'JAKARTA'
                GROUP BY tbl_paket.KodeCabang";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".mysql_error());exit;
}
while($row=$db->sql_fetchrow($result)){
    $paket_from_jakarta[$row['KodeCabang']]['FromJakarta'] = $row['JmlFromJakarta'];
}

/*MENGAMBIL JUMLAH TIKET YANG TUJUANYA KE JAKARTA*/
$sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan AS KodeCabang, Kota
                FROM tbl_reservasi
                JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
                JOIN tbl_md_cabang ON tbl_md_jurusan.KodeCabangTujuan = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = '$bulan' AND YEAR(TglBerangkat) = '$tahun'
                AND FlagBatal != 1 AND CetakTiket = 1
                AND Kota = 'JAKARTA'
                GROUP BY KodeCabangTujuan";
if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".__LINE__);exit;
}
while($row=$db->sql_fetchrow($result)){
    $tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'] = $row['JmlFromBandungToJakarta'];
}

/*MENGAMBIL JUMLAH PAKET YANG TUJUANYA KE JAKARTA*/
$sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan AS KodeCabang, Kota
                FROM tbl_paket
                JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
                JOIN tbl_md_cabang ON tbl_md_jurusan.KodeCabangTujuan = tbl_md_cabang.KodeCabang
                WHERE MONTH(TglBerangkat) = '$bulan' AND YEAR(TglBerangkat) = '$tahun'
                AND FlagBatal != 1 AND CetakTiket = 1
                AND Kota = 'JAKARTA'
                GROUP BY KodeCabangTujuan";
if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".__LINE__);exit;
}
while($row=$db->sql_fetchrow($result)){
    $paket_to_jakarta[$row['KodeCabang']]['ToJakarta'] = $row['JmlFromBandungToJakarta'];
}



/*MENGAMBIL DATA CABANG */
$sql ="SELECT * FROM tbl_md_cabang ORDER BY Nama ASC";

if (!$result = $db->sql_query($sql)){
    //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
    echo("Error:".__LINE__);exit;
    //die(mysql_error());
}

$monthNum  = $bulan;
$dateObj   = DateTime::createFromFormat('!m', $monthNum);
$monthName = $dateObj->format('F');

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    )
);
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Target Counter Bulan '.$monthName." ".$tahun);
//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Kode Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kota');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Target Pax');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Real Pax');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Capaian Pax');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Target Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Real Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Capaian Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

//BODY
$idx_row = 4;
while ($row = $db->sql_fetchrow($result)){
    $target_pax     = ($data_target[$row['KodeCabang']]['TargetPax']!=""?$data_target[$row['KodeCabang']]['TargetPax']:0);
    $real_pax       = number_format($data_target[$row['KodeCabang']]['TargetPaxTercapai'],0,",",".");
    $capaianpax     = $data_target[$row['KodeCabang']]['TargetPax']>0?100*($data_target[$row['KodeCabang']]['TargetPaxTercapai']/$data_target[$row['KodeCabang']]['TargetPax']):0;
    $target_paket   = ($data_target[$row['KodeCabang']]['TargetPaket']!=""?$data_target[$row['KodeCabang']]['TargetPaket']:0);
    $real_paket     = number_format($data_target[$row['KodeCabang']]['TargetPaketTercapai'],0,",",".");
    $capaianpaket   = $capaianpaket  = $data_target[$row['KodeCabang']]['TargetPaket']>0?100*($data_target[$row['KodeCabang']]['TargetPaketTercapai']/$data_target[$row['KodeCabang']]['TargetPaket']):0;

    if($data_target[$row['KodeCabang']]['TargetPax']){
        $jakarta = ($tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'] == "")?0:$tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'];
        $bandung = ($tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'] == "")?0:$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'];
        if($jakarta == 0 && $bandung == 0){
            $capaian = 0;
        }else{
            $capaian = (($jakarta+$bandung)/$data_target[$row['KodeCabang']]['TargetPax'])*100;
            $capaian = round($capaian,2);
        }
    }else{
        $capaian = 0;
    }

    if($data_target[$row['KodeCabang']]['TargetPaket']){
        if($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'] == 0){
            $capaianpaket = 0;
        }else{
            $capaianpaket = ($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'])/$data_target[$row['KodeCabang']]['TargetPaket']*100;
            $capaianpaket = round($capaianpaket,2);
        }
    }else{
        $capaianpaket = 0;
    }


    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $row['KodeCabang']);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['Kota']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, ($data_target[$row['KodeCabang']]['TargetPax']!=""?$data_target[$row['KodeCabang']]['TargetPax']:0));
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($tiket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'],0,",","."));
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $capaian." %");
    $objPHPExcel->getActiveSheet()->getStyle('F'.$idx_row)->applyFromArray($style);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, ($data_target[$row['KodeCabang']]['TargetPaket']!=""?$data_target[$row['KodeCabang']]['TargetPaket']:0));
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'],0,",","."));
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row,$capaianpaket." %");
    $objPHPExcel->getActiveSheet()->getStyle('I'.$idx_row)->applyFromArray($style);
    $idx_row++;
}


$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$idx_row--;
$objPHPExcel->getActiveSheet()->getStyle('A3:I'.$idx_row)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
if ($idx_row>0){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Target Counter Bulan '.$monthName." ".$tahun);
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
?>