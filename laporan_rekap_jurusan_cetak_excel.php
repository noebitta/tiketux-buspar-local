<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//################################################################################

$bulan = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];



require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$styleheader = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font'  => array(
        'bold'  => true,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '#48616c')
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);

$border = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);

$fill = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => '#48616c')
    )
);

//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:Z1');
//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Jurusan Bulan '.BulanString($bulan).' Tahun '.$tahun);
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('5');
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'JURUSAN');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('25');

$objPHPExcel->getActiveSheet()->getStyle('A3:B4')->applyFromArray($styleheader);

$objPHPExcel->getActiveSheet()->freezePane('C5');

$sql_jurusan="SELECT 
                    IdJurusan,KodeJurusan,f_cabang_get_name_by_kode(KodeCabangAsal) AS CabangAsal,
                    f_cabang_get_name_by_kode(KodeCabangTujuan) AS CabangTujuan,
                    KodeCabangAsal
              FROM tbl_md_jurusan
              ORDER BY CabangAsal";
if (!$result_laporan = $db->sql_query($sql_jurusan)){
    echo("Err:".__LINE__);exit;
}

$sql_tiket = "SELECT 
                    IdJurusan, TglBerangkat, COUNT(NoTiket) AS PAX 
              FROM tbl_reservasi
              WHERE
                    MONTH(TglBerangkat) = $bulan
                    AND YEAR (TglBerangkat) = $tahun
                    AND CetakTiket = 1 
                    AND FlagBatal != 1
              GROUP BY 
                   IdJurusan, TglBerangkat";
if (!$tiket = $db->sql_query($sql_tiket)){
    echo("Err:".__LINE__);exit;
}
while($row = $db->sql_fetchrow($tiket)){
    $result_tiket[$row['IdJurusan']][$row['TglBerangkat']] = $row['PAX'];
}

$sql_paket = "SELECT 
                    IdJurusan, TglBerangkat, COUNT(NoTiket) AS PAKET 
              FROM tbl_paket
              WHERE
                    MONTH(TglBerangkat) = $bulan
                    AND YEAR (TglBerangkat) = $tahun
                    AND CetakTiket = 1 
                    AND FlagBatal != 1
              GROUP BY 
                   IdJurusan, TglBerangkat";
if (!$paket = $db->sql_query($sql_paket)){
    echo("Err:".__LINE__);exit;
}
while($row = $db->sql_fetchrow($paket)){
    $result_paket[$row['IdJurusan']][$row['TglBerangkat']] = $row['PAKET'];
}

//================LOOPING DATE
$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;

$kolom = 2;
while($currentdate <= $end){

    $cur_date = date('j', $currentdate);

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).'4', $cur_date);
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).'4')->applyFromArray($styleheader);

    $currentdate = strtotime('+1 days', $currentdate);
    $kolom++;
}

$objPHPExcel->getActiveSheet()->mergeCells('C3:'.position($kolom-1).'3');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'TIKET');
$objPHPExcel->getActiveSheet()->getStyle('C3:'.position($kolom-1).'3')->applyFromArray($styleheader);

// untuk paket
$kolom++;
$index_marge = $kolom;
$currentdate = $start;
while($currentdate <= $end){

    $cur_date = date('j', $currentdate);

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).'4', $cur_date);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).'4')->applyFromArray($styleheader);

    $currentdate = strtotime('+1 days', $currentdate);
    $kolom++;
}

$objPHPExcel->getActiveSheet()->mergeCells(position($index_marge).'3:'.position($kolom-1).'3');
$objPHPExcel->getActiveSheet()->setCellValue(position($index_marge).'3', 'PAKET');
$objPHPExcel->getActiveSheet()->getStyle(position($index_marge).'3:'.position($kolom-1).'3')->applyFromArray($styleheader);
//================END LOOPING DATE




//================LOOPING JURUSAN

$idx=0;
while($jurusan = $db->sql_fetchrow($result_laporan)){

    $idx_row=$idx+5;

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $jurusan['CabangAsal']." => ".$jurusan['CabangTujuan']);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);


    $jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

    $startdate = "{$tahun}/{$bulan}/01";
    $enddate = "{$tahun}/{$bulan}/{$jml_hari}";

    $start = strtotime($startdate);
    $end = strtotime($enddate);

    $currentdate = $start;

    $kolom = 2;
    $kolom_paket = $index_marge;
    while($currentdate <= $end){

        $cur_date = date('Y-m-d', $currentdate);

        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, $result_tiket[$jurusan['IdJurusan']][$cur_date]);
        $objPHPExcel->getActiveSheet()->setCellValue(position($kolom_paket).$idx_row, $result_paket[$jurusan['IdJurusan']][$cur_date]);

        $currentdate = strtotime('+1 days', $currentdate);
        $kolom++;
        $kolom_paket++;
    }

    $idx++;
}

    $idx_row = $idx+5;

    $objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':B'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, "Total");
    $objPHPExcel->getActiveSheet()->getStyle('A'.$idx_row.':B'.$idx_row)->applyFromArray($styleheader);

//================SUMERY

$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;

$kolom = 2;
while($currentdate <= $end){

    $cur_date = date('j', $currentdate);

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, "=SUM(".position($kolom)."5:".position($kolom).($idx_row-1).")");
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).$idx_row)->applyFromArray($styleheader);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);

    $currentdate = strtotime('+1 days', $currentdate);
    $kolom++;
}

// untuk paket
$kolom++;

$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;
while($currentdate <= $end){

    $cur_date = date('j', $currentdate);

    $objPHPExcel->getActiveSheet()->setCellValue(position($kolom).$idx_row, "=SUM(".position($kolom)."5:".position($kolom).($idx_row-1).")");
    $objPHPExcel->getActiveSheet()->getStyle(position($kolom).$idx_row)->applyFromArray($styleheader);
    $objPHPExcel->getActiveSheet()->getColumnDimension(position($kolom))->setAutoSize(true);

    $currentdate = strtotime('+1 days', $currentdate);
    $kolom++;
}


//END
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Rekap Jurusan Bulan '.BulanString($bulan).' Tahun '.$tahun.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

function position($posisi){
    $alpa = array(
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
        'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
        'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
        'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ'
    );
    return $alpa[$posisi];
}
?>