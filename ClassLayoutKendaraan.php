<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  //redirect('index.'.$phpEx,true);
  exit;
}
//#############################################################################

class LayoutKendaraan{

  //KAMUS GLOBAL
  var $ID_FILE; //ID Kelas

  //CONSTRUCTOR
  function LayoutKendaraan(){
    $this->ID_FILE="C-LYK";
  }

  //BODY

  function ambilData($order_by=0,$sort_by=0,$idx_page="",$cari=""){

    //kamus
    global $db, $VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("KodeLayout","JumlahBaris","JumlahKolom","Kapasitas");

    $sort     = ($sort_by==0)?"ASC":"DESC";

    $order		= ($order_by!=="" && $order_by!==null)?" ORDER BY $koloms[$order_by] $sort":'';
    $set_limit= ($idx_page!=="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1".($cari==""?"":" AND (KodeLayout LIKE '%$cari%' OR Kapasitas LIKE '%$cari%')");

    $sql  =
      "SELECT SQL_CALC_FOUND_ROWS
        IdLayout,KodeLayout,JumlahBaris,JumlahKolom,Kapasitas
      FROM tbl_md_kendaraan_layout
      $set_kondisi
      $order
	    $set_limit";

    if (!$result = $db->sql_query($sql)){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "SQL ERROR";

      return $ret_val;
    }

    $ret_val["status"]  = "OK";
    $ret_val["data"]    = $result;

    return $ret_val;

  }//  END ambilData

  function isDuplikasi($kode,$id_layout=""){
    global $db;

    $kondisi_tambahan = $id_layout==""?"":" AND IdLayout!=$id_layout";

    $sql  = "SELECT COUNT(1) FROM tbl_md_kendaraan_layout WHERE KodeLayout='$kode' $kondisi_tambahan";

    $result=$db->sql_query($sql);

    $row  = $db->sql_fetchrow($result);

    return $row[0]>0?true:false;
  }

	function tambah($KodeLayout,$JumlahBaris,$JumlahKolom,$Kapasitas,$PetaKursi){
    global $db;

    //CEK DUPLIKASI
    if($this->isDuplikasi($KodeLayout)) {
      $ret_val["status"] = "GAGAL";
      $ret_val["pesan"]  = "KODE SUDAH TERDAFTAR";

      return $ret_val;
    }

    $sql  =
      "INSERT INTO tbl_md_kendaraan_layout SET
        KodeLayout='$KodeLayout',
        JumlahBaris='$JumlahBaris',
        JumlahKolom='$JumlahKolom',
        Kapasitas='$Kapasitas',
        PetaKursi='$PetaKursi'";

    if(!$result=$db->sql_query($sql)){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "SQL ERROR";

      return $ret_val;
    }

    $ret_val["status"]  = "OK";
    $ret_val["id"]      = $db->sql_nextid();

    return $ret_val;

  } //tambah

  function ubah($IdLayout,$KodeLayout,$JumlahBaris,$JumlahKolom,$Kapasitas,$PetaKursi){
    global $db;

    //CEK DUPLIKASI
    if($this->isDuplikasi($KodeLayout,$IdLayout)){
      $ret_val["status"] = "GAGAL";
      $ret_val["pesan"]  = "KODE SUDAH TERDAFTAR";

      return $ret_val;
    }

    $sql  =
      "UPDATE tbl_md_kendaraan_layout SET
        KodeLayout='$KodeLayout',
        JumlahBaris='$JumlahBaris',
        JumlahKolom='$JumlahKolom',
        Kapasitas='$Kapasitas',
        PetaKursi='$PetaKursi'
      WHERE IdLayout=$IdLayout";

    if(!$result=$db->sql_query($sql)){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "SQL ERROR";

      return $ret_val;
    }

    $ret_val["status"]  = "OK";

    return $ret_val;

  } //ubah

  function ambilDetailData($id_layout){
    global $db;

    $sql  = "SELECT * FROM tbl_md_kendaraan_layout WHERE IdLayout='$id_layout'";

    if(!$result=$db->sql_query($sql)){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "SQL ERROR";

      return $ret_val;
    }

    $row  = $db->sql_fetchrow($result);

    $ret_val["status"]  = "OK";
    $ret_val["data"]    = $row;

    return $ret_val;
  } //ambilDetailData

  function hapus($list_id){

    //kamus

    global $db,$userdata;

    //MEMBACKUP JADWAL YANG DIHAPUS

    $sql="SELECT * FROM tbl_md_kendaraan_layout WHERE IdLayout IN ($list_id)";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    while($row=$db->sql_fetchrow($result)){
      $sql=
        "INSERT INTO tbl_md_kendaraan_layout_deleted SET
          IdLayout=$row[IdLayout],KodeLayout='$row[KodeLayout]',JumlahBaris='$row[JumlahBaris]',
          JumlahKolom='$row[JumlahKolom]', Kapasitas='$row[Kapasitas]',PetaKursi=\"$row[PetaKursi]\",
          DihapusOleh='$userdata[user_id]',NamaPenghapus='$userdata[nama]',
          WaktuHapus=NOW()";

      if (!$result=$db->sql_query($sql)){
        echo("Err: $this->ID_FILE".__LINE__);exit;
      }
    }

    //MENGHAPUS DATA DARI DATABASE
    $sql =
      "DELETE FROM tbl_md_kendaraan_layout
			WHERE IdLayout IN($list_id);";

    if (!$db->sql_query($sql)){
      return false;
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $ret_val["status"]  = "OK";

    return $ret_val;
  }//end hapus
}
?>