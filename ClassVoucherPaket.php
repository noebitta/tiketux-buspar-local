<?php
$adp_root_path = './';

class VoucherPaket{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
		
	//CONSTRUCTOR
	function VoucherPaket(){
		$this->ID_FILE="C-VCRPKT";
	}
	
	//BODY
	
	public function generateKodeVoucher($kode_mitra){

    $char = array(
      "0", "6", "C", "I", "O", "U",
      "1", "7", "D", "J", "P", "V",
      "2", "8", "E", "K", "Q", "W",
      "3", "9", "F", "L", "R", "X",
      "4", "A", "G", "M", "S", "Y",
      "5", "B", "H", "N", "T", "Z");

    $y  = date("y");
    $m  = date("m");
    $d  = date("d");
    $h  = date("H");
    $i  = date("i");
    $s  = date("s");

    $kode_mitra = substr(strtoupper($kode_mitra),0,3);

    $key1 = ord(substr($kode_mitra,0,1));
    $key2 = ord(substr($kode_mitra,1,1));
    $key3 = ord(substr($kode_mitra,2,1));

    $matrix1[0] = array($y.rand(1, 99), $m.rand(1, 99), $d.rand(1, 99));
    $matrix1[1] = array($d.rand(1, 99), $y.rand(1, 99), $m.rand(1, 99));
    $matrix1[2] = array($m.rand(1, 99), $d.rand(1, 99), $y.rand(1, 99));

    $matrix2[0] = array($h.rand(1, 99), $i.rand(1, 99), $s.rand(1, 99));
    $matrix2[1] = array($s.rand(1, 99), $h.rand(1, 99), $i.rand(1, 99));
    $matrix2[2] = array($i.rand(1, 99), $s.rand(1, 99), $h.rand(1, 99));

    $row = 0;
    $col = 0;

    for ($r1 = 0; $r1 <= 2; $r1++) {
      for ($c2 = 0; $c2 <= 2; $c2++) {
        $crossmat[$row][$col] = $matrix1[$r1][0] * $matrix2[0][$c2]*$key1 + $matrix1[$r1][1] * $matrix2[1][$c2]*$key2 + $matrix1[$r1][2] * $matrix2[2][$c2]*$key3;
        $col++;
      }
      $row++;
    }

    $kode_voucher = "";

    foreach ($crossmat as $arr_baris) {
      foreach ($arr_baris as $value) {
        $kode_voucher .= $char[$value % 36];
        $col++;
      }
      $row++;
    }

    return $kode_voucher;
	}
	
	private function getKodeVoucher(){
		global $db;
		global $config;

		do{
			
			$kode_voucher	= $this->generateKodeVoucher($config["kode_mitra"]);
			
			$sql = 
				"SELECT COUNT(1) AS Duplikasi
				FROM tbl_voucher_paket
				WHERE KodeVoucher='$kode_voucher';";
					
			if (!$result = $db->sql_query($sql)){
				echo("Err: $this->ID_FILE".__LINE__);exit;
			}
			
			$row=$db->sql_fetchrow($result);
			
			$duplikasi	= $row['Duplikasi']==""?false:true;
				
		}while(!$duplikasi);
		
		return $kode_voucher;
	}
	
	function setVoucher(
		$KotaBerlaku,$CabangBerangkatBerlaku,$CabangTujuanBerlaku,
		$PetugasPencetak,$BerlakuMulai,$ExpiredDate,
    $NilaiVoucher,$IsHargaTetap,$IsBolehWeekEnd,
    $Keterangan){
		
		global $db;

		$kode_voucher		= $this->getKodeVoucher();

		$sql =
			"INSERT INTO tbl_voucher_paket(
				KodeVoucher,KotaBerlaku,CabangBerangkatBerlaku,CabangTujuanBerlaku,
        PetugasPencetak,WaktuCetak,BerlakuMulai,ExpiredDate,
        NilaiVoucher,IsHargaTetap,IsBolehWeekEnd,
        Keterangan)
			VALUES(
				'$kode_voucher',".($KotaBerlaku==""?"NULL":"'$KotaBerlaku'").",".($CabangBerangkatBerlaku==""?"NULL":"'$CabangBerangkatBerlaku'").",".($CabangTujuanBerlaku==""?"NULL":"'$CabangTujuanBerlaku'").",
        '$PetugasPencetak',NOW(),'$BerlakuMulai','$ExpiredDate',
        '$NilaiVoucher','$IsHargaTetap','$IsBolehWeekEnd',
        '$Keterangan');";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE $sql".__LINE__);exit;
		}

		return 	$kode_voucher;
	}

  function ubahVoucher(
    $KodeVoucher,$KotaBerlaku,$CabangBerangkatBerlaku,
    $CabangTujuanBerlaku,$BerlakuMulai,$ExpiredDate,
    $NilaiVoucher,$IsHargaTetap,$IsBolehWeekEnd,
    $Keterangan){

    global $db,$userdata;

    $data_voucher = $this->getDataVoucher($KodeVoucher);

    $field_diubah = "";
    $field_diubah   .= $data_voucher["KotaBerlaku"]==$KotaBerlaku?"":"KotaBerlaku=".($KotaBerlaku==""?"NULL":"'$KotaBerlaku'").",";
    $field_diubah   .= $data_voucher["CabangBerangkatBerlaku"]==$CabangBerangkatBerlaku?"":"CabangBerangkatBerlaku=".($CabangBerangkatBerlaku==""?"NULL":"'$CabangBerangkatBerlaku'").",";
    $field_diubah   .= $data_voucher["CabangTujuanBerlaku"]==$CabangTujuanBerlaku?"":"CabangTujuanBerlaku=".($CabangTujuanBerlaku==""?"NULL":"'$CabangTujuanBerlaku'").",";
    $field_diubah   .= $data_voucher["BerlakuMulai"]==trim($BerlakuMulai)?"":"BerlakuMulai='".trim($BerlakuMulai)."',";
    $field_diubah   .= $data_voucher["ExpiredDate"]==trim($ExpiredDate)?"":"ExpiredDate='".trim($ExpiredDate)."',";
    $field_diubah   .= $data_voucher["NilaiVoucher"]==$NilaiVoucher?"":"NilaiVoucher='$NilaiVoucher',";
    $field_diubah   .= $data_voucher["IsHargaTetap"]==$IsHargaTetap?"":"IsHargaTetap='$IsHargaTetap',";
    $field_diubah   .= $data_voucher["IsBolehWeekEnd"]==$IsBolehWeekEnd?"":"IsBolehWeekEnd='$IsBolehWeekEnd',";
    $field_diubah   .= $data_voucher["Keterangan"]==$Keterangan?"":"Keterangan='$Keterangan',";

    $field_diubah   = substr($field_diubah,0,-1);

    if($field_diubah==""){
      return true;
    }

    $sql =
      "UPDATE tbl_voucher_paket SET $field_diubah,Log=CONCAT(\"$userdata[nama] ($userdata[user_id]) UPDATE $field_diubah Pada:\",NOW(),';',Log)
      WHERE KodeVoucher='$KodeVoucher' AND NoResiDigunakan='' AND IsBatal=0";

    if (!$db->sql_query($sql)){
      echo("Err: $sql $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function hapus($list_kode_voucher){

    //kamus
    global $db;

    $kode_vouchers  = explode(",",$list_kode_voucher);

    $str_kode_voucher="";

    foreach($kode_vouchers as $value){
      $str_kode_voucher .="'$value',";
    }

    $str_kode_voucher = substr($str_kode_voucher,0,-1);

    //MENGHAPUS VOUCHER YANG BELUM TERPAKAI

    $sql =
      "DELETE FROM tbl_voucher_paket
			WHERE KodeVoucher IN($str_kode_voucher) AND NoResiDigunakan='';";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);
    }

    return true;
  }//end hapus

  function batal($list_kode_voucher,$user_batal,$alasan_batal){

    //kamus
    global $db;

    $kode_vouchers  = explode(",",$list_kode_voucher);

    $str_kode_voucher="";

    foreach($kode_vouchers as $value){
      $str_kode_voucher .="'$value',";
    }

    $str_kode_voucher = substr($str_kode_voucher,0,-1);

    //MEMBATALKAN VOUCHER YANG BELUM TERPAKAI
    $sql =
      "UPDATE tbl_voucher_paket
      SET IsBatal=1,DibatalkanOleh='$user_batal',WaktuPembatalan=NOW(),AlasanPembatalan='$alasan_batal'
			WHERE KodeVoucher IN($str_kode_voucher) AND NoResiDigunakan='' AND IsBatal=0;";

    if (!$db->sql_query($sql)){
      echo("Err:$this->ID_FILE".__LINE__);
    }

    return true;
  }//end hapus

  function ambilData($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("KodeVoucher","IsHargaTetap","NilaiVoucher",
      "Keterangan","IsBolehWeekEnd","KotaBerlaku",
      "CabangBerangkatBerlaku","CabangTujuanBerlaku","BerlakuMulai",
      "ExpiredDate","WaktuCetak","TglBerangkatDigunakan",
      "KodeJadwalDigunakan","NoResiDigunakan","WaktuDigunakan",
      "WaktuPembatalan","AlasanPembatalan");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1 ".($cari==""?"":" AND (KodeVoucher LIKE '%$cari%' OR Keterangan LIKE '%$cari%'
      OR KotaBerlaku LIKE '%$cari%' OR CabangBerangkatBerlaku LIKE '$cari%' OR CabangTujuanBerlaku LIKE '$cari%'
      OR KodeJadwalDigunakan LIKE '$cari%' OR NoResiDigunakan LIKE '%$cari%' OR AlasanPembatalan LIKE '%$cari%')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";

    $sql =
      "SELECT SQL_CALC_FOUND_ROWS *
			FROM tbl_voucher_paket
			$set_kondisi $set_kondisi_tambahan
			$order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilData

  function getDataVoucher($kode_voucher){
    global $db;

    $sql =
      "SELECT *
			FROM tbl_voucher_paket
			WHERE KodeVoucher='$kode_voucher';";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $row=$db->sql_fetchrow($result);


    return $row;
  }
	
	function verifyVoucher($kode_voucher,$tgl_berangkat,$kode_jadwal){


		global $db;

    //MENGAMBIL DATA JADWAL & JURUSAN
    $sql=
      "SELECT tjd.IdJurusan,tjr.KodeCabangAsal,tjr.KodeCabangTujuan
      FROM tbl_md_jadwal tjd INNER JOIN tbl_md_jurusan tjr ON tjd.IdJurusan=tjr.IdJurusan WHERE KodeJadwal='$kode_jadwal'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);
    }

    $data_jadwal    = $db->sql_fetchrow($result);

    $cabang_asal    = $data_jadwal["KodeCabangAsal"];
    $cabang_tujuan  = $data_jadwal["KodeCabangTujuan"];

    //MENGAMBIL KOTA
    $sql=
      "SELECT Kota FROM tbl_md_cabang WHERE KodeCabang='$cabang_asal'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);
    }

    $data_kota  = $db->sql_fetchrow($result);

		$sql = 
			"SELECT
				KodeVoucher,
				IF(KotaBerlaku='$data_kota[Kota]' OR KotaBerlaku IS NULL,1,0) AS IsKotaVerify,
				IF(ExpiredDate>=DATE(NOW()),0,1) AS IsExpired,
				IF(IsBolehWeekEnd=1 OR (IsBolehWeekEnd=0 AND DAYOFWEEK('$tgl_berangkat') IN (2,3,4,5)),1,0) AS IsBolehWeekEndVerify,
				IF(CabangBerangkatBerlaku='$cabang_asal' OR CabangBerangkatBerlaku IS NULL,1,0) AS IsCabangBerangkatVerify,
				IF(CabangTujuanBerlaku='$cabang_tujuan' OR CabangTujuanBerlaku IS NULL,1,0) AS IsCabangTujuanVerify,
				IF(BerlakuMulai<=DATE(NOW()),1,0) AS SudahMulaiBerlaku,
				IF(WaktuDigunakan IS NULL,0,1) AS SudahDigunakan,
				IsHargaTetap,NilaiVoucher
			FROM tbl_voucher_paket
			WHERE
				KodeVoucher='$kode_voucher' AND IsBatal!=1";

		$data_return['sql'] = $sql;

		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$row=$db->sql_fetchrow($result);
		
		if($row["KodeVoucher"]!=$kode_voucher){
			$data_return['status']=false;
			$data_return['keterangan']='Kode voucher tidak benar';
			return $data_return;
		}

    if(!$row["IsKotaVerify"]){
      $data_return['status']=false;
      $data_return['keterangan']='Kota penggunaan tidak sesuai';
      return $data_return;
    }
		
		if($row["IsExpired"]){
			$data_return['status']=false;
			$data_return['keterangan']='voucher sudah expired';
			return $data_return;
		}
		
		if(!$row["IsBolehWeekEndVerify"]){
			$data_return['status']=false;
			$data_return['keterangan']='voucher tidak boleh digunakan hari ini';
			return $data_return;
		}

    if(!$row["IsCabangBerangkatVerify"]){
			$data_return['status']=false;
			$data_return['keterangan']='voucher tidak boleh digunakan dari cabang ini';
			return $data_return;
		}

    if(!$row["IsCabangTujuanVerify"]){
			$data_return['status']=false;
			$data_return['keterangan']='voucher tidak boleh digunakan untuk tujuan cabang ini';
			return $data_return;
		}

		if(!$row["SudahMulaiBerlaku"]){
			$data_return['status']=false;
			$data_return['keterangan']='voucher belum berlaku';
			return $data_return;
		}

		if($row["SudahDigunakan"]){
			$data_return['status']=false;
			$data_return['keterangan']='voucher sudah digunakan';
			return $data_return;
		}

		$data_return['status']      = true;
		$data_return['keterangan']  = "Voucher ".(!$row["IsHargaTetap"]?"DISKON Sebesar ":"harga flat, HANYA BAYAR ").($row["NilaiVoucher"]>1?"Rp.".number_format($row["NilaiVoucher"],0,",","."):($row["NilaiVoucher"]*100)."%");
		$data_return["NilaiVoucher"]= $row["NilaiVoucher"];
		$data_return["IsHargaTetap"]= $row["IsHargaTetap"];
    return $data_return;
		
	}
	
	function pakaiVoucher($kode_voucher,$petugas_pengguna,$no_resi){

		global $db;

    //MENGAMBIL DATA PAKET
    $sql  =
      "SELECT NoResi,IdJurusan,KodeJadwal,TglBerangkat,HargaPaket FROM tbl_paket WHERE NoResi='$no_resi' AND FlagBatal=0";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_paket = $db->sql_fetchrow($result);

		$verify	= $this->verifyVoucher($kode_voucher,$data_paket["TglBerangkat"],$data_paket["KodeJadwal"]);

    if(!$verify["status"]){
      $return["status"] = false;
      $return["error"]  = $verify["error"];
    }

    //MENGGUNAKAN VOUCHER DAN MENGUPDATE KE DATABASE
    $sql =
      "UPDATE tbl_voucher_paket
			SET
				WaktuDigunakan=NOW(),
				PetugasPengguna='$petugas_pengguna',
				IdJurusanDigunakan='$data_paket[IdJurusan]',
				TglBerangkatDigunakan='$data_paket[TglBerangkat]',
				KodeJadwalDigunakan='$data_paket[KodeJadwal]',
				NoResiDigunakan='$no_resi',
				HargaJual='$data_paket[HargaPaket]'
			WHERE KodeVoucher='$kode_voucher';";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }
		
		return true;
	}
	
	function getVoucherByNoResi($no_resi){
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_voucher_paket
			WHERE NoResiDigunakan='$no_resi';";
					
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
			
		$row=$db->sql_fetchrow($result);

		return $row;
	}

	public function sendVoucher($kode_voucher,$nama,$nominal,$expired,$idjurusan,$isweekend,$jenisharga,$hargajual){

	    global $config;

		$reqSignature	= new RequestSignature();
		$params 		= array();

		//Api Parameter
		$params['auth_nonce']	  	= $reqSignature->createNonce(true);
		$params['auth_timestamp'] 	= time();
		$params['auth_client_id']	= $config['client_id']; //CLIENTID;

		$params['return_url'] 		= $config['return_url_voucher'];
		$params['kode']             = $kode_voucher;
		$params['nama']             = $nama;
		$params['nominal']          = $nominal;
		$params['expired']          = $expired;
		$params['jurusan']			= $idjurusan;
		$params['weekend']			= $isweekend;
        $params['isharga']          = $jenisharga;
        $params['harga_jual']       = $hargajual;
		$params['agen']             = 'DTR';

		$accessToken				= $config['client_id']; //CLIENTID;
		$accessTokenSecret			= $config['client_secret']; //CLIENTSECRET;

		if (!empty($accessToken)) {
			$params['auth_access_token'] = $accessToken;
			$key 	= $accessToken;
			$secret = $accessTokenSecret;
		} else {
			$key 	= $config['client_id'];
			$secret = $config['client_secret'];
		}

		$baseSignature = $reqSignature->createSignatureBase("POST", $config['api_url']."voucher/receive.json", $params);
		$signature     = $reqSignature->createSignature($baseSignature, $key, $secret);

		return sendHttpPost($config['api_url'] ."voucher/receive.json",$reqSignature->normalizeParams($params).'&auth_signature=' .$signature);
	}

  function getListKota(){
    global $db;

    $sql  =
      "SELECT * FROM tbl_md_kota ORDER BY NamaKota";

    if(!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);exit;
    }

    return $result;

  }

  function getListCabang($kota,$cabang_asal=""){
    global $db;

    if($cabang_asal==""){
      $kolom                = "KodeCabangAsal AS KodeCabang";
      $kolom_join           = "tmj.KodeCabangAsal";
      $kondisi_cabang_asal  = "";
      $kondisi_kota         = "Kota='$kota'";
    }
    else{
      $kolom                = "KodeCabangTujuan AS KodeCabang";
      $kolom_join           = "tmj.KodeCabangTujuan";
      $kondisi_cabang_asal  = "KodeCabangAsal='$cabang_asal'";
      $kondisi_kota         = "";
    }


    $sql  =
      "SELECT DISTINCT $kolom,tmc.Nama AS NamaCabang
      FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON $kolom_join=tmc.KodeCabang
      WHERE $kondisi_kota $kondisi_cabang_asal ORDER BY NamaCabang";

    if(!$result = $db->sql_query($sql)){
      echo("Err $this->ID_FILE:".__LINE__);exit;
    }

    return $result;

  }


}

?>