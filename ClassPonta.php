<?php

class Ponta{

    //KAMUS GLOBAL
    var $ID_FILE; //ID Kelas

    //CONSTRUCTOR
    function Promo(){
        $this->ID_FILE="C-PNT";
    }

	//BODY

    public function login($kode_cabang, $nama_cabang)
    {
        global $config;

        $params = ['kode_cabang'=>$kode_cabang, 'nama_cabang'=>$nama_cabang];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$config['url_login']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $json = json_decode($server_output);

        return $json;
    }

    public function enquery($kode_cabang, $nama_cabang, $access_token, $card_id)
    {
        global $config;

        $params = ['kode_cabang'=>$kode_cabang, 'nama_cabang'=>$nama_cabang,
                    'access_token'=>$access_token, 'card_id'=>$card_id
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$config['url_enquiry']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $json = json_decode($server_output);

        return $json;
    }

    public function request_reedem($kode_cabang, $nama_cabang, $access_token, $card_id, $total_bayar, $telp)
    {
        global $config;

        $params = ['kode_cabang'=>$kode_cabang, 'nama_cabang'=>$nama_cabang,
            'access_token'=>$access_token, 'card_id'=>$card_id, 'poin_reedem'=>$total_bayar,
            'req_reedem'=>1, 'telp_reedem'=>$telp
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$config['url_enquiry']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $json = json_decode($server_output);

        return $json;
    }

    public function reedem($kode_cabang, $nama_cabang, $access_token, $card_id, $total_bayar, $kode_booking, $no_tiket)
    {
        global $config;

        $params = ['kode_cabang'=>$kode_cabang, 'nama_cabang'=>$nama_cabang,
            'access_token'=>$access_token, 'card_id'=>$card_id, 'kode_booking'=>$kode_booking,
            'no_tiket'=>$no_tiket, 'poin'=>$total_bayar, 'total'=>$total_bayar
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$config['url_reedem']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $json = json_decode($server_output);

        return $json;
    }

    public function void_reedem($kode_cabang, $nama_cabang, $access_token, $card_id, $kode_booking, $no_tiket)
    {
        global $config;

        $params = ['kode_cabang'=>$kode_cabang, 'nama_cabang'=>$nama_cabang,
                    'access_token'=>$access_token, 'card_id'=>$card_id,
                    'kode_booking'=>$kode_booking, 'no_tiket'=>$no_tiket,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$config['url_voidreedem']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);

        $json = json_decode($server_output);

        return $json;
        
    }
    
    public function voidByKodeBooking($kode_cabang, $nama_cabang, $access_token, $card_id, $kode_booking){

        global $db;
        $sql = "SELECT NoTiket FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";
        $result = $db->sql_query($sql);

        while ($data_kursi = $db->sql_fetchrow($result)) {
            $this->void_reedem($kode_cabang,$nama_cabang,$access_token,$card_id,$kode_booking,$data_kursi['NoTiket']);
        }
    }

    public function getDataAward($no_tiket){

        global $db;

        $sql = "SELECT * FROM tbl_ponta_transaksi WHERE NoTiket = '$no_tiket'";

        if ($result = $db->sql_query($sql)){
            $row=$db->sql_fetchrow($result);
        }
        else{
            die_error("Err: $this->ID_FILE".__LINE__);
        }

        return $row;
    }

}