<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassPomBensin.php');

// SESSION
$id_page = 702;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassMobil.php');

// PARAMETER
$perpage 	= $config['perpage'];
$mode 		= getVariabel("mode");
$Jurusan	= new Jurusan();
$Cabang		= new Cabang();
$Mobil		= new Mobil();
$SPBU		= new SPBU();

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

//ROUTER ==================================================================================================================
switch ($mode){
	//View List
	case 0:
		//VIEW LIST
		$cari         = getVariabel("cari");
		$idx_page     = getVariabel("idxpage");
		$order_by     = getVariabel("orderby");
		$sort         = getVariabel("sort");
		$scroll_value = getVariabel("scrollvalue");
		//View List
		$template->assign_vars(array(
			'BCRUMP'	  =>setBcrump($id_page),
			'U_TEST_ACT'=>append_sid('pengaturan.jurusan.php?mode=8')
		));
		include($adp_root_path . 'includes/page_header.php');
		showData();
		include($adp_root_path . 'includes/page_tail.php');
		exit;

    //Hapus Data
    case 2:
        $Jurusan = new Jurusan();
        $list_jurusan = getVariabel('listjurusan');
//        var_dump($list_jurusan);
//        break;
        $Jurusan->hapus($list_jurusan);

    exit;

	//Show Dialog Tambah
	case 3:
		//Tambah Data
		ShowDialogTambah();
		exit;

	//Proses Data
	case 3.1:
		$id_jurusan 							= getVariabel("id_jurusan");
		$kode       							= str_replace(" ","", getVariabel("kode"));
		$kode_jurusan_old       				= str_replace(" ","", getVariabel("kode_jurusan_old"));
		$asal 									= getVariabel("asal");
		$tujuan									= getVariabel("tujuan");
		$harga_tiket 							= getVariabel("harga_tiket");
		$harga_paket_1_kilo_pertama				= getVariabel("harga_paket_1_kilo_pertama");
		$harga_paket_1_kilo_berikut				= getVariabel("harga_paket_1_kilo_berikut");
		$harga_paket_2_kilo_pertama				= getVariabel("harga_paket_2_kilo_pertama");
		$harga_paket_2_kilo_berikut				= getVariabel("harga_paket_2_kilo_berikut");
		$harga_paket_3_kilo_pertama				= getVariabel("harga_paket_3_kilo_pertama");
		$harga_paket_3_kilo_berikut				= getVariabel("harga_paket_3_kilo_berikut");
		$harga_paket_4_kilo_pertama				= getVariabel("harga_paket_4_kilo_pertama");
		$harga_paket_4_kilo_berikut				= getVariabel("harga_paket_4_kilo_berikut");
		$harga_paket_5_kilo_pertama				= getVariabel("harga_paket_5_kilo_pertama");
		$harga_paket_5_kilo_berikut				= getVariabel("harga_paket_5_kilo_berikut");
		$harga_paket_6_kilo_pertama				= getVariabel("harga_paket_6_kilo_pertama");
		$harga_paket_6_kilo_berikut				= getVariabel("harga_paket_6_kilo_berikut");
		$flag_aktif 							= getVariabel("flag_aktif");
		$flag_jenis 							= getVariabel("flag_jenis");
		$flag_op_jurusan 						= getVariabel("flag_op_jurusan");
		$biaya_sopir 							= getVariabel("biaya_sopir");
		$flag_biaya_sopir_kumulatif				= getVariabel("flag_biaya_sopir_kumulatif"!='on'?0:1);
		$biaya_tol	 							= getVariabel("biaya_tol");
		$biaya_parkir 							= getVariabel("biaya_parkir");
		$biaya_bbm 								= getVariabel("biaya_bbm");
		$flag_biaya_voucher_bbm 				= getVariabel("flag_biaya_voucher_bbm"!='on'?0:1);
		$lokasi_spbu 							= getVariabel("spbu");
		$terjadi_error 							= false;
		
		echo (prosesSimpan($kode,$asal,$tujuan,
			$harga_tiket,$biaya_sopir,$biaya_tol,$biaya_parkir,$biaya_bbm,$flag_aktif,$flag_jenis,
			$harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
			$harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
			$harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
			$harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
			$harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
			$harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
			$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$liter_bbm,$lokasi_spbu));
		
		break;
		
		
		$data_layout	= $Mobil->getArrayLayout();

		if($Jurusan->periksaDuplikasi($kode) && $kode!=$kode_jurusan_old){
			$pesan="<font color='white' size=3>Kode jurusan yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			echo $kode;

			if($flag_op_jurusan==1){
				$harga_tiket=0;
				$harga_tiket_tuslah=0;
			}

			$liter_bbm	= "";

			foreach($data_layout as $layout_id	=> $layout_value){
				$liter_bbm	.= $layout_id."=".$HTTP_POST_VARS["biayabbm".$layout_id].";";
				$liter_bbm2	.= $layout_id."=".$HTTP_POST_VARS["biayabbmext".$layout_id].";";
			}

			if($submode==0){
				$judul="Tambah Data Jurusan";


				if($Jurusan->tambah(
					$kode,$asal,$tujuan,
					$harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
					$kode_akun_pendapatan_penumpang,$kode_akun_pendapatan_paket,
					$kode_akun_charge,$kode_akun_biaya_sopir,$biaya_sopir,$kode_akun_biaya_tol,
					$biaya_tol,$kode_akun_biaya_parkir,$biaya_parkir,
					$kode_akun_biaya_bbm,$biaya_bbm,$kode_akun_komisi_penumpang_sopir,
					$komisi_penumpang_sopir,$kode_akun_komisi_penumpang_cso,$komisi_penumpang_cso,
					$kode_akun_komisi_paket_sopir,$komisi_paket_sopir,$kode_akun_komisi_paket_cso,
					$komisi_paket_cso,$flag_aktif,$flag_jenis,
					$harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
					$harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
					$harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
					$harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
					$harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
					$harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
					$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$liter_bbm,$liter_bbm2,$lokasi_spbu)){

					redirect(append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add&pesan=1',true));
					//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');

				}
			}
			else{

				$judul="Ubah Data Jurusan";

				if($Jurusan->ubah(
					$id_jurusan,
					$kode,$asal,$tujuan,
					$harga_tiket,$harga_tiket_tuslah,$flag_tuslah,
					$kode_akun_pendapatan_penumpang,$kode_akun_pendapatan_paket,
					$kode_akun_charge,$kode_akun_biaya_sopir,$biaya_sopir,$kode_akun_biaya_tol,
					$biaya_tol,$kode_akun_biaya_parkir,$biaya_parkir,
					$kode_akun_biaya_bbm,$biaya_bbm,$kode_akun_komisi_penumpang_sopir,
					$komisi_penumpang_sopir,$kode_akun_komisi_penumpang_cso,$komisi_penumpang_cso,
					$kode_akun_komisi_paket_sopir,$komisi_paket_sopir,$kode_akun_komisi_paket_cso,
					$komisi_paket_cso,$flag_aktif,$flag_jenis,
					$harga_paket_1_kilo_pertama,$harga_paket_1_kilo_berikut,
					$harga_paket_2_kilo_pertama,$harga_paket_2_kilo_berikut,
					$harga_paket_3_kilo_pertama,$harga_paket_3_kilo_berikut,
					$harga_paket_4_kilo_pertama,$harga_paket_4_kilo_berikut,
					$harga_paket_5_kilo_pertama,$harga_paket_5_kilo_berikut,
					$harga_paket_6_kilo_pertama,$harga_paket_6_kilo_berikut,
					$flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$liter_bbm,$liter_bbm2,$lokasi_spbu)){

					//redirect(append_sid('pengaturan_jurusan.'.$phpEx.'?mode=add',true));
					//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('pengaturan_jurusan.'.$phpEx.'?mode=edit&id='.$id_jurusan).'">Sini</a> Untuk Melanjutkan','');

					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}

			//exit;

		}

		$temp_var_tuslah="tuslah_".$flag_tuslah;
		$$temp_var_tuslah="selected";

		$temp_var_aktif="aktif_".$flag_aktif;
		$$temp_var_aktif="selected";

		$temp_var_aktif="jenis_".$flag_jenis;
		$$temp_var_aktif="selected";

		$temp_var_sub_jurusan	="sub_jurusan_".$flag_sub_jurusan;
		$$temp_var_sub_jurusan="selected";

		$temp_var_op_jurusan	="op_".$flag_op_jurusan;
		$$temp_var_op_jurusan	="selected";

		$temp_var_lokasi_spbu   ="spbu_".$lokasi_spbu;
		$$temp_var_lokasi_spbu  ="selected";

		foreach($data_layout as $layout_id	=> $layout_value){
			$template->assign_block_vars("LISTBBM",array(
					"idlayout"	=> $layout_id,
					"literbbm"	=> $HTTP_POST_VARS["biayabbm".$layout_id],
					"literbbm2"	=> $HTTP_POST_VARS["biayabbmext".$layout_id])
			);
		}

		$template->set_filenames(array('body' => 'pengaturan.jurusan/detail.tpl'));
		$template->assign_vars(array(
				'BCRUMP'		                  	=>setBcrump($id_page),
				'JUDUL'		                  		=>$judul,
				'MODE'   	                  		=> 'save',
				'SUB'    	                  		=> $submode,
				'ID_JURUSAN'				  		=> $id_jurusan,
				'KODE_JURUSAN_OLD'	          		=> $kode,
				'KODE_JURUSAN'    	          		=> $kode,
				'OPT_ASAL'    			      		=> setComboCabang($asal),
				'OPT_TUJUAN'   		          		=> setComboCabang($tujuan),
				'HARGA_TIKET'			      		=> $harga_tiket,
				//'HARGA_TIKET_TUSLAH'          		=> $harga_tiket_tuslah,
				'HARGA_PAKET_1_KILO_PERTAMA'  		=> $harga_paket_1_kilo_pertama,
				'HARGA_PAKET_1_KILO_BERIKUT'  		=> $harga_paket_1_kilo_berikut,
				'HARGA_PAKET_2_KILO_PERTAMA'  		=> $harga_paket_2_kilo_pertama,
				'HARGA_PAKET_2_KILO_BERIKUT'  		=> $harga_paket_2_kilo_berikut,
				'HARGA_PAKET_3_KILO_PERTAMA'  		=> $harga_paket_3_kilo_pertama,
				'HARGA_PAKET_3_KILO_BERIKUT'  		=> $harga_paket_3_kilo_berikut,
				'HARGA_PAKET_4_KILO_PERTAMA'  		=> $harga_paket_4_kilo_pertama,
				'HARGA_PAKET_4_KILO_BERIKUT'  		=> $harga_paket_4_kilo_berikut,
				'HARGA_PAKET_5_KILO_PERTAMA'  		=> $harga_paket_5_kilo_pertama,
				'HARGA_PAKET_5_KILO_BERIKUT'  		=> $harga_paket_5_kilo_berikut,
				'HARGA_PAKET_6_KILO_PERTAMA'  		=> $harga_paket_6_kilo_pertama,
				'HARGA_PAKET_6_KILO_BERIKUT'  		=> $harga_paket_6_kilo_berikut,
				'TUSLAH_0'					  		=> $tuslah_0,
				'TUSLAH_1'					  		=> $tuslah_1,
				'AKTIF_0'					  		=> $aktif_0,
				'AKTIF_1'					  		=> $aktif_1,
				'JENIS_0'					  		=> $jenis_0,
				'JENIS_1'					  		=> $jenis_1,
				'OP_0'						  		=> $op_0,
				'OP_1'						  		=> $op_1,
				'OP_2'						  		=> $op_2,
				'SPBU_1'                      		=> $spbu_1,
				'SPBU_2'                      		=> $spbu_2,
				'BIAYA_SOPIR'			      		=> $biaya_sopir,
				'FLAG_BIAYA_SOPIR_KUMULATIF'  		=> $flag_biaya_sopir_kumulatif!=1?"":"checked",
				'BIAYA_TOL'				      		=> $biaya_tol,
				'BIAYA_PARKIR'			      	   	=> $biaya_parkir,
				'BIAYA_BBM'				           	=> $biaya_bbm,
				'FLAG_BIAYA_VOUCHER_BBM'	       	=> $flag_biaya_voucher_bbm!=1?"":"checked",
				'KOMISI_PENUMPANG_SOPIR'		   	=> $komisi_penumpang_sopir,
				'KOMISI_PENUMPANG_CSO'			   	=> $komisi_penumpang_cso,
				'KOMISI_PAKET_SOPIR'			   	=> $komisi_paket_sopir,
				'KOMISI_PAKET_CSO'				   	=> $komisi_paket_cso,
				'KODE_AKUN_PENDAPATAN_PENUMPANG'   	=> $kode_akun_pendapatan_penumpang,
				'KODE_AKUN_PENDAPATAN_PAKET'	   	=> $kode_akun_pendapatan_paket,
				'KODE_AKUN_BIAYA_SOPIR'			   	=> $kode_akun_biaya_sopir,
				'KODE_AKUN_BIAYA_TOL'			   	=> $kode_akun_biaya_tol,
				'KODE_AKUN_BIAYA_PARKIR'		   	=> $kode_akun_biaya_parkir,
				'KODE_AKUN_BIAYA_BBM'			   	=> $kode_akun_biaya_bbm,
				'KODE_AKUN_KOMISI_PENUMPANG_SOPIR' 	=> $kode_akun_komisi_penumpang_sopir,
				'KODE_AKUN_KOMISI_PENUMPANG_CSO'   	=> $kode_akun_komisi_penumpang_cso,
				'KODE_AKUN_KOMISI_PAKET_SOPIR'	   	=> $kode_akun_komisi_paket_sopir,
				'KODE_AKUN_KOMISI_PAKET_CSO'	   	=> $kode_akun_komisi_paket_cso,
				'KODE_AKUN_CHARGE'				  	=> $kode_akun_charge,
				'BGCOLOR_PESAN'					   	=> $bgcolor_pesan,
				'PESAN'						       	=> $pesan,
				'U_JURUSAN_ADD_ACT'=>append_sid('pengaturan_jurusan.'.$phpEx)
			)
		);

    //Show Dialog Ubah
	case 3.2:
		$id_jurusan = getVariabel('idjurusan');
		showDialogUbah($id_jurusan);

	exit;

	//Show Dialog Tamabah
	case 4:
		showDialogTambahTest();
	exit;

	//Proses Data
	case 4.1:

        $id_jurusan 							= getVariabel("id_jurusan");
        $kode       							= str_replace(" ","", getVariabel("kode_jurusan"));
        $kode_jurusan_old       				= str_replace(" ","", getVariabel("kode_jurusan_old"));
        $asal 									= getVariabel("cabangasal");
        $tujuan									= getVariabel("cabangtujuan");
        $estimasi_waktu_tempuh					= getVariabel("estimasiwaktu");
        $harga_tiket 							= getVariabel("hargatiket");
        $harga_paket_kilo_pertama				= getVariabel("hargapaketpertama");
        $harga_paket_kilo_berikut				= getVariabel("hargapaketberikut");
        $flag_aktif 							= getVariabel("isaktif");
        $flag_jenis 							= getVariabel("flagjenis");
        $flag_op_jurusan 						= getVariabel("flagopjurusan");
        $biaya_sopir 							= getVariabel("biayasopir");
        $flag_biaya_sopir_kumulatif				= getVariabel("flagbiayasopirkumulatif");
        $biaya_tol	 							= getVariabel("biayatol");
        $biaya_tol_extra	 					= getVariabel("biayatolextra");
        $biaya_parkir 							= getVariabel("biayaparkir");
        $bbm_liter			                    = getVariabel("bbmliter");
		$bbm_liter_ext		                    = getVariabel("bbmliterext");
		$bbm_rupiah			                    = getVariabel("bbmrupiah");
		$bbm_rupiah_ext		                    = getVariabel("bbmrupiahext");
        $flag_biaya_voucher_bbm 				= getVariabel("flagbiayavoucherbbm");
        $lokasi_spbu 							= getVariabel("lokasipengisianbbm");
        $terjadi_error 							= false;


//        echo($flag_biaya_voucher_bbm);
//        exit;
//		$kode 				= getVariabel("kode_jurusan");
//		$asal 				= getVariabel("cabangasal");
//		$tujuan 			= getVariabel("cabangtujuan");
//		$harga_tiket		= getVariabel("hargatiket");
//		//$harga_paket		= getVariabel("hargapaket");
//		$bbm_liter			= getVariabel("bbmliter");
//		$bbm_liter_ext		= getVariabel("bbmliterext");
//		$bbm_rupiah			= getVariabel("bbmrupiah");
//		$bbm_rupiah_ext		= getVariabel("bbmrupiahext");
//		$flag_aktif 		= getVariabel("flagaktif");
//		$flag_op_jurusan 	= getVariabel("flagopjurusan");
//		$biaya_sopir 		= getVariabel("biayasopir");
//		$biaya_tol 			= getVariabel("biayatol");
//		if($Jurusan->periksaDuplikasi($kode) && $kode!=$kode_jurusan_old){
//			$pesan="<font color='white' size=3>Kode jurusan yang dimasukkan sudah terdaftar dalam sistem!</font>";
//			$bgcolor_pesan="red";
//			$terjadi_error=true;
//		}else{
//			
//		}
		
		//return false;
		echo(prosesSimpan(
			$id_jurusan,
            $kode,
            $kode_jurusan_old,
            $asal,
            $tujuan,
            $estimasi_waktu_tempuh,
            $harga_tiket,
            $biaya_sopir,
            $biaya_tol,
            $biaya_tol_extra,
            $biaya_parkir,
            $bbm_liter,
            $bbm_liter_ext,
            $bbm_rupiah,
            $bbm_rupiah_ext,
            $flag_aktif,
            $flag_jenis,
            $harga_paket_kilo_pertama,
            $harga_paket_kilo_berikut,
            $flag_op_jurusan,
            $flag_biaya_sopir_kumulatif,
            $flag_biaya_voucher_bbm,
            $lokasi_spbu

		));
		break;

	//Ubah Status Aktif
    case 5:
    $id_jurusan = getVariabel("id_jurusan");
    ubahStatusAktif($id_jurusan);
    break;

	//Set Combo Cabang
	case 7:
		//SET COMBO CABANG

		$cari       = getVariabel('filter');
		$cabang_asal= getVariabel('cabangasal');

		setListCabang($cari,$cabang_asal);

		break;

	case 8:
		$result_kolom = $Jurusan->dataLayout();

		while ($kolom = $db->sql_fetchrow($result_kolom)){
			$arr[$kolom['KodeLayout']] = array(
				'harga_tiket' => getVariabel('hargatiket_'.$kolom['KodeLayout']),
				'bbm_liter' => getVariabel('liter_'.$kolom['KodeLayout'])
			);
		}
		$str_harga_tiket = json_encode($arr);

		echo $str_harga_tiket;

	exit;

}



// METHODES & PROCESSES ===============================================================================================
function showData(){
global $db,$cari,$order_by,$sort,$idx_page,$template,$VIEW_PER_PAGE,$userdata,$id_page,$scroll_value, $show_dialog_tambah;

	$Jurusan = new Jurusan();
	$Layout = $Jurusan->dataLayout();
//	echo $Layout;

	$template->set_filenames(array('body' => 'pengaturan.jurusan/index.tpl'));

	//cek permission untuk CRUD
	$Permission = new Permission();

	if($Permission->isPermitted($userdata["user_level"],$id_page.".1")){
		$template->assign_block_vars("CRUD_ADD",array());
		$template->assign_block_vars("CRUD_DEL",array());
		$template->assign_block_vars("EXPORT",array());
	}
	$result = $Jurusan->ambilData($order_by,$sort,$idx_page,$cari);


	$temp_cari=str_replace("asal=","",$cari);
	if($temp_cari==$cari){
		$kondisi_asal = "";
	}
	else{
		$kondisi_asal = "AND f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$temp_cari%' ";
		$cari=$temp_cari;
	}

	$temp_cari=str_replace("tujuan=","",$cari);
	if($temp_cari==$cari){
		$kondisi_tujuan = "";
	}
	else{
		$kondisi_tujuan = "AND f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$temp_cari%' ";
		$cari=$temp_cari;
	}
	//PAGING======================================================
	$paging=setPaging($idx_page,"formdata");
	//END PAGING==================================================

	$no = 0;
    $layoutpaket = [
        "DOK",
        "XS",
        "S",
        "M",
        "L",
        "XL"
    ];
	//FETCH DATA LAYOUT========================================================
	//$sql = "SELECT IdLayout,KodeLayout FROM tbl_md_kendaraan_layout ORDER BY IdLayout";
	$result_kolom = $Layout;
	$i = 0;
	while ($kolom = $db->sql_fetchrow($result_kolom)){
		$template->assign_block_vars("DATA_LAYOUT", array(
			"kodelayout" => $kolom["KodeLayout"],
			'iharga' => $i
		));
		$i++;

	}
	while($row=$db->sql_fetchrow($result)){
		$no++;
		$odd = ($no%2)==0?"even":"odd";

        if($row["Aktif"]=="AKTIF"){
            $class_crud_aktif = "crudgreen";
        }
        else{
            $odd = "red";
            $class_crud_aktif = "crud";
        }

		$class_op_jurusan="";

		if($row['FlagOperasionalJurusan']==1){
			$class_op_jurusan	= "class='jurusan_paket'";
		}
		elseif($row['FlagOperasionalJurusan']==2){
			$class_op_jurusan	= "class='jurusan_paket_travel'";
		}

		$template->assign_block_vars(
			'ROW', array(
				'odd'		=>$odd,
				'idx'		=>$no,
				'id'        =>$row['IdJurusan'],
				'no'		=>$id_page*$VIEW_PER_PAGE+$no,
				'class_op_jurusan'=>$class_op_jurusan,
				'kode'=>$row['KodeJurusan'],
				'asal'=>$row['NamaCabangAsal'],
				'tujuan'=>$row['NamaCabangTujuan'],
				'jenis'=>($row['FlagLuarKota']==1)?"LUAR KOTA":"DALAM KOTA",
                'classcrudaktif' => $class_crud_aktif,
				'aktif'  => $row['Aktif'],
			)
		);
		//JSON HARGA TIKET====
		$stringjson=$row['HargaTiket'];

		//JSON LITER BBM & EXTRA====
		$stringliter_bbm=$row['LiterBBM'];
		$stringliter_bbm_ext=$row['LiterBBMExtra'];

		//RUPIAH BBM & EXTRA====
        $stringrupiah_bbm=$row['RupiahBBM'];
        $stringrupiah_bbm_ext=$row['RupiahBBMExtra'];

        $stringpaket_pertama = $row['HargaPaketPertama'];
        $stringpaket_berikut = $row['HargaPaketBerikut'];


		$arr_harga = json_decode($stringjson, true);
		$arr_liter_bbm = json_decode($stringliter_bbm, true);
		$arr_liter_bbm_ext = json_decode($stringliter_bbm_ext, true);
        $arr_rupiah_bbm = json_decode($stringrupiah_bbm, true);
        $arr_rupiah_bbm_ext = json_decode($stringrupiah_bbm_ext, true);

        $arr_paket_pertama= json_decode($stringpaket_pertama, true);
        $arr_paket_berikut= json_decode($stringpaket_berikut, true);
        //echo($arr_paket_pertama);
		$db->sql_rowseek(0, $result_kolom);
		//harga2
		while($kolom=$db->sql_fetchrow($result_kolom)){
			//echo $kolom['KodeLayout'] ;
			$template->assign_block_vars("ROW.KOLOM_HARGA",array(
				"harga" => $arr_harga[$kolom["IdLayout"]]
			));
            $template->assign_block_vars("ROW.KOLOM_BBM",array(
                "literbbm" => $arr_liter_bbm[$kolom["IdLayout"]],
                "literbbmext" => $arr_liter_bbm_ext[$kolom["IdLayout"]],
                "rupiahbbm" => $arr_rupiah_bbm[$kolom["IdLayout"]],
                "rupiahbbmext" => $arr_rupiah_bbm_ext[$kolom["IdLayout"]]
            ));

		}

//        'harga_paket_1'=>number_format($row['HargaPaket1KiloPertama'],0,",",".")."<br>+".number_format($row['HargaPaket1KiloBerikut'],0,",",".")
//

        $l = 0;
        while($layoutpaket[$l]){
            $template->assign_block_vars("ROW.KOLOM_PAKET_PERTAMA", array(
                "paketpertama" => number_format($arr_paket_pertama[$layoutpaket[$l]],0,",",".")."<br>+".number_format($arr_paket_berikut[$layoutpaket[$l]],0,",",".")
            ));
            $l++;
        }



		//ACTION
		$template->assign_block_vars("ROW.ACT_EDIT", array());
		$template->assign_block_vars("ROW.ACT_DEL", array());

	}

	if($no>0){
		$template->assign_block_vars("TABLE_HEADER",array());
	}
	else{
		$template->assign_block_vars("NO_DATA",array());
	}

	//rowseek
	$db->sql_rowseek(0, $result_kolom);

	//kolom_header==================================
	while($row=$db->sql_fetchrow($result_kolom)) {
		$template->assign_block_vars("TABLE_HEADER.KOLOM", array(
			"kolomharga" => $row['KodeLayout'],
			"kolombbmliter" => $row['KodeLayout'],
			"kolombbmliterext" => $row['KodeLayout'],
			"kolombbmrupiah" => $row['KodeLayout'],
			"kolombbmrupiahext" => $row['KodeLayout']
		));
	}
	$k = 0;
	while($layoutpaket[$k]){
        $template->assign_block_vars("TABLE_HEADER.LAYOUTPAKET", array(
            "kolompaket" => $layoutpaket[$k]
        ));
	    $k++;
    }
    //==============================================

	//var_dump($result_kolom);
	$template->assign_vars(array(
			'URL_CRUD'	    => basename(__FILE__),
			'CARI'          => $cari,
			'ORDER'         => $order_by,
			'SORT'          => $sort,
			'IDX_PAGE'      => $idx_page,
			'PAGING'        => $paging,
			'U_EXPORT_EXCEL'=> substr(basename(__FILE__),0,-4).".export.excel.php",
			'SCROLL_VALUE'  => ($scroll_value==""?0:$scroll_value),
			'SHOW_DIALOG_TAMBAH'  => ($show_dialog_tambah==3?1:0)
		)
	);
	$template->pparse('body');
}


function prosesSimpan(
	$id_jurusan,
    $kode,
    $kode_jurusan_old,
    $asal,
    $tujuan,
	$estimasi_waktu_tempuh,
	$harga_tiket,
    $biaya_sopir,
    $biaya_tol,
    $biaya_tol_extra,
    $biaya_parkir,
    $bbm_liter,
    $bbm_liter_ext,
    $bbm_rupiah,
    $bbm_rupiah_ext,
    $flag_aktif,
    $flag_jenis,
    $harga_paket_kilo_pertama,
    $harga_paket_kilo_berikut,
    $flag_op_jurusan,
    $flag_biaya_sopir_kumulatif,
    $flag_biaya_voucher_bbm,
    $lokasi_spbu
){

    global $template;
	$Jurusan = new Jurusan();

	if ($id_jurusan==""){
        $template->set_filenames(array('body' => 'pengaturan.jurusan/edit.tpl'));
        if($Jurusan->periksaDuplikasi($kode) && $kode!=$kode_jurusan_old){
            $res = array("errorjurusan" => "Kode yang dimasukkan sudah terdaftar !!");
            return json_encode($res);
        }else{
            $id_jurusan= $Jurusan->tambah1($kode,$asal,$tujuan,$estimasi_waktu_tempuh,
                $harga_tiket,$biaya_sopir,$biaya_tol,$biaya_tol_extra,$biaya_parkir,$bbm_liter, $bbm_liter_ext, $bbm_rupiah, $bbm_rupiah_ext,$flag_aktif,$flag_jenis,
                $harga_paket_kilo_pertama,$harga_paket_kilo_berikut,
                $flag_op_jurusan,$flag_biaya_sopir_kumulatif,$flag_biaya_voucher_bbm,$lokasi_spbu);
        }

	}else{
		$Jurusan->ubah($id_jurusan,$kode,
    $asal,
    $tujuan,
	$estimasi_waktu_tempuh,
	$harga_tiket,
    $biaya_sopir,
    $biaya_tol,
    $biaya_tol_extra,
    $biaya_parkir,
    $bbm_liter,
    $bbm_liter_ext,
    $bbm_rupiah,
    $bbm_rupiah_ext,
    $flag_aktif,
    $flag_jenis,
    $harga_paket_kilo_pertama,
    $harga_paket_kilo_berikut,
    $flag_op_jurusan,
    $flag_biaya_sopir_kumulatif,
    $flag_biaya_voucher_bbm,
    $lokasi_spbu);
	}
	$res = array("status" => "OK", "id_jurusan" => $id_jurusan);
	return json_encode($res);
	
}

function showDialogTambah(){
	global $template;


	$template->set_filenames(array('body' => 'pengaturan.jurusan/detail.tpl'));

	$template->assign_vars(array(
			"JUDUL"	            => "Tambah data Jurusan",
			"STATUS_AKTIF"      => "AKTIF",
			"STATUS_ONLINE"     => "OFFLINE",
			"JENIS_JADWAL"      => "INDUK"
		)
	);

	$template->pparse('body');
}
function showDialogUbah($id_jurusan){
	global $template, $db;
	$Jurusan = new Jurusan();

	$template->set_filenames(array('body' => 'pengaturan.jurusan/edit.tpl'));

	$result_kolom = $Jurusan->dataLayout();

	$data = $Jurusan->ambilDataDetail($id_jurusan);
	$harga_tiket = json_decode($data['HargaTiket'],true);
	$liter_bbm = json_decode($data['LiterBBM'],true);
	$liter_bbm_extra = json_decode($data['LiterBBMExtra'],true);

    $rupiah_bbm = json_decode($data['RupiahBBM'],true);
    $rupiah_bbm_extra = json_decode($data['RupiahBBMExtra'],true);

	$db->sql_rowseek(0, $result_kolom);
	//harga2
	while($kolom=$db->sql_fetchrow($result_kolom)){
		//echo $kolom['KodeLayout'] ;
		$template->assign_block_vars("KOLOM_HARGA",array(
			"harga" => $harga_tiket[$kolom["IdLayout"]],
			"literbbm" => $liter_bbm[$kolom["IdLayout"]],
			"literbbmext" => $liter_bbm_extra[$kolom["IdLayout"]],
            "rupiahbbm" => $rupiah_bbm[$kolom["IdLayout"]],
            "rupiahbbmext" => $rupiah_bbm_extra[$kolom["IdLayout"]],
            "idlayout" => $kolom["IdLayout"],
            "namalayout" => $kolom["KodeLayout"]
		));

	}

	//==================PAKET=================================================
    $arr_paket_pertama= json_decode($data['HargaPaketPertama'],true);
    $arr_paket_berikut= json_decode($data['HargaPaketBerikut'],true);
    //paket
    $layoutpaket = [
        "DOK",
        "XS",
        "S",
        "M",
        "L",
        "XL"
    ];
    $l = 0;
    while($layoutpaket[$l]){
        $template->assign_block_vars("KOLOM_HARGA_PAKET", array(
            "jenispaket" => $layoutpaket[$l],
            "paketpertama" => $arr_paket_pertama[$layoutpaket[$l]],
            "paketberikut" => $arr_paket_berikut[$layoutpaket[$l]],
        ));
        $l++;
    }
	
	$temp_var_op_jurusan	=$data['FlagOperasionalJurusan'];
	$$temp_var_op_jurusan	="selected";
	//echo $temp_var_op_jurusan;
	$template->assign_vars(array(

			"JUDUL"	                => "Tambah data Jurusan",
			"STATUS_ONLINE"         => "OFFLINE",
			"JENIS_JADWAL"          => "INDUK",
			'ID_JURUSAN'		    => $data["IdJurusan"],
			'KODE_JURUSAN_OLD'	    => $data['KodeJurusan'],
			'KODE_JURUSAN'		    => $data["KodeJurusan"],

			'CABANG_ASAL'		    => $data["KodeCabangAsal"],
			'CABANG_TUJUAN'		    => $data["KodeCabangTujuan"],
			'ESTIMASI_WAKTU_TEMPUH'	=> $data["EstimasiWaktuTempuh"],

			'HARGA_TIKET'		    => $data['HargaTiket'],
			'HARGA_PAKET_PERTAMA'	=> $data["HargaPaketPertama"],
			'HARGA_PAKET_BERIKUT'	=> $data["HargaPaketBerikut"],

			'IS_BBM_VOUCHER'	    => $data["IsBBMVoucher"]!=1?"":"checked",
			'IS_BIAYA_BBM_LITER'    => $data["IsBiayaBBMLiter"]!=1?"":"checked",
			'RUPIAH_BBM'		    => $data["RupiahBBM"],
			'RUPIAH_BBM_EXTRA'	    => $data["RupiahBBMExtra"],
			'LITER_BBM'			    => $data["LiterBBM"],
			'LITER_BBM_EXTRA'	    => $data["LiterBBMExtra"],
			'SPBU'				    => setComboSPBU($data["SPBU"]),

			'BIAYA_SOPIR'		    => $data["BiayaSopir"],
			'BIAYA_SOPIR_KUMULATIF' => $data["IsBiayaSopirKumulatif"]!=1?"":"checked",
			'BIAYA_TOL'			    => $data["BiayaTol"],
			'BIAYA_TOL_EXTRA'	    => $data["BiayaTolExtra"],
			'BIAYA_PARKIR'		    => $data["BiayaParkir"],

			'FLAG_OP_JURUSAN'	    => setComboOp($data["FlagOperasionalJurusan"]),
			'FLAG_LUAR_KOTA'	    => $data['FlagLuarKota'],
			'STATUS_AKTIF'			    => $data["IsAktif"]
			//'IS_AKTIF'			    => $data["IsAktif"]
		)
	);

	//rowseek
	$db->sql_rowseek(0, $result_kolom);
	while($row=$db->sql_fetchrow($result_kolom)) {
		//echo $result_kolom;
		$template->assign_block_vars("KOLOM", array(
			"kolomharga" => $row['KodeLayout'],
		));
	}

	$template->pparse('body');
}

function setComboSPBU($id_spbu){
	//SET COMBO SPBU
	global $db;
	
	$SPBU = new SPBU();
	$result = $SPBU->setComboSPBU();
	

	$opt_spbu="";
	if($id_spbu == ''){
		$nselected	 = ($id_spbu!='')?"":"not selected";
		$opt_spbu .="<option value='' $nselected >------------PILIH-----------</option>";
	}

	while($row=$db->sql_fetchrow($result)){
		$selected	 = ($id_spbu!=$row['IdSPBU'])?"":"selected";
		$opt_spbu .="<option value='$row[IdSPBU]' $selected>$row[Nama]</option>";


	}

	return $opt_spbu;
	//END SET COMBO SPBU
}


function showDialogTambahTest(){
	global $template, $db;
	$Jurusan = new Jurusan();
	$SPBU = new SPBU();

	$Layout = $Jurusan->dataLayout();
	$PomBensin = $SPBU->ambilData();

	$template->set_filenames(array('body' => 'pengaturan.jurusan/detail_test.tpl'));

	//LAYOUT
	$result_kolom = $Layout;
	$i = 0;
	while ($kolom = $db->sql_fetchrow($result_kolom)){

		$template->assign_block_vars("DATA_LAYOUT", array(
			"kodelayout" => $kolom["KodeLayout"],
			"idlayout" => $kolom["IdLayout"],
			'iharga' => $i
		));
		$i++;
	}
	//paket
    $layoutpaket = [
        "DOK",
        "XS",
        "S",
        "M",
        "L",
        "XL"
    ];
    $id = 0;
    while($layoutpaket[$id]){
        $template->assign_block_vars("PAKET_INPUT", array(
            'paketname' =>  $layoutpaket[$id],
            'paketnamelayout' =>  $layoutpaket[$id],
            'num' => $id
        ));
        //echo ($id);
        $id++;
    }

	//POM BENSIN
	$result_spbu = $PomBensin;
	$idspbu = 0;
	while($spbu = $db->sql_fetchrow($result_spbu)){
		$template->assign_block_vars("DATA_SPBU", array(
			'idpombensin' => $spbu["IdSPBU"],
			'pombensin' => $spbu["Nama"]
		));
		$idspbu++;
	}

    $template->assign_vars(array(
			"JUDUL"	            => "Tambah data Jurusan",
			"STATUS_AKTIF"      => 0
		)
	);

	$template->pparse('body');
}

function prosesSimpan1($kode, $asal, $tujuan,$harga_tiket, $bbm_liter, $bbm_liter_ext, $bbm_rupiah, $bbm_rupiah_ext, $flag_aktif, $flag_op_jurusan, $biaya_sopir, $biaya_tol){

	$J = new Jurusan();
	

	return $J->tambah2($kode, $asal, $tujuan, $harga_tiket,$bbm_liter, $bbm_liter_ext, $bbm_rupiah, $bbm_rupiah_ext, $flag_aktif, $flag_op_jurusan, $biaya_sopir, $biaya_tol);
}

function ubahStatusAktif($id_jurusan){
    $Jurusan = new Jurusan();

    if($Jurusan->ubahStatusAktif($id_jurusan)){
        $ret_val["status"] = "OK";
    }
    else{
        $ret_val["status"] = "GAGAL";
    }

    echo(json_encode($ret_val));
}//ubahStatus


function setListCabang($cari="",$cabang_asal=""){
	global $db;

	$Jurusan = new Jurusan();

	$result = $Jurusan->setComboListCabang($cari,$cabang_asal);

	while($row=$db->sql_fetchrow($result)){
		$list_cabang[]=array("group"=>$row["Kota"],"id"=>$row['KodeCabang'],"text"=>$row["Nama"]);
	}

	$ret_val  =json_encode($list_cabang);

	echo($ret_val);
}//setListCabang

function setListJadwal($id_jurusan,$cari=""){
	global $db;

	$Jadwal = new Jadwal();

	$result = $Jadwal->setComboListJadwal($id_jurusan,$cari);

	while($row=$db->sql_fetchrow($result)){
		$list_cabang[]=array("group"=>"","id"=>$row["KodeJadwal"],"text"=>$row["KodeJadwal"]." [$row[JamBerangkat]");
	}

	$ret_val  =json_encode($list_cabang);

	echo($ret_val);
}//setListJadwal


?>