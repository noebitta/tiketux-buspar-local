<?php
//
// AUTENTIFIKASI (login n logout)
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';

include($adp_root_path . 'common.php');

//SESSION
$id_page  = 900;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);


//CEK MACADDRESS
$sql ="SELECT Id,KodeCabang, IsAktif
			FROM  tbl_mac_address
			WHERE MacAddress = '" .$HTTP_POST_VARS['MacAddress']. "' AND IsAktif=1";


if ( (!$result = $db->sql_query($sql)) )
{    
	echo("Status=ERROR|Result=".__LINE__);exit;
}


$data_mac_address = $db->sql_fetchrow($result);

// LOGIN
$uname = adp_clean($HTTP_POST_VARS['username']); // we clean
$upass = $HTTP_POST_VARS['password'];

$sql ="SELECT user_id, username, user_password, user_active, user_level,status_online
			FROM  tbl_user
			WHERE username = '" . str_replace("\\'", "''", $uname) . "'";


if ( (!$result = $db->sql_query($sql)) )
{    
	echo("Status=ERROR|Result=".__LINE__);exit;
}


if( $row = $db->sql_fetchrow($result) ){ 

	$signature		= $HTTP_POST_VARS['signature'];
	$gen_signature= md5($uname."#".$HTTP_POST_VARS['MacAddress']."#".date("Ymd")."#".$row["user_password"]);

	//CEK PASSWORD
	if( md5($upass) == $row['user_password'] && $row['user_active']){
	  // PASSWORD MATCH

	  //SECURITY CEK HASHING
	  if($signature!=$gen_signature){
			//hashing tidak sama
			echo("Status=ERROR|Result=Signature Tidak sama");exit;
		}
		//END SECURITY CEK

		if($data_mac_address[0]=="" && $row["user_level"]>$USER_LEVEL_INDEX["MANAJER"]){
			echo("Status=ERROR|Result=Mac Address Tidak Terdaftar");exit;
		}

	  // register ulang session
		$autologin = ( isset($HTTP_POST_VARS['autologin']) ) ? TRUE : 0;
		$session_id = session_begin($row['user_id'], $user_ip, PAGE_INDEX, FALSE, $autologin);
		$userdata['user_level']=$row[4];
		
		if($data_mac_address[0]!=""){
			//JIKA MAC ADDRESS TERDAFTAR

			//UPDATE KODE CABANG LOGIN
			$sql ="UPDATE tbl_user	SET KodeCabang='".$data_mac_address['KodeCabang']."' WHERE user_id =".$row['user_id'];

			if ( (!$result = $db->sql_query($sql)) ){    
				echo("Status=ERROR|Result=".__LINE__);exit;
			}
		}
		
		if($session_id)
		{				
			//mencatat log login user
			$sql ="CALL sp_log_user_login($row[user_id],'$user_ip')";

			if ( (!$result = $db->sql_query($sql))){    
				echo("Status=ERROR|Result=".__LINE__);exit;
			}
			
			if($data_mac_address[0]!=""){
				$url = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : "main.".$phpEx;
			}
			elseif($row["user_level"]<=$USER_LEVEL_INDEX["MANAJER"]){
				$url = ( !empty($HTTP_POST_VARS['redirect']) ) ? str_replace('&amp;', '&', htmlspecialchars($HTTP_POST_VARS['redirect'])) : "pengaturan_macaddress.".$phpEx;
			}

			echo("Status=OK|Result=".append_sid($url, true));exit;
		}
		else
		{
			echo("Status=ERROR|Result=".__LINE__);exit;
		}
	}
	else{
		// PASSWORD TIDAK MATCH
		echo("Status=ERROR|Result=Username atau Password tidak benar");exit;
	}
}
else{
	//USER TIDAK DITEMUKAN
	echo("Status=ERROR|Result=Username atau Password tidak benar");exit;
}  
?>