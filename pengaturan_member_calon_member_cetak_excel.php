<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 603;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

//INISIALISASI

$kondisi_sort	= ($sort_by=='') ?"ORDER BY Nama" : "ORDER BY $sort_by $order";
		
$sql = 
	"SELECT * 
	FROM view_calon_member_frekwensi 
	WHERE Frekwensi>=$MINIMUM_KEBERANGKATAN_JADI_MEMBER
	$kondisi_sort";
					
if ($result = $db->sql_query($sql)){
		
	$i=1;
	
	$objPHPExcel = new PHPExcel();          
  $objPHPExcel->setActiveSheetIndex(0);  
  $objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
  $objPHPExcel->getActiveSheet()->mergeCells('A2:G2');
  
	//HEADER
	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Data Pelanggan Potensial Per Tanggal '.dateNowD_MMM_Y());
  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Telp');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Frekwensi Pergi');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	
	$idx=0;
	
	while ($row = $db->sql_fetchrow($result)){
		$idx++;
		$idx_row=$idx+3;
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['Nama']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, "'".$row['Telp']);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['Frekwensi']);
		
	}
	$temp_idx=$idx_row;
	
	$idx_row++;		
		
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

	if ($idx>0){
		header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Data Pelanggan Potensial Per Tanggal '.dateNowD_MMM_Y().'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output'); 
	}
}
else{
	die_error('Err:',__LINE__);
}   
  
  
?>
