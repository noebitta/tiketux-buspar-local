<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

class Pelanggan{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Pelanggan(){
		$this->ID_FILE="C-PLG";
	}
	
	//BODY
	
	function periksaDuplikasi($no_telp){
		
		/*
		Desc	:Mengembalikan true jika no_polisi tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(IdPelanggan) AS jumlah_data
			FROM tbl_pelanggan
			WHERE NoTelp='$no_telp' ";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function tambah(
		$no_telp ,$nama, $alamat,
    $kode_jurusan_terakhir){

		//kamus
		global $db,$userdata;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql	= 
			"INSERT INTO tbl_pelanggan (
		    NoTelp, Nama, Alamat, TglPertamaTransaksi, TglTerakhirTransaksi,
		    CSOTerakhir, KodeJurusanTerakhir, FrekwensiPergi)
		  VALUES(
				'$no_telp','$nama','$alamat', NOW(),NOW(),
		    '$userdata[user_id]', '$kode_jurusan_terakhir',1)
      ON DUPLICATE KEY UPDATE TglTerakhirTransaksi=NOW(),FrekwensiPergi=FrekwensiPergi+1;";
		
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ubah(
		$no_telp_old,$p_hp_old,
		$no_hp, $no_telp ,$nama,
	  $alamat,$tgl_terakhir_transaksi,
	  $cso_terakhir, $kode_jurusan_terakhir){
	  
		/*
		IS	: data kendaraan sudah ada dalam database
		FS	:Data kendaraan diubah 
		*/
		
		//kamus
		global $db;
		
		$nama	= str_replace("'"," ",$nama);
		
		//MENGUBAH DATA KEDALAM DATABASE
		$sql	= 
			"UPDATE tbl_pelanggan SET
				NoHP='$no_hp', NoTelp='$no_telp', Nama='$nama',
				TglTerakhirTransaksi='$tgl_terakhir_transaksi',
				 Alamat='$alamat',
				CSOTerakhir='$cso_terakhir', KodeJurusanTerakhir='$kode_jurusan_terakhir', FrekwensiPergi=FrekwensiPergi+1
			WHERE NoTelp='$no_telp_old';";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE ".__LINE__);
		}
		
		return true;
	}
	
	function hapus($list_sopir){
	  
		/*
		IS	: data sopir sudah ada dalam database
		FS	:Data sopir dihapus
		*/
		
		//kamus
		global $db;
		
		//MENGHAPUS DATA
		$sql =
			"DELETE FROM tbl_md_sopir
			WHERE KodeSopir IN($list_sopir);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ubahStatusAktif($kode_sopir){
	  
		/*
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql ="CALL sp_sopir_ubah_status_aktif('$kode_sopir');";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
	
	function ambilDataDetail($no_telp){
		
		/*
		Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_pelanggan
			WHERE NoTelp='$no_telp'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
	}//  END ambilData
		
	function ambilDataDetailMember($no_id){
		
		/*
		Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_member
			WHERE IdMember='$no_id';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}// END ambilDataMember
		
	function registerMember($IdMember,$Nama,$JenisKelamin,$KategoriMember,$TempatLahir,$TglLahir,$NoKTP,$Alamat,$Kota,$KodePos,$Telp,$Handphone,$Email,$Pekerjaan,$Point,$IdKartu,$NoSeriKartu,$KataSandi){
		
		/*
		Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"INSERT INTO tbl_md_member (IdMember,Nama,JenisKelamin,TempatLahir,TglLahir,NoKTP,Alamat,Kota,KodePos,Telp,Handphone,Email,Pekerjaan,KataSandi) VALUES ('$IdMember','$Nama','$JenisKelamin','$TempatLahir','$TglLahir','$NoKTP','$Alamat','$Kota','$KodePos','$Telp','$Handphone','$Email','$Pekerjaan','$KataSandi')";
				
		if ($result = $db->sql_query($sql)){			
			return $result;
		} 
		else{
			die_error("Err: $this->ID_FILE $sql".__LINE__);	
		}
		
	}// END register User	
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		Desc	:Mengembalikan data sopir sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_md_sopir
			WHERE 
				(KodeSopir LIKE '$pencari' 
				OR Nama LIKE '$pencari' 
				OR HP LIKE '%$pencari%' 
				OR Alamat LIKE '%$pencari%'
				OR NoSIM LIKE '$pencari') 
				AND FlagAktif=1
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
}
?>