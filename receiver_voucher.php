<?php


// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

global $db;

$kode_voucher = $HTTP_POST_VARS['kode_voucher'];
$kode_booking = $HTTP_POST_VARS['kode_booking'];

$sql = "SELECT * FROM tbl_voucher WHERE KodeVoucher = '$kode_voucher'";
$data_voucher = $db->sql_fetchrow($db->sql_query($sql));
$voucher = $data_voucher['NilaiVoucher'];
$isharga = $data_voucher['IsHargaTetap'];

if($kode_voucher == "" || $kode_voucher != $data_voucher['KodeVoucher']){
    die("Kode Vocuher Not Valid");
}

if($data_voucher['NoTiket'] != ""){
    die("Kode Voucher Sudah Dipakai");
}

$sql = "SELECT KodeBooking FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";

$row = $db->sql_fetchrow($db->sql_query($sql));

if($kode_booking == "" || $kode_booking != $row['KodeBooking']){
    die("Kode Booking Not Valid");
}


$sql = "SELECT * FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";

$result = $db->sql_query($sql);

while($row = $db->sql_fetchrow($result)){
    if($voucher > 0 || $isharga == 1){
        $no_tiket           = $row['NoTiket'];
        $id_jurusan         = $row['IdJurusan'];
        $kode_jadwal        = $row['KodeJadwal'];
        $petugas_pengguna   = 0;

        $harga_tiket        = $row['HargaTiket'];
        $discount           = ($harga_tiket < $voucher)?$harga_tiket:$voucher;
        $total              = $harga_tiket-$discount;

        if($isharga == 1){
            $total = $discount;
            $discount = $harga_tiket-$total;
        }

        $sql ="UPDATE tbl_reservasi
               SET
                    Discount=$discount,
                    JenisDiscount='VOUCHER',
                    Total=$total
               WHERE NoTiket='".$no_tiket."';";

        if (!$db->sql_query($sql)){
           die("Error Update Reservasi");
        }

        $voucher = ($harga_tiket < $voucher)?$voucher-$harga_tiket:0;

        $sql =
            "UPDATE tbl_voucher
			SET
				NoTiket='".$no_tiket."',
				IdJurusan='".$id_jurusan."',
				KodeJadwal='".$kode_jadwal."',
				CabangBerangkat=f_jurusan_get_kode_cabang_asal_by_jurusan($id_jurusan),
				CabangTujuan=f_jurusan_get_kode_cabang_tujuan_by_jurusan($id_jurusan),
				WaktuDigunakan=NOW(),
				PetugasPengguna='".$petugas_pengguna."'
			WHERE KodeVoucher='".$kode_voucher."';";

        if (!$db->sql_query($sql)){
            die("Error Update Voucher");
        }

        if($isharga == 1){
            die("success");
        }
    }

    echo "success";
}
?>