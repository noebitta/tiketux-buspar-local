<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 208;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];


// LIST
$template->set_filenames(array('body' => 'daftar_manifest/daftar_manifest_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$Cabang	= new Cabang();

switch($mode){
	case 'getasal':
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;

    case 'ubahstatus':
        $no_spj     = $HTTP_GET_VARS['nospj'];
        $status     = $HTTP_GET_VARS['status'];

        $sql = "UPDATE tbl_spj SET IsLongTrip = $status WHERE NoSPJ = '$no_spj'";
        if(!$db->sql_query($sql)){
            echo ('alter("'.mysql_error().'");');
        }else{
            echo ('alter("berhasil");');
        }
        exit;
}

if($mode == 'delete'){
	$no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];
	// Cek Role
	if($userdata['user_level'] != 0){
		echo 0;
		exit;
	}else{
		// Hapus Manifest (tbl_biaya_op)
		$sql1 = "DELETE FROM tbl_biaya_op WHERE NoSPJ='$no_spj'";
		// Hapus Manifest (tbl_ba_op)
		//$sql2 = "DELETE FROM tbl_ba_bop WHERE NoSPJ='$no_spj'";
		// Update Manifest (tbl_posisi)
		$sql3 = "UPDATE tbl_posisi SET NoSPJ = null, KodeKendaraan ='', KodeSopir = '', TglCetakSPJ = null, PetugasCetakSPJ = null
                    WHERE NoSPJ = '$no_spj'";
		// Update Manifest (tbl_reservasi)
		$sql4 = "UPDATE tbl_reservasi SET KodeKendaraan = '', KodeSopir = '', NoSPJ = '', TglCetakSPJ = '', CetakSPJ = 0
                    WHERE NoSPJ = '$no_spj'";
		// Hapus Manifest (tbl_spj)
		$sql5 = "DELETE FROM tbl_spj WHERE NoSPJ='$no_spj'";
		if(!$db->sql_query($sql1)){
			echo 2;
			die_error("ERR: ".__LINE__);
		}/*else if(!$db->sql_query($sql2)){
			echo 3;
			die_error("ERR: ".__LINE__);
		}*/else if(!$db->sql_query($sql3)){
			echo 4;
			die_error("ERR: ".__LINE__);
		}else if(!$db->sql_query($sql4)){
			echo 5;
			die_error("ERR: ".__LINE__);
		}else if(!$db->sql_query($sql5)){
			echo 6;
			die_error("ERR: ".__LINE__);
		}else{
			echo 1;
			exit;
		}
	}

}

$kondisi =	$cari==""?"":
	" AND (Driver LIKE '%$cari%'
	    OR NoSPJ LIKE '%$cari%' 
		OR KodeJadwal LIKE '%$cari%'
		OR ts.NoPolisi LIKE '%$cari%'
		OR f_kendaraan_ambil_nopol_by_kode(ts.NoPolisi) LIKE '%$cari%')";

$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan))='$kota'":"";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'":"";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"TglBerangkat,Jamberangkat":$sort_by;


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"NoSPJ","tbl_spj ts",
"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
"WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"daftar_manifest.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	=
	"SELECT 
		KodeDriver,Driver,
		NoSPJ,TglSPJ,KodeJadwal,
		TglBerangkat,JamBerangkat,JumlahKursiDisediakan,
		JumlahPenumpang,JumlahPaket,ts.NoPolisi, f_kendaraan_ambil_nopol_by_kode(ts.NoPolisi) as NoPol,
		TIMEDIFF(TglSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) AS Keterlambatan,
		IF(TIMEDIFF(TglSPJ,CONCAT(DATE(TglBerangkat),' ',JamBerangkat)) >'00:30:00',1,0) IsTerlambat,
		NamaChecker,WaktuCheck,JumlahPenumpangCheck,JumlahPaketCheck, JumlahPenumpangTambahanCheck,
		JumlahPenumpangTanpaTiketCheck, CatatanTambahanCheck, PathFoto,IsLongTrip
	FROM tbl_spj ts JOIN tbl_md_kendaraan tk ON ts.NoPolisi = tk.KodeKendaraan
	WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
		
	if (($i % 2)==0){
		$odd = 'even';
	}

	if ($row['PathFoto']) {
		$foto = "<a href='http://daytrans-stg.tiketux.com/apichecker/".$row['PathFoto']."' target='_blank' >Foto</a>";
	}
	else{
		$foto = "--";
	}

	if($row['IsTerlambat']==1){
		$flagterlambat = "red";
		$lama_terlambat	= $row['Keterlambatan'];
	}
	else{
		$flagterlambat	= "";
		$lama_terlambat	= "";
	}

    if($row['IsLongTrip'] == 0){
        $bgtrip = $odd;
        $trip = "<a href='' onClick='return ubahStatus(\"$row[NoSPJ]\",1)'>Short</a>";
    }else{
        $bgtrip = 'greenyellow';
        $trip = "<a href='' onClick='return ubahStatus(\"$row[NoSPJ]\",0)'>Long</a>";;
    }

	$sqlKoli = "SELECT SUM(JumlahKoli) as Koli FROM tbl_paket WHERE NoSPJ = '".$row['NoSPJ']."'";

	if(!$resKoli = $db->sql_query($sqlKoli)){
		echo("Err:".__LINE__);exit;
	}
	$rw      = $db->sql_fetchrow($resKoli);

	$sqlBbm	 = "SELECT COUNT(Id) as jml, JumlahLiter FROM tbl_voucher_bbm WHERE NoSPJ = '".$row['NoSPJ']."'";

	if(!$resBbm = $db->sql_query($sqlBbm)){
		echo("Err:".__LINE__);exit;
	}
	$rwBbm   = $db->sql_fetchrow($resBbm);
	if($rwBbm['jml'] == 0){
		$bbm = 'Belum Isi';
		$bgbbm = 'red';
		$literbbm = 0;
	}else{
		$bbm = 'Sudah Isi';
		$bgbbm = '#c0ff53';
		$literbbm = $rwBbm['JumlahLiter'];
	}

	if($userdata['user_level'] == 0){
		$spjTemp        = "'".$row['NoSPJ']."'";
		$delete         = "<a href='#' onClick='return hapusData(\"".$row['NoSPJ']."\");'>Hapus<a/>";
	}else{
		$delete         = "";
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'flagterlambat'=>$flagterlambat,
				'no'=>$i+$idx_page*$VIEW_PER_PAGE,
				'jadwal'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
				'kodejadwal'=>$row['KodeJadwal'],
				'berangkat'=>dateparse(FormatMySQLDateToTglWithTime($row['TglSPJ'])),
				'keterlambatan'=>$lama_terlambat,
				'manifest'=>$row['NoSPJ'],
				'sopir'=>$row['Driver'],
				'mobil'=>$row['NoPolisi'],
				'nopol'=>$row['NoPol'],
				'penumpang'=>number_format($row['JumlahPenumpang'],0,",","."),
				'paket'=>number_format($row['JumlahPaket'],0,",","."),
				'waktucheck'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuCheck'])),
				'checker'=>$row['NamaChecker'],
				'penumpangcheck'=>number_format($row['JumlahPenumpangCheck'],0,",","."),
				'paketcheck'=>number_format($row['JumlahPaketCheck'],0,",","."),
				'bgpnpchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangCheck']==$row['JumlahPenumpang']?"#c0ff53":"red"),
				'bgpktchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPaketCheck']==$row['JumlahPaket']?"#c0ff53":"red"),
				'penumpangtcheck'=>number_format($row['JumlahPenumpangTambahanCheck'],0,",","."),
				'penumpangttcheck'=>number_format($row['JumlahPenumpangTanpaTiketCheck'],0,",","."),
				'koli' => $rw['Koli'],
				'bbm'  => $bbm,
				'bgbbm'=> $bgbbm,
				'literbbm'=> number_format($literbbm,2,'.',','),
				'catatan'=>$row['CatatanTambahanCheck'],
				'foto'=>$foto,
				'bgpnptchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangTambahanCheck']==0?"#c0ff53":"red"),
				'bgpnpttchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangTanpaTiketCheck']==0?"#c0ff53":"red"),
				'trip'=>$trip,
                'bgtrip'=>$bgtrip,
                'linkHapus' => $delete,
			)
		);
	$i++;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&kota=$kota&asal=$asal&tujuan=$tujuan&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&sort_by=$sort_by&order=$order&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=$kota&asal=$asal&tujuan=$tujuan";								
$script_cetak_excel="Start('daftar_manifest_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT


$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('daftar_manifest.'.$phpEx),
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'OPT_KOTA'			=> setComboKota($kota),
	'TXT_CARI'			=> $cari,
	'KOTA'					=> $kota,
	'ASAL'					=> $asal,
	'TUJUAN'				=> $tujuan,
	'A_SORT_1'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=TglBerangkat,JamBerangkat'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan berdasarkan Jadwal ($order_invert)",
	'A_SORT_2'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan berdasarkan KodeJadwal ($order_invert)",
	'A_SORT_3'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=TglSPJ'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan berdasarkan Berangkat ($order_invert)",
	'A_SORT_4'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=Keterlambatan'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan berdasarkan Keterlambatan ($order_invert)",
	'A_SORT_5'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=NoSPJ'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan berdasarkan Manifest($order_invert)",
	'A_SORT_6'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=Driver'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan berdasarkan Sopir ($order_invert)",
	'A_SORT_7'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=NoPolisi'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan berdasarkan Mobil ($order_invert)",
	'A_SORT_8'			=> append_sid('daftar_manifest.'.$phpEx.'?sort_by=JumlahPenumpang'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan berdasarkan Jumlah Penumpang ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>