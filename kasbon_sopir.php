<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassKasbonSopir.php');

// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){
	die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//#############################################################################

$page_title	= "Kasbon Sopir";
$interface_menu_utama=false;

function setComboSopir($kode_sopir_dipilih=""){
	//SET COMBO SOPIR
	global $db;
	$Sopir = new Sopir();
			
	$result=$Sopir->ambilData("","Nama,Alamat","ASC");
	$opt_sopir="<option value=''>- silahkan pilih sopir  -</option>";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_sopir_dipilih!=$row['KodeSopir'])?"":"selected";
			$opt_sopir .="<option value='$row[KodeSopir]' $selected>$row[Nama] ($row[KodeSopir])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_sopir;
	//END SET COMBO SOPIR
}

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 

$KasbonSopir	= new KasbonSopir();

if($mode=='0' || $mode==''){
	//MODE SHOW ALL TRANSAKSI
	
	$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
	$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
	$kota_dipilih 	= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
	$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
	$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

	if($HTTP_POST_VARS["txt_cari"]!=""){
		$cari=$HTTP_POST_VARS["txt_cari"];
	}
	else{
		$cari=$HTTP_GET_VARS["cari"];
	}

	$start	= $tanggal_mulai==''?true:false;
	$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
	$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
	$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
	$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
			
	$kondisi_cari	=($cari=="")?
		" WHERE tms.KodeSopir LIKE '%' ":
		" WHERE (tms.KodeSopir LIKE '$cari%' OR tms.Nama LIKE '%$cari%' OR tu.nama LIKE '%$cari%')";
		

		
	$order	=($order=='')?"ASC":$order;
		
	$sort_by =($sort_by=='')?"TglTransaksi":$sort_by;

	// LIST
	$template->set_filenames(array('body' => 'kasbon_sopir/kasbon_sopir_body.tpl')); 

	//PAGING======================================================
	$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
	$paging		= pagingData($idx_page,"Id",
	"(tbl_kasbon_sopir tks INNER JOIN tbl_md_sopir tms ON tks.KodeSopir=tms.KodeSopir) INNER JOIN tbl_user tu ON tks.IdKasir=tu.user_id",
	"&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&cari=".$cari."&sort_by=".$sort_by."&order=".$order,
	$kondisi_cari,"kasbon_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
	//END PAGING======================================================

	//mengambil query biaya op

	$sql	=
		"SELECT ID,TglTransaksi,tms.Nama AS NamaSopir ,tks.KodeSopir,Jumlah,tu.nama AS Kasir,IsBatal,WaktuBatal,WaktuCatatTransaksi
		FROM 
			(tbl_kasbon_sopir tks INNER JOIN tbl_md_sopir tms ON tks.KodeSopir=tms.KodeSopir)
			INNER JOIN tbl_user tu ON tks.IdKasir=tu.user_id
		$kondisi_cari AND (TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
	if ($result = $db->sql_query($sql)){
		$i = $idx_page*$VIEW_PER_PAGE+1;
	  while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$keterangan	= "Transaksi dicatat pada: ".FormatMySQLDateToTglWithTime($row['WaktuCatatTransaksi']);
			
			if($row['IsBatal']!=1){
				if($userdata['user_level']<=$USER_LEVEL_INDEX["MANAJEMEN"]){
					$action	= "<a href='#' onClick='false;batalkanTransaksi($row[ID]);'>Batalkan</a>";
				}
			}
			else{
				$odd	= 'red';
				$keterangan	.= "<br>Transaksi Dibatalkan pada ".FormatMySQLDateToTglWithTime($row['WaktuBatal']);
				$action	= "-none-";
			}
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i,
						'TglTransaksi'=>FormatMySQLDateToTglWithTime($row['TglTransaksi']),
						'NamaSopir'=>$row['NamaSopir'].$test,
						'KodeSopir'=>$row['KodeSopir'],
						'Jumlah'=>number_format($row['Jumlah'],0,",","."),
						'Kasir'=>$row['Kasir'],
						'Keterangan'=>$keterangan,
						'Action'=>$action
					)
				);
			
			$i++;
	  }
	} 
	else{
		//die_error('Cannot Load laporan_omzet_sopir',__FILE__,__LINE__,$sql);
		echo("Err:".__LINE__);exit;
	} 

	//KOMPONEN UNTUK EXPORT
	$parameter_cetak	= "&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih.
											"&cari=".$cari."&sort_by=".$sort_by."&order=".$order."";
		
	$script_cetak_pdf="Start('laporan_pembayaran_sopir_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
													
	$script_cetak_excel="Start('laporan_pembayaran_sopir_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
	//--END KOMPONEN UNTUK EXPORT

	//BEGIN KOMPONEN-KOMPONEN SORTING
	$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
	$parameter_sorting	= 
		"&page=".$idx_page."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=".$kota_dipilih.
		"&cari=".$cari."&order=".$order_invert."";
	//END KOMPONEN-KOMPONEN SORTING

	$template->assign_vars(array(
		'BCRUMP'    		=>setBcrump($id_page),
		'ACTION_CARI'		=> append_sid('kasbon_sopir.'.$phpEx),
		'TXT_CARI'			=> $cari,
		'TGL_AWAL'			=> $tanggal_mulai,
		'TGL_AKHIR'			=> $tanggal_akhir,
		'PAGING'				=> $paging,
		'OPT_SORT'			=> $opt_sort_by,
		'OPT_ORDER'			=> $opt_order,
		'CETAK_PDF'			=> $script_cetak_pdf,
		'CETAK_XL'			=> $script_cetak_excel,
		'A_SORT_1'			=> append_sid('kasbon_sopir.'.$phpEx.'?sort_by=tgltransaksi'.$parameter_sorting),
		'TIPS_SORT_1'		=> "Urutkan Tgl. Transaksi ($order_invert)",
		'A_SORT_2'			=> append_sid('kasbon_sopir.'.$phpEx.'?sort_by=namasopir'.$parameter_sorting),
		'TIPS_SORT_2'		=> "Urutkan Nama Sopir ($order_invert)",
		'A_SORT_3'			=> append_sid('kasbon_sopir.'.$phpEx.'?sort_by=kodesopir'.$parameter_sorting),
		'TIPS_SORT_3'		=> "Urutkan NRP ($order_invert)",
		'A_SORT_4'			=> append_sid('kasbon_sopir.'.$phpEx.'?sort_by=jumlah'.$parameter_sorting),
		'TIPS_SORT_4'		=> "Urutkan Jumlah ($order_invert)",
		'A_SORT_5'			=> append_sid('kasbon_sopir.'.$phpEx.'?sort_by=kasir'.$parameter_sorting),
		'TIPS_SORT_5'		=> "Urutkan Kasir ($order_invert)",
		'A_SORT_6'			=> append_sid('kasbon_sopir.'.$phpEx.'?sort_by=isbatal'.$parameter_sorting),
		'TIPS_SORT_6'		=> "Urutkan Keterangan ($order_invert)",
		'OPT_SOPIR'			=> setComboSopir()
		)
	);
	
	include($adp_root_path . 'includes/page_header.php');
	$template->pparse('body');
	include($adp_root_path . 'includes/page_tail.php');
		     
}
elseif($mode=='1'){
	//MODE  SIMPAN
	//get parameter values
	$TglTransaksi 		= FormatTglToMySQLDate($HTTP_POST_VARS['TglTransaksi']);
	$KodeSopir  			= $HTTP_POST_VARS['KodeSopir'];
	$Jumlah  					= $HTTP_POST_VARS['Jumlah'];
	$IdKasir					= $userdata['user_id'];
	$DiberikaDiCabang	= $userdata['KodeCabang'];
	
	
	if($KodeSopir==''){
		echo("alert(\"Anda belum memilih sopir yang akan kasbon!\");");
		exit;
	}
	
	if($Jumlah=='' || $Jumlah<10000){
		echo(
			"alert(\"Anda belum memasukkan jumlah uang yang akan di kasbon kan \\nATAU\\nJumlah yang dimasukkan dibawah Rp. 10.000!\");");
		exit;
	}
	
	$return_val	= $KasbonSopir->tambah($TglTransaksi,$KodeSopir,$Jumlah,$IdKasir,$DiberikaDiCabang);
	
	if($return_val){
		echo("alert(\"Data BERHASIL disimpan!\");window.location.reload();");
	}
	else{
		echo("alert(\"Data GAGAL disimpan!\");");
	}
	
	exit;
}
elseif($mode=='2'){
	//MODE  BATALKAN TRANSAKSI
	//get parameter values
	$ID  = $HTTP_POST_VARS['ID'];
	
	
	//SECURITY
	if($userdata['user_level']>$USER_LEVEL_INDEX["MANAJEMEN"]){
		exit;
	}
	
	$return_val	= $KasbonSopir->batalkan($ID);
	
	if($return_val){
		echo("alert(\"Data BERHASIL dibatalkan!\");window.location.reload();");
	}
	else{
		echo("alert(\"Data GAGAL dibatalkan!\");");
	}
	
	exit;
}

?>