<?php

class Promo{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function Promo(){
		$this->ID_FILE="C-PRM";
	}
	
	//BODY
	
	function periksaDuplikasi($kode,$asal,$tujuan,$berlaku_mula,$berlaku_akhir,$target_promo,$level_promo){
		
		/*
		ID	: 001
		Desc	:Mengembalikan true jika kode tidak ditemukan dalam database dan False jika  ditemukan
		*/
		
		//kamus
		global $db;
		
		$berlaku_mula		= FormatTglToMySQLDate($berlaku_mula);
		$berlaku_akhir	= FormatTglToMySQLDate($berlaku_akhir);
		
		$sql = "SELECT f_promo_periksa_duplikasi(
			'$kode','$asal','$tujuan',
			'$berlaku_mula','$berlaku_akhir','$target_promo',
			'$level_promo') AS jumlah_data";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				//jika data ditemukan,berarti no_polisi sudah pernah disimpan, maka akan langsung keluar dari rutin
				$ditemukan = ($row['jumlah_data']<=0)?false:true;
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $ditemukan;
		
	}//  END periksaDuplikasi
	
	function ambilDiscountPoint($kode_jadwal,$tgl_berangkat){
		
		/*
		ID	:002
		Desc	:Mengembalikan nilai discount
		*/
		
		//kamus
		global $db;
		
		$sql_jam_berangkat	= "(SELECT JamBerangkat FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal')";
		
		$sql_id_jurusan	= "f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')";
		
		$sql_kode_cabang_asal	="f_jurusan_get_kode_cabang_asal_by_jurusan($sql_id_jurusan)";
		$sql_kode_cabang_tujuan	="f_jurusan_get_kode_cabang_tujuan_by_jurusan($sql_id_jurusan)";
		
		$sql = 
			"SELECT JumlahDiscount,FlagDiscount,JumlahPoint,FlagTargetPromo,NamaPromo
			FROM tbl_md_promo
			WHERE 
				IF(KodeCabangAsal!='',KodeCabangAsal LIKE $sql_kode_cabang_asal,1)
				AND IF(KodeCabangTujuan!='',KodeCabangTujuan LIKE $sql_kode_cabang_tujuan,1)
				AND (CONCAT('$tgl_berangkat',' ',$sql_jam_berangkat) BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir))
				AND FlagAktif=1
			ORDER BY LevelPromo DESC LIMIT 0,1;";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $row;
		
	}//  END ambilDiscountDanPoint
	
	function tambah(
		$kode_promo, $nama_promo, $priority, $kode_cabang_asal, $kode_cabang_tujuan, 
		$mulai_jam, $akhir_jam, $tanggal_mulai_promo, $tanggal_akhir_promo, $jumlah_minimum,$flag_discount,
    $discount, $target_discount,$aktif){
	  
		/*
		ID	: 002
		IS	: data promo belum ada dalam database
		FS	:Data promo baru telah disimpan dalam database 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_md_promo
				(KodePromo, NamaPromo, LevelPromo, KodeCabangAsal, KodeCabangTujuan, 
				JamMulai, JamAkhir, BerlakuMula, BerlakuAkhir, JumlahPenumpangMinimum, 
				JumlahDiscount, FlagTargetPromo,FlagDiscount,FlagAktif)
			VALUES(
				'$kode_promo', '$nama_promo', '$priority', '$kode_cabang_asal', '$kode_cabang_tujuan', 
				'$mulai_jam', '$akhir_jam', '$tanggal_mulai_promo', '$tanggal_akhir_promo', '$jumlah_minimum',
				'$discount', '$target_discount','$flag_discount','$aktif')";
				
		if (!$db->sql_query($sql)){
			die_error("Gagal");
		}
		
		return true;
	}
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data promo sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":"ORDER BY masa_berlaku_mula desc";
		
		$sql = 
			"SELECT 
				id_promo,layer_promo, kode_jadwal, CONVERT(CHAR(25),masa_berlaku_mula,106) as masa_berlaku_mula, CONVERT(CHAR(25),masa_berlaku_akhir,106) as masa_berlaku_akhir, 
				point, discount_jumlah, discount_persentase,status_aktif, 
				dibuat_oleh, CONVERT(CHAR(25),waktu_dibuat,103) as waktu_dibuat,
				diubah_oleh, CONVERT(CHAR(25),waktu_diubah,103) as waktu_diubah
			FROM $this->TABEL1
			WHERE 
				kode_jadwal LIKE '$pencari%'
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Gagal $this->ID_FILE 003");
		}
		
	}//  END ambilData
	
	function ubah(
		$kode_promo_old,$kode_promo, $nama_promo, $priority, $kode_cabang_asal, $kode_cabang_tujuan, 
		$mulai_jam, $akhir_jam, $tanggal_mulai_promo, $tanggal_akhir_promo, $jumlah_minimum, $flag_discount,
    $discount, $target_discount,$aktif){
	  
		/*
		ID	: 002
		IS	: data promo belum ada dalam database
		FS	:Data promo baru telah disimpan dalam database 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		//$user_level	= $userdata['user_level'];
		//$useraktif	= $userdata['username'];
		
		//if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
		//	return false;
		//}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_md_promo SET
				KodePromo='$kode_promo', NamaPromo='$nama_promo', LevelPromo=' $priority', 
				KodeCabangAsal='$kode_cabang_asal', KodeCabangTujuan='$kode_cabang_tujuan', 
				JamMulai='$mulai_jam', JamAkhir='$akhir_jam', BerlakuMula='$tanggal_mulai_promo', BerlakuAkhir='$tanggal_akhir_promo', 
				JumlahPenumpangMinimum='$jumlah_minimum', 
				JumlahDiscount='$discount', FlagTargetPromo='$target_discount',
				FlagDiscount='$flag_discount',
				FlagAktif='$aktif'
			WHERE KodePromo='$kode_promo_old'";
				
		if (!$db->sql_query($sql)){
			die_error("Gagal");
		}
		
		return true;
	}
	
	function hapus($list_promo){
	  
		/*
		ID	: 005
		IS	: data member sudah ada dalam database
		FS	:Data member dihapus
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//ASPEK KEAMANAN ==========================================================================================
		//if (!in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER))){
			//jika level pengguna tidak balid atau jika id member duplikasi
		//	return 0;
		//}
		//END ASPEK KEAMANAN======================================================================================
		
		//MENGHAPUS DATA DALAM DATABASE
		$sql =
			"DELETE FROM tbl_md_promo
			WHERE KodePromo IN($list_promo);";
								
		if (!$db->sql_query($sql)){
			return 3;
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		
		return 1;
	}//end hapus
	
	function ubahStatus($id_promo){
	  
		/*
		ID	: 006
		IS	: data promo sudah ada dalam database
		FS	: Status promo diubah 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE $this->TABEL1
			SET 
				status_aktif=1-status_aktif,
				diubah_oleh='$useraktif',waktu_diubah={fn NOW()}
			WHERE id_promo='$id_promo';";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Gagal $this->ID_FILE 006");
		}
		
		return true;
	}//end ubahStatus
	
	function ambilDataDetail($id_promo){
		
		/*
		ID	:007
		Desc	:Mengembalikan data promo sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_md_promo
			WHERE KodePromo='$id_promo';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Gagal $this->ID_FILE 007");
		}
		
	}//  END ambilDataDetail
	
	function ubahLayerPromo($id_promo,$layer_promo){
	  
		/*
		ID	: 008
		IS	: data promo sudah ada dalam database
		FS	: layer promo diubah 
		*/
		
		//kamus
		global $userdata;
		global $LEVEL_MANAJER;
		global $db;
		
		//ASPEK KEAMANAN ==========================================================================================
		$user_level	= $userdata['user_level'];
		$useraktif	= $userdata['username'];
		
		if ($user_level>$LEVEL_MANAJER){
			//jika level pengguna tidak balid atau jika id member duplikasi
			return false;
		}
		//END ASPEK KEAMANAN======================================================================================		
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"UPDATE $this->TABEL1
			SET 
				layer_promo=$layer_promo,
				diubah_oleh='$useraktif',waktu_diubah={fn NOW()}
			WHERE id_promo='$id_promo';";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Gagal $this->ID_FILE 008");
		}
		
		return true;
	}//end ubahLayerPromo
	
	function ubahStatusAktif($kode_promo){
	  
		/*
		IS	: data promo sudah ada dalam database
		FS	: Status promo doubah*/
		
		//kamus
		global $db;
		
		$sql ="CALL sp_promo_ubah_status_aktif('$kode_promo');";
		
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end ubahStatus
}
?>