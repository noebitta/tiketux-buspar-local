<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSMSGateway.php');
$time = date('Y-m-d H:i:s');

global $db;

$payment_code = $HTTP_POST_VARS['payment_code'];

if($payment_code == ""){
    die("Payment Code Null");
}

$sql = "SELECT * FROM tbl_reservasi WHERE PaymentCode = '$payment_code'";

$data_reservasi = $db->sql_fetchrow($db->sql_query($sql));
if($payment_code != $data_reservasi['PaymentCode']){
    die("Kode Payment Invalid");
}

$kode_booking = $data_reservasi['KodeBooking'];

$update = "UPDATE tbl_posisi_detail SET StatusBayar = 1 WHERE KodeBooking = '$kode_booking'";

if(!$db->sql_query($update)){
    die("error update tbl_posisi_detail");
}

$update_res = "UPDATE tbl_reservasi SET  JenisPembayaran = 7, PetugasCetakTiket = 0, CetakTiket = 1, WaktuCetakTiket = '$time' WHERE KodeBooking = '$kode_booking'";
if(!$db->sql_query($update_res)){
    die("error update tbl_reservasi");
}

//KIRIM TOKEN ke Pelanggan
$SMSGateway = new SMSGateway();
$isi_pesan  = 'DAYTRANS : Terima Kasih Tn/Ny '. $data_reservasi['Nama'] .', Kode OTP : '. $data_reservasi['OTP'] .' , Gunakan utk mendapatkan tiket. Kode Booking '.$kode_booking;
$SMSGateway->sendSMS($data_reservasi['Telp'],$isi_pesan);

echo "success";

?>