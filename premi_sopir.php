<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$id_page = 999;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

$Sopir	= new Sopir();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];


$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:"1-".date('m-Y');
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}

$kondisi	=($cari=="")?"":
    " WHERE KodeSopir LIKE '%$cari%' 
				OR tbl_md_sopir.Nama LIKE '%$cari%'
				OR tbl_md_sopir.Alamat LIKE '%$cari%'
				OR HP LIKE '%$cari%'
				OR NoSIM LIKE '%$cari%'";


// FILE BODY
$template->set_filenames(array('body' => 'premi_sopir_body.tpl'));

/* mengambil jumlah jalan
meski dalam 1 hari hanya berangkat 1 kali, dianggap hadir
*/

$sql = "SELECT IF(COUNT(KodeDriver) > 0, 1, 0) AS PREMI, KodeDriver, DATE(TglBerangkat) AS TglBerangkat
        FROM tbl_spj 
        WHERE DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
        GROUP BY KodeDriver, TglBerangkat";
if($result = $db->sql_query($sql)){
    while ($row = $db->sql_fetchrow($result)){
        $list_spj[$row['KodeDriver']][$row['TglBerangkat']] = $row['PREMI'];
    }
}else{
    die("Error :".__LINE__);
}

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"KodeSopir","tbl_md_sopir","&cari=$cari",$kondisi,"premi_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql = "SELECT *, f_cabang_get_name_by_kode(KodeCabang)  AS Cabang
        FROM tbl_md_sopir
		$kondisi
		ORDER BY tbl_md_sopir.Nama LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if ($result = $db->sql_query($sql)){
    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
            $odd = 'even';
        }

        if($row['IsReguler'] == 1){
            $status = "REGULER";
        }else{
            $status = "BACKUP";
        }

        $kerja = 0;
        if($list_spj[$row['KodeSopir']] != ""){
            foreach ($list_spj[$row['KodeSopir']] as $item => $value){
                $kerja += $value;
            }
        }

        if($kerja >= 20){
            $odd = "green";
        }

        $parameter_cetak	= "&kode_sopir=".$row['KodeSopir']."&nama=".$row['Nama']."&hari=".$kerja."&tanggal_mulai=".$tanggal_mulai_mysql."&tanggal_akhir=".$tanggal_akhir_mysql;
        $detail = "<a href='".append_sid('premi_sopir_detail.'.$phpEx.'?sid='.$userdata['session_id'].$parameter_cetak)."'>Detail Kehadiran</a>";


        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'           =>$odd,
                'no'            =>$i,
                'nama'          =>$row['Nama'],
                'kode_sopir'    =>$row['KodeSopir'],
                'cabang'        =>$row['Cabang'],
                'status'        =>$status,
                'hari_kerja'    =>$kerja,
                'detail'        =>$detail
            )
        );

        $i++;
    }

    if($i-1<=0){
        $no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
    }
}
else{
    echo("Error :".__LINE__);exit;
}

$template->assign_vars(array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('premi_sopir.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'TGL_AWAL'      => $tanggal_mulai,
        'TGL_AKHIR'     => $tanggal_akhir,
        'NO_DATA'			  => $no_data,
        'PAGING'			  => $paging,
    )
);


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');