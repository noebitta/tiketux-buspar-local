<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPayoutCGS.php');

// SESSION
$id_page = 408.1;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

//INIT
$mode   = isset($HTTP_POST_VARS['mode'])?$HTTP_POST_VARS['mode'] : $HTTP_GET_VARS['mode'];
$kode_sopir     = isset($HTTP_POST_VARS['kodesopir'])?$HTTP_POST_VARS['kodesopir']:$HTTP_GET_VARS['kodesopir'];
$nama           = isset($HTTP_POST_VARS['nama'])?$HTTP_POST_VARS['nama']:$HTTP_GET_VARS['nama'];

//ROUTER
switch($mode){
  case 0:
    pilihJenisPayment($kode_sopir,$nama);

    break;

  case 1:
    $siklus      = isset($HTTP_POST_VARS['siklus'])?$HTTP_POST_VARS['siklus'] : $HTTP_GET_VARS['siklus'];
    $bulan       = isset($HTTP_POST_VARS['bulan'])?$HTTP_POST_VARS['bulan']:$HTTP_GET_VARS['bulan'];
    $tahun       = isset($HTTP_POST_VARS['tahun'])?$HTTP_POST_VARS['tahun']:$HTTP_GET_VARS['tahun'];
    $jenis_bayar = isset($HTTP_POST_VARS['jenisbayar'])?$HTTP_POST_VARS['jenisbayar']:$HTTP_GET_VARS['jenisbayar'];

    echo(prosesPayout($kode_sopir,$siklus,$bulan,$tahun,$jenis_bayar));

    break;

  case 2:
    $id = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];

    cetakResiPayout($id);

    break;
}

//METHODES & PROCESS

function pilihJenisPayment($kode_sopir,$nama){
  global $template;

  $template->set_filenames(array('body' =>'keuangan.payoutcgs/payout.pilihbayar.tpl'));

  $template->assign_vars(array(
    "KODE_SOPIR"    => $kode_sopir,
    "NAMA_SOPIR"    => $nama
  ));

  $template->pparse('body');
}

function prosesPayout($kode_sopir,$siklus,$bulan,$tahun,$jenis_bayar){
  global $userdata;

  $PayoutCGS  = new PayoutCGS();

  //PERIKSA APAKAH PERNAH DI PAYOUT
  $data_payout  = $PayoutCGS->hitungTotalCGS($siklus,$bulan,$tahun,$kode_sopir);

  if($data_payout["IdPayout"]==""){
    //BELUM PERNAH PAYOUT, SILAHKAN LANJUT
    $id=$PayoutCGS->payout($kode_sopir,$siklus,$bulan,$tahun,$jenis_bayar,$data_payout["TotalCGS"],$data_payout["TotalTrip"],$data_payout["Jurusan"],$userdata["user_id"],$userdata["nama"]);

    $return = array("status" => "OK","id"=>$id);
  }
  else{
    $return = array("status" => "GAGAL","error"=>"sudah pernah dilakukan payout");
  }


  return json_encode($return);
}

function cetakResiPayout($id){
  global $template;

  $PayoutCGS  = new PayoutCGS();

  $data_payout  = $PayoutCGS->getDataPayoutCGS($id);

  $arr_jurusan = explode(",", $data_payout["Jurusan"]);

  $arr_count_jurusan = array_count_values($arr_jurusan);

  $list_jurusan="";

  foreach ($arr_count_jurusan as $key => $value) {
    $list_jurusan .= $key . ":" . $value . "<br>";
  }

  $list_jurusan = substr($list_jurusan, 0, -2);

  $template->set_filenames(array('body' =>'keuangan.payoutcgs/payout.resi.tpl'));

  $template->assign_vars(array(
    "NO_RESI"           => $data_payout["KodeReferensi"],
    "KODE_SOPIR"        => $data_payout["KodeSopir"],
    "NAMA_SOPIR"        => $data_payout["NamaSopir"],
    "JENIS_PEMBAYARAN"  => ($data_payout["PayoutVia"]==0?"TUNAI":"TRANSFER"),
    "TOTAL_TRIP"        => number_format($data_payout["TotalTrip"],0,",","."),
    "TOTAL_CGS"         => number_format($data_payout["JumlahPayout"],0,",","."),
    "JURUSAN"           => $list_jurusan,
    "NAMA_PETUGAS"      => $data_payout["NamaPetugas"],
    "WAKTU_PAYOUT"      => dateparseWithTime(FormatMySQLDateToTglWithTime($data_payout["WaktuPayout"])),
  ));

  $template->pparse('body');
}


?>
