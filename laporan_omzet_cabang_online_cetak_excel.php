<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
//SESSION
$id_page = 504;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
elseif(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"]))){
    die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>","Silahkan klik <a href='".append_sid("main.".$phpEx)."'>disini</a> untuk kembali");
}
//################################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$type  			= isset($HTTP_GET_VARS['berdasarkan'])? $HTTP_GET_VARS['berdasarkan'] : $HTTP_POST_VARS['berdasarkan'];

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$kondisi_cari2	=($cari=="")?"WHERE 1 ":
    " WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

//kondisi_cari update
$kondisi_cari   =($cari=="")?"IS NOT NULL ":
    " IN (SELECT KodeCabang
                FROM tbl_md_cabang
                WHERE (KodeCabang LIKE '$cari%'
                OR Nama LIKE '%$cari%'
                OR Kota LIKE '%$cari%'
                OR Telp LIKE '$cari%'
                OR Alamat LIKE '%$cari%'))";
switch($type){
    case 1 :
        $date_filter = 'TglBerangkat';
        break;
    case 2 :
        $date_filter = 'WaktuCetakTiket';
        break;
    default :
        $date_filter = 'WaktuCetakTiket';
        break;
}

//MENGAMBIL DATA-DATA MASTER CABANG
$sql_cabang=
    "SELECT
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari2";

//echo $sql_cabang;
if (!$result_laporan = $db->sql_query($sql_cabang)){
    echo("Err:".__LINE__);exit;
}

$sql=
    "SELECT
          f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
          (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
          -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
          +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
        FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
          AND (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
        WHERE
                f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan)
		$kondisi_cari
        GROUP BY KodeCabang
        ORDER BY KodeCabang";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $data_jadwal_tersedia[$row['KodeCabang']]	= $row['JadwalDibuka'];
}

//DATA PENJUALAN TIKET
$sql	=
    "SELECT
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(IF(PetugasPenjual=0 AND FlagBatal!=1,NoTiket,NULL)),0) AS TotalPenumpangO,
		IS_NULL(SUM(IF(PetugasPenjual=0 AND FlagBatal!=1,Total,0)),0) AS TotalPenjualanTiket
	FROM tbl_reservasi
	WHERE (DATE($date_filter) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND CetakTiket = 1
		$kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $data_tiket_total[$row['KodeCabang']]	= $row;
}
//isi array temp laporan

$temp_array=array();

$idx=0;

$total_m	= 0;
$total_r	= 0;
$total_pp   = 0;
$total_ppcc = 0;
$total_pppcc = 0;
$total_t 	= 0;
$total_s 	= 0;
$total_p 	= 0;
while ($row = $db->sql_fetchrow($result_laporan)){

    $temp_array[$idx]['KodeCabang']				= $row['KodeCabang'];
    $temp_array[$idx]['Nama']					= $row['Nama'];
    $temp_array[$idx]['Alamat']					= $row['Alamat'];
    $temp_array[$idx]['Kota']					= $row['Kota'];
    $temp_array[$idx]['Telp']					= $row['Telp'];
    $temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
    $temp_array[$idx]['PenjualanPenumpangM']	= $data_tiket_total[$row['KodeCabang']]['PenjualanPenumpangM'];
    $temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
    $temp_array[$idx]['PenjualanPenumpangR']	= $data_tiket_total[$row['KodeCabang']]['PenjualanPenumpangR'];
    $temp_array[$idx]['TotalPenumpangPP']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangPP'];
    $temp_array[$idx]['PenjualanPenumpangPP']	= $data_tiket_total[$row['KodeCabang']]['PenjualanPenumpangPP'];
    $temp_array[$idx]['TotalPenumpangPPCC']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangPPCC'];
    $temp_array[$idx]['TotalTiket']				= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
    $temp_array[$idx]['TotalPenjualanTiket']	= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
    $temp_array[$idx]['TotalPenjualanTiketPPCC']= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanPPCC'];
    $temp_array[$idx]['Komisi']					= ($data_tiket_total[$row['KodeCabang']]['TotalPenumpangO']*5000);
    $temp_array[$idx]['KomisiPPCC']				= ($data_tiket_total[$row['KodeCabang']]['TotalPenumpangPPCC']*2500);

    $total_t	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
    $total_p	+= ($data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket']);
    $total_s	+= ($data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'])-(($data_tiket_total[$row['KodeCabang']]['TotalPenumpangO']*5000));

    $idx++;
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);

//judul
$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Cabang Online dari'.$tanggal_mulai." sampai dengan ".$tanggal_akhir);
//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Kode Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Total Penjualan Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Setor');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);


//BODY
$idx_row = 4;
$i = 0;
while($i < count($temp_array)){

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $temp_array[$i]['Nama']);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$i]['KodeCabang']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$i]['TotalTiket']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$i]['TotalPenjualanTiket']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$i]['TotalPenjualanTiket'] - $temp_array[$i]['Komisi']);
    $idx_row ++;
    $i ++;
}

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':B'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Jumlah');
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $total_t);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $total_p);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $total_s);

if ($idx_row>0){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Omzet Cabang Online dari '.$tanggal_mulai." sampai ".$tanggal_akhir);
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
?>