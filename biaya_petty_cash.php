<?php
//STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');

// SESSION
$id_page = 222;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$idbiaya        = isset($HTTP_GET_VARS['idbiaya'])? $HTTP_GET_VARS['idbiaya'] : $HTTP_POST_VARS['idbiaya']; // ubah status jika tidak kosong
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$tgl            = date('Y-m-d H:i:s');

//INISIALISASI
if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["cari"];
}
$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
    " WHERE (tbl_biaya_tambahan.KodeCabang LIKE '$cari%'
		OR NamaPencetak LIKE '%$cari%'
		OR NamaPenerima LIKE '%$cari%'
		OR NamaPembuat LIKE '$cari%'
		OR JenisBiaya LIKE '$cari%'
		OR Keterangan LIKE '$cari%'
		OR Jumlah LIKE '$cari%'
		OR NamaReleaser LIKE '%$cari%')";

// mengambil saldo cabang
$sql = "SELECT Nama,SaldoPettyCash FROM tbl_md_cabang WHERE KodeCabang = '$userdata[KodeCabang]'";
if (!$result = $db->sql_query($sql)){
    die(mysql_error());
}else{
    $kas = $db->sql_fetchrow($result);
}

function setComboJenisBiaya(){
    //SET COMBO MOBIL
    global $db;

    $sql = "SElECT * FROM tbl_biaya_coa WHERE FlagJenis=2";
    $opt_biaya="<option value=''>- silahkan pilih jenis biaya  -</option>";
    if (!$result = $db->sql_query($sql)){
        die(mysql_error());
    }
    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $opt_biaya .="<option value='$row[JenisBiaya]'>$row[JenisBiaya]</option>";
        }
    }
    else{
        echo("Err :".__LINE__);exit;
    }

    return $opt_biaya;
    //END SET COMBO MOBIL
}

// seleksi mode
if($mode == 'InsertData'){
    $jenisbiaya = $HTTP_GET_VARS['jenis'];
    $jumlah     = $HTTP_GET_VARS['jumlah'];
    $penerima   = $HTTP_GET_VARS['penerima'];
    $keterangan = $HTTP_GET_VARS['keterangan'];
    //simpan data ke database
    $sql = "INSERT INTO tbl_biaya_petty_cash (KodeCabang, JenisBiaya, TglBuat, IdPembuat, NamaPembuat, NamaPenerima, Keterangan, Jumlah)
            VALUES ('$userdata[KodeCabang]','$jenisbiaya',NOW(),'$userdata[user_id]','$userdata[nama]','$penerima','$keterangan','$jumlah')";
    global $db;

    $db->sql_query($sql)or die(mysql_error());

    echo("alert('Petty Cash berhasil dibuat!');window.location.reload();");
    exit;
}
else if($mode == 'release'){
    $petugas_release = $userdata['user_id'];
    $nama_petugas    = $userdata['nama'];
    $sql ="UPDATE tbl_biaya_petty_cash SET IdReleaser = $petugas_release, NamaReleaser = '$nama_petugas', TglReleas = '$tgl' WHERE IdBiayaPettyCash = $idbiaya";
    if (!$db->sql_query($sql)){
        die(mysql_error());
    }
}

$saldo      ="<font size='3'><b> SALDO CABANG ".$kas['Nama']." Rp.".number_format($kas['SaldoPettyCash'],0,',','.')."</b></font>";
//cek siapa yang login
if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"]))){
    $kondisi    = "AND IdPembuat = '$userdata[user_id]'AND tbl_biaya_petty_cash.KodeCabang = '$userdata[KodeCabang]'";
}
$kondisi_cari .=$kondisi;
// QUERY AMBIL BIAYA PETTY CASH
$sql = "SELECT *
        FROM tbl_biaya_petty_cash
        JOIN tbl_md_cabang ON tbl_biaya_petty_cash.KodeCabang = tbl_md_cabang.KodeCabang
        $kondisi_cari AND DATE(TglBuat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'
        ORDER BY TglBuat DESC";

if ($result = $db->sql_query($sql)){

    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){

        $tglcetak = "";
        $link   = "";
        $onclick= "";
        $icon   = "";
        $target= "";

        if($row['TglReleas'] == null){
            $status = "<font color='red'><b>Unreleased</b></font>";
            $icon  = "b_drop.png";
            if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["KASIR"],$USER_LEVEL_INDEX["SPV_RESERVASI"]))){
                $link   = append_sid('biaya_petty_cash.'.$phpEx.'?mode=release&idbiaya='.$row['IdBiayaPettyCash']);
                $target= "_self";
            }
        }else{
            if($row['TglCetak'] == null){
                $status = "<font color='green'><b>Released</b></font>";
                $onclick = "Cetak(".$row['IdBiayaPettyCash'].",".$row['Jumlah'].")";
                $icon   = "b_print.png";
                $target= "_self";
            }
            else{
                $status = "<font color='green'><b>Released</b></font>";
                $icon   = "b_okay.png";
            }
        }

        if($row['TglCetak'] != null){
            $tglcetak = date_format(date_create($row['TglCetak']),'d-m-Y H:i:s');
        }
        if($row['TopUP'] != 0){
            $baris = 'blue';
        }else{
            $baris = 'even';
        }
        $template->
        assign_block_vars(
            'ROW',
            array(
                'baris'     =>$baris,
                'No'        =>$i,
                'KodeCabang'=>$row['Nama'],
                'Pembuat'   =>$row['NamaPembuat'],
                'Releaser'  =>$row['NamaReleaser'],
                'Penerima'  =>$row['NamaPenerima'],
                'Pencetak'  =>$row['NamaPencetak'],
                'JenisBiaya'=>$row['JenisBiaya'],
                'Keterangan'=>$row['Keterangan'],
                'Jml'       =>number_format($row['Jumlah'],0,',','.'),
                'TglBuat'   =>date_format(date_create($row['TglBuat']),'d-m-Y H:i:s'),
                'TglCetak'  =>$tglcetak,
                'Status'    =>$status,
                'Link'      =>$link,
                'Icon'      =>$icon,
                'Onclik'   =>$onclick,
                'target'    =>$target
            )
        );
        $i++;
    }
}else{
    //echo("Err:".__LINE__);exit;
    die(mysql_error());
}


$page_title = "Petty Cash";
$template->assign_vars(array(
        'BCRUMP'    	=>setBcrump($id_page),
        'URL'			    => append_sid('biaya_petty_cash.'.$phpEx),
        'BIAYA'         => setComboJenisBiaya(),
        'SALDO'         => $saldo,
        'KAS'           => $kas['SaldoPettyCash'],
        'TGL_AWAL'	    => $tanggal_mulai,
        'TGL_AKHIR'		=> $tanggal_akhir,
        'TXT_CARI'		=> $cari,
    )
);
$template->set_filenames(array('body' => 'biaya_petty_cash_body.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>