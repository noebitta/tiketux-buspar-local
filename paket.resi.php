<?php
//HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 223;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassStokKardus.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'phpqrcode/BarcodeQR.php');

// PARAMETER
$no_resi 			= getVariabel('noresidipilih');
$otp 	        = getVariabel('otp');

//INIT

//CREATE INSTANCE
$Paket      = new Paket();
$StokKardus = new StokKardus();
$Pengaturan = new PengaturanUmum();
$qr         = new BarcodeQR();

//PROSES
$data_paket = $Paket->ambilDetailDataPaket($no_resi);
$data_kardus= $StokKardus->getDetailDataKardus($data_paket["JenisKardus"]);

$qrcode = "phpqrcode/imageQR/".$no_resi.".png";

if(!file_exists ($qrcode)) {
  $qr->draw(100, $qrcode);
}

if($data_paket["CetakTiket"]==2){
  //PERIKSA OTORISASI UNTUK CETAK ULANG
  if(!$Permission->isPermitted($userdata["user_level"],$id_page.".1") && !$Paket->verifyOTPCetakUlang($data_paket["NoResi"],$otp)) {
    //JIKA TIDAK MEMILIKI OTORISASI, AKAN DITOLAK
    echo("Anda tidak memiliki otoritas untuk mencetak ulang resi ini.");exit;
  }

  $template->assign_block_vars("IS_COPY",array());
  $petugas      = $userdata["nama"];
  $waktu_cetak  = dateparseWithTime(date("d-m-Y H:i:s"));
}
else{
  $petugas      = $data_paket["NamaPetugas"];
  $waktu_cetak  = dateparseWithTime(FormatMySQLDateToTglWithTime($data_paket["WaktuCetakTiket"]));
}

$Paket->updateCetakTiket($no_resi);

$template->set_filenames(array('body' => 'paket/resi.tpl'));

if($data_paket["IsPaketTransit"]){
  $template->assign_block_vars("TRANSIT",array());
}

$data_keterangan  = $Pengaturan->ambilDataPerusahaanKeteranganTiket();

$template->assign_vars(array(
  "TGL_BERANGKAT"     => dateparse(FormatMySQLDateToTgl($data_paket["TglBerangkat"])),
  "QR_CODE"           => $qrcode,
  "NO_RESI"           => $data_paket["NoResi"],
  "ASAL"              => substr($data_paket["Asal"],0,20),
  "TUJUAN"            => substr($data_paket["Tujuan"],0,20),
  "JAM_BERANGKAT"     => $data_paket["JamBerangkat"],
  "KODE_JADWAL"       => $data_paket["KodeJadwal"],
  "TELP_PENGIRIM"     => $data_paket["TelpPengirim"],
  "NAMA_PENGIRIM"     => $data_paket["NamaPengirim"],
  "ALAMAT_PENGIRIM"   => $data_paket["AlamatPengirim"],
  "IS_DIJEMPUT"       => $data_paket["IsDijemput"]?"YA":"TIDAK",
  "TELP_PENERIMA"     => $data_paket["TelpPenerima"],
  "NAMA_PENERIMA"     => $data_paket["NamaPenerima"],
  "ALAMAT_PENERIMA"   => $data_paket["AlamatPenerima"],
  "IS_DIANTAR"        => $data_paket["IsDiantar"]?"YA":"TIDAK",
  "BERAT"             => $data_paket["Berat"],
  "LAYANAN"           => $config["layanan_paket"][$data_paket["Layanan"]],
  "JENIS_KARDUS"      => ($data_kardus["NamaJenisKardus"]!=""?$data_kardus["NamaJenisKardus"]:"-tanpa kardus-"),
  "KETERANGAN"        => $data_paket["KeteranganPaket"],
  "KET_DISCOUNT"      => ($data_paket["JenisDiscount"]==""?"-tanpa diskon-":$data_paket["JenisDiscount"]),
  "HARGA"             => number_format($data_paket["HargaSebelumDiscount"],0,",","."),
  "DISCOUNT"          => number_format($data_paket["Discount"],0,",","."),
  "CHARGE_JEMPUT"     => number_format($data_paket["ChargeJemput"],0,",","."),
  "CHARGE_ANTAR"      => number_format($data_paket["ChargeAntar"],0,",","."),
  "TOTAL_BAYAR"       => number_format($data_paket["HargaPaket"]+$data_paket["ChargeJemput"]+$data_paket["ChargeAntar"],0,",","."),
  "WAKTU_TRANSAKSI"   => $waktu_cetak,
  "PETUGAS"           => $petugas,
  "NAMA_PERUSAHAAN"   => $data_keterangan["PERUSAHAAN_NAMA"],
  "ALAMAT_PERUSAHAAN" => $data_keterangan["PERUSAHAAN_ALAMAT"],
  "TELP_PERUSAHAAN"   => $data_keterangan["PERUSAHAAN_TELP"],
  "WEBSITE"           => $data_keterangan["PERUSAHAAN_WEBSITE"],
  "KETERANGAN_RESI"   => $data_keterangan["TIKET_CATATAN"],
));

$template->pparse('body');
	
?>