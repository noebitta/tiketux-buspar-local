<?php
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');


// SESSION
$id_page = 313;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);


// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

$bulan = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$bulan = ($bulan != "")?$bulan:date('m');
$tahun = ($tahun != "")?$tahun:date('Y');

//INISIALISASI VARIABLE

$list_bulan         = "";
$laporan_dt         = array();
$laporan_sum_dt     = array();
$gt_jakarta_dt      = 0;
$gt_bandung_dt      = 0;

$laporan_wh         = array();
$laporan_sum_wh     = array();
$gt_jakarta_wh      = 0;
$gt_bandung_wh      = 0;

$trip_pasteur_dt    = array();
$trip_pasteur_wh    = array();
$sum_trip_pasteur_dt= array();
$sum_trip_pasteur_wh= array();
//LIST BULAN
for($idx_bln=1;$idx_bln<=12;$idx_bln++){

    $font_size	= 2;
    $font_color='';

    if($bulan==$idx_bln){
        $font_size=4;
        $font_color='008609';
    }

    $list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";

}

// mengambil total trip pertanggal dan perjurusan mobil daytrans
$sql_dt = "SELECT
                DATE(TglBerangkat) AS Tgl,
                IdJurusan,
                f_cabang_get_name_by_kode (
                    f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
                ) ASAL,
                f_cabang_get_name_by_kode (
                    f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)
                ) TUJUAN,
                COUNT(NoSPJ) AS TRIP
            FROM
                tbl_spj
            JOIN tbl_md_kendaraan
               ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
            WHERE
                MONTH (TglBerangkat) = $bulan
            AND YEAR (TglBerangkat) = $tahun
            AND tbl_md_kendaraan.IsWH != 1
            GROUP BY
                TglBerangkat, IdJurusan";

if(!$result_dt = $db->sql_query($sql_dt)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_dt)){
    $laporan_dt[$row['Tgl']][$row['IdJurusan']] = $row['TRIP'];
}

// mengambil total trip perjurusan mobil daytrans
$sql_sum_dt = "SELECT
                    IdJurusan,
                    f_cabang_get_name_by_kode (
                        f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
                    ) ASAL,
                    f_cabang_get_name_by_kode (
                        f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)
                    ) TUJUAN,
                    COUNT(NoSPJ) AS TRIP
                FROM
                    tbl_spj
                JOIN tbl_md_kendaraan
                   ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
                WHERE
                    MONTH (TglBerangkat) = $bulan
                AND YEAR (TglBerangkat) = $tahun
                AND tbl_md_kendaraan.IsWH != 1
                GROUP BY
                    IdJurusan";

if(!$result_sum_dt = $db->sql_query($sql_sum_dt)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_sum_dt)){
    $laporan_sum_dt[$row['IdJurusan']] = $row['TRIP'];
}

//================================================================

// mengambil total trip pertanggal dan perjurusan mobil wh
$sql_wh = "SELECT
            DATE(TglBerangkat) AS Tgl,
            IdJurusan,
            f_cabang_get_name_by_kode (
                f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
            ) Cabang,
            COUNT(NoSPJ) AS TRIP
        FROM
            tbl_spj
        JOIN tbl_md_kendaraan
           ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
        WHERE
            MONTH (TglBerangkat) = $bulan
        AND YEAR (TglBerangkat) = $tahun
        AND tbl_md_kendaraan.IsWH != 0
        GROUP BY
            TglBerangkat, IdJurusan";

if(!$result_wh = $db->sql_query($sql_wh)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_wh)){
    $laporan_wh[$row['Tgl']][$row['IdJurusan']] = $row['TRIP'];
}

// mengambil total trip perjurusan mobil wh

$sql_sum_wh = "SELECT
                    IdJurusan,
                    f_cabang_get_name_by_kode (
                        f_jurusan_get_kode_cabang_asal_by_jurusan (IdJurusan)
                    ) Cabang,
                    COUNT(NoSPJ) AS TRIP
                FROM
                    tbl_spj
                JOIN tbl_md_kendaraan
                   ON tbl_md_kendaraan.KodeKendaraan = tbl_spj.NoPolisi
                WHERE
                    MONTH (TglBerangkat) = $bulan
                AND YEAR (TglBerangkat) = $tahun
                AND tbl_md_kendaraan.IsWH != 0
                GROUP BY
                    IdJurusan";

if(!$result_sum_wh = $db->sql_query($sql_sum_wh)){
    die("Error : ".__LINE__);
}

while($row = $db->sql_fetchrow($result_sum_wh)){
    $laporan_sum_wh[$row['IdJurusan']] = $row['TRIP'];
}
//===============================================================


// list jurusan jakarta ke bandung
$sql_cabang_jakarta = 'SELECT
                            IdJurusan,
                            KodeJurusan,
                            f_cabang_get_name_by_kode (KodeCabangAsal) AS Nama,
                            f_cabang_get_name_by_kode (KodeCabangTujuan) AS Tujuan
                        FROM
                            tbl_md_jurusan j
                        WHERE
                            (
                                SELECT
                                    Kota
                                FROM
                                    tbl_md_cabang
                                WHERE
                                    KodeCabang = KodeCabangAsal
                            ) = "JAKARTA" 
                        ORDER BY
                            IdJurusan';

if(!$list_cabang_jakarta = $db->sql_query($sql_cabang_jakarta)){
    die("Error : ".__LINE__);
}

// looping nama jurusan untuk header tabel, looping total_trip perjurusan
while($cabang_jakarta = $db->sql_fetchrow($list_cabang_jakarta)){

    $gt_jakarta_dt += $laporan_sum_dt[$cabang_jakarta['IdJurusan']];
    $gt_jakarta_wh += $laporan_sum_wh[$cabang_jakarta['IdJurusan']];

    $template->
    assign_block_vars(
        'Cabang',
        array(
            'nama'      => $cabang_jakarta['KodeJurusan'],
            'sum_jkt_dt'=> $laporan_sum_dt[$cabang_jakarta['IdJurusan']],
            'sum_jkt_wh'=> $laporan_sum_wh[$cabang_jakarta['IdJurusan']]
        )
    );
}

//=====================================================================


// list jurusan bandung jakarta
$sql_cabang_bandung = 'SELECT
                            IdJurusan AS IdJurusanJakarta,
                            KodeJurusan,
                            f_cabang_get_name_by_kode (KodeCabangAsal) AS DariJakarta,
                            f_cabang_get_name_by_kode (KodeCabangTujuan) AS KeBandung,
                            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanBandung,
                            (SELECT KodeJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KodeJurusanBandung,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS DariBandung,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakarta,
                            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanPasteur,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS DariPasteur,
                            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakartaDariPasteur
                            
                        FROM
                            tbl_md_jurusan j
                        WHERE
                            (
                                SELECT
                                    Kota
                                FROM
                                    tbl_md_cabang
                                WHERE
                                    KodeCabang = KodeCabangAsal
                            ) = "JAKARTA"
                        ORDER BY
                            IdJurusan';
if(!$list_cabang_bandung = $db->sql_query($sql_cabang_bandung)){
    die("Error : ".__LINE__);
}
// looping nama jurusan untuk header tabel, looping total_trip perjurusan
while($cabang_bandung = $db->sql_fetchrow($list_cabang_bandung)){

    $jml_dt = "";
    $jml_wh = "";

    if($cabang_bandung['IdJurusanBandung'] != $cabang_bandung['IdJurusanPasteur']){

        $gt_bandung_dt += $laporan_sum_dt[$cabang_bandung['IdJurusanBandung']] + $laporan_sum_dt[$cabang_bandung['IdJurusanPasteur']];
        $gt_bandung_wh += $laporan_sum_wh[$cabang_bandung['IdJurusanBandung']] + $laporan_sum_wh[$cabang_bandung['IdJurusanPasteur']];

        $jml_dt = $laporan_sum_dt[$cabang_bandung['IdJurusanBandung']] + $laporan_sum_dt[$cabang_bandung['IdJurusanPasteur']];
        $jml_wh = $laporan_sum_wh[$cabang_bandung['IdJurusanBandung']] + $laporan_sum_wh[$cabang_bandung['IdJurusanPasteur']];

    }else{
        $gt_bandung_dt += $laporan_sum_dt[$cabang_bandung['IdJurusanBandung']];
        $gt_bandung_wh += $laporan_sum_wh[$cabang_bandung['IdJurusanBandung']];

        $jml_dt = $laporan_sum_dt[$cabang_bandung['IdJurusanBandung']];
        $jml_wh = $laporan_sum_wh[$cabang_bandung['IdJurusanBandung']];
    }

    $jml_dt = ($jml_dt != 0)?$jml_dt:'';
    $jml_wh = ($jml_wh != 0)?$jml_wh:'';

    $template->
    assign_block_vars(
        'Bandung',
        array(
            'nama'      => $cabang_bandung['KodeJurusanBandung'],
            'sum_bdg_dt'=> $jml_dt,
            'sum_bdg_wh'=> $jml_wh
        )
    );
}



// looping tanggal dalam 1 bulan untuk isian tabel
$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;
$idx=0;
$index=0;

while($currentdate <= $end)
{
    $cur_date = date('Y-m-d', $currentdate);

    $trip_jakarta_dt    = "";
    $sum_trip_jakarta_dt= 0;
    $trip_bandung_dt    = "";
    $sum_trip_bandung_dt= 0;

    $trip_jakarta_wh    = "";
    $sum_trip_jakarta_wh= 0;
    $trip_bandung_wh    = "";
    $sum_trip_bandung_wh= 0;


    // looping jurusan jakarta, ambil total trip perjurusan dalam tanggal ini
    if(!$list_cabang_jakarta = $db->sql_query($sql_cabang_jakarta)){
        die("Error : ".__LINE__);
    }

    while($cabang_jakarta = $db->sql_fetchrow($list_cabang_jakarta)){
        $trip_jakarta_dt .= "<td align='center'>".$laporan_dt[$cur_date][$cabang_jakarta['IdJurusan']]."</td>";
        $sum_trip_jakarta_dt += $laporan_dt[$cur_date][$cabang_jakarta['IdJurusan']];

        $trip_jakarta_wh .= "<td align='center'>".$laporan_wh[$cur_date][$cabang_jakarta['IdJurusan']]."</td>";
        $sum_trip_jakarta_wh += $laporan_wh[$cur_date][$cabang_jakarta['IdJurusan']];
    }


    //looping jurusan bandung, ambil total trip perjurusan dalam tanggal ini
    if(!$list_cabang_bandung = $db->sql_query($sql_cabang_bandung)){
        die("Error : ".__LINE__);
    }

    while($cabang_bandung = $db->sql_fetchrow($list_cabang_bandung)){

        if($cabang_bandung['IdJurusanBandung'] != $cabang_bandung['IdJurusanPasteur']){
            $jml_dt = $laporan_dt[$cur_date][$cabang_bandung['IdJurusanBandung']] + $laporan_dt[$cur_date][$cabang_bandung['IdJurusanPasteur']];
            $jml_wh = $laporan_wh[$cur_date][$cabang_bandung['IdJurusanBandung']] + $laporan_wh[$cur_date][$cabang_bandung['IdJurusanPasteur']];
        }else{
            $jml_dt = $laporan_dt[$cur_date][$cabang_bandung['IdJurusanBandung']];
            $jml_wh = $laporan_wh[$cur_date][$cabang_bandung['IdJurusanBandung']];
        }

        $jml_dt = ($jml_dt != 0)?$jml_dt:'';
        $jml_wh = ($jml_wh != 0)?$jml_wh:'';

        $trip_bandung_dt .= "<td align='center'>".$jml_dt."</td>";
        $sum_trip_bandung_dt += $jml_dt;

        $trip_bandung_wh .= "<td align='center'>".$jml_wh."</td>";
        $sum_trip_bandung_wh += $jml_wh;

    }



    $odd = 'odd';

    if (($idx % 2) == 0) {
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'       => $odd,
            'hari'      => translateDay(date_format(date_create($cur_date),'l')),
            'tanggal'   => date_format(date_create($cur_date),'d-m-Y'),
            'trip_jakarta_dt'       => $trip_jakarta_dt,
            'sum_trip_jakarta_dt'   => $sum_trip_jakarta_dt,
            'trip_jakarta_wh'       => $trip_jakarta_wh,
            'sum_trip_jakarta_wh'   => $sum_trip_jakarta_wh,
            'trip_bandung_dt'       => $trip_bandung_dt,
            'sum_trip_bandung_dt'   => $sum_trip_bandung_dt,
            'trip_bandung_wh'       => $trip_bandung_wh,
            'sum_trip_bandung_wh'   => $sum_trip_bandung_wh
        )
    );

    $idx++;
    $currentdate = strtotime('+1 days', $currentdate);

}




$cetak_excel = "Start('laporan_rekap_trip_excel.php?sid=".$userdata['session_id']."&bulan=".$bulan."&tahun=".$tahun."');return false;";

$template->assign_vars(
    array(
        'BCRUMP'    		=>setBcrump($id_page),
        'URL'				    => append_sid('laporan_rekap_trip.'.$phpEx),
        'LIST_BULAN'		=> "| ".$list_bulan,
        'BULAN'				  => $bulan,
        'TAHUN'				  => $tahun,
        'CETAK_XL'      => $cetak_excel,
        'GT_JAKARTA_DT' => $gt_jakarta_dt,
        'GT_JAKARTA_WH' => $gt_jakarta_wh,
        'GT_BANDUNG_DT' => $gt_bandung_dt,
        'GT_BANDUNG_WH' => $gt_bandung_wh
    )
);


$template->set_filenames(array('body' => 'laporan_rekap_trip/laporan_rekap_trip.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>