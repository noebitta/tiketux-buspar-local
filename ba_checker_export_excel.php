<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$kondisi =	$cari==""?"":
    " AND (KodeDriver LIKE '%$cari%'
		OR Driver LIKE '%$cari%'
		OR JamBerangkat LIKE '%$cari%'
		OR KodeJurusan LIKE '%$cari%'
		OR NoPolisi LIKE '%$cari%'
		OR NamaChecker LIKE '%$cari%')";

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Berita Acara Checker : per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Jurusan');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Nomor Berita Acara');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Mobil');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Checker');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Penumpang Check');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Paket Check');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Jumlah Liter');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'SPBU');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Petugas Isi');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Catatan');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);


$sql   ="SELECT
                KodeDriver,Driver,
                NoBA,TglBA,KodeJurusan,
                TglBerangkat,JamBerangkat,
                NoPolisi,
                NamaChecker,JumlahPenumpangCheck,JumlahPaketCheck,
                CatatanTambahanCheck,PathFoto
            FROM tbl_ba_check ts
            WHERE (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
            $kondisi
            ORDER BY TglBerangkat,JamBerangkat";

if(!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

$i=1;

$idx=0;

while ($row = $db->sql_fetchrow($result)){
    $idx++;
    $idx_row=$idx+3;

    $sqlBbm	 = "SELECT COUNT(Id) as jml, JumlahLiter, NamaSPBU, TglDicatat, NamaPetugas,Kilometer,IsApprove FROM tbl_voucher_bbm  WHERE NoSPJ = '".$row['NoBA']."'";

    if(!$resBbm = $db->sql_query($sqlBbm)){
        echo("Err:".__LINE__);exit;
    }
    $rwBbm   = $db->sql_fetchrow($resBbm);

    if($rwBbm['jml'] == 0){
        $literbbm = 0;
        $tglbbm = '';
        $petugasbbm = '';
    }else{
        $literbbm = $rwBbm['JumlahLiter'];
        $tglbbm = dateparse(FormatMySQLDateToTgl($rwBbm['TglDicatat']));
        $petugasbbm = $rwBbm['NamaPetugas'];
    }

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat']);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $row['KodeJurusan']);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['TglBA']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $row['NoBA']);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Driver']);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['NoPolisi']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NamaChecker']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($row['JumlahPenumpangCheck'],0,",","."));
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($row['JumlahPaketCheck'],0,",","."));
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($rwBbm['JumlahLiter'],2,'.',','));
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $rwBbm['NamaSPBU']);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $petugasbbm);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $row['CatatanTambahanCheck']);

}
$temp_idx=$idx_row;

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);



if ($idx>0){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Berita Acara Checker: per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir.'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}


?>