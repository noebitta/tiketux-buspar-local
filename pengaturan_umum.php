<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');

// SESSION
$id_page = 704;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$PengaturanUmum	= new PengaturanUmum();


	if ($mode=='save'){
		// aksi penyimpanan 
		$pesan_di_tiket = $HTTP_POST_VARS['pesan_di_tiket'];
		$alamat  				= $HTTP_POST_VARS['alamat'];
		$telp						= $HTTP_POST_VARS['telp'];
		$email					= $HTTP_POST_VARS['email'];
		$website 				= $HTTP_POST_VARS['website'];
		$tanggal_mulai 	= $HTTP_POST_VARS['tanggal_mulai_tuslah'];
		$tanggal_akhir 	= $HTTP_POST_VARS['tanggal_akhir_tuslah'];
		
		$harga_paket_kg_pertama_p 		= $HTTP_POST_VARS['harga_paket_pertama_p'];
		$harga_paket_kg_selanjutnya_p = $HTTP_POST_VARS['harga_paket_selanjutnya_p'];
		$harga_paket_kg_pertama_ga 		= $HTTP_POST_VARS['harga_paket_pertama_ga'];
		$harga_paket_kg_selanjutnya_ga = $HTTP_POST_VARS['harga_paket_selanjutnya_ga'];
		$harga_paket_kg_pertama_gd 		= $HTTP_POST_VARS['harga_paket_pertama_gd'];
		$harga_paket_kg_selanjutnya_gd = $HTTP_POST_VARS['harga_paket_selanjutnya_gd'];
		$harga_paket_kg_pertama_s 		= $HTTP_POST_VARS['harga_paket_pertama_s'];
		$harga_paket_kg_selanjutnya_s = $HTTP_POST_VARS['harga_paket_selanjutnya_s'];
		
		$terjadi_error=false;
		
		$judul="Ubah Data Pengaturan Umum";
		
		if($PengaturanUmum->ubahPengaturanUmum(
				$pesan_di_tiket,$alamat,$telp,$email,
				$website,FormatTglToMySQLDate($tanggal_mulai),FormatTglToMySQLDate($tanggal_akhir),
				$harga_paket_kg_pertama_p,$harga_paket_kg_selanjutnya_p,
				$harga_paket_kg_pertama_ga,$harga_paket_kg_selanjutnya_ga,
				$harga_paket_kg_pertama_gd,$harga_paket_kg_selanjutnya_gd,
				$harga_paket_kg_pertama_s,$harga_paket_kg_selanjutnya_s)){
					
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
					
		}
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";
		
		$template->set_filenames(array('body' => 'pengaturan_umum/add_body.tpl'));

    $template->assign_vars(array(
			 'BCRUMP'		=>setBcrump($id_page),
			 'JUDUL'		=>$judul,
			 'MODE'   	=> 'save',
			 'PESAN_DI_TIKET'	=> $pesan_di_tiket,
			 'ALAMAT'    			=> $alamat,
			 'TELP'    	=> $telp,
			 'EMAIL'   	=> $email,
			 'WEBSITE'	=> $website,
			 'PESAN'		=> $pesan,
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'TGL_MULAI_TUSLAH'	=> $tanggal_mulai,
			 'TGL_AKHIR_TUSLAH'	=> $tanggal_akhir,
			 'PAKET_HARGA_PERTAMA_P'			=> $harga_paket_kg_pertama_p,
			 'PAKET_HARGA_SELANJUTNYA_P'	=> $harga_paket_kg_selanjutnya_p,
			 'PAKET_HARGA_PERTAMA_GA'			=> $harga_paket_kg_pertama_ga,
			 'PAKET_HARGA_SELANJUTNYA_GA'	=> $harga_paket_kg_selanjutnya_ga,
			 'PAKET_HARGA_PERTAMA_GD'			=> $harga_paket_kg_pertama_gd,
			 'PAKET_HARGA_SELANJUTNYA_GD'	=> $harga_paket_kg_selanjutnya_gd,
			 'PAKET_HARGA_PERTAMA_S'			=> $harga_paket_kg_pertama_s,
			 'PAKET_HARGA_SELANJUTNYA_S'	=> $harga_paket_kg_selanjutnya_s,
			 'U_ADD_ACT'=>append_sid('pengaturan_umum.'.$phpEx)
			)
		);
	
	} 
	else{
		// show data
		
		$row=$PengaturanUmum->ambilData();
		
		$tanggal_mulai	= ($row['TglMulaiTuslah']!='' && $row['TglMulaiTuslah']!='0000-00-00')?FormatMySQLDateToTgl($row['TglMulaiTuslah']):dateD_M_Y();
		$tanggal_akhir	= ($row['TglAkhirTuslah']!='' && $row['TglAkhirTuslah']!='0000-00-00')?FormatMySQLDateToTgl($row['TglAkhirTuslah']):dateD_M_Y();
		
		$page_title	= "Pengaturan Umum";
		
		$template->set_filenames(array('body' => 'pengaturan_umum/add_body.tpl'));

    $template->assign_vars(array(
			 'BCRUMP'		      =>setBcrump($id_page),
			 'JUDUL'		      => 'Ubah Data Pengaturan Umum',
			 'MODE'   	      => 'save',
			 'PESAN_DI_TIKET'	=> $row['PesanDiTiket'],
			 'ALAMAT'    			=> $row['AlamatPerusahaan'],
			 'TELP'    	      => $row['TelpPerusahaan'],
			 'EMAIL'   	      => $row['EmailPerusahaan'],
			 'WEBSITE'	      => $row['WebSitePerusahaan'],
			 'TGL_MULAI_TUSLAH'	=> $tanggal_mulai,
			 'TGL_AKHIR_TUSLAH'	=> $tanggal_akhir,
			 'PAKET_HARGA_PERTAMA_P'			=> $harga_paket_kg_pertama['P'],
			 'PAKET_HARGA_SELANJUTNYA_P'	=> $harga_paket_kg_selanjutnya['P'],
			 'PAKET_HARGA_PERTAMA_GA'			=> $harga_paket_kg_pertama['GA'],
			 'PAKET_HARGA_SELANJUTNYA_GA'	=> $harga_paket_kg_selanjutnya['GA'],
			 'PAKET_HARGA_PERTAMA_GD'			=> $harga_paket_kg_pertama['GD'],
			 'PAKET_HARGA_SELANJUTNYA_GD'	=> $harga_paket_kg_selanjutnya['GD'],
			 'PAKET_HARGA_PERTAMA_S'			=> $harga_paket_kg_pertama['S'],
			 'PAKET_HARGA_SELANJUTNYA_S'	=> $harga_paket_kg_selanjutnya['S'],
			 'U_ADD_ACT'=>append_sid('pengaturan_umum.'.$phpEx)
			 )
		);
	}       

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>