<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');

// SESSION
$id_page = 218;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//################################################################################

$mode           = $HTTP_POST_VARS['mode'];
$tiket          = $HTTP_POST_VARS['tiket'];
$kursi          = $HTTP_POST_VARS['kursi'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari           = isset($HTTP_GET_VARS["cari"])? $HTTP_GET_VARS["cari"] : $HTTP_POST_VARS["txt_cari"];

$kondisi = ($cari == "")?"":"AND KodeBooking = '$cari' OR NoTiket = '$cari' OR Telp = '$cari'";

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Reservasi = new Reservasi();

if($mode == "release"){
    $Reservasi->pembatalan($tiket, $kursi, $userdata['user_id']);

    echo(1);

    exit;
}


// TIKET ONLINE YANG SUDAH LEBIH DARI 2 SETENGAH JAM BELUM BATAL OTOMATIS
$sql = "SELECT
            NoTiket,
            KodeJadwal,
            NomorKursi,
            TglBerangkat,
            WaktuPesan,
            Nama,
            NOW() AS SEKARANG,
            TIMEDIFF(NOW(), WaktuPesan) AS SELISIH
        FROM
            tbl_reservasi
        WHERE
            PetugasPenjual = 0
        AND CetakTiket != 1
        AND FlagBatal != 1
        AND TglBerangkat BETWEEN '$tanggal_mulai_mysql'AND '$tanggal_akhir_mysql'
        $kondisi
        HAVING SELISIH > '02:30:00';";

if(!$result = $db->sql_query($sql)){
    echo ("Error : ".__LINE__);exit;
}

$idx=0;
$odd ='odd';
while($row = $db->sql_fetchrow($result)){

    if (($idx % 2)==0){
        $odd = 'even';
    }
    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'=>$odd,
            'no'=>$idx+1,
            'tiket'=>$row['NoTiket'],
            'jadwal'=>$row['KodeJadwal'],
            'tanggal'=>date_format(date_create($row['TglBerangkat']),'d-m-Y'),
            'kursi'=>$row['NomorKursi'],
            'nama'=>$row['Nama'],
            'pesan'=>date_format(date_create($row['WaktuPesan']),'H:i:s d-m-Y'),)
    );

    $idx++;
}


// TIKET YANG DI TBL RESERVASI ADA TAPI DI LAYOUT MOBIL TIDAK ADA

// buat array tabel posisi detail
$sql_posisi = "SELECT * FROM tbl_posisi_detail WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'";
if(!$result_posisi = $db->sql_query($sql_posisi)){
    echo ("Error : ".__LINE__);exit;
}

$posisi = array();
while($row = $db->sql_fetchrow($result_posisi)){
    array_push($posisi,$row['NoTiket']);
}

$sql = "SELECT 
            NoTiket,
            KodeJadwal,
            NomorKursi,
            TglBerangkat,
            WaktuPesan,
            Nama
        FROM tbl_reservasi
        WHERE FlagBatal != 1 
        AND TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'";

if(!$result = $db->sql_query($sql)){
    echo ("Error : ".__LINE__);exit;
}


while($row = $db->sql_fetchrow($result)){

    if (($idx % 2)==0){
        $odd = 'even';
    }

    // tampilkan tiket yang tidak ada di tabel posisi
    if(!in_array($row['NoTiket'],$posisi)){

        $template->
        assign_block_vars(
            'POSISI',
            array(
                'odd'=>$odd,
                'no'=>$idx+1,
                'tiket'=>$row['NoTiket'],
                'jadwal'=>$row['KodeJadwal'],
                'tanggal'=>date_format(date_create($row['TglBerangkat']),'d-m-Y'),
                'kursi'=>$row['NomorKursi'],
                'nama'=>$row['Nama'],
                'pesan'=>date_format(date_create($row['WaktuPesan']),'H:i:s d-m-Y'),)
        );

        $idx++;
    }
}

$template->assign_vars(
    array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('release_kursi.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'TGL_AWAL'			=> $tanggal_mulai,
        'TGL_AKHIR'			=> $tanggal_akhir,
    )
);

$template->set_filenames(array('body' => 'release_kursi_body.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>