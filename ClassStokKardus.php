<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class StokKardus{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function StokKardus(){
		$this->ID_FILE="C-STK";
	}
	
	//BODY
	
	function generateKardus($IdJenisKardus)
	{
		global $db;

		$sql = "select * from tbl_md_cabang";
		
		if ($result = $db->sql_query($sql))
		{
		 	while ($row = $db->sql_fetchrow($result))
		 	{
		 		$sql_generate = 
		 		"insert into tbl_stok_kardus(KodeCabang,JenisKardus,JumlahStok) 
				  values('$row[KodeCabang]','$IdJenisKardus','0')";
				$db->sql_query($sql_generate);
			}
		} 
		else
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
		exit;
	}

	function destroyKardus($IdJenisKardus)
	{
		global $db;

		$sql = "delete from tbl_stok_kardus where JenisKardus = '$IdJenisKardus'";
		
		if (!$result = $db->sql_query($sql))
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
		exit;
	}

	function latest_id()
	{
		global $db;

		$sql = "select IdJenisKardus from tbl_jenis_kardus order by IdJenisKardus desc limit 1";
		
		if (!$result = $db->sql_query($sql))
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
		
		$row=$db->sql_fetchrow($result);

		return $row['IdJenisKardus']+1;
	}

	function generateTambahLog($IdJenisKardus,$TambahStok,$KodeCabang,$StokAsal)
	{

		global $db;

		$query 	= "select IdStokKardus from tbl_stok_kardus where KodeCabang='$KodeCabang' and JenisKardus='$IdJenisKardus'";
		$result = $db->sql_query($query);
		$record = $db->sql_fetchrow($result);

		$IdStokKardus = $record['IdStokKardus'];

		$StokAkhir=$TambahStok+$StokAsal;
		$DetailTransaksi="Tambah stok";
		$sql = "insert into tbl_log_kardus(IdStokKardus,WaktuTransaksi,DetailTransaksi,JumlahTransaksi,Stok,KodeCabang) 
				values('$IdStokKardus',NOW(),'$DetailTransaksi','$TambahStok','$StokAkhir','$KodeCabang')";
		
		if (!$result = $db->sql_query($sql))
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
	}

	function generateOpnameLog($IdStokKardus,$StokOpname,$KodeCabang,$StokAsal,$Keterangan)
	{

		global $db;
		$DetailTransaksi="Stok Opname, stok dari sistem $StokAsal. ".$Keterangan;
		$sql = "insert into tbl_log_kardus(IdStokKardus,WaktuTransaksi,DetailTransaksi,JumlahTransaksi,Stok,KodeCabang) 
				values('$IdStokKardus',NOW(),'$DetailTransaksi','$StokOpname','$StokOpname','$KodeCabang')";
		
		if (!$result = $db->sql_query($sql))
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
	}

	function kurangiStok($id_jenis_kardus,$kode_cabang)
	{
		global $db;
 	
		$sql = "SELECT * FROM tbl_stok_kardus WHERE JenisKardus='$id_jenis_kardus' AND KodeCabang='$kode_cabang'";

		if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

		$stok_data	= $db->sql_fetchrow($result);

		//Cek stok kardus
		if($stok_data['JumlahStok']!='' && $stok_data['JumlahStok']!='0') {
			$sql = "UPDATE tbl_stok_kardus SET JumlahStok = JumlahStok - 1 WHERE IdStokKardus='$stok_data[IdStokKardus]';";

			$db->sql_query("START TRANSACTiON;");
			$db->sql_query($sql);
			$db->sql_query("COMMIT;");

			$stok_akhir = $stok_data["JumlahStok"]-1;

      $detail_transaksi="Mengurangi stok";
			
			$sql_log = "INSERT INTO tbl_log_kardus(IdStokKardus,WaktuTransaksi,DetailTransaksi,JumlahTransaksi,Stok,KodeCabang)
					VALUES('$stok_data[IdStokKardus]',NOW(),'$detail_transaksi','1','$stok_akhir','$kode_cabang')";
			
			if(!$result_log = $db->sql_query($sql_log)){
				echo("Err: $this->ID_FILE ".__LINE__);exit;
			}

			return true;
		}
		else {
			return false;
		}
		
	}

	function getStok($KodeCabang)
	{
		global $db;
		$stok= "";

		$sql = "SELECT tjk.*,tsk.JumlahStok
				FROM tbl_jenis_kardus tjk
				LEFT JOIN tbl_stok_kardus tsk
				ON tjk.IdJenisKardus=tsk.JenisKardus AND tsk.KodeCabang='$KodeCabang';";
		
		if (!$result = $db->sql_query($sql))
		{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
		
		while($row=$db->sql_fetchrow($result))
		{
			$stok .= "<td align='right'>".($row['JumlahStok']==null? "<div style='color:red;'>0</div>":($row['JumlahStok'] <= 5? "<div style='color:red;'>".$row['JumlahStok']."</div>":$row['JumlahStok']))."</td>";
		};

		return $stok;
	}

  function getListJenisKardus(){
    global $db;

    $sql = "SELECT * FROM tbl_jenis_kardus ORDER BY IdJenisKardus";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    return $result;

  }

  function getDetailDataKardus($id_jenis_kardus){
    global $db;

    $sql = "SELECT * FROM tbl_jenis_kardus WHERE IdJenisKardus='$id_jenis_kardus'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }

  function ambilStokKardus($kode_cabang,$jenis_kardus=""){
    global $db;

    if($jenis_kardus==""){
      $sql = "SELECT tjk.IdJenisKardus,tjk.NamaJenisKardus,tsk.JumlahStok AS Stok
				FROM tbl_jenis_kardus tjk
				LEFT JOIN tbl_stok_kardus tsk
				ON tjk.IdJenisKardus=tsk.JenisKardus AND tsk.KodeCabang='$kode_cabang';";

      if (!$result = $db->sql_query($sql)){
        echo("Err:".__LINE__);exit;
      }

      while($row=$db->sql_fetchrow($result)){
        $data_ret[$row["IdJenisKardus"]]["NamaJenisKardus"] = $row["NamaJenisKardus"];
        $data_ret[$row["IdJenisKardus"]]["Stok"]            = ($row["Stok"]!=""?$row["Stok"]:0);
      }

      return $data_ret;
    }
    else{
      $sql = "SELECT tsk.JumlahStok AS Stok
				FROM tbl_jenis_kardus tjk
				LEFT JOIN tbl_stok_kardus tsk
				ON tjk.IdJenisKardus=tsk.JenisKardus AND tsk.KodeCabang='$kode_cabang'
				WHERE tjk.IdJenisKardus=$jenis_kardus;";

      if (!$result = $db->sql_query($sql)){
        echo("Err:".__LINE__);exit;
      }

      $row = $db->sql_fetchrow($result);

      return  $row["Stok"];
    }

  }
}
?>