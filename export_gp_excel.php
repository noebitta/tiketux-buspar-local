<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 407;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_KEUANGAN))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$periode  			= $HTTP_GET_VARS['periode'];
$periode_mysql	= FormatTglToMySQLDate($periode);

$periode_exp		= explode("-",$periode);
$batch					= substr($periode_exp[2],-2).$periode_exp[1].$periode_exp[0];
$tgl_batch			= $periode_exp[1]."/".$periode_exp[0]."/".$periode_exp[2];

$idx=$idx_awal_record;

$objPHPExcel = new PHPExcel();
$objPHPExcel->createSheet(4);


//[ INTEGRASI SALES ]===============================================================================================================================
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("Integrasi Sales");
//HEADER SOP HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'TYPE');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'TYPE_ID');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'DOC_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'DATE');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'CUSTOMER_ID');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'SITE_ID');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'CURRENCY_ID');

//HEADER SOP ITEM
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'DOC_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'ITEM_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'ITEM_DESCRIPTION');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'QUANTITY');
$objPHPExcel->getActiveSheet()->setCellValue('N1', 'UOFM');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'UNIT_PRICE');
$objPHPExcel->getActiveSheet()->setCellValue('P1', 'MARK_DOWN_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'COA_SALES');
$objPHPExcel->getActiveSheet()->setCellValue('R1', 'COA_MARK_DOWN');
$objPHPExcel->getActiveSheet()->setCellValue('S1', 'SITE_ID');
$objPHPExcel->getActiveSheet()->setCellValue('T1', 'PRICE_LEVEL');
$objPHPExcel->getActiveSheet()->setCellValue('U1', 'SEQUENCE_LINE');

//HEADER SOP DISTRIBUTION
$objPHPExcel->getActiveSheet()->setCellValue('W1', 'DOC_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('X1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'KREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'DISTRIBUTION_TYPE');
$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'DISTRIBUTION_REFERENCE');
$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'SEQUENCE_LINE');

//HEADER SOP ANALYTIC & DIMENSION
$objPHPExcel->getActiveSheet()->setCellValue('AE1', 'DOC_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('AF1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('AG1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('AH1', 'KREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('AI1', 'ASSIGN_ID');
$objPHPExcel->getActiveSheet()->setCellValue('AJ1', 'TRX_DIMENSION_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('AK1', 'TRX_DIMENSION');
$objPHPExcel->getActiveSheet()->setCellValue('AL1', 'TRX_DIMENSION_ALPHANUMERIC');
$objPHPExcel->getActiveSheet()->setCellValue('AM1', 'SEQUENCE_LINE');

//SET AUTOSIZE
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);

//[ CASH RECEIPT ]===============================================================================================================================
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle("Cash Receipt");
//HEADER CR HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'RECEIPT_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'DATE');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'CUSTOMER_ID');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'CURRENCY_ID');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'CASH_RECEIPT_TYPE');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'CHECKBOOK_ID');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'COMMENT');

//HEADER CR DISTRIBUTION
$objPHPExcel->getActiveSheet()->setCellValue('K1', 'RECEIPT_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('L1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('M1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('N1', 'DISTRIBUTION_TYPE');
$objPHPExcel->getActiveSheet()->setCellValue('O1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('P1', 'KREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'DISTRIBUTION_REFERENCE');
$objPHPExcel->getActiveSheet()->setCellValue('R1', 'SEQUENCE_LINE');

//SET AUTOSIZE
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);


//*******************************************************************************************************************************
//*******************************************************************************************************************************

//DATA HEADER------------------
$tbl_reservasi	= "tbl_reservasi"; //jaga-jaga ketika diberlakukan tbl_reservasi OLAP

$kondisi	= 
	" WHERE DATE(WaktuCetakTiket)='$periode_mysql' AND CetakTiket=1 AND FlagBatal!=1";
	
//AMBIL DATA TIKET GROUP BY KodeCabang
$sql=
	"SELECT KodeCabang,
		COUNT(1) AS Tiket,IS_NULL(SUM(SubTotal),0) AS TotalPenjualan,
		IS_NULL(SUM(Discount),0) AS TotalDiscount,
		IS_NULL(SUM(IF(JenisPembayaran=0,Total,0)),0) AS TotalBayarTunai,
		IS_NULL(SUM(IF(JenisPembayaran=1,Total,0)),0) AS TotalBayarDebit,
		IS_NULL(SUM(IF(JenisPembayaran=2,Total,0)),0) AS TotalBayarKredit
	FROM $tbl_reservasi $kondisi GROUP BY KodeCabang ORDER BY KodeCabang";

if (!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket[$row['KodeCabang']]['Tiket']						= $row['Tiket'];
	$data_tiket[$row['KodeCabang']]['TotalPenjualan']		= $row['TotalPenjualan'];
	$data_tiket[$row['KodeCabang']]['TotalDiscount']		= $row['TotalDiscount'];
	$data_tiket[$row['KodeCabang']]['TotalBayarTunai']	= $row['TotalBayarTunai'];
	$data_tiket[$row['KodeCabang']]['TotalBayarDebit']	= $row['TotalBayarDebit'];
	$data_tiket[$row['KodeCabang']]['TotalBayarKredit']	= $row['TotalBayarKredit'];
}

//AMBIL DATA PAKET GROUP BY KodeCabang
$sql=
	"SELECT KodeCabang,COUNT(1) AS Paket,IS_NULL(SUM(HargaPaket),0) AS TotalPenjualan
	FROM tbl_paket WHERE TglBerangkat='$periode_mysql' GROUP BY KodeCabang ORDER BY KodeCabang";

if (!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket[$row['KodeCabang']]['Paket']					= $row['Paket'];
	$data_paket[$row['KodeCabang']]['TotalPenjualan']	= $row['TotalPenjualan'];
}

//AMBIL DATA TIKET ANALYTIC & DIMENSIONS
$sql=
	"SELECT 
		KodeCabang,IdJurusan,(SELECT KodeJurusan FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tr.IdJurusan) AS KodeJurusan,
		KodeKendaraan,
		IS_NULL(SUM(SubTotal),0) AS TotalPenjualan,IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM $tbl_reservasi tr $kondisi
	GROUP BY KodeCabang,IdJurusan,KodeKendaraan ORDER BY KodeCabang,IdJurusan,KodeKendaraan";

if (!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['KodeKendaraan']		= $row['KodeKendaraan'];
	$data_tiket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['KodeJurusan']			= $row['KodeJurusan'];
	$data_tiket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['TotalPenjualan']	= $row['TotalPenjualan'];
	$data_tiket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['TotalDiscount']		= $row['TotalDiscount'];
}

//AMBIL DATA PAKET ANALYTIC & DIMENSIONS
$sql=
	"SELECT 
		KodeCabang,IdJurusan,(SELECT KodeJurusan FROM tbl_md_jurusan tmj WHERE tmj.IdJurusan=tp.IdJurusan) AS KodeJurusan,
		KodeKendaraan,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualan
	FROM tbl_paket tp WHERE TglBerangkat='$periode_mysql'
	GROUP BY KodeCabang,IdJurusan,KodeKendaraan ORDER BY KodeCabang,IdJurusan,KodeKendaraan";

if (!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['KodeKendaraan']		= $row['KodeKendaraan'];
	$data_paket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['KodeJurusan']			= $row['KodeJurusan'];
	$data_paket_analytic[$row['KodeCabang']][$row['IdJurusan']][$row['KodeKendaraan']]['TotalPenjualan']	= $row['TotalPenjualan'];
}

//AMBIL DATA CABANG
$sql=
	"SELECT
		KodeCabang,COAAR,
		COASalesTiket,COAMarkDownTiket,
		COASalesPaket,COAMarkDownPaket,
		COACash,COABank
	FROM tbl_md_cabang ORDER BY KodeCabang";

if (!$result= $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$baris_header_s				= 2;
$baris_item_s					= 2;
$baris_distribution_s	= 2;
$baris_analytic_s			= 2;

$baris_header_cr				= 2;
$baris_distribution_cr	= 2;

while ($row = $db->sql_fetchrow($result)){
	//[ INTEGRASI SALES ] =======================================================================================================	
	$objPHPExcel->setActiveSheetIndex(0); 
	//PLOT SOP HEADER----------------------
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris_header_s, "3");
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris_header_s, "INVTIM");
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris_header_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris_header_s, $tgl_batch);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris_header_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris_header_s, $row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris_header_s, $row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$baris_header_s, "IDR");
	 //SELA
	$baris_header_s++;
	//END PLOT SOP HEADER------------------
	
	//PLOT SOP ITEM------------------------
	$item_sequence_line=1;
	//Tiket
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$baris_item_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$baris_item_s, "TIKET");
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$baris_item_s, $data_tiket[$row['KodeCabang']]['Tiket']." PAX");
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$baris_item_s, "1");
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$baris_item_s, "TIKET");
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$baris_item_s, $data_tiket[$row['KodeCabang']]['TotalPenjualan']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$baris_item_s, $data_tiket[$row['KodeCabang']]['TotalDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$baris_item_s, $row['COASalesTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$baris_item_s, $row['COAMarkDownTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$baris_item_s, $row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('T'.$baris_item_s, "TX");
	$objPHPExcel->getActiveSheet()->setCellValue('U'.$baris_item_s, $item_sequence_line);
	
	$baris_item_s++;
	$item_sequence_line++;
	$idx_ascii	-=12;
	//paket
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$baris_item_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$baris_item_s, "PAKET");
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$baris_item_s, $data_paket[$row['KodeCabang']]['Paket']." PAKET");
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$baris_item_s, "1");
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$baris_item_s, "PAKET");
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$baris_item_s, $data_paket[$row['KodeCabang']]['TotalPenjualan']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$baris_item_s, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$baris_item_s, $row['COASalesPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$baris_item_s, $row['COAMarkDownPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$baris_item_s, $row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('T'.$baris_item_s, "TX");
	$objPHPExcel->getActiveSheet()->setCellValue('U'.$baris_item_s, $item_sequence_line);
	$baris_item_s++;
	//END PLOT SOP ITEM--------------------
	
	//PLOT SOP DISTRIBUTION------------------------
	//COA Umum
	$distribution_sequence_line=1;
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$baris_distribution_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$baris_distribution_s, $row['COAAR']);
	$objPHPExcel->getActiveSheet()->setCellValue('Y'.$baris_distribution_s, $data_tiket[$row['KodeCabang']]['TotalPenjualan']+$data_paket[$row['KodeCabang']]['TotalPenjualan']-$data_tiket[$row['KodeCabang']]['TotalDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$baris_distribution_s, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('AA'.$baris_distribution_s, "2");
	$objPHPExcel->getActiveSheet()->setCellValue('AB'.$baris_distribution_s, "SALESTX ".$row['KodeCabang']." ".$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.$baris_distribution_s, $distribution_sequence_line);
	$baris_distribution_s++;
	$distribution_sequence_line++;
	
	//COA Sales Tiket
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$baris_distribution_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$baris_distribution_s, $row['COASalesTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('Y'.$baris_distribution_s, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$baris_distribution_s, $data_tiket[$row['KodeCabang']]['TotalPenjualan']);
	$objPHPExcel->getActiveSheet()->setCellValue('AA'.$baris_distribution_s, "1");
	$objPHPExcel->getActiveSheet()->setCellValue('AB'.$baris_distribution_s, "SALESTX ".$row['KodeCabang']." ".$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.$baris_distribution_s, $distribution_sequence_line);
	$baris_distribution_s++;
	$distribution_sequence_line++;
	
	//COA Mark Down Tiket
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$baris_distribution_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$baris_distribution_s, $row['COAMarkDownTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('Y'.$baris_distribution_s, $data_tiket[$row['KodeCabang']]['TotalDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$baris_distribution_s, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('AA'.$baris_distribution_s, "10");
	$objPHPExcel->getActiveSheet()->setCellValue('AB'.$baris_distribution_s, "SALESTX ".$row['KodeCabang']." ".$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.$baris_distribution_s, $distribution_sequence_line);
	$baris_distribution_s++;
	$distribution_sequence_line++;
	
	//COA Sales Paket
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$baris_distribution_s, $row['KodeCabang'].$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$baris_distribution_s, $row['COASalesPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('Y'.$baris_distribution_s, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$baris_distribution_s, $data_paket[$row['KodeCabang']]['TotalPenjualan']);
	$objPHPExcel->getActiveSheet()->setCellValue('AA'.$baris_distribution_s, "1");
	$objPHPExcel->getActiveSheet()->setCellValue('AB'.$baris_distribution_s, "SALESTX ".$row['KodeCabang']." ".$batch);
	$objPHPExcel->getActiveSheet()->setCellValue('AC'.$baris_distribution_s, $distribution_sequence_line);
	$baris_distribution_s++;
	$distribution_sequence_line++;
	
	
	//PLOT SOP ANALYTICS & DIMENSIONS------------------------
	$analytic_sequence_line=1;
	//-COA SALES TIKET-
	$assign_id=1;
	
	$ada_data	= false;
	if(count($data_tiket_analytic[$row['KodeCabang']])>0){
		foreach($data_tiket_analytic[$row['KodeCabang']] as $data_tiket_by_jurusan){
			foreach($data_tiket_by_jurusan as $data_tiket_by_kendaraan){
				if($data_tiket_by_kendaraan['TotalPenjualan']>0){
					$ada_data=true;
					//Counter
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COASalesTiket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, $data_tiket[$row['KodeCabang']]['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_tiket_by_kendaraan['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "COUNTER");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $row['KodeCabang']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					//Rute
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COASalesTiket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, $data_tiket[$row['KodeCabang']]['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_tiket_by_kendaraan['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "RUTE");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $data_tiket_by_kendaraan['KodeJurusan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					//Rute
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COASalesTiket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, $data_tiket[$row['KodeCabang']]['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_tiket_by_kendaraan['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "BODY");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $data_tiket_by_kendaraan['KodeKendaraan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					$assign_id++;
				}
			}
		}
		$analytic_sequence_line	+= $ada_data?1:0;
	}
	
	//-COA MARK DOWN-
	$assign_id=1;
	
	$ada_data	= false;
	if(count($data_tiket_analytic[$row['KodeCabang']])>0){
		foreach($data_tiket_analytic[$row['KodeCabang']] as $data_tiket_by_jurusan){
			foreach($data_tiket_by_jurusan as $data_tiket_by_kendaraan){
				
				if($data_tiket_by_kendaraan['TotalDiscount']>0){
					$ada_data=true;
					//Counter
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COAMarkDownTiket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, $data_tiket[$row['KodeCabang']]['TotalDiscount']);
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_tiket_by_kendaraan['TotalDiscount']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "COUNTER");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $row['KodeCabang']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					//Rute
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COAMarkDownTiket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, $data_tiket[$row['KodeCabang']]['TotalDiscount']);
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_tiket_by_kendaraan['TotalDiscount']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "RUTE");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $data_tiket_by_kendaraan['KodeJurusan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					//Body
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COAMarkDownTiket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, $data_tiket[$row['KodeCabang']]['TotalDiscount']);
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_tiket_by_kendaraan['TotalDiscount']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "BODY");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $data_tiket_by_kendaraan['KodeKendaraan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					$assign_id++;
				}
				
			}
		}
		$analytic_sequence_line	+= $ada_data?1:0;
	}
	
	
	//-COA PAKET-
	$assign_id=1;
	
	$ada_data	= false;
	if(count($data_paket_analytic[$row['KodeCabang']])>0){
		foreach($data_paket_analytic[$row['KodeCabang']] as $data_paket_by_jurusan){
			foreach($data_paket_by_jurusan as $data_paket_by_kendaraan){
				
				if($data_paket_by_kendaraan['TotalPenjualan']>0){
					$ada_data=true;
					//Counter
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COASalesPaket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, $data_paket[$row['KodeCabang']]['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_paket_by_kendaraan['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "COUNTER");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $row['KodeCabang']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					//Rute
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COASalesPaket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, $data_paket[$row['KodeCabang']]['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_paket_by_kendaraan['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "RUTE");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $data_paket_by_kendaraan['KodeJurusan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					//Body
					$objPHPExcel->getActiveSheet()->setCellValue('AE'.$baris_analytic_s, $row['KodeCabang'].$batch);
					$objPHPExcel->getActiveSheet()->setCellValue('AF'.$baris_analytic_s, $row['COASalesPaket']);
					$objPHPExcel->getActiveSheet()->setCellValue('AG'.$baris_analytic_s, $data_paket[$row['KodeCabang']]['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AH'.$baris_analytic_s, "0");
					$objPHPExcel->getActiveSheet()->setCellValue('AI'.$baris_analytic_s, $assign_id);
					$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$baris_analytic_s, $data_paket_by_kendaraan['TotalPenjualan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AK'.$baris_analytic_s, "BODY");
					$objPHPExcel->getActiveSheet()->setCellValue('AL'.$baris_analytic_s, $data_paket_by_kendaraan['KodeKendaraan']);
					$objPHPExcel->getActiveSheet()->setCellValue('AM'.$baris_analytic_s, $analytic_sequence_line);
					$baris_analytic_s++;
					
					$assign_id++;
				}
				
			}
		}
		$analytic_sequence_line	+= $ada_data?1:0;
	}
	
	//[ CASH RECEIPT ] =======================================================================================================	
	$objPHPExcel->setActiveSheetIndex(1); 
	//PLOT CR HEADER----------------------
	//Kas
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris_header_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris_header_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris_header_cr, $tgl_batch);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris_header_cr, $row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris_header_cr, "IDR");
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris_header_cr, "2");
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris_header_cr, $data_tiket[$row['KodeCabang']]['TotalBayarTunai']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$baris_header_cr, "KAS ".$row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$baris_header_cr, "");
	$baris_header_cr++;
	
	//Bank (Pembayaran Debit ataupun Kredit)
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$baris_header_cr, $row['KodeCabang'].$batch."02");
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$baris_header_cr, $row['KodeCabang'].$batch."02");
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$baris_header_cr, $tgl_batch);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$baris_header_cr, $row['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$baris_header_cr, "IDR");
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$baris_header_cr, "2");
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$baris_header_cr, $data_tiket[$row['KodeCabang']]['TotalBayarDebit']+$data_tiket[$row['KodeCabang']]['TotalBayarKredit']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$baris_header_cr, "BANK 1118");
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$baris_header_cr, "");
	$baris_header_cr++;
	
	//PLOT CR DISTRIBUTION----------------------
	//Kas COA Cash
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$baris_distribution_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$baris_distribution_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$baris_distribution_cr, $row['COACash']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$baris_distribution_cr, "1");
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$baris_distribution_cr, $data_tiket[$row['KodeCabang']]['TotalBayarTunai']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$baris_distribution_cr, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$baris_distribution_cr, "CRTX ".$row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$baris_distribution_cr, "1");
	$baris_distribution_cr++;
	
	//Kas COA AR
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$baris_distribution_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$baris_distribution_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$baris_distribution_cr, $row['COAAR']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$baris_distribution_cr, "3");
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$baris_distribution_cr, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$baris_distribution_cr, $data_tiket[$row['KodeCabang']]['TotalBayarTunai']);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$baris_distribution_cr, "CRTX ".$row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$baris_distribution_cr, "2");
	$baris_distribution_cr++;
	
	//Bank COA Bank
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$baris_distribution_cr, $row['KodeCabang'].$batch."02");
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$baris_distribution_cr, $row['KodeCabang'].$batch."02");
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$baris_distribution_cr, $row['COABank']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$baris_distribution_cr, "1");
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$baris_distribution_cr, $data_tiket[$row['KodeCabang']]['TotalBayarDebit']+$data_tiket[$row['KodeCabang']]['TotalBayarKredit']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$baris_distribution_cr, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$baris_distribution_cr, "CRTX ".$row['KodeCabang'].$batch."02");
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$baris_distribution_cr, "1");
	$baris_distribution_cr++;
	
	//Bank COA AR
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$baris_distribution_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$baris_distribution_cr, $row['KodeCabang'].$batch."01");
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$baris_distribution_cr, $row['COAAR']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$baris_distribution_cr, "3");
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$baris_distribution_cr, "0");
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$baris_distribution_cr, $data_tiket[$row['KodeCabang']]['TotalBayarDebit']+$data_tiket[$row['KodeCabang']]['TotalBayarKredit']);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$baris_distribution_cr, "CRTX ".$row['KodeCabang'].$batch."02");
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$baris_distribution_cr, "2");
	$baris_distribution_cr++;
	
}

//$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION); 

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Export Untuk GP Periode '.$periode.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output'); 
?>
