<?php

define('FRAMEWORK', true);
chdir("/home/trustudio/public_html/");

$adp_root_path = './';

include($adp_root_path . 'common.php');

$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!=""){ 
  echo("ACCESS DENIED");
	exit;
}
//#############################################################################

//INISIALISASI

$waktu_kenaikan	= "2014090101";
$waktu_kenaikan_mysql	= "2014-09-01";

//$waktu_kenaikan	= "2012123108";

$waktu_sekarang	= date("YmdH");

if($waktu_sekarang!=$waktu_kenaikan){
	echo("TIME DENIED ".$waktu_sekarang);
	exit;
}

$kenaikan_harga	= 10000;

$sql_include	=
	"'ATR-DPU','DPU-ATR','PST-ATR','BNS-DPU','DPU-BNS','PST-BNS','FXP-DPU','DPU-FXP','PST-FXP','GRG-CHP','CHP-GRG','PST-GRG','JTW-CHP','CHP-JTW','PST-JTW','JTW-DPU','DPU-JTW','PST-JTW','KRT-CHP','CHP-KRT','PST-KRT','PSQ-CHP','CHP-PSQ','PST-PSQ','PIM-CHP','CHP-PIM','PST-PIM','SNY-DPU','DPU-SNY','PST-SNY','TGC-CHP','CHP-TGC','PST-TGC','TBT-CHP','CHP-TBT','PST-TBT'";
	
//UPDATE TARIF
$sql =
	"UPDATE tbl_md_jurusan
	SET
		HargaTiket=HargaTiket+$kenaikan_harga,
		HargaTiketTuslah=HargaTiketTuslah+$kenaikan_harga
	WHERE KodeJurusan IN ($sql_include)";
			
	if (!$result = $db->sql_query($sql)){
		echo('Err: '.__LINE__);
		exit;
	}

//UPDATE HARGA TIKET YANG BELUM BAYAR	
	$sql =
	"UPDATE tbl_reservasi
	SET
		HargaTiket=HargaTiket+$kenaikan_harga,
		Subtotal=Subtotal+$kenaikan_harga,
		Total=Total+$kenaikan_harga
	WHERE TglBerangkat>='$waktu_kenaikan_mysql' AND CetakTiket!=1 AND IdJurusan IN(SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeJurusan IN($sql_include))";
			
	if (!$result = $db->sql_query($sql)){
		echo('Err: '.__LINE__);
		exit;
	}

?>