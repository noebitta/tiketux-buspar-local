<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassUser.php');

// SESSION
$id_page = 708;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Cabang	= new Cabang();
$User		= new User();

function setComboCabang($cabang_dipilih){
	//SET COMBO cabang
	global $db;
	global $Cabang;
			
	$result=$Cabang->ambilData("","Nama,Kota","ASC");
	$opt_cabang="";
		
	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
			$opt_cabang .="<option value='$row[KodeCabang]' $selected>$row[Nama] $row[Kota] ($row[KodeCabang])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END SET COMBO CABANG
}


	if ($mode=='add'){
		// add 
		
		$tgl_berlaku	= date("d-m-").(date("Y")+1);
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'user/add_body.tpl'));

    $template->assign_vars(array(
		 'BCRUMP'		=>setBcrump($id_page),
		 'JUDUL'		=> 'Tambah Data User',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'TGL_BERLAKU'=>$tgl_berlaku,
		 'OPT_CABANG'			=>setComboCabang(""),
		 'OPT_USER_LEVEL'	=>setComboUserLevel(2),
		 'PESAN'						=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'U_USER_ADD_ACT'	=> append_sid('pengaturan_user.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah user
		$user_id  				= str_replace(" ","",$HTTP_POST_VARS['user_id']);
		$nrp							= str_replace(" ","",$HTTP_POST_VARS['nrp']);
		$nama   					= $HTTP_POST_VARS['nama'];
		$alamat   				= $HTTP_POST_VARS['alamat'];
		$telp							= $HTTP_POST_VARS['telp'];
		$hp								= $HTTP_POST_VARS['hp'];
		$email						= $HTTP_POST_VARS['email'];
		$user_name_old		= $HTTP_POST_VARS['user_name_old'];
		$user_name				= $HTTP_POST_VARS['user_name'];
		$user_password		= $HTTP_POST_VARS['userpassword'];
		$konfirm_password	= $HTTP_POST_VARS['kofirm_password'];
		$cabang  					= $HTTP_POST_VARS['opt_cabang'];
		$user_level				= $HTTP_POST_VARS['opt_user_level'];
		$tgl_berlaku			= $HTTP_POST_VARS['tgl_berlaku'];
		$status_aktif			= $HTTP_POST_VARS['aktif'];
		$cetak_bbm				= $HTTP_POST_VARS['cetak_bbm'];

    $terjadi_error=false;
		
		if($User->periksaDuplikasi($user_name) && $user_name!=$user_name_old){
			$pesan="<font color='white' size=3>Username yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data User";

				if($User->tambah(
					$user_name, $user_password , $nrp,
				  $nama, $telp, $hp,
				  $email,$alamat, $cabang,
				  0, $tgl_berlaku, $user_level,
				  $status_aktif,$cetak_bbm)){
					
					redirect(append_sid('pengaturan_user.'.$phpEx.'?mode=add&pesan=1',true));
					
				}
			}
			else{
				
				$judul="Ubah Data User";

				if($User->ubah(
					$user_id,
					$user_name, $user_password , $nrp,
				  $nama, $telp, $hp,
				  $email,$alamat, $cabang,
				  0, $tgl_berlaku, $user_level,
				  $status_aktif,$cetak_bbm)){
					
					if($user_password!=""){
						$User->ubahPassword($user_id,$user_password);
					}
					
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}
			}
			
		}
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";

		$temp_var_bbm="cetak_bbm_".$cetak_bbm;
		$$temp_var_bbm="selected";
		
		$template->set_filenames(array('body' => 'user/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'		=>setBcrump($id_page),
			 'JUDUL'		=> $judul,
			 'MODE'   	=> 'save',
			 'SUB'    	=> $submode,
			 'USER_ID' 	=> $user_id,
			 'NRP'    	=> $nrp,
			 'NAMA'    	=> $nama,
			 'ALAMAT'   => $alamat,
			 'TELP'			=> $telp,
			 'HP'				=> $hp,
			 'EMAIL'		=> $email,
			 'USER_NAME_OLD'=> $user_name_old,
			 'USER_NAME' => $user_name,
			 'OPT_CABANG'=>setComboCabang($cabang),
			 'OPT_USER_LEVEL'	=>setComboUserLevel(substr($user_level.".0",0,3)),
			 'TGL_BERLAKU'		=>$tgl_berlaku,
			 'AKTIF_1'				=> $status_aktif_1,
			 'AKTIF_0'				=> $status_aktif_0,
			 'BBM_1'				=> $cetak_bbm_1,
			 'BBM_0'				=> $cetak_bbm_0,
			 'PESAN'					=> $pesan,
			 'BGCOLOR_PESAN'=> $bgcolor_pesan,
			 'U_USER_ADD_ACT'=>append_sid('pengaturan_user.'.$phpEx)
			)
		);
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$User->ambilDataDetail($id);
		
		$temp_var_aktif="status_aktif_".$row['user_active'];
		$$temp_var_aktif="selected";

		$temp_var_bbm="cetak_bbm_".$row['cetak_bbm'];
		$$temp_var_bbm="selected";
		
		$template->set_filenames(array('body' => 'user/add_body.tpl'));

    $template->assign_vars(array(
			 'BCRUMP'		=>setBcrump($id_page),
			 'JUDUL'		=> 'Ubah Data User',
			 'MODE'   	=> 'save',
			 'SUB'    	=> '1',
			 'USER_ID' 	=> $row['user_id'],
			 'NRP'    	=> $row['NRP'],
			 'NAMA'    	=> $row['nama'],
			 'ALAMAT'   => $row['address'],
			 'TELP'			=> $row['telp'],
			 'HP'				=> $row['hp'],
			 'EMAIL'		=> $row['email'],
			 'USER_NAME_OLD'=> $row['username'],
			 'USER_NAME' => $row['username'],
			 'OPT_CABANG'=>setComboCabang($row['KodeCabang']),
			 'OPT_USER_LEVEL'	=>setComboUserLevel($row['user_level']),
			 'TGL_BERLAKU'		=>FormatMySQLDateToTgl($row['berlaku']),
			 'AKTIF_1'				=> $status_aktif_1,
			 'AKTIF_0'				=> $status_aktif_0,
				'BBM_1'				=> $cetak_bbm_1,
				'BBM_0'				=> $cetak_bbm_0,
			 'U_USER_ADD_ACT'=>append_sid('pengaturan_user.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus user
		$list_user = str_replace("\'","'",$HTTP_GET_VARS['list_user']);
		//echo($list_user. " asli :".$HTTP_GET_VARS['list_user']);
		$User->hapus($list_user);
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		// aksi ubah status aktif
		$user_id = str_replace("\'","'",$HTTP_GET_VARS['user_id']);
		
		$User->ubahStatus($user_id);
		
		exit;
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'user/user_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$temp_cari=str_replace("cabang=","",$cari);
		if($temp_cari==$cari){
			$kondisi_cabang = 
				"nrp LIKE '%$cari%' 
				OR nama LIKE '%$cari%' 
				OR username LIKE '%$cari%'
				OR telp LIKE '%$cari%'
				OR hp LIKE '%$cari%'
				OR email LIKE '%$cari%'
				OR address LIKE '%$cari%'";
		}
		else{
			$kondisi_cabang = "f_cabang_get_name_by_kode(KodeCabang) LIKE '%$temp_cari%' ";
			$cari=$temp_cari;
		}
		
		$kondisi	=($cari=="")?"WHERE 1":
			" WHERE  $kondisi_cabang";
		
		$kondisi_user	= $userdata['user_level']!=$USER_LEVEL_INDEX['ADMIN']?" AND user_level!='".$USER_LEVEL_INDEX['ADMIN']."'":"";
		
		$sort_by	= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
		$order		= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

		$order	=($order=='')?"ASC":$order;
			
		$sort_by =($sort_by=='')?"Nama":$sort_by;

		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging		= pagingData($idx_page,"user_id","tbl_user","&cari=$cari&sort_by=$sort_by&order=$order",$kondisi.$kondisi_user,"pengaturan_user.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT user_id,nama,nrp,username,f_cabang_get_name_by_kode(KodeCabang) as namacabang,user_level,telp,hp,user_active,cetak_bbm
			FROM tbl_user $kondisi $kondisi_user
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		

		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['user_active']){
					$status="<a href='' onClick='return ubahStatus(\"$row[0]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[0]\")'>Nonaktif</a>";
				}

			  	if($row['cetak_bbm'] == 0){
					$akses_cetak_bbm = "<font color='black'><b>TIDAK BISA CETAK</b></font>";
				}else{
					$akses_cetak_bbm = "<font color='red'><b>BISA CETAK</b></font>";
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_user.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'=>$odd,
							'check'=>$check,
							'no'=>$i,
							'nama'=>$row['nama'],
							'nrp'=>$row['nrp'],
							'username'=>$row['username'],
							'cabang'=>$row['namacabang'],
							'user_level'=>$USER_LEVEL[$row['user_level']],
							'telp_hp'=>$row['telp']."/".$row['hp'],
							'status'=>$status,
							'akses_cetak'=>$akses_cetak_bbm,
							'action'=>$act
						)
					);
				
				$i++;
		  }
		} 
		else{
			//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
			echo("Error:".__LINE__);exit;
		} 
		
		$page_title	= "Pengaturan User";
		
		//paramter sorting
		$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
		$parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

    $template->assign_vars(array(
			'BCRUMP'    		=>setBcrump($id_page),
			'U_USER_ADD'		=> append_sid('pengaturan_user.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_user.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'PAGING'				=> $paging,
			'A_SORT_1'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=nama'.$parameter_sorting),
			'A_SORT_2'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=NRP'.$parameter_sorting),
			'A_SORT_3'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=username'.$parameter_sorting),
			'A_SORT_4'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=namacabang'.$parameter_sorting),
			'A_SORT_5'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=user_level'.$parameter_sorting),
			'A_SORT_6'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=telp'.$parameter_sorting),
			'A_SORT_7'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=user_active'.$parameter_sorting),
			'A_SORT_8'			=> append_sid('pengaturan_user.'.$phpEx.'?sort_by=cetak_bbm'.$parameter_sorting),
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>