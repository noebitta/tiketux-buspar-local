<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 302;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//################################################################################

$Cabang =new Cabang();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$filter_by 			= isset($HTTP_GET_VARS['filterby'])? $HTTP_GET_VARS['filterby'] : $HTTP_POST_VARS['filterby'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_cabang/laporan_omzet_cabang_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

if($userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	

	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"KodeCabang","tbl_md_cabang",
"&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_omzet_cabang.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

//MENGAMBIL DATA-DATA MASTER CABANG
$sql_cabang=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql_cabang)){
	echo("Err:".__LINE__);exit;
}

$fil_tgl	= $filter_by==0?"TglBerangkat":"DATE(WaktuCetakTiket)";

//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang2,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) IN (SELECT KodeCabang
		FROM tbl_md_cabang
		$kondisi_cari)
	GROUP BY KodeCabang2
	ORDER BY KodeCabang2";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['KodeCabang2']]	= $row['JadwalDibuka'];
}


//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang2,
		IS_NULL(COUNT(IF(CetakTiket!=1,NoTiket,NULL)),0) AS TotalPenumpangB,
		IS_NULL(COUNT(IF(JenisPenumpang IN('U','','14','14_20K','14_5K','TP','TK') AND CetakTiket=1  AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='GV' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang LIKE 'R%' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(JenisPenumpang='T' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangO,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS TotalTiket,
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,IF(JenisPenumpang!='T',SubTotal,HargaTiketux-Komisi),0),0)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE ($fil_tgl BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY KodeCabang2 ORDER BY KodeCabang2";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang2']]	= $row;
}

//DATA KEBERANGKATAN BY MANIFEST
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang2,
		IS_NULL(COUNT(1),0) AS TotalBerangkat
	FROM tbl_spj
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY KodeCabang2 ORDER BY KodeCabang2";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['KodeCabang2']]	= $row['TotalBerangkat'];
}

//DATA KEBERANGKATAN MANIFEST PICKUP/TRANSIT
/*$sql	=
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang2,
		IS_NULL(COUNT(DISTINCT(IF(CetakTiket=1,NoSPJ,NULL))),0) AS TotalBerangkat
	FROM tbl_reservasi tr
	WHERE ($fil_tgl BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 AND (SELECT FlagSubJadwal FROM tbl_md_jadwal tmj WHERE tmj.KodeJadwal=tr.KodeJadwal)=1 $kondisi_cabang_2
	GROUP BY KodeCabang2 ORDER BY KodeCabang2";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['KodeCabang2']]	= $jumlah_berangkat[$row['KodeCabang2']]+$row['TotalBerangkat'];
}*/


//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang2,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (".($filter_by==0?"DATE(TglBerangkat)":"DATE(WaktuPesan)")." BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY KodeCabang2 ORDER BY KodeCabang2";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang2']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang2,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang
	GROUP BY KodeCabang2 ORDER BY KodeCabang2";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['KodeCabang2']]	= $row;
}

//QUERY BIAYA TAMBAHAN
$sql = "SELECT KodeCabang,
		IS_NULL(SUM(Jumlah),0) as BiayaTambahan
		FROM tbl_biaya_tambahan
		$kondisi_cari
		AND (DATE(TglCetak) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		GROUP BY KodeCabang ORDER BY KodeCabang";

if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
	//die(mysql_error());
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_tambahan_total[$row['KodeCabang']]	= $row;
}


//isi array temp laporan

$temp_array=array();

$idx=0;

$total_b	= 0;
$total_u	= 0;
$total_m	= 0;
$total_k	= 0;
$total_kk	= 0;
$total_v	= 0;
$total_g	= 0;
$total_r	= 0;
$total_vr	= 0;
$total_o 	= 0;
$total_t	= 0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']				= $row['KodeCabang'];
	$temp_array[$idx]['Nama']					= $row['Nama'];
	$temp_array[$idx]['Alamat']					= $row['Alamat'];
	$temp_array[$idx]['Kota']					= $row['Kota'];
	$temp_array[$idx]['Telp']					= $row['Telp'];
	$temp_array[$idx]['TotalOpenTrip']			= $data_jadwal_tersedia[$row['KodeCabang']];
	$temp_array[$idx]['TotalPenumpangB']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangB'];
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangV']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalPenumpangO']		= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
	$temp_array[$idx]['TotalBerangkat']			= $jumlah_berangkat[$row['KodeCabang']];
	$temp_array[$idx]['TotalTiket']				= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']	= $data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']	= $data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']				= $data_paket_total[$row['KodeCabang']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']				= $data_biaya_total[$row['KodeCabang']]['TotalBiaya'];
	$temp_array[$idx]['BiayaTambahan']			= $data_biaya_tambahan_total[$row['KodeCabang']]['BiayaTambahan'];
	$temp_array[$idx]['TotalPenumpangPerTrip']	= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']					= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'] - $temp_array[$idx]['BiayaTambahan'];
	
	$total_b	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangB'];
	$total_u	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$total_m	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$total_k	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$total_kk	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$total_v	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangV'];
	$total_g	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$total_r	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$total_vr	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangVR'];
	$total_o	+= $data_tiket_total[$row['KodeCabang']]['TotalPenumpangO'];
	$total_t	+= $data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_omzet_cabang_detail.php?tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&filterby='.$filter_by)."\");return false'>Detail<a/>";		
	
	/*$act 	.="<a href='".append_sid('laporan_omzet_cabang_grafik.php?kode_cabang='.$temp_array[$idx]['KodeCabang'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order)."'>Grafik<a/>";		
	*/
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];

	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	$total_tambahan					= $temp_array[$idx]['BiayaTambahan'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'kode_cabang'=>$temp_array[$idx]['KodeCabang'],
				'cabang'=>$temp_array[$idx]['Nama'],
				'alamat'=>$temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Kota'],
				'open_trip'=>number_format($temp_array[$idx]['TotalOpenTrip'],0,",","."),
				'total_keberangkatan'=>number_format($temp_array[$idx]['TotalBerangkat'],0,",","."),
				'total_penumpang_b'=>number_format($temp_array[$idx]['TotalPenumpangB'],0,",","."),
				'total_penumpang_u'=>number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."),
				'total_penumpang_m'=>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
				'total_penumpang_k'=>number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."),
				'total_penumpang_kk'=>number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."),
				'total_penumpang_v'=>number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."),
				'total_penumpang_g'=>number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."),
				'total_penumpang_r'=>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
				'total_penumpang_vr'=>number_format($temp_array[$idx]['TotalPenumpangVR'],0,",","."),
				'total_penumpang_o'=>number_format($temp_array[$idx]['TotalPenumpangO'],0,",","."),
				'total_penumpang'=>number_format($total_tiket,0,",","."),
				'rata_pnp_per_trip'=>number_format($temp_array[$idx]['TotalPenumpangPerTrip'],0,",","."),
				'total_omzet'=>number_format($total_penjualan_tiket,0,",","."),
				'total_paket'=>number_format($total_paket,0,",","."),
				'total_omzet_paket'=>number_format($total_penjualan_paket,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
				'total_tambahan'=>number_format($total_tambahan,0,",","."),
				'total_biaya'=>number_format($total_biaya,0,",","."),
				'total_profit'=>number_format($total,0,",","."),
				'act'=>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$temp_array[$idx]['KodeCabang'].
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
											
$script_cetak_excel="Start('laporan_omzet_cabang_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
	
$page_title	= "Penjualan Cabang";

$temp_var	= "filterby".$filter_by;
$$temp_var= "selected";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_omzet_cabang.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'TOTAL_B'				=> number_format($total_b,0,",","."),
	'TOTAL_U'				=> number_format($total_u,0,",","."),
	'TOTAL_M'				=> number_format($total_m,0,",","."),
	'TOTAL_K'				=> number_format($total_k,0,",","."),
	'TOTAL_KK'			=> number_format($total_kk,0,",","."),
	'TOTAL_V'				=> number_format($total_v,0,",","."),
	'TOTAL_G'				=> number_format($total_g,0,",","."),
	'TOTAL_R'				=> number_format($total_r,0,",","."),
	'TOTAL_VR'			=> number_format($total_vr,0,",","."),
	'TOTAL_O'				=> number_format($total_o,0,",","."),
	'TOTAL_T'				=> number_format($total_t,0,",","."),
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=Nama'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Nama cabang ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=KodeCabang'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Kode cabang ($order_invert)",
	'A_SORT_3'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=Alamat'.$parameter_sorting),
	'TIPS_SORT_3'		=> "Urutkan Alamat cabang ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalBerangkat'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan Total trip ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangU'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Total penumpang ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangM'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Total penumpang ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangK'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Total penumpang member ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangKK'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Total Penumpang ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangG'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan Total gratis ($order_invert)",
	'A_SORT_10'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalTiket'.$parameter_sorting),
	'TIPS_SORT_10'	=> "Urutkan Total penumpang ($order_invert)",
	'A_SORT_11'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangPerTrip'.$parameter_sorting),
	'TIPS_SORT_11'	=> "Urutkan Total penumpang/trip($order_invert)",
	'A_SORT_12'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenjualanTiket'.$parameter_sorting),
	'TIPS_SORT_12'	=> "Urutkan Total omzet tiket ($order_invert)",
	'A_SORT_13'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPaket'.$parameter_sorting),
	'TIPS_SORT_13'	=> "Urutkan Total paket ($order_invert)",
	'A_SORT_14'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenjualanPaket'.$parameter_sorting),
	'TIPS_SORT_14'	=> "Urutkan Total omzet paket ($order_invert)",
	'A_SORT_15'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalDiscount'.$parameter_sorting),
	'TIPS_SORT_15'	=> "Urutkan Total discount ($order_invert)",
	'A_SORT_16'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalBiaya'.$parameter_sorting),
	'TIPS_SORT_16'	=> "Urutkan Total biaya ($order_invert)",
	'A_SORT_17'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=Total'.$parameter_sorting),
	'TIPS_SORT_17'	=> "Urutkan Total ($order_invert)",
	'A_SORT_18'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangB'.$parameter_sorting),
	'TIPS_SORT_18'	=> "Urutkan Jumlah Booking ($order_invert)",
	'A_SORT_19'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangV'.$parameter_sorting),
	'TIPS_SORT_19'	=> "Urutkan Total Penumpang ($order_invert)",
	'A_SORT_20'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangR'.$parameter_sorting),
	'TIPS_SORT_20'	=> "Urutkan total tiket return ($order_invert)",
	'A_SORT_21'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangVR'.$parameter_sorting),
	'TIPS_SORT_21'	=> "Urutkan total tiket voucher return ($order_invert)",
	'FILTER_BY0'		=> $filterby0,
	'FILTER_BY1'		=> $filterby1,
	)
);
	     
				
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>