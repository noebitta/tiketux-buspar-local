<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSuratJalan.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$id_page = 212;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode'];
$asal 			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal']; 
$tujuan 		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$Jurusan		= new Jurusan();
$Cabang			= new Cabang();
$SuratJalan	= new SuratJalan();
$Mobil			= new Mobil();
$Sopir			= new Sopir();

//FUNCTION=============================================================================================================================

function setComboJam($jam_dipilih){
	$opt_jam="";
		
		for($jam=0;$jam<24;$jam++){
			$str_jam=substr("0".$jam,-2);
			
			$selected=($jam_dipilih!=$str_jam)?"":"selected";
			$opt_jam .="<option $selected value=$str_jam>$str_jam</option>";
		}
		
		return $opt_jam;
}

function setComboMenit($menit_dipilih){
	$opt_menit="";
	
	for($menit=0;$menit<60;$menit++){
		$str_menit	= substr("0".$menit,-2);
		$selected=($menit_dipilih!=$str_menit)?"":"selected";
		$opt_menit .="<option $selected value=$str_menit>$str_menit</option>";
	}
	
	return $opt_menit;
}

function setComboCabangTujuan($cabang_asal="",$cabang_dipilih=""){
	//SET COMBO cabang
	global $db;
	
	$opt_cabang="<option value='' >silahkan pilih...</option>";
	
	if($cabang_asal==""){
		return $opt_cabang;
	}
	
	$sql = 
			"SELECT IdJurusan,KodeJurusan,KodeCabangTujuan,Nama,Kota
			FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON KodeCabangTujuan=KodeCabang
			WHERE KodeCabangAsal='$cabang_asal'
			ORDER BY Kota,Nama;";
				
	if (!$result = $db->sql_query($sql)){
		echo("Err: $this->ID_FILE". __LINE__);
		exit;
	}
	
	if($result){
		
		$kota="";
		
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($cabang_dipilih!=$row['IdJurusan'])?"":"selected";
			
			if($kota!=$row['Kota']){
					
				if($kota!=""){
					$opt_cabang .= "</optgroup>";
				}
					
				$kota=$row['Kota'];
				$opt_cabang .="<optgroup label='$kota'>";
			}
			
			$opt_cabang .="<option value='$row[KodeCabangTujuan]|$row[IdJurusan]|$row[KodeJurusan]' $selected>$row[Nama] ($row[KodeCabangTujuan])</option>";
		}
	}
	else{
		echo("Error :".__LINE__);exit;
	}		
	return $opt_cabang;
	//END setInterfaceComboCabang
}

//AKSI================================================================================================================

switch($mode){
	case "":
		//show index list surat jalan
		$pool  					= isset($HTTP_GET_VARS['pool'])? $HTTP_GET_VARS['pool'] : $HTTP_POST_VARS['pool'];
		$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
		$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
		$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
		$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
		
		$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
		$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
		$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
		$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

		
		$order	=($order=='')?"ASC":$order;
	
		$sort_by =($sort_by=='')?"Berangkat":$sort_by;
		
		// LIST
		$template->set_filenames(array('body' => 'surat_jalan/index_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" AND(tsj.NoSJ LIKE '%$cari' 
				OR NamaPetugas LIKE '%$cari%' 
				OR NoBody LIKE '%$cari'
				OR NoPolisi LIKE '%$cari'
				OR KodeSopir LIKE '%$cari'
				OR NamaSopir LIKE '%$cari%')";
		
		$kondisi.= " AND (TglSJ BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')";
		$kondisi.= $pool==""?"":" AND IdPool=$pool";
		
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"DISTINCT(tsj.NoSJ)",
			"tbl_surat_jalan tsj LEFT JOIN  tbl_surat_jalan_biaya tsjb ON tsj.NoSJ=tsjb.NoSJ",
			"&cari=$cari&sort_by=$sort_by&order=$order&pool=$pool",
			"WHERE 1 $kondisi GROUP BY tsj.NoSJ","surat_jalan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT tsj.*,
				CONCAT(TglSJ,'<br>',WaktuSJ) AS Berangkat,
				CONCAT(NoBody,'<br>',NoPolisi) AS Kendaraan,
				CONCAT(NamaSopir,'<br>',KodeSopir) AS Sopir,
				GROUP_CONCAT(CONCAT(KodeJurusan,':',NamaBiaya,'=',Jumlah) ORDER BY IdBiaya SEPARATOR '|') AS DetailBiaya,
				GROUP_CONCAT(DISTINCT(KodeJurusan) ORDER BY IdBiaya) AS Rute,
				SUM(Jumlah) AS TotalBiaya
			FROM tbl_surat_jalan tsj LEFT JOIN  tbl_surat_jalan_biaya tsjb ON tsj.NoSJ=tsjb.NoSJ
			WHERE 1 $kondisi
			GROUP BY tsj.NoSJ
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";
		
		$idx_check=0;
		
		
		if (!$result = $db->sql_query($sql)){
			//die_error('Cannot Load jurusan',__FILE__,__LINE__,$sql);
			echo("Err:".__LINE__);exit;
		}
		
		$i = $idx_page*$VIEW_PER_PAGE+1;
		
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
			
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$odd	= !$row['IsBatal']?$odd:"red";
			
			$act 	="<a href='".append_sid('surat_jalan.php?mode=cetakt&id='.$row['NoSJ'])."'>Cetak</a> + ";
			$act .="<a href='#' onclick='batal(\"".$row['NoSJ']."\");'>Batal</a>";
			
			if($row['IsBatal']==0){
				$remark="";
			}
			else{
				$act		= "DIBATALKAN";
				$remark	= "Dibatalkan Oleh: $row[NamaPembatal]<br>Pada: ".FormatMySQLDateToTglWithTime($row['WaktuBatal']);
			}
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i,
						'nosj'=>$row['NoSJ'],
						'berangkat'=>$row['Berangkat'],
						'kendaraan'=>$row['Kendaraan'],
						'sopir'=>$row['Sopir'],
						'trip'=>$row['JumlahTrip'],
						'pool'=>$LIST_POOL[$row['IdPool']],
						'rute'=>$row['Rute'],
						'biaya'=>$row['DetailBiaya'],
						'total'=>"Rp. ".number_format($row['TotalBiaya'],0,",","."),
						'waktucetak'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuCetak'])),
						'petugas'=>$row['NamaPetugas'],
						'reamark'=>$remark,
						'title'=>$row['DetailBiaya'],
						'act'=>$act
					)
				);
			
			$i++;
		}
		
		if($i-1<=0){
			$no_data	=	"<tr><td colspan=15 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
		}
			
		
		
		$page_title	= "Surat Jalan";
		
		$temp_var	= "pool_".$pool;
		$$temp_var = "selected";

		$template->assign_vars(array(
			'BCRUMP'    			=>setBcrump($id_page),
			'U_ADD'						=> append_sid('surat_jalan.'.$phpEx.'?mode=add'),
			'ACTION_CARI'			=> append_sid('surat_jalan.'.$phpEx),
			'TGL_AWAL'				=> $tanggal_mulai,
			'TGL_AKHIR'				=> $tanggal_akhir,
			'OPT_KOTA'				=> setComboKota($kota),
			'POOL_0'					=> $pool_0,
			'POOL_1'					=> $pool_1,
			'TXT_CARI'				=> $cari,
			'NO_DATA'					=> $no_data,
			'KETERANGAN_SORT'	=> "sort by:".$sort_by." ".$order,
			'PAGING'					=> $paging
			)
		);
	break;
	
	case "getasal":
		$act_idx	= $HTTP_GET_VARS['idx']==""?"":",".$HTTP_GET_VARS['idx'];
	
		echo "
			<select name='asal$idx' id='asal$idx' onChange='getUpdateTujuan(this.value,$act_idx);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
	exit;
		
	case "gettujuan":
		$idx	= $HTTP_GET_VARS['idx'];
	
		$next_idx		= $idx+1;
		
		$asals	= explode("|",$asal);
		
		echo "
			<select name='tujuan$idx' id='tujuan$idx' onChange='getUpdateTujuan(this.value,$next_idx);getBiaya(this.value,$idx);'>
				".setComboCabangTujuan($asals[0],$asals[1])."
			</select>";
	exit;
	
	case "setdetailbiaya":
		$no_sj= $HTTP_GET_VARS["no_sj"];
		$trip	= $HTTP_GET_VARS["trip"];
		
		$asal		= array();
		$tujuan	= array();
		
		if($no_sj!=""){
			//JIKA NO SURAT JALAN TIDAK KOSONG, BERARTI MENGAMBIL DATA DARI DATABASE	
			
		}
		
		$output	=
			"<table width='500'>
				<tr><td colspan=3><h3>Detail Biaya:</h3></td></tr>";
		
		for($idx=0;$idx<$trip;$idx++){
			$no_trip	= $idx+1;
			
			$output .=($no_trip!=1)?"":"
				<tr>
					<td valign='top'><b>ASAL</b></td><td  valign='top'>:</td>
					<td valign='top'>
						<select name='asal$idx' id='asal$idx' onChange='getUpdateTujuan(this.value,$idx);'>
							".$Cabang->setInterfaceComboCabangByKota("",$asal[$idx],"")."
						</select>
					</td>
				</tr>";
			
			$output	.=
				"<tr><td valign='top' colspan='3'><b>TRIP $no_trip</b></td></tr>
				<tr>
					<td valign='top'>Tujuan</td><td  valign='top'>:</td>
					<td valign='top'><span id='rewrite_tujuan$idx'></span></td>
				</tr>
				<tr><td valign='top' colspan='3'><br><b>Biaya-biaya</b></td></tr>
				<tr><td colspan='3'><span id='rewrite_biaya$idx'></span></td></tr>
				<tr><td colspan='3'><hr></td></tr>
		";
		}
		
		$output.="</table>";
	
		echo($output);
	exit;
	
	case "getbiaya":
		$idx	= $HTTP_GET_VARS['idx'];
	
		if($no_sj!=""){
			//JIKA NO SURAT JALAN TIDAK KOSONG, BERARTI MENGAMBIL DATA DARI DATABASE	
			
		}

		$asals	= explode("|",$asal);
		$data_biaya	= $Jurusan->ambilBiaya($asals[1]);
		
		$output	= "<table padding=0 spacing=0>";
		
		//BBM
		$output	.="<tr><td>Voucher BBM</td><td width='5'>:</td><td align='right'>Rp. ".number_format($data_biaya['BiayaBBM'],0,",",".")."<input type='hidden' name='biaya_bbm$idx' value='".$data_biaya['BiayaBBM']."'><input type='hidden' name='coa_bbm$idx' value='".$data_biaya['KodeAkunBiayaBBM']."'></td></tr>";
		
		//TOL
		$output	.="<tr><td>Biaya TOL</td><td width='5'>:</td><td align='right'>Rp. ".number_format($data_biaya['BiayaTol'],0,",",".")."<input type='hidden' name='biaya_tol$idx' value='".$data_biaya['BiayaTol']."'><input type='hidden' name='coa_tol$idx' value='".$data_biaya['KodeAkunBiayaTol']."'></td></tr>";
		
		//UANG ORDER
		$output	.="<tr><td>Biaya ORDER</td><td width='5'>:</td><td align='right'>Rp. ".number_format($data_biaya['BiayaSopir'],0,",",".")."<input type='hidden' name='biaya_sopir$idx' value='".$data_biaya['BiayaSopir']."'><input type='hidden' name='coa_sopir$idx' value='".$data_biaya['KodeAkunBiayaSopir']."'></td></tr>";
	
		
		$output .="<table>";
		
		echo($output);
	
	exit;
	
	case "add":
		// add 
	
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'surat_jalan/add_body.tpl'));

		$template->assign_vars(array(
			'BCRUMP'		=>setBcrump($id_page),
			'JUDUL'			=> 'Tambah Surat Jalan',
			'MODE'   		=> 'save',
			'SUB'    		=> '0',
			'TGL_SJ'		=> dateD_M_Y(),
			'OPT_JAM' 	=> setComboJam(date("H")),
			'OPT_MENIT'	=> setComboMenit(date("i")),
			'OPT_POOL'	=> $SuratJalan->setComboPool(0),
			'OPT_KENDARAAN'=> $SuratJalan->setComboMobil(""),
			'OPT_SOPIR'	=> $SuratJalan->setComboSopir(""),
			'PESAN'						=> $pesan,
			'BGCOLOR_PESAN'		=> $bgcolor_pesan,
			'U_JURUSAN_ADD_ACT'	=> append_sid('surat_jalan.'.$phpEx)
		 )
		);
	break;

	case "save":
		// aksi membuat surat jalan

		$judul="Tambah Surat Jalan";
		$path	='<a href="'.append_sid('surat_jalan.'.$phpEx."?mode=add").'">Tambah Surat Jalan</a> ';
		
		$tgl_sj				= FormatTglToMySQLDate($_POST['tgl_sj']);
		$waktu_sj			= $_POST['opt_jam'].":".$_POST['opt_menit'];
		$id_pool			= $_POST['pool'];
		$id_petugas		= $userdata['user_id'];
		$nama_petugas	= $userdata['nama'];
		$kendaraan		= explode("|",$_POST['kendaraan']);
		$no_body			= $kendaraan[0];
		$no_polisi		= $kendaraan[1];
		$sopir				= explode("|",$_POST['sopir']);
		$kode_sopir		= $sopir[0];
		$nama_sopir		= $sopir[1];
		$jumlah_trip	= $_POST['trip'];

		$no_sj	= $SuratJalan->tambahHeader(
																				$tgl_sj,$waktu_sj,$id_pool,
																				$id_petugas,$nama_petugas,$no_body,
																				$no_polisi,$kode_sopir,$nama_sopir,
																				$jumlah_trip);

		if($no_sj!=""){
			//jika input berhasil, akan dilanjutkan dengan input biaya2

			for($idx=0;$idx<$jumlah_trip;$idx++){
				$idx_bbm			= "biaya_bbm$idx";
				$idx_coa_bbm	= "coa_bbm$idx";
				
				$idx_tol			= "biaya_tol$idx";
				$idx_coa_tol	= "coa_tol$idx";
				
				$idx_sopir		= "biaya_sopir$idx";
				$idx_coa_sopir= "coa_sopir$idx";
				
				$idx_jurusan	= "tujuan$idx";
				$jurusans			= explode("|",$_POST[$idx_jurusan]);
				
				$id_jurusan		= $jurusans[1];
				$kode_jurusan	= $jurusans[2];
				
				
				//VOUCHER BBM
				$SuratJalan->tambahBiaya(
																$no_sj,0,$_POST[$idx_coa_bbm],
																$id_jurusan,$kode_jurusan,"VOUCHER BBM",
																$_POST[$idx_bbm]);
				
				//BIAYA TOL
				$SuratJalan->tambahBiaya(
																$no_sj,1,$_POST[$idx_coa_tol],
																$id_jurusan,$kode_jurusan,"BIAYA TOL",
																$_POST[$idx_tol]);
				
				//BIAYA ORDER
				$SuratJalan->tambahBiaya(
																$no_sj,2,$_POST[$idx_coa_sopir],
																$id_jurusan,$kode_jurusan,"BIAYA ORDER",
																$_POST[$idx_sopir]);
			}
		}

    redirect(append_sid('surat_jalan.'.$phpEx.'?mode=add&pesan=1',true));
		//die_message('<h2>Data jurusan Telah Tersimpan</h2>','Click Di <a href="'.append_sid('surat_jalan.'.$phpEx.'?mode=add').'">Sini</a> Untuk Melanjutkan','');
	exit;	
	
	case "batal":
		$no_sj	= $_GET['no_sj'];
		$SuratJalan->batalkanSuratJalan($no_sj,$userdata['user_id'],$userdata['nama']);
	exit;
}
  
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>