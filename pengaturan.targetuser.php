<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassTargetUser.php');

// SESSION
$id_page = 710;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= isset($HTTP_GET_VARS['start'])? intval($HTTP_GET_VARS['start']) : 0;
$bulan          = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun          = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];
$cari   		= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['cari'];
$page		    = isset($HTTP_GET_VARS['page'])? $HTTP_GET_VARS['page'] : $HTTP_POST_VARS['page'];


$TargetUser		= new TargetUser();
$User		    = new User();

$mode   = $mode==""?"exp":$mode;
$bulan  = $bulan==""?date("m"):$bulan;
$bulan  = (int)$bulan;
$tahun  = $tahun==""?date("Y"):$tahun;
$page = ($page!='')?$page:0;

/**
 * UPDATE 31-10-2016
 * target user menjadi target call center
 */

switch($mode){
  case "simpan":
    /*ACTION SIMPAN PENGUBAHAN TARGET*/

    $i=$page*$VIEW_PER_PAGE;
    do{
      $i++;
      $user_id       = $HTTP_POST_VARS["userid$i"];
      $target_before = $HTTP_POST_VARS["targetbefore$i"];
      $target_input  = $HTTP_POST_VARS["targetinput$i"];

      if($user_id!="" && $target_before!=$target_input){
        /*JIKA ADA PERUBAHAN DATA, MAKA AKAN DISIMPAN*/

        $data_user  = $User->ambilDataDetail($userdata);
        $TargetUser->ubahTarget($bulan,$tahun,$user_id,$data_user["Nama"],$target_input,'UPDATE: '.date("H:i:s d-m-Y").' BY: '.$userdata['nama']);
      }

    }while($i<($page+1)*$VIEW_PER_PAGE && $user_id!="");

  case "exp":
    /*VIEW MODE*/

    $kondisi     = "WHERE user_level = $USER_LEVEL_INDEX[CALL_CENTER]";

    $kondisi	.=($cari=="")?"":" AND (nama LIKE '%$cari%' OR NRP LIKE '%$cari' OR telp LIKE '%$cari' OR hp LIKE '%$cari' OR username LIKE '%$cari%')";

    //PAGING======================================================
    $paging		= pagingData($page,"user_id","tbl_user","&cari=$cari&sort_by=$sort_by&order=$order",$kondisi.$kondisi_user,"pengaturan.targetuser.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
    //END PAGING======================================================

    /*MENGAMBIL DATA TARGET USER DARI TABEL TARGET*/
    $sql =
      "SELECT UserId,Target,TargetTercapai,Log
			FROM tbl_user_target
			WHERE BulanTarget='$bulan' AND TahunTarget='$tahun'
			ORDER BY UserId ASC";

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
      echo("Error:".__LINE__);exit;
    }

    while($row=$db->sql_fetchrow($result)){
      $data_target[$row['UserId']]['Target']        = $row['Target'];
      $data_target[$row['UserId']]['TargetTercapai']= $row['TargetTercapai'];
      $data_target[$row['UserId']]['Log']           = $row['Log'];
    }

    /*MENGAMBIL DATA CAPAIAN PENJUALAN CALL CENTER */
    $sql = "SELECT COUNT(NoTiket) AS Tiket, PetugasPenjual 
            FROM tbl_reservasi
            WHERE MONTH(WaktuPesan) = '$bulan' AND YEAR(WaktuPesan) = '$tahun'
            GROUP BY PetugasPenjual";

    if (!$result = $db->sql_query($sql)){
      echo("Error:".__LINE__);exit;
    }

    while ($row = $db->sql_fetchrow($result)){
        $data_tiket[$row['PetugasPenjual']] = $row['Tiket'];
    }


    /*MENGAMBIL DATA USER DENGAN LEVEL CALL CENTER */
    $sql = "SELECT user_id,tbl_user.nama,nrp,user_level,user_active
			FROM tbl_user
			$kondisi
			ORDER BY Nama ASC LIMIT $idx_awal_record,$VIEW_PER_PAGE";

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load user',__FILE__,__LINE__,$sql);
      echo("Error:".__LINE__);exit;
    }

    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){
      $odd ='odd';

      if (($i % 2)==0){
        $odd = 'even';
      }

      //$capaian  = $data_target[$row['user_id']]['Target']>0?100*($data_tiket[$row['user_id']]/$data_target[$row['user_id']]['Target']):0;
      //$real     = number_format($data_tiket[$row['user_id']],0,",",".");

      $capaian  = $data_target[$row['user_id']]['Target']>0?100*($data_target[$row['user_id']]['TargetTercapai']/$data_target[$row['user_id']]['Target']):0;
      $real     = number_format($data_target[$row['user_id']]['TargetTercapai'],0,",",".");


      $template->
        assign_block_vars(
          'ROW',
          array(
            'odd'         =>$odd,
            'check'       =>$check,
            'no'          =>$i,
            'user_id'     =>$row['user_id'],
            'nama'        =>$row['nama'],
            'nrp'         =>$row['nrp'],
            'user_level'  =>$USER_LEVEL[$row['user_level']],
            'telp_hp'     =>$row['telp']."/".$row['hp'],
            'target'      =>($data_target[$row['user_id']]['Target']!=""?$data_target[$row['user_id']]['Target']:0),
            'real'        =>$real,
            'capaian'     =>round($capaian,2)
          )
        );

      $i++;
    }

    if($i-1<=0){
      $template->assign_block_vars("NO_DATA",array());
    }

    //paramter sorting
    $order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
    $parameter_sorting	= "&page=$page&cari=$cari&order=$order_invert";

    /*LOOP TAHUN PENCARIAN*/
    $thn_now  = date("Y");
    for($thn=$thn_now-2;$thn<=$thn_now+1;$thn++){
      $template->assign_block_vars(
        "TAHUN",array(
          "value"     => $thn
      ));
    }

    $page_title = "Target User";
    $template->set_filenames(array('body' => 'pengaturan.targetuser/index.tpl'));

    $template->assign_vars(array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('pengaturan.targetuser.'.$phpEx),
        'ACTION_SAVE'		=> append_sid('pengaturan.targetuser.'.$phpEx),
        'MODE'          => "simpan",
        'BULAN'         => $bulan,
        'TAHUN'         => $tahun,
        'PAGE'          => $page,
        'CARI'			    => $cari,
        'PAGING'			  => $paging
        )
    );

    include($adp_root_path . 'includes/page_header.php');
    $template->pparse('body');
    include($adp_root_path . 'includes/page_tail.php');
    exit;

  case "reset":
    /*ACTION RESET TARGET PER BULAN*/
    $TargetUser->resetTarget($bulan,$tahun,'RESETED: '.date("H:i:s d-m-Y").' BY: '.$userdata['nama']);
    exit;

}
?>