<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');

// SESSION
$id_page = 705;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Sopir	= new Sopir();
$Cabang = new Cabang();
function setDropdownCabang($cabang){
	global $db;

	$Cabang	= new Cabang();

	$result	= $Cabang->setComboCabang();

	$opt = "";

	if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;

		while ($row = $db->sql_fetchrow($result)){
			$selected = ($cabang != $row[0])?"":"selected";
			$opt .= "<option id='$row[0]' value='$row[0]' $selected>$row[1] ($row[0])</option>";
		}
	}
	else{
		$opt .="<option selected=selected>Error</option>";
	}

	return $opt;
}
function setComboMobil($kodekendaraan){
    //SET COMBO MOBIL
    global $db;
    $Mobil = new Mobil();

    $result=$Mobil->ambilDataForComboBox();
    $opt_mobil="<option value=''>- silahkan pilih kendaraan  -</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ($kodekendaraan == $row['KodeKendaraan'])?"selected":"";
            $opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[Merek] $row[Jenis]</option>";
        }
    }
    else{
        echo("Err :".__LINE__);exit;
    }

    return $opt_mobil;
    //END SET COMBO MOBIL
}


	if ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'sopir/add_body.tpl'));

		$template->assign_vars(array(
		 'BCRUMP'			=>setBcrump($id_page),
		 'JUDUL'			=>'Tambah Data Sopir',
		 'MODE'   			=> 'save',
         'MOBIL1'            => setComboMobil(""),
         'MOBIL2'            => setComboMobil(""),
		 'MOBIL3'            => setComboMobil(""),
		 'SUB'    			=> '0',
		 'OPT_CABANG'		=> setDropdownCabang(""),
		 'PESAN'			=> $pesan,
		 'BGCOLOR_PESAN'	=> $bgcolor_pesan,
		 'U_SOPIR_ADD_ACT'	=> append_sid('pengaturan_sopir.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah sopir
		$kode_sopir  		= str_replace(" ","",$HTTP_POST_VARS['kode_sopir']);
		$kode_sopir_old		= str_replace(" ","",$HTTP_POST_VARS['kode_sopir_old']);
        $kode_rolling       = $HTTP_POST_VARS['rolling'];
		$nama   			= $HTTP_POST_VARS['nama'];
		$alamat   			= $HTTP_POST_VARS['alamat'];
		$hp					= $HTTP_POST_VARS['hp'];
		$no_sim				= $HTTP_POST_VARS['no_sim'];
		$no_rek 			= $HTTP_POST_VARS['no_rek'];
		$status_aktif 		= $HTTP_POST_VARS['aktif'];
		$kodecabang			= $HTTP_POST_VARS['kodecabang'];
        $mobil1             = $HTTP_POST_VARS['mobil1'];
        $mobil2             = $HTTP_POST_VARS['mobil2'];
		$mobil3             = $HTTP_POST_VARS['mobil3'];
		$isReguler			= $HTTP_POST_VARS['isReguler'];

    $terjadi_error=false;
		
		if($Sopir->periksaDuplikasi($kode_sopir) && $kode_sopir!=$kode_sopir_old){
			$pesan="<font color='white' size=3>Kode sopir yang dimasukkan sudah terdaftar dalam sistem!</font>";
			$bgcolor_pesan="red";
			$terjadi_error=true;
		}
		else{
			
			if($submode==0){
				$judul="Tambah Data Sopir";

				$sql = "INSERT INTO tbl_md_sopir (
    						KodeSopir, Nama, HP, Alamat, NoSIM, Norek,FlagAktif,IsReguler,KodeCabang,KodeRolling)
  						VALUES(
    						'$kode_sopir','$nama','$hp','$alamat','$no_sim','$no_rek','$status_aktif','$isReguler','$kodecabang','$kode_rolling');";

				if(!$db->sql_query($sql)){
					die("Error : ".__LINE__);
				}

				$sql_mobil1 = "UPDATE tbl_md_kendaraan SET KodeSopir1 = '$kode_sopir' WHERE KodeKendaraan = '$mobil1';";
				if(!$db->sql_query($sql_mobil1)){
					die("Error : ".__LINE__);
				}

				$sql_mobil2 = "UPDATE tbl_md_kendaraan SET KodeSopir2 = '$kode_sopir' WHERE KodeKendaraan = '$mobil2';";
				if(!$db->sql_query($sql_mobil2)){
					die("Error : ".__LINE__);
				}

				$sql_mobil3 = "UPDATE tbl_md_kendaraan SET KodeSopir3 = '$kode_sopir' WHERE KodeKendaraan = '$mobil3';";
				if(!$db->sql_query($sql_mobil3)){
					die("Error : ".__LINE__);
				}


				redirect(append_sid('pengaturan_sopir.'.$phpEx.'?mode=add&pesan=1',true));
			}
			else{
				
				$judul="Ubah Data Sopir";

				$sql = "UPDATE tbl_md_sopir SET
    						KodeSopir='$kode_sopir', Nama='$nama', HP='$hp',KodeRolling='$kode_rolling',
    						Alamat='$alamat', NoSIM='$no_sim', Norek='$no_rek',FlagAktif='$status_aktif',IsReguler='$isReguler',KodeCabang='$kodecabang',Mobil1='$mobil1',Mobil2='$mobil2',Mobil3='$mobil3'
						WHERE KodeSopir='$kode_sopir_old';";

				if($db->sql_query($sql)){
					$pesan="<font color='green' size=3>Data Berhasil Diubah!</font>";
					$bgcolor_pesan="98e46f";
				}

				$sql_mobil1 = "UPDATE tbl_md_kendaraan SET KodeSopir1 = '$kode_sopir_old' WHERE KodeKendaraan = '$mobil1';";
				if(!$db->sql_query($sql_mobil1)){
					die("Error : ".__LINE__);
				}

				$sql_mobil2 = "UPDATE tbl_md_kendaraan SET KodeSopir2 = '$kode_sopir_old' WHERE KodeKendaraan = '$mobil2';";
				if(!$db->sql_query($sql_mobil2)){
					die("Error : ".__LINE__);
				}

				$sql_mobil3 = "UPDATE tbl_md_kendaraan SET KodeSopir3 = '$kode_sopir_old' WHERE KodeKendaraan = '$mobil3';";
				if(!$db->sql_query($sql_mobil3)){
					die("Error : ".__LINE__);
				}
			}
			
		}
		
		$temp_var_aktif="status_aktif_".$status_aktif;
		$$temp_var_aktif="selected";

		$temp_var_reguler="reguler_".$isReguler;
		$$temp_var_reguler="selected";

		$template->set_filenames(array('body' => 'sopir/add_body.tpl'));
		$template->assign_vars(array(
			 'BCRUMP'		    =>setBcrump($id_page),
			 'JUDUL'		    =>$judul,
			 'MODE'   	        => 'save',
			 'SUB'    	        => $submode,
			 'OPT_CABANG'		=> setDropdownCabang($kodecabang),
			 'KODE_SOPIR_OLD'   => $kode_sopir_old,
			 'KODE_SOPIR'    	=> $kode_sopir,
             'ROLLING'          => $kode_rolling,
			 'NAMA'    	        => $nama,
			 'ALAMAT'           => $alamat,
             'MOBIL1'           => setComboMobil($mobil1),
             'MOBIL2'           => setComboMobil($mobil2),
			 'MOBIL3'           => setComboMobil($mobil3),
			 'HP'				=> $hp,
			 'NO_SIM'		    => $no_sim,
			 'NO_REK'		    => $no_rek,
			 'AKTIF_1'	        => $status_aktif_1,
			 'AKTIF_0'	        => $status_aktif_0,
			 'REGULER_0'        => $reguler_0,
			 'REGULER_1'        => $reguler_1,
			 'PESAN'		    => $pesan,
			 'BGCOLOR_PESAN'    => $bgcolor_pesan,
			 'U_SOPIR_ADD_ACT'  =>append_sid('pengaturan_sopir.'.$phpEx)
			)
		);
	
	} 
	else if ($mode=='edit'){
		// edit
		
		$id = $HTTP_GET_VARS['id'];
		
		$row=$Sopir->ambilDataDetail($id);
		$temp_var_aktif="status_aktif_".$row['FlagAktif'];
		$$temp_var_aktif="selected";

		$temp_var_reguler="status_reguler_".$row['IsReguler'];
		$$temp_var_reguler="selected";

		$sql = "SELECT KodeKendaraan FROM tbl_md_kendaraan WHERE KodeSopir1 = '".$row['KodeSopir']."'";
		if ($result = $db->sql_query($sql)){
			$baris = $db->sql_fetchrow($result);
			$mobil1 = $baris[0];
		}

		$sql = "SELECT KodeKendaraan FROM tbl_md_kendaraan WHERE KodeSopir2 = '".$row['KodeSopir']."'";
		if ($result = $db->sql_query($sql)){
			$baris = $db->sql_fetchrow($result);
			$mobil2 = $baris[0];
		}

		$sql = "SELECT KodeKendaraan FROM tbl_md_kendaraan WHERE KodeSopir3 = '".$row['KodeSopir']."'";
		if ($result = $db->sql_query($sql)){
			$baris = $db->sql_fetchrow($result);
			$mobil3 = $baris[0];
		}

		$template->set_filenames(array('body' => 'sopir/add_body.tpl'));

    $template->assign_vars(array(
			 'BCRUMP'			=>setBcrump($id_page),
			 'JUDUL'			=>'Ubah Data Sopir',
			 'MODE'   			=> 'save',
			 'SUB'    			=> '1',
			 'OPT_CABANG'		=> setDropdownCabang($row['KodeCabang']),
			 'KODE_SOPIR_OLD'	=> $row['KodeSopir'],
			 'KODE_SOPIR'    	=> $row['KodeSopir'],
             'ROLLING'          => $row['KodeRolling'],
			 'NAMA'    			=> $row['Nama'],
			 'ALAMAT'   		=> $row['Alamat'],
			 'HP'				=> $row['HP'],
             'MOBIL1'           => setComboMobil($mobil1),
             'MOBIL2'           => setComboMobil($mobil2),
			 'MOBIL3'           => setComboMobil($mobil3),
			 'NO_SIM'			=> $row['NoSIM'],
			 'NO_REK'			=> $row['Norek'],
			 'AKTIF_1'			=> $status_aktif_1,
			 'AKTIF_0'			=> $status_aktif_0,
			 'REGULER_1'		=> $status_reguler_1,
			 'REGULER_0'		=> $status_reguler_0,
			 'U_SOPIR_ADD_ACT'	=>append_sid('pengaturan_sopir.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='delete'){
		// aksi hapus sopir
		$list_sopir = str_replace("\'","'",$HTTP_GET_VARS['list_sopir']);
		//echo($list_sopir. " asli :".$HTTP_GET_VARS['list_sopir']);
		$Sopir->hapus($list_sopir);
		
		exit;
	} 
	else if ($mode=='ubahstatus'){
		// aksi hapus jadwal
		$kode_sopir = str_replace("\'","'",$HTTP_GET_VARS['kode_sopir']);
	
		$Sopir->ubahStatusAktif($kode_sopir);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'sopir/sopir_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?"":
			" WHERE KodeSopir LIKE '%$cari%' 
				OR tbl_md_sopir.Nama LIKE '%$cari%'
				OR tbl_md_sopir.Alamat LIKE '%$cari%'
				OR HP LIKE '%$cari%'
				OR NoSIM LIKE '%$cari%'";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"KodeSopir","tbl_md_sopir","&cari=$cari",$kondisi,"pengaturan_sopir.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================

		$sql =
			"SELECT KodeSopir, tbl_md_sopir.Nama AS NamaSopir, HP, tbl_md_sopir.Alamat, NoSIM, Norek, IsReguler, KodeRolling, 
					tbl_md_sopir.FlagAktif, tbl_md_sopir.KodeCabang, tbl_md_cabang.Nama AS Cabang,
					(SELECT GROUP_CONCAT(NoPolisi) FROM tbl_md_kendaraan WHERE KodeSopir1 = KodeSopir AND IsReguler = 1) AS MOBIL1,
					(SELECT GROUP_CONCAT(NoPolisi) FROM tbl_md_kendaraan WHERE KodeSopir2 = KodeSopir AND IsReguler = 1) AS MOBIL2,
					(SELECT GROUP_CONCAT(NoPolisi) FROM tbl_md_kendaraan WHERE KodeSopir3 = KodeSopir AND IsReguler = 1) AS MOBIL3
			 FROM tbl_md_sopir
			 LEFT JOIN tbl_md_cabang ON tbl_md_sopir.KodeCabang = tbl_md_cabang.KodeCabang
			 $kondisi
			 ORDER BY tbl_md_sopir.Nama LIMIT $idx_awal_record,$VIEW_PER_PAGE";

		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				if($row['FlagAktif']){
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeSopir]\")'>Aktif</a>";
				}
				else{
					$odd	= "red";
					$status="<a href='' onClick='return ubahStatus(\"$row[KodeSopir]\")'>Tidak Aktif</a>";
				}

			    if($row['IsReguler'] == 1){
					$reg = "REGULER";
				}else{
					$reg = "BACKUP";
				}

				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_sopir.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";

                $template->
					assign_block_vars(
						'ROW',
						array(
							'odd'           =>$odd,
							'check'         =>$check,
							'no'            =>$i,
							'nama'          =>$row['NamaSopir'],
							'no_rek'        =>$row['Norek'],
							'kode_sopir'    =>$row['KodeSopir'],
                            'kode_rolling'  =>$row['KodeRolling'],
							'alamat'        =>$row['Alamat'],
							'hp'            =>$row['HP'],
							'no_sim'        =>$row['NoSIM'],
							'Cabang'        =>$row['Cabang'],
                            'mobil'         =>$row['MOBIL1']."<BR>".$row['MOBIL2']."<BR>".$row['MOBIL3'],
							'REG'           =>$reg,
							'aktif'	        =>$status,
							'action'        =>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load sopir',__FILE__,__LINE__,$sql);
			//echo("Error :".__LINE__);exit;
			die(mysql_error());
		} 
		
		$page_title	= "Pengaturan Sopir";

    $template->assign_vars(array(
			'BCRUMP'    		=>setBcrump($id_page),
			'U_SOPIR_ADD'		=> append_sid('pengaturan_sopir.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_sopir.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'			=> $no_data,
			'PAGING'			=> $paging,
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>