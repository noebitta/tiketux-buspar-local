<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
//$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect(append_sid('index.'.$phpEx),true); 
}

//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Reservasi	= new Reservasi();
//MODE ACTION=======================
switch($mode){
	case "gunakan":
		$id  = $HTTP_POST_VARS['id'];
		$Reservasi->updateGunakanWaitingList($id,$userdata['nama']);
		exit;
	
	case "hapus":
		$id  = $HTTP_POST_VARS['id'];
		$Reservasi->updateBatalWaitingList($id,$userdata['nama']);
		exit;
}

//==MODE EXPLORE===================

$tgl_berangkat  = $HTTP_GET_VARS['tglberangkat'];
$kode_jadwal  	= $HTTP_GET_VARS['kodejadwal'];

$result	= $Reservasi->ambilAllWaitingList($tgl_berangkat,$kode_jadwal,"ORDER BY IsDigunakan,IsBatal,Id");

$i = 1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	$logs	= explode("#",$row['Log']);
	$remark="";
	
	for($idx=0;$idx<count($logs);$idx++){
		$log		= explode("|",$logs[$idx]);
		$remark	.=$log[0]!=""?$log[0]." oleh <b>".$log[1]."</b> pada ".$log[2]."<br>":"";
	}
	
	$remark	= substr($remark,0,-4);
	
	$act	= "<a href='' onClick=\"gunakanData('$row[Id]','$row[Nama]','$row[Telp]','$row[Keterangan]');return false;\">Gunakan</a> | <a href='' onClick=\"hapusData('$row[Id]');return false;\">Hapus</a>";
	
	if($row['IsDigunakan']){
		$odd="green";
		$act="";
	}
	elseif($row['IsBatal']){
		$odd="red";
		$act="";	
	}
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'nama'=>$row['Nama'],
				'telp'=>$row['Telp'],
				'jumlah_pesan'=>$row['JumlahPesan'],
				'keterangan'=>$row['Keterangan'],
				'remark'=>$remark,
				'act'=>$act
			)
		);
	
	$i++;
}

$template->assign_vars(array(	
	'DATA_KEBERANGKATAN'			=> "Tanggal ".FormatMySQLDateToTgl($tgl_berangkat)." Rute $kode_jadwal"
	)
);

// LIST
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->set_filenames(array('body' => 'reservasi.waitinglist.tpl')); 
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>