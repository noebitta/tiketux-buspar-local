<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassSMSGateway.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  die_message("<h3>Anda tidak diperbolehkan mengakses halaman ini!</h3>");
}
//#############################################################################
// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$order			= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$Member	= new Member();

$mode	= $mode==""?"show":$mode;

switch($mode){
	//tambah
	case "show":
		
    $harga_point      = $Member->ambilHargaPoin();

		$template->assign_block_vars('tombolsubmit',array());
		
		$template->assign_vars(array(
		    'KODE_REF'				=> $Member->generateKodeReferensi()."CR",
            'HARGA_POINT'    => number_format($harga_point,0,",",".")
		    )
		);

		$template->set_filenames(array('body' => 'reservasi/reservasi.member.topup.tpl')); 
		
		include($adp_root_path . 'includes/page_header_detail.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
	exit;
	
	case "getdatamember":
		// aksi menambah member
		
		$ponta  		= $HTTP_POST_VARS['ponta'];
		$jumlah_topup		= $HTTP_POST_VARS['jumlahtopup'];
		
		$data_member		= $Member->ambilDataByKondisi("NoSeriKartu='$ponta'");
		
		if($data_member['IdMember']==""){
			echo(0);exit;
		}

        $harga_point      = $Member->ambilHargaPoin();

        $jumlah_poin      = $jumlah_topup>$harga_point?$jumlah_topup/$harga_point:0;

		$return_val	=
			"nama.innerHTML='".$data_member['Nama']."';
			idmember.innerHTML='".$data_member['IdMember']."';
			hp.innerHTML='".$data_member['Handphone']."';
			hp2.innerHTML='".$data_member['Telp']."';
			email.innerHTML='".$data_member['Email']."';
			jeniskelamin.innerHTML='".($data_member['JenisKelamin']==0?"Laki-laki":"Perempuan")."';
			kota.innerHTML='".$data_member['Kota']."';
			pekerjaan.innerHTML='".$data_member['Pekerjaan']."';
			tglberlaku.innerHTML='".dateparse(FormatMySQLDateToTgl($data_member['ExpiredDate']))."';
			jumlahtopupshow.innerHTML='Rp.".number_format($jumlah_topup,0,",",".")." (".number_format($jumlah_poin,0,",",".")." Poin)"."';
			saldo.innerHTML='".number_format($data_member['SaldoDeposit'],0,",",".")."';";
		
		echo($return_val);
	exit;

    case "kirimtoken":

    $kode_ref		  = $HTTP_POST_VARS['koderef'];
    $id_member	  = $HTTP_POST_VARS['idmember'];
    $jumlah_topup	= $HTTP_POST_VARS['jumlahtopup'];

    $harga_point      = $Member->ambilHargaPoin();

    $jumlah_poin      = $jumlah_topup>$harga_point?$jumlah_topup/$harga_point:0;

    //periksa kode token
    $kode_token	= rand(100000,999999);

    if($Member->topUpMember($id_member,$kode_ref,$jumlah_topup,
      $jumlah_poin,$userdata['user_id'],$userdata['nama'],$kode_token)){
      //berhasil menambahkan data

      $data_member = $Member->ambilData($id_member);
      $hp = $data_member["Handphone"];

      //KIRIM TOKEN ke Pelanggan
      $SMSGateway = new SMSGateway();
      $isi_pesan  = "Tn/Ny ".strtoupper(substr($data_member['Nama'],0,1)).substr($data_member['Nama'],1,14).", anda akan melakukan pembelian poin Day Trans Addict Sebesar Rp.".number_format($jumlah_topup,0,",",".").". Berikan kode ini pada petugas kami:".$kode_token;
      $SMSGateway->sendSMS($hp,$isi_pesan);

      echo(99);

    }
    else{
      echo(0);
    }

    exit;

	case "topup":
		
		$kode_ref	= $HTTP_POST_VARS['koderef'];
		$id_member	= $HTTP_POST_VARS['idmember'];
		$kode_token	= $HTTP_POST_VARS['kodetoken'];
		
		$data_topup = $Member->ambilDetailLogTopUp($kode_ref);

		if($data_topup["ID"]==""){
          echo("status='ERROR';err=2;");
          exit;
        }

        if($data_topup["KodeToken"]!=$kode_token){
          echo("status='ERROR';err=1;");
          exit;
        }

        //MEMERIKSA APAKAH KODE REF SUDAH DIPROSES TOP UPNYA
        if($Member->isSudahProsesKodeRef($kode_ref)){
          echo("status='ERROR';err=3;");
          exit;
        }

		if($Member->verifikasiTopUp($id_member,$kode_ref,$data_topup["JumlahRupiah"], $data_topup["JumlahPoin"],$data_topup["PetugasTopUp"],$data_topup["NamaPetugasTopUp"], $kode_token)){

		     // update kode_cabang
              $sql = "UPDATE tbl_member_deposit_topup_log 
                      SET KodeCabang = '".$userdata['KodeCabang']."' 
                      WHERE KodeReferensi = '$kode_ref' AND IsVerified = 1";
              if(!$db->sql_query($sql)){
                  echo("status='ERROR';err=4");
              }

              //berhasil menambahkan data
              $data_member = $Member->ambilData($id_member);
              $hp = $data_member["Handphone"];

              //KIRIM TOKEN ke Pelanggan
              $SMSGateway = new SMSGateway();
              $isi_pesan  = "Tn/Ny ".strtoupper(substr($data_member['Nama'],0,1)).substr($data_member['Nama'],1,14).", Top Up Berhasil! Poin anda berjumlah: ".number_format($data_member["SaldoDeposit"],0,",",".")." Poin";
              $SMSGateway->sendSMS($hp,$isi_pesan);

              echo("status='OK';");
			
		}
		else{
              echo("status='ERROR';err=9;");
            exit;
		}
		
	exit;
	
	case "cetak":
		
		include($adp_root_path . 'ClassReservasi.php');
		
		$Reservasi = new Reservasi();
		
		$kode_ref		= $HTTP_GET_VARS['kodereferensi'];
		
		$data_log	= $Member->ambilLogTransaksiTopup($kode_ref);
		
		if($data_log[0]==""){
			echo("Data tidak ada");exit;
		}
		
		$data_perusahaan = $Reservasi->ambilDataPerusahaan();
		
		$template->set_filenames(array('body' => 'reservasi/reservasi.member.topup.struk.tpl')); 
		
		$template->assign_vars(array(
			'NAMA_PERUSAHAAN'	=> $data_perusahaan['NamaPerusahaan'],
			'KODE_REFERENSI'	=> $data_log['KodeReferensi'],
			'WAKTU_TRANSAKSI'	=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_log['WaktuTransaksi'])),
			'KODE_MEMBER'			=> $data_log['IdMember'],
			'NAMA_MEMBER'			=> $data_log['Nama'],
			'JUMLAH_RUPIAH'		=> number_format($data_log['JumlahRupiah'],0,",","."),
			'JUMLAH_TIKET'		=> number_format($data_log['JumlahPoin'],0,",","."),
			'SALDO'						=> number_format($data_log['SaldoDeposit'],0,",","."),
			'CSO'							=> $data_log['NamaPetugasTopUp']
		 )
		);
		
		
		$template->pparse('body');
	exit;
}

?>