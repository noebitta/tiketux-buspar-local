<?php
//
// CETAK TIKET UNTUK LINUX
//
// STANDARD

define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassPelanggan.php');
include($adp_root_path . 'ClassAsuransi.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLogSms.php');
include($adp_root_path . 'ClassVoucher.php');

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];
$kode_booking		= str_replace("\'","",$HTTP_GET_VARS['kode_booking']);		
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);		
$cetak_tiket		= $HTTP_GET_VARS['cetak_tiket'];		
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];
$kode_voucher		= $HTTP_GET_VARS['kode_voucher'];	
$cso						= $userdata['nama'];

$Reservasi= new Reservasi();
$User			= new User();
$Jurusan	= new Jurusan();
$Voucher	= new Voucher();

$template->set_filenames(array('body' => 'tiket.tpl')); 

//MEMERIKSA JIKA KODE TIKET KOSONG, MAKA AKAN DICARI DATA BERDASARKAN KODE BOOKING
if($cetak_tiket!=1){
	//mengambil data berdasarkan kode booking dan dapat mencetak tiket lebih dari 1 tiket
	
	if($jenis_pembayaran!=3){
		//PEMBAYARAN TIDAK MENGGUNAKAN VOUCHER
		$result	= $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
	}
	else{
		$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
	}
	$keterangan_duplikat	= "";
}
else{
	//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
	$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
	$keterangan_duplikat	= "**DUPLIKAT**<br>";
}

if($result){
				
	$i=0;
	
	$list_no_tiket		="";
	$list_kode_booking="";
	
	$sudah_diperiksa			= false;
	
	//MENGAMBIL JUMLAH TIKET YANG DIPESAN
	$jum_pesanan	= @mysql_num_rows($result);
	
	$data_sms			= array();
	$list_no_telp	= array();
	
	while ($row = $db->sql_fetchrow($result)){
		
		$array_jurusan	= $Jurusan->ambilNamaJurusanByIdJurusan($row['IdJurusan']);
		
		$no_tiket					= $row['NoTiket'];
		$kode_booking			= $row['KodeBooking'];
		$id_member				= $row['IdMember'];
		$nama							= $row['Nama'];
		$alamat						= $row['Alamat'];
		$telp							= $row['Telp'];
		$tanggal					= $row['TglBerangkat'];
		$jam							= $row['JamBerangkat'];
		$asal							= $array_jurusan['Asal'];
		$tujuan						= $array_jurusan['Tujuan'];
		$id_jurusan				= $row['IdJurusan'];
		$harga_tiket			= $row['HargaTiket'];
		$discount					= $row['Discount'];
		$total_bayar			= $row['Total'];
		$bayar						= $row['Total'];
		$nomor_kursi			= $row['NomorKursi'];
		$cetak_tiket			= $row['CetakTiket'];
		$jenis_pembayaran	= ($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
		$kode_jadwal			= $row['KodeJadwal'];
		$jenis_penumpang	= $row['JenisPenumpang'];
		$waktu_cetak_tiket= dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket']));
		
		//VERIFY VOUCHER
		if($kode_voucher!=""){
			if(!$Voucher->verifyVoucher($kode_voucher,$tgl_berangkat,$id_jurusan)){
				echo("INVALID VOUCHER");
				exit;
			}
		}
		
		if($cetak_tiket!=1){
			if(!$sudah_diperiksa){
				if($id_member==''){
					//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
					$Pelanggan	= new Pelanggan();
					
					if($Pelanggan->periksaDuplikasi($telp)){
						//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN
						
						$Pelanggan->ubah(
							$telp,$telp,
							$telp, $telp ,$nama,
							"", $tanggal,
							$userdata['user_id'], $kode_jadwal);
					}
					else{
						//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
						$Pelanggan->tambah(
							$telp, $telp ,$nama,
							$alamat, $tanggal, $tanggal,
							$userdata['user_id'], $kode_jadwal,1,
							0);
					}
					
					$sudah_diperiksa	= true;
				}
				else{
					$Reservasi->updateFrekwensiMember($id_member,$tanggal);
				}
			}
			
			$nama_cso	= $cso;
			
			if($jenis_penumpang=="R"){
				//penumpang return
				$kode_voucher = $Voucher->setVoucherReturn($no_tiket, $id_jurusan, $kode_jadwal, $discount, $userdata['user_id'],1,$config['masa_berlaku_voucher_return']);
			}
			
			$waktu_cetak_tiket=dateparseWithTime(FormatMySQLDateToTglWithTime(date("Y-m-d H:i")));
			
		}
		else{
			$nama_cso	= $User->ambilNamaUser($row['PetugasCetakTiket']);
		}
		
		$cso	= $nama_cso;
		
		$pesanan=(trim($row['pesanan'])==1)?"Pesanan":"Go Show";
			
		switch($jenis_pembayaran){
			case 0:
				$ket_jenis_pembayaran="TUNAI";
			break;
			case 1:
				$ket_jenis_pembayaran="DEBIT CARD";
			break;
			case 2:
				$ket_jenis_pembayaran="KREDIT CARD";
			break;
			case 3:
				if($cetak_tiket==1){
					$data_voucher	= $Voucher->getDataVoucherByNoTiketPulang($no_tiket);
					$kode_voucher	= $data_voucher['KodeVoucher'];
				}
				
				$ket_jenis_pembayaran="VOUCHER $kode_voucher";
			break;
		}
		
		$list_no_tiket			.="'$no_tiket',";
		$list_kode_booking	.="'$kode_booking',";
		
		$data_perusahaan	= $Reservasi->ambilDataPerusahaan();
		
		if($jenis_penumpang!="R"){
			if($jenis_pembayaran!=3){
				$show_harga_tiket	= "Tiket:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>";
			}
			else{
				$show_harga_tiket	= "Tiket:".substr("------------Rp.".number_format(0,0,",","."),-12)."<br>";
				$discount	= 0;
				$bayar		= 0;
			}
		}
		else{
			//CETAK VOUCHER
			/*$show_harga_tiket	=
				"Tk.Pr:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>
				 Tk.Pl:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>";*/
			
			$show_harga_tiket	=
				"<br>";
			
			$show_harga_tiket	= "";
			
			$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);
			
			//$data_tgl_tuslah = $PengaturanUmum->ambilTglTuslah();
			
			/*$pesan_tuslah	=
				"<font size=3>Jika anda menggunakan voucher ini antara
				tgl ".FormatMySQLDateToTgl($data_tgl_tuslah['TGL_MULAI_TUSLAH1'])." s/d tgl ".FormatMySQLDateToTgl($data_tgl_tuslah['TGL_AKHIR_TUSLAH1'])."
				Anda diwajibkan membayar Rp.10.000
				sebagai biaya tuslah</font><br><br><br>";*/
			
			$template->
				assign_block_vars(
					'VOUCHER',
					array(
						'NAMA_PENUMPANG'		=>substr($nama,0,20),
						'JURUSAN_VOUCHER'		=>substr($tujuan,0,20)."-".substr($asal,0,20),
						'KODE_VOUCHER'			=>$data_voucher['KodeVoucher'],
						'EXPIRED_VOUCHER'		=>FormatMySQLDateToTglWithTime($data_voucher['ExpiredDate']),
						'PESAN_TUSLAH_VOUCHER'=>$pesan_tuslah
					)
				);
		}
		
		//PESAN SPONSOR
		
		$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();
		
		//set data array untuk sms
		if(!in_array($telp,$list_no_telp)){
			$list_no_telp[]							= $telp;		
			
			$data_pnp['KodeBooking']		= $kode_booking;
			$data_pnp['Telp']						= $telp;
			$data_pnp['Nama']						= $nama;
			$data_pnp['TipePengiriman']	= 0;
			
			$data_sms[]	= $data_pnp;
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					/*'NAMA_PERUSAHAAN'		=>strtoupper($data_perusahaan[NamaPerusahaan]),*/
					'NO_TIKET'					=>$no_tiket,
					'DUPLIKAT'					=>$keterangan_duplikat,
					'JENIS_BAYAR'				=>$ket_jenis_pembayaran,
					'TGL_BERANGKAT'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
					'JURUSAN'						=>substr($asal,0,20)."-".substr($tujuan,0,20),
					'JAM_BERANGKAT'			=>$jam,
					'NAMA_PENUMPANG'		=>substr($nama,0,20),
					'NOMOR_KURSI'				=>$nomor_kursi,
					'HARGA_TIKET'				=>$show_harga_tiket,
					'DISKON'						=>substr("------------Rp.".number_format($discount,0,",","."),-12),
					'BAYAR'							=>substr("------------Rp.".number_format($bayar,0,",","."),-12),
					'POINT_MEMBER'			=>$show_point_member,
					'CSO'								=>substr($cso,0,20),
					'PESAN'							=>$pesan_sponsor,
					'WAKTU_CETAK'				=>$waktu_cetak_tiket
				)
			);
		
		$i++;
		$list_no_kursi	.= $nomor_kursi .",";
	}
	
	//mengambil jadwal utama
	$sql	=
		"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
		FROM tbl_md_jadwal
		WHERE KodeJadwal='$kode_jadwal';";
	
	if (!$result = $db->sql_query($sql)){
		echo("Err :".__LINE__);
		exit;
	}
	
	$row = $db->sql_fetchrow($result);
	$kode_jadwal_utama	= $row[0];
	
	$sql = 
			"UPDATE tbl_posisi_detail SET StatusBayar=1
			WHERE 
				KodeJadwal='$kode_jadwal_utama' 
				AND TGLBerangkat='$tanggal'
				AND NoTiket IN (".substr($list_no_tiket,0,-1).");";
	
	if(!$result = $db->sql_query($sql)){
		echo("Err :".__LINE__);
		exit;
	}
	
	//mengupate flag cetak tiket
	if($cetak_tiket!=1){
		$list_kode_booking	= substr($list_kode_booking,0,-1);
		$list_kode_booking	= substr($list_no_tiket,0,-1);
		$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_kode_booking,$userdata['KodeCabang']);
		
		//UPDATE PAKAI VOUCHER
		if($jenis_pembayaran==3){
			$signature	= $Voucher->setSignature($kode_voucher,$no_tiket,$id_jurusan,$kode_jadwal,$userdata['user_id']);
			$Voucher->pakaiVoucher($kode_voucher,$tanggal,$no_tiket,$id_jurusan,$kode_jadwal,$userdata['user_id'],$signature);
		}
		
		/*
		//SMS REMINDER
		
		//mengambil asal dan tujuan
		$asal		= substr($asal,0,10);
		$tujuan	= substr($tujuan,0,10);
			
		$list_no_kursi	= substr($list_no_kursi,0,-1);
		
		$LogSMS					= new LogSMS();
		
		for($idx=0;$idx<count($data_sms);$idx++){
			$telp	= $data_sms[$idx]['Telp'];
			 
			if(in_array(substr($telp,0,2),$HEADER_NO_TELP)){
				
				$isi_pesan	= 
					"TERIMA KASIH Sdr/i ".strtoupper(substr($data_sms[$idx]['Nama'],0,10))." TELAH MEMBELI TIKET DAYTRANS UNTUK $asal-$tujuan TGL ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam SEAT NO:$list_no_kursi";
				
				$telepon	= "62".substr($telp,1);
				$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
				//$response	= sendHttpPost($sms_config['url'],$parameter);	
				
				if($response=="00"){
					$LogSMS->tambah(
						$telp, $data_sms[$idx]['Nama'], $data_sms[$idx]['TipePengiriman'], 
						$data_sms[$idx]['KodeBooking'], $isi_pesan);
					
					//UPDATE FLAG SEND SMS REMINDER
					$sql = 
						"UPDATE tbl_reservasi
							SET FlagSendSMSReminder=1, WaktuKirimSMSReminder=NOW()
						WHERE KodeBooking IN('$kode_booking')";
									
					if (!$result = $db->sql_query($sql)){
						die_error("Err: ID_FILE".__LINE__);
					}
				}
			}
		}//end for
		*/
	}
	
} 
else{
	//die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
	echo("Error :".__LINE__);
	exit;
}

$template->pparse('body');	
?>