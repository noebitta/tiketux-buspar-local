<?php
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//require_once dirname(__FILE__) . '/phpGrid_Lite/conf.php';

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$cabang = $HTTP_GET_VARS['cabang'];
$awal   = date('Y-m-d', strtotime($HTTP_GET_VARS['awal']));
$akhir  = date('Y-m-d', strtotime($HTTP_GET_VARS['akhir']));

$periode = $awal.' - '.$akhir;

$objPHPExcel = new PHPExcel();
$objPHPExcel->createSheet();

//HEADER LPOC
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle("HEADER LPOC");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DATE');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'TYPE');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'SOURCE_DOCUMENT');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'REFERRENCE');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'CURRENCY_ID');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

//DISTRIBUTION LPOC
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle("DISTRIBUTION LPOC");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BATCH_ID');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DATE');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'CREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DISTRIBUTION_REFERRENCE');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SEQUENCE_LINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

//DIMENTION LPOC
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()->setTitle("DIMENTION LPOC");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'CREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ASSIGN_ID');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TRX_DIMENSION_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DISTRIBUTION_REFERENCE');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SEQUENCE_LINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

//DIMENTIONCODE LPOC
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(3);
$objPHPExcel->getActiveSheet()->setTitle("DIMENTION CODE LPOC");
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'INDEX_NUMBER');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'LAST_GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('C1', 'GL_NUMBER_GP');
$objPHPExcel->getActiveSheet()->setCellValue('D1', 'COA');
$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DEBIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'CREDIT_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ASSIGN_ID');
$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TRX_DIMENSION_AMOUNT');
$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DISTRIBUTION_REFERENCE');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SEQUENCE_LINE');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

$i              = 1;
$cellNumber     = 2;
$cellNumber2    = 2;
$cellNumber3    = 2;
$cellNumber4    = 2;
$lastGLNumber   = 1;
$seqLine        = 100;
$type           = 'TOLL';
$sourceDocument = 'IMGJ';
$currency       = 'IDR';
$arrayDimensionCode = array('BODY','COUNTER','ROUTE');

//data jadwal
$sql            = "SELECT * FROM tbl_md_cabang WHERE KodeCabang = '$cabang'";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

while($row = $db->sql_fetchrow($result)){
    $listCOA['TOLL']['DEBIT']   = $row['COADisposisiBebanLangsungToll'];
    $listCOA['TOLL']['KREDIT']  = $row['COABebanLangsungToll'];
}

//Biaya Toll
$sql            = "SELECT ba.TglTransaksi, SUM(ba.Jumlah) as Jumlah FROM tbl_biaya_op ba JOIN tbl_spj s ON ba.NoSPJ = s.NoSPJ WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(s.IdJurusan) = '$cabang' AND (s.TglBerangkat BETWEEN '$awal' AND '$akhir') AND ba.FlagJenisBiaya = 1 GROUP BY s.TglBerangkat ORDER BY s.TglBerangkat";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

$dataBiaya = array();

while($row = $db->sql_fetchrow($result)){
    $dataBiaya[$row['TglTransaksi']]['TOLL']['TOTAL'] = $row['Jumlah'];
}

//ambil data jadwal per tanggal
$sql            = "SELECT ba.TglTransaksi,s.KodeJadwal FROM tbl_biaya_op ba JOIN tbl_spj s ON ba.NoSPJ = s.NoSPJ WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(s.IdJurusan) = '$cabang' AND (s.TglBerangkat BETWEEN '$awal' AND '$akhir') AND ba.FlagJenisBiaya = 1 GROUP BY CONCAT(s.TglBerangkat,s.KodeJadwal) ORDER BY s.TglBerangkat";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

$dataJadwal = array();

while($row = $db->sql_fetchrow($result)){
    $dataJadwal[$row['TglTransaksi']][] = $row['KodeJadwal'];
}

//biaya toll per kendaraan
$sql            = "SELECT ba.TglTransaksi,s.KodeJadwal, SUM(ba.Jumlah) as Jumlah,f_kendaraan_ambil_nopol_by_kode(s.NoPolisi) as NoPol,s.Driver,ba.NoPolisi FROM tbl_biaya_op ba JOIN tbl_spj s ON ba.NoSPJ = s.NoSPJ WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(s.IdJurusan) = '$cabang' AND (s.TglBerangkat BETWEEN '$awal' AND '$akhir') AND ba.FlagJenisBiaya = 1 GROUP BY CONCAT(s.TglBerangkat,s.KodeJadwal) ORDER BY s.TglBerangkat";

if(!$result = $db->sql_query($sql)){
    die_error('ERROR : '.__LINE__);
}

//$dataBiaya = array();

while($row = $db->sql_fetchrow($result)){
    $dataBiaya[$row['TglTransaksi']]['TOLL']['PERJADWAL'][$row['KodeJadwal']]['NOPOL']    = $row['NoPol'];
    $dataBiaya[$row['TglTransaksi']]['TOLL']['PERJADWAL'][$row['KodeJadwal']]['BODY']     = $row['NoPolisi'];
    $dataBiaya[$row['TglTransaksi']]['TOLL']['PERJADWAL'][$row['KodeJadwal']]['DRIVER']   = $row['Driver'];
    $dataBiaya[$row['TglTransaksi']]['TOLL']['PERJADWAL'][$row['KodeJadwal']]['TOTAL']    = number_format($row['Jumlah'],2,'.','');
}

//echo json_encode($dataBiaya);

while($awal <= $akhir){
    //HEADER LPOC
    $referrence = 'By '.$type.' '.$cabang.' '.date('d/m/y', strtotime($awal));
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber, $cabang.'-'.$type.date('dm',strtotime($awal)));
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber, $i);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber, $lastGLNumber);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber, $i);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber, date('m/d/y',strtotime($awal)));
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber, 0);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber, $sourceDocument);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber, $referrence);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber, $currency);

    //DISTRIBUTION LPOC
    foreach($listCOA[$type] as $key => $coa){
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber2, $cabang.'-'.$type.date('dm',strtotime($awal)));
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber2, $i);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber2, $lastGLNumber);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber2, $i);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber2, date('m/d/y',strtotime($awal)));
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber2, $coa);
        switch($key){
            case 'DEBIT':
                $debit  = 0;
                $kredit = ($dataBiaya[$awal]['TOLL']['TOTAL'])?$dataBiaya[$awal]['TOLL']['TOTAL']:0;
                break;
            case 'KREDIT':
                $debit  = ($dataBiaya[$awal]['TOLL']['TOTAL'])?$dataBiaya[$awal]['TOLL']['TOTAL']:0;
                $kredit = 0;
                break;
        }
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber2, $debit);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber2, $kredit);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber2, $referrence);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$cellNumber2, $seqLine);

        //DIMENTION
        if($key == 'KREDIT'){
            if($dataJadwal[$awal]){
                $assignId2 = 1;
                foreach ($arrayDimensionCode as $dimensionCodeType) {
                    $assignId       = 1;
                    foreach ($dataJadwal[$awal] as $jadwal) {
                        if($dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL'] > 0){
                            switch($dimensionCodeType){
                                case 'BODY'     :
                                    $trxDimensionAlphaNumeric   = $dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['BODY'];
                                    break;
                                case 'COUNTER'  :
                                    $trxDimensionAlphaNumeric   = $cabang;
                                    break;
                                case 'ROUTE'    :
                                    $trxDimensionAlphaNumeric   = substr($jadwal,0,7);
                                    break;

                            }

                            if($dimensionCodeType == 'BODY'){
                                $referrence = 'By.'.$type.' '.$dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['NOPOL'].'/'.$dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['DRIVER'].'/'.$cabang;
                                $objPHPExcel->setActiveSheetIndex(2);
                                $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber3, $i);
                                $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber3, $lastGLNumber);
                                $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber3, $i);
                                $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber3, $coa);
                                $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber3, $debit);
                                $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber3, $kredit);
                                $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber3, $assignId);
                                $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber3, $dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']?$dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']:0);
                                $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber3, $referrence);
                                $objPHPExcel->getActiveSheet()->setCellValue('J'.$cellNumber3, $seqLine);

                                $cellNumber3++;
                                $assignId++;
                            }

                            $referrence = 'By.'.$type.' '.$dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['NOPOL'].'/'.$dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['DRIVER'].'/'.$cabang;
                            $objPHPExcel->setActiveSheetIndex(3);
                            $objPHPExcel->getActiveSheet()->setCellValue('A'.$cellNumber4, $i);
                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$cellNumber4, $lastGLNumber);
                            $objPHPExcel->getActiveSheet()->setCellValue('C'.$cellNumber4, $i);
                            $objPHPExcel->getActiveSheet()->setCellValue('D'.$cellNumber4, $coa);
                            $objPHPExcel->getActiveSheet()->setCellValue('E'.$cellNumber4, $debit);
                            $objPHPExcel->getActiveSheet()->setCellValue('F'.$cellNumber4, $kredit);
                            $objPHPExcel->getActiveSheet()->setCellValue('G'.$cellNumber4, $assignId2);
                            $objPHPExcel->getActiveSheet()->setCellValue('H'.$cellNumber4, $dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']?$dataBiaya[$awal][$type]['PERJADWAL'][$jadwal]['TOTAL']:0);
                            $objPHPExcel->getActiveSheet()->setCellValue('I'.$cellNumber4, $referrence);
                            $objPHPExcel->getActiveSheet()->setCellValue('J'.$cellNumber4, $seqLine);

                            $cellNumber4++;
                            $assignId2++;
                        }
                    }
                }
            }

        }

        $cellNumber2++;
        $seqLine= $seqLine+100;
    }

    $i++;
    $cellNumber++;
    $awal = date ("Y-m-d", strtotime("+1 day", strtotime($awal)));
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Export Untuk GP Biaya Cabang '.$cabang.' Periode '.$periode.'.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');