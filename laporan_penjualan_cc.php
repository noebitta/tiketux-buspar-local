<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 315;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//################################################################################

$Reservasi  = new Reservasi();
$Cabang     = new Cabang();

$mode           = $HTTP_POST_VARS['mode'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cari           = isset($HTTP_GET_VARS["cari"])? $HTTP_GET_VARS["cari"] : $HTTP_POST_VARS["txt_cari"];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi = ($cari == "")? "" : "AND KodeBooking LIKE '$cari%' 
                                OR NoTiket LIKE '$cari%'
                                OR tbl_reservasi.Nama LIKE '$cari%'";

if($mode == "setdaftarcounter"){
    $tiket = $HTTP_POST_VARS['notiket'];

    $data_tiket = $Reservasi->ambilDataKursi($tiket);

    $counter = "<select name='counter' id='counter'>
                    ".$Cabang->setInterfaceComboCabangByKota("",$data_tiket['KodeCabang'],"")."
                </select>";
    echo $counter;
    exit;
}

if($mode == "setdaftarcso"){
    $tiket = $HTTP_POST_VARS['notiket'];

    $data_tiket = $Reservasi->ambilDataKursi($tiket);

    $sql = "SELECT nama,user_id FROM tbl_user WHERE user_level = $LEVEL_CSO";
    if(!$result = $db->sql_query($sql)){
        die("Error : ".__LINE__);
    }

    $option = "<option value=''> -- silahkan pilih --</option>";

    while ($row = $db->sql_fetchrow($result)){
        $selected = "";
        if($row['user_id'] == $data_tiket['PetugasCetakTiket']){
            $selected = "selected";
        }
        $option .= "<option value='$row[1]' $selected>$row[0]</option>";
    };

    $select_cso = "<select name='cso' id='cso'>$option</select>";

    echo $select_cso;
    exit;
}

if($mode == "update"){
    $tiket      = $HTTP_POST_VARS['tiket'];
    $counter    = $HTTP_POST_VARS['counter'];
    $cso        = $HTTP_POST_VARS['cso'];

    $sql = "UPDATE tbl_reservasi SET KodeCabang = '$counter' WHERE NoTiket = '$tiket'";
    if(!$result = $db->sql_query($sql)){
        echo mysql_error();
        exit;
    }

    if($cso != ""){
        $sql = "UPDATE tbl_reservasi SET PetugasCetakTiket = $cso WHERE NoTiket = '$tiket'";
        if(!$result = $db->sql_query($sql)){
            echo mysql_error();
            exit;
        }
    }

    echo 1;

    exit;
}


$list_cc = "";
$sql_cc = "SELECT user_id FROM tbl_user WHERE user_level = '$LEVEL_CALL_CENTER' ";

if(!$result = $db->sql_query($sql_cc)){
    die("Error : ".__LINE__);
}else{
    while ($row = $db->sql_fetchrow($result)){
        $list_cc .= $row["user_id"].",";
    }
}

$list_cc = rtrim($list_cc,",");

//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"NoTiket","tbl_reservasi","&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&cari=$cari","WHERE FlagBatal != 1 AND CetakTiket = 1 AND tbl_reservasi.PetugasCetakTiket IN ($list_cc)
        AND DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' $kondisi","laporan_penjualan_cc.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================


$sql = "SELECT NoTiket, KodeJadwal, TglBerangkat, WaktuCetakTiket ,NomorKursi, 
              tbl_reservasi.Nama, f_user_get_nama_by_userid(PetugasCetakTiket) AS petugas, 
              f_cabang_get_name_by_kode(tbl_reservasi.KodeCabang) AS counter
        FROM tbl_reservasi
        WHERE FlagBatal != 1 AND CetakTiket = 1 AND tbl_reservasi.PetugasCetakTiket IN ($list_cc)
        AND DATE(WaktuCetakTiket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' $kondisi 
        LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if(!$result = $db->sql_query($sql)){
    die("Error : ".__LINE__);
}

$i = $idx_page*$VIEW_PER_PAGE+1;
while ($row = $db->sql_fetchrow($result)){
    $odd ='odd';
    if (($i % 2)==0){
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'=>$odd,
            'no'=>$i,
            'tiket'=>$row['NoTiket'],
            'cabang'=>$row['counter'],
            'jadwal'=>$row['KodeJadwal'],
            'tanggal'=>date_format(date_create($row['TglBerangkat']),'d-m-Y'),
            'kursi'=>$row['NomorKursi'],
            'nama'=>$row['Nama'],
            'petugas'=>$row['petugas']
        )
    );

    $i++;

};

$template->assign_vars(
    array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('laporan_penjualan_cc.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'TGL_AWAL'			=> $tanggal_mulai,
        'TGL_AKHIR'			=> $tanggal_akhir,
        'PAGING'		    => $paging,
    )
);

$template->set_filenames(array('body' => 'laporan_penjualan_cc_body.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>