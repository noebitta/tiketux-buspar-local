<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 305;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//################################################################################

$Cabang =new Cabang();

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$bulan			= isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];


$bulan	=($bulan!='')?$bulan:date("m");
$tahun	= ($tahun!='')?$tahun:date("Y");

//LIST BULAN
$list_bulan="";

for($idx_bln=1;$idx_bln<=12;$idx_bln++){
	
	$font_size	= 2;
	$font_color='';
	
	if($bulan==$idx_bln){
		$font_size=4;
		$font_color='008609';
	}
	
	//$list_bulan	.="<a href='".append_sid('laporan_omzet_cabang_grafik.php'.$parameter)."'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
	$list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";
}

// LIST
$template->set_filenames(array('body' => 'laporan_omzet/laporan_omzet_body.tpl')); 


//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}
	
$kondisi_cabang	= $userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])?" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]' ":"";
	
//QUERY PENUMPANG
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPenumpang,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet,
		IS_NULL(SUM(Discount),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	$kondisi_cabang
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_penumpang = $db->sql_query($sql)){
	$data_penumpang = $db->sql_fetchrow($result_penumpang);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
//QUERY PAKET
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket,
		IS_NULL(SUM(HargaPaket),0) AS TotalOmzet
	FROM tbl_paket
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1 AND FlagBatal!=1
	$kondisi_cabang
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if ($result_paket = $db->sql_query($sql)){
	$data_paket = $db->sql_fetchrow($result_paket);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

//QUERY BIAYA
$sql=
	"SELECT 
		WEEKDAY(TglTransaksi)+1 AS Hari,DAY(TglTransaksi) AS Tanggal,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE MONTH(TglTransaksi)=$bulan AND YEAR(TglTransaksi)=$tahun
	$kondisi_cabang
	GROUP BY TglTransaksi
	ORDER BY TglTransaksi ";

if ($result_biaya = $db->sql_query($sql)){
	$data_biaya = $db->sql_fetchrow($result_biaya);
} 
else{
	//die_error('Cannot Load laporan_omzet_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}
	
$sum_penumpang						= 0;
$sum_paket								= 0;
$sum_omzet_penumpang			= 0;
$sum_omzet_paket					= 0;
$sum_discount							= 0;
$sum_pendapatan_penumpang	= 0;
$sum_biaya								= 0;
$sum_profit								= 0;

for($idx_tgl=0;$idx_tgl<getMaxDate($bulan,$tahun);$idx_tgl++){
	$odd ='odd';
	
	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl+1 ."-".HariStringShort($idx_str_hari)."";
	
	if($idx_str_hari!=7){
		$font_color	= "000000";
	}
	else{
		$font_color = "ffffff";
		$odd	='red';
	}
	
	
	//OMZET PENUMPANG
	if($data_penumpang['Tanggal']==$idx_tgl+1){
		$total_penumpang				= $data_penumpang['TotalPenumpang']; 
		$total_omzet_penumpang	= $data_penumpang['TotalOmzet']; 
		$total_discount					= $data_penumpang['TotalDiscount'];
		$pendapatan_penumpang		= $total_omzet_penumpang-$total_discount;
		$total_profit						= $pendapatan_penumpang;
		$data_penumpang 				= $db->sql_fetchrow($result_penumpang);
	}
	else{
		$total_penumpang				= 0;
		$total_omzet_penumpang	= 0;
		$total_discount					= 0;
		$pendapatan_penumpang		= 0;
		$total_profit						= 0;
	}
	
	//OMZET PAKET
	if($data_paket['Tanggal']==$idx_tgl+1){
		$total_paket				= $data_paket['TotalPaket']; 
		$total_omzet_paket	= $data_paket['TotalOmzet'];
		$total_profit				+= $total_omzet_paket;
		$data_paket 				= $db->sql_fetchrow($result_paket);
	}
	else{
		$total_paket				= 0;
		$total_omzet_paket	= 0;
	}
	
	//OMZET BIAYA
	if($data_biaya['Tanggal']==$idx_tgl+1){
		$total_biaya		= $data_biaya['TotalBiaya']; 
		$total_profit		-= $total_biaya;
		$data_biaya 		= $db->sql_fetchrow($result_biaya);
	}
	else{
		$total_biaya		= 0;
	}
	
	$sum_penumpang				+= $total_penumpang;
	$sum_paket						+= $total_paket;
	$sum_omzet_penumpang	+= $total_omzet_penumpang;
	$sum_omzet_paket			+= $total_omzet_paket;
	$sum_biaya						+= $total_biaya;
	$sum_discount					+= $total_discount;
	$sum_pendapatan_penumpang	+= $pendapatan_penumpang ;
	$sum_profit						+= $total_profit;
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'font_color'=>$font_color,
				'tgl'=>$tgl_transaksi,
				'total_penumpang'=>number_format($total_penumpang,0,",","."),
				'total_omzet_penumpang'=>number_format($total_omzet_penumpang,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
				'pendapatan_penumpang'=>number_format($pendapatan_penumpang,0,",","."),
				'total_paket'=>number_format($total_paket,0,",","."),
				'total_omzet_paket'=>number_format($total_omzet_paket,0,",","."),
				'total_biaya'=>number_format($total_biaya,0,",","."),
				'total_profit'=>number_format($total_profit,0,",",".")
			)
		);
		
	$temp_hari++;
}
			


//$parameter	= "&sort_by=".$sort_by."&order=".$order;

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$bulan."&p2=".$tahun;
	
$script_cetak_pdf="Start('laporan_omzet_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$page_title	= "Semua Penjualan";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'LIST_BULAN'		=> "| ".$list_bulan,
	'BULAN'					=> $bulan,
	'TAHUN'					=> $tahun,
	'SUM_PENUMPANG'	=>number_format($sum_penumpang,0,",","."),
	'SUM_OMZET_PENUMPANG'=>number_format($sum_omzet_penumpang,0,",","."),
	'SUM_DISCOUNT'	=>number_format($sum_discount,0,",","."),
	'SUM_PENDAPATAN_PENUMPANG'=>number_format($sum_pendapatan_penumpang,0,",","."),
	'SUM_PAKET'			=>number_format($sum_paket,0,",","."),
	'SUM_OMZET_PAKET'=>number_format($sum_omzet_paket,0,",","."),
	'SUM_BIAYA'			=>number_format($sum_biaya,0,",","."),
	'SUM_PROFIT'		=>number_format($sum_profit,0,",","."),
	'U_LAPORAN_OMZET_GRAFIK'=>append_sid('laporan_omzet_grafik.php?bulan='.$bulan.'&tahun='.$tahun),
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>