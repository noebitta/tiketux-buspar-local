<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 309;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 


// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$tahun 			= isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

//QUERY
$sql=
	"SELECT Bulan,JumlahBerangkat,COUNT(DISTINCT(Telp)) AS JumlahPax FROM v_group_book
	GROUP BY JumlahBerangkat,Bulan
	ORDER BY JumlahBerangkat";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}  

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:M1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:M2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Frekwensi Pelanggan Tahun '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Grup');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Jan');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Feb');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Mar');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Apr');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Mei');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Jun');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jul');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Agu');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Sep');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Okt');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Nov');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Des');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

$max_grup	= 0;

//PLOT DATA
while($row = $db->sql_fetchrow($result)){
	
	$data[$row["Bulan"]][$row["JumlahBerangkat"]]	= $row["JumlahPax"];
	
	$max_grup	= $row["JumlahBerangkat"]<=$max_grup?$max_grup:$row["JumlahBerangkat"];
		
}

$idx_row=3;

for($i=1;$i<=$max_grup;$i++){
	
	$idx_row++;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $i);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, number_format($data[1][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, number_format($data[2][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, number_format($data[3][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, number_format($data[4][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, number_format($data[5][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, number_format($data[6][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, number_format($data[7][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, number_format($data[8][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, number_format($data[9][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, number_format($data[10][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, number_format($data[11][$i],0,",","."));
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, number_format($data[12][$i],0,",","."));
}

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($max_grup>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Frekwensi Pelanggan Tahun '.$tahun.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}


?>
