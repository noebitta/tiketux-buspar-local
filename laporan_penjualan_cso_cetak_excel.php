<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 301;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$cari  					= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$sort_by				= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$order					= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"":
	" WHERE (username='$cari'
	  OR nama LIKE '%$cari%' 
		OR telp LIKE '%$cari%'
		OR NRP LIKE '%$cari%')";

$kondisi_cabang	= $userdata['user_level']!=$USER_LEVEL_INDEX["SUPERVISOR"]?"":" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";
		
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"nama":$sort_by;		

//QUERY
$sql=
	"SELECT 
	  user_id,nama,username,nrp,telp,hp,KodeCabang,f_cabang_get_name_by_kode(KodeCabang) AS cabang
	FROM 
	  tbl_user
	$kondisi_cari";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		PetugasCetakTiket,
		IS_NULL(SUM(IF(JenisPembayaran=0,SubTotal,0)),0) AS TotalTiketTunai, 
		IS_NULL(SUM(IF(JenisPembayaran=5,SubTotal,0)),0) AS TotalTiketEDC, 
		IS_NULL(SUM(SubTotal),0) AS TotalUangTiket, 
		IS_NULL(SUM(Discount),0) AS TotalDiscount, 
		IS_NULL(COUNT(NoTiket),0) AS TotalTiket 
	FROM tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		AND (FlagPesanan IS NULL OR FlagPesanan=0) 
		$kondisi_cabang
	GROUP BY PetugasCetakTiket ORDER BY PetugasCetakTiket";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_total[$row['PetugasCetakTiket']]	= $row;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		PetugasPenjual,
		IS_NULL(SUM(IF(JenisPembayaran=0,HargaPaket,0)),0) AS TotalPaketTunai, 
		IS_NULL(SUM(IF(JenisPembayaran=5,HargaPaket,0)),0) AS TotalPaketEDC,
		IS_NULL(SUM(HargaPaket),0) AS TotalUangPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY PetugasPenjual ORDER BY PetugasPenjual";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['PetugasPenjual']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){
	$temp_array[$idx]['user_id']					= $row['user_id'];
	$temp_array[$idx]['nama']							= $row['nama'];
	$temp_array[$idx]['username']					= $row['username'];
	$temp_array[$idx]['nrp']							= $row['nrp'];
	$temp_array[$idx]['telp']							= $row['telp'];
	$temp_array[$idx]['telp']							= $row['telp'];
	$temp_array[$idx]['hp']								= $row['hp'];
	$temp_array[$idx]['KodeCabang']				= $row['KodeCabang'];
	$temp_array[$idx]['cabang']						= $row['cabang'];
	$temp_array[$idx]['TotalTiketTunai']	= $data_total[$row['user_id']]['TotalTiketTunai'];
	$temp_array[$idx]['TotalTiketEDC']	= $data_total[$row['user_id']]['TotalTiketEDC'];
	$temp_array[$idx]['TotalUangTiket']		= $data_total[$row['user_id']]['TotalUangTiket'];
	$temp_array[$idx]['TotalDiscount']		= $data_total[$row['user_id']]['TotalDiscount'];
	$temp_array[$idx]['TotalTiket']				= $data_total[$row['user_id']]['TotalTiket'];
	$temp_array[$idx]['TotalPaketTunai']	= $data_paket_total[$row['user_id']]['TotalPaketTunai'];
	$temp_array[$idx]['TotalPaketEDC']	= $data_paket_total[$row['user_id']]['TotalPaketEDC'];
	$temp_array[$idx]['TotalUangPaket']		= $data_paket_total[$row['user_id']]['TotalUangPaket'];
	$temp_array[$idx]['TotalPaket']				= $data_paket_total[$row['user_id']]['TotalPaket'];
	$temp_array[$idx]['Total']						= $temp_array[$idx]['TotalUangTiket'] + $temp_array[$idx]['TotalUangPaket'] - $temp_array[$idx]['TotalDiscount'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}



//EXPORT KE MS-EXCEL

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Penjualan per CSO per Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'NRP');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jum. Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Penjualan Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Uang Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Jum.Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Penjualan Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Uang Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Tot.Disc');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

$idx=0;
$idx_row=4;
while ($idx<count($temp_array)){
	
	$total=0;
	
	$idx_row_penjualan1	= $idx_row+1;
	$idx_row_penjualan2	= $idx_row+2;
	
	
	//total tiket
	$total_tunai			= $temp_array[$idx]['TotalTiketTunai'];
	$total_edc			= $temp_array[$idx]['TotalTiketEDC'];
	$total_uang_tiket	= $temp_array[$idx]['TotalUangTiket'];
	$total_discount		= $temp_array[$idx]['TotalDiscount'];
	$total_tiket			= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_paket_tunai	= $temp_array[$idx]['TotalPaketTunai'];
	$total_paket_edc	= $temp_array[$idx]['TotalPaketEDC'];
	$total_uang_paket		= $temp_array[$idx]['TotalUangPaket'];
	$total_paket				= $temp_array[$idx]['TotalPaket'];
	
	//total
	$total							= $temp_array[$idx]['Total'];
			
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['nrp']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['cabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $total_tiket);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, "Tunai :".$total_tunai);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row_penjualan1, "EDC :".$total_edc);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $total_uang_tiket);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $total_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, "Tunai:".$total_paket_tunai);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row_penjualan1, "EDC:".$total_paket_edc);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $total_uang_paket);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $total_discount);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $total);
	
	$idx_row +=4;
	$idx++;
}
$temp_idx=$idx_row-1;

//$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K4:K'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, '=SUM(L4:L'.$temp_idx.')');
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Penjualan per CSO per '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
  
?>
