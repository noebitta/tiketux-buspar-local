<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 407;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
/*$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;*/

// LIST
$template->set_filenames(array('body' => 'export_gp/index.tpl')); 
							
$page_title	= "Export GP";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'AWAL'				=> dateD_M_Y(),
	'AKHIR' 			=> dateD_M_Y(),
	//'ACTION_EXPORT'	=> "Start('export_gp_excel.php?sid=".$userdata['session_id']."&periode='+document.getElementById('periode').value);"
	'ACTION_EXPORT_PENJUALAN'	=> "Start('export_gp_penjualan.php?sid=".$userdata['session_id']."&cabang='+document.getElementById('asal').value+'&awal='+document.getElementById('awal').value+'&akhir='+document.getElementById('akhir').value);",
	'ACTION_EXPORT_BIAYA'	=> "Start('export_gp_biaya.php?sid=".$userdata['session_id']."&cabang='+document.getElementById('asal').value+'&awal='+document.getElementById('awal').value+'&akhir='+document.getElementById('akhir').value);"
	)
);

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>