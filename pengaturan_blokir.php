<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 999;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx.'?menu_blokir',true); 
}
//###############################################################################

include($adp_root_path . 'ClassPengaturanUmum.php');

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$PengaturanUmum	= new PengaturanUmum();

	if($mode=='ubah'){
		
		$status_blokir	= $HTTP_GET_VARS['status_blokir'];
			
		//periksa wewenang
		if($PengaturanUmum->ubahStatusBlokir($status_blokir,$userdata['user_id'])){
			echo(1);
		}
		else{
			echo(2);
		}
		exit;
	}
	else {
		// LIST
		$template->set_filenames(array('body' => 'pengaturan_blokir/pengaturan_blokir.tpl')); 
		
		$status_blokir	= $PengaturanUmum->ambilStatusBlokir();
		
		$temp_var	= "status_blokir".$status_blokir;
		
		$$temp_var	= "selected";
		
		$template->assign_vars(array(
			'STATUS_BLOKIR0'    		=> $status_blokir0,
			'STATUS_BLOKIR1'    		=> $status_blokir1
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>