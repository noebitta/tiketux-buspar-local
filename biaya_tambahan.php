<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
// SESSION

$id_page = 221;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$penerima       = isset($HTTP_GET_VARS['penerima'])? $HTTP_GET_VARS['penerima'] : $HTTP_POST_VARS['penerima']; // ubah status jika tidak kosong
$tgl            = isset($HTTP_GET_VARS['tgl'])? $HTTP_GET_VARS['tgl'] : $HTTP_POST_VARS['tgl']; // ubah status jika tidak kosong
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$cabang         = isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];

if($HTTP_POST_VARS["txt_cari"]!=""){
    $cari=$HTTP_POST_VARS["txt_cari"];
}
else{
    $cari=$HTTP_GET_VARS["txt_cari"];
}

$kondisi_cari	=($cari=="")?"WHERE 1 ":
    " WHERE (tbl_biaya_tambahan.KodeCabang LIKE '$cari%'
		OR NamaPencetak LIKE '%$cari%'
		OR NamaPenerima LIKE '%$cari%'
		OR NamaPembuat LIKE '$cari%'
		OR JenisBiaya LIKE '$cari%'
		OR Keterangan LIKE '$cari%'
		OR Jumlah LIKE '$cari%'
		OR NamaReleaser LIKE '%$cari%')";

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$th_otp         = '<th width="100">OTP</th>';
$th_realisasi   = '<th width="100">Realisasi</th>';
$th_action      = '<th width="">Action<th>';

if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"]))){
    $th_otp         = "";
    $th_realisasi   = "";
}
if($userdata['user_level'] == $USER_LEVEL_INDEX["KASIR"] || $userdata['user_level'] == $USER_LEVEL_INDEX["SPV_RESERVASI"] || $userdata['user_level'] ==$USER_LEVEL_INDEX["CSO"] || $userdata['user_level'] == $USER_LEVEL_INDEX["CSO_PAKET"]){
    $th_action      = "";
}

switch($mode){
    case 'release':
        // RELEASE BIAYA
        $petugas_release = $userdata['user_id'];
        $nama_petugas    = $userdata['nama'];
        $KodeBiaya = $HTTP_POST_VARS['KodeBiaya'];
        //$nama_penerima = preg_replace('/\\\\/', '', $penerima);
        //$tgl_buat = preg_replace('/\\\\/', '', $tgl);

        $sql ="UPDATE tbl_biaya_tambahan SET StatusReleas = 1, IdReleaser = $petugas_release, NamaReleaser = '$nama_petugas' WHERE KodeBiaya = $KodeBiaya";

        if (!$db->sql_query($sql)){
            echo("Err:".__LINE__."<br>");
        }

        break;
    case 'KolomPenerima':
        $kategori = $HTTP_GET_VARS['kategori'];
        $opt = setComboSupir();
        if($kategori == 1){
            echo("<td width='200'></td><td width='5'>:</td>
                      <td width='300'>
                      <select id='penerima' name='penerima'>$opt</select>
                      <span id='penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                  </td>");
        }else{
            echo("<td width='200'></td><td width='5'>:</td>
                      <td width='300'>
                      <input type='text' id='penerima' name='penerima'>
                      <span id='penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                  </td>");
        }
        exit;
    case 'InsertData':
        $cabang     = $userdata['KodeCabang'];
        $idpembuat  = $userdata['user_id'];
        $namaPembuat= $userdata['nama'];
        $penerima   = $HTTP_GET_VARS['penerima'];
        $keterangan = $HTTP_GET_VARS['keterangan'];
        $tglbuat    = date('Y-m-d H:i:s');
        $status     = 0;
        $jenis      = $HTTP_GET_VARS['jenis'];
        $mobil      = $HTTP_GET_VARS['mobil'];
        $jurusan    = $HTTP_GET_VARS['jurusan'];
        $i=1;
        do{
            $otp        = generateString(7);
            //JIKA OTP BELUM PERNAH DIGUNAKAN MAKA KELUAR DARI LOOPING
            if(cekDuplikasiOTP($otp) == 0){
                $i=2;
            }
        }while($i<2);

        // AMBIL Kode Biaya Terbesar
        $sql = "SELECT MAX(KodeBiaya) as KodeBiaya FROM tbl_biaya_tambahan";
        if (!$Kode = $db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__."<br>");
            die(mysql_error());
        }
        $result = $db->sql_fetchrow($Kode);
        $KodeBiaya = $result['KodeBiaya']+1;

        $sql = "SELECT *
        FROM tbl_biaya_coa
        WHERE FlagJenis=1
        ORDER BY IdBiayaCOA ASC";

        if ($result = $db->sql_query($sql)){
            $i = 0;

            while ($row = $db->sql_fetchrow($result)){
                $namaBiaya[] = $row['JenisBiaya'];
                $i++;
            }

            $detail = explode(',',$jenis);
            foreach($detail as $k=>$d){
                if($detail[$k] != 0){
                    $sql = "INSERT INTO tbl_biaya_tambahan(
                            KodeCabang,IdPembuat,NamaPembuat,NamaPenerima,Keterangan,
                            JenisBiaya,Jumlah,TglBuat,StatusReleas,KodeKendaraan,OTPCode,KodeJurusan,KodeBiaya)
                            VALUES ('".$cabang."','".$idpembuat."','".$namaPembuat."','".$penerima."','".$keterangan."','".$namaBiaya[$k]."','".$d."','".$tglbuat."','".$status."','".$mobil."','".$otp."','".$jurusan."','".$KodeBiaya."');";

                    global $db;
                    $db->sql_query($sql)or die(mysql_error());
                }
            }
        }

        echo("alert('Biaya Tambahan berhasil dibuat!');window.location.reload();");
        exit;
    case 'realisasi':
        $KodeBiaya = $HTTP_GET_VARS['KodeBiaya'];
        $sql = "SELECT tbl_biaya_tambahan.*, tbl_md_kendaraan.NoPolisi AS Nopol
                FROM tbl_biaya_tambahan
                JOIN tbl_md_kendaraan ON tbl_biaya_tambahan.KodeKendaraan = tbl_md_kendaraan.KodeKendaraan
                WHERE KodeBiaya = $KodeBiaya";
        if(!$result = $db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__."<br>");
            die(mysql_error());
        }else{
            while($row = $db->sql_fetchrow($result)){
                $nopol = $row['Nopol'];
                $sopir = $row['NamaPenerima'];
                $jurusan = $row['KodeJurusan'];
                $keterangan = $row['Keterangan'];

                if($row['Realisasi1'] == 0){
                    $SUBMODE='Pertama';
                    $posisiRealisasi = $row['Jumlah'];
                }else{
                    $SUBMODE='Kedua';
                    $posisiRealisasi = $row['Jumlah'];
                }

                $template->
                assign_block_vars(
                    'ROWJENIS',
                    array(
                        'JENIS' => '<tr>
                                    <td width="200"><u>'.$row['JenisBiaya'].'</u></td><td width="5">:</td>
                                    <td width="300">
                                       <input type="hidden" name="id[]" value="'.$row['id_biaya_tambahan'].'">
                                       <input type="hidden" name="biaya[]" value="'.$posisiRealisasi.'">
                                       <input type="text" name="jumlah[]">
                                    </td>
                                </tr>'
                    )
                );
            }
        }

        $template->assign_vars(array(
                'BCRUMP'    	=>setBcrump($id_page),
                'URL'			=> append_sid('biaya_tambahan.'.$phpEx.'?&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari),
                'SELECT'        => setDropdownCabang(),
                'MOBIL'         => $nopol,
                'SUPIR'         => $sopir,
                'JURUSAN'       => $jurusan,
                'KETERANGAN'    => $keterangan,
                'MODE'          => 'simpanRealisasi',
                'SUBMODE'       => $SUBMODE
            )
        );
        $template->set_filenames(array('body' => 'biaya_tambahan_realisasi.tpl'));
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
    case 'simpanRealisasi':
        $submode    = $HTTP_POST_VARS['submode'];
        $id         = $HTTP_POST_VARS['id'];
        $biaya      = $HTTP_POST_VARS['biaya'];
        $jumlah     = $HTTP_POST_VARS['jumlah'];
        $keterangan = $HTTP_POST_VARS['keterangan'];
        for($a=0;$a<count($jumlah);$a++){
            $field = ($submode == 'Pertama')?$field='Realisasi1':$field='Realisasi2';
            if($jumlah[$a] != null){
                $sql = "UPDATE tbl_biaya_tambahan SET $field = '$jumlah[$a]', Selisih = Jumlah - Realisasi1 - Realisasi2, Keterangan = '$keterangan'
                        WHERE id_biaya_tambahan = '$id[$a]'";
                if(!$db->sql_query($sql)){
                    echo("Err:".__LINE__."<br>");
                    die(mysql_error());
                }
            }
        }

        redirect(append_sid('biaya_tambahan.'.$phpEx).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari);
        exit;
    case 'add':
        $sql = "SELECT *
        FROM tbl_biaya_coa
        WHERE FlagJenis=1
        ORDER BY IdBiayaCOA ASC";

        if ($result = $db->sql_query($sql)){

           while($row = $db->sql_fetchrow($result)){

               $template->
               assign_block_vars(
                   'ROWJENIS',
                   array(
                       'JENIS' => '<tr>
                                    <td width="200"><u>'.$row['JenisBiaya'].'</u></td><td width="5">:</td>
                                    <td width="300">
                                       <input type="hidden" name="jenis[]" value="'.$row['JenisBiaya'].'">
                                       <input type="text" name="jumlah[]">
                                    </td>
                                </tr>'
                   )
               );
           }
        }



        $page_title = "Biaya Tambahan";
        $template->assign_vars(array(
            'BCRUMP'    	=>setBcrump($id_page),
            'URL'			=> append_sid('biaya_tambahan.'.$phpEx),
            'SELECT'        => setDropdownCabang(),
            'MOBIL'         => setComboMobil(""),
            'SUPIR'         => setComboSupir(),
            'JURUSAN'       => setComboJadwal(""),
            'HARI_INI'      => date('j-n-Y'),
            'MODE'          => 'save'
            )
        );
        $template->set_filenames(array('body' => 'biaya_tambahan_add.tpl'));
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
    case 'save':
        $cabang     = $userdata['KodeCabang'];
        $idpembuat  = $userdata['user_id'];
        $namaPembuat= $userdata['nama'];
        $penerima   = $HTTP_POST_VARS['penerima'];
        $keterangan = $HTTP_POST_VARS['ket'];
        $tanggal    = $HTTP_POST_VARS['tanggal'];
        $jam        = date('H:i:s');
        $tglbuat    = date_format(date_create($tanggal." ".$jam),'Y-m-d H:i:s');
        $status     = 0;
        $jenis      = $HTTP_POST_VARS['jenis'];
        $jumlah     = $HTTP_POST_VARS['jumlah'];
        $mobil      = $HTTP_POST_VARS['mobil'];
        $jurusan    = $HTTP_POST_VARS['jurusan'];
        $katpenerima= $HTTP_POST_VARS['katpenerima'];
        if($katpenerima == 1){
            $sql = "SELECT Nama FROM tbl_md_sopir WHERE KodeSopir = '$penerima'";
            if (!$Kode = $db->sql_query($sql)){
                echo("Err:".__LINE__."<br>");
                die(mysql_error());
            }else{
                $row = $db->sql_fetchrow($Kode);
                $kodeSopir = $penerima;
                $penerima  = $row['Nama'];
            }
        }else{
            $kodeSopir = "";
        }
        $i=1;
        do{
            $otp        = generateString(7);
            //JIKA OTP BELUM PERNAH DIGUNAKAN MAKA KELUAR DARI LOOPING
            if(cekDuplikasiOTP($otp) == 0){
                $i=2;
            }
        }while($i<2);

        // AMBIL Kode Biaya Terbesar
        $sql = "SELECT MAX(KodeBiaya) as KodeBiaya FROM tbl_biaya_tambahan";
        if (!$Kode = $db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__."<br>");
            die(mysql_error());
        }
        $result = $db->sql_fetchrow($Kode);
        $KodeBiaya = $result['KodeBiaya']+1;

        for($a=0;$a<count($jumlah);$a++){
            if($jumlah[$a] != null){
                $sql = "INSERT INTO tbl_biaya_tambahan(
                            KodeCabang,IdPembuat,NamaPembuat,NamaPenerima,Keterangan,
                            JenisBiaya,Jumlah,TglBuat,StatusReleas,KodeKendaraan,OTPCode,KodeJurusan,KodeBiaya,KodeSopir)
                        VALUES ('".$cabang."','".$idpembuat."','".$namaPembuat."','".$penerima."','".$keterangan."','".$jenis[$a]."','".$jumlah[$a]."','".$tglbuat."','".$status."','".$mobil."','".$otp."','".$jurusan."','".$KodeBiaya."','".$kodeSopir."');";
                if(!$db->sql_query($sql)){
                    echo("Err:".__LINE__."<br>");
                    die(mysql_error());
                }
            }
        }
    redirect(append_sid('biaya_tambahan.'.$phpEx));
        exit;
    case 'edit':
        $KodeBiaya = $HTTP_GET_VARS['KodeBiaya'];
        $sql = "SELECT tbl_biaya_tambahan.*, tbl_md_kendaraan.NoPolisi AS Nopol
                FROM tbl_biaya_tambahan
                JOIN tbl_md_kendaraan ON tbl_biaya_tambahan.KodeKendaraan = tbl_md_kendaraan.KodeKendaraan
                WHERE KodeBiaya = $KodeBiaya";
        if(!$result = $db->sql_query($sql)){
            echo("Err:".__LINE__."<br>");
            die(mysql_error());
        }else{
            $arrayJenis = array();
            $arrayJumlah = array();
            $arrayId = array();
            while($row = $db->sql_fetchrow($result)){
                $kodekendaraan = $row['KodeKendaraan'];
                $nopol = $row['Nopol'];
                $sopir = $row['NamaPenerima'];
                $jurusan = $row['KodeJurusan'];
                $keterangan = $row['Keterangan'];

                if($row['Realisasi1'] == ""){
                    $SUBMODE='Pertama';
                    $posisiRealisasi = $row['Jumlah'];
                }else{
                    $SUBMODE='Kedua';
                    $posisiRealisasi = $row['Jumlah'];
                }

                array_push($arrayJenis,$row['JenisBiaya']);
                array_push($arrayJumlah,$row['Jumlah']);
                array_push($arrayId,$row['id_biaya_tambahan']);

                if($row['KodeSopir'] == ""){
                    $sasaran = "<td width='200'></td><td width='5'>:</td>
                                      <td width='300'>
                                      <input type='text' id='penerima' name='penerima' value='".$row['NamaPenerima']."'>
                                      <span id='penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                  </td>";
                    $checked = "";
                    $cekdua = "checked";
                }else{
                    $opt = setComboSupir($row['KodeSopir']);
                    $sasaran = "<td width='200'></td><td width='5'>:</td>
                                      <td width='300'>
                                      <select id='penerima' name='penerima'>$opt</select>
                                      <span id='penerima_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
                                  </td>";
                    $checked = "checked";
                    $cekdua = "";
                }
            }
        }

        $sql = "SELECT *
        FROM tbl_biaya_coa
        WHERE FlagJenis=1
        ORDER BY IdBiayaCOA ASC";

        if ($result = $db->sql_query($sql)){

            while($row = $db->sql_fetchrow($result)){

                foreach($arrayJenis as $key => $value){
                    if($value == $row['JenisBiaya']){
                        $Kolom = '<tr>
                                    <td width="200"><u>'.$row['JenisBiaya'].'</u></td><td width="5">:</td>
                                    <td width="300">
                                       <input type="hidden" name="id[]" value="'.$arrayId[$key].'"> 
                                       <input type="hidden" name="jenis[]" value="'.$row['JenisBiaya'].'">
                                       <input type="text" name="jumlah[]" value="'.$arrayJumlah[$key].'">
                                    </td>
                                  </tr>';
                        break;
                    }else{
                        $Kolom = '<tr>
                                    <td width="200"><u>'.$row['JenisBiaya'].'</u></td><td width="5">:</td>
                                    <td width="300">
                                       <input type="hidden" name="id[]"> 
                                       <input type="hidden" name="jenis[]" value="'.$row['JenisBiaya'].'">
                                       <input type="text" name="jumlah[]">
                                    </td>
                                  </tr>';
                    }
                }
                $template->
                assign_block_vars(
                    'ROWJENIS',
                    array(
                        'JENIS' => $Kolom
                    )
                );
            }
        }
        $page_title = "Biaya Tambahan";



        $template->assign_vars(array(
                'BCRUMP'    	=>setBcrump($id_page),
                'URL'			=> append_sid('biaya_tambahan.'.$phpEx).'&KodeBiaya='.$KodeBiaya.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari,
                'SELECT'        => setDropdownCabang(),
                'MOBIL'         => setComboMobil($kodekendaraan),
                'BIAYA'         => setComboJenisBiaya(),
                'SUPIR'         => setComboSupir(),
                'JURUSAN'       => setComboJadwal($jurusan),
                'PENERIMA'      => $sasaran,
                'CEK'           => $checked,
                'CEK2'          => $cekdua,
                'KETERANGAN'    => $keterangan,
                'MODE'          => 'update',
            )
        );

        $template->set_filenames(array('body' => 'biaya_tambahan_edit.tpl'));
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
    case 'update':

        $KodeBiaya = $HTTP_GET_VARS['KodeBiaya'];
        $penerima   = $HTTP_POST_VARS['penerima'];
        $keterangan = $HTTP_POST_VARS['ket'];
        $jenis      = $HTTP_POST_VARS['jenis'];
        $jumlah     = $HTTP_POST_VARS['jumlah'];
        $id_biaya   = $HTTP_POST_VARS['id'];
        $mobil      = $HTTP_POST_VARS['mobil'];
        $jurusan    = $HTTP_POST_VARS['jurusan'];
        $katpenerima= $HTTP_POST_VARS['katpenerima'];

        if($katpenerima == 1){
            $sql = "SELECT Nama FROM tbl_md_sopir WHERE KodeSopir = '$penerima'";
            if (!$Kode = $db->sql_query($sql)){
                echo("Err:".__LINE__."<br>");
                die(mysql_error());
            }else{
                $row = $db->sql_fetchrow($Kode);
                $kodeSopir = $penerima;
                $penerima  = $row['Nama'];
            }
        }else{
            $kodeSopir = "";
        }

        //looping sebanyak jenis biaya yang ada
        for($a=0;$a<count($jenis);$a++){

            // jika jumlah biaya ada dan id biaya ada maka update
            if($jumlah[$a] != null && $id_biaya[$a] != null){
                $sql = "UPDATE tbl_biaya_tambahan
                        SET NamaPenerima = '$penerima', Keterangan = '$keterangan', KodeKendaraan = '$mobil', KodeJurusan = '$jurusan', KodeSopir = '$kodeSopir',
                            JenisBiaya = '$jenis[$a]', Jumlah = $jumlah[$a]
                        WHERE id_biaya_tambahan=$id_biaya[$a]";
                if(!$db->sql_query($sql)){
                    echo("Err:".__LINE__."<br>");
                    die($sql);
                }

            // jika jumlah biaya kosong tapi id biaya ada maka delete
            }else if($jumlah[$a] == null && $id_biaya[$a] != null){
                $sql = "DELETE FROM tbl_biaya_tambahan WHERE id_biaya_tambahan = $id_biaya[$a]";
                if(!$db->sql_query($sql)){
                    echo("Err:".__LINE__."<br>");
                    die($sql);
                }

            // jika jumalah biaya ada tapi id biaya tidak ada maka insert
            }else if($jumlah[$a] != null && $id_biaya[$a] == null){

                // get data detail from kode biaya
                $sql = "SELECT IdPembuat, IdPencetak, IdReleaser, TglBuat, TglCetak, OTPCode 
                        FROM tbl_biaya_tambahan WHERE KodeBiaya = $KodeBiaya LIMIT 1";
                echo $sql;
                if (!$detail_biaya = $db->sql_fetchrow($db->sql_query($sql))){
                    echo("Err:".__LINE__."<br>");
                    die(mysql_error());
                }

                $sql = "INSERT INTO tbl_biaya_tambahan(
                            KodeCabang,IdPembuat,NamaPembuat,NamaPenerima,Keterangan,
                            JenisBiaya,Jumlah,TglBuat,StatusReleas,KodeKendaraan,OTPCode,KodeJurusan,KodeBiaya,KodeSopir)
                        VALUES ('".$userdata['KodeCabang']."','".$userdata['user_id']."','".$userdata['nama']."','".$penerima."','".$keterangan."','".$jenis[$a]."','".$jumlah[$a]."','".$detail_biaya['TglBuat']."',0,'".$mobil."','".$detail_biaya['OTPCode']."','".$jurusan."','".$KodeBiaya."','".$kodeSopir."');";
                if(!$db->sql_query($sql)){
                    echo("Err:".__LINE__."<br>");
                    die(mysql_error());
                }
            }
        }

        redirect(append_sid('biaya_tambahan.'.$phpEx).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari);
        exit;
    case 'hapus':
        $KodeBiaya = $HTTP_GET_VARS['KodeBiaya'];
        $sql = "DELETE FROM tbl_biaya_tambahan WHERE KodeBiaya = $KodeBiaya";
        if(!$result = $db->sql_query($sql)){
            echo("Err:$this->ID_FILE".__LINE__."<br>");
            die(mysql_error());
        }
        redirect(append_sid('biaya_tambahan.'.$phpEx).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir);
}
// QUERY AMBIL BIAYA TAMBAHAN
$kodecabang = ($cabang == "")? $userdata['KodeCabang']:$cabang;

if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"]))) {
    $template->assign_block_vars('show_list_cabang',array());
}

$sql = "SELECT tbl_biaya_tambahan.*, tbl_md_kendaraan.NoPolisi, tbl_md_cabang.Nama,
            GROUP_CONCAT(tbl_biaya_tambahan.id_biaya_tambahan ORDER BY id_biaya_tambahan) AS id_biaya,
            GROUP_CONCAT(tbl_biaya_tambahan.JenisBiaya ORDER BY id_biaya_tambahan) AS jenis,
	        GROUP_CONCAT(tbl_biaya_tambahan.Jumlah ORDER BY id_biaya_tambahan) AS jumlahBiaya,
	        GROUP_CONCAT(tbl_biaya_tambahan.Realisasi1 ORDER BY id_biaya_tambahan) AS Realisasi1,
	        GROUP_CONCAT(tbl_biaya_tambahan.Realisasi2 ORDER BY id_biaya_tambahan) AS Realisasi2,
	        GROUP_CONCAT(tbl_biaya_tambahan.Selisih ORDER BY id_biaya_tambahan) AS Selisih
        FROM tbl_biaya_tambahan
        JOIN tbl_md_kendaraan ON tbl_md_kendaraan.KodeKendaraan=tbl_biaya_tambahan.KodeKendaraan
        JOIN tbl_md_cabang ON tbl_md_cabang.KodeCabang = tbl_biaya_tambahan.KodeCabang
        $kondisi_cari AND DATE(TglBuat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND tbl_biaya_tambahan.KodeCabang = '$kodecabang'
        GROUP BY OTPCode,NamaPenerima,TglBuat
        ORDER BY TglBuat DESC";

if ($result = $db->sql_query($sql)){
    $status = "";


    $icon   = "";
    $target = "";
    $otp    = "";
    $button = "";

    $temp_array = [];

    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){
        // seleksi jika yang login bukan cso maka tampilkan kolom otp dan action realisasi
        if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"]))){
            $otp = '<td align="center">'.$row['OTPCode'].'</td>';
            $action = '<td align="center"><a href="'.append_sid('biaya_tambahan.'.$phpEx.'?mode=edit&KodeBiaya='.$row['KodeBiaya']).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari.'">EDIT</a>|<a href="'.append_sid('biaya_tambahan.'.$phpEx.'?mode=hapus&KodeBiaya='.$row['KodeBiaya']).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari.'">DELETE</a></td>';
            //tampilkan tombol realisasi pada data yang sudah dicetak
            if($row['StatusCetak'] == 1){
                //jika sudah pernah direalisasi sebanyak 2 kali maka tombol realisasi dihilangkan
                if($row['Realisasi1'] == 0 || $row['Realisasi2'] == 0){
                    $realisasi = '<td align="center"><a href="'.append_sid('biaya_tambahan.'.$phpEx.'?mode=realisasi&KodeBiaya='.$row['KodeBiaya']).'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&txt_cari='.$cari.'">Realisasi</a></td>';
                }else{
                    $realisasi = '<td align="center"></td>';
                }
            }else{
                $realisasi = '<td align="center"></td>';
            }
        }

        if($row['StatusCetak'] != 1){
            $onclick= "StartCustomeSize('".append_sid('biaya_tambahan_cetak.'.$phpEx.'?id='.$row['OTPCode'])."','600','650');";
        }else{
            $onclick = "ShowDialogCetak('".$row['OTPCode']."','".$row['OTPCode']."');";
        }

        if($row['TglCetak'] != null){
            $tglCetak = date_format(date_create($row['TglCetak']),'d-m-Y H:i:s');
        }else{
            $tglCetak="-";
        }

        $arrayJenis = explode(",",$row['jenis']);
        $arrayJumlah = explode(",",$row['jumlahBiaya']);
        $arrayRealisasi1 = explode(",",$row['Realisasi1']);
        $arrayRealisasi2 = explode(",",$row['Realisasi2']);
        $arraySelisih = explode(",",$row['Selisih']);
        foreach($arrayJenis as $k=>$jenis){
            $idx = ($i-1);

            $JenB = "<tr><td><div>" . $jenis . "</div></td></tr>";
            $jenisbiaya[$idx] .= $JenB;

            $JumB = "<tr><td><div>" . number_format($arrayJumlah[$k],0,',','.') . "</div></td></tr>";
            $jumlahbiaya[$idx] .= $JumB;

            $JinB = "<tr><td><div>" . number_format((float)$arrayRealisasi1[$k],0,',','.') . "</div></td></tr>";
            $jumlahRealisasi1[$idx] .=$JinB;

            $JunB = "<tr><td><div>" . number_format((float)$arrayRealisasi2[$k],0,',','.') . "</div></td></tr>";
            $jumlahRealisasi2[$idx] .=$JunB;

            $JonB = "<tr><td><div>" . number_format((float)$arraySelisih[$k],0,',','.') . "</div></td></tr>";
            $jumlahSelisih[$idx] .=$JonB;

            /*
            if($arrayRealisasi1[$k] != null && $arrayRealisasi2[$k] == null){
                $sel = "<tr><td><div>" . number_format($arrayJumlah[$k] - $arrayRealisasi1[$k],0,',','.') . "</div></td></tr>";
            }else if($arrayRealisasi1[$k] != null && $arrayRealisasi2[$k] != null){
                $sel = "<tr><td><div>" . number_format($arrayJumlah[$k] - $arrayRealisasi2[$k],0,',','.') . "</div></td></tr>";
            }else{
                $sel = "<tr><td><div>0</div></td></tr>";
            }
            $ArraySelisih[$idx] .= $sel;
            */
        }

        $idx = ($i-1);

        $template->
        assign_block_vars(
            'ROW',
            array(
                'No'        =>$i,
                'KodeCabang'=>$row['Nama'],
                'Pembuat'   =>$row['NamaPembuat'],
                'Releaser'  =>$row['NamaReleaser'],
                'Penerima'  =>$row['NamaPenerima'],
                'Pencetak'  =>$row['NamaPencetak'],
                'JenisBiaya'=>$jenisbiaya[$idx],
                'Keterangan'=>$row['Keterangan'],
                'Jml'       =>$jumlahbiaya[$idx],
                'Realisasi1' =>$jumlahRealisasi1[$idx],
                'Realisasi2' =>$jumlahRealisasi2[$idx],
                'Selisih'   =>$jumlahSelisih[$idx],
                'TglBuat'   =>date_format(date_create($row['TglBuat']),'d-m-Y H:i:s'),
                'TglCetak'  =>$tglCetak,
                'Status'    =>$status,
                'NoPolisi'  =>$row['NoPolisi'],
                'Jurusan'   =>$row['KodeJurusan'],
                'Icon'      =>$icon,
                'Link'      =>$link,
                'Target'    =>$target,
                'Onclik'    =>$onclick,
                'OTP'       =>$otp,
                'REALISASI' =>$realisasi,
                'ACTION'    =>$action,
            )
        );

        $i++;
    }

}else{
    echo("Err:".__LINE__);exit;
    /*die(mysql_error());*/
}

    $parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir.
        "&p3=".$cari."&p4=".$kodecabang;
    $script_cetak_excel="Start('biaya_tambahan_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
// QUERY AMBIL BIAYA TAMBAHAN
$sql = "SELECT *
        FROM tbl_biaya_coa
        WHERE FlagJenis=1
        ORDER BY IdBiayaCOA ASC";

if ($result = $db->sql_query($sql)){
    $i = 0;
    while ($row = $db->sql_fetchrow($result)){
        $template->
        assign_block_vars(
            'ROWJENIS',
            array(
                'ID'            =>$row['IdBiayaCOA'],
                'JENIS'         =>$row['JenisBiaya'],
            )
        );
        $i++;
    }
}else{
    echo("Err:".__LINE__);exit;
    /*die(mysql_error());*/
}

$page_title = "Biaya Tambahan";


$template->assign_vars(array(
        'BCRUMP'    	=>setBcrump($id_page),
        'URL'			=> append_sid('biaya_tambahan.'.$phpEx),
        'LIST_CABANG'   => setDropdownCabangHeader($kodecabang),
        'SELECT'        => setDropdownCabang(),
        'MOBIL'         => setComboMobil(),
        'BIAYA'         => setComboJenisBiaya(),
        'SUPIR'         => setComboSupir(),
        'KOLOM_OTP'     => $th_otp,
        'KOLOM_REALISASI'=> $th_realisasi,
        'KOLOM_ACTION'  => $th_action,
        'TGL_AWAL'	    => $tanggal_mulai,
        'TGL_AKHIR'		=> $tanggal_akhir,
        'TXT_CARI'		=> $cari,
        'CETAK_XL'      => $script_cetak_excel,
    )
);

$template->set_filenames(array('body' => 'biaya_tambahan_body.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

function setComboMobil($kodekendaraan = ""){
    //SET COMBO MOBIL
    global $db;
    $Mobil = new Mobil();

    $result=$Mobil->ambilDataForComboBox();
    $opt_mobil="<option value=''>- silahkan pilih kendaraan  -</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ($kodekendaraan == $row['KodeKendaraan'])?"selected":"";
            $opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan] ($row[NoPolisi]) $row[Merek] $row[Jenis]</option>";
        }
    }
    else{
        echo("Err :".__LINE__);exit;
    }

    return $opt_mobil;
    //END SET COMBO MOBIL
}
function setComboJenisBiaya(){
    //SET COMBO MOBIL
    global $db;

    $sql = "SElECT * FROM tbl_biaya_coa WHERE FlagJenis=1";
    $opt_biaya="<option value=''>- silahkan pilih jenis biaya  -</option>";
    if (!$result = $db->sql_query($sql)){
        die(mysql_error());
    }
    if($result){
        while ($row = $db->sql_fetchrow($result)){

            $opt_biaya .="<option value='$row[JenisBiaya]'>$row[JenisBiaya]</option>";
        }
    }
    else{
        echo("Err :".__LINE__);exit;
    }

    return $opt_biaya;
    //END SET COMBO MOBIL
}
function setComboSupir($driver = ""){
    //SET COMBO MOBIL
    global $db;

    $sql = "SElECT * FROM tbl_md_sopir WHERE FlagAktif=1 ORDER BY Nama";
    $opt_biaya="<option value=''>- silahkan pilih penerima  -</option>";
    if (!$result = $db->sql_query($sql)){
        die(mysql_error());
    }
    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ($driver == $row['KodeSopir'])?"selected":"";
            $opt_biaya .="<option value='$row[KodeSopir]' $selected>$row[Nama]</option>";
        }
    }
    else{
        echo("Err :".__LINE__);exit;
    }

    return $opt_biaya;
    //END SET COMBO MOBIL
}
function cekDuplikasiOTP($otp){
    global $db;
    $sql = "SELECT COUNT(OTPCode) AS jumlah_data FROM tbl_biaya_tambahan WHERE OTPCode = '$otp'";

    if ($result = $db->sql_query($sql)){
        $row = $db->sql_fetchrow($result);
    }
    else{
        die_error(mysql_error());
    }

    return $row['jumlah_data'];
}
function generateString($length)
{
    $charset = "0123456789";
    $key ="";
    for($i=0; $i<$length; $i++)
    {
        $key .= $charset[(mt_rand(0,(strlen($charset)-1)))];
    }

    return $key;
}
function setDropdownCabang(){
    global $db;

    $Cabang	= new Cabang();

    $result	= $Cabang->setComboCabang();

    $opt = "";

    if ($result){
        $opt = "<option value=''>(none)</option>" . $opt;

        while ($row = $db->sql_fetchrow($result)){
            $opt .= "<option value='$row[0]'>$row[1]</option>";
        }
    }
    else{
        $opt .="<option selected=selected>Error</option>";
    }

    return $opt;
}
function setComboJadwal($kodejur = ""){
    //SET COMBO jurusan
    global $db;
    global $userdata;

    if($userdata[KodeCabang] == 'PRW' || $userdata[KodeCabang] == 'KDPU' || $userdata[KodeCabang] == 'KCHP'){
        $kondisi = "WHERE 1";
    }else{
        $kondisi = "WHERE asal.KodeCabang = '$userdata[KodeCabang]'";
    }
    $sql = "SELECT KodeJurusan, asal.Nama AS ASAL, tujuan.Nama AS TUJUAN
            FROM tbl_md_jurusan jurusan
            JOIN tbl_md_cabang asal ON jurusan.KodeCabangAsal = asal.KodeCabang
            JOIN tbl_md_cabang tujuan ON jurusan.KodeCabangTujuan = tujuan.KodeCabang
            $kondisi
            ORDER BY KodeJurusan ASC";
    $opt_jurusan ="<option value=''>- silahkan pilih jurusan -</option>";
    if($result = $db->sql_query($sql)){
        while ($row = $db->sql_fetchrow($result)){
            $selected = ($kodejur == $row['KodeJurusan'])?"selected":"";
            $opt_jurusan .="<option value='$row[KodeJurusan]' $selected>$row[ASAL] - $row[TUJUAN]</option>";
        }
    }
    else{
        echo("Error :".__LINE__);exit;
    }

    return $opt_jurusan;
    //END SET COMBO JURUSAN
}
function setDropdownCabangHeader($cabang_dipilih){
    //SET COMBO cabang
    global $db;
    $Cabang	= new Cabang();

    $result=$Cabang->ambilData("","Nama,Kota","ASC");
    $opt_cabang="<option value=''>none</option>";

    if($result){
        while ($row = $db->sql_fetchrow($result)){
            $selected	=($cabang_dipilih!=$row['KodeCabang'])?"":"selected";
            $opt_cabang .='<option value="'.$row['KodeCabang'].'" '.$selected.'>'.$row['Nama'].' ('.$row['Kota'].') ('.$row['KodeCabang'].')</option>';
        }
    }
    else{
        echo("Error :".__LINE__);exit;
    }
    return $opt_cabang;
}
?>