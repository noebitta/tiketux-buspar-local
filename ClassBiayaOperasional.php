<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit; 
}
//#############################################################################

class BiayaOperasional{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function BiayaOperasional(){
		$this->ID_FILE="C-BOP";
	}
	
	//BODY
	
	function tambah(
		$no_spj,$kode_akun,$flag_jenis_biaya,
		$kode_kendaraan,$kode_sopir,$jumlah,
		$id_petugas,$kode_jadwal,$kode_cabang){
	  
		/*
		ID		: 001
		DESC	: menambahkan data biaya operasional 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		
		$sql	=
			"CALL sp_biaya_op_tambah(
				'$no_spj','$kode_akun','$flag_jenis_biaya',
				'$kode_kendaraan','$kode_sopir','$jumlah',
				'$id_petugas','$kode_jadwal','$kode_cabang')";
		
		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilBiayaOpByJurusan($id_jurusan){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				KodeAkunBiayaSopir, BiayaSopir, 
				KodeAkunBiayaTol, BiayaTol, 
				KodeAkunBiayaParkir, BiayaParkir, 
				KodeAkunBiayaBBM, BiayaBBM,
				KodeAkunKomisiPenumpangSopir,KomisiPenumpangSopir,
				IsBiayaSopirKumulatif
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaOpByJurusan
	
	function ambilBiayaOpByKodeJadwal($kode_jadwal){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				KodeAkunBiayaSopir, BiayaSopir, 
				KodeAkunBiayaTol, BiayaTol, 
				KodeAkunBiayaParkir, BiayaParkir, 
				KodeAkunBiayaBBM, BiayaBBM,
				KodeAkunKomisiPenumpangSopir,KomisiPenumpangSopir,
				KodeAkunKomisiPaketSopir,KomisiPaketSopir,
				IsBiayaSopirKumulatif,IsVoucherBBM
			FROM tbl_md_jurusan
			WHERE IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal');";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaOpByKodeJadwal
	
	function ambilBiayaBBMByNoSPJ($no_spj){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $FLAG_BIAYA_BBM;
		
		$sql = 
			"SELECT Jumlah
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj' AND FlagJenisBiaya = '$FLAG_BIAYA_BBM';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaBBMByNoSPJ
	
	function ambilBiayaVoucherBBMByNoSPJ($no_spj){
		
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		global $FLAG_BIAYA_VOUCHER_BBM;
		
		$sql = 
			"SELECT Jumlah
			FROM tbl_biaya_op
			WHERE NoSPJ='$no_spj' AND FlagJenisBiaya = '$FLAG_BIAYA_VOUCHER_BBM';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row[0];
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaBBMByNoSPJ
}
?>