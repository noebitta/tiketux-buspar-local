<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 215;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] ){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$tgl_acuan		= isset($HTTP_GET_VARS['tgl_acuan'])? $HTTP_GET_VARS['tgl_acuan'] : $HTTP_POST_VARS['tgl_acuan'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

// LIST
$template->set_filenames(array('body' => 'generaltotal/index.tpl'));

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);
$tgl_acuan_par = $tgl_acuan;
if($tgl_acuan == 0){
	$tgl_acuan = "TglBerangkat";
	$selected1  = "selected";
	$selected2  = "";
}else{
	$tgl_acuan = "DATE(WaktuCetakTiket)";
	$selected1  = "";
	$selected2  = "selected";
}
//MEGAMBIL OPEN TRIP=========
//mengambil list jadwal yang aktif
$sql	=
	"SELECT KodeJadwal
	FROM tbl_penjadwalan_kendaraan
	WHERE TglBerangkat	BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND StatusAktif=1";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$list_kode_jadwal_aktif	= "";
$idx	=0;
while($row=$db->sql_fetchrow($result)){
	$list_kode_jadwal_aktif .="'".$row[0]."',";
	$idx++;
}

$jumlah_jadwal_diaktifkan	= $db->sql_numrows($result);
$list_kode_jadwal_aktif		= $list_kode_jadwal_aktif!=""?substr($list_kode_jadwal_aktif,0,-1):"''";


//mengambil list jadwal yang tidak aktif
$sql	=
	"SELECT KodeJadwal
	FROM tbl_penjadwalan_kendaraan
	WHERE TglBerangkat	BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND StatusAktif=0";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$list_kode_jadwal_nonaktif	= "";

while($row=$db->sql_fetchrow($result)){
	$list_kode_jadwal_nonaktif .="'".$row[0]."',";
}


$list_kode_jadwal_nonaktif		= $list_kode_jadwal_nonaktif!=""?substr($list_kode_jadwal_nonaktif,0,-1):"''";

$sql=
	"SELECT IS_NULL(COUNT(1),0)+$jumlah_jadwal_diaktifkan AS JadwalAktif
	FROM tbl_md_jadwal
	WHERE KodeJadwal NOT IN($list_kode_jadwal_aktif) AND KodeJadwal NOT IN($list_kode_jadwal_nonaktif) AND FlagAktif=1";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_open_trip=$db->sql_fetchrow($result);

//END====MEGAMBIL OPEN TRIP=========

//MEGAMBIL TRIP ISI================
$sql	=
	"SELECT IS_NULL(COUNT(DISTINCT(KodeJadwal)),0) AS TripIsi
	FROM tbl_reservasi WHERE FlagBatal!=1 AND TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' ";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_trip_isi=$db->sql_fetchrow($result);
//END====MEGAMBIL TRIP ISI=========

//MEGAMBIL BOOKING & CANCEL================
$sql	=
	"SELECT IS_NULL(COUNT(1),0) AS Booking,IS_NULL(COUNT(IF(FlagBatal=1,1,NULL)),0) AS Cancel
	FROM tbl_reservasi WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_book=$db->sql_fetchrow($result);
//END====BOOKING & CANCEL=========

//MEGAMBIL DATA TIKET================
//mengambil jurusan dari Jakarta
$sql	= "SELECT IdJurusan
		   FROM tbl_md_jurusan
		   JOIN tbl_md_cabang ON KodeCabangAsal = KodeCabang
		   WHERE Kota = 'JAKARTA'";
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);
	die(mysql_error());
}

$list_jurusan_jakarta	= "";

while($row=$db->sql_fetchrow($result)){
	$list_jurusan_jakarta .=$row[0].",";
}

$list_jurusan_jakarta		= $list_jurusan_jakarta!=""?substr($list_jurusan_jakarta,0,-1):"''";

$sql	=
	"SELECT
		IS_NULL(COUNT(1),0) AS Penumpang,
		IS_NULL(COUNT(IF(PetugasCetakTiket=0,1,NULL)),0) AS PenumpangOnline,
		IS_NULL(SUM(Total),0) AS OmzetPenumpang,
		IS_NULL(SUM(IF(PetugasCetakTiket=0,Total,0)),0) AS OmzetPenumpangOnline
	FROM tbl_reservasi WHERE $tgl_acuan  BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND FlagBatal!=1  AND IdJurusan IN($list_jurusan_jakarta)";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_tiket_jakarta=$db->sql_fetchrow($result);

//mengambil jurusan dari bandung
$sql	= "SELECT IdJurusan
			FROM tbl_md_jurusan
			JOIN tbl_md_cabang ON KodeCabangAsal = KodeCabang
			WHERE Kota = 'BANDUNG'";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$list_jurusan_bandung	= "";

while($row=$db->sql_fetchrow($result)){
	$list_jurusan_bandung .=$row[0].",";
}

$list_jurusan_bandung		= $list_jurusan_bandung!=""?substr($list_jurusan_bandung,0,-1):"''";

$sql	=
	"SELECT
		IS_NULL(COUNT(1),0) AS Penumpang,
		IS_NULL(COUNT(IF(PetugasCetakTiket=0,1,NULL)),0) AS PenumpangOnline,
		IS_NULL(SUM(Total),0) AS OmzetPenumpang,
		IS_NULL(SUM(IF(PetugasCetakTiket=0,Total,0)),0) AS OmzetPenumpangOnline
	FROM tbl_reservasi WHERE $tgl_acuan BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND FlagBatal!=1 AND IdJurusan IN($list_jurusan_bandung)";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_tiket_bandung=$db->sql_fetchrow($result);
//END==== DATA TIKET=========

//KENDARAAN=====================
//jumlah kendaraan keseluruhan
$sql	=
	"SELECT IS_NULL(COUNT(1),0)
	FROM tbl_md_kendaraan
	WHERE KodeKendaraan NOT LIKE '%WH-%'";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_kendaraan=$db->sql_fetchrow($result);

$sql	=
	"SELECT IS_NULL(COUNT(DISTINCT(NoPolisi)),0)
	FROM tbl_spj
	WHERE DATE(TglBerangkat) BETWEEN'$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_kendaraan_aktif=$db->sql_fetchrow($result);
//END KENDARAAN=================

//SOPIR=====================
//jumlah sopir keseluruhan
$sql	=
	"SELECT IS_NULL(COUNT(1),0)
	FROM tbl_md_sopir
	WHERE FlagAktif=1";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_sopir=$db->sql_fetchrow($result);

$sql	=
	"SELECT IS_NULL(COUNT(DISTINCT(KodeDriver)),0)
	FROM tbl_spj
	WHERE DATE(TglBerangkat) BETWEEN'$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'";
	
if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_sopir_aktif=$db->sql_fetchrow($result);
//END SOPIR=================

//MEGAMBIL DATA PAKET================
//mengambil jurusan dari Jakarta

$sql	=
	"SELECT
		IS_NULL(COUNT(1),0) AS Paket,
		IS_NULL(SUM(HargaPaket),0) AS OmzetPaket
	FROM tbl_paket WHERE $tgl_acuan BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND FlagBatal!=1 AND CetakTiket=1 AND IdJurusan IN($list_jurusan_jakarta)";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit();
	//die(mysql_error());
}

$data_paket_jakarta=$db->sql_fetchrow($result);

//mengambil jurusan dari bandung
$sql	=
	"SELECT
		IS_NULL(COUNT(1),0) AS Paket,
		IS_NULL(SUM(HargaPaket),0) AS OmzetPaket
	FROM tbl_paket WHERE $tgl_acuan BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' AND FlagBatal!=1  AND CetakTiket=1 AND IdJurusan IN($list_jurusan_bandung)";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$data_paket_bandung=$db->sql_fetchrow($result);
//END==== DATA PAKET=========

$page_title = "General Total";



/*MENGAMBIL DATA TARGET CABANG DARI TABEL TARGET*/
$bulan1 = date_format(date_create($tanggal_mulai),'n');
$bulan2 = date_format(date_create($tanggal_akhir),'n');
$tahun1 = date_format(date_create($tanggal_mulai),'Y');
$tahun2 = date_format(date_create($tanggal_akhir),'Y');
$sql ="SELECT KodeCabang,SUM(TargetPax) AS TargetPax,SUM(TargetPaxTercapai) AS TargetPaxTercapai,SUM(TargetPaket) AS TargetPaket,SUM(TargetPaketTercapai) AS TargetPaketTercapai
       FROM tbl_md_cabang_target
       WHERE BulanTarget BETWEEN $bulan1 AND $bulan2 AND TahunTarget BETWEEN $tahun1 AND $tahun2
       GROUP BY KodeCabang
       ORDER BY KodeCabang ASC";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load user',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}


while($row=$db->sql_fetchrow($result)){
	$data_target[$row['KodeCabang']]['TargetPax']          = $row['TargetPax'];
	$data_target[$row['KodeCabang']]['TargetPaxTercapai']  = $row['TargetPaxTercapai'];
	$data_target[$row['KodeCabang']]['TargetPaket']        = $row['TargetPaket'];
	$data_target[$row['KodeCabang']]['TargetPaketTercapai']= $row['TargetPaketTercapai'];
	$data_target[$row['KodeCabang']]['Log']                = $row['Log'];
	$data_target[$row['KodeCabang']]['BulanTarget']		   = $row['BulanTarget'];
	$data_target[$row['KodeCabang']]['TahunTarget']		   = $row['TahunTarget'];
}



/*MENGAMBIL JUMLAH TIKET YANG DIBELI COUNTER DIJAKARTA */
$sql="SELECT tbl_reservasi.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta, Kota
      FROM tbl_reservasi
      JOIN tbl_md_cabang ON tbl_reservasi.KodeCabang = tbl_md_cabang.KodeCabang
      WHERE ($tgl_acuan BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
      AND CetakTiket = 1 AND FlagBatal != 1 AND Kota = 'JAKARTA'
      GROUP BY tbl_reservasi.KodeCabang";

if (!$result = $db->sql_query($sql)){
	echo("Error:".__LINE__."<br>");
	die(mysql_error());
}
while($row=$db->sql_fetchrow($result)){
	$tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'] = $row['JmlFromJakarta'];
}

/*MENGAMBIL JUMLAH TIKET YANG TUJUANYA KE JAKARTA*/
$sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan AS KodeCabang, Kota
        FROM tbl_reservasi
        JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
        JOIN tbl_md_cabang ON tbl_md_jurusan.KodeCabangTujuan = tbl_md_cabang.KodeCabang
        WHERE ($tgl_acuan BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
        AND FlagBatal != 1 AND CetakTiket = 1 AND Kota = 'JAKARTA'
        GROUP BY KodeCabangTujuan";
if (!$result = $db->sql_query($sql)){
	echo("Error:".__LINE__."<br>");
	die(mysql_error());
}
while($row=$db->sql_fetchrow($result)){
	$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'] = $row['JmlFromBandungToJakarta'];
}

/*MENGAMBIL JUMLAH PAKET DARI COUNTER JAKARTA*/
$sql="SELECT tbl_paket.KodeCabang, COUNT(NoTiket) AS JmlFromJakarta, Kota
      FROM tbl_paket
      JOIN tbl_md_cabang ON tbl_paket.KodeCabang = tbl_md_cabang.KodeCabang
      WHERE ($tgl_acuan BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
      AND CetakTiket = 1 AND FlagBatal != 1 AND Kota = 'JAKARTA'
      GROUP BY tbl_paket.KodeCabang";

if (!$result = $db->sql_query($sql)){
	echo("Error:".__LINE__."<br>");
	die(mysql_error());
}
while($row=$db->sql_fetchrow($result)){
	$paket_from_jakarta[$row['KodeCabang']]['FromJakarta'] = $row['JmlFromJakarta'];
}

/*MENGAMBIL JUMLAH PAKET YANG TUJUANYA KE JAKARTA*/
$sql = "SELECT COUNT(NoTiket) AS JmlFromBandungToJakarta, KodeCabangTujuan AS KodeCabang, Kota
        FROM tbl_paket
        JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
        JOIN tbl_md_cabang ON tbl_md_jurusan.KodeCabangTujuan = tbl_md_cabang.KodeCabang
        WHERE ($tgl_acuan BETWEEN  '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
        AND FlagBatal != 1 AND CetakTiket = 1 AND Kota = 'JAKARTA'
        GROUP BY KodeCabangTujuan";
if (!$result = $db->sql_query($sql)){
	echo("Error:".__LINE__."<br>");
	die(mysql_error());
}
while($row=$db->sql_fetchrow($result)){
	$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'] = $row['JmlFromBandungToJakarta'];
}


/*MENGAMBIL DATA CABANG */
$sql ="SELECT *
       FROM tbl_md_cabang
       WHERE Kota = 'JAKARTA' $kondisi
       ORDER BY Nama ASC";
if (!$result = $db->sql_query($sql)){
	echo("Error:".__LINE__."<br>");
	die(mysql_error());
}

while ($row = $db->sql_fetchrow($result)){

	if($data_target[$row['KodeCabang']]['TargetPax']){
		$jakarta = ($tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'] == "")?0:$tiket_from_jakarta[$row['KodeCabang']]['FromJakarta'];
		$bandung = ($tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'] == "")?0:$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'];
		if($jakarta == 0 && $bandung == 0){
			$capaian = 0;
		}else{
			$capaian = (($jakarta+$bandung)/$data_target[$row['KodeCabang']]['TargetPax'])*100;
			$capaian = round($capaian,2);
		}
	}else{
		$capaian = 0;
	}

	if($data_target[$row['KodeCabang']]['TargetPaket']){
		if($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'] == 0){
			$capaianpaket = 0;
		}else{
			$capaianpaket = ($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'])/$data_target[$row['KodeCabang']]['TargetPaket']*100;
			$capaianpaket = round($capaianpaket,2);
		}
	}else{
		$capaianpaket = 0;
	}

	$parameter_cetak = "tgl1=".$tanggal_mulai_mysql."&tgl2=".$tanggal_akhir_mysql."&p3=".$row['KodeCabang']."&nama=".$row['Nama'];
	$script_grafik = append_sid('generaltotal_grafik.php?'.$parameter_cetak);

	$template->
	assign_block_vars(
			'ROW',
			array(
					'Cabang'      =>$row['Nama'],
					'targetpax'   =>($data_target[$row['KodeCabang']]['TargetPax']!=""?$data_target[$row['KodeCabang']]['TargetPax']:0),
					'capaianpax'  =>$capaian,
					'realpax'     =>number_format($tiket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$tiket_to_jakarta[$row['KodeCabang']]['ToJakarta'],0,",","."),
					'targetpaket' =>($data_target[$row['KodeCabang']]['TargetPaket']!=""?$data_target[$row['KodeCabang']]['TargetPaket']:0),
					'capaianpaket'=>$capaianpaket,
				    'realpaket'	  =>number_format($paket_from_jakarta[$row['KodeCabang']]['FromJakarta']+$paket_to_jakarta[$row['KodeCabang']]['ToJakarta'],0,",","."),
					'SCRIPT'	  => $script_grafik
			)
	);
}

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('generaltotal.'.$phpEx),
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'SELECT1'			=> $selected1,
	'SELECT2'			=> $selected2,
	'TANGGAL'				=> dateparse($tanggal_mulai)." s/d ".dateparse($tanggal_akhir),
	'OPEN_TRIP'			=> number_format($data_open_trip[0],0,",","."),
	'TRIP_ISI'			=> number_format($data_trip_isi[0],0,",","."),
	'BOOKING'				=> number_format($data_book['Booking'],0,",","."),
	'CANCEL'				=> number_format($data_book['Cancel'],0,",","."),
	'PNP_JKT'				=> number_format($data_tiket_jakarta['Penumpang'],0,",","."),
	'PNP_BDG'				=> number_format($data_tiket_bandung['Penumpang'],0,",","."),
	'PNP_OL_JKT'		=> number_format($data_tiket_jakarta['PenumpangOnline'],0,",","."),
	'PNP_OL_BDG'		=> number_format($data_tiket_bandung['PenumpangOnline'],0,",","."),
	'PKT_JKT'				=> number_format($data_paket_jakarta['Paket'],0,",","."),
	'PKT_BDG'				=> number_format($data_paket_bandung['Paket'],0,",","."),
	'OMZ_PNP_JKT'		=> number_format($data_tiket_jakarta['OmzetPenumpang'],0,",","."),
	'OMZ_PNP_BDG'		=> number_format($data_tiket_bandung['OmzetPenumpang'],0,",","."),
	'OMZ_OL_JKT'		=> number_format($data_tiket_jakarta['OmzetPenumpangOnline'],0,",","."),
	'OMZ_OL_BDG'		=> number_format($data_tiket_bandung['OmzetPenumpangOnline'],0,",","."),
	'OMZ_PKT_JKT'		=> number_format($data_paket_jakarta['OmzetPaket'],0,",","."),
	'OMZ_PKT_BDG'		=> number_format($data_paket_bandung['OmzetPaket'],0,",","."),
	'UNIT_AKTIF'		=> number_format($data_kendaraan_aktif[0],0,",","."),
	'UNIT_NONAKTIF'	=> number_format($data_kendaraan[0]-$data_kendaraan_aktif[0],0,",","."),
	'SUPIR_AKTIF'		=> number_format($data_sopir_aktif[0],0,",","."),
	'SUPIR_NONAKTIF'=> number_format($data_sopir[0]-$data_sopir_aktif[0],0,",","."),
	'MEMBER_AKTIF'	=> number_format(0,0,",","."),
	'MEMBER_NONAKTIF'=> number_format(0,0,",","."),
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>