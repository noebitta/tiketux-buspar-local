<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"]))){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

$Cabang =new Cabang();
// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$cari  					= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$sort_by				= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
    " WHERE (tbl_biaya_tambahan.KodeCabang LIKE '$cari%'
		OR NamaPencetak LIKE '%$cari%'
		OR NamaPenerima LIKE '%$cari%'
		OR NamaPembuat LIKE '$cari%'
		OR JenisBiaya LIKE '$cari%'
		OR Keterangan LIKE '$cari%'
		OR Jumlah LIKE '$cari%'
		OR NamaReleaser LIKE '%$cari%')";

$order	=($order=='')?"ASC":$order;

$sort_by =($sort_by=='')?"NamaPenerima":$sort_by;

//MENGAMBIL DATA-DATA MASTER CABANG
$sql=
    "SELECT
		TglCetak,f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) as NoPol,NamaPenerima,
		KodeKendaraan,KodeCabang,KodeJurusan,KodeBiaya,id_biaya_tambahan,
		GROUP_CONCAT(tbl_biaya_tambahan.id_biaya_tambahan ORDER BY id_biaya_tambahan ASC) AS id_biaya,
		GROUP_CONCAT(tbl_biaya_tambahan.JenisBiaya ORDER BY id_biaya_tambahan ASC) AS jenis,
		GROUP_CONCAT(tbl_biaya_tambahan.Jumlah ORDER BY id_biaya_tambahan) AS jumlahBiaya,
	    GROUP_CONCAT(tbl_biaya_tambahan.Realisasi1 ORDER BY id_biaya_tambahan) AS Realisasi1,
	    GROUP_CONCAT(tbl_biaya_tambahan.Realisasi2 ORDER BY id_biaya_tambahan) AS Realisasi2,
	    GROUP_CONCAT(tbl_biaya_tambahan.Selisih ORDER BY id_biaya_tambahan) AS Selisih,
		Keterangan,OTPCode
	FROM tbl_biaya_tambahan
	$kondisi_cari AND DATE(TglCetak) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'
	AND StatusCetak=1
	GROUP BY OTPCode,NamaPenerima";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}


//isi array temp laporan
$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result)) {

    $temp_array[$idx]['IdBiaya']  = $row['id_biaya_tambahan'];
    $temp_array[$idx]['KodeBiaya']= $row['KodeBiaya'];
    $temp_array[$idx]['TglCetak'] = date_format(date_create($row['TglCetak']),'d-m-Y H:i:s');
    $temp_array[$idx]['NoPol'] = $row['NoPol'];
    $temp_array[$idx]['NamaPenerima'] = $row['NamaPenerima'];
    $temp_array[$idx]['KodeKendaraan'] = $row['KodeKendaraan'];
    $temp_array[$idx]['KodeCabang'] = $row['KodeCabang'];
    $temp_array[$idx]['KodeJurusan'] = $row['KodeJurusan'];
    $temp_array[$idx]['Keterangan'] = $row['Keterangan'];
    $temp_array[$idx]['jenis'] = $row['jenis'];
    $temp_array[$idx]['jumlah_biaya'] = $row['jumlahBiaya'];
    $temp_array[$idx]['Realisasi1'] = $row['Realisasi1'];
    $temp_array[$idx]['Realisasi2'] = $row['Realisasi2'];
    $temp_array[$idx]['Selisih'] = $row['Selisih'];

    $temp_idx += $idx;
    $idx++;
}


if($order=='ASC'){
    //$temp_array = multiSortArray($temp_array, array($sort_by=>1));
    $temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
    //$temp_array = multiSortArray($temp_array, array($sort_by=>0));
    $temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');

$objPHPExcel->getActiveSheet()->mergeCells('A4:A5');
$objPHPExcel->getActiveSheet()->mergeCells('B4:B5');
$objPHPExcel->getActiveSheet()->mergeCells('C4:C5');
$objPHPExcel->getActiveSheet()->mergeCells('D4:D5');
$objPHPExcel->getActiveSheet()->mergeCells('E4:G4');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'REALISASI BIAYA OPERASIONAL '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'No Polisi');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Sopir');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'ANALITICAL ACCOUNTING');
$objPHPExcel->getActiveSheet()->setCellValue('E5', 'NO BODY');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F5', 'COUNTER');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G5', 'ROUTE');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'BIAYA');

// Mengambil jenis biaya
$tambahan = "SELECT JenisBiaya FROM tbl_biaya_coa WHERE FlagJenis = 1 ORDER BY JenisBiaya ASC";
if(!$BIAYATAMBAHAN = $db->sql_query($tambahan)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}

$posisi = 7;
$biayajenis = array();
while($row = $db->sql_fetchrow($BIAYATAMBAHAN)){
    $alpa = position($posisi);
    $namajenis = $row['JenisBiaya'];
    $objPHPExcel->getActiveSheet()->setCellValue($alpa.'5', $namajenis);
    $objPHPExcel->getActiveSheet()->getColumnDimension($alpa)->setAutoSize(true);
    $posisi++;

    array_push($biayajenis,$row['JenisBiaya']);
}

$objPHPExcel->getActiveSheet()->mergeCells('H4:'.position($posisi-1).'4');

//$objPHPExcel->getActiveSheet()->mergeCells('N4:N5');
//$objPHPExcel->getActiveSheet()->mergeCells('O4:O5');

$objPHPExcel->getActiveSheet()->setCellValue(position($posisi).'4', 'KETERANGAN');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells(position($posisi).'4:'.position($posisi).'5');

$posisi++;
$objPHPExcel->getActiveSheet()->setCellValue(position($posisi).'4', 'Total Biaya');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells(position($posisi).'4:'.position($posisi).'5');

$posisi++;
$objPHPExcel->getActiveSheet()->setCellValue(position($posisi).'4', 'Realisasi 1');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells(position($posisi).'4:'.position($posisi).'5');

$posisi++;
$objPHPExcel->getActiveSheet()->setCellValue(position($posisi).'4', 'Realisasi 2');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells(position($posisi).'4:'.position($posisi).'5');

$posisi++;
$objPHPExcel->getActiveSheet()->setCellValue(position($posisi).'4', 'Selisih');
$objPHPExcel->getActiveSheet()->getColumnDimension(position($posisi))->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells(position($posisi).'4:'.position($posisi).'5');


$idx=0;
$idx_row=6;
while ($idx<count($temp_array)) {
    $objPHPExcel->getActiveSheet()->setCellValue('A' . $idx_row, $idx + 1);
    $objPHPExcel->getActiveSheet()->setCellValue('B' . $idx_row, $temp_array[$idx]['TglCetak']);
    $objPHPExcel->getActiveSheet()->setCellValue('C' . $idx_row, $temp_array[$idx]['NoPol']);
    $objPHPExcel->getActiveSheet()->setCellValue('D' . $idx_row, $temp_array[$idx]['NamaPenerima']);
    $objPHPExcel->getActiveSheet()->setCellValue('E' . $idx_row, $temp_array[$idx]['KodeKendaraan']);
    $objPHPExcel->getActiveSheet()->setCellValue('F' . $idx_row, $temp_array[$idx]['KodeCabang']);
    $objPHPExcel->getActiveSheet()->setCellValue('G' . $idx_row, $temp_array[$idx]['KodeJurusan']);

    //DiNAMIK kolom

    $posisi = 7;
    $jenis = explode(",",$temp_array[$idx]['jenis']);
    sort($jenis);
    $biaya = explode(",",$temp_array[$idx]['jumlah_biaya']);
    sort($biaya);

    for($x = 0; $x < count($jenis); $x++) {
        $index_alpa =  array_search($jenis[$x],$biayajenis);
        $objPHPExcel->getActiveSheet()->setCellValue(position($posisi+$index_alpa).$idx_row, $biaya[$x]);
    }

    $objPHPExcel->getActiveSheet()->setCellValue(position($posisi+count($biayajenis)). $idx_row, $temp_array[$idx]['Keterangan']);

    $objPHPExcel->getActiveSheet()->setCellValue(position($posisi+count($biayajenis)+1). $idx_row, array_sum($biaya));

    $realisasi = explode(",",$temp_array[$idx]['Realisasi1']);
    $objPHPExcel->getActiveSheet()->setCellValue(position($posisi+count($biayajenis)+2). $idx_row, array_sum($realisasi));

    $realisasi2 = explode(",",$temp_array[$idx]['Realisasi2']);
    $objPHPExcel->getActiveSheet()->setCellValue(position($posisi+count($biayajenis)+3). $idx_row, array_sum($realisasi2));

    $selisih = explode(",",$temp_array[$idx]['Selisih']);
    $objPHPExcel->getActiveSheet()->setCellValue(position($posisi+count($biayajenis)+4). $idx_row, array_sum($selisih));
    $idx_row++;
    $idx++;
}

$temp_idx=$idx_row-1;

$idx_row++;
$posisi = 7;
for($a=0;$a<count($biayajenis);$a++){
    $objPHPExcel->getActiveSheet()->setCellValue(position($posisi).$idx_row, '=SUM('.position($posisi).'6:'.position($posisi).$temp_idx.')');
    $posisi++;
}

$posisi = 7;
$objPHPExcel->getActiveSheet()->setCellValue('C'.($idx_row+3),'MENYERAHKAN');
$objPHPExcel->getActiveSheet()->setCellValue('C'.($idx_row+8),'RIZKY');
$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+2),'Total');
$objPHPExcel->getActiveSheet()->setCellValue('H'.($idx_row+2),'=SUM('.position($posisi).$idx_row.':'.position($posisi+count($biayajenis)-1).$idx_row.')');

$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+3),'MENERIMA');
$objPHPExcel->getActiveSheet()->setCellValue('G'.($idx_row+8),'AYU');
$objPHPExcel->getActiveSheet()->setCellValue('M'.($idx_row+3),'MENGETAHUI');
$objPHPExcel->getActiveSheet()->setCellValue('M'.($idx_row+8),'REZA');


$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
for($col = 'A'; $col !== position(7 + count($biayajenis) +5); $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.'4:'.$col.$idx_row)->applyFromArray($styleArray);
}

$gaya = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font'  => array(
        'bold'  => false,
        'color' => array('rgb' => '000'),
        'size'  => 12,
        'name'  => 'Arial'
    ),
);

for($col = 'A'; $col !== position(7 + count($biayajenis) +5); $col++) {
    $objPHPExcel->getActiveSheet()->getStyle($col.'4:'.$col.'5')->applyFromArray($gaya);
}

$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'font'  => array(
        'bold'  => false,
        'color' => array('rgb' => '000'),
        'size'  => 12,
        'name'  => 'Arial'
    ),
);

$objPHPExcel->getDefaultStyle()->applyFromArray($style);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

if ($idx>0){
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Realisasi Biaya Operasional Tgl '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}

function position($posisi){
    $alpa = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    return $alpa[$posisi];
}
?>
