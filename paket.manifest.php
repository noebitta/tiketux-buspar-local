<?php
//HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 223;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassPaketEkspedisi.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'phpqrcode/BarcodeQR.php');

// PARAMETER
$tgl_berangkat 			= getVariabel('submittglberangkat');
$kode_jadwal 			      = getVariabel('submitjadwal');
$otp 	              = getVariabel('otpmanifest');

//INIT

//CREATE INSTANCE
$Paket      = new Paket();
$Pengaturan = new PengaturanUmum();
$qr         = new BarcodeQR();

//PROSES
$data_manifest= $Paket->cekManifestPaketByKeberangkatan(FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal); //PENGECEKKAN NO MANIFEST MENGGUNAKAN METHODE INI UTK KEAMANAN JIKA ADA HACKING DARI JAVASCRIPT

$no_manifest_induk= $data_manifest["NoManifestInduk"];

if($no_manifest_induk==""){
  //PENGECEKKAN LAGI APAKAH MANIFEST INDUK SUDAH DICETAK
  echo("Anda tidak memiliki otoritas untuk mencetak ulang manifest ini.(ERR:".__LINE__.")");exit;
}

$no_manifest_paket= $data_manifest["NoManifest"];

if($no_manifest_paket!=""){
  //PERIKSA OTORISASI UNTUK CETAK ULANG
  if(!$Permission->isPermitted($userdata["user_level"],$id_page.".4") && !$Paket->verifyOTPCetakUlangManifest($data_manifest["NoManifest"],$otp)) {
    //JIKA TIDAK MEMILIKI OTORISASI, AKAN DITOLAK
    echo("Anda tidak memiliki otoritas untuk mencetak ulang manifest ini.(ERR:".__LINE__.")");exit;
  }

  $template->assign_block_vars("IS_COPY",array());
  $template->assign_block_vars("PENCETAK_ULANG",array(
    "waktucetak"  => dateparseWithTime(date("d-m-Y H:i:s")),
    "petugas"     => $userdata["nama"]
  ));

}

//MENDAFTARKAN MANIFEST KEDALAM SISTEM
$no_manifest_paket  = $Paket->buatManifest($no_manifest_induk,FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal);

//MENGAMBIL DATA MANIFEST TERKINI YANG TELAH TERINPUT ATAU TERUPDATE
$data_manifest  = $Paket->ambilDataManifest($no_manifest_paket);

$qrcode = "phpqrcode/imageQR/".$no_manifest_paket.".png";

if(!file_exists ($qrcode)) {
  $qr->draw(100, $qrcode);
}

$template->set_filenames(array('body' => 'paket/manifest.tpl'));

$data_pengaturan = $Pengaturan->ambilDataPerusahaanKeteranganTiket();

$template->assign_vars(array(
  "NAMA_PERUSAHAAN"   => $data_pengaturan["PERUSAHAAN_NAMA"],
  "ALAMAT_PERUSAHAAN" => $data_pengaturan["PERUSAHAAN_ALAMAT"],
  "TELP_PERUSAHAAN"   => $data_pengaturan["PERUSAHAAN_TELP"],
  "KODE_UNIT"         => $data_manifest["KodeUnit"],
  "NO_PLAT"           => $data_manifest["NoPlat"],
  "DRIVER"            => $data_manifest["Driver"],
  "KODE_DRIVER"       => $data_manifest["KodeDriver"],
  "TGL_BERANGKAT"     => dateparse(FormatMySQLDateToTgl($data_manifest["TglBerangkat"])),
  "ASAL"              => $data_manifest["Asal"],
  "TUJUAN"            => $data_manifest["Tujuan"],
  "JAM_BERANGKAT"     => $data_manifest["JamBerangkat"],
  "KODE_JADWAL"       => $data_manifest["KodeJadwal"],
  "QR_CODE"           => $qrcode,
  "NO_MANIFEST"       => $data_manifest["NoManifest"],
  "WAKTU_CETAK"       => dateparseWithTime(FormatMySQLDateToTglWithTime($data_manifest["WaktuCetak"])),
  "PETUGAS"           => $data_manifest["NamaPencetakManifest"]
));

$res_list_paket = $Paket->ambilListPaket(FormatTglToMySQLDate($tgl_berangkat),$kode_jadwal,$data_manifest["IdJurusan"]);

while($row=$db->sql_fetchrow($res_list_paket)){

  $template->assign_block_vars("LIST_BARANG",array(
    "noresi"  => $row["NoResi"],
    "layanan" => $config["layanan_paket"][$row["Layanan"]],
    "pengirim"=> $row["NamaPengirim"],
    "penerima"=> $row["NamaPenerima"]
  ));

  if($row["IsPaketTransit"]){
    $template->assign_block_vars("LIST_BARANG.TRANSIT",array("tujuanakhir"=>$row["NamaCabangTujuanAkhir"]));
  }
}


$template->pparse('body');
	
?>