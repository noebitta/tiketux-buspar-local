<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassAsuransi.php');

// SESSION
$id_page = 200;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);



// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['p0'])? $HTTP_GET_VARS['p0'] : $HTTP_POST_VARS['p0'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$pencari				= isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$jenis_laporan	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$jenisTgl	= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];

// LIST
$template->set_filenames(array('body' => 'laporan_rekap_tiket_detail/laporan_rekap_tiket_detail_body.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Asuransi	= new Asuransi();

if($jenis_laporan==0){
	//laporan omzet cso
	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND PetugasPenjual='$pencari'";
	
	$kondisi_waktu_paket	= "TglBerangkat";
	$kondisi_laporan_paket= $kondisi_laporan;
	$kondisi_waktu_tambahan= "TglBuat";
}
elseif($jenis_laporan==1){
	//laporan omzet cabang	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$pencari'";
	
	$kondisi_waktu_paket	= "TglBerangkat";
	$kondisi_laporan_paket= $kondisi_laporan;
}
elseif($jenis_laporan==2){
	//laporan omzet Jurusan	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND IdJurusan='$pencari'";
	
	$kondisi_waktu_paket	= "TglBerangkat";
	$kondisi_laporan_paket= "AND IdJurusan='$pencari'";
}
elseif($jenis_laporan==3){
	//laporan rekap uang user	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND PetugasCetakTiket='$pencari'";
	
	$kondisi_waktu_paket	= "TglBerangkat";
	$kondisi_laporan_paket= "AND IF(CaraPembayaran!=$PAKET_CARA_BAYAR_DI_TUJUAN,PetugasPenjual='$pencari',PetugasPemberi='$pencari')";
}
elseif($jenis_laporan==4){
	//laporan rekap uang	
	$kondisi_waktu		= "TglBerangkat";
	$kondisi_laporan	= "AND KodeCabang='$pencari'";
	
	$kondisi_waktu_paket	= "TglBerangkat";
	$kondisi_laporan_paket= "AND KodeCabang='$pencari'";
}


$kondisi_cabang	= $userdata['user_level']!=$USER_LEVEL_INDEX["SUPERVISOR"]?"":" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";

$kondisi	= 
	"WHERE (DATE($kondisi_waktu) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 $kondisi_laporan $kondisi_cabang";

$kondisi_paket	= 
	"WHERE (DATE($kondisi_waktu_paket) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 $kondisi_laporan_paket $kondisi_cabang";

$kondisi_tambahan	=
	"WHERE StatusCetak=1 AND IdPencetak='$pencari' $kondisi_cabang";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"WaktuPesan":$sort_by;

//QUERY TIKET
$sql=
    "SELECT
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,Telp,
		Alamat,Telp,NomorKursi,
		IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,HargaTiket,0),SubTotal) AS HargaTiket,SubTotal,
		IF(JenisPembayaran!=3,Discount,0) AS Discount,IF(JenisPembayaran!=3,Total,0) AS Total,
		IF(JenisPenumpang!='R',JenisDiscount,'RETURN') AS JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,WaktuCetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan,
		NoSPJ
	FROM 
		tbl_reservasi
	$kondisi
	ORDER BY WaktuCetakTiket";

if ($result = $db->sql_query($sql)){
	$i = 1;//$idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$pesanan	= (!$row['FlagPesanan'])?"Go Show":"Pesanan";
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$odd	= "blue";
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$odd	= 'red';
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		}

        switch($row['JenisPembayaran']){
          case 0:
              $ket_jenis_pembayaran="TUNAI";
              break;
          case 1:
              $ket_jenis_pembayaran="DEBIT CARD";
              break;
          case 2:
              $ket_jenis_pembayaran="KREDIT CARD";
              break;
          case 3:
              $ket_jenis_pembayaran="VOUCHER $kode_voucher";
              break;
          case 4:
              //BAYAR DENGAN MEMBER
              $ket_jenis_pembayaran="MEMBER";
              break;
          case 5:
              $ket_jenis_pembayaran="EDC";
              break;
          case 6:
              $ket_jenis_pembayaran="TRANSFER";
              break;
          case 7:
              $ket_jenis_pembayaran="INDOMARET";
              break;
          case 8:
              $ket_jenis_pembayaran="POIN PONTA";
              break;
      }

		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
					'no_tiket'=>$row['NoTiket'],
					'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
					'kode_jadwal'=>$row['KodeJadwal'],
					'nama'=>$row['Nama'],
					'telp'=>$row['Telp'],
					'no_kursi'=>$row['NomorKursi'],
					'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
					'discount'=>number_format($row['Discount'],0,",","."),
					'manifest'=>$row['NoSPJ'],
					'body_mobil'=>$row['NoPolisi'],
					'total'=>number_format($row['Total'],0,",","."),
					'tipe_discount'=>$row['JenisDiscount'],
					'cso'=>$row['NamaCSO'],
					'status'=>$status,
					'ket'=>$keterangan,
                    'bayar'=>$ket_jenis_pembayaran
				)
			);
		
		$i++;
  }
} 
else{
    die(mysql_error());
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 


//QUERY PAKET
$sql=
    "SELECT
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket
	$kondisi_paket
	ORDER BY WaktuPesan";


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$odd	= "blue";
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$odd	= 'red';
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		}

        switch($row['JenisPembayaran']){
          case 0:
              $ket_jenis_pembayaran="TUNAI";
              break;
          case 1:
              $ket_jenis_pembayaran="DEBIT CARD";
              break;
          case 2:
              $ket_jenis_pembayaran="KREDIT CARD";
              break;
          case 3:
              $ket_jenis_pembayaran="VOUCHER $kode_voucher";
              break;
          case 4:
              //BAYAR DENGAN MEMBER
              $ket_jenis_pembayaran="MEMBER";
              break;
          case 5:
              $ket_jenis_pembayaran="EDC";
              break;
          case 6:
              $ket_jenis_pembayaran="TRANSFER";
              break;
          case 7:
              $ket_jenis_pembayaran="INDOMARET";
              break;
          case 8:
              $ket_jenis_pembayaran="POIN PONTA";
              break;
      }

        $template->
			assign_block_vars(
				'ROWPAKET',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuPesan'])),
					'no_tiket'=>$row['NoTiket'],
					'kode_jadwal'=>$row['KodeJadwal'],
					'dari'=>$row['NamaPengirim'],
					'untuk'=>$row['NamaPenerima'],
					'harga_paket'=>number_format($row['HargaPaket'],0,",","."),
					'cso'=>$row['NamaCSO'],
					'status'=>$status,
					'ket'=>$keterangan,
                    'bayar'=>$ket_jenis_pembayaran
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

/*QUERY TAMBAHAN
$sql="SELECT *,
			GROUP_CONCAT(tbl_biaya_tambahan.JenisBiaya ORDER BY id_biaya_tambahan) AS jenis,
	        GROUP_CONCAT(tbl_biaya_tambahan.Jumlah ORDER BY id_biaya_tambahan) AS jumlahBiaya
		FROM tbl_biaya_tambahan
		WHERE DATE(TglCetak) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql'
		AND StatusCetak='1'
		AND IdPencetak='$pencari'
		GROUP BY OTPCode,NamaPenerima,TglBuat";

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
	while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';

		if (($i % 2)==0){
			$odd = 'even';
		}

		if($row['TglCetak']== ""){
			$odd	= "red";
			$status	= "Not Released";
		}
		else{
			$odd = "green";
			$status	= "Released";
		}

		$arrayJenis = explode(",",$row['jenis']);
		$arrayJumlah = explode(",",$row['jumlahBiaya']);
		foreach($arrayJenis as $k=>$jenis){
			$JenB = $jenis . "<br>";
			$jenisbiaya[$i] .= $JenB;

			$JumB = number_format($arrayJumlah[$k],0,',','.') . "<br>";
			$jumlahbiaya[$i] .= $JumB;
		}

		$template->
		assign_block_vars(
			'ROWTAMBAHAN',
			array(
				'odd'=>$odd,
				'no'=>$i,
				'cabang'=>$row['KodeCabang'],
				'releaser'=>$row['NamaReleaser'],
				'pembuat'=>$row['NamaPembuat'],
				'penerima'=>$row['NamaPenerima'],
				'tgl_buat'=>$row['TglBuat'],
                'tgl_cetak'=>$row['TglCetak'],
				'status'=>$status,
				'jenis_biaya'=>$jenisbiaya[$i],
				'jumlah'=>$jumlahbiaya[$i],
				'ket'=>$row['Keterangan']
			)
		);

		$i++;
	}
}
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}
*/

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p0=".$tanggal_mulai."&p1=".$tanggal_akhir."&p2=".$pencari."&p3=".$jenis_laporan."";
	
$script_cetak_pdf="Start('laporan_rekap_tiket_detail_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_rekap_tiket_detail_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'PENCARI'				=> $pencari,
	'JENIS_LAPORAN'	=> $jenis_laporan,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>