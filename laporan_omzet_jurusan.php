<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
//include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 303;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//################################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kode_cabang  	= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$username				= $userdata['username'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_jurusan/laporan_omzet_jurusan_body.tpl')); 

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeJurusan LIKE '$cari%' 
		OR f_cabang_get_name_by_kode(KodeCabangAsal) LIKE '%$cari%' 
		OR f_cabang_get_name_by_kode(KodeCabangTujuan) LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabangAsal='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Jurusan":$sort_by;
		
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"IdJurusan","tbl_md_jurusan",
"&cabang=$kode_cabang&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
$kondisi_cari,"laporan_omzet_jurusan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql=
	"SELECT 
		IdJurusan,KodeJurusan,f_cabang_get_name_by_kode(KodeCabangAsal) AS CabangAsal,
		f_cabang_get_name_by_kode(KodeCabangTujuan) AS CabangTujuan,
		KodeCabangAsal
	FROM tbl_md_jurusan
	$kondisi_cari";
	
if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  tmj.IdJurusan,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		tmj.IdJurusan IN (SELECT IdJurusan
		FROM tbl_md_jurusan
		$kondisi_cari)
	GROUP BY IdJurusan
	ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['IdJurusan']]	= $row['JadwalDibuka'];
}

//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(IF(CetakTiket!=1,NoTiket,NULL)),0) AS TotalPenumpangB,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='' OR JenisPenumpang='14' OR JenisPenumpang='14_20K' OR JenisPenumpang='14_5K') AND CetakTiket=1  AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='GV' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang LIKE 'R%' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS TotalTiket,
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,SubTotal,0),0)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['IdJurusan']]	= $row;
}

//DATA KEBERANGKATAN BY MANIFEST
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(1),0) AS TotalBerangkat
	FROM tbl_spj
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['IdJurusan']]	= $row['TotalBerangkat'];
}

//DATA KEBERANGKATAN MANIFEST PICKUP/TRANSIT
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(COUNT(DISTINCT(IF(CetakTiket=1,NoSPJ,NULL))),0) AS TotalBerangkat
	FROM tbl_reservasi tr
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 AND (SELECT FlagSubJadwal FROM tbl_md_jadwal tmj WHERE tmj.KodeJadwal=tr.KodeJadwal)=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['IdJurusan']]	= $jumlah_berangkat[$row['IdJurusan']]+$row['TotalBerangkat'];
}


//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['IdJurusan']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		IdJurusan,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY IdJurusan ORDER BY IdJurusan";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['IdJurusan']					= $row['IdJurusan'];
	$temp_array[$idx]['KodeJurusan']				= $row['KodeJurusan'];
	$temp_array[$idx]['Jurusan']						= $row['CabangAsal']."->".$row['CabangTujuan'];
	$temp_array[$idx]['KodeCabangAsal']			= $row['KodeCabangAsal'];
	$temp_array[$idx]['TotalOpenTrip']			= $data_jadwal_tersedia[$row['IdJurusan']];
	$temp_array[$idx]['TotalPenumpangB']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangB'];
	$temp_array[$idx]['TotalPenumpangU']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangV']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalPenumpangG']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangR']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']		= $data_tiket_total[$row['IdJurusan']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalBerangkat']			= $jumlah_berangkat[$row['IdJurusan']];
	$temp_array[$idx]['TotalTiket']					= $data_tiket_total[$row['IdJurusan']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']= $data_tiket_total[$row['IdJurusan']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= $data_tiket_total[$row['IdJurusan']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']= $data_paket_total[$row['IdJurusan']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']					= $data_paket_total[$row['IdJurusan']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']					= $data_biaya_total[$row['IdJurusan']]['TotalBiaya'];
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']							= $temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$idx=$idx_awal_record;

//PLOT DATA
while($idx<($idx_awal_record+$VIEW_PER_PAGE) && $idx<count($temp_array)){
	$odd ='odd';
	
	if (($idx % 2)==0){
		$odd = 'even';
	}
	
	$act 	="<a href='#' onClick='Start(\"".append_sid('laporan_rekap_tiket_detail.php?p0='.$tanggal_mulai.'&p1='.$tanggal_akhir.'&p2='.$temp_array[$idx]['IdJurusan'].'&p3=2')."\");return false'>Detail<a/>";		
	
	/*$act 	.="<a href='".append_sid('laporan_omzet_jurusan_grafik.php?id_jurusan='.$temp_array[$idx]['IdJurusan'].'&bulan='.$bulan.'&tahun='.$tahun.
					'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&cabang='.$temp_array[$idx]['KodeCabang'].'&sort_by='.$sort_by.'&order='.$order)."'>Grafik<a/>";		
	*/
	
	//total tiket
	$total_penjualan_tiket	= $temp_array[$idx]['TotalPenjualanTiket'];
	$total_discount					= $temp_array[$idx]['TotalDiscount'];
	$total_tiket						= $temp_array[$idx]['TotalTiket'];
	
	//total paket
	$total_penjualan_paket	= $temp_array[$idx]['TotalPenjualanPaket'];
	$total_paket						= $temp_array[$idx]['TotalPaket'];
	
	//total biaya
	$total_biaya						= $temp_array[$idx]['TotalBiaya'];
	
	//total
	$total									= $temp_array[$idx]['Total'];
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'=>$odd,
				'no'=>$idx+1,
				'kode_jurusan'=>$temp_array[$idx]['KodeJurusan'],
				'jurusan'=>$temp_array[$idx]['Jurusan'],
				'open_trip'=>number_format($temp_array[$idx]['TotalOpenTrip'],0,",","."),
				'total_keberangkatan'=>number_format($temp_array[$idx]['TotalBerangkat'],0,",","."),
				'total_penumpang_b'=>number_format($temp_array[$idx]['TotalPenumpangB'],0,",","."),
				'total_penumpang_u'=>number_format($temp_array[$idx]['TotalPenumpangU'],0,",","."),
				'total_penumpang_m'=>number_format($temp_array[$idx]['TotalPenumpangM'],0,",","."),
				'total_penumpang_k'=>number_format($temp_array[$idx]['TotalPenumpangK'],0,",","."),
				'total_penumpang_kk'=>number_format($temp_array[$idx]['TotalPenumpangKK'],0,",","."),
				'total_penumpang_v'=>number_format($temp_array[$idx]['TotalPenumpangV'],0,",","."),
				'total_penumpang_g'=>number_format($temp_array[$idx]['TotalPenumpangG'],0,",","."),
				'total_penumpang_r'=>number_format($temp_array[$idx]['TotalPenumpangR'],0,",","."),
				'total_penumpang_vr'=>number_format($temp_array[$idx]['TotalPenumpangVR'],0,",","."),
				'total_penumpang'=>number_format($total_tiket,0,",","."),
				'rata_pnp_per_trip'=>number_format($temp_array[$idx]['TotalPenumpangPerTrip'],0,",","."),
				'total_omzet'=>number_format($total_penjualan_tiket,0,",","."),
				'total_paket'=>number_format($total_paket,0,",","."),
				'total_omzet_paket'=>number_format($total_penjualan_paket,0,",","."),
				'total_discount'=>number_format($total_discount,0,",","."),
				'total_biaya'=>number_format($total_biaya,0,",","."),
				'total_profit'=>number_format($total,0,",","."),
				'act'=>$act
			)
		);
	
	$idx++;
}

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p1=".$tanggal_mulai."&p2=".$tanggal_akhir."&p3=".$temp_array[$idx]['KodeCabangAsal'].
										"&p4=".$cari."&p5=".$sort_by."&p6=".$order."";
	
$script_cetak_pdf="Start('laporan_omzet_jurusan_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_omzet_jurusan_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
	
$page_title	= "Penjualan Jurusan";

$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('laporan_omzet_jurusan.'.$phpEx),
	'TXT_CARI'			=> $cari,
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'NAMA'					=> $userdata['Nama'],
	'SUMMARY'				=> $summary,
	'PAGING'				=> $paging,
	'CETAK_PDF'			=> $script_cetak_pdf,
	'CETAK_XL'			=> $script_cetak_excel,
	'A_SORT_1'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=Jurusan'.$parameter_sorting),
	'TIPS_SORT_1'		=> "Urutkan Nama jurusan ($order_invert)",
	'A_SORT_2'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=KodeJurusan'.$parameter_sorting),
	'TIPS_SORT_2'		=> "Urutkan Kode jurusan ($order_invert)",
	'A_SORT_4'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalBerangkat'.$parameter_sorting),
	'TIPS_SORT_4'		=> "Urutkan Total trip ($order_invert)",
	'A_SORT_5'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenumpangU'.$parameter_sorting),
	'TIPS_SORT_5'		=> "Urutkan Total penumpang umum ($order_invert)",
	'A_SORT_6'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenumpangM'.$parameter_sorting),
	'TIPS_SORT_6'		=> "Urutkan Total penumpang mahasiswa/group ($order_invert)",
	'A_SORT_7'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenumpangK'.$parameter_sorting),
	'TIPS_SORT_7'		=> "Urutkan Total penumpang member ($order_invert)",
	'A_SORT_8'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenumpangKK'.$parameter_sorting),
	'TIPS_SORT_8'		=> "Urutkan Total keluarga karyawan ($order_invert)",
	'A_SORT_9'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenumpangG'.$parameter_sorting),
	'TIPS_SORT_9'		=> "Urutkan Total gratis ($order_invert)",
	'A_SORT_10'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalTiket'.$parameter_sorting),
	'TIPS_SORT_10'	=> "Urutkan Total penumpang ($order_invert)",
	'A_SORT_11'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenumpangPerTrip'.$parameter_sorting),
	'TIPS_SORT_11'	=> "Urutkan Total penumpang/trip($order_invert)",
	'A_SORT_12'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenjualanTiket'.$parameter_sorting),
	'TIPS_SORT_12'	=> "Urutkan Total omzet tiket ($order_invert)",
	'A_SORT_13'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPaket'.$parameter_sorting),
	'TIPS_SORT_13'	=> "Urutkan Total paket ($order_invert)",
	'A_SORT_14'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalPenjualanPaket'.$parameter_sorting),
	'TIPS_SORT_14'	=> "Urutkan Total omzet paket ($order_invert)",
	'A_SORT_15'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalDiscount'.$parameter_sorting),
	'TIPS_SORT_15'	=> "Urutkan Total discount ($order_invert)",
	'A_SORT_16'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=TotalBiaya'.$parameter_sorting),
	'TIPS_SORT_16'	=> "Urutkan Total biaya ($order_invert)",
	'A_SORT_17'			=> append_sid('laporan_omzet_jurusan.'.$phpEx.'?sort_by=Total'.$parameter_sorting),
	'TIPS_SORT_17'	=> "Urutkan Total ($order_invert)",
	'A_SORT_18'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangB'.$parameter_sorting),
	'TIPS_SORT_18'	=> "Urutkan Jumlah Booking ($order_invert)",
	'A_SORT_19'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangV'.$parameter_sorting),
	'TIPS_SORT_19'	=> "Urutkan Total Penumpang ($order_invert)",
	'A_SORT_20'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangR'.$parameter_sorting),
	'TIPS_SORT_20'	=> "Urutkan total tiket return ($order_invert)",
	'A_SORT_21'			=> append_sid('laporan_omzet_cabang.'.$phpEx.'?sort_by=TotalPenumpangVR'.$parameter_sorting),
	'TIPS_SORT_21'	=> "Urutkan total tiket voucher return ($order_invert)",
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>