<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);

$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassCabang.php');
// SESSION
$id_page  = 606;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################
// HEADER

$page_title	= "Menu Daftar Transaksi";
$interface_menu_utama=true;

$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$sort_by		= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order			= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$cari           = isset($HTTP_GET_VARS["cari"])?$HTTP_GET_VARS["cari"]:$HTTP_POST_VARS["cari"];
$Member	= new Member();
$Cabang	= new Cabang();

// LIST
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= date_format(date_create($tanggal_mulai),'Y-m-d');
$tanggal_akhir_mysql	= date_format(date_create($tanggal_akhir),'Y-m-d');

$template->set_filenames(array('body' => 'member/daftar_transaksi_ponta.tpl'));


$kondisi	=($cari=="")?"":
    " AND (tbl_reservasi.Nama LIKE '%$cari%'
				OR tbl_reservasi.Telp LIKE '%$cari%'
				OR tbl_md_member.IdMember LIKE '%$cari%'
				OR tbl_ponta_transaksi.IdCard LIKE '%$cari%'
				OR tbl_ponta_transaksi.KodeBooking LIKE '%$cari%')";
$kondisi_paging = "WHERE (DATE(WaktuTransaksi) BETWEEN '".$tanggal_mulai_mysql."' AND '".$tanggal_akhir_mysql."')".$kondisi;
//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging=pagingData($idx_page,"IdPontaTransaksi","tbl_ponta_transaksi","&cari=$cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",$kondisi_paging,"pengaturan_member_daftar_transaksi_ponta.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql ="SELECT tbl_reservasi.Nama, tbl_reservasi.Telp,tbl_reservasi.NoTiket, 
              tbl_ponta_transaksi.IdCard, tbl_ponta_transaksi.WaktuTransaksi, 
              tbl_reservasi.KodeBooking, tbl_ponta_transaksi.JumlahTransaksi, 
              tbl_ponta_transaksi.JumlahPoin, tbl_ponta_transaksi.JumlahTagihan,
              tbl_md_member.IdMember, tbl_reservasi.FlagBatal, 
              tbl_ponta_transaksi.StatusBatch, tbl_reservasi.FlagVoidReedem
       FROM tbl_reservasi
       JOIN tbl_ponta_transaksi
            ON tbl_ponta_transaksi.NoTiket = tbl_reservasi.NoTiket
       LEFT JOIN tbl_md_member 
            ON tbl_reservasi.IDPONTA = tbl_md_member.NoSeriKartu
	   WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	   AND tbl_reservasi.IDPONTA IS NOT NULL 
	   $kondisi  ORDER BY WaktuTransaksi LIMIT $idx_awal_record,$VIEW_PER_PAGE";

if ($result = $db->sql_query($sql)){
    $i = $idx_page*$VIEW_PER_PAGE+1;
    while ($row = $db->sql_fetchrow($result)){
        $odd ='odd';

        if (($i % 2)==0){
            $odd = 'even';
        }

        if($row['FlagBatal'] == 1 && $row['StatusBatch'] == 1 && $row['FlagVoidReedem'] != 1){
            $odd = 'pink';
        }
        


        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'=>$odd,
                'no'=>$i,
                'nama'=>$row['Nama'],
                'hp'=>$row['Telp'],
                'PontaID'=>$row['IdCard'],
                'IdMember'=>$row['IdMember'],
                'tgl'=>date_format(date_create($row['WaktuTransaksi']),'d-m-Y H:i:s'),
                'kodebooking'=>$row['KodeBooking'],
                'notiket'=>$row['NoTiket'],
                'jmlTransaksi'=>number_format($row['JumlahTransaksi'],0,',','.'),
                'jmlPoin'=>number_format($row['JumlahPoin'],0,',','.'),
                'jmlTagihan'=>number_format($row['JumlahTagihan'],0,',','.')
                )
        );
        $i++;
    }
    if($i-1<=0){
        $no_data	=	"<div class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></div>";
    }
}else{
    die(mysql_error());
    echo("Error :".__LINE__);exit;
}
$page_title	= "Transaksi Ponta";
$order_invert	= ($order=='asc' || $order=='')?'desc':'asc';

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&cari=".$cari."&sort_by=".$sort_by."&order=".$order_invert."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir;

//$script_cetak_pdf="Start('pengaturan_member_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";

$script_cetak_excel="Start('daftar_transaksi_ponta_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
        'BCRUMP'    		=>setBcrump($id_page),
        'ACTION_CARI'		=> append_sid('pengaturan_member_daftar_transaksi_ponta.'.$phpEx),
        'TXT_CARI'			=> $cari,
        'TGL_AWAL'			=> $tanggal_mulai,
        'TGL_AKHIR'			=> $tanggal_akhir,
        'NO_DATA'			=> $no_data,
        'PAGING'			=> $paging,
        'A_SORT_BY_NAMA'	=>  append_sid('pengaturan_member_daftar_transaksi_ponta.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Nama&order='.$order_invert),
        'A_SORT_BY_KODE'	=>  append_sid('pengaturan_member_daftar_transaksi_ponta.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=IdMember&order='.$order_invert),
        'A_SORT_BY_HP'		=>  append_sid('pengaturan_member_daftar_transaksi_ponta.'.$phpEx.'?cari='.$cari.'&tanggal_mulai='.$tanggal_mulai.'&tanggal_akhir='.$tanggal_akhir.'&sort_by=Handphone&order='.$order_invert),
        'CETAK_XL'			=> $script_cetak_excel
    )
);
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>