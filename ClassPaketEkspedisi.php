<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  //redirect('index.'.$phpEx,true);
	exit;
}
//#############################################################################

class Paket{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;

	//CONSTRUCTOR
	function Paket(){
		$this->ID_FILE="C-PKTX";
	}

	//UDPATE TERBARU
  public function generateNoResi($kode_mitra){

    $char = array(
      "6", "C", "I", "O", "U","0",
      "7", "D", "J", "P", "V","1",
      "8", "E", "K", "Q", "W","2",
      "9", "F", "L", "R", "X","3",
      "A", "G", "M", "S", "Y","4",
      "B", "H", "N", "T", "Z","5");

    $y  = date("y");
    $m  = date("m");
    $d  = date("d");
    $h  = date("H");
    $i  = date("i");
    $s  = date("s");

    $kode_mitra = substr(strtoupper($kode_mitra),0,3);

    $key1 = ord(substr($kode_mitra,0,1));
    $key2 = ord(substr($kode_mitra,1,1));
    $key3 = ord(substr($kode_mitra,2,1));

    $matrix1[0] = array($y.rand(1, 99), $m.rand(1, 99), $d.rand(1, 99));
    $matrix1[1] = array($d.rand(1, 99), $y.rand(1, 99), $m.rand(1, 99));
    $matrix1[2] = array($m.rand(1, 99), $d.rand(1, 99), $y.rand(1, 99));

    $matrix2[0] = array($h.rand(1, 99), $i.rand(1, 99), $s.rand(1, 99));
    $matrix2[1] = array($s.rand(1, 99), $h.rand(1, 99), $i.rand(1, 99));
    $matrix2[2] = array($i.rand(1, 99), $s.rand(1, 99), $h.rand(1, 99));

    $row = 0;
    $col = 0;

    for ($r1 = 0; $r1 <= 2; $r1++) {
      for ($c2 = 0; $c2 <= 2; $c2++) {
        $crossmat[$row][$col] = $matrix1[$r1][0] * $matrix2[0][$c2]*$key1 + $matrix1[$r1][1] * $matrix2[1][$c2]*$key2 + $matrix1[$r1][2] * $matrix2[2][$c2]*$key3;
        $col++;
      }
      $row++;
    }

    $no_resi = "";

    foreach ($crossmat as $arr_baris) {
      foreach ($arr_baris as $value) {
        $no_resi .= $char[$value % 36];
        $col++;
      }
      $row++;
    }

    return "DTP".$no_resi;
  }

  function isResiDuplikasi($no_resi){
    global $db;

    $sql = "SELECT COUNT(1) FROM tbl_paket WHERE NoResi='$no_resi'";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $row = $db->sql_fetchrow($result);

    return ($row[0]<=0?false:true);

  }//  END isResiDuplikasi

  function setNoResi(){
    global $db,$config;

    $max_try  = 200;

    $berhasil = false;

    $i=0;

    while($i<$max_try && !$berhasil) {
      $no_resi = $this->generateNoResi($config["kode_mitra"]);
      $berhasil = !($this->isResiDuplikasi($no_resi));
      $i++;
    }

    return ($berhasil?$no_resi:"");
  }

  function registerPaket(
    $KodeCabang, $KodeJadwal,$IdJurusan,
    $TglBerangkat, $JamBerangkat , $NamaPengirim ,
    $AlamatPengirim, $TelpPengirim , $NamaPenerima ,
    $AlamatPenerima, $TelpPenerima,$Discount,
    $JenisDiscount,$HargaSebelumDiscount,$HargaPaket,
    $ChargeJemput, $ChargeAntar,$JumlahKoli,
    $Berat,$KeteranganPaket, $KodeAkunPendapatan,
    $PetugasPenjual, $KomisiPaketSopir, $KodeAkunKomisiPaketSopir,
    $KomisiPaketCSO, $KodeAkunKomisiPaketCSO,$JenisPembayaran,
    $CaraPembayaran,$JenisBarang,$Layanan,
    $PetugasCetakTiket,$JenisKardus,$IsDijemput,
    $IsDiantar,$CabangTujuanAkhir,$IsPaketTransit){

    global $db;

    $no_resi  = $this->setNoResi();

    $sql=
      "INSERT INTO tbl_paket SET NoResi='$no_resi',
        KodeCabang='$KodeCabang', KodeJadwal='$KodeJadwal',IdJurusan='$IdJurusan',
        TglBerangkat='$TglBerangkat', JamBerangkat='$JamBerangkat', NamaPengirim='$NamaPengirim',
        AlamatPengirim='$AlamatPengirim',TelpPengirim='$TelpPengirim',NamaPenerima='$NamaPenerima',
        AlamatPenerima='$AlamatPenerima',TelpPenerima='$TelpPenerima',Discount='$Discount',
        JenisDiscount='$JenisDiscount',HargaSebelumDiscount='$HargaSebelumDiscount',HargaPaket='$HargaPaket',
        ChargeJemput='$ChargeJemput',ChargeAntar='$ChargeAntar',JumlahKoli='$JumlahKoli',
        Berat='$Berat',KeteranganPaket='$KeteranganPaket',KodeAkunPendapatan='$KodeAkunPendapatan',
        PetugasPenjual='$PetugasPenjual',KomisiPaketSopir='$KomisiPaketSopir',KodeAkunKomisiPaketSopir='$KodeAkunKomisiPaketSopir',
        KomisiPaketCSO='$KomisiPaketCSO',KodeAkunKomisiPaketCSO='$KodeAkunKomisiPaketCSO',JenisPembayaran='$JenisPembayaran',
        CaraPembayaran='$CaraPembayaran',JenisBarang='$JenisBarang',Layanan='$Layanan',
        IsEkspedisi=1,PetugasCetakTiket='$PetugasCetakTiket',JenisKardus='$JenisKardus',
        IsDijemput='$IsDijemput',IsDiantar='$IsDiantar',CetakTiket=1,WaktuCetakTiket=NOW(),
        CabangTujuanAkhir='$CabangTujuanAkhir',IsPaketTransit='$IsPaketTransit'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return $no_resi;
  }

  function registerPaketTransit($no_resi){

    global $db;

    $sql=
      "INSERT INTO tbl_paket_transit (
        NoResi,KodeCabang,KodeJadwal,
        IdJurusan,TglBerangkat,JamBerangkat,
        NamaPengirim,AlamatPengirim,TelpPengirim,
        NamaPenerima,AlamatPenerima,TelpPenerima,
        Discount,JenisDiscount,HargaSebelumDiscount,
        HargaPaket,ChargeJemput,ChargeAntar,
        JumlahKoli,Berat,KeteranganPaket,
        KodeAkunPendapatan,PetugasPenjual,KomisiPaketSopir,
        KodeAkunKomisiPaketSopir,KomisiPaketCSO,KodeAkunKomisiPaketCSO,
        JenisPembayaran,CaraPembayaran,JenisBarang,
        Layanan,IsEkspedisi,PetugasCetakTiket,
        JenisKardus,IsDijemput,IsDiantar,
        CetakTiket,WaktuCetakTiket,CabangTujuanAkhir,IsPaketTransit)
      SELECT
        NoResi,KodeCabang,KodeJadwal,
        IdJurusan,TglBerangkat,JamBerangkat,
        NamaPengirim,AlamatPengirim,TelpPengirim,
        NamaPenerima,AlamatPenerima,TelpPenerima,
        Discount,JenisDiscount,HargaSebelumDiscount,
        HargaPaket,ChargeJemput,ChargeAntar,
        JumlahKoli,Berat,KeteranganPaket,
        KodeAkunPendapatan,PetugasPenjual,KomisiPaketSopir,
        KodeAkunKomisiPaketSopir,KomisiPaketCSO,KodeAkunKomisiPaketCSO,
        JenisPembayaran,CaraPembayaran,JenisBarang,
        Layanan,IsEkspedisi,PetugasCetakTiket,
        JenisKardus,IsDijemput,IsDiantar,
        CetakTiket,WaktuCetakTiket,CabangTujuanAkhir,IsPaketTransit
      FROM tbl_paket WHERE NoResi='$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function ambilDaftarHarga($id_jurusan){

    //kamus
    global $db;

    $sql =
      "SELECT
				HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
				HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
				HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
				HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
				HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
				HargaPaket6KiloPertama,HargaPaket6KiloBerikut
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan';";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }//  END ambilDaftarHarga

  function ambilListPaket($tgl_berangkat,$kode_jadwal){

    //kamus
    global $db;

    $sql =
      "SELECT tp.*,tmc.Nama AS NamaCabangTujuanAkhir FROM tbl_paket tp LEFT JOIN tbl_md_cabang tmc ON tp.CabangTujuanAkhir=tmc.KodeCabang
      WHERE TglBerangkat='$tgl_berangkat' AND KodeJadwal='$kode_jadwal'  AND FlagBatal=0
      ORDER BY WaktuCetakTiket;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return $result;

  }//  END ambilListPaket

  function ambilSummary($tgl,$kode_jadwal){

    //kamus
    global $db;

    $sql= "SELECT KodeJadwalUtama FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal' AND FlagSubJadwal=1";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    $kode_jadwal_utama  = $row["KodeJadwalUtama"];

    //MENGAMBIL DATA SPJ
    $sql =
      "SELECT
				tm.NoManifest AS NoManifestInduk,tm.NoPlat,tm.KodeUnit,tm.KodeDriver,tm.Driver,tm.WaktuCetakManifest AS WaktuCetakManifestInduk,
				tmp.NoManifest AS NoManifestPaket,tmp.WaktuCetak AS WaktuCetakManifestPaket
			FROM tbl_manifest tm LEFT JOIN tbl_manifest_paket tmp ON tm.NoManifest=tmp.NoManifestInduk AND tmp.KodeJadwal='$kode_jadwal' AND tmp.TglBerangkat='$tgl'
			WHERE tm.KodeJadwal='".($kode_jadwal_utama==""?$kode_jadwal:$kode_jadwal_utama)."' AND tm.TglBerangkat='$tgl'";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    $data_summary['NoManifestInduk']	        = $row['NoManifestInduk'];
    $data_summary['NoManifestPaket']		      = $row['NoManifestPaket'];
    $data_summary['KodeUnit']                 = $row['KodeUnit'];
    $data_summary['NoPlat']	                  = $row['NoPlat'];
    $data_summary['KodeDriver']		            = $row['KodeDriver'];
    $data_summary['Driver']				            = $row['Driver'];
    $data_summary['WaktuCetakManifestInduk']	= $row['WaktuCetakManifestInduk'];
    $data_summary['WaktuCetakManifestPaket']	= $row['WaktuCetakManifestPaket'];
    $data_summary["KodeJadwalUtama"]          = $kode_jadwal_utama;

    //MENGAMBIL TOTAL OMZET DAN PAKET

    $sql =
      "SELECT COUNT(1) AS TotalPaket, SUM(HargaPaket) AS TotalOmzet
			FROM tbl_paket
			WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl' AND FlagBatal=0";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    $data_summary["TotalPaket"]  = $row["TotalPaket"];
    $data_summary["TotalOmzet"]   = $row["TotalOmzet"];

    return $data_summary;

  }//  END ambilSummary

  function ambilDetailDataPaket($no_resi){

    //kamus
    global $db;

    //MENGAMBIL DATA SPJ
    $sql =
      "SELECT tp.*,KodeCabangTujuan,f_cabang_get_name_by_kode(KodeCabangAsal) AS Asal,f_cabang_get_name_by_kode(CabangTujuanAkhir) AS Tujuan,tu.nama AS NamaPetugas
			FROM (tbl_paket tp LEFT JOIN tbl_user tu ON tp.PetugasCetakTiket=tu.user_id) LEFT JOIN tbl_md_jurusan tmj ON tp.IdJurusan=tmj.IdJurusan
			WHERE NoResi='$no_resi'";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $sql $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return $row;

  }//  END ambilDetailDataPaket

  function updateCetakTiket($no_resi){

    global $db;

    $sql =
      "UPDATE tbl_paket SET
        CetakTiket=2,RequestCetakUlang=0,OTPCetak=NULL,
        Log=".$this->catatLog("CETAK","mencetak tiket")."
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "DELETE FROM tbl_paket_request_otp_cetak
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function setRequestCetakUlang($no_resi,$alasan){

    global $db,$userdata;

    $sql =
      "UPDATE tbl_paket SET
        RequestCetakUlang=1,PetugasRequestOTPCetak=$userdata[user_id],
        Log=".$this->catatLog("REQUEST CETAK ULANG",$alasan)."
      WHERE NoResi = '$no_resi' AND RequestCetakUlang=0";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    //AMBIL DATA RESI
    $data_paket = $this->ambilDetailDataPaket($no_resi);

    $sql =
      "INSERT INTO tbl_paket_request_otp_cetak SET
        NoResi='$no_resi',TglBerangkat='$data_paket[TglBerangkat]',IdJurusan='$data_paket[IdJurusan]',
        KodeJadwal='$data_paket[KodeJadwal]',JamBerangkat='$data_paket[JamBerangkat]',NamaPengirim='$data_paket[NamaPengirim]',
        NamaPenerima='$data_paket[NamaPenerima]',Keterangan='$data_paket[KeteranganPaket]',PetugasRequest=$userdata[user_id],
        NamaPetugasRequest='$userdata[nama]',WaktuRequest=NOW(),Alasan='$alasan'";

    if (!$db->sql_query($sql)){
      //echo("Err:  $this->ID_FILE".__LINE__);exit;
      //JIKA ERROR BIARKAN SAJA, KARENA KEMUNGKINAN DUPLIKASI
    }

    return true;
  }

  function setOTPCetak($no_resi){

    global $db,$userdata;

    $otp  = rand(111111,999999);

    //SET OTP
    $sql =
      "UPDATE tbl_paket_request_otp_cetak SET
        PetugasApproved='$userdata[user_id]',NamaPetugasApproved='$userdata[nama]',WaktuApproved=NOW(),OTP='$otp'
      WHERE NoResi = '$no_resi' AND (OTP IS NULL OR OTP='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "UPDATE tbl_paket SET
        OTPCetak='$otp',PetugasOtorisasiCetak=$userdata[user_id],WaktuOtorisasiCetak=NOW(),
        Log=".$this->catatLog("APPROVED CETAK ULANG","cetak ulang disetujui")."
      WHERE NoResi = '$no_resi' AND (OTPCetak IS NULL OR OTPCetak='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return $otp;
  }

  function verifyOTPCetakUlang($no_resi,$otp){

    global $db;

    if($otp==""){
      return false;
    }

    $sql =
      "SELECT COUNT(1) AS Verified
      FROM tbl_paket
      WHERE NoResi='$no_resi' AND FlagBatal=0 AND RequestCetakUlang=1 AND OTPCetak='$otp'";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return ($row[0]>0?true:false);
  }

  function ambilDataRequestOTPCetak($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("NoResi","TglBerangkat","KodeJadwal",
      "JamBerangkat","NamaPengirim","NamaPenerima",
      "Keterangan","NamaPetugasRequest","WaktuRequest",
      "Alasan","OTP","NamaPetugasApproved",
      "WaktuApproved");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1 ".($cari==""?"":" AND (NoResi LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%'
      OR NamaPenerima LIKE '%$cari%' OR NamaPetugasRequest LIKE '$cari%' OR NamaPetugasApproved LIKE '$cari%' OR Keterangan LIKE '%$cari%')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";

    $sql =
      "SELECT SQL_CALC_FOUND_ROWS *
			FROM tbl_paket_request_otp_cetak
			$set_kondisi $set_kondisi_tambahan
			$order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDataRequestOTPCetak

  function setRequestBatal($no_resi,$alasan){

    global $db,$userdata;

    $sql =
      "UPDATE tbl_paket SET
        RequestBatal=1,PetugasRequestOTPBatal=$userdata[user_id],
        Log=".$this->catatLog("REQUEST PEMBATALAN",$alasan)."
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    //AMBIL DATA RESI
    $data_paket = $this->ambilDetailDataPaket($no_resi);

    $sql =
      "INSERT INTO tbl_paket_request_otp_batal SET
        NoResi='$no_resi',TglBerangkat='$data_paket[TglBerangkat]',IdJurusan='$data_paket[IdJurusan]',
        KodeJadwal='$data_paket[KodeJadwal]',JamBerangkat='$data_paket[JamBerangkat]',NamaPengirim='$data_paket[NamaPengirim]',
        NamaPenerima='$data_paket[NamaPenerima]',Keterangan='$data_paket[KeteranganPaket]',PetugasRequest=$userdata[user_id],
        NamaPetugasRequest='$userdata[nama]',WaktuRequest=NOW(),Alasan='$alasan'";

    if (!$db->sql_query($sql)){
      //echo("Err:  $this->ID_FILE".__LINE__);exit;
      //JIKA ERROR BIARKAN SAJA, KARENA KEMUNGKINAN DUPLIKASI
    }

    return true;
  }

  function setOTPBatal($no_resi){

    global $db,$userdata;

    $otp  = rand(111111,999999);

    //SET OTP
    $sql =
      "UPDATE tbl_paket_request_otp_batal SET
        PetugasApproved='$userdata[user_id]',NamaPetugasApproved='$userdata[nama]',WaktuApproved=NOW(),OTP='$otp'
      WHERE NoResi = '$no_resi' AND (OTP IS NULL OR OTP='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "UPDATE tbl_paket SET
        OTPBatal='$otp',PetugasOtorisasiBatal=$userdata[user_id],WaktuOtorisasiBatal=NOW(),
        Log=".$this->catatLog("APPROVED PEMBATALAN","pembatalan disetujui")."
      WHERE NoResi = '$no_resi' AND (OTPBatal IS NULL OR OTPBatal='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return $otp;
  }

  function verifyOTPBatal($no_resi,$otp){

    global $db;

    if($otp==""){
      return false;
    }

    $sql =
      "SELECT COUNT(1) AS Verified
      FROM tbl_paket
      WHERE NoResi='$no_resi' AND FlagBatal=0 AND RequestBatal=1 AND OTPBatal='$otp'";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return ($row[0]>0?true:false);
  }

  function ambilDataRequestOTPBatal($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("NoResi","TglBerangkat","KodeJadwal",
      "JamBerangkat","NamaPengirim","NamaPenerima",
      "Keterangan","NamaPetugasRequest","WaktuRequest",
      "Alasan","OTP","NamaPetugasApproved",
      "WaktuApproved");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1 ".($cari==""?"":" AND (NoResi LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%'
      OR NamaPenerima LIKE '%$cari%' OR NamaPetugasRequest LIKE '$cari%' OR NamaPetugasApproved LIKE '$cari%' OR Keterangan LIKE '%$cari%' OR Alasan LIKE '%$cari%')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";

    $sql =
      "SELECT SQL_CALC_FOUND_ROWS *
			FROM tbl_paket_request_otp_batal
			$set_kondisi $set_kondisi_tambahan
			$order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDataRequestOTPBatal

  function batalkanResi($no_resi){

    global $db,$userdata;

    $sql =
      "UPDATE tbl_paket SET
        FlagBatal=1,RequestBatal=0,WaktuPembatalan=NOW(),PetugasPembatalan='$userdata[user_id]',
        Log=".$this->catatLog("PEMBATALAN","proses pembatalan resi")."
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "DELETE FROM tbl_paket_request_otp_batal
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "DELETE FROM tbl_paket_transit
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function setRequestMutasi($no_resi,$alasan){

    global $db,$userdata;

    $sql =
      "UPDATE tbl_paket SET
        RequestMutasi=1,PetugasRequestOTPMutasi=$userdata[user_id],
        Log=".$this->catatLog("REQUEST MUTASI",$alasan)."
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    //AMBIL DATA RESI
    $data_paket = $this->ambilDetailDataPaket($no_resi);

    $sql =
      "INSERT INTO tbl_paket_request_otp_mutasi SET
        NoResi='$no_resi',TglBerangkat='$data_paket[TglBerangkat]',IdJurusan='$data_paket[IdJurusan]',
        KodeJadwal='$data_paket[KodeJadwal]',JamBerangkat='$data_paket[JamBerangkat]',NamaPengirim='$data_paket[NamaPengirim]',
        NamaPenerima='$data_paket[NamaPenerima]',Keterangan='$data_paket[KeteranganPaket]',PetugasRequest=$userdata[user_id],
        NamaPetugasRequest='$userdata[nama]',WaktuRequest=NOW(),Alasan='$alasan'";

    if (!$db->sql_query($sql)){
      //echo("Err:  $this->ID_FILE".__LINE__);exit;
      //JIKA ERROR BIARKAN SAJA, KARENA KEMUNGKINAN DUPLIKASI
    }

    return true;
  }

  function setOTPMutasi($no_resi){

    global $db,$userdata;

    $otp  = rand(111111,999999);

    //SET OTP
    $sql =
      "UPDATE tbl_paket_request_otp_mutasi SET
        PetugasApproved='$userdata[user_id]',NamaPetugasApproved='$userdata[nama]',WaktuApproved=NOW(),OTP='$otp'
      WHERE NoResi = '$no_resi' AND (OTP IS NULL OR OTP='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "UPDATE tbl_paket SET
        OTPMutasi='$otp',PetugasOtorisasiMutasi=$userdata[user_id],WaktuOtorisasiMutasi=NOW(),
        Log=".$this->catatLog("APPROVED MUTASI","mutasi disetujui")."
      WHERE NoResi = '$no_resi' AND (OTPMutasi IS NULL OR OTPMutasi='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return $otp;
  }

  function verifyOTPMutasi($no_resi,$otp){

    global $db;

    if($otp==""){
      return false;
    }

    $sql =
      "SELECT COUNT(1) AS Verified
      FROM tbl_paket
      WHERE NoResi='$no_resi' AND FlagBatal=0 AND RequestMutasi=1 AND OTPMutasi='$otp'";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return ($row[0]>0?true:false);
  }

  function ambilDataRequestOTPMutasi($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("NoResi","TglBerangkat","KodeJadwal",
      "JamBerangkat","NamaPengirim","NamaPenerima",
      "Keterangan","NamaPetugasRequest","WaktuRequest",
      "Alasan","OTP","NamaPetugasApproved",
      "WaktuApproved");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1 ".($cari==""?"":" AND (NoResi LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%'
      OR NamaPenerima LIKE '%$cari%' OR NamaPetugasRequest LIKE '$cari%' OR NamaPetugasApproved LIKE '$cari%' OR Keterangan LIKE '%$cari%')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";

    $sql =
      "SELECT SQL_CALC_FOUND_ROWS *
			FROM tbl_paket_request_otp_mutasi
			$set_kondisi $set_kondisi_tambahan
			$order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDataRequestOTPBatal

  function mutasikanResi($no_resi,$id_jurusan,$tgl_berangkat,$kode_jadwal,$jam_berangkat){

    global $db,$config;

    //MENGAMBIL DATA MANIFEST APAKAH SUDAH PERNAH DICETAK
    $data_manifest= $this->cekManifestPaketByKeberangkatan($tgl_berangkat,$kode_jadwal);

    //JIKA MANIFEST TUJUAN MUTASI SUDAH PERNAK DICETAK, PROSES MUTASI DITOLAK, TIDAK BOLEH MUTASI KE JADWAL YANG SUDAH BERANGKAT ATAU KE JAM YANG SUDAH LALU
    if($data_manifest["NoManifest"]!="" || !verifySelisihWaktu($tgl_berangkat,$jam_berangkat,$config["toleransi_buka_jam_lalu"])){
      return false;
    }

    //AMBIL DATA PAKET SEBELUM MUTASI
    $data_paket = $this->ambilDetailDataPaket($no_resi);

    //AMBIL ALASAN
    $sql= "SELECT Alasan FROM tbl_paket_request_otp_mutasi WHERE NoResi='$no_resi'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $row    = $db->sql_fetchrow($result);
    $alasan = $row["Alasan"];

    $sql =
      "UPDATE tbl_paket SET
        RequestMutasi=0,TglBerangkat='$tgl_berangkat',KodeJadwal='$kode_jadwal',IdJurusan='$id_jurusan',JamBerangkat='$jam_berangkat',OTPMutasi=NULL,
        NoManifest=NULL,WaktuCetakManifest=NULL,CetakManifest=0,
        Log=".$this->catatLog("MUTASI","proses mutasi dari tgl $data_paket[TglBerangkat] $data_paket[KodeJadwal] ke tgl $tgl_berangkat $kode_jadwal karena $alasan")."
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "DELETE FROM tbl_paket_request_otp_mutasi
      WHERE NoResi = '$no_resi'";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return true;
  }

  function getJenisKardus($jenis_kardus){
    global $db;

    $sql =
      "SELECT NamaJenisKardus,Keterangan from tbl_jenis_kardus where IdJenisKardus='$jenis_kardus'";

    if ($result = $db->sql_query($sql)){
      $row = $db->sql_fetchrow($result);
      return $row['NamaJenisKardus']." - ".$row['Keterangan'];
    }
    else{
      die_error("Err: $this->ID_FILE".__LINE__);
    }
  }

  function getListKota(){
    global $db;

    $sql  =
      "SELECT * FROM tbl_md_kota ORDER BY NamaKota";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);
    }

    return $result;
  }

  function getListPointAsal($kota_asal){
    global $db;

    $sql =
      "SELECT KodeCabang,Nama,Kota
			FROM tbl_md_cabang
			WHERE Kota='$kota_asal' AND FlagAgen!=1
			ORDER BY Nama;";

    if (!$result = $db->sql_query($sql)){
      echo("Err:$sql $this->ID_FILE". __LINE__);exit;
    }

    return $result;

  }

  function getListPointTujuan($cabang_asal){
    global $db;

    $sql =
      "SELECT Kota,IdJurusan,tmc.Nama AS Tujuan,KodeJurusan
			FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON tmj.KodeCabangTujuan=tmc.KodeCabang
			WHERE FlagOperasionalJurusan IN(1,2) AND KodeCabangAsal='$cabang_asal' AND FlagAktif=1
			ORDER BY Kota,Tujuan;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE". __LINE__);exit;
    }

    return $result;

  }//  END getListPointTujuan

  function getDataCabangTujuan($jurusan){
    global $db;

    $sql= "SELECT tmc.Nama,tmc.KodeCabang,Kota FROM tbl_md_jurusan tmj INNER JOIN tbl_md_cabang tmc ON tmj.KodeCabangTujuan=tmc.KodeCabang WHERE IdJurusan='$jurusan'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $row  = $db->sql_fetchrow($result);

    return $row;

  }

  private function catatLog($action,$keterangan){
    global $userdata;

    return "CONCAT(IF(Log IS NULL,'',Log),'action=$action,keterangan=$keterangan,waktu=',NOW(),',oleh=$userdata[nama]#$userdata[user_id];')";
  }

  function cariPaketPelanggan($cari){

    //kamus
    global $db;

    $data_return=array();

    //MENCARI DI TABEL TRANSIT
    $sql=
      "SELECT NoResi,TglBerangkat,KodeJadwal,JamBerangkat,f_cabang_get_name_by_kode(CabangTujuanAkhir) AS Tujuan,CetakManifest,WaktuCetakManifest,IsTransit,WaktuTransit
      FROM tbl_paket_transit tp LEFT JOIN tbl_user tu ON tp.PetugasPemberi=tu.user_id
      WHERE (NoResi LIKE '%$cari' OR TelpPengirim='$cari' OR TelpPenerima='$cari') AND FlagBatal=0 AND IsTransit=1
      ORDER BY TglBerangkat DESC,JamBerangkat DESC
      LIMIT 0,1";

    if (!$result_transit = $db->sql_query($sql)){
      echo("Err:$sql $this->ID_FILE".__LINE__);exit;
    }

    while($row=$db->sql_fetchrow($result_transit)){
      $data_return[] = $row;
    }

    $sql=
      "SELECT NoResi,TglBerangkat,KodeJadwal,JamBerangkat,f_cabang_get_name_by_kode(CabangTujuanAkhir) AS Tujuan,
        CetakManifest,WaktuCetakManifest,StatusDiambil,NamaPengambil,WaktuPengambilan,tu.nama AS NamaPetugasPemberi,
        IsSampai, WaktuSampai,IsDispatch,WaktuDispatch
      FROM tbl_paket tp LEFT JOIN tbl_user tu ON tp.PetugasPemberi=tu.user_id
      WHERE (NoResi LIKE '%$cari' OR TelpPengirim='$cari' OR TelpPenerima='$cari') AND FlagBatal=0 ORDER BY WaktuCetakTiket ASC";

    if (!$result_paket = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    while($row=$db->sql_fetchrow($result_paket)) {
      $data_return[] = $row;
    }

    return $data_return;

  }//  END cariPaketPelanggan

  function cekManifestPaketByKeberangkatan($tgl_berangkat,$kode_jadwal){

    //kamus
    global $db;

    //MENGAMBIL DATA JADWAL UTAMA
    $sql= "SELECT IF(FlagSubJadwal=0,KodeJadwal,KodeJadwalUtama) AS KodeJadwalUtama FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";


    if (!$result = $db->sql_query($sql)){
      echo("Err: $sql $this->ID_FILE".__LINE__);exit;
    }

    $data_jadwal=$db->sql_fetchrow($result);

    //MENGAMBIL DATA MANIFEST
    $sql =
      "SELECT tm.NoManifest AS NoManifestInduk,tmp.NoManifest FROM tbl_manifest tm LEFT JOIN tbl_manifest_paket tmp ON tm.NoManifest=tmp.NoManifestInduk AND tmp.TglBerangkat='$tgl_berangkat' AND tmp.KodeJadwal='$kode_jadwal'
			WHERE tm.TglBerangkat='$tgl_berangkat' AND tm.KodeJadwal='$data_jadwal[KodeJadwalUtama]'";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $sql $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return $row;

  }//  END cekManifestPaketByKeberangkatan

  function verifyOTPCetakUlangManifest($no_manifest,$otp){

    global $db;

    if($otp==""){
      return false;
    }

    $sql =
      "SELECT COUNT(1) AS Verified
      FROM tbl_manifest_paket
      WHERE NoManifest='$no_manifest' AND RequestCetakUlang=1 AND OTPCetak='$otp'";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return ($row[0]>0?true:false);
  }

  function ambilDataRequestOTPCetakManifest($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array("NoManifest","TglBerangkat","KodeJadwal",
      "JamBerangkat","KodeUnit","NoPlat",
      "KodeDriver","Driver","NamaPetugasRequest",
      "WaktuRequest","Alasan","OTP",
      "NamaPetugasApproved","WaktuApproved");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = "WHERE 1 ".($cari==""?"":" AND (NoManifest LIKE '%$cari%' OR KodeUnit LIKE '%$cari%'
      OR NoPlat LIKE '%$cari%'OR KodeDriver LIKE '%$cari%'OR Driver LIKE '%$cari%' OR NamaPetugasRequest LIKE '$cari%' OR NamaPetugasApproved LIKE '$cari%' OR Alasan LIKE '%$cari%')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";

    $sql =
      "SELECT SQL_CALC_FOUND_ROWS *
			FROM tbl_manifest_paket_request_otp_cetak
			$set_kondisi $set_kondisi_tambahan
			$order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDataRequestOTPCetakManifest

  function setRequestCetakManifest($no_manifest,$alasan){

    global $db,$userdata;

    $sql =
      "UPDATE tbl_manifest_paket  SET
        RequestCetakUlang=1,PetugasRequestCetak=$userdata[user_id],
        Log=".$this->catatLog("REQUEST CETAK ULANG",$alasan)."
      WHERE NoManifest = '$no_manifest' AND RequestCetakUlang=0";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql  =
      "SELECT * FROM tbl_manifest_paket WHERE NoManifest='$no_manifest'";

    if (!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $data_manifest  = $db->sql_fetchrow($result);

    $sql =
      "INSERT INTO tbl_manifest_paket_request_otp_cetak SET
        NoManifest='$no_manifest',TglBerangkat='$data_manifest[TglBerangkat]',KodeJadwal='$data_manifest[KodeJadwal]',
        JamBerangkat='$data_manifest[JamBerangkat]',KodeUnit='$data_manifest[KodeUnit]',NoPlat='$data_manifest[NoPlat]',
        KodeDriver='$data_manifest[KodeDriver]',Driver='$data_manifest[Driver]',PetugasRequest=$userdata[user_id],
        NamaPetugasRequest='$userdata[nama]',WaktuRequest=NOW(),Alasan='$alasan'";

    if (!$db->sql_query($sql)){
      //echo("Err:  $this->ID_FILE".__LINE__);exit;
      //JIKA ERROR BIARKAN SAJA, KARENA KEMUNGKINAN DUPLIKASI
    }

    return true;
  }

  function setOTPCetakManifest($no_manifest){

    global $db,$userdata;

    $otp  = rand(111111,999999);

    //SET OTP
    $sql =
      "UPDATE tbl_manifest_paket_request_otp_cetak SET
        PetugasApproved='$userdata[user_id]',NamaPetugasApproved='$userdata[nama]',WaktuApproved=NOW(),OTP='$otp'
      WHERE NoManifest = '$no_manifest' AND (OTP IS NULL OR OTP='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $sql =
      "UPDATE tbl_manifest_paket SET
        OTPCetak='$otp',PetugasOtorisasiCetak=$userdata[user_id],WaktuOtorisasiCetak=NOW(),
        Log=".$this->catatLog("APPROVED CETAK ULANG","cetak ulang disetujui")."
      WHERE NoManifest = '$no_manifest' AND (OTPCetak IS NULL OR OTPCetak='')";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    return $otp;
  }

  function ambilDataManifest($no_manifest){
    global $db;

    //MENGAMBIL DATA MANIFEST
    $sql =
      "SELECT tmp.*,f_cabang_get_name_by_kode(KodeCabangAsal) AS Asal,f_cabang_get_name_by_kode(KodeCabangTujuan) AS Tujuan,tu.nama AS NamaPencetakManifest
      FROM (tbl_manifest_paket tmp LEFT JOIN tbl_md_jurusan tmj ON tmp.IdJurusan) LEFT JOIN tbl_user tu ON tu.user_id=tmp.PencetakManifest
			WHERE NoManifest='$no_manifest'";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    $row=$db->sql_fetchrow($result);

    return $row;

  }

  function buatManifest($no_manifest_induk,$tgl_berangkat,$jadwal){
    global $db,$userdata;

    //AMBIL DETAIL JADWAL
    $sql  = "SELECT tmj.IdJurusan,KodeCabangAsal,JamBerangkat FROM tbl_md_jadwal tmj LEFT JOIN tbl_md_jurusan tjr ON tmj.IdJurusan=tjr.IdJurusan WHERE KodeJadwal='$jadwal'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_jadwal  = $db->sql_fetchrow($result);

    //AMBIL DETAIL MANIFEST INDUK
    $sql  = "SELECT KodeUnit,NoPlat,KodeDriver,Driver FROM tbl_manifest WHERE NoManifest='$no_manifest_induk'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_manifest_induk  = $db->sql_fetchrow($result);

    //AMBIL SUMMARY PAKET
    $sql  =
      "SELECT COUNT(1) AS TotalPaket,SUM(HargaPaket+ChargeJemput+ChargeAntar) AS TotalOmzetPaket
      FROM tbl_paket
      WHERE TglBerangkat='$tgl_berangkat' AND KodeJadwal='$jadwal' AND FlagBatal=0";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_summary  = $db->sql_fetchrow($result);

    //CREATE MANIFEST
    $no_manifest  = $no_manifest_induk."-PK-".$data_jadwal["KodeCabangAsal"];

    $sql  =
      "INSERT INTO tbl_manifest_paket SET
        NoManifest='$no_manifest',NoManifestInduk='$no_manifest_induk',TglBerangkat='$tgl_berangkat',
        IdJurusan='$data_jadwal[IdJurusan]',KodeJadwal='$jadwal',JamBerangkat='$data_jadwal[JamBerangkat]',
        JumlahPaket='$data_summary[TotalPaket]',TotalOmzetPaket='$data_summary[TotalOmzetPaket]',KodeUnit='$data_manifest_induk[KodeKendaraan]',
        NoPlat='$data_manifest_induk[NoPolisi]',KodeDriver='$data_manifest_induk[KodeDriver]',Driver='$data_manifest_induk[Driver]',
        PencetakManifest='$userdata[user_id]',WaktuCetak=NOW(),InsentifSopir=0,
        IsEkspedisi=1,Log=".$this->catatLog("CETAK","cetak manifest pertama")."
      ON DUPLICATE KEY UPDATE
        JumlahPaket='$data_summary[TotalPaket]',TotalOmzetPaket='$data_summary[TotalOmzetPaket]',KodeUnit='$data_manifest_induk[KodeKendaraan]',
        NoPlat='$data_manifest_induk[NoPolisi]',KodeDriver='$data_manifest_induk[KodeDriver]',Driver='$data_manifest_induk[Driver]',
        RequestCetakUlang=0,WaktuRequestCetak=NULL,PetugasRequestCetak=NULL,
        PetugasOtorisasiCetak=NULL,WaktuOtorisasiCetak=NULL,OTPCetak=NULL,
        Log=".$this->catatLog("CETAK ULANG","mencetak ulang manifest");

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    //MENGHAPUS REQUEST CETAK ULANG
    $sql  = "DELETE FROM tbl_manifest_paket_request_otp_cetak WHERE NoManifest='$no_manifest'";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $sql  =
      "UPDATE tbl_paket SET
        NoManifest='$no_manifest',CetakManifest=1,WaktuCetakManifest=NOW()
      WHERE TglBerangkat='$tgl_berangkat' AND KodeJadwal='$jadwal' AND FlagBatal=0";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    return $no_manifest;

  }

  function setRequestCetakUlangManifest($no_manifest,$alasan){

    global $db,$userdata;

    $sql =
      "UPDATE tbl_manifest_paket SET
        RequestCetakUlang=1,PetugasRequestCetak=$userdata[user_id],WaktuRequestCetak=NOW(),
        Log=".$this->catatLog("REQUEST CETAK ULANG",$alasan)."
      WHERE NoManifest = '$no_manifest' AND RequestCetakUlang=0";

    if (!$db->sql_query($sql)){
      echo("Err $sql: $this->ID_FILE".__LINE__);exit;
    }

    //AMBIL DATA RESI
    $data_manifest = $this->ambilDataManifest($no_manifest);

    $sql =
      "INSERT INTO tbl_manifest_paket_request_otp_cetak SET
        NoManifest='$no_manifest',TglBerangkat='$data_manifest[TglBerangkat]',KodeJadwal='$data_manifest[KodeJadwal]',
        JamBerangkat='$data_manifest[JamBerangkat]',KodeUnit='$data_manifest[KodeUnit]',NoPlat='$data_manifest[NoPlat]',
        KodeDriver='$data_manifest[KodeDriver]',Driver='$data_manifest[Driver]',PetugasRequest=$userdata[user_id],
        NamaPetugasRequest='$userdata[nama]',WaktuRequest=NOW(),Alasan='$alasan'";

    if (!$db->sql_query($sql)){
      //echo("Err: $sql  $this->ID_FILE".__LINE__);exit;
      //JIKA ERROR BIARKAN SAJA, KARENA KEMUNGKINAN DUPLIKASI
    }

    return true;
  }

  function ambilDaftarManifest($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array(
      "NoManifestInduk","ListManifestPaket","JumlahManifestPaket",
      "TglBerangkat","Asal","Tujuan",
      "KodeJadwal","JamBerangkat","PaketTermanifest",
      "PaketBelumManifest","PaketSampai","WaktuCetakManifest",
      "KodeUnit", "NoPlat","KodeDriver",
      "Driver");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = ($cari==""?"":" AND (tp.NoResi LIKE '%$cari' OR tmc1.Nama LIKE '$cari%' OR tp.NoManifest LIKE '%$cari%' OR tm.KodeUnit='$cari' OR tm.NoPlat LIKE '%$cari' OR tm.NoManifest LIKE '%$cari%')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";
    $sql=
      "SELECT
        SQL_CALC_FOUND_ROWS
        tm.NoManifest AS NoManifestInduk,
        GROUP_CONCAT(DISTINCT tp.NoManifest ORDER BY tm.WaktuCetakManifest SEPARATOR ',') AS ListManifestPaket,
        COUNT(DISTINCT tp.NoManifest) AS JumlahManifestPaket,
        tm.TglBerangkat,
        tmc1.Nama AS Asal,
        tmc2.Nama AS Tujuan,
        tm.KodeJadwal,
        tm.JamBerangkat,
        COUNT(IF(tp.NoManifest IS NOT NULL AND IsSampai=0,1,NULL)) AS PaketTermanifest,
        COUNT(IF(tp.NoManifest IS NULL AND IsSampai=0,1,NULL)) AS PaketBelumManifest,
        COUNT(IF(IsSampai=1,1,NULL)) AS PaketSampai,
        tm.WaktuCetakManifest,
        tm.KodeUnit,
        tm.NoPlat,
        tm.KodeDriver,
        tm.Driver
      FROM (((tbl_manifest tm LEFT JOIN tbl_paket tp ON tm.TglBerangkat=tp.TglBerangkat AND tm.KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(tp.KodeJadwal)) LEFT JOIN tbl_md_jurusan tmj ON tmj.IdJurusan=tm.Idjurusan) LEFT JOIN tbl_md_cabang tmc1 ON tmj.KodeCabangAsal=tmc1.KodeCabang) LEFT JOIN tbl_md_cabang tmc2 ON tmj.KodeCabangTujuan=tmc2.KodeCabang
      WHERE FlagBatal=0 AND tm.KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(tm.KodeJadwal) $set_kondisi $set_kondisi_tambahan
      GROUP BY tm.TglBerangkat,f_jadwal_ambil_kodeutama_by_kodejadwal(tm.KodeJadwal)
      $order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDaftarManifest

  function ambilDaftarManifestPaketByManifestInduk($no_manifest_induk){

    //kamus
    global $db;

    $sql=
      "SELECT
        NoManifest,TglBerangkat,KodeJadwal
      FROM tbl_manifest_paket
      WHERE NoManifestInduk='$no_manifest_induk'
      ORDER BY TglBerangkat,JamBerangkat";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDaftarManifestPaketByManifestInduk

  function ambilDaftarPaket($parameters){

    global $db;

    if($parameters["NoManifest"]!=""){
      $kondisi  = " AND Nomanifest='$parameters[NoManifest]'";
    }
    else{
      $kondisi  = " AND TglBerangkat='$parameters[TglBerangkat]' AND KodeJadwal='$parameters[KodeJadwal]'";
    }


    $sql=
      "SELECT
          NoResi,tmc.Nama AS TujuanAkhir,NamaPengirim,
          TelpPengirim,AlamatPengirim,NamaPenerima,
          TelpPenerima,AlamatPenerima,Berat,
          Layanan,KeteranganPaket,CetakManifest,
          IsSampai,WaktuSampai
      FROM tbl_md_cabang tmc RIGHT JOIN tbl_paket tp ON tmc.KodeCabang=tp.CabangTujuanAkhir
      WHERE FlagBatal=0 $kondisi
      ORDER BY WaktuCetakTiket";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDaftarPaket

  function checkInPaket($list_resi,$no_manifest){
    global $db,$userdata,$config;

    //MENGAMBIL DATA MANIFEST
    $sql="SELECT WaktuCetak FROM tbl_manifest_paket WHERE NoManifest='$no_manifest'";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_manifest  = $db->sql_fetchrow($result);

    if($config["otp_paket"]){ //JIKA OTP PEMGAMBILAN PAKET DIAKTIFKAN, MAKA AKAN MENGENERATE QUERY UTK KODE OTP
      $query_kode_otp = ",OTPPengambilan=(IF(IsDiantar=0,FLOOR(RAND() * 888888) + 111111,NULL))" ;
    }

    //MENGUPDATE DATA PAKET
    $sql=
      "UPDATE tbl_paket SET
        NoManifest='$no_manifest',WaktuCetakManifest='$data_manifest[WaktuCetak]',CetakManifest=1,
        IsSampai=1,WaktuSampai=NOW(),PetugasSampai=$userdata[user_id] $query_kode_otp
      WHERE NoResi IN($list_resi)";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    return true;


  } //checkInPaket

  function checkInPaketByBarcode($no_resi){
    global $db,$userdata,$config;

    //MENGAMBIL DATA PAKET
    $sql  = "SELECT TglBerangkat,KodeJadwal,IsSampai,NoManifest,IsDiantar FROM tbl_paket WHERE NoResi='$no_resi' AND FlagBatal=0";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_paket = $db->sql_fetchrow($result);

    if($db->sql_numrows($result)<=0){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Paket tidak ditemukan";
      return $ret_val;
    }

    if($data_paket["IsSampai"]!=0){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Paket sudah check-in";
      return $ret_val;
    }

    if($data_paket["NoManifest"]=="") {
      //MENGAMBIL DATA MANIFEST SESUAI DENGAN DATA PAKET
      $sql = "SELECT NoManifest,WaktuCetak FROM tbl_manifest_paket WHERE TglBerangkat='$data_paket[TglBerangkat]' AND KodeJadwal='$$data_paket[KodeJadwal]' ORDER BY WaktuCetak ASC LIMIT 0,1";

      if (!$result = $db->sql_query($sql)) {
        echo("Err: $this->ID_FILE " . __LINE__);
        exit;
      }

      $data_manifest = $db->sql_fetchrow($result);

      if ($data_manifest["NoManifest"] == "") {
        $ret_val["status"] = "GAGAL";
        $ret_val["pesan"] = "Paket belum diberangkatkan";
        return $ret_val;
      }

      $update_tentang_manifest  = ",NoManifest='$data_manifest[NoManifest]',WaktuCetakManifest='$data_manifest[WaktuCetak]',CetakManifest=1";
    }

    if($config["otp_paket"] && !$data_paket["IsDiantar"]){ //JIKA OTP PAKET TRUE, MAKA SETIAP CHECK IN, AKAN DI ASSIGN KODE OTP UTK PENGAMBILAN PAKET DAN DIKIRIM SMS KE PENERIMA, KECUALI PAKET YANG DIANTAR
      $otp_paket  = rand(111111,999999);
      $set_otp    = ",OTPPengambilan='$otp_paket'";
    }

    //MENGUPDATE DATA PAKET
    $sql=
      "UPDATE tbl_paket SET
        IsSampai=1,WaktuSampai=NOW(),PetugasSampai=$userdata[user_id] $update_tentang_manifest $set_otp
      WHERE NoResi='$no_resi'";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $ret_val["status"]  = "OK";
    $ret_val["pesan"]   = "";

    return $ret_val;


  } //checkInPaketByBarcode

  function ambilListPaketKirimSMSOTP($list_resi){
    global $db;

    $sql  = "SELECT TelpPenerima,NamaPenerima FROM tbl_paket WHERE NoResi IN($list_resi) AND IsDiantar=0 AND FlagBatal=0 AND IsSampai=1";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    return $result;
  } //ambilListPaketKirimSMSOTP

  function prosesPengambilaBarang($no_resi,$nama_pengambil,$telp_pengambil,$ktp_pengambil,$otp=""){
    global $db,$userdata;

    //MENGAMBIL DATA PAKET
    $sql  = "SELECT OTPPengambilan,IsSampai,StatusDiambil FROM tbl_paket WHERE NoResi='$no_resi' AND FlagBatal=0";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_paket = $db->sql_fetchrow($result);

    if($db->sql_numrows($result)<=0){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Paket tidak ditemukan";
      return $ret_val;
    }

    if($data_paket["IsSampai"]==0){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Paket belum sampai";
      return $ret_val;
    }

    if($data_paket["StatusDiambil"]==1){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Barang sudah diambil";
      return $ret_val;
    }

    if($data_paket["OTPPengambilan"]!=$otp && $data_paket["OTPPengambilan"]!=""){ //PENCOCOKKAN OTP
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "OTP Ditolak";
      return $ret_val;
    }

    //MENGUPDATE DATA PAKET
    $sql=
      "UPDATE tbl_paket SET
        StatusDiambil=1,
        NamaPengambil='$nama_pengambil',
        TelpPengambil='$telp_pengambil',
        NoKTPPengambil='$ktp_pengambil',
        WaktuPengambilan=NOW(),
        PetugasPemberi=$userdata[user_id]
      WHERE NoResi='$no_resi' AND StatusDiambil=0";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $ret_val["status"]  = "OK";
    $ret_val["pesan"]   = "";

    return $ret_val;


  } //prosesPengambilaBarang

  function prosesPenerimaanBarang($no_resi,$nama_penerima,$telp_penerima){
    global $db,$userdata;

    //MENGAMBIL DATA PAKET
    $sql  = "SELECT IsSampai,StatusDiambil FROM tbl_paket WHERE NoResi='$no_resi' AND FlagBatal=0";

    if(!$result=$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $data_paket = $db->sql_fetchrow($result);

    if($db->sql_numrows($result)<=0){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Paket tidak ditemukan";
      return $ret_val;
    }

    if($data_paket["IsSampai"]==0){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Paket belum sampai";
      return $ret_val;
    }

    if($data_paket["StatusDiambil"]==1){
      $ret_val["status"]  = "GAGAL";
      $ret_val["pesan"]   = "Barang sudah diterima";
      return $ret_val;
    }

    //MENGUPDATE DATA PAKET
    $sql=
      "UPDATE tbl_paket SET
        StatusDiambil=1,
        NamaPengambil='$nama_penerima',
        TelpPengambil='$telp_penerima',
        WaktuPengambilan=NOW(),
        PetugasPemberi=$userdata[user_id]
      WHERE NoResi='$no_resi' AND StatusDiambil=0";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    $ret_val["status"]  = "OK";
    $ret_val["pesan"]   = "";

    return $ret_val;


  } //prosesPenerimaanBarang

  function ambilDaftarAntaranPaket($order_by=0,$sort_by=0,$idx_page="",$cari="",$filter=""){

    //kamus
    global $db,$VIEW_PER_PAGE;

    //UNTUK SORTING
    $koloms = array(
      "NoResi","TglBerangkat","KodeJadwal",
      "TujuanAkhir","Status", "NamaPengirim",
      "TelpPengirim","AlamatPengirim","NamaPenerima",
      "TelpPenerima","AlamatPenerima", "Berat",
      "Layanan","KeteranganPaket");

    $sort     = ($sort_by==0)?"ASC":"DESC";
    $order		= ($order_by!='')?" ORDER BY $koloms[$order_by] $sort":"";
    $set_limit= ($idx_page!="")?" LIMIT ".($idx_page*$VIEW_PER_PAGE).",$VIEW_PER_PAGE":"";

    $set_kondisi  = ($cari==""?"":" AND (tp.NoResi LIKE '%$cari' OR tp.NoManifest LIKE '%$cari%' OR NamaPenerima LIKE '%$cari%' OR AlamatPenerima LIKE '%$cari%' OR TelpPenerima LIKE '%$cari' OR NamaPengirim LIKE '%$cari%' OR AlamatPengirim LIKE '%$cari%' OR TelpPengirim LIKE '%$cari')");

    $set_kondisi_tambahan  = $filter==""?"":" AND $filter";
    $sql=
      "SELECT
        SQL_CALC_FOUND_ROWS
        NoResi,TglBerangkat,KodeJadwal,tmc.Nama AS TujuanAkhir,IF(KurirAntar IS NULL OR KurirAntar='','Belum Diantar','Dalam Pengantaran') AS Status,
        NamaPengirim,TelpPengirim,AlamatPengirim,NamaPenerima,TelpPenerima,AlamatPenerima,
        Berat,Layanan,KeteranganPaket,KurirAntar,WaktuAntar
      FROM tbl_paket tp LEFT JOIN tbl_md_cabang tmc ON tp.CabangTujuanAkhir=tmc.KodeCabang
      WHERE FlagBatal=0 AND IsDiantar=1 AND IsSampai=1 AND StatusDiambil=0 $set_kondisi $set_kondisi_tambahan
      $order
			$set_limit;";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }//  END ambilDaftarAntaranPaket

  function antarBarang($list_resi,$nama_kurir){
    global $db;

    //MENGUPDATE DATA PAKET
    $sql=
      "UPDATE tbl_paket SET
        KurirAntar='".strtoupper($nama_kurir)."',WaktuAntar=NOW(),
        Log=".$this->catatLog("ANTAR PAKET","mengirim barang ke pelanggan")."
      WHERE NoResi IN($list_resi)";

    if(!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE ".__LINE__);exit;
    }

    return true;


  } //antarBarang

}
?>