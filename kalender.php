<?
//HEADER SCRIPT
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 0;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

//INCLUDE


$mode   = getVariabel("mode");
$tanggal= getVariabel("tanggal");
$bulan  = getVariabel("bulan");
$tahun  = getVariabel("tahun");

//ROUTER========================================================================================================================================================================

switch($mode){
  case 0:
    $tanggal  = $tanggal==""?(date("d-m-Y")):$tanggal;
    $bulan    = $bulan==""?1*(date("m")):$bulan;
    $tahun    = $tahun==""?date("Y"):$tahun;
    setKalender($tanggal,$bulan,$tahun);
    break;

  case 1:
    setBulan($tanggal,$bulan,$tahun);
    break;

  case 2:
    setTahun($tanggal,$bulan,$tahun);
    break;
}

//METHODES & PROCESS ========================================================================================================================================================================

function setKalender($tanggal,$bulan,$tahun){
  global $template,$Permission,$userdata;

  $bulans = array("","Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nov","Des");

  $template->set_filenames(array('body' => './kalender/kalender.tpl'));

  //MENGATUR TANGGAL

  if($bulan-1>0){
    $bulan_lalu = $bulan-1;
    $tahun_lalu = $tahun;
  }
  else{
    $bulan_lalu =12;
    $tahun_lalu = $tahun-1;
  }

  $max_hari  = getMaxDate($bulan,$tahun);
  $max_hari_bln_lalu  = getMaxDate($bulan_lalu,$tahun_lalu);

  $tgl_awal  = strtotime("$bulan/01/$tahun");
  $tgl_akhir = strtotime("$bulan/$max_hari/$tahun");

  $hari_pertama   = date("w",$tgl_awal)+1;
  $hari_terakhir  = date("w",$tgl_akhir)+1;


  $tgl_pertama  = $max_hari_bln_lalu-($hari_pertama-2);
  $tgl_terakhir = 7-$hari_terakhir;

  $i_tanggal    = $tgl_pertama;
  $i_bulan      = $bulan_lalu;
  $i_tahun      = $tahun_lalu;
  $max_hari_now = $max_hari_bln_lalu;

  $max_loop     = ceil((($max_hari_bln_lalu-$tgl_pertama)+$max_hari+$tgl_terakhir)/7);

  $tgl_today    = date("Ymd");

  for($i=1;$i<=$max_loop;$i++){

    $id_sel_tgl   = "";

    for($day=1;$day<=7;$day++) {
      $tanggals[$day] = $i_tanggal;

      $atr_class[$day]= ($i_bulan==$bulan)?"":"dif";

      $tglfull        = substr("0".$i_tanggal,-2)."-".substr("0".$i_bulan,-2)."-".$i_tahun;

      //memeriksa hak akses, jika 201.1 diberi akses, menampilkan semua tanggal
      $tgl_iterasi    = $i_tahun.substr("0".$i_bulan,-2).substr("0".$i_tanggal,-2);
      if($Permission->isPermitted($userdata["user_level"],201.1) || $tgl_iterasi*1>=$tgl_today*1){
        $act[$day]  = "pilihTanggal('$tglfull')";
      }
      else{
        $act[$day]  = "";
        $atr_class[$day]= "off";
      }

      if ($i_tanggal + 1 <= $max_hari_now) {
        $i_tanggal++;
      }
      else {
        $i_tanggal = 1;
        $max_hari_now = $max_hari;
        $i_bulan++;

        if($i_bulan>12){
          $i_bulan=1;
          $i_tahun++;
        }

      }

      if($tglfull==$tanggal) {
        $id_sel_tgl = $day;
      }
    }

    $template->assign_block_vars("WEEK",array(
      "d1"              => $tanggals[1],
      "d2"              => $tanggals[2],
      "d3"              => $tanggals[3],
      "d4"              => $tanggals[4],
      "d5"              => $tanggals[5],
      "d6"              => $tanggals[6],
      "d7"              => $tanggals[7],
      "dsel1"           => $act[1],
      "dsel2"           => $act[2],
      "dsel3"           => $act[3],
      "dsel4"           => $act[4],
      "dsel5"           => $act[5],
      "dsel6"           => $act[6],
      "dsel7"           => $act[7],
      "sel2"            => $atr_class[2],
      "sel3"            => $atr_class[3],
      "sel4"            => $atr_class[4],
      "sel5"            => $atr_class[5],
      "sel6"            => $atr_class[6],
      "sel7"            => "we".$atr_class[7],
      "sel1"            => "we".$atr_class[1],
      "sel".$id_sel_tgl => "sel"
    ));
  }

  $template->assign_vars (
    array(
      'TITLE'         => "Tanggal Berangkat",
      'BULAN_STR'     => $bulans[$bulan],
      'TANGGAL'       => $tanggal,
      'BULAN'         => $bulan,
      'TAHUN'         => $tahun,
      'TGL_SEKARANG'  => date("d-m-Y"),
      'HARI_INI'      => dateparse(date("d-m-Y"))
    )
  );

  $template->pparse('body');
}

function setBulan($tanggal,$bulan,$tahun){
  global $template;

  $template->set_filenames(array('body' => './kalender/bulan.tpl'));

  $template->assign_vars (
    array(
      'TITLE'     => "Tanggal Berangkat",
      'TANGGAL'   => $tanggal,
      'BULAN'     => $bulan,
      'TAHUN'     => $tahun,
      'SEL'.$bulan=> "sel"
    )
  );

  $template->pparse('body');
}

function setTahun($tanggal,$bulan,$tahun){
  global $template;

  $template->set_filenames(array('body' => './kalender/tahun.tpl'));

  $template->assign_vars (
    array(
      'TITLE'   => "Tanggal Berangkat",
      'TANGGAL' => $tanggal,
      'BULAN'   => $bulan,
      'TAHUN'   => $tahun
    )
  );

  $thn  = $tahun-8;

  while($thn<=$tahun+3){

    $idx_sel1  = $thn+1!=$tahun?"":"sel";
    $idx_sel2  = $thn+2!=$tahun?"":"sel";
    $idx_sel3  = $thn+3!=$tahun?"":"sel";

    $template->assign_block_vars("LIST_TAHUN",array(
      "1"     => $thn+1,
      "2"     => $thn+2,
      "3"     => $thn+3,
      "sel1"  => $idx_sel1,
      "sel2"  => $idx_sel2,
      "sel3"  => $idx_sel3

    ));

    $thn  +=3;
  }

  $template->pparse('body');
}


?>