<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassRekon.php');


// SESSION
$id_page = 405;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$bulan  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tahun  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];

//INISIALISASI
$Rekon					= new Rekon();
$PengaturanUmum	= new PengaturanUmum();
$fee_transaksi	= $PengaturanUmum->ambilFeeTransaksi();
$fee_tiket			= $fee_transaksi['FeeTiket']; 
$fee_paket			= $fee_transaksi['FeePaket']; 
$fee_sms				= $fee_transaksi['FeeSMS']; 
		

//HEADER EXCEL

			
$i=1;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Rekap Fee periode '.BulanString($bulan).' '.$tahun);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Tiket');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Tiket Batal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Tiket Kena Fee');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Total Fee');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Status Rekon');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

//QUERY
//AMBIL HARI
$sql=
	"SELECT WEEKDAY('$tahun-$bulan-01')+1 AS Hari";

if ($result = $db->sql_query($sql)){
	$row = $db->sql_fetchrow($result);
	$temp_hari	= $row['Hari'];
}

//MENGAMBIL DATA DARI TABEL REKON
$result_rekon	= $Rekon->ambilDataRekonPerBulan($bulan,$tahun);

$list_tgl_sudah_rekon	= "";

while ($row = $db->sql_fetchrow($result_rekon)){
	$list_tgl_sudah_rekon .=$row["Tanggal"].",";
	
	$data_rekon[$row['Tanggal']]["IdRekonData"]			=  $row["IdRekonData"];
	$data_rekon[$row['Tanggal']]["Hari"]						=  $row["Hari"];
	$data_rekon[$row['Tanggal']]["JumlahSPJ"]				=  $row["JumlahSPJ"];
	$data_rekon[$row['Tanggal']]["JumlahPenumpang"]	=  $row["JumlahPenumpang"];
	$data_rekon[$row['Tanggal']]["JumlahTiketBatal"]=  $row["JumlahTiketBatal"];
	$data_rekon[$row['Tanggal']]["JumlahFeeTiket"]	=  $row["JumlahFeeTiket"];
	$data_rekon[$row['Tanggal']]["JumlahPaket"]			=  $row["JumlahPaket"];
	$data_rekon[$row['Tanggal']]["JumlahFeePaket"]	=  $row["JumlahFeePaket"];
	$data_rekon[$row['Tanggal']]["JumlahSMS"]				=  $row["JumlahSMS"];
	$data_rekon[$row['Tanggal']]["JumlahFeeSMS"]		=  $row["JumlahFeeSMS"];
	$data_rekon[$row['Tanggal']]["TotalFee"]				=  $row["TotalFee"];
	$data_rekon[$row['Tanggal']]["TotalDiskonFee"]	=  $row["TotalDiskonFee"];
	$data_rekon[$row['Tanggal']]["TotalBayarFee"]		=  $row["TotalBayarFee"];
	$data_rekon[$row['Tanggal']]["FlagDibayar"]			=  $row["FlagDibayar"];
	$data_rekon[$row['Tanggal']]["WaktuCatatBayar"]	=  $row["WaktuCatatBayar"];
}

//END MENGAMBIL DATA DARI TABEL REKON

//FEE PENUMPANG
$sql_sub1	= "COUNT(IF(FlagBatal!=1 OR FlagBatal IS NULL,NoTiket,NULL))";
$sql=
	"SELECT
		WEEKDAY(TglBerangkat)+1 AS Hari,
		DAY(TglBerangkat) AS Tanggal,
		$sql_sub1 Tiket,
		COUNT(IF(FlagBatal=1,NoTiket,NULL)) TiketBatal,
		IF($sql_sub1>3,$sql_sub1-3,0) TiketKenaFee
  FROM tbl_reservasi
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND CetakTiket=1
	GROUP BY TglBerangkat,KodeJadwal
	ORDER BY TglBerangkat";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_penumpang[$row['Tanggal']]["Hari"]							 =  $row["Hari"];
	$data_penumpang[$row['Tanggal']]["TotalTiket"]				+=  $row["Tiket"];
	$data_penumpang[$row['Tanggal']]["TotalTiketBatal"]		+=  $row["TiketBatal"];
	$data_penumpang[$row['Tanggal']]["TotalTiketKenaFee"]	+=  $row["TiketKenaFee"];
	$data_penumpang[$row['Tanggal']]["Trip"]++;
}

// SPJ
$sql=
	"SELECT 
		WEEKDAY(TglBerangkat)+1 AS Hari,DAY(TglBerangkat) AS Tanggal,
		IS_NULL(COUNT(NoSPJ),0) AS TotalSPJ
	FROM tbl_spj
	WHERE MONTH(TglBerangkat)=$bulan AND YEAR(TglBerangkat)=$tahun AND IsEkspedisi=0
	GROUP BY TglBerangkat
	ORDER BY TglBerangkat ";

if (!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_keuangan_fee_kendaraan',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_spj[$row['Tanggal']]["Hari"]					=  $row["Hari"];
	$data_spj[$row['Tanggal']]["TotalSPJ"]			=  $row["TotalSPJ"];
}

//END MENGAMBIL TOTAL SPJ

$sum_trip									= 0;
$sum_tiket								= 0;
$sum_tiket_batal					= 0;
$sum_fee_tiket						= 0;
$sum_total_fee						= 0;
$jumlah_rekon_terhutang		= 0; 

//perulangan mengambil data selama 1 bulan
$idx_row	= 3;
for($idx_tgl=1;$idx_tgl<=getMaxDate($bulan,$tahun);$idx_tgl++){

	$idx_str_hari	= ($temp_hari%7!=0)?$temp_hari%7:7;
	
	$tgl_transaksi	= $idx_tgl."-".HariStringShort($idx_str_hari)."";
	
	$total_trip					= $data_penumpang[$idx_tgl]['Trip'];
	$total_tiket				= $data_penumpang[$idx_tgl]['TotalTiket']; 
	$total_tiket_batal	= $data_penumpang[$idx_tgl]['TotalTiketBatal'];
	$total_tiket_fee		= $data_penumpang[$idx_tgl]['TotalTiketKenaFee']; 
	$total_fee_tiket		= $total_tiket_fee*$fee_tiket;
		
	$status_rekon				= "OPEN";
	
	//MEMPLOT DATA
	if($data_rekon[$idx_tgl]["Hari"]==""){
		$jumlah_rekon_terhutang	+= $total_fee_tiket;
	}
	else{
		//FEE PENUMPANG
		$total_tiket_fee		= $data_rekon[$idx_tgl]['JumlahPenumpang']; 
		$total_fee_tiket		= $data_rekon[$idx_tgl]['JumlahFeeTiket'];
		
		if ($data_rekon[$idx_tgl]['FlagDibayar']!="1"){
			$status_rekon				= "CLOSE";
			$jumlah_rekon_terhutang	+= $total_fee_tiket;	
		}
		else{
			$status_rekon				= "LUNAS";
		}
	}
	
	$total_tiket				= $total_tiket!=""?$total_tiket:0;
	$total_tiket_batal	= $total_tiket_batal!=""?$total_tiket_batal:0;
	$total_tiket_fee		= $total_tiket_fee!=""?$total_tiket_fee:0;
	$total_fee_tiket		= $total_fee_tiket!=""?$total_fee_tiket:0;
	//END MEMPLOT DATA
	
	$sum_trip									+= $total_trip;
	$sum_tiket								+= $total_tiket;
	$sum_tiket_batal					+= $total_tiket_batal;
	$sum_tiket_fee						+= $total_tiket_fee;
	$sum_fee_tiket						+= $total_fee_tiket;
	
	$idx_row++;
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $tgl_transaksi);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $total_trip!=""?$total_trip:0);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $total_tiket!=""?$total_tiket:0);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $total_tiket_batal!=""?$total_tiket_batal:0);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $total_tiket_fee!=""?$total_tiket_fee:0);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $total_fee_tiket!=""?$total_fee_tiket:0);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $status_rekon);
		
	$temp_hari++;
}
			
$temp_idx=$idx_row;

$idx_row++;		

$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, '=SUM(B4:B'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, '=SUM(C4:C'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, '=SUM(D4:D'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, '=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');

	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Laporan Rekap Fee periode '.BulanString($bulan).' '.$tahun.'.xls"');
header('Cache-Control: max-age=0');
	
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
 
  
  
?>
