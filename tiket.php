<?php
//
// STANDARD

define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassPelanggan.php');
include($adp_root_path . 'ClassAsuransi.php');
include($adp_root_path . 'ClassUser.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLogSms.php');
include($adp_root_path . 'ClassVoucher.php');
include($adp_root_path . 'ClassTargetUser.php');
include($adp_root_path . 'ClassTargetCabang.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassPonta.php');
include($adp_root_path . 'phpqrcode/BarcodeQR.php');
/// set BarcodeQR object
$qr = new BarcodeQR();

// SESSION
$id_page = 201;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$layout_kursi		= $HTTP_GET_VARS['layout_kursi'];
$kode_booking		= str_replace("\'","",$HTTP_GET_VARS['kode_booking']);		
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);		
$cetak_tiket		= $HTTP_GET_VARS['cetak_tiket'];		
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];
$jenis_discount	= $HTTP_GET_VARS['jenis_discount'];
$kode_voucher		= $HTTP_GET_VARS['kode_voucher'];
$cso						= $userdata['nama'];
$antrian						= $HTTP_GET_VARS['antrian'];

$template->assign_vars(array('antrian' => $antrian));

function getDiscountGroup($kode_jadwal,$tgl_berangkat){
		
		global $db;
		
		//mengambil tanggal tuslah
		$sql	= 
			"SELECT TglMulaiTuslah,TglAkhirTuslah
				FROM tbl_pengaturan_umum LIMIT 0,1";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$tgl_mulai_tuslah	= $row['TglMulaiTuslah'];
		$tgl_akhir_tuslah	= $row['TglAkhirTuslah'];
		
		//mengambil idjurusan dari kodejadwal
		$sql	= 
			"SELECT  IdJurusan
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal'";
		
		if(!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$id_jurusan	= ($row['IdJurusan']!="")?$row['IdJurusan']:0;
		
		//mengambil harga tiket
		$sql	= 
			"SELECT IF($tgl_berangkat NOT BETWEEN $tgl_mulai_tuslah AND $tgl_akhir_tuslah,HargaTiket,HargaTiketTuslah) AS HargaTiket
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan'";
		
		if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$harga_tiket	= ($row['HargaTiket']!="")?$row['HargaTiket']:0;
		
		$sql = "SELECT FlagLuarKota
            FROM   tbl_md_jurusan
						WHERE IdJurusan='$id_jurusan'";
	  
	  if (!$result = $db->sql_query($sql)){
			return "Error";
		}
		
		$row = $db->sql_fetchrow($result);
			
		$flag_luar_kota	= $row['FlagLuarKota'];
		
    $sql = "SELECT IdDiscount,NamaDiscount,IS_NULL(JumlahDiscount,0) AS JumlahDiscount
            FROM   tbl_jenis_discount
						WHERE FlagAktif=1 AND FlagLuarKota='$flag_luar_kota' AND KodeDiscount='M'";
	  
	  if (!$result = $db->sql_query($sql)){
			return 0;
		}
		
		$row = $db->sql_fetchrow($result);
		
		return $row;
}

function kirimSMSOTPMember($nama,$telp,$otp,$kode_booking){
	$userKey 	= 'o23tc4';
	$passKey 	= 'orionaja';
	$pesan   	= 'DAYTRANS : Terima Kasih Tn/Ny '.$nama.', Kode OTP : '.$otp.', Kode Booking : '.$kode_booking.', Gunakan utk mendapatkan tiket';

	return sendHttpPost('http://reguler.sms-notifikasi.com/apps/smsapi.php','userkey='.$userKey.'&passkey='.$passKey.'&nohp='.$telp.'&pesan='.$pesan);
}

function requestPonta($params,$url){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$params);

	// receive server response ...
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec ($ch);

	curl_close ($ch);

	$json = json_decode($server_output);

	return $json;
}

$Reservasi  	= new Reservasi();
$User			= new User();
$Jurusan	  	= new Jurusan();
$Voucher	  	= new Voucher();
$TargetUser 	= new TargetUser();
$TargetCabang	= new TargetCabang();
$Cabang         = new Cabang();
$Ponta 			= new Ponta();

$template->set_filenames(array('body' => 'tiket.tpl'));
//MEMERIKSA JIKA KODE TIKET KOSONG, MAKA AKAN DICARI DATA BERDASARKAN KODE BOOKING

$keterangan_voucher = 0;

if($cetak_tiket!=1){
	//pengakuan penjualan
	$Reservasi->ubahCabang($kode_booking,$userdata['KodeCabang']);


	//mengambil data berdasarkan kode booking dan dapat mencetak tiket lebih dari 1 tiket
	
	if($jenis_pembayaran!=3){

		//PEMBAYARAN TIDAK MENGGUNAKAN VOUCHER
		$result	= $Reservasi->ambilDataKursiByKodeBooking4Tiket($kode_booking);
	}
	else{
		$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
        $info_voucher = $Voucher->getDataVoucher($kode_voucher);
        $keterangan_voucher = $info_voucher['IsReturn'];
	}
	$keterangan_duplikat	= "";
}
else{
	//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
	$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
	$keterangan_duplikat	= "**DUPLIKAT**<br>";
}


if($result){
				
	$i=0;
	
	$list_no_tiket		="";
	
	$sudah_diperiksa			= false;
	
	//MENGAMBIL JUMLAH TIKET YANG DIPESAN
	$jum_pesanan	= @mysql_num_rows($result);
	
	$data_sms			= array();
	$list_no_telp	= array();


	/**
	 * lOOPING TIKET INI 
	 * BIKIN PUSING
	 */
	while ($row = $db->sql_fetchrow($result)){

		$array_jurusan	= $Jurusan->ambilNamaJurusanByIdJurusan($row['IdJurusan']);

		$no_tiket			= $row['NoTiket'];
		$kode_booking		= $row['KodeBooking'];
		$id_member			= $row['IdMember'];
		$nama				= $row['Nama'];
		$alamat				= $row['Alamat'];
		$telp				= $row['Telp'];
		$tanggal			= $row['TglBerangkat'];
		$jam				= $row['JamBerangkat'];
		$asal				= $array_jurusan['Asal'];
		$tujuan				= $array_jurusan['Tujuan'];
		$id_jurusan			= $row['IdJurusan'];
		$harga_tiket		= $row['HargaTiket'];
		$discount			= $row['Discount'];
		$total_bayar		= $row['Total'];
		$bayar				= $row['Total'];
		$nomor_kursi		= $row['NomorKursi'];
		$cetak_tiket		= $row['CetakTiket'];
		$jenis_pembayaran	= ($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
		$kode_jadwal		= $row['KodeJadwal'];
		$jenis_penumpang	= $row['JenisPenumpang'];
		$waktu_cetak_tiket  = dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket']));
		$waktu_cetak		= $row['WaktuCetakTiket'];
		$petugas_cetak		= $row['PetugasCetakTiket'];
		$kategori_penumang  = $row['KategoriPenumpang'];
		$nama_discount 		= ($row['JenisDiscount'] != '')?"<br>(".$row['JenisDiscount'].")":$row['JenisDiscount'];
		$id_discount 		= $row['IdDiscount'];
		$is_infant 			= $row['IsWithInfant'];
		$nama_infant		= $row['NamaInfant'];
		$charge				= $row['Charge'];
        $petugas_penjual    = $row['PetugasPenjual'];
		$id_ponta			= $row['IDPONTA'];

        $list_no_tiket			.="'$no_tiket',";




		//VERIFY VOUCHER
		if($kode_voucher!=""){
			if(!$Voucher->verifyVoucher($kode_voucher,$tanggal,$id_jurusan)){
				echo("INVALID VOUCHER");
				exit;
			}
		}



		//belum cetak tiket
		if($cetak_tiket!=1){

			//UPDATE PAKAI VOUCHER
			if($jenis_pembayaran==3){
				$signature	= $Voucher->setSignature($kode_voucher,$no_tiket,$id_jurusan,$kode_jadwal,$userdata['user_id']);
				$Voucher->pakaiVoucher($kode_voucher,$tanggal,$no_tiket,$id_jurusan,$kode_jadwal,$userdata['user_id'],$signature);
			}


			//update jenis discount
			if($jenis_discount != ""){
				$data_discount	= $Reservasi->ambilDiscount($jenis_discount);
				$jenis_penumpang= $data_discount['KodeDiscount'];
				$jenis_discount	= $data_discount['NamaDiscount'];
				$nama_discount 	= "(".$data_discount['NamaDiscount'].")";
				$is_harga_tetap = $data_discount['IsHargaTetap'];
				$is_return		= $data_discount['IsReturn'];
				$listJurusan	= $data_discount['ListJurusan'];
				$id_discount    = $data_discount['IdDiscount'];
				if($is_harga_tetap!=1) {
					if ($data_discount['JumlahDiscount'] > $harga_tiket) {
						$besar_discount = $harga_tiket;
					} else if ($data_discount['JumlahDiscount'] > 0 && $data_discount['JumlahDiscount'] <= 1) {
						$besar_discount = $harga_tiket * $data_discount['JumlahDiscount'];
					} else {
						$besar_discount = $data_discount['JumlahDiscount'];
					}
				}
				else{
					$besar_discount = $harga_tiket-$data_discount['JumlahDiscount'];
				}

				$discount	= ($besar_discount<=$harga_tiket)?$besar_discount:$harga_tiket;

				$sub_total	= substr($data_discount['KodeDiscount'],0,1)!="R"?$harga_tiket:$harga_tiket+$Reservasi->getHargaTiketNormal($id_jurusan);
				$total_bayar= $sub_total-$discount;
				$bayar 		= $total_bayar;

				$sql = "UPDATE tbl_reservasi SET IdDiscount = $id_discount, Discount='$discount',SubTotal='$sub_total',Total='$total_bayar',JenisPenumpang ='$jenis_penumpang',JenisDiscount='$jenis_discount' WHERE NoTiket='$no_tiket'";
				if(!$result = $db->sql_query($sql)){
					echo("Err :".__LINE__);
					exit;
				}
			}



			$data_bayar = $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
            $data_bayar = $db->sql_fetchrow($data_bayar);
            $info_bayar = $data_bayar['Total'];



			//jenis pembayaran 8 == reedem, jadi tidak boleh menyimpan poin
			if($jenis_pembayaran != 8 && $keterangan_voucher != 1 && $info_bayar > 0){
				/**
				 * SIMPAN POINT TRANSAKSI PONTA
				 */
				$JmlRupiah = $info_bayar;
				$poinPonta = (5*$JmlRupiah)/100;
				$JmlTagihan= 1.2 * $poinPonta;



				if($id_ponta != ''){
					//SIMPAN DATA TRANSAKSI DI TBL TRANSASKSI PONTA
					$sql = "INSERT INTO tbl_ponta_transaksi (IdCard,KodeBooking,NoTiket,WaktuTransaksi,JumlahTransaksi,JumlahPoin,JumlahTagihan)
							VALUES('$id_ponta','$kode_booking','$no_tiket',NOW(),$JmlRupiah,$poinPonta,$JmlTagihan)";
					if(!$db->sql_query($sql)){
						die("Err: ".__LINE__);
					}
				}
			}

			if(!$sudah_diperiksa){
				if($id_member==''){
					//MEMERIKSA DATA PENUMPANG, JIKA BELUM PERNAH NAIK, AKAN DIMASUKKAN DALAM TBL PELANGGAN
					$Pelanggan	= new Pelanggan();

					if($Pelanggan->periksaDuplikasi($telp)){
						//JIKA SUDAH PERNAH NAIK, FREKWENSI KEBERANGKATAN AKAN DITAMBAHKAN

						$Pelanggan->ubah(
							$telp,$telp,
							$telp, $telp ,$nama,
							"", $tanggal,
							$userdata['user_id'], $kode_jadwal);
					}
					else{
						//JIKA BELUM PERNAH NAIK , AKAN DITAMBAHKAN
						$Pelanggan->tambah(
							$telp, $telp ,$nama,
							$alamat, $tanggal, $tanggal,
							$userdata['user_id'], $kode_jadwal,1,
							0);
					}

					$sudah_diperiksa	= true;
				}
				else{
					$Reservasi->updateFrekwensiMember($id_member,$tanggal);
				}
			}

			$nama_cso	= $cso;

			if($is_return == 1){
				//penumpang return
				$kode_voucher = $Voucher->setVoucherReturn($no_tiket, $id_jurusan, $kode_jadwal, $discount, $userdata['user_id'],1,$config['masa_berlaku_voucher_return']);
			}

			$waktu_cetak_tiket=dateparseWithTime(FormatMySQLDateToTglWithTime(date("Y-m-d H:i")));

		}
		else{
			$nama_cso	= $User->ambilNamaUser($row['PetugasCetakTiket']);
			if($id_discount == ""){
				$is_return = 0;
			}else {
				$data_discount	= $Reservasi->ambilDiscount($id_discount);
				$is_return		= $data_discount['IsReturn'];
			}
		}



		$cso	= $nama_cso;

		$pesanan=(trim($row['pesanan'])==1)?"Pesanan":"Go Show";
        $ket_jenis_pembayaran="";
        $voucher_keterangan = "";
		switch($jenis_pembayaran){
			case 0:
				$ket_jenis_pembayaran="TUNAI";
			break;
			case 1:
				$ket_jenis_pembayaran="DEBIT CARD";
			break;
			case 2:
				$ket_jenis_pembayaran="KREDIT CARD";
			break;
			case 3:

				if($cetak_tiket==1){
					$data_voucher	= $Voucher->getDataVoucherByNoTiketPulang($no_tiket);
					$kode_voucher	= $data_voucher['KodeVoucher'];
				}

				$ket_jenis_pembayaran="VOUCHER $kode_voucher";

                if( strpos($kode_voucher, 'ELDAY') !== false ){
                    $voucher_keterangan = "ELEVANIA LUNAS<br>";
                }

                break;
		  	case 4:
			//BAYAR DENGAN MEMBER
					$ket_jenis_pembayaran="MEMBER";
			break;
			case 5:
				$ket_jenis_pembayaran="EDC";
				break;
			case 6:
				$ket_jenis_pembayaran="TRANSFER";
				break;
			case 7:
				$ket_jenis_pembayaran="INDOMARET";
				break;
			case 8:
				$ket_jenis_pembayaran="POIN PONTA";
				break;
		}



		$data_perusahaan	= $Reservasi->ambilDataPerusahaan();

		if($is_return == 1){
			//CETAK VOUCHER
			/*$show_harga_tiket	=
				"Tk.Pr:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>
				 Tk.Pl:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>";*/

			$show_harga_tiket	=
				"<br>";

			$show_harga_tiket	= "";

			$data_voucher	= $Voucher->getDataVoucherByNoTiketBerangkat($no_tiket);

			//$data_tgl_tuslah = $PengaturanUmum->ambilTglTuslah();

			/*$pesan_tuslah	=
				"<font size=3>Jika anda menggunakan voucher ini antara
				tgl ".FormatMySQLDateToTgl($data_tgl_tuslah['TGL_MULAI_TUSLAH1'])." s/d tgl ".FormatMySQLDateToTgl($data_tgl_tuslah['TGL_AKHIR_TUSLAH1'])."
				Anda diwajibkan membayar Rp.10.000
				sebagai biaya tuslah</font><br><br><br>";*/

			$template->
			assign_block_vars(
				'VOUCHER',
				array(
					'NAMA_PENUMPANG'		=>substr($nama,0,20),
					'JURUSAN_VOUCHER'		=>substr($data_voucher['CabangBerangkat'],0,20)."-".substr($data_voucher['CabangTujuan'],0,20),
					'KODE_VOUCHER'			=>$data_voucher['KodeVoucher'],
					'EXPIRED_VOUCHER'		=>FormatMySQLDateToTglWithTime($data_voucher['ExpiredDate']),
					'PESAN_TUSLAH_VOUCHER'  =>$pesan_tuslah
				)
			);
		}
		else{

			if($jenis_pembayaran!=3){
				$show_harga_tiket	= "Tiket:".substr("------------Rp.".number_format($harga_tiket,0,",","."),-12)."<br>";
			}
			else{
				//cek voucher return
				$Return = $Voucher->getDataVoucher($kode_voucher);
				if($Return['IsReturn'] == 1){
					$show_harga_tiket	= "Tiket:".substr("------------Rp.".number_format(0,0,",","."),-12)."<br>";
					$discount	= 0;
					$bayar		= 0;
				}else{
					$result	= $Reservasi->ambilDataKursiByNoTiket4Tiket($no_tiket);
					$row = $db->sql_fetchrow($result);
					$show_harga_tiket	= "Tiket:".substr("------------Rp.".number_format($row['HargaTiket'],0,",","."),-12)."<br>";
					$discount	= $row['Discount'];
					$bayar		= $row['Total'];
				}

			}

		}

		//PESAN SPONSOR

		$pesan_sponsor	= $Reservasi->ambilPesanUntukDiTiket();

		//set data array untuk sms
		if(!in_array($telp,$list_no_telp)){
			$list_no_telp[]							= $telp;

			$data_pnp['KodeBooking']		= $kode_booking;
			$data_pnp['Telp']						= $telp;
			$data_pnp['Nama']						= $nama;
			$data_pnp['TipePengiriman']	= 0;

			$data_sms[]	= $data_pnp;
		}

		$varBayar = substr("------------Rp.".number_format($bayar,0,",","."),-12);

		$arr_kategori 	= array(1 => "Dewasa/Umum",2 => "Manula");

		$PESANPONTA = "";
		$AWARDPOINT = "";
		$BALANCEPONTA = "";

        $award_ponta = $Ponta->getDataAward($no_tiket);

        if($award_ponta['JumlahPoin'] != ""){
            $PESANPONTA = "Award poin dikalkulasikan paling lambat dalam waktu 3 x 24 jam ";
            $AWARDPOINT = "Anda Mendapatkan ".number_format($award_ponta['JumlahPoin'],0,',','.')." Point Ponta";
        }

        if($jenis_pembayaran == 8){
            //kalau pembayaran reedem, satuannya jgn rupiah tapi poin
            $varBayar = substr("------------".number_format($bayar,0,",",".")." POIN",-12);
        }

        // pesan ponta dan balance ponta
		if($id_ponta != ""){
			//Mengambil detail cabang
			$data_cabang = $Cabang->ambilDataDetail($userdata['KodeCabang']);
			$nama_cabang = $data_cabang['Nama'];

			$login = $Ponta->login($userdata['KodeCabang'],$nama_cabang);

			if($login->daytrans->status != "OK"){
				echo ("Error :".__LINE__);exit;
			}

			$token = $login->daytrans->results->accessToken;

			$point = $Ponta->enquery($userdata['KodeCabang'],$nama_cabang,$token,$id_ponta);

			if($point->daytrans->status != "OK"){
				echo ("Error :".__LINE__);exit;
			}

			$BALANCEPONTA = 'BALANCE POINT:<span style="font-size: 16px;"><strong>'.number_format($point->daytrans->results->pool->RAPUnit,0,',','.').'</strong></span><br>';

		}

        //save qr to image
        $qr->draw(100, "phpqrcode/imageQR/".$no_tiket.".png");
        $qrcode = "phpqrcode/imageQR/".$no_tiket.".png";



        $template->
			assign_block_vars(
				'ROW',
				array(
					/*'NAMA_PERUSAHAAN'		=>strtoupper($data_perusahaan[NamaPerusahaan]),*/
					'NO_TIKET'					=>$no_tiket,
                    'QR_CODE'                   =>$qrcode,
					'DUPLIKAT'					=>$keterangan_duplikat,
					'JENIS_BAYAR'				=>$voucher_keterangan.$ket_jenis_pembayaran,
					'TGL_BERANGKAT'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
					'JURUSAN'						=>substr($asal,0,20)."-".substr($tujuan,0,20),
					'JAM_BERANGKAT'			=>$jam,
					'NAMA_PENUMPANG'		=>substr($nama,0,20),
					'NOMOR_KURSI'				=>$nomor_kursi,
					'HARGA_TIKET'				=>$show_harga_tiket,
					'DISKON'						=>substr("------------Rp.".number_format($discount,0,",","."),-12).$nama_discount,
					'BAYAR'							=>$varBayar,
					'POINT_MEMBER'			=>$show_point_member,
					'CSO'								=>substr($cso,0,20),
					'PESAN'							=>$pesan_sponsor,
					'KATEGORI'							=>$arr_kategori[$kategori_penumang],
					'WAKTU_CETAK'				=>$waktu_cetak_tiket,
					'PESANPONTA'	=> $PESANPONTA,
					'AWARDPONTA'	=> $AWARDPOINT,
					'BALANCEPONTA'	=> $BALANCEPONTA,
				)
			);

		//UNTUK MENCETAK KUPON MERCHANDISE UNTUK PEMBELIAN ONLINE
		$tgl_awal_promo		= "2014-12-15";
		$tgl_akhir_promo	= "2014-12-31";
		$tgl_cetak				= substr($waktu_cetak,0,-9);
		$jumlah_kupon			= 320;
		$sql	= "SELECT COUNT(1) AS JumlahKupon FROM tbl_reservasi WHERE (WaktuCetakTiket BETWEEN '$tgl_awal_promo' AND '$tgl_akhir_promo') AND PetugasCetakTiket=0 AND KodeBooking!='$kode_booking'";

		if (!$result_merch = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}

		$data_kupon	= $db->sql_fetchrow($result_merch);

		if(($waktu_cetak>=$tgl_awal_promo && $waktu_cetak<=$tgl_akhir_promo) && $data_kupon['JumlahKupon']<$jumlah_kupon && $petugas_cetak==0){

			$template->
				assign_block_vars(
					'KUPON_MERCH',
					array(
						'notiket'	=>$no_tiket,
						'tgl'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
						'jadwal'	=>$kode_jadwal,
						'nama'		=>substr($nama,0,20),
						'telp'		=>substr($telp,0,3)."****".substr($telp,7)
					)
			);
		}

		if($is_infant == 1){
			$template->
			assign_block_vars(
				'INFANT',
				array(
					'NO_TIKET'	=>$no_tiket,
					'TGL_BERANGKAT'			=>dateparse(FormatMySQLDateToTgl($tanggal)),
					'JURUSAN'						=>substr($asal,0,20)."-".substr($tujuan,0,20),
					'JAM_BERANGKAT'			=>$jam,
					'NAMA_BAYI'			=>$nama_infant,
					'ASURANSI'			=>substr("------------Rp.".number_format($charge,0,",","."),-12),
					'CSO'								=>substr($cso,0,20),
					'WAKTU_CETAK'				=>$waktu_cetak_tiket
				)
			);
		}

		$i++;
		$list_no_kursi	.= $nomor_kursi .",";


	}
	/**
	 * END LOOPING TIKET
	 */




	//mengambil jadwal utama
	$sql	=
		"SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama)
		FROM tbl_md_jadwal
		WHERE KodeJadwal='$kode_jadwal';";
	
	if (!$result = $db->sql_query($sql)){
		echo("Err :".__LINE__);
		exit;
	}
	
	$row = $db->sql_fetchrow($result);
	$kode_jadwal_utama	= $row[0];

	$sql =
		"UPDATE tbl_posisi_detail SET StatusBayar=1
		WHERE
		  KodeJadwal='$kode_jadwal_utama'
			AND TGLBerangkat='$tanggal'
			AND NoTiket IN (".substr($list_no_tiket,0,-1).");";

	if(!$result = $db->sql_query($sql)){
		echo("Err :".__LINE__);
		exit;
	}
	
	//mengupate flag cetak tiket
	if($cetak_tiket!=1){
		$list_no_tiket	= substr($list_no_tiket,0,-1);

		
		// Generate OTP
		$digit          	= 6 ;
		$otp            	= rand(pow(10, $digit-1), pow(10, $digit)-1);

		$Reservasi->updateStatusCetakTiket($userdata['user_id'],$jenis_pembayaran,$list_no_tiket,$userdata['KodeCabang']);

		if($jenis_pembayaran == 6){
			// Update OTP
			$Reservasi->updateOTP($list_no_tiket,$otp);

            $sql = "SELECT f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS CabangAsal FROM tbl_reservasi WHERE KodeBooking = '$kode_booking'";
            if (!$hasil = $db->sql_query($sql)){
                die_error("Err :".__LINE__);
            }
            $data_tiket = $db->sql_fetchrow($hasil);
            // Tiket Transfer Pengakuan DiCabang Berangkat
            $Reservasi->ubahCabang($kode_booking,$data_tiket[0]);

			// Kirim SMS OTP
			$resSMS 	= kirimSMSOTPMember($nama,$telp,$otp,$kode_booking);
			$pesan   	= 'DAYTRANS : Terima Kasih Tn/Ny '.$nama.', Kode OTP : '.$otp.' , Gunakan utk mendapatkan tiket';

			$sql = "INSERT INTO tbl_log_sms (NoTujuan,WaktuKirim,JumlahSMSDikirim,IsiPesan) VALUES('$telp','$time',1,'$pesan')";
			if (!$db->sql_query($sql)){
				die_error("Err :".__LINE__);
			}
            echo("<font color='red' size='5'><b>KONFIRMASI PEMBAYARAN TRANSFER BERHASIL, SILAHKAN TUTUP JENDELA INI</b></font>");
			exit;
		}

    /*UPDATE TARGET USER*/
    //$TargetUser->addTransaksi($i);
    /*END UPDATE TARGET USER*/
	/*UPDATE TARGET CABANG*/
	// update tanggal 1 april hitungan target cabang dirubah, bukan dari cetak tiket tapi dari tbl reservasi langsung
	//$TargetCabang->addTransaksiPax($i);
	/*UPDATE TARGET CABANG*/
		/*
		//SMS REMINDER
		
		//mengambil asal dan tujuan
		$asal		= substr($asal,0,10);
		$tujuan	= substr($tujuan,0,10);
			
		$list_no_kursi	= substr($list_no_kursi,0,-1);
		
		$LogSMS					= new LogSMS();
		
		for($idx=0;$idx<count($data_sms);$idx++){
			$telp	= $data_sms[$idx]['Telp'];
			 
			if(in_array(substr($telp,0,2),$HEADER_NO_TELP)){
				
				$isi_pesan	= 
					"TERIMA KASIH Sdr/i ".strtoupper(substr($data_sms[$idx]['Nama'],0,10))." TELAH MEMBELI TIKET DAYTRANS UNTUK $asal-$tujuan TGL ".dateparse(FormatMySQLDateToTgl($tanggal))." $jam SEAT NO:$list_no_kursi";
				
				$telepon	= "62".substr($telp,1);
				$parameter= "username=$sms_config[user]&token=".md5($sms_config['password'].$sms_config['user'].$config['key_token'])."&destination=$telepon&message=$isi_pesan";	
				//$response	= sendHttpPost($sms_config['url'],$parameter);	
				
				if($response=="00"){
					$LogSMS->tambah(
						$telp, $data_sms[$idx]['Nama'], $data_sms[$idx]['TipePengiriman'], 
						$data_sms[$idx]['KodeBooking'], $isi_pesan);
					
					//UPDATE FLAG SEND SMS REMINDER
					$sql = 
						"UPDATE tbl_reservasi
							SET FlagSendSMSReminder=1, WaktuKirimSMSReminder=NOW()
						WHERE KodeBooking IN('$kode_booking')";
									
					if (!$result = $db->sql_query($sql)){
						die_error("Err: ID_FILE".__LINE__);
					}
				}
			}
		}//end for
		*/
	}else{
        //jika sudah cetak tiket
        //apabila jenis pembayaran transfer atau dibeli dari tiketuk maka ubah
        //pengakuan penjualan
//        if($jenis_pembayaran == 6 || $petugas_penjual == 0){
//            $Reservasi->ubahCabang($kode_booking,$userdata['KodeCabang']);
//        }
    }
	
} 
else{
	//die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
	echo("Error :".__LINE__);
	exit;
}



$template->pparse('body');	
?>