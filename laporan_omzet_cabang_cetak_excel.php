<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
// SESSION
$id_page = 302;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SPV_OPERASIONAL"],$USER_LEVEL_INDEX["SPV_RESERVASI"],$USER_LEVEL_INDEX["KEUANGAN"],$USER_LEVEL_INDEX["KASIR"]))){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

$Cabang =new Cabang();

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$tanggal_mulai  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$tanggal_akhir  = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];
$kode_cabang  	= isset($HTTP_GET_VARS['p3'])? $HTTP_GET_VARS['p3'] : $HTTP_POST_VARS['p3'];
$cari  					= isset($HTTP_GET_VARS['p4'])? $HTTP_GET_VARS['p4'] : $HTTP_POST_VARS['p4'];
$sort_by				= isset($HTTP_GET_VARS['p5'])? $HTTP_GET_VARS['p5'] : $HTTP_POST_VARS['p5'];
$order					= isset($HTTP_GET_VARS['p6'])? $HTTP_GET_VARS['p6'] : $HTTP_POST_VARS['p6'];
$username				= $userdata['username'];

//INISIALISASI
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

if($userdata['user_level']==$USER_LEVEL_INDEX["SPV_RESERVASI"] && !$Cabang->isCabangPusat($userdata["KodeCabang"])){
	$kondisi_cabang	= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"Nama":$sort_by;

//MENGAMBIL DATA-DATA MASTER CABANG
$sql_cabang=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql_cabang)){
	echo("Err:".__LINE__);exit;
}


//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) IN (SELECT KodeCabang
		FROM tbl_md_cabang
		$kondisi_cari)
	GROUP BY KodeCabang
	ORDER BY KodeCabang";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['KodeCabang']]	= $row['JadwalDibuka'];
}


//MENGAMBIL NILAI JADWAL YANG DIBUKA UNTUK PERCABANG DAN PER TANGGAL TERTENTU
$sql=
	"SELECT
	  f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) KodeCabang,
	  (DATEDIFF('$tanggal_akhir_mysql','$tanggal_mulai_mysql')+1)*(COUNT(DISTINCT(tmj.KodeJadwal))-COUNT(DISTINCT(IF(FlagAktif=0,tmj.KodeJadwal,NULL))))
	  -COUNT(IF(StatusAktif=0 AND FlagAktif=1,1,NULL))
	  +COUNT(IF(StatusAktif=1 AND FlagAktif=0,1,NULL)) AS JadwalDibuka
	FROM tbl_md_jadwal tmj LEFT JOIN tbl_penjadwalan_kendaraan tpk ON tpk.KodeJadwal=tmj.KodeJadwal
	  AND (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND  '$tanggal_akhir_mysql')
	WHERE 
		f_jurusan_get_kode_cabang_asal_by_jurusan(tmj.IdJurusan) IN (SELECT KodeCabang
		FROM tbl_md_cabang
		$kondisi_cari)
	GROUP BY KodeCabang
	ORDER BY KodeCabang";

if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_jadwal_tersedia[$row['KodeCabang']]	= $row['JadwalDibuka'];
}


//DATA PENJUALAN TIKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(IF(CetakTiket!=1,NoTiket,NULL)),0) AS TotalPenumpangB,
		IS_NULL(COUNT(IF((JenisPenumpang='U' OR JenisPenumpang='' OR JenisPenumpang='14' OR JenisPenumpang='14_20K' OR JenisPenumpang='14_5K') AND CetakTiket=1  AND JenisPembayaran!=3,NoTiket,NULL)),0) AS TotalPenumpangU,
		IS_NULL(COUNT(IF(JenisPenumpang='M' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangM,
		IS_NULL(COUNT(IF(JenisPenumpang='K' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangK,
		IS_NULL(COUNT(IF(JenisPenumpang='KK' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangKK,
		IS_NULL(COUNT(IF(JenisPenumpang='G' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangG,
		IS_NULL(COUNT(IF(JenisPenumpang='GV' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangV,
		IS_NULL(COUNT(IF(JenisPenumpang='R' AND CetakTiket=1,NoTiket,NULL)),0) AS TotalPenumpangR,
		IS_NULL(COUNT(IF(JenisPembayaran='3',NoTiket,NULL)),0) AS TotalPenumpangVR,
		IS_NULL(COUNT(IF(CetakTiket=1,NoTiket,NULL)),0) AS TotalTiket,
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,SubTotal,0),0)),0) AS TotalPenjualanTiket, 
		IS_NULL(SUM(IF(CetakTiket=1,IF(JenisPembayaran!=3,Discount,0),0)),0) AS TotalDiscount
	FROM tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_tiket_total[$row['KodeCabang']]	= $row;
}

//DATA KEBERANGKATAN BY MANIFEST
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(1),0) AS TotalBerangkat
	FROM tbl_spj
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['KodeCabang']]	= $row['TotalBerangkat'];
}

//DATA KEBERANGKATAN MANIFEST PICKUP/TRANSIT
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(DISTINCT(IF(CetakTiket=1,NoSPJ,NULL))),0) AS TotalBerangkat
	FROM tbl_reservasi tr
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND FlagBatal!=1 AND (SELECT FlagSubJadwal FROM tbl_md_jadwal tmj WHERE tmj.KodeJadwal=tr.KodeJadwal)=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$jumlah_berangkat[$row['KodeCabang']]	= $jumlah_berangkat[$row['KodeCabang']]+$row['TotalBerangkat'];
}


//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(SUM(HargaPaket),0) AS TotalPenjualanPaket, 
		IS_NULL(COUNT(NoTiket),0) AS TotalPaket 
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang']]	= $row;
}

//DATA BIAYA
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(SUM(Jumlah),0) AS TotalBiaya
	FROM tbl_biaya_op
	WHERE 
		(TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi_cabang
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_total[$row['KodeCabang']]	= $row;
}

//QUERY BIAYA TAMBAHAN
$sql = "SELECT KodeCabang,
		IS_NULL(SUM(Jumlah),0) as BiayaTambahan
		FROM tbl_biaya_tambahan
		$kondisi_cari
		AND (DATE(TglCetak) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		GROUP BY KodeCabang ORDER BY KodeCabang";

if (!$result = $db->sql_query($sql)){
	//echo("Err: ".__LINE__);exit;
	die(mysql_error());
}

while ($row = $db->sql_fetchrow($result)){
	$data_biaya_tambahan_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']								= $row['Nama'];
	$temp_array[$idx]['Alamat']							= $row['Alamat'];
	$temp_array[$idx]['Kota']								= $row['Kota'];
	$temp_array[$idx]['Telp']								= $row['Telp'];
	$temp_array[$idx]['TotalOpenTrip']			= 0+$data_jadwal_tersedia[$row['KodeCabang']];
	$temp_array[$idx]['TotalPenumpangB']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangB'];
	$temp_array[$idx]['TotalPenumpangU']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangU'];
	$temp_array[$idx]['TotalPenumpangM']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangM'];
	$temp_array[$idx]['TotalPenumpangK']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangK'];
	$temp_array[$idx]['TotalPenumpangKK']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangKK'];
	$temp_array[$idx]['TotalPenumpangV']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangV'];
	$temp_array[$idx]['TotalPenumpangG']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangG'];
	$temp_array[$idx]['TotalPenumpangR']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangR'];
	$temp_array[$idx]['TotalPenumpangVR']		= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenumpangVR'];
	$temp_array[$idx]['TotalBerangkat']			= 0+$jumlah_berangkat[$row['KodeCabang']];;
	$temp_array[$idx]['TotalTiket']					= 0+$data_tiket_total[$row['KodeCabang']]['TotalTiket'];
	$temp_array[$idx]['TotalPenjualanTiket']= 0+$data_tiket_total[$row['KodeCabang']]['TotalPenjualanTiket'];
	$temp_array[$idx]['TotalDiscount']			= 0+$data_tiket_total[$row['KodeCabang']]['TotalDiscount'];
	$temp_array[$idx]['TotalPenjualanPaket']= 0+$data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket'];
	$temp_array[$idx]['TotalPaket']					= 0+$data_paket_total[$row['KodeCabang']]['TotalPaket'];
	$temp_array[$idx]['TotalBiaya']					= 0+$data_biaya_total[$row['KodeCabang']]['TotalBiaya'];
	$temp_array[$idx]['BiayaTambahan']			= 0+$data_biaya_tambahan_total[$row['KodeCabang']]['BiayaTambahan'];
	$temp_array[$idx]['TotalPenumpangPerTrip']= ($temp_array[$idx]['TotalBerangkat']>0)?$temp_array[$idx]['TotalTiket']	/$temp_array[$idx]['TotalBerangkat']:0;
	$temp_array[$idx]['Total']							= 0+$temp_array[$idx]['TotalPenjualanTiket'] + $temp_array[$idx]['TotalPenjualanPaket'] - $temp_array[$idx]['TotalDiscount'] - $temp_array[$idx]['TotalBiaya'] - $temp_array[$idx]['BiayaTambahan'];
	
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Reservasi Per Cabang Tanggal '.$tanggal_mulai.' s/d '.$tanggal_akhir);
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Kode');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Open Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Total Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Total Booking');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Pnp.Umum');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Total Pnp.Anak/Lansia');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Total Pnp.Member');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('K3', 'Total Pnp.Special Ticket Free Staff');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L3', 'Total Pnp.Gift Voucher');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M3', 'Total Pnp.Delay');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('N3', 'Total Pnp.Return');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('O3', 'Total Pnp.Voucher Return');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('P3', 'Total Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Total Penumpang/Trip');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('R3', 'Total Omzet Penumpang');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('S3', 'Total Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('T3', 'Total Omzet Paket');
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('U3', 'Total Disc.');
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('V3', 'Total BO Langsung');
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('W3', 'Total Biaya Tambahan ');
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('X3', 'L/R Kotor');
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);

$idx=0;
$idx_row=4;
while ($idx<count($temp_array)){
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Nama']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['Alamat']." ".$temp_array[$idx]['Kota']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['TotalOpenTrip']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['TotalBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['TotalPenumpangB']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $temp_array[$idx]['TotalPenumpangU']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $temp_array[$idx]['TotalPenumpangM']);
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $temp_array[$idx]['TotalPenumpangK']);
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $temp_array[$idx]['TotalPenumpangKK']);
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $temp_array[$idx]['TotalPenumpangV']);
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $temp_array[$idx]['TotalPenumpangG']);
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $temp_array[$idx]['TotalPenumpangR']);
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $temp_array[$idx]['TotalPenumpangVR']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $temp_array[$idx]['TotalTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $temp_array[$idx]['TotalPenumpangPerTrip']);
	$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, $temp_array[$idx]['TotalPenjualanTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, $temp_array[$idx]['TotalPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('T'.$idx_row, $temp_array[$idx]['TotalPenjualanPaket']);
	$objPHPExcel->getActiveSheet()->setCellValue('U'.$idx_row, $temp_array[$idx]['TotalDiscount']);
	$objPHPExcel->getActiveSheet()->setCellValue('V'.$idx_row, $temp_array[$idx]['TotalBiaya']);
	$objPHPExcel->getActiveSheet()->setCellValue('W'.$idx_row, $temp_array[$idx]['BiayaTambahan']);
	$objPHPExcel->getActiveSheet()->setCellValue('X'.$idx_row, $temp_array[$idx]['Total']);
	
	$idx_row++;
	$idx++;
}
$temp_idx=$idx_row-1;

//$idx_row++;		

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':D'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'TOTAL');
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row,'=SUM(E4:E'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, '=SUM(F4:F'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, '=SUM(G4:G'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H4:H'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I4:I'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J4:J'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K4:K'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, '=SUM(M4:M'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, '=SUM(N4:N'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, '=SUM(O4:O'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, '=SUM(P4:P'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, '=SUM(Q4:Q'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, '=SUM(R4:R'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, '=SUM(S4:S'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('T'.$idx_row, '=SUM(T4:T'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('U'.$idx_row, '=SUM(U4:U'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('V'.$idx_row, '=SUM(V4:V'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('W'.$idx_row, '=SUM(W4:W'.$temp_idx.')');
$objPHPExcel->getActiveSheet()->setCellValue('X'.$idx_row, '=SUM(X4:W'.$temp_idx.')');

	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Reservasi Per Cabang Tgl '.$tanggal_mulai.' sd '.$tanggal_akhir.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
