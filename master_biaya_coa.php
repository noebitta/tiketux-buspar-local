<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
// SESSION
$id_page = 712;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode

switch($mode){
    case 'add':
        $template->set_filenames(array('body' => 'master_biaya_coa/add.tpl'));

        $template->assign_vars(array(
            'BCRUMP'    =>setBcrump($id_page),
            'URL'       => append_sid('master_biaya_coa.'.$phpEx),
            'MODE'			=> 'InsertData',
            )
        );
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
    case 'InsertData':
        $biaya  = $HTTP_POST_VARS['biaya'];
        $jenis  = $HTTP_POST_VARS['jenis'];
        $coa    = $HTTP_POST_VARS['coa'];

        $sql = "INSERT INTO tbl_biaya_coa (FlagJenis,JenisBiaya,COA) VALUES ('$biaya','$jenis','$coa')";
        if (!$result = $db->sql_query($sql)){
            die(mysql_error());
        }
        // QUERY AMBIL BIAYA TAMBAHAN
        $sql = "SElECT * FROM tbl_biaya_coa";

        if ($result = $db->sql_query($sql)){
            while ($row = $db->sql_fetchrow($result)){
                switch ($row['FlagJenis']){
                    case 1:
                        $jenis = "BIAYA TAMBAHAN";
                        break;
                    case 2:
                        $jenis = "BIAYA PETTY CASH";
                        break;
                }

                $template->
                assign_block_vars(
                    'ROW',
                    array(
                        'FlagJenis'    =>$jenis,
                        'JenisBiaya'   =>$row['JenisBiaya'],
                        'KodeCOA'      =>$row['COA'],
                        'EDIT'         =>append_sid('master_biaya_coa.'.$phpEx.'?mode=edit&id='.$row['IdBiayaCOA'])
                    )
                );
                $i++;
            }
        }else{
            echo("Err:".__LINE__);exit;
        }
        $template->set_filenames(array('body' => 'master_biaya_coa/index.tpl'));
        $page_title = "Biaya Tambahan";


        $template->assign_vars(array(
                'BCRUMP'  =>setBcrump($id_page),
                'ADD'     => append_sid('master_biaya_coa.'.$phpEx.'?mode=add'),
                'URL'			=> append_sid('biaya_tambahan.'.$phpEx),
                'SUMMARY' => $summary,
                'PAGING'  => $paging,
            )
        );
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
    case 'edit':
        $id = $HTTP_GET_VARS['id'];
        $sql = "SELECT * FROM tbl_biaya_coa WHERE IdBiayaCOA = '$id'";
        if ($result = $db->sql_query($sql)){
            $row=$db->sql_fetchrow($result);
        }
        else{
            die_error(mysql_error());
        }
        if($row['FlagJenis'] == 1){
            $selected1 = 'selected';
            $selected2 = '';
        }else{
            $selected2 = 'selected';
            $selected1 = '';
        }
        $template->set_filenames(array('body' => 'master_biaya_coa/add.tpl'));

        $template->assign_vars(array(
                'BCRUMP' =>setBcrump($id_page),
                'URL'    => append_sid('master_biaya_coa.'.$phpEx),
                'MODE'   => 'update',
                'ID'     => $row['IdBiayaCOA'],
                'JENIS1' => $selected1,
                'JENIS2' => $selected2,
                'BIAYA'  => $row['JenisBiaya'],
                'COA'    => $row['COA']
            )
        );
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
    case 'update':
        $id     = $HTTP_POST_VARS['id'];
        $biaya  = $HTTP_POST_VARS['biaya'];
        $jenis  = $HTTP_POST_VARS['jenis'];
        $coa    = $HTTP_POST_VARS['coa'];

        $sql = "UPDATE tbl_biaya_coa SET FlagJenis='$biaya', JenisBiaya='$jenis', COA='$coa' WHERE IdBiayaCOA='$id'";
        if (!$result = $db->sql_query($sql)){
            die(mysql_error());
        }
    default :
        // QUERY AMBIL BIAYA TAMBAHAN
        $sql = "SElECT * FROM tbl_biaya_coa";

        if ($result = $db->sql_query($sql)){
            while ($row = $db->sql_fetchrow($result)){
                switch ($row['FlagJenis']){
                    case 1:
                        $jenis = "BIAYA TAMBAHAN";
                        break;
                    case 2:
                        $jenis = "BIAYA PETTY CASH";
                        break;
                }

                $template->
                assign_block_vars(
                    'ROW',
                    array(
                        'FlagJenis'    =>$jenis,
                        'JenisBiaya'   =>$row['JenisBiaya'],
                        'KodeCOA'      =>$row['COA'],
                        'EDIT'         =>append_sid('master_biaya_coa.'.$phpEx.'?mode=edit&id='.$row['IdBiayaCOA'])
                    )
                );
                $i++;
            }
        }else{
            echo("Err:".__LINE__);exit;
        }
		
        $template->set_filenames(array('body' => 'master_biaya_coa/index.tpl'));
        $page_title = "Jenis Biaya";

        $template->assign_vars(array(
                'BCRUMP'  =>setBcrump($id_page),
                'ADD'     => append_sid('master_biaya_coa.'.$phpEx.'?mode=add'),
                'URL'			=> append_sid('biaya_tambahan.'.$phpEx),
                'SUMMARY' => $summary,
                'PAGING'  => $paging,
            )
        );
        include($adp_root_path . 'includes/page_header.php');
        $template->pparse('body');
        include($adp_root_path . 'includes/page_tail.php');
        exit;
}

?>