<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 703;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassLayoutKendaraan.php');

// PARAMETER
$mode 			= getVariabel("mode");

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST
    $cari         = getVariabel("cari");
    $idx_page     = getVariabel("idxpage");
    $order_by     = getVariabel("orderby");
    $sort         = getVariabel("sort");
    $scroll_value = getVariabel("scrollvalue");

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    break;

  case 1:
    //SHOW DIALOG TAMBAH DATA
    showDialogTambah();

    break;

  case 1.1:
    //SIMPAN DATA

    $kode_jadwal_old  = getVariabel("kodejadwalold");
    $kode_jadwal      = getVariabel("kodejadwal");
    $id_jurusan       = getVariabel("idjurusan");
    $jam_berangkat    = getVariabel("jamberangkat");
    $jenis_layout     = getVariabel("jenislayout");
    $jenis_jadwal     = getVariabel("jenisjadwal");
    $kode_jadwal_utama= getVariabel("kodejadwalutama");
    $hari_aktif       = getVariabel("hariaktif");
    $is_aktif         = getVariabel("isaktif");
    $is_online        = getVariabel("isonline");

    echo(prosesSimpan($kode_jadwal_old,strtoupper($kode_jadwal),$id_jurusan,$jam_berangkat,$jenis_layout,$jenis_jadwal,$kode_jadwal_utama,$hari_aktif,$is_aktif,$is_online));

    break;

  case 2:

    $kode_jadwal  = getVariabel("kodejadwal");

    //SHOW DIALOG TAMBAH DATA
    showDialogUbah($kode_jadwal);

    break;

  case 2.1:

    $kode_jadwal        = getVariabel("kodejadwal");
    $kode_jadwal_utama  = getVariabel("kodejadwalutama");

    //SHOW DIALOG TAMBAH DATA
    showDetailRute($kode_jadwal,$kode_jadwal_utama);

    break;

  case 3:
    //HAPUS DATA
    $list_id      = getVariabel("listid");

    $list_id = str_replace("\'","'",$list_id);

    echo(hapusData($list_id));

    break;

  case 4:
    //UBAH STATUS AKTIF
    $kode_jadwal  = getVariabel("kodejadwal");

    ubahStatusAktif($kode_jadwal);

    break;

  case 5:
    //UBAH STATUS ONLINE
    $kode_jadwal  = getVariabel("kodejadwal");

    ubahStatusOnline($kode_jadwal);

    break;

  case 6:
    //SET COMBO CABANG

    $cari         = getVariabel('filter');
    $cabang_asal  = getVariabel('cabangasal');

    setListCabang($cari,$cabang_asal);

    break;

  case 7:
    //SET COMBO JADWAL

    $cari         = getVariabel('filter');
    $id_jurusan   = getVariabel('idjurusan');
    $jam_berangkat= getVariabel('jamberangkat');

    setListJadwal($id_jurusan,$cari,$jam_berangkat);

    break;
}

//METHODES & PROCESSES ========================================================================================================================================================================

function showData(){

  global $db,$cari,$order_by,$sort,$idx_page,$template,$VIEW_PER_PAGE,$userdata,$id_page,$scroll_value;

  $Jadwal = new Jadwal();

  $template->set_filenames(array('body' => 'pengaturan.jadwal/index.tpl'));


  //cek permission untuk CRUD
  $Permission = new Permission();

  if($Permission->isPermitted($userdata["user_level"],$id_page.".1")){
    $template->assign_block_vars("CRUD_ADD",array());
    $template->assign_block_vars("CRUD_DEL",array());
    $template->assign_block_vars("EXPORT",array());
  }

  $result = $Jadwal->ambilData($order_by,$sort,$idx_page,$cari);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd = ($no%2)==0?"even":"odd";

    if($row["StatusAktif"]=="AKTIF"){
      $class_crud_aktif = "crudgreen";
    }
    else{
      $odd  = "red";
      $class_crud_aktif = "crud";
    }

    $class_crud_online = ($row["StatusOnline"]=="OFFLINE")?"crud":"crudgreen";

    $template->assign_block_vars(
      'ROW',array(
        'odd'             => $odd,
        'idx'             => $no,
        'no'              => $idx_page*$VIEW_PER_PAGE+$no,
        'kode'            => $row["KodeJadwal"],
        'jenisjadwal'     => $row["JenisJadwal"],
        'jurusan'         => $row["Jurusan"],
        'jam'             => $row["JamBerangkat"],
        'layout'          => $row["JenisLayout"],
        'aktif'           => $row["StatusAktif"],
        'online'          => $row["StatusOnline"],
        'classcrudaktif'  => $class_crud_aktif,
        'classcrudonline' => $class_crud_online
      )
    );

    //ACTION
    $template->assign_block_vars("ROW.ACT_EDIT", array());
    $template->assign_block_vars("ROW.ACT_DEL", array());

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	          => basename(__FILE__),
      'CARI'                => $cari,
      'ORDER'               => $order_by,
      'SORT'                => $sort,
      'IDX_PAGE'            => $idx_page,
      'PAGING'              => $paging,
      'U_EXPORT_EXCEL'      => substr(basename(__FILE__),0,-4).".export.excel.php",
      'SCROLL_VALUE'        => ($scroll_value==""?0:$scroll_value)
    )
  );

  $template->pparse('body');

} //showData

function ubahStatusAktif($kode_jadwal){
  $Jadwal = new Jadwal();

  if($Jadwal->ubahStatusAktif($kode_jadwal)){
    $ret_val["status"]  = "OK";
  }
  else{
    $ret_val["status"]  = "GAGAL";
  }

  echo(json_encode($ret_val));
}//ubahStatusAktif

function ubahStatusOnline($kode_jadwal){
  $Jadwal = new Jadwal();

  if($Jadwal->ubahStatusOnline($kode_jadwal)){
    $ret_val["status"]  = "OK";
  }
  else{
    $ret_val["status"]  = "GAGAL";
  }

  echo(json_encode($ret_val));
}//ubahStatusOnline

function setListCabang($cari="",$cabang_asal=""){
  global $db;

  $Cabang = new Cabang();

  $result = $Cabang->setComboListCabang($cari,$cabang_asal);

  $id = $cabang_asal==""?"KodeCabang":"IdJurusan";

  while($row=$db->sql_fetchrow($result)){
    $list_cabang[]=array("group"=>$row["Kota"],"id"=>$row[$id],"text"=>$row["Nama"].($row["KodeJurusan"]==""?"":" ($row[KodeJurusan])"));
  }

  $ret_val  =json_encode($list_cabang);

  echo($ret_val);
}//setListCabang

function setListJadwal($id_jurusan,$cari="",$jam_berangkat=""){
  global $db;

  $Jadwal = new Jadwal();

  $result = $Jadwal->setComboListJadwal($id_jurusan,$cari,$jam_berangkat);

  while($row=$db->sql_fetchrow($result)){
    $list_cabang[]=array("group"=>"","id"=>$row["KodeJadwal"],"text"=>$row["JamBerangkat"]." [$row[KodeJadwal]]");
  }

  $ret_val  =json_encode($list_cabang);

  echo($ret_val);
}//setListJadwal

function showDialogTambah(){
  global $template;

  $template->set_filenames(array('body' => 'pengaturan.jadwal/detail.tpl'));

  //INISIALISASI NILAI PARAMETER
  $template->assign_vars(array(
      "JUDUL"	            => "Tambah data jadwal",
      "STATUS_AKTIF"      => 1,
      "STATUS_ONLINE"     => 0,
      "HARI_AKTIF_0"      => 1,
      "HARI_AKTIF_1"      => 1,
      "HARI_AKTIF_2"      => 1,
      "HARI_AKTIF_3"      => 1,
      "HARI_AKTIF_4"      => 1,
      "HARI_AKTIF_5"      => 1,
      "HARI_AKTIF_6"      => 1,
      "JENIS_JADWAL"      => 0
    )
  );

  //SET COMBO JAM
  setListJam($template);

  //SET COMBO MENIT
  setListMenit($template);

  //SET COMBO LAYOUT KENDARAAN
  setListLayoutKendaraan($template);

  $template->pparse('body');
}//showDialogTambah

function showDialogUbah($kode_jadwal){
  global $template;

  $Jadwal   = new Jadwal();

  $data_jadwal  = $Jadwal->ambilDataDetail($kode_jadwal);

  $template->set_filenames(array('body' => 'pengaturan.jadwal/detail.tpl'));

  $arr_hari_aktif = explode(",",$data_jadwal["HariAktif"]);

  foreach($arr_hari_aktif as $value){
    $temp_var   = "hari_aktif_".$value;
    $$temp_var  = 1;
  }

  //INISIALISASI NILAI PARAMETER
  $template->assign_vars(array(
      "JUDUL"	            => "Ubah data jadwal",
      "KODE_JADWAL"       => $data_jadwal["KodeJadwal"],
      "CABANG_ASAL"       => $data_jadwal["KodeCabangAsal"],
      "ID_JURUSAN"        => $data_jadwal["IdJurusan"],
      "CABANG_ASAL_UTAMA" => $data_jadwal["KodeCabangAsalUtama"],
      "ID_JURUSAN_UTAMA"  => $data_jadwal["IdJurusanUtama"],
      "KODE_JADWAL_UTAMA" => $data_jadwal["KodeJadwalUtama"],
      "STATUS_AKTIF"      => $data_jadwal["IsAktif"],
      "HARI_AKTIF_0"      => ($hari_aktif_0==""?0:$hari_aktif_0),
      "HARI_AKTIF_1"      => ($hari_aktif_1==""?0:$hari_aktif_1),
      "HARI_AKTIF_2"      => ($hari_aktif_2==""?0:$hari_aktif_2),
      "HARI_AKTIF_3"      => ($hari_aktif_3==""?0:$hari_aktif_3),
      "HARI_AKTIF_4"      => ($hari_aktif_4==""?0:$hari_aktif_4),
      "HARI_AKTIF_5"      => ($hari_aktif_5==""?0:$hari_aktif_5),
      "HARI_AKTIF_6"      => ($hari_aktif_6==""?0:$hari_aktif_6),
      "STATUS_ONLINE"     => $data_jadwal["IsOnline"],
      "JENIS_JADWAL"      => $data_jadwal["IsSubJadwal"]
    )
  );

  $arr_jam_berangkat  = explode(":",$data_jadwal["JamBerangkat"]);

  //SET COMBO JAM
  setListJam($template,$arr_jam_berangkat[0]);

  //SET COMBO MENIT
  setListMenit($template,$arr_jam_berangkat[1]);

  //SET COMBO LAYOUT KENDARAAN
  setListLayoutKendaraan($template,$data_jadwal["JenisLayout"]);

  $template->pparse('body');
}//showDialogUbah

function showDetailRute($kode_jadwal,$kode_jadwal_utama){
  global $db;

  $Jadwal = new Jadwal();

  $res_detail_rute = $Jadwal->ambilDetailRute($kode_jadwal,$kode_jadwal_utama);

  $str_detail_rute  = "";

  while($row=$db->sql_fetchrow(($res_detail_rute))) {
    $str_detail_rute .= $row["NamaCabangAsal"] . " " . $row["GroupJamBerangkat"] . " (" . $row["GroupKodeJadwal"] . ")<br>";
  }

  echo(substr($str_detail_rute,0,-4));

}

function prosesSimpan($kode_jadwal_old,$kode_jadwal,$id_jurusan,$jam_berangkat,$jenis_layout,$jenis_jadwal,$kode_jadwal_utama,$hari_aktif,$is_aktif,$is_online){

  $Jadwal           = new Jadwal();
  $LayoutKendaraan  = new LayoutKendaraan();
  $Jurusan          = new Jurusan();

  //mengambil kapasitas kursi
  $res_layout  = $LayoutKendaraan->ambilDetailData($jenis_layout);

  $data_layout  = $res_layout["data"];

  $kapasitas    = $data_layout["Kapasitas"]!=""?$data_layout["Kapasitas"]:0;

  $data_jurusan = $Jurusan->ambilDataDetail($id_jurusan);

  if($kode_jadwal_old==""){
    //proses tambah
    $kode_jadwal_utama  = $jenis_jadwal==0?"":$kode_jadwal_utama;
    $return = $Jadwal->tambah($kode_jadwal,$id_jurusan,$data_jurusan["KodeCabangAsal"],$data_jurusan["KodeCabangTujuan"],$jam_berangkat,$jenis_layout,$kapasitas,$jenis_jadwal,$kode_jadwal_utama,$hari_aktif,$is_aktif,$is_online);
  }
  else{
    //proses ubah
    $kode_jadwal_utama  = $jenis_jadwal==0?"":$kode_jadwal_utama;
    $return = $Jadwal->ubah($kode_jadwal,$id_jurusan,$data_jurusan["KodeCabangAsal"],$data_jurusan["KodeCabangTujuan"],$jam_berangkat,$jenis_layout,$kapasitas,$jenis_jadwal,$kode_jadwal_utama,$hari_aktif,$is_aktif,$is_online,$kode_jadwal_old);
  }

  return json_encode($return);
}

function hapusData($list_id){
  $Jadwal = new Jadwal();

  $return = $Jadwal->hapus($list_id);

  return json_encode($return);

}

function setListJam($target_template,$jam_dipilih=""){

  $target_template->assign_block_vars("OPT_JAM",array(
    "value"     => "",
    "text"      => "-jam-",
    "selected"  => ($jam_dipilih==""?"selected":"")
  ));

  for($jam=0;$jam<=23;$jam++){

    $str_jam = substr("0".$jam,-2);

    $target_template->assign_block_vars("OPT_JAM",array(
      "value"     => $str_jam,
      "text"      => $str_jam,
      "selected"  => ($str_jam!==$jam_dipilih?"":"selected")
    ));
  }

}

function setListMenit($target_template,$menit_dipilih=""){

  $target_template->assign_block_vars("OPT_MENIT",array(
    "value"     => "",
    "text"      => "-menit-",
    "selected"  => ($menit_dipilih==""?"selected":"")
  ));

  for($menit=0;$menit<60;$menit+=15){

    $str_menit = substr("0".$menit,-2);

    $target_template->assign_block_vars("OPT_MENIT",array(
      "value"     => $str_menit,
      "text"      => $str_menit,
      "selected"  => ($str_menit!==$menit_dipilih?"":"selected")
    ));
  }

}

function setListLayoutKendaraan($target_template,$layout_dipilih=""){
  global $db;

  $LayoutKendaraan  = new LayoutKendaraan();

  $res_layout = $LayoutKendaraan->ambilData(0);

  if ($res_layout["status"]!="OK"){
    die_error("Error:".$res_layout["pesan"]);
  }

  $target_template->assign_block_vars("LAYOUT_KURSI",array(
    "id"        => "",
    "kodelayout"=> "-pilih-",
    "selected"  => ($layout_dipilih===""?"selected":"")
  ));

  while($data_layout=$db->sql_fetchrow($res_layout["data"])){
    $target_template->assign_block_vars("LAYOUT_KURSI",array(
      "id"        => $data_layout["IdLayout"],
      "kodelayout"=> $data_layout["KodeLayout"],
      "kapasitas" => " [".$data_layout["Kapasitas"]." kursi]",
      "selected"  => ($data_layout["IdLayout"]==$layout_dipilih?"selected":"")
    ));
  }

}
?>