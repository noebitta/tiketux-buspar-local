<?php
//
// File Konfigurasi
//VERSI UPDATE: 29 September 2910 20:14
//

// PHP
$phpEx = 'php';

$dbms  = "mysql";

// langguage
$lang['ENCODING']    = 'iso-8859-1';
$lang['DIRECTION']   = 'ltr';
$lang['LEFT']        = 'left';
$lang['RIGHT']       = 'right';
$lang['DATE_FORMAT'] = 'd M Y';
$lang['Title']       = 'Travel Manager';
$lang['Next']        = ' >';
$lang['Previous']    = '< ';
$lang['Goto_page']   = '';

$lang['Error_login'] = 'Username tidak terdaftar atau password anda salah!';
$lang['Click_return_login'] = 'Click <a href="./index.php">Here</a> To Return To Login';

// Error codes
define('GENERAL_MESSAGE', 'General');
define('GENERAL_ERROR', 'Error');
define('CRITICAL_MESSAGE', 'Critical');
define('CRITICAL_ERROR', 'Critical');

// public config
$config['site_name']            = "BARAYA - Bus Pariwisata";
$config['perpage'] 						  = 10; // jumlah item per halaman
$config['zip'] 								  = FALSE;   // kita lakukan gzip encoding ?
$config['protocol'] 						= '';
$config['name']   							= '.';
$config['script'] 							= '';
$config['template'] 						= '';
$config['jadwal'] 							= '';
$config['range_show_sisa_kursi']= 2; //jam
$config['limit_kasbon_sopir']	  = 500000; //Rupiah
$config['email_to_rekon']    	  = "aloi@gmail.com";
$config['email_to_name']     	  = "Keuangan Day Trans";
$config['email_host']        	  = "smtp.gmail.com";
$config['email_smtp_debug']  	  = 2;
$config['email_smtp_auth']   	  = true;
$config['email_smtp_secure'] 	  = "ssl";
$config['email_port']        	  = 465;
$config['limit_utang_rekon'] 	  = 4000000;
$config['limit_max_fee_rekon']  = 180000;
$config['key_token']         	  = "indonesiatanahairbeta";
$config['sms_to_tiketux']    	  = "628170525609"; //no barton
$config['masa_berlaku_voucher'] = 30; //HARI
$config['masa_berlaku_voucher_return']= 14; //HARI
$config['jenis_penumpang']			=  array("U"=>"UMUM","K"=>"Pelanggan Setia","G"=>"GRATIS","KK"=>"Keluarga Karyawan","M"=>"Mahasiswa dll","V"=>"Voucher","R"=>"Pergi-Pulang","RM"=>"Pergi-Pulang Mahasiswa","RT"=>"Pergi-Pulang Telkomsel","MBR"=>"MEMBER");
$config['kode_mitra']           = "DTR";
$config['stok_kardus_min']      = 10;
$config['toleransi_buka_jam_lalu']= 120; //MENIT
$config['otp_paket']            = false;
$config['role_admin']           = 0;
$config['id_pengaturan_halaman']= 713;
$config['layanan_paket']        = array(1=>"Small Parcel",2=>"Reguler Parcel",3=>"Medium Parcel",4=>"Heavy Parcel",5=>"Super Parcel");
$config['jenis_pembayaran']     = array("tunai","debit","kredit","voucher","member","edc","transfer","ponta");

//PONTA
$config['url_login'] 			= "http://daytrans.tiketux.com/api/v1/ponta/login.json";
$config['url_enquiry']			= "http://daytrans.tiketux.com/api/v1/ponta/enquiry.json";
$config['url_reedem']			= "http://daytrans.tiketux.com/api/v1/ponta/reedem.json";
$config['url_voidreedem']		= "http://daytrans.tiketux.com/api/v1/ponta/voidreedem.json";

 //UNTUK TIKETUX
$config['apiUrl']						= "https://api.tiketux.com/dev1/";//development
//$config['apiUrl']					= "https://api.tiketux.com/v1/";//LIVE
$config['authKey']						= 'DTRTTX'; //development
$config['authSecret']				= '2104117ff43c323803ac3d5ceb1747cgk52a49d94';
$config['merchant']					= 'BMT';
$config['returnURL']					= 'http://bimo.tiketux.com/dev/service.Payment.php';



/**
 * update 08-08-2016
 */
//UNTUK TIKETUX
// DEVELOPMENT
//$config['Merchant']        = "DAYTRANS";
//$config['key']             = "33addbf82f8be4f1b87751983d3b646d";
//$config['secret']          = "34d160168099b5be14de731803db6495";
//$config['api_url']         = "https://api.tiketux.com/dev1/";
//$config['client_id']       = "TEST";
//$config['client_secret']   = "b014dcc2a3a984ebaea090dd96e6d0fc";
//$config['return_url_voucher'] = "http://daytrans-dev.tiketux.com/receiver_voucher.php";
//$config['return_url_indomaret'] = "http://daytrans-dev.tiketux.com/receiver_indomaret.php";

//LIVE
$config['Merchant']             = "DAYTRANS";
$config['key']                  = "9833f4fc7f1fc4591e0f499d71815a49";
$config['secret']               = "d3383f75585669cfbd83beb36a34509e";
$config['api_url']              = "https://api.tiketux.com/v2/";
$config['client_id']            = "DAYTRANS";
$config['client_secret']        = "e331e9b70c5acd7c6135b1995c9f2146053854147";
$config['return_url_voucher']   = "http://daytrans-stg.tiketux.com/receiver_voucher.php";
$config['return_url_indomaret'] = "http://daytrans-stg.tiketux.com/receiver_indomaret.php";

$LIST_KOTA=array(
	"BANDUNG",
	"JAKARTA");

$LIST_JENIS_PEMBAYARAN=array(
	"TUNAI",
	"DEBIT",
	"KREDIT",
	"VOUCHER",
  "MEMBER",
	"EDC",
	"TRANSFER",
	"PONTA");

$LIST_JENIS_BIAYA=array(
	"JASA",
	"TOL",
	"SOPIR",
	"BBM",
	"PARKIR",
	"SOPIR KUMULATIF",
  "VOUCHER BBM");

$LIST_POOL=array(
	"RAWA BOKOR",
	"CIHAMPELAS"
);

$FLAG_BIAYA_JASA	= 0;
$FLAG_BIAYA_TOL		= 1;
$FLAG_BIAYA_SOPIR	= 2;
$FLAG_BIAYA_BBM		= 3;
$FLAG_BIAYA_PARKIR= 4;
$FLAG_BIAYA_SOPIR_KUMULATIF	= 5;
$FLAG_BIAYA_VOUCHER_BBM=6;

$LEVEL_ADMIN				= 0;
$LEVEL_MANAJEMEN			= 1.0;
$LEVEL_MANAJER				= 1.2;
$LEVEL_SUPERVISOR			= 1.3;
$LEVEL_SUPERVISOR_OPS		= 1.4;
$LEVEL_HRD					= 1.5;
$LEVEL_CSO					= 2.0;
$LEVEL_CSO_PAKET			= 2.1;
$LEVEL_CALL_CENTER			= 2.2;
$LEVEL_SCHEDULER			= 3.0;
$LEVEL_KASIR				= 4.0;
$LEVEL_KEUANGAN				= 5.0;
$LEVEL_CCARE				= 6.0;
$LEVEL_MEKANIK				= 7.0;
$LEVEL_CHECKER				= 8.0;
$LEVEL_SUPERVISOR_CHECKER	= 9.0;

$USER_LEVEL= array(
	'0.0'=>"Admin",
	'1.0'=>"Manajemen",
	'1.2'=>"Manajer",
	'1.3'=>"Spv.Reservasi",
	'1.4'=>"Spv.Operasional",
	'1.5'=>"HRD",
	'2.0'=>"CSO",
	'2.1'=>"CSO Paket",
	'2.2'=>"Call Center",
	'3.0'=>"Scheduler",
	'4.0'=>"Kasir",
	'5.0'=>"Keuangan",
	'6.0'=>"Customer Care",
	'7.0'=>"Mekanik",
	'8.0'=>"Checker",
	'9.0'=>"Spv.Checker"
);

$USER_LEVEL_INDEX= array(
	"ADMIN"					=>0.0,
	"MANAJEMEN"			    =>floatval(1.0),
	"MANAJER"				=>1.2,
	"SPV_RESERVASI"	        =>1.3,
	"SPV_OPERASIONAL"       =>1.4,
	"HRD"				    =>1.5,
	"CSO"					=>floatval(2.0),
	"CSO_PAKET"				=>2.1,
	"CALL_CENTER"			=>2.2,
	"SCHEDULER"				=>floatval(3.0),
	"KASIR"					=>floatval(4.0),
	"KEUANGAN"				=>floatval(5.0),
	"CUSTOMER_CARE"			=>floatval(6.0),
	"MEKANIK"				=>floatval(7.0),
	"CHECKER"				=>floatval(8.0),
	"SPV_CHECKER"			=>floatval(9.0),
);

$TOLERANSI_KEBERANGKATAN=125; //menit

$TOP_UP_AWAL=150000; //menit
$BIAYA_PENDAFTARAN=10000; //menit

//PAGING
$VIEW_PER_PAGE=50;
$PAGE_PER_SECTION=7;
$MAX_DATA_EXPORTED_PER_PAGE=1;

//LAYOUT MAKSIMUM UNTUK TBL_POSISI
$LAYOUT_MAKSIMUM=17;
$LAYOUT_KURSI_DEFAULT=8;

//INSENTIF SOPIR
$INSENTIF_SOPIR_JUMLAH=2500;
$INSENTIF_SOPIR_JUMLAH_PNP_MINIMUM=6;
$INSENTIF_SOPIR_LAYOUT_MAKSIMUM=20;

//INTENSIF PAKET
$INSENTIF_SOPIR_PAKET_JUMLAH=2500;
$INSENTIF_SOPIR_JUMLAH_PAKET_MINIMUM=6;
$INSENTIF_SOPIR_JUMLAH_PAKET_MAKSIMUM=20;

//PENGATURAN MEMBER
$MINIMUM_KEBERANGKATAN_JADI_MEMBER	= 10;
$THRESHOLD_MEMBER_EXPIRED	= 90; //hari sebelum expired

//PENGATURAN PAKET
$LIST_JENIS_PEMBAYARAN_PAKET=array(
	"TUNAI",
	"LANGGANAN",
	"BAYAR DI TUJUAN");

$PAKET_CARA_BAYAR_TUNAI			= 0;
$PAKET_CARA_BAYAR_LANGGANAN	= 1;
$PAKET_CARA_BAYAR_DI_TUJUAN	= 2;

//SESSION TIME
$SESSION_TIME_EXPIRED	= 600; // Detik
$JUMLAH_MINIMUM_DISCOUNT_GROUP	= 4;

#KONFIGURASI SMS GATEWAY TIKETUX
$sms_config['url']				= "http://sms.cektagihan.com:88/";
$sms_config['user']				= "tiketux";
$sms_config['password']		= "Or10n";
$sms_config['keytoken']		= "indonesiatanahairbeta";
$sms_config['range_reminder']		= 35;

#KONFIGURASI SMS GATEWAY ZENZIVA
$sms_config['url_zen']				= "https://reguler.zenziva.net/apps/smsapi.php?";
//$sms_config['url_zen']				= "http://reguler.sms-notifikasi.com/apps/smsapi.php?";
$sms_config['userkey_zen']		= "o23tc4";
//$sms_config['userkey_zen']		= "o23tc4";
$sms_config['passkey_zen']		= "Or10n";
//$sms_config['passkey_zen']		= "orionaja";

$HEADER_NO_TELP	= array("02","03","04","05","06","07","08","09");

?>