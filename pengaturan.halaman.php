<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 701;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDE
include($adp_root_path . 'ClassPages.php');

// PARAMETER
$mode 			= getVariabel("mode");
$cari       = getVariabel("cari");
$idx_page   = getVariabel("idxpage");
$order_by   = getVariabel("orderby");
$sort       = getVariabel("sort");

//INIT
$mode       = $mode==""?0:$mode;
$idx_page   = $idx_page==""?0:$idx_page;

$Pages      = new Pages();

//ROUTER========================================================================================================================================================================
switch($mode){
  case 0:
    //VIEW LIST

    $template->assign_vars(array(
      'BCRUMP'	  =>setBcrump($id_page)
    ));

    include($adp_root_path . 'includes/page_header.php');
    showData();
    include($adp_root_path . 'includes/page_tail.php');

    exit;

  case 1:
    //TAMBAH DATA
    showInputData();

    exit;

  case 2:
    //SHOW DETAIL DATA
    $id = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];

    showDetailData($id);

    exit;

  case 3:
    //SIMPAN DATA
    $page_id_old  = isset($HTTP_POST_VARS['idold'])?$HTTP_POST_VARS['idold'] : $HTTP_GET_VARS['idold'];
    $page_id      = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];
    $nama_page    = isset($HTTP_POST_VARS['namapage'])?$HTTP_POST_VARS['namapage'] : $HTTP_GET_VARS['namapage'];
    $nama_tampil  = isset($HTTP_POST_VARS['namatampil'])?$HTTP_POST_VARS['namatampil'] : $HTTP_GET_VARS['namatampil'];
    $group        = isset($HTTP_POST_VARS['group'])?$HTTP_POST_VARS['group'] : $HTTP_GET_VARS['group'];
    $urutan       = isset($HTTP_POST_VARS['urutan'])?$HTTP_POST_VARS['urutan'] : $HTTP_GET_VARS['urutan'];
    $nama_file    = isset($HTTP_POST_VARS['namafile'])?$HTTP_POST_VARS['namafile'] : $HTTP_GET_VARS['namafile'];
    $icon         = isset($HTTP_POST_VARS['icon'])?$HTTP_POST_VARS['icon'] : $HTTP_GET_VARS['icon'];
    $is_hide      = isset($HTTP_POST_VARS['ishide'])?$HTTP_POST_VARS['ishide'] : $HTTP_GET_VARS['ishide'];

    echo(prosesSimpan($page_id,$nama_page,$nama_tampil,$group,$urutan,$nama_file,$icon,$is_hide,$page_id_old));

    exit;

  case 4:
    //HAPUS DATA
    $id        = isset($HTTP_POST_VARS['id'])?$HTTP_POST_VARS['id'] : $HTTP_GET_VARS['id'];

    echo(hapusData($id));

    break;
}

//METHODES & PROCESSES ========================================================================================================================================================================

function showData(){

  global $db,$cari,$order_by,$sort,$idx_page,$template,$Pages,$VIEW_PER_PAGE;

  $template->set_filenames(array('body' => 'pengaturan.halaman/index.tpl'));

  $template->assign_block_vars("CRUD_ADD",array());
  $template->assign_block_vars("CRUD_DEL",array());

  $result = $Pages->ambilData($order_by,$sort,$idx_page,$cari);

  //PAGING======================================================
  $paging=setPaging($idx_page,"formdata");
  //END PAGING==================================================

  $no = 0;

  while($row=$db->sql_fetchrow($result)){
    $no++;

    $odd =($no%2)==0?"even":"odd";

    if(!$row["IsHide"]){
      $penampilan       = "Tampil";
      $title_penampilan = "menyembunyikan";
    }
    else{
      $penampilan       = "Sembunyi";
      $title_penampilan = "menampilkan";
      $odd              = "red";
    }
    $template->assign_block_vars(
      'ROW',array(
        'odd'             => $odd,
        'idx'             => $no,
        'no'              => $idx_page*$VIEW_PER_PAGE+$no,
        'pageid'          => $row["PageId"],
        'namapage'        => $row["NamaPage"],
        'namatampil'      => $row["NamaTampil"],
        'grouppage'       => $row["GroupPage"],
        'urutan'          => $row["Urutan"],
        'namafile'        => $row["NamaFile"],
        'imageicon'       => $row["ImageIcon"],
        'penampilan'      => $penampilan,
        'titlepenampilan' => $title_penampilan
      )
    );

    //ACTION
    $template->assign_block_vars("ROW.ACT_EDIT", array());
    $template->assign_block_vars("ROW.ACT_DEL", array());

  }

  if($no>0){
    $template->assign_block_vars("TABLE_HEADER",array());
  }
  else{
    $template->assign_block_vars("NO_DATA",array());
  }

  $template->assign_vars(array(
      'URL_CRUD'	=> basename(__FILE__),
      'CARI'      => $cari,
      'ORDER'     => $order_by,
      'SORT'      => $sort,
      'IDX_PAGE'  => $idx_page,
      'PAGING'    => $paging
    )
  );


  $template->pparse('body');

}

function showInputData(){
  global $template;

  $template->set_filenames(array('body' => 'pengaturan.halaman/edit.tpl'));

  $template->assign_block_vars("ACT_SAVE",array());

  $template->assign_vars(array(
      'OPERATION'	=> "tambah",
      'NAMA_ROLE' => ""
    )
  );

  $template->pparse('body');
}

function showDetailData($id){
  global $template,$Pages;

  $template->set_filenames(array('body' => 'pengaturan.halaman/edit.tpl'));

  $data = $Pages->ambilDataDetail($id);

  $template->assign_vars(array(
    "OPERATION"	  => "Data",
    "PAGE_ID"     => $data["PageId"],
    "NAMA_PAGE"   => $data["NamaPage"],
    "NAMA_TAMPIL" => $data["NamaTampil"],
    "GROUP"       => $data["GroupPage"],
    "URUTAN"      => $data["Urutan"],
    "NAMA_FILE"   => $data["NamaFile"],
    "ICON"        => $data["ImageIcon"],
    "TAMPIL_SEL".$data["IsHide"] => "selected"
  ));

  $template->pparse('body');
}

function prosesSimpan($id,$nama_page,$nama_tampil,$group,$urutan,$nama_file,$icon,$is_hide,$id_old=""){
  global $Pages;

  //cek duplikasi
  if(!$Pages->periksaDuplikasi($id) || $id_old==$id){
    if($id_old==""){
      //proses tambah
      $id=$Pages->tambah($id,$nama_page,$nama_tampil,$group,$urutan,$nama_file,$icon,$is_hide);
    }
    else{
      //proses ubah
      $Pages->ubah($id_old,$id,$nama_page,$nama_tampil,$group,$urutan,$nama_file,$icon,$is_hide);
    }

    $return = array("status" => "OK","id"=>$id);

  }
  else{
    $return = array("status" => "GAGAL","error"=>"data dengan ID ini sudah terdaftar dalam sistem!");
  }


  return json_encode($return);
}

function hapusData($id){
  global $Pages;

  $Pages->hapus($id);

  $return = array("status" => "OK","id"=>$id);

  return json_encode($return);
}
?>