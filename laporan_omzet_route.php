<?php

define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJurusan.php');


// SESSION
$id_page = 312;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################


$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_route/laporan_omzet_route.tpl'));

// mengambil jurusan yang berasal dari jakarta
$sql = 'SELECT
            IdJurusan AS IdJurusanJakarta,
            KodeJurusan,
            f_cabang_get_name_by_kode (KodeCabangAsal) AS DariJakarta,
            f_cabang_get_name_by_kode (KodeCabangTujuan) AS KeBandung,
            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanBandung,
            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS DariBandung,
            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = j.KodeCabangTujuan AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakarta,
            (SELECT IdJurusan FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS IdJurusanPasteur,
            (SELECT f_cabang_get_name_by_kode (KodeCabangAsal) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS DariPasteur,
            (SELECT f_cabang_get_name_by_kode (KodeCabangTujuan) FROM tbl_md_jurusan WHERE KodeCabangAsal = "PST" AND KodeCabangTujuan = j.KodeCabangAsal) AS KeJakartaDariPasteur
            
        FROM
            tbl_md_jurusan j
        WHERE
            (
                SELECT
                    Kota
                FROM
                    tbl_md_cabang
                WHERE
                    KodeCabang = KodeCabangAsal
            ) = "JAKARTA"
        ORDER BY
            IdJurusan';

if (!$jurusan_jakarta = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}


//=======================================TIKET==============================================
//DATA PENJUALAN TIKET
$sql	= "SELECT 
                IdJurusan,
                IF(COUNT(NoTiket) < 0,0,COUNT(NoTiket)) AS PAX 
            FROM tbl_reservasi
            WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
                AND FlagBatal!=1 AND CetakTiket = 1
            GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $data_tiket_total[$row['IdJurusan']]	= $row;
}

//=======================================PAKET==============================================
//DATA PENJUALAN PAKET
$sql	= "SELECT 
                IdJurusan,
                IF(COUNT(NoTiket) < 0,0,COUNT(NoTiket)) AS PAKET 
            FROM tbl_paket
            WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
                AND FlagBatal!=1 AND CetakTiket = 1
            GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err: ".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $data_paket_total[$row['IdJurusan']]	= $row;
}

//=========================================MANIFEST==============================
//DATA KEBERANGKATAN BY MANIFEST
$sql	=
    "SELECT 
		IdJurusan,
		IF(COUNT(NoSPJ) < 0 ,0 , COUNT(NoSPJ)) AS TRIP
	FROM tbl_spj
	WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
	GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
    $jumlah_berangkat[$row['IdJurusan']]	= $row;
}

//=======================================UNIT=====================================
//DATA UNIT YANG BERANGKAT BY MANIFEST
$sql	=
    "SELECT 
		IdJurusan,
		IF(COUNT(DISTINCT(NoPolisi)) < 0 , 0 ,COUNT(DISTINCT(NoPolisi))) AS UNIT
	FROM tbl_spj
	WHERE TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql' 
	GROUP BY IdJurusan ORDER BY IdJurusan";

if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}
while ($row = $db->sql_fetchrow($result)){
    $jumlah_unit[$row['IdJurusan']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$sumpax=0;
$sumpaket=0;
$sumtrip=0;
$sumunit=0;
$idx=0;

while ($row = $db->sql_fetchrow($jurusan_jakarta)){

    if($row['IdJurusanBandung'] == $row['IdJurusanPasteur']){
        $counter_pasteur = "";
        $id_pasteur      = 0;
        $nama_pasteur    = "";
    }else{
        $counter_pasteur = $row['DariPasteur'];
        $id_pasteur      = $row['IdJurusanPasteur'];
        $nama_pasteur    = $row['DariPasteur']."->".$row['KeJakartaDariPasteur'];
    }

    $temp_array[$idx]['Counter']		= $row['DariJakarta'];
    $temp_array[$idx]['jakarta']		= $row['DariJakarta']."->".$row['KeBandung'];
    $temp_array[$idx]['bandung']        = $row['DariBandung']."->".$row['KeJakarta'];
    $temp_array[$idx]['pasteur']        = $nama_pasteur;
    $temp_array[$idx]['shortname']      = $row['DariJakarta']."-".$row['DariBandung']."-".$counter_pasteur;
    $temp_array[$idx]['pax']            = $data_tiket_total[$row['IdJurusanJakarta']]['PAX']+$data_tiket_total[$row['IdJurusanBandung']]['PAX']+$data_tiket_total[$id_pasteur]['PAX'];
    $temp_array[$idx]['paket']          = $data_paket_total[$row['IdJurusanJakarta']]['PAKET']+$data_paket_total[$row['IdJurusanBandung']]['PAKET']+$data_paket_total[$id_pasteur]['PAKET'];
    $temp_array[$idx]['trip']           = $jumlah_berangkat[$row['IdJurusanJakarta']]['TRIP']+$jumlah_berangkat[$row['IdJurusanBandung']]['TRIP']+$jumlah_berangkat[$id_pasteur]['TRIP'];
    $temp_array[$idx]['unit']           = $jumlah_unit[$row['IdJurusanJakarta']]['UNIT']+$jumlah_unit[$row['IdJurusanBandung']]['UNIT']+$jumlah_unit[$id_pasteur]['UNIT'];

    $sumpax += $temp_array[$idx]['pax'];
    $sumpaket += $temp_array[$idx]['paket'];
    $sumtrip += $temp_array[$idx]['trip'];
    $sumunit += $temp_array[$idx]['unit'];

    $counter_pasteur = "";
    $id_pasteur = 0;
    $nama_pasteur = "";

    $idx++;
}

$idx=0;


//PLOT DATA
while($idx<count($temp_array)) {

    $odd = 'odd';

    if (($idx % 2) == 0) {
        $odd = 'even';
    }


    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'       =>$odd,
            'no'        =>$idx+1,
            'counter'   =>$temp_array[$idx]['Counter'],
            'jakarta'   =>$temp_array[$idx]['jakarta'],
            'bandung'   =>$temp_array[$idx]['bandung'],
            'pasteur'   =>$temp_array[$idx]['pasteur'],
            'shortname' =>$temp_array[$idx]['shortname'],
            'pax'       =>$temp_array[$idx]['pax'],
            'paket'     =>$temp_array[$idx]['paket'],
            'trip'      =>$temp_array[$idx]['trip'],
            'unit'      =>$temp_array[$idx]['unit']
        )
    );

    $idx++;
}


$export = "Start('laporan_omzet_route_cetak_excel.php?sid=".$userdata['session_id']."&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."');return false;";

$template->assign_vars(
    array(
    'BCRUMP'    		=>setBcrump($id_page),
    'URL'				    => append_sid('laporan_omzet_route.'.$phpEx),
    'TGL_AWAL'			=> $tanggal_mulai,
    'TGL_AKHIR'			=> $tanggal_akhir,
    'SUM_PAX'           => $sumpax,
    'SUM_PAKET'         => $sumpaket,
    'SUM_TRIP'          => $sumtrip,
    'SUM_UNIT'          => $sumunit,
    'CETAK_XL'          => $export,
    )
);


include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>