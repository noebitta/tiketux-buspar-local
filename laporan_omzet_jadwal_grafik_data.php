<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 304;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["SUPERVISOR"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$Cabang								= new Cabang();

if(in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_KEUANGAN))){
	$kondisi_cabang	= ($kode_cabang=="")?"":" AND KodeCabang='$kode_cabang'";
	$cabang_default	= "";
}
else{
	$kondisi_cabang	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]' ";
	$cabang_default	= $userdata['KodeCabang'];
}

//AMBIL DATA CABANG
//Cabang Asal
$data_cabang_asal		= $Cabang->ambilDataDetail($asal);
$data_cabang_tujuan	= $Cabang->ambilDataDetail($tujuan);

$keterangan_asal	= ($data_cabang_asal['Nama']=="")?"Semua Asal":"$data_cabang_asal[Nama] ($data_cabang_asal[KodeCabang]) $data_cabang_asal[Kota]"; 
$keterangan_tujuan= ($data_cabang_tujuan['Nama']=="")?"Semua Tujuan":"$data_cabang_tujuan[Nama] ($data_cabang_tujuan[KodeCabang]) $data_cabang_tujuan[Kota]"; 

if($asal!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$asal'";
}

if($asal!="" && $tujuan!=""){
	$kondisi_cabang.= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan)='$tujuan'";
}


$sql=
	"SELECT 
		IF(MINUTE(JamBerangkat)<30,HOUR(JamBerangkat),HOUR(JamBerangkat)+1) AS Jam,
		IS_NULL(SUM(SubTotal),0) AS TotalOmzet
	FROM tbl_reservasi
	WHERE  (TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1
		$kondisi_cabang
	GROUP BY Jam
	ORDER BY Jam ";


//INISIALISASI
include_once( 'chart/php-ofc-library/open-flash-chart.php' );
$g = new graph();

$data	= array();
$axis	= array();

if (!$result = $db->sql_query($sql)){
	echo("Err $sql:".__LINE__);exit;
}

	$row = $db->sql_fetchrow($result);

	for($idx_jam=0;$idx_jam<24;$idx_jam++){
		
		$axis[$idx_jam]	= $idx_jam;
		
		if($row['Jam']==$idx_jam){
			$data[$idx_jam]	= $row['TotalOmzet'];
			$row = $db->sql_fetchrow($result);
		}
		else{
			$data[$idx_jam]	= 0;
		}
		
	}
	
	$judul_grafik	="Grafik Omzet Per Jam: periode ".$tanggal_mulai." s/d ".$tanggal_akhir." Asal:".$keterangan_asal." Tujuan:".$keterangan_tujuan;
	$legend	="Jam";
	
	// we add 3 sets of data:
	$g->set_data($data);

	// we add the 3 line types and key labels
	//$g->line( 2, '0x9933CC', 'Page views', 10 );
	//$g->line_dot( 3, 5, '0xCC3399', 'Downloads', 10);    // <-- 3px thick + dots
	$g->line_hollow( 2, 4, '0x0000ff', 'Omzet', 12 );
	$g->set_y_legend( 'Jumlah (Rp.)', 12, '#736AFF' );
	
	$temp_max_value	= array(max($data));
	
	$max_value_y	= max($temp_max_value);

	$max_value	= (round(ceil($max_value_y/10)*10)>50)?round(ceil($max_value_y/10)*10):50; 

	$g->title($judul_grafik, '{font-size: 12px; color: #736AFF}' );

	$g->set_x_labels($axis);
	$g->set_x_label_style( 12, '0x000000', 0, 2 );
	$g->set_x_legend($legend, 13, '#736AFF' );

	$g->set_y_max($max_value);

	$g->y_label_steps(5);
	echo $g->render();

?>