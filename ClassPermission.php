<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
/*if(!$userdata['session_logged_in']){
  //redirect('index.'.$phpEx,true); 
	exit;
}*/
//#############################################################################

class Permission{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL;

	//CONSTRUCTOR
	function Permission(){
		$this->ID_FILE="C-PRMS";
    $this->TABEL="tbl_permissions";
	}
	
	//BODY
	
	function periksaDuplikasi($IdRole){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT COUNT(1) AS Ditemukan FROM $this->TABEL  WHERE IdRole=$IdRole";
				
		if (!$result = $db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}


    $row  = $db->sql_fetchrow($result);
    $ditemukan  = $row["Ditemukan"]?true:false;
		
		return $ditemukan;
		
	}//  END periksaDuplikasi

  function ambilGroupPage(){

    //kamus
    global $db;

    $sql =
      "SELECT PageId,NamaTampil
			FROM tbl_pages
			WHERE NamaTampil IS NOT NULL AND GroupPage IS NULL
			ORDER BY PageId";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);
    }

    return $result;

  }//  END ambilData

  function ambilMenuPage($group_page){

    //kamus
    global $db,$userdata,$config;

    $set_kondisi_admin  = $userdata["user_level"]!=$config["role_admin"]?" AND PageId!=".$config["id_pengaturan_halaman"]:"";

    $sql =
      "SELECT PageId,NamaTampil
			FROM tbl_pages
			WHERE NamaTampil IS NOT NULL AND GroupPage IS NOT NULL AND GroupPage=$group_page $set_kondisi_admin
			ORDER BY PageId";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);
    }

    return $result;

  }

  function listPermission($id_role){

    //kamus
    global $db;

    $sql =
      "SELECT  PageId
			FROM $this->TABEL
			WHERE IdRole=$id_role";

    if (!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE:".__LINE__);
    }

    $list_permission=array();

    while($row=$db->sql_fetchrow($result)){
      $list_permission[]  = $row["PageId"];
    }

    return $list_permission;

  }//  END listPermission

  function simpan($IdRole,$list_page){

    //kamus
    global $db;

    //MENGHAPUS PERMISSION YANG ADA
    //MENAMBAHKAN DATA KEDALAM DATABASE
    $sql ="DELETE FROM  $this->TABEL WHERE IdRole=$IdRole";

    if (!$db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);exit;
    }

    foreach($list_page as $page_id){
      //MENAMBAHKAN DATA KEDALAM DATABASE
      $sql ="INSERT INTO $this->TABEL SET IdRole=$IdRole,PageId='$page_id'";

      if (!$db->sql_query($sql)){
        echo("Err:$this->ID_FILE".__LINE__);exit;
      }
    }

    return true;
  }
	
	function ambilDataDetail($IdRole){
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM $this->TABEL
			WHERE IdRole='$IdRole';";
		
		if (!$result = $db->sql_query($sql,TRUE)){
			echo("Err:$this->ID_FILE ".__LINE__);
		}

    $row=$db->sql_fetchrow($result);

    return $row;
		
	}//  END ambilDataDetail

  function getGroupMenu($IdRole){
    global $db;

    $sql =
      "SELECT tp.PageId FROM tbl_permissions tp INNER JOIN tbl_pages tpg ON tp.PageId=tpg.PageId
      WHERE tp.IdRole=$IdRole  AND IsHide=0 AND (GroupPage IS NULL OR GroupPage='') AND (NamaFile IS NOT NULL OR NamaFile='')
      ORDER BY Urutan;";

    if(!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }

  function getMenu($IdRole,$GroupPage=""){
    global $db;
    global $userdata;
    global $config;

    $kondisi_admin  = $userdata["user_level"]!=$config['role_admin']?" AND tpg.PageId!=$config[id_pengaturan_halaman]":"";

    $kondisi_group = ($GroupPage!="")?"AND GroupPage='$GroupPage'":"";

    $sql =
      "SELECT tp.PageId,tpg.* FROM $this->TABEL tp INNER JOIN tbl_pages tpg ON tp.PageId=tpg.PageId
      WHERE tp.IdRole=$IdRole  AND IsHide=0 $kondisi_group $kondisi_admin
      ORDER BY GroupPage,Urutan;";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    return $result;

  }

  function isPermitted($IdRole,$IdPage){

    global $db;

    if($IdPage==0){
      //JIKA ID PAGE=0, TIDAK MEMERLUKAN AUTENTIFIKASI
      return true;
    }

    $sql =
      "SELECT COUNT(1) IsPermitted FROM $this->TABEL
      WHERE IdRole=$IdRole  AND PageId='$IdPage'";

    if(!$result = $db->sql_query($sql)){
      echo("Err: $this->ID_FILE".__LINE__);
    }

    $row  = $db->sql_fetchrow($result);

    return ($row["IsPermitted"]==1?true:false);

  }
  
}
?>