<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 409;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################
//PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$cabang         = isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];
$awal           = isset($HTTP_GET_VARS['awal'])? $HTTP_GET_VARS['awal'] : $HTTP_POST_VARS['awal'];
//inisialisasi
$today = strtotime('now');
$yesterday = date('Y-m-d',strtotime('-1 day', $today));
$tomorrow = date('Y-m-d',strtotime('+1 day', $today));
$USER = $userdata['nama'];
switch($mode){
    case 'validasi':
        $ID = $HTTP_GET_VARS['id'];
        $CABANG = $HTTP_GET_VARS['cabang'];
        $MODAL = str_replace(',','.',$HTTP_GET_VARS['modal']);
        $SALDO = str_replace('.','',$HTTP_GET_VARS['saldo']);
        $SETORAN = str_replace('.','',$HTTP_GET_VARS['setoran']);
        //UPDATE MODAL AKHIR HARI INI
        $sql = "UPDATE tbl_md_cabang_modal SET ModalAkhir = '$SALDO', JumlahTransaksi='$SETORAN', TglTransaksiModal=NOW(),
                Keterangan = 'DATA TELAH DIVALIDASI USER $USER' WHERE IDModal ='$ID'";
        if(!$data = $db->sql_query($sql)){
            echo("alert('GAGAL VALIDASI');");
            exit;
        }

        $sql = "SELECT * FROM tbl_md_cabang_modal WHERE KodeCabang = '$CABANG' AND TglTransaksiModal = '$tomorrow'";

        if(!$data = $db->sql_query($sql)){
            echo("alert('GAGAL CEK MODAL AWAL BESOK');");
            exit;
        }else{
            $ModalCABANG = $db->sql_fetchrow($data);
        }

        if($ModalCABANG['KodeCabang'] == null){
            //SET MODAL AWAL BESOK
            $sql = "INSERT INTO tbl_md_cabang_modal(KodeCabang,ModalAwal,Keterangan,TglTransaksiModal)VALUES('$CABANG',$SALDO,'MODAL AWAL BESOK DISET USER $USER',NOW() + INTERVAL 1 DAY)";
            if(!$data = $db->sql_query($sql)){
                echo("alert('GAGAL CLOSE DATA');");
                exit;
            }
        }else{
            //UPDATE MODAL AWAL BESOK KARENA TELAH DISET SEBELUMNYA
            $sql = "UPDATE tbl_md_cabang_modal SET ModalAwal = $SALDO, Keterangan ='MODAL  AWAL BESOK DIUPDATE USER $USER'WHERE KodeCabang = '$CABANG' AND TglTransaksiModal ='$tomorrow'";
            if(!$data = $db->sql_query($sql)){
                echo("alert('GAGAL UPDATE CLOSE DATA');");
                exit;
            }
        }

        echo ("alert('DATA TELAH DIVALIDASI');");
    exit;
}
$saldo_akhir = 0;
$awal = date_format(date_create($awal),"Y-m-d");
if($cabang != '' && $awal != '' && strtotime($awal) <= strtotime(date('Y-m-d'))) {

        //QUERY AMBIL MODAL CABANG
        $sql = "SELECT IDModal, IS_NULL(ModalAwal,0) as ModalAwal FROM tbl_md_cabang_modal WHERE KodeCabang='$cabang' AND TglTransaksiModal = '$awal' ";

        if(!$data = $db->sql_query($sql)){
            echo("Err:".__LINE__."<br>");
            die(mysql_error());
        }else{
            $dataModal = $db->sql_fetchrow($modal);
            $modalcabang = ($dataModal['ModalAwal'] == null)?0:$dataModal['ModalAwal'];
        }

        //QUERY AMBIL MODAL FIX DI TABEL CABANG
        $sql = "SELECT ModalCabang FROM tbl_md_cabang WHERE KodeCabang='$cabang'";
        if(!$data = $db->sql_query($sql)){
            echo("Err:".__LINE__."<br>");
            die(mysql_error());
        }else{
            $result = $db->sql_fetchrow($modal);
            $MODALFIX = ($result['ModalCabang'] == null)?0:$result['ModalCabang'];
        }


    //QUERY AMBIL LIST SPJ DI TABEL RESERVASI YANG SUDAH ADA CETAK TIKET
    $sql = "SELECT NoSPJ, Driver, tbl_md_kendaraan.NoPolisi AS NOPOL,tbl_md_jurusan.KodeJurusan AS Jurusan, JamBerangkat
            FROM tbl_spj JOIN tbl_md_jurusan
            ON tbl_spj.IdJurusan = tbl_md_jurusan.IdJurusan
            LEFT JOIN tbl_md_kendaraan
            ON tbl_spj.NoPolisi = tbl_md_kendaraan.KodeKendaraan
            WHERE tbl_md_jurusan.KodeCabangAsal = '$cabang'
            AND DATE(tbl_spj.TglBerangkat) = '$awal'";
    if (!$spj = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }

    // QUERY PEMBAYARAN CASH PAKET
    $sql = "SELECT
            NoSPJ,
            SUM(HargaPaket) AS DEBIT,
            DATE(TglCetakSPJ) AS TGLSPJ,
            JamBerangkat,
            JenisPembayaran
            FROM
                tbl_paket
            WHERE KodeCabang = '$cabang'AND JenisPembayaran = 0 AND CetakTiket = 1 AND TglBerangkat = '$awal'
            GROUP BY NoSPJ";

    if(!$paket_cash = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }
    while($row = $db->sql_fetchrow($paket_cash)){
        $data_paket_cash_total[$row['NoSPJ']] = $row;
    }

    // QUERY PEMBAYARAN EDC PAKET
    $sql = "SELECT
            NoSPJ,
            SUM(HargaPaket) AS DEBIT,
            DATE(TglCetakSPJ) AS TGLSPJ,
            JamBerangkat,
            JenisPembayaran
            FROM
                tbl_paket
            WHERE KodeCabang = '$cabang'AND JenisPembayaran = 5 AND CetakTiket = 1 AND TglBerangkat = '$awal'
            GROUP BY NoSPJ";

    if(!$paket_edc = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }
    while($row = $db->sql_fetchrow($paket_edc)){
        $data_paket_edc_total[$row['NoSPJ']] = $row;
    }

    // QUERY PEMBAYARAN CASH
    $sql = "SELECT
                NoSPJ,
                SUM(Total) AS DEBIT,
                JamBerangkat,
                JenisPembayaran
            FROM
                tbl_reservasi
            WHERE JenisPembayaran = 0 AND CetakTiket = 1 AND TglBerangkat = '$awal'
            GROUP BY NoSPJ";
    if (!$cash = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }
    while($row = $db->sql_fetchrow($cash)){
        $data_cash_total[$row['NoSPJ']] = $row;
    }

    //QUERY PEMBAYARAN EDC
    $sql = "SELECT
                NoSPJ,
                SUM(Total) AS DEBIT,
                JamBerangkat,
                JenisPembayaran
            FROM
                tbl_reservasi
            WHERE JenisPembayaran = 5 AND CetakTiket = 1 AND TglBerangkat = '$awal'
            GROUP BY NoSPJ";
    if (!$edc = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }
    while ($row = $db->sql_fetchrow($edc)){
        $data_edc_total[$row['NoSPJ']]	= $row;
    }

    //QUERY PEMBAYARAN TIKETUK
    $sql = "SELECT
                NoSPJ,
                SUM(Total) AS DEBIT,
                JamBerangkat,
                JenisPembayaran
            FROM
                tbl_reservasi
            WHERE JenisPembayaran = 99 AND CetakTiket = 1 AND TglBerangkat = '$awal'
            GROUP BY NoSPJ";
    if (!$sistem = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }
    while ($row = $db->sql_fetchrow($sistem)){
        $data_sistem_total[$row['NoSPJ']]	= $row;
    }

    //QUERY BIAYA OPERASIONAL
    $sql ="SELECT
                tbl_biaya_op.NoSPJ,
                SUM(Jumlah) AS KREDIT
            FROM
                tbl_biaya_op
            WHERE
            FlagJenisBiaya IN (1, 2) AND TglTransaksi = '$awal'
            GROUP BY
                tbl_biaya_op.NoSPJ";

    if (!$biaya_op = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }
    while ($row = $db->sql_fetchrow($biaya_op)){
        $data_credit_total[$row['NoSPJ']]	= $row;
    }

    $temp_array=array();
    $idx=0;

    $sum_cash   = 0;
    $sum_edc    = 0;
    $sum_sistem = 0;
    $sum_lpoc   = 0;
    while($debit = $db->sql_fetchrow($spj)){
        $temp_array[$idx]['NoSPJ']			= $debit['NoSPJ'];
        $temp_array[$idx]['KodeJurusan']    = $debit['Jurusan'];
        $temp_array[$idx]['JamBerangkat']   = $debit['JamBerangkat'];
        $temp_array[$idx]['NoPolisi']       = $debit['NOPOL'];
        $temp_array[$idx]['Sopir']          = $debit['Driver'];
        $temp_array[$idx]['Cash']           = $data_cash_total[$debit['NoSPJ']]['DEBIT']+ $data_paket_cash_total[$debit['NoSPJ']]['DEBIT'];
        $temp_array[$idx]['Edc']            = $data_edc_total[$debit['NoSPJ']]['DEBIT']+ $data_paket_edc_total[$debit['NoSPJ']]['DEBIT'];
        $temp_array[$idx]['Sistem']         = $data_sistem_total[$debit['NoSPJ']]['DEBIT'];
        $temp_array[$idx]['LPOC']           = $data_credit_total[$debit['NoSPJ']]['KREDIT'];


        $sum_cash   +=$data_cash_total[$debit['NoSPJ']]['DEBIT']+ $data_paket_cash_total[$debit['NoSPJ']]['DEBIT'];
        $sum_edc    +=$data_edc_total[$debit['NoSPJ']]['DEBIT']+ $data_paket_edc_total[$debit['NoSPJ']]['DEBIT'];;
        $sum_sistem +=$data_sistem_total[$debit['NoSPJ']]['DEBIT'];
        $sum_lpoc   +=$data_credit_total[$debit['NoSPJ']]['KREDIT'];

        $idx++;
    }

    $idx=0;
    while($idx<count($temp_array)){

        $total_cash     = $temp_array[$idx]['Cash'];
        $total_edc      = $temp_array[$idx]['Edc'];
        $total_sistem   = $temp_array[$idx]['Sistem'];
        $total_lpoc     = $temp_array[$idx]['LPOC'];
        $odd ='odd';

        if (($idx % 2)==0){
            $odd = 'even';
        }

        $template->
        assign_block_vars(
            'ROW',
            array(
                'odd'           =>$odd,
                'no'            =>$idx+1,
                'NoSPJ'         =>$temp_array[$idx]['NoSPJ'],
                'KodeJurusan'   =>$temp_array[$idx]['KodeJurusan'],
                'JamBerangkat'  =>date_format(date_create($temp_array[$idx]['JamBerangkat']),'H:i'),
                'NoPolisi'      =>$temp_array[$idx]['NoPolisi'],
                'Sopir'         =>$temp_array[$idx]['Sopir'],
                'total_cash'    =>number_format($total_cash,0,",","."),
                'total_edc'     =>number_format($total_edc,0,",","."),
                'total_sistem'  =>number_format($total_sistem,0,',','.'),
                'total_lpoc'    =>number_format($total_lpoc,0,',','.'),
            )
        );
        $idx++;
    }

    //QUERY BIAYA TAMBAHAN
    $sql = "SELECT KodeCabang,SUM(Jumlah) AS Jumlah,KodeJurusan, Keterangan, NamaPenerima, JenisBiaya
            FROM tbl_biaya_tambahan
            WHERE DATE(TglCetak)='$awal' AND KodeCabang = '$cabang'
            GROUP BY NamaPenerima, TglCetak ";
    if (!$other = $db->sql_query($sql)){
        echo("Err:".__LINE__."<br>");
        die(mysql_error());
    }


 if($db->sql_numrows($other)){
    $sum_other = 0;
     $index = $idx;
        while($dataother = $db->sql_fetchrow($other)){
            $sum_other += $dataother['Jumlah'];

            if (($index % 2)==0){
                $odd = 'even';
            }

            $template->
            assign_block_vars(
                'ROWBIAYA',
                array(
                    'odd'           =>$odd,
                    'no'            =>$index+1,
                    'Penerima'      =>$dataother['NamaPenerima'],
                    'Jurusan'       =>$dataother['KodeJurusan'],
                    'Keterangan'    =>$dataother['Keterangan'],
                    'Jumlah'        =>$dataother['Jumlah'],
                    )
            );
            $index++;
        }
    }
    //END PENANGANAN BIAYA TAMBAHAN

    // jika pendapatan lebih besar dari pengeluaran maka modal jangan dipake
    if($sum_cash >= ($sum_lpoc+$sum_other)){
        $saldo_akhir = $modalcabang;
    }else{
        $saldo_akhir = $modalcabang + $sum_cash - ($sum_lpoc+$sum_other);
    }

    //jika modal terakhir kurang dari MODALFIX maka harus digenapkan lagi jadi MODALFIX
    if($modalcabang < $MODALFIX){
        if(($sum_cash - ($sum_lpoc+$sum_other)) > ($MODALFIX- $modalcabang)){
            $saldo_akhir = $MODALFIX;
        }else{
            $saldo_akhir = $modalcabang + $sum_cash - ($sum_lpoc+$sum_other);
        }
    }


    $setoran = $sum_cash - ($sum_lpoc+$sum_other);
    $interface_menu_utama=false;
    $parameter_cetak = "&p1=".$cabang."&p2=".$awal;
    $script_excel ="Start('laporan_penerimaan_pengeluaran_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
}


$page_title	= "LPPC";
// Dropdown Cabang
function setDropdownCabang($cabang){
    global $db;

    $Cabang	= new Cabang();

    $result	= $Cabang->setComboCabang();

    $opt = "";

    if ($result){
        $opt = "<option value=''>(none)</option>" . $opt;

        while ($row = $db->sql_fetchrow($result)){
            $selected = ($cabang != $row[0])?"":"selected";
            $opt .= "<option id='$row[0]' value='$row[0]' $selected>$row[1]</option>";
        }
    }
    else{
        $opt .="<option selected=selected>Error</option>";
    }

    return $opt;
}

$template->set_filenames(array('body' => 'laporan_penerimaan_pengeluaran/laporan_penerimaan_pengeluaran_body.tpl'));

$template->assign_vars(array(
    'BCRUMP'    		    =>setBcrump($id_page),
    'SELECT'            => setDropdownCabang($cabang),
    'AWAL'				      => date_format(date_create($awal),'d-m-Y'),
    'CABANG'            => $cabang,
    'IDMODAL'           => $dataModal['IDModal'],
    'MODAL'             => number_format($modalcabang,0,',','.'),
    'BLAIN'             => number_format($sum_other,0,',','.'),
    'SALDO'             => number_format($saldo_akhir,0,',','.'),
    'SETORAN'           => number_format($setoran,0,',','.'),
    'TOTAL_CASH'        => number_format($sum_cash,0,',','.'),
    'TOTAL_EDC'         => number_format($sum_edc,0,',','.'),
    'TOTAL_SISTEM'      => number_format($sum_sistem,0,',','.'),
    'TOTAL_LPOC'        => number_format($sum_lpoc,0,',','.'),
    'ACTION_CARI'       => append_sid('laporan_penerimaan_pengeluaran.'.$phpEx),
    'EXCEL'             => $script_excel
));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>