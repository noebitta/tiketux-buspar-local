<?php
//
// CETAK TIKET UNTUK LINUX
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPaket.php');
include($adp_root_path . 'ClassReservasi.php');
include($adp_root_path . 'ClassJurusan.php');

// SESSION
$id_page = 202;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 				= $config['perpage'];
$mode    				= $HTTP_GET_VARS['mode'];
$submode 				= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   				= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination
$no_tiket				= str_replace("\'","",$HTTP_GET_VARS['no_tiket']);			
$jenis_pembayaran	= $HTTP_GET_VARS['jenis_pembayaran'];		
$cso							= $userdata['nama'];

$Reservasi	= new Reservasi();
$Paket			= new Paket();

	//mengambil data berdasarkan no tiket, dan hanya dapat mencetak 1 tiket saja
	$row	= $Paket->ambilDataDetail($no_tiket);
	
	$no_tiket=$row['NoTiket'];
	$nama_pengirim=$row['NamaPengirim'];
	$telp_pengirim=$row['TelpPengirim'];
	$alamat_pengirim=$row['AlamatPengirim'];
	$nama_penerima=$row['NamaPenerima'];
	$telp_penerima=$row['TelpPenerima'];
	$alamat_penerima=$row['AlamatPenerima'];
	$tanggal=$row['TglBerangkat'];
	$jam=$row['JamBerangkat'];
	$asal=explode("(",$row['NamaAsal']);
	$tujuan=explode("(",$row['NamaTujuan']);
	$harga_paket=number_format($row['HargaPaket'],0,",",".");
	$operator=$row['NamaCSO'];
	$cetak_tiket=$row['CetakTiket'];
	$jenis_pembayaran=($row['JenisPembayaran']=='')?$jenis_pembayaran:$row['JenisPembayaran'];
	$kode_jadwal=$row['KodeJadwal'];
	$cara_bayar	= $row['CaraBayar'];
	$id_jurusan	= $row['IdJurusan'];
	
	//EXPORT KE PDF
	class PDF extends FPDF {
		function Footer() {
			$this->SetY(-1.5);
		  $this->SetFont('Arial','I',8);
		  $this->Cell(0,1,'',0,0,'R');
		}
	}
				
	//set kertas & file
	$pdf=new PDF('P','cm','A4');
	$pdf->Open();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	$pdf->Setmargins(0.5,0,0,0);
	$pdf->SetFont('courier','',9);
	$i=0;
	$line_space	=0.3;
	
	//TIKET==========
	
	//content
	$pdf->Ln(2);
	$pdf->Cell(3,$line_space,dateparse(FormatMySQLDateToTgl($tanggal)),0,0,'C');
	$pdf->Cell(3.4,$line_space,substr($asal[0],0,20),0,0,'C');
	$pdf->Cell(3.4,$line_space,substr($tujuan[0],0,20),0,0,'C');
	$pdf->Cell(2.6,$line_space,$jam,0,0,'C');
	$pdf->Cell(2,$line_space,$row['JumlahKoli'],0,0,'C');
	$pdf->Cell(2,$line_space,$row['Berat'],0,0,'C');
	$pdf->Cell(1,$line_space,"",0,0,'C');
	$pdf->Cell(3,$line_space,$row['NoTiket'],0,0,'L');
	$pdf->Ln(0.9);
	
	//baris kedua
	$tunai			="";
	$langganan	= "";
	
	if($row['CaraPembayaran']==0){
		$tunai="X";
	}
	else{
		$langganan="X";
	}
	$pdf->Cell(0.5,$line_space,"",0,0,'L');$pdf->Cell(4,$line_space,$tunai,0,0,'L');
	$pdf->Cell(8,$line_space,substr($nama_pengirim,0,15),0,0,'L');$pdf->Cell(8,$line_space,substr($nama_penerima,0,15),0,0,'L');
	$pdf->Ln(0.3);
	$pdf->Cell(3,$line_space,$langganan,0,0,'L');
	$pdf->Ln(1.1);
	$pdf->Cell(0.5,$line_space,"",0,0,'L');$pdf->Cell(4,$line_space,$harga_paket,0,0,'L');
	$pdf->Ln(0.3);
	$pdf->Cell(5,$line_space,'',0,0,'L');$pdf->Cell(8.5,$line_space,$telp_pengirim,0,0,'');
	$pdf->Cell(8,$line_space,$telp_penerima,0,0,'');
	$pdf->Ln(1.1);
	
	$jenis_dokumen	= "";
	$jenis_paket		= "";
	if($row['JenisBarang']==0){
		$jenis_dokumen="X";
	}
	else{
		$jenis_paket="X";
	}
	
	$layanan_p	= "";
	$layanan_ga	= "";
	$layanan_gd	= "";
	$layanan_s	= "";
	
	if($row['Layanan']=='P'){
		$layanan_p="X";
	}
	elseif($row['Layanan']=='GA'){
		$layanan_ga="X";
	}
	elseif($row['Layanan']=='GD'){
		$layanan_gd="X";
	}
	elseif($row['Layanan']=='S'){
		$layanan_s="X";
	}
	
	$pdf->Cell(0.5,$line_space,"",0,0,'L');$pdf->Cell(1.5,$line_space,$jenis_dokumen,0,0,'L');$pdf->Cell(2,$line_space,$jenis_paket,0,0,'L');
	$pdf->Cell(0.7,$line_space,"",0,0,'L');
	$pdf->Cell(1,$line_space,$layanan_p,0,0,'L');$pdf->Cell(1.3,$line_space,$layanan_ga,0,0,'L');$pdf->Cell(1.2,$line_space,$layanan_gd,0,0,'L');$pdf->Cell(1.5,$line_space,$layanan_s,0,0,'L');
	$pdf->Cell(5,$line_space,$row['KeteranganPaket'],0,0,'L');
	$pdf->Ln(1.7);
	$pdf->Cell(14.2,$line_space,"",0,0,'');
	$pdf->Cell(3,$line_space,substr($asal[0],0,20),0,0,'C');$pdf->Cell(3,$line_space,substr($tujuan[0],0,20),0,0,'C');
	$pdf->Ln(0.3);
	$pdf->Cell(5.3,$line_space,"",0,0,'');$pdf->Cell(9.2,$line_space,$nama_pengirim,0,0,'');
		
	//mengupate flag cetak tiket
	if($cetak_tiket!=1){
		$cabang_transaksi	=($cara_bayar<$PAKET_CARA_BAYAR_DI_TUJUAN)?$userdata['KodeCabang']:"";
		$Paket->updateCetakTiket($no_tiket,$jenis_pembayaran,$cabang_transaksi);
		
	}
	
	$pdf->Output();
	
?>