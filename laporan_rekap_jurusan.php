<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
$Cabang	= new Cabang();
// SESSION
$id_page = 303;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//################################################################################

$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];

$bulan = isset($HTTP_GET_VARS['bulan'])? $HTTP_GET_VARS['bulan'] : $HTTP_POST_VARS['bulan'];
$tahun = isset($HTTP_GET_VARS['tahun'])? $HTTP_GET_VARS['tahun'] : $HTTP_POST_VARS['tahun'];

$bulan = ($bulan != "")?$bulan:date('m');
$tahun = ($tahun != "")?$tahun:date('Y');



$kondisi = ($asal == "")?"":"AND KodeCabangAsal = '$asal'";

$kondisi .= ($tujuan == "")?"":"AND KodeCabangTujuan = '$tujuan'";

switch($mode){
    case 'getasal':

        echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";

        exit;

    case 'gettujuan':
        echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
        exit;
}

//LIST BULAN
$list_bulan="";
$laporan = array();
for($idx_bln=1;$idx_bln<=12;$idx_bln++){

    $font_size	= 2;
    $font_color='';

    if($bulan==$idx_bln){
        $font_size=4;
        $font_color='008609';
    }

    $list_bulan	.="<a href='#' onClick='setData($idx_bln);return false;'><font size=$font_size color='$font_color'>".BulanString($idx_bln)."</font></a>|&nbsp;";

}

$sql_jurusan="SELECT 
                    IdJurusan,KodeJurusan,f_cabang_get_name_by_kode(KodeCabangAsal) AS CabangAsal,
                    f_cabang_get_name_by_kode(KodeCabangTujuan) AS CabangTujuan,
                    KodeCabangAsal
              FROM tbl_md_jurusan
              WHERE 1 $kondisi
              ORDER BY CabangAsal";
if (!$result_laporan = $db->sql_query($sql_jurusan)){
    echo $sql_jurusan;
    echo("Err:".__LINE__);exit;
}

$sql_tiket = "SELECT 
                    IdJurusan, TglBerangkat, COUNT(NoTiket) AS PAX 
              FROM tbl_reservasi
              WHERE
                    MONTH(TglBerangkat) = $bulan
                    AND YEAR (TglBerangkat) = $tahun
                    AND CetakTiket = 1 
                    AND FlagBatal != 1
              GROUP BY 
                   IdJurusan, TglBerangkat";
if (!$tiket = $db->sql_query($sql_tiket)){
    echo("Err:".__LINE__);exit;
}
while($row = $db->sql_fetchrow($tiket)){
    $result_tiket[$row['IdJurusan']][$row['TglBerangkat']] = $row['PAX'];
}

$sql_sum_tiket = "SELECT 
                    tbl_reservasi.IdJurusan, TglBerangkat, COUNT(NoTiket) AS SUM_PAX 
              FROM tbl_reservasi
              JOIN tbl_md_jurusan ON tbl_reservasi.IdJurusan = tbl_md_jurusan.IdJurusan
              WHERE
                    MONTH(TglBerangkat) = $bulan
                    AND YEAR (TglBerangkat) = $tahun
                    AND CetakTiket = 1 
                    AND FlagBatal != 1
              $kondisi      
              GROUP BY TglBerangkat";
if (!$list_sum_tiket = $db->sql_query($sql_sum_tiket)){
    echo mysql_error();
    echo("Err:".__LINE__);exit;
}
while($row = $db->sql_fetchrow($list_sum_tiket)){
    $result_sum_tiket[$row['TglBerangkat']] = $row['SUM_PAX'];
}


$sql_paket = "SELECT 
                   IdJurusan, TglBerangkat, COUNT(NoTiket) AS PAKET 
              FROM tbl_paket
              WHERE
                    MONTH(TglBerangkat) = $bulan
                    AND YEAR (TglBerangkat) = $tahun
                    AND CetakTiket = 1 
                    AND FlagBatal != 1
              GROUP BY 
                   IdJurusan, TglBerangkat";
if (!$paket = $db->sql_query($sql_paket)){
    echo("Err:".__LINE__);exit;
}
while($row = $db->sql_fetchrow($paket)){
    $result_paket[$row['IdJurusan']][$row['TglBerangkat']] = $row['PAKET'];
}

$sql_sum_paket = "SELECT 
                    tbl_paket.IdJurusan, TglBerangkat, COUNT(NoTiket) AS SUM_PAKET 
              FROM tbl_paket
              JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
              WHERE
                    MONTH(TglBerangkat) = $bulan
                    AND YEAR (TglBerangkat) = $tahun
                    AND CetakTiket = 1 
                    AND FlagBatal != 1
              $kondisi      
              GROUP BY TglBerangkat";
if (!$list_sum_paket = $db->sql_query($sql_sum_paket)){
    echo mysql_error();
    echo("Err:".__LINE__);exit;
}
while($row = $db->sql_fetchrow($list_sum_paket)){
    $result_sum_paket[$row['TglBerangkat']] = $row['SUM_PAKET'];
}


$jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

$startdate = "{$tahun}/{$bulan}/01";
$enddate = "{$tahun}/{$bulan}/{$jml_hari}";

$start = strtotime($startdate);
$end = strtotime($enddate);

$currentdate = $start;

while($currentdate <= $end){

    $cur_date = date('Y-m-d', $currentdate);

    $template->
    assign_block_vars(
        'HARI',
        array(
            'hari' => date_format(date_create($cur_date),'j'),
            'sum_tiket' => $result_sum_tiket[$cur_date],
            'sum_paket' => $result_sum_paket[$cur_date],
        )
    );
    $currentdate = strtotime('+1 days', $currentdate);
}






$idx = 0;
while($jurusan = $db->sql_fetchrow($result_laporan)){

    $jml_hari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

    $startdate = "{$tahun}/{$bulan}/01";
    $enddate = "{$tahun}/{$bulan}/{$jml_hari}";

    $start = strtotime($startdate);
    $end = strtotime($enddate);

    $currentdate = $start;

    $jml_tiket = "";
    $jml_paket = "";
    while($currentdate <= $end){
        $cur_date = date('Y-m-d', $currentdate);

        $jml_tiket .= "<td width='5'>".$result_tiket[$jurusan['IdJurusan']][$cur_date]."</td>";
        $jml_paket .= "<td width='5'>".$result_paket[$jurusan['IdJurusan']][$cur_date]."</td>";

        $currentdate = strtotime('+1 days', $currentdate);
    }

    $odd = 'odd';

    if (($idx % 2) == 0) {
        $odd = 'even';
    }

    $template->
    assign_block_vars(
        'ROW',
        array(
            'odd'       => $odd,
            'no'        => $idx+1,
            'jurusan'   => $jurusan['CabangAsal']." => ".$jurusan['CabangTujuan'],
            'jml_tiket' => $jml_tiket,
            'jml_paket' => $jml_paket,
        )
    );


    $idx++;

}


$cetak_excel = "Start('laporan_rekap_jurusan_cetak_excel.php?sid=".$userdata['session_id']."&bulan=".$bulan."&tahun=".$tahun."');return false;";

$template->assign_vars(
    array(
        'BCRUMP'    		=>setBcrump($id_page),
        'URL'				    => append_sid('laporan_rekap_jurusan.'.$phpEx),
        'LIST_BULAN'		=> "| ".$list_bulan,
        'BULAN'				  => $bulan,
        'TAHUN'				  => $tahun,
        'CETAK_XL'      => $cetak_excel,
        'JML_HARI'      => $jml_hari,
        'ASAL'					=> $asal,
        'TUJUAN'				=> $tujuan,
    )
);

$template->set_filenames(array('body' => 'laporan_rekap_jurusan/laporan_rekap_jurusan_body.tpl'));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>