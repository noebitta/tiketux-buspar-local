<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page = 408;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//INCLUDES
include($adp_root_path . 'ClassPayoutCGS.php');

//METHODES

// PARAMETER
$idx_page       = isset($HTTP_POST_VARS["idxpage"])?$HTTP_POST_VARS["idxpage"]:$HTTP_GET_VARS["idxpage"];
$cari           = isset($HTTP_POST_VARS["cari"])?$HTTP_POST_VARS["cari"]:$HTTP_GET_VARS["cari"];
$siklus         = isset($HTTP_POST_VARS['siklus'])?$HTTP_POST_VARS['siklus'] : $HTTP_GET_VARS['siklus'];
$fil_bln        = isset($HTTP_POST_VARS['bulan'])?$HTTP_POST_VARS['bulan']:$HTTP_GET_VARS['bulan'];
$fil_thn       = isset($HTTP_POST_VARS['tahun'])?$HTTP_POST_VARS['tahun']:$HTTP_GET_VARS['tahun'];
$order_by       = isset($HTTP_POST_VARS['orderby'])?$HTTP_POST_VARS['orderby']:$HTTP_GET_VARS['orderby'];
$sort           = isset($HTTP_POST_VARS['sort'])?$HTTP_POST_VARS['sort']:$HTTP_GET_VARS['sort'];
$order_by       = $order_by==""?1:$order_by; //2=>kolom Nama

$siklus   = ($siklus=="")?0:$siklus;
$fil_bln	= ($fil_bln!='')?$fil_bln:date("m");
$fil_thn	= ($fil_thn!='')?$fil_thn:date("Y");

$PayoutCGS  = new PayOutCGS();

$idx_page   = $idx_page==""?0:$idx_page;

$result = $PayoutCGS->getDataCGS($siklus,$fil_bln,$fil_thn,$order_by,$sort,$idx_page,$cari);

//PAGING======================================================
$paging=setPaging($idx_page,"formdata");
//END PAGING==================================================

$idx = 0;

//PLOT DATA
while($data=$db->sql_fetchrow($result)){

  $odd =($idx%2)==0?"even":"odd";

  $list_jurusan = "";

  if($data["Jurusan"]!="") {
    $arr_jurusan = explode(",", $data["Jurusan"]);

    $arr_count_jurusan = array_count_values($arr_jurusan);

    foreach ($arr_count_jurusan as $key => $value) {
      $list_jurusan .= $key . ":" . $value . "| ";
    }

    $list_jurusan = substr($list_jurusan, 0, -2);
  }

  $remark = $data["IdPayout"]==""?"":"Payout:".dateparseWithTime(FormatMySQLDateToTglWithTime($data["WaktuPayout"]))."| Via:".($data["PayoutVia"]==0?"Cash":"Transfer")."| #Ref:".$data["KodeReferensi"]."| Oleh:".$data["NamaPetugas"];

  $template->assign_block_vars(
    'ROW',array(
      'odd'       =>$odd,
      'no'        =>$idx_page*$VIEW_PER_PAGE+$idx+1,
      'kodesopir' =>$data['KodeSopir'],
      'nama'      =>$data['Nama'],
      'norek'     =>$data['NoRek'],
      'bank'      =>$data['NamaBank'],
      'cabangbank'=>$data['CabangBank'],
      'jurusan'   =>$list_jurusan,
      'totaltrip' =>number_format($data['TotalTrip'],0,",","."),
      'totalcgs'  =>"Rp.".number_format($data['TotalCGS'],0,",","."),
      'remark'    =>$remark
    )
  );

  //ACTION BUTTON BROWSE DATA
  if($Permission->isPermitted($userdata["user_level"],408.2)) {

    if($data['TotalTrip']>0){
      $act      = "browseData('$data[KodeSopir]','$data[Nama]');";
      $disabled = "";
    }
    else{
      $act      = "";
      $disabled = "_disabled";
    }

    $template->assign_block_vars(
      "ROW.ACT_BROWSE", array(
        "title"     => "Browse Data",
        "act"       => $act,
        "disabled"  => $disabled
      )
    );
  }

  if($data["IdPayout"]=="") {

    //ACTION BUTTON PAYOUT
    if ($Permission->isPermitted($userdata["user_level"], 408.1)) {

      if ($data['TotalTrip'] > 0) {
        $act = "pilihBayar('$data[KodeSopir]','$data[Nama]');";
        $disabled = "";
      } else {
        $act = "";
        $disabled = "_disabled";
      }

      $template->assign_block_vars(
        "ROW.ACT_PAY", array(
          "title" => "Payout",
          "act" => $act,
          "disabled" => ($data['TotalTrip'] > 0 ? "" : "_disabled")
        )
      );
    }
  }
  else{
    //ACTION BUTTON CETAK
    if ($Permission->isPermitted($userdata["user_level"], 408.1)) {
      $template->assign_block_vars(
        "ROW.ACT_PRINT", array(
          "title" => "Cetak Resi",
          "act" => "cetakResi($data[IdPayout])"
        )
      );
    }
  }

  $idx++;
}

if($idx>0){
  $template->assign_block_vars("TABLE_HEADER",array());
}
else{
  $template->assign_block_vars("NO_DATA",array());
}

for($bln=1; $bln<=12;$bln++){
  $template->assign_block_vars("OPT_BLN",array(
    "value"   => $bln,
    "text"    => BulanStringShort($bln),
    "selected"=> ($fil_bln!=$bln)?"":"selected"
  ));
}

$list_tahun = $PayoutCGS->getListTahunCGS();

foreach($list_tahun as $thn){
  $template->assign_block_vars("OPT_THN",array(
    "value"   => $thn,
    "text"    => $thn,
    "selected"=> ($fil_thn!=$thn)?"":"selected"
  ));
}

$data_summary = $PayoutCGS->summaryCGS($siklus,$fil_bln,$fil_thn);

$page_title	= "Payout CGS";

$template->set_filenames(array('body' =>'keuangan.payoutcgs/index.tpl'));

$template->assign_block_vars("TOMBOL_CRUD",array());

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&siklus=$siklus&bulan=$fil_bln&tahun=$fil_thn&orderby=$order_by&sort=$sort&cari=$cari";

$script_cetak_excel="keuangan.payoutcgs.export.excel.php?sid=".$userdata['session_id'].$parameter_cetak;
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(
  'BCRUMP'    	        =>setBcrump($id_page),
  'ACTION_CARI'		      => append_sid(basename(__FILE__)),
  'EXPORT_EXCEL'		    => $script_cetak_excel,
  'IDX_PAGE'		        => $idx_page,
  'PAGING'		          => $paging,
  'CARI'		            => $cari,
  'ORDER_BY'		        => $order_by,
  'SORT'		            => $sort,
  'CARI'		            => $cari,
  'OPT_SIKLUS'.$siklus  => "selected",
  'PERIODE_SIKLUS'      => ($siklus==0?"Tgl 26 s.d. 10":"Tgl 11 s.d. 25"),
  'TOTAL_TRIP'          => number_format($data_summary["TotalTrip"],0,",","."),
  'TOTAL_CGS'           => number_format($data_summary["TotalCGS"],0,",","."),
  'TOTAL_PAYOUT'        => number_format($data_summary["TotalPayout"],0,",","."),
  'PAYOUT_TERHUTANG'    => number_format($data_summary["TotalCGS"]-$data_summary["TotalPayout"],0,",","."))
);
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>