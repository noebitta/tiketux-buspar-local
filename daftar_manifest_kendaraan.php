<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassMobil.php');
include($adp_root_path . 'ClassPengaturanUmum.php');
// SESSION
$id_page = 217;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['CHECKER'],$USER_LEVEL_INDEX['SPV_CHECKER']))){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$kota  			= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$asal  			= isset($HTTP_GET_VARS['asal'])? $HTTP_GET_VARS['asal'] : $HTTP_POST_VARS['asal'];
$tujuan  		= isset($HTTP_GET_VARS['tujuan'])? $HTTP_GET_VARS['tujuan'] : $HTTP_POST_VARS['tujuan'];
$spbu           = isset($HTTP_GET_VARS['spbu'])? $HTTP_GET_VARS['spbu'] : $HTTP_POST_VARS['spbu'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];
$kode_kendaraaan = isset($HTTP_GET_VARS['mobil'])? $HTTP_GET_VARS['mobil'] : $HTTP_POST_VARS['mobil'];


// LIST
$template->set_filenames(array('body' => 'daftar_manifest/daftar_manifest_kendaraan_body.tpl'));

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$Cabang	= new Cabang();

switch($mode){
	case 'getasal':

		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";

		exit;

	case 'gettujuan':
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
		exit;

	case 'getmobil':
		echo "<select name='mobil' id='mobil'>"
			.setComboMobil($mobil_dipilih).
			"</select>";;
		exit;
}

function setComboMobil($kode_kendaraan){
	//SET COMBO MOBIL
	global $db;

	$Mobil = new Mobil();
	$result=$Mobil->ambilDataForComboBox();
	$selected_default	= ($kode_kendaraan!="")?"":"selected";

	$opt_mobil="<option value='' $selected_default>silahkan pilih...</option>";

	if($result){
		while ($row = $db->sql_fetchrow($result)){
			$selected	=($kode_kendaraan!=$row['KodeKendaraan'])?"":"selected";
			$opt_mobil .="<option value='$row[KodeKendaraan]' $selected>$row[KodeKendaraan]</option>";
		}
	}
	else{
		echo("err :".__LINE__);exit;
	}

	return $opt_mobil;
	//END SET COMBO MOBIL
}
switch ($mode) {
	case 'delete':
		$no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];
		// Cek Role
		if($userdata['user_level'] != 0){
			echo 0;
			exit;
		}else{
			// Hapus Manifest (tbl_biaya_op)
			$sql1 = "DELETE FROM tbl_biaya_op WHERE NoSPJ='$no_spj'";
			// Hapus Manifest (tbl_ba_op)
			$sql2 = "DELETE FROM tbl_ba_bop WHERE NoSPJ='$no_spj'";
			// Update Manifest (tbl_posisi)
			$sql3 = "UPDATE tbl_posisi SET NoSPJ = null, KodeKendaraan ='', KodeSopir = '', TglCetakSPJ = null, PetugasCetakSPJ = null
	                    WHERE NoSPJ = '$no_spj'";
			// Update Manifest (tbl_reservasi)
			$sql4 = "UPDATE tbl_reservasi SET KodeKendaraan = '', KodeSopir = '', NoSPJ = '', TglCetakSPJ = '', CetakSPJ = 0
	                    WHERE NoSPJ = '$no_spj'";
			// Hapus Manifest (tbl_spj)
			$sql5 = "DELETE FROM tbl_spj WHERE NoSPJ='$no_spj'";
			if(!$db->sql_query($sql1)){
				echo 2;
				die_error("ERR: $this->ID_FILE".__LINE__);
			}else if(!$db->sql_query($sql2)){
				echo 3;
				die_error("ERR: $this->ID_FILE".__LINE__);
			}else if(!$db->sql_query($sql3)){
				echo 4;
				die_error("ERR: $this->ID_FILE".__LINE__);
			}else if(!$db->sql_query($sql4)){
				echo 5;
				die_error("ERR: $this->ID_FILE".__LINE__);
			}else if(!$db->sql_query($sql5)){
				echo 6;
				die_error("ERR: $this->ID_FILE".__LINE__);
			}else{
				echo 1;
				exit;
			}
		}
		break;

	case 'add_voucher':
		$date 		= date('Y-m-d H:i:s');
		$no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];
		$kilometer	= $HTTP_GET_VARS['kilometer'];
		$jenis_bbm  = $HTTP_GET_VARS['jenis_bbm'];
		$jml_liter	= $HTTP_GET_VARS['jml_liter'];
		$kode_spbu	= $HTTP_GET_VARS['kode_spbu'];
		$nama_bbm	= ($jenis_bbm == 1)?'SOLAR':'PREMIUM';
		switch ($kode_spbu) {
			case 1:
				$nama_spbu = 'BBM_SPBU_57';
				break;

			case 2:
				$nama_spbu = 'BBM_SPBU_72';
				break;
		}
		$sql 			= "SELECT NilaiParameter FROM tbl_pengaturan_parameter WHERE NamaParameter LIKE '%%$nama_bbm%%'";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$harga_bbm		= $row['NilaiParameter'];
		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		$kode_voucher   = $kode_spbu.date('YmdHis').rand(1,100);
		$digit          = 7 ;
		$secretKey      = rand(pow(10, $digit-1), pow(10, $digit)-1);

		$jml_biaya		= $harga_bbm*$jml_liter;

		$sql 		= "SELECT * FROM tbl_spj WHERE NoSPJ = '$no_spj';";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$tgl_berangkat	= $row['TglBerangkat'];
			$kode_jadwal	= $row['KodeJadwal'];
			$id_jurusan		= $row['IdJurusan'];
			$mobil 			= $row['NoPolisi'];
			$sopir 			= $row['KodeDriver'];
			$nama_sopir		= $row['Driver'];

		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}

		$user_id 		= $userdata['user_id'];
		$sql 			= "SELECT f_user_get_nama_by_userid('$user_id') User";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$nama_user		= $row['User'];
		}

		$sql 			= "INSERT INTO tbl_voucher_bbm SET KodeVoucher = '$kode_voucher', NoSPJ = '$no_spj', TglBerangkat = '$tgl_berangkat',
        						KodeJadwal = '$kode_jadwal', IdJurusan = '$id_jurusan', NoBody = '$mobil', KodeSopir = '$sopir', NamaSopir = '$nama_sopir',
        						Kilometer = '$kilometer', JenisBBM = '$jenis_bbm', JumlahLiter = '$jml_liter', JumlahBiaya = '$jml_biaya', 
        						IdPetugas = '$user_id', NamaPetugas = '$nama_user', TglDicatat = '$date', KodeSPBU = '$kode_spbu', NamaSPBU = '$nama_spbu'";
		if($db->sql_query($sql)){
			echo("alert('Voucher telah berhasil dibuat!');window.location.reload();");
		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}

		exit;

	case 'edit_voucher' :
		$no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];

		$sql 		= "SELECT * FROM tbl_voucher_bbm WHERE NoSPJ = '$no_spj'";
		if(!$result = $db->sql_query($sql)){
			echo("Err:".__LINE__);exit;
		}

		$row = $db->sql_fetchrow($result);

		$temp_var_aktif = 'spbu_'.$row['KodeSPBU'];
		$$temp_var_aktif = 'selected';

		$temp_var_aktif = 'jenis_bbm_'.$row['JenisBBM'];
		$$temp_var_aktif = 'selected';

		$result = "<form onsubmit='return false;'>
						<table width='400'>
						<tr>
							<td bgcolor='ffffff' align='center'>
								<table>
									<tr><td colspan='2'><h2>Edit Voucher</h2></td></tr>
									<tr>
										<td align='right'>Manifest :</td>
										<td>
											<input id='no_spj_edit' name='no_spj_edit' type='text' onfocus='this.style.background='white'' value='".$row['NoSPJ']."' />
										</td>
									</tr>
									<tr>
										<td align='right'>Nama SPBU:</td>
										<td>
											<select id='kode_spbu_edit'>
												<option value='1' $spbu_1>SPBU 57</option>
												<option value='2' $spbu_2>SPBU BSP</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align='right'>Kilometer:</td>
										<td><input id='kilometer_edit' name='kilometer_edit' type='text' onfocus='this.style.background='white'' size='5' onkeypress='validasiLiter(event)' value ='".$row['Kilometer']."'/></td>
									</tr>
									<tr>
										<td align='right'>Jenis BBM:</td>
										<td>
											<select id='jenis_bbm_edit'>
												<option value='1' $jenis_bbm_1>SOLAR</option>
												<option value='2' $jenis_bbm_2>PREMIUM</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align='right'>Jumlah Liter:</td>
										<td><input id='jml_liter_edit' name='jml_liter_edit' type='text' onfocus='this.style.background='white'' size='5' onkeypress='validasiLiter(event)' value='".$row['JumlahLiter']."'/></td>
									</tr>
								</table>
								<br>
							</td>
						</tr>
						<tr>
						   <td colspan='2' align='center'>  
								<br>
								<input type='button' id='dialogeditvouchercancel' value='&nbsp;Cancel&nbsp;' onClick='dlg_edit_voucher.hide();'> 
								<input type='button' onclick='updateVoucher();' id='dialogeditvoucherproses' value='&nbsp;Update&nbsp;'>
							 </td>
						</tr>
						</table>
						</form>";
		echo $result;
		exit;

	case 'update_voucher' :
		$date 		= date('Y-m-d H:i:s');
		$no_spj     = isset($HTTP_GET_VARS['no_spj_edit'])? $HTTP_GET_VARS['no_spj_edit'] : $HTTP_POST_VARS['no_spj_edit'];
		$kilometer	= $HTTP_GET_VARS['kilometer_edit'];
		$jenis_bbm  = $HTTP_GET_VARS['jenis_bbm_edit'];
		$jml_liter	= $HTTP_GET_VARS['jml_liter_edit'];
		$kode_spbu	= $HTTP_GET_VARS['kode_spbu_edit'];
		$nama_bbm	= ($jenis_bbm == 1)?'SOLAR':'PREMIUM';
		switch ($kode_spbu) {
			case 1:
				$nama_spbu = 'BBM_SPBU_57';
				break;

			case 2:
				$nama_spbu = 'BBM_SPBU_72';
				break;
		}
		$sql 			= "SELECT NilaiParameter FROM tbl_pengaturan_parameter WHERE NamaParameter LIKE '%%$nama_bbm%%'";
		if ($result = $db->sql_query($sql)){
			$row 			= $db->sql_fetchrow($result);
			$harga_bbm		= $row['NilaiParameter'];
		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}
		$digit          = 7 ;
		$secretKey      = rand(pow(10, $digit-1), pow(10, $digit)-1);

		$jml_biaya		= $harga_bbm*$jml_liter;

		$user_id 		= $userdata['user_id'];

		$sql 			= "UPDATE tbl_voucher_bbm SET
        						Kilometer = '$kilometer', JenisBBM = '$jenis_bbm', JumlahLiter = '$jml_liter', JumlahBiaya = '$jml_biaya', 
        						PetugasEdit = '$user_id', WaktuEdit = '$date', KodeSPBU = '$kode_spbu', NamaSPBU = '$nama_spbu'
        					WHERE NoSPJ = '$no_spj'";
		if($db->sql_query($sql)){
			echo("alert('Voucher telah berhasil Di Ubah!');window.location.reload();");
		}else{
			die_error("ERR: $this->ID_FILE".__LINE__);
		}

		exit;
	case 'approve':
		$no_spj     = isset($HTTP_GET_VARS['no_spj'])? $HTTP_GET_VARS['no_spj'] : $HTTP_POST_VARS['no_spj'];
		$date 		= date("Y-m-d H:i:s");
		$user_id 	= $userdata['user_id'];
		$sql 		= "UPDATE tbl_voucher_bbm SET IsApprove = 1, WaktuApprove = '$date', PetugasApprove = '$user_id'
							WHERE NoSPJ = '$no_spj';";
		if(!$db->sql_query($sql)){
			echo 0;
			die_error("ERR: $this->ID_FILE".__LINE__);
		}else{
			echo 1;
			exit;
		}
		break;
}

$kondisi =	$cari==""?"":
	" AND (KodeDriver LIKE '%$cari%'
		OR Driver LIKE '%$cari%'
	  OR ts.NoSPJ LIKE '%$cari%'
		OR ts.KodeJadwal LIKE '%$cari%'
		OR NoPolisi LIKE '%$cari%'
		OR NamaChecker LIKE '%$cari%')";

$kondisi .= $kota!="" ? " AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(ts.IdJurusan))='$kota'":"";
$kondisi .= $asal!="" ? " AND f_jurusan_get_kode_cabang_asal_by_jurusan(ts.IdJurusan)='$asal'":"";
$kondisi .= $asal!="" && $tujuan!="" ? " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(ts.IdJurusan)='$tujuan'":"";
$kondisi .= $kode_kendaraaan!="" ? " AND ts.NoPolisi='$kode_kendaraaan'":"";
$kondisi .= $spbu!="" ? "AND NamaSPBU = '$spbu'":"";
$order	=($order=='')?"DESC":$order;

$sort_by =($sort_by=='')?"NoPolisi,TglSPJ":$sort_by;


//PAGING======================================================
$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
$paging		= pagingData($idx_page,"ts.NoSPJ","tbl_spj ts",
	"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order&spbu=$spbu",
	"WHERE (ts.TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"daftar_manifest_kendaraan.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
//END PAGING======================================================

$sql	=
	"SELECT
		ts.KodeDriver,
		ts.Driver,
		ts.NoSPJ,
		ts.TglSPJ,
		ts.KodeJadwal,
		ts.TglBerangkat,
		ts.JamBerangkat,
		ts.JumlahKursiDisediakan,
		ts.JumlahPenumpang,
		ts.JumlahPaket,
		ts.NoPolisi,
		f_kendaraan_ambil_nopol_by_kode(ts.NoPolisi) AS NoPol,
		TIMEDIFF(ts.TglSPJ,CONCAT(DATE(ts.TglBerangkat),' ',ts.JamBerangkat)) AS Keterlambatan,
		IF(TIMEDIFF(ts.TglSPJ,CONCAT(DATE(ts.TglBerangkat),' ',ts.JamBerangkat)) >'00:30:00',1,0) IsTerlambat,
		ts.NamaChecker,
		ts.WaktuCheck,
		ts.JumlahPenumpangCheck,
		ts.JumlahPaketCheck,
		ts.JumlahPenumpangTambahanCheck,
		ts.JumlahPenumpangTanpaTiketCheck,
		ts.CatatanTambahanCheck,
		ts.PathFoto,
		vm.NamaSPBU
	FROM tbl_spj ts
	LEFT JOIN tbl_voucher_bbm vm ON ts.NoSPJ = vm.NoSPJ
	WHERE (ts.TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";

if(!$result = $db->sql_query($sql)){
    die(mysql_error());
	echo("Err:".__LINE__);exit;
}

$sqlBody = "SELECT NoPolisi,COUNT(NoPolisi) AS jml 
	FROM tbl_spj ts
	LEFT JOIN tbl_voucher_bbm vm ON ts.NoSPJ = vm.NoSPJ
	WHERE (ts.TglBerangkat BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
	$kondisi
	GROUP BY NoPolisi
	ORDER BY $sort_by $order";

if(!$resultBody = $db->sql_query($sqlBody)){
    die(mysql_error());
	echo("Err:".__LINE__);exit;
}

$i=1;

while ($rowBody = $db->sql_fetchrow($resultBody)) {
	$odd ='odd';
	$b = '<strong>';
	$a = '</strong>';

	if (($i % 2)==0){
		$odd = 'even2';
		$b ='';
		$a ='';
	}

	$groupBody[$rowBody['NoPolisi']]['mobil'] = $rowBody['NoPolisi'];
	$groupBody[$rowBody['NoPolisi']]['rowspan'] = $rowBody['jml'];
	$groupBody[$rowBody['NoPolisi']]['odd'] = $odd;
	$groupBody[$rowBody['NoPolisi']]['b'] = $b;
	$groupBody[$rowBody['NoPolisi']]['a'] = $a;
	$i++;
}

//print_r($groupBody);

$i=1;
$j=1;
while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';

	if (($i % 2)==0){
		$odd = 'even';
	}

	if ($row['PathFoto']) {
		$foto = "<a href='http://daytrans-stg.tiketux.com/apichecker/".$row['PathFoto']."' target='_blank' >Foto</a>";
	}
	else{
		$foto = "--";
	}

	if($row['IsTerlambat']==1){
		$flagterlambat = "red";
		$lama_terlambat	= substr($row['Keterlambatan'],0,8);
	}
	else{
		$flagterlambat	= "";
		$lama_terlambat	= "";
	}

	if($userdata['user_level'] <= 1){
		$linkViewLog    = "<a href='#' onClick='Start(\"".append_sid('daftar_log_manifest.php?no_manifest='.$row['NoSPJ'].'&kode_jadwal='.$row['KodeJadwal'].'&tgl='.$row['TglBerangkat'].'&jam='.$row['JamBerangkat'].'')."\");return false'>View<a/>";
	}else{
		$linkViewLog    = "";
	}

	if($userdata['user_level'] == 0){
		$spjTemp        = "'".$row['NoSPJ']."'";
		$delete         = "<a href='#' onClick='return hapusData(\"".$row['NoSPJ']."\");'>Hapus<a/>";
	}else{
		$delete         = "";
	}

	$sqlKoli = "SELECT SUM(JumlahKoli) as Koli FROM tbl_paket WHERE NoSPJ = '".$row['NoSPJ']."'";

	if(!$resKoli = $db->sql_query($sqlKoli)){
		echo("Err:".__LINE__);exit;
	}
	$rw      = $db->sql_fetchrow($resKoli);

	$sqlBbm	 = "SELECT COUNT(Id) as jml, JumlahLiter, NamaSPBU, TglDicatat, NamaPetugas,Kilometer,IsApprove FROM tbl_voucher_bbm  WHERE NoSPJ = '".$row['NoSPJ']."'";

	if(!$resBbm = $db->sql_query($sqlBbm)){
		echo("Err:".__LINE__);exit;
	}
	$rwBbm   = $db->sql_fetchrow($resBbm);
	if($rwBbm['jml'] == 0){
		$bbm = 'Belum Isi';
		$bgbbm = 'red';
		$literbbm = 0;
		$tglbbm = '-';
		$petugasbbm = '-';
		$act = "showDialogBuatVoucher('".$row['NoSPJ']."')";
	}else{
		$bbm = 'Sudah Isi';
		$bgbbm = '#c0ff53';
		$literbbm = $rwBbm['JumlahLiter'];
		$tglbbm = dateparse(FormatMySQLDateToTglWithTime($rwBbm['TglDicatat']));
		$petugasbbm = $rwBbm['NamaPetugas'];
		$act = "showDialogEditVoucher('".$row['NoSPJ']."')";
	}
	if($rwBbm['IsApprove'] == 1){
		$app 	= 'Sudah Approve';
		$bgApprove	= '#c0ff53';
	}else{
		$app 	= "<a href='#' onClick='return approve(\"".$row['NoSPJ']."\");'>Approve<a/>";
		$bgApprove	= '';
	}

	//$mobil = '<td></td>';

	//$mobil = '';
	//if($j == 1){
	$mobil = '<td align="center">'.$groupBody[$row['NoPolisi']]['b'].$row['NoPolisi'].$groupBody[$row['NoPolisi']]['a'].'</td>';
	//}
	//if ($j == $groupBody[$row['NoPolisi']]['rowspan']) {
	//	$j = 0;
	//}



	$template->
	assign_block_vars(
		'ROW',
		array(
			'odd'=>$groupBody[$row['NoPolisi']]['odd'],
			'flagterlambat'=>$flagterlambat,
			'no'=>$i+$idx_page*$VIEW_PER_PAGE,
			'jadwal'=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat']))." ".$row['JamBerangkat'],
			'kodejadwal'=>$row['KodeJadwal'],
			'berangkat'=>dateparse(FormatMySQLDateToTglWithTime($row['TglSPJ'])),
			'keterlambatan'=>$lama_terlambat,
			'manifest'=>$row['NoSPJ'],
			'sopir'=>$row['Driver'],
			'nopol'=>$row['NoPol'],
			'mobil'=>$mobil,
			'penumpang'=>number_format($row['JumlahPenumpang'],0,",","."),
			'paket'=>number_format($row['JumlahPaket'],0,",","."),
			'waktucheck'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuCheck'])),
			'checker'=>$row['NamaChecker'],
			'penumpangcheck'=>number_format($row['JumlahPenumpangCheck'],0,",","."),
			'paketcheck'=>number_format($row['JumlahPaketCheck'],0,",","."),
			'bgpnpchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangCheck']==$row['JumlahPenumpang']?"#c0ff53":"red"),
			'bgpktchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPaketCheck']==$row['JumlahPaket']?"#c0ff53":"red"),
			'penumpangtcheck'=>number_format($row['JumlahPenumpangTambahanCheck'],0,",","."),
			'penumpangttcheck'=>number_format($row['JumlahPenumpangTanpaTiketCheck'],0,",","."),
			'koli' => $rw['Koli'],
			'bbm'  => $bbm,
			'bgbbm'=> $bgbbm,
			'tglbbm' => $tglbbm,
			'petugasbbm' => $petugasbbm,
			'spbu' => $rwBbm['NamaSPBU'],
			'literbbm'=> number_format($literbbm,2,'.',','),
			'kilometer'=> $rwBbm['Kilometer'],
			'catatan'=>$row['CatatanTambahanCheck'],
			'foto'=>$foto,
			'catatan'=>$row['CatatanTambahanCheck'],
			'bgpnptchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangTambahanCheck']==0?"#c0ff53":"red"),
			'bgpnpttchk'=>$row['WaktuCheck']==""?"yellow":($row['JumlahPenumpangTanpaTiketCheck']==0?"#c0ff53":"red"),
			'linkViewLog' => $linkViewLog,
			'linkHapus' => $delete,
			'app'		=>$app,
			'bgstatus'		=>$bgApprove,
			'act' => $act
		)
	);
	$i++;
	$j+=1;
}

//paramter sorting
$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
$parameter_sorting	= "&page=$idx_page&kota=$kota&asal=$asal&tujuan=$tujuan&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&sort_by=$sort_by&order=$order&tanggal_mulai=".$tanggal_mulai."&tanggal_akhir=".$tanggal_akhir."&kota=$kota&asal=$asal&tujuan=$tujuan";
$script_cetak_excel="Start('daftar_manifest_kendaraan_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT


$template->assign_vars(array(
		'BCRUMP'    		=>setBcrump($id_page),
		'ACTION_CARI'		=> append_sid('daftar_manifest_kendaraan.'.$phpEx),
		'PAGING'		=> $paging,
		'CETAK_XL'		=> $script_cetak_excel,
		'TGL_AWAL'		=> $tanggal_mulai,
		'TGL_AKHIR'		=> $tanggal_akhir,
		'OPT_KOTA'		=> setComboKota($kota),
		'TXT_CARI'		=> $cari,
		'KOTA'		 	=> $kota,
		'ASAL'			=> $asal,
		'TUJUAN'		=> $tujuan,
		'A_SORT_1'		=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=TglBerangkat,JamBerangkat'.$parameter_sorting),
		'TIPS_SORT_1'		=> "Urutkan berdasarkan Jadwal ($order_invert)",
		'A_SORT_2'		=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=KodeJadwal'.$parameter_sorting),
		'TIPS_SORT_2'		=> "Urutkan berdasarkan KodeJadwal ($order_invert)",
		'A_SORT_3'		=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=TglSPJ'.$parameter_sorting),
		'TIPS_SORT_3'		=> "Urutkan berdasarkan Berangkat ($order_invert)",
		'A_SORT_4'		=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=Keterlambatan'.$parameter_sorting),
		'TIPS_SORT_4'		=> "Urutkan berdasarkan Keterlambatan ($order_invert)",
		'A_SORT_5'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=NoSPJ'.$parameter_sorting),
		'TIPS_SORT_5'		=> "Urutkan berdasarkan Manifest($order_invert)",
		'A_SORT_6'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=Driver'.$parameter_sorting),
		'TIPS_SORT_6'		=> "Urutkan berdasarkan Sopir ($order_invert)",
		'A_SORT_7'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=NoPolisi'.$parameter_sorting),
		'TIPS_SORT_7'		=> "Urutkan berdasarkan Mobil ($order_invert)",
		'A_SORT_8'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=JumlahPenumpang'.$parameter_sorting),
		'TIPS_SORT_8'		=> "Urutkan berdasarkan Jumlah Penumpang ($order_invert)",
		'A_SORT_9'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=JumlahPenumpang'.$parameter_sorting),
		'TIPS_SORT_9'		=> "Urutkan berdasarkan Jumlah Penumpang ($order_invert)",
		'A_SORT_10'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=WaktuCheck'.$parameter_sorting),
		'TIPS_SORT_10'	=> "Urutkan berdasarkan waktu check ($order_invert)",
		'A_SORT_11'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=NamaChecker'.$parameter_sorting),
		'TIPS_SORT_11'	=> "Urutkan berdasarkan Nama Checker ($order_invert)",
		'A_SORT_12'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=JumlahPenumpangCheck'.$parameter_sorting),
		'TIPS_SORT_12'	=> "Urutkan berdasarkan Jumlah Penumpang Checker ($order_invert)",
		'A_SORT_13'			=> append_sid('daftar_manifest_kendaraan.'.$phpEx.'?sort_by=JumlahPaketCheck'.$parameter_sorting),
		'TIPS_SORT_13'	=> "Urutkan berdasarkan Jumlah Paket Checker ($order_invert)",
	)
);

$PengaturanUmum	= new PengaturanUmum();
$result	= $PengaturanUmum->ambilParameterBatch("BBM_SPBU_%");

while($row=$db->sql_fetchrow($result)){
    $template->
    assign_block_vars(
        'OPT_SPBU',
        array(
            'value'		=> $row['NilaiParameter'],
            'nama'		=> $row['NilaiParameter'],
            'selected'=> ($spbu==$row['NilaiParameter']?"selected":"")
        )
    );
}

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>
