<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');

// SESSION
$id_page = 409;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
    redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';
require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php';

// PARAMETER
$cabang  = isset($HTTP_GET_VARS['p1'])? $HTTP_GET_VARS['p1'] : $HTTP_POST_VARS['p1'];
$awal    = isset($HTTP_GET_VARS['p2'])? $HTTP_GET_VARS['p2'] : $HTTP_POST_VARS['p2'];

$detail = new Cabang();
$detail_cabang = $detail->ambilDataDetail($cabang);
$awal = date_format(date_create($awal),"Y-m-d");
//QUERY AMBIL MODAL CABANG
$sql = "SELECT IDModal, IS_NULL(ModalAwal,0) as ModalAwal FROM tbl_md_cabang_modal WHERE KodeCabang='$cabang' AND TglTransaksiModal = '$awal' ";

if(!$data = $db->sql_query($sql)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}else{
    $dataModal = $db->sql_fetchrow($modal);
    $modalcabang = ($dataModal['ModalAwal'] == null)?0:$dataModal['ModalAwal'];
}
//QUERY AMBIL MODAL FIX DI TABEL CABANG
$sql = "SELECT ModalCabang FROM tbl_md_cabang WHERE KodeCabang='$cabang'";
if(!$data = $db->sql_query($sql)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}else{
    $result = $db->sql_fetchrow($modal);
    $MODALFIX = ($result['ModalCabang'] == null)?0:$result['ModalCabang'];
}

$style = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'   =>PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);
$border = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);

$font = array(
    'font'  => array(
        'color' => array('rgb' => 'FF0000')
    )
);

$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
$objPHPExcel->getActiveSheet()->mergeCells('B6:G7');
$objPHPExcel->getActiveSheet()->mergeCells('A6:A7');
$objPHPExcel->getActiveSheet()->mergeCells('H6:N6');
$objPHPExcel->getActiveSheet()->mergeCells('O6:Q6');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Penerimaan Pengeluaran Counter');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Divisi');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Tanggal');
$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Modal Cabang');
$objPHPExcel->getActiveSheet()->setCellValue('O5', $modalcabang);
$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
$objPHPExcel->getActiveSheet()->setCellValue('B3', ": ".$detail_cabang['Nama']);
$objPHPExcel->getActiveSheet()->setCellValue('B4', ": ".date_format(date_create($awal),'d-m-Y'));
$objPHPExcel->getActiveSheet()->setCellValue('B6', "Keterangan");
$objPHPExcel->getActiveSheet()->setCellValue('H6', "Penjualan");
$objPHPExcel->getActiveSheet()->setCellValue('H7', "Cash");
$objPHPExcel->getActiveSheet()->setCellValue('I7', "EDC");
$objPHPExcel->getActiveSheet()->setCellValue('J7', "Voucher");
$objPHPExcel->getActiveSheet()->setCellValue('K7', "Elevania");
$objPHPExcel->getActiveSheet()->setCellValue('L7', "Member");
$objPHPExcel->getActiveSheet()->setCellValue('M7', "Transfer");
$objPHPExcel->getActiveSheet()->setCellValue('N7', "Sistem");
$objPHPExcel->getActiveSheet()->setCellValue('O6', "Pengeluaran");
$objPHPExcel->getActiveSheet()->setCellValue('O7', "LPOC");
$objPHPExcel->getActiveSheet()->setCellValue('P7', "Other");

$objPHPExcel->getActiveSheet()->getStyle("A6:O7")->applyFromArray($style);

//AMBIL DATA TIKET TUNAI BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(Total) AS tiket_tunai, COUNT(Total) AS jml_tiket_tunai, NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal' AND JenisPembayaran = 0 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
            GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_tunai [$row['NoSPJ']]['TUNAI'] = $row['tiket_tunai'];
    $data_tiket_tunai [$row['NoSPJ']]['JML_TUNAI'] = $row['jml_tiket_tunai'];
}

//AMBIL DATA TIKET EDC BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(Total) AS tiket_edc, COUNT(Total) AS jml_tiket_edc,NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal' AND JenisPembayaran = 5 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
            GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_edc [$row['NoSPJ']]['EDC'] = $row['tiket_edc'];
    $data_tiket_edc [$row['NoSPJ']]['JML_EDC'] = $row['jml_tiket_edc'];
}

//AMBIL DATA TIKET TRANSFER BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(Total) AS tiket_transfer, COUNT(Total) AS jml_tiket_transfer, NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal'  AND JenisPembayaran = 6 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_transafer [$row['NoSPJ']]['TRANSFER'] = $row['tiket_transfer'];
    $data_tiket_transafer [$row['NoSPJ']]['JML_TRANSFER'] = $row['jml_tiket_transfer'];
}

//AMBIL DATA TIKET TIKETUX BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(Total) AS tiket_tiketux, COUNT(Total) AS jml_tiket_tiketux,NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal' AND JenisPembayaran = 99 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_tiketux [$row['NoSPJ']]['TIKETUX'] = $row['tiket_tiketux'];
    $data_tiket_tiketux [$row['NoSPJ']]['JML_TIKETUX'] = $row['jml_tiket_tiketux'];
}

//AMBIL DATA TIKET MEMBER BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(Total) AS tiket_member, COUNT(Total) AS jml_tiket_member, NoSPJ
            FROM tbl_reservasi
            WHERE TglBerangkat = '$awal'  AND JenisPembayaran = 4 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_member [$row['NoSPJ']]['MEMBER'] = $row['tiket_member'];
    $data_tiket_member [$row['NoSPJ']]['JML_MEMBER'] = $row['jml_tiket_member'];
}

//AMBIL DATA TIKET VOUCHER BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(Total) AS tiket_voucher, COUNT(Total) AS jml_tiket_voucher, NoSPJ
            FROM tbl_reservasi
            WHERE  Total > 0 AND TglBerangkat = '$awal'  AND JenisPembayaran = 3 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";

if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_voucher [$row['NoSPJ']]['VOUCHER'] = $row['tiket_voucher'];
    $data_tiket_vocuher [$row['NoSPJ']]['JML_VOUCHER'] = $row['jml_tiket_voucher'];
}

//AMBIL DATA TIKET VOUCHER ELEVANIA BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql = "SELECT sum(HargaTiket) AS tiket_elevania, COUNT(Total) AS jml_tiket_elevania, NoSPJ
            FROM tbl_reservasi
            JOIN tbl_voucher ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
            WHERE KodeVoucher LIKE 'ELDAY%' AND IsReturn != 1 AND TglBerangkat = '$awal'  AND JenisPembayaran = 3 AND KodeCabang = '$cabang'
            AND FlagBatal != 1 AND CetakTiket = 1 
            GROUP BY NoSPJ";

if(!$tiket = $db->sql_query($sql)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_tiket_elevania [$row['NoSPJ']]['ELEVANIA'] = $row['tiket_elevania'];
    $data_tiket_elevania [$row['NoSPJ']]['JML_ELEVANIA'] = $row['jml_tiket_elevania'];
}

// SPJ
//SET JURUSAN BERANGKAT DARI CABANG DIPILIH
$sql  ="SELECT IdJurusan,KodeCabangTujuan 
                FROM tbl_md_jurusan 
                WHERE KodeCabangAsal ='$cabang' AND FlagAktif=1 
                ORDER BY KodeCabangTujuan";

if(!$result=$db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

$id_jurusan = "";
while ($row = $db->sql_fetchrow($result)){
    $id_jurusan .= $row['IdJurusan'].',';
}

$id_jurusan = rtrim($id_jurusan,',');

$id_jurusan = $id_jurusan != ""?$id_jurusan:0;

//AMBIL KODE JADWAL UTAMA DARI LIST ID JURUSAN
$sql = "SELECT IF(FlagSubJadwal = 1,KodeJadwalUtama,KodeJadwal) AS Jadwal
            FROM tbl_md_jadwal
            WHERE IdJurusan IN ($id_jurusan) 
            AND FlagAktif = 1
            ORDER BY Jadwal";

if(!$result=$db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

$kode_jadwal = "";
while ($row = $db->sql_fetchrow($result)){
    $kode_jadwal .= "'".$row['Jadwal']."',";
}

$kode_jadwal = $kode_jadwal != ""?$kode_jadwal:"'KOSONG'";

//BIAYA OPERASIONAL DARI SPJ
$sql = "SELECT tbl_biaya_op.NoSPJ, SUM( Jumlah ) AS Biaya
            FROM tbl_biaya_op
            JOIN tbl_spj ON tbl_spj.NoSPJ = tbl_biaya_op.NoSPJ
            WHERE DATE(TglBerangkat) =  '$awal' 
            AND KodeJadwal IN (".rtrim($kode_jadwal,',').")
            AND tbl_biaya_op.FlagJenisBiaya = 1
            GROUP BY NoSPJ
            ORDER BY tbl_spj.IdJurusan, JamBerangkat";
if(!$op = $db->sql_query($sql)){
    die("Err:".__LINE__);
}
while ($row = $db->sql_fetchrow($op)){
    $data_op[$row['NoSPJ']]['OP'] = $row['Biaya'];
}

//AMBIL DAFTAR SPJ DARI CABANG TERTENTU DAN TANGGALNYA
$sql = "SELECT NoSPJ, KodeCabangAsal, TglBerangkat, JamBerangkat, Driver,tbl_md_jurusan.KodeJurusan, f_kendaraan_ambil_nopol_by_kode(NoPolisi) AS Mobil
            FROM tbl_spj 
            JOIN tbl_md_jurusan ON tbl_spj.IdJurusan = tbl_md_jurusan.IdJurusan
            WHERE DATE(TglBerangkat) = '$awal' 
            AND KodeJadwal IN (".rtrim($kode_jadwal,',').")
            ORDER BY tbl_spj.KodeJadwal, JamBerangkat;";
if(!$spj = $db->sql_query($sql)){
    die("Err:".mysql_error());
}

$paket_sql_spj = $sql;

$list_no_spj = "";

$sum_tunai = "";
$sum_member = "";
$sum_voucher = "";
$sum_edc = "";
$sum_transfer = "";
$sum_tiketux = "";
$sum_lpoc = "";
$sum_elevania = "";

$objPHPExcel->getActiveSheet()->setCellValue('B8', "TIKET");
$objPHPExcel->getActiveSheet()->mergeCells('B8:G8');

$idx=1;
while($row = $db->sql_fetchrow($spj)){

    $data_spj_qty        = $data_tiket_tunai[$row['NoSPJ']]['JML_TUNAI']+$data_tiket_edc[$row['NoSPJ']]['JML_EDC']+
                           $data_tiket_transafer[$row['NoSPJ']]['JML_TRANSFER']+$data_tiket_tiketux[$row['NoSPJ']]['JML_TIKETUX']+
                           $data_tiket_member[$row['NoSPJ']]['JML_MEMBER']+$data_tiket_voucher[$row['NoSPJ']]['JML_VOUCHER']+
                           $data_tiket_elevania[$row['NoSPJ']]['JML_ELEVANIA'];

    $sum_tunai      += $data_tiket_tunai[$row['NoSPJ']]['TUNAI'];
    $sum_edc        += $data_tiket_edc[$row['NoSPJ']]['EDC'];
    $sum_member     += $data_tiket_member[$row['NoSPJ']]['MEMBER'];
    $sum_voucher    += $data_tiket_voucher[$row['NoSPJ']]['VOUCHER'];
    $sum_elevania   += $data_tiket_elevania[$row['NoSPJ']]['ELEVANIA'];
    $sum_transfer   += $data_tiket_transafer[$row['NoSPJ']]['TRANSFER'];
    $sum_tiketux    += $data_tiket_tiketux[$row['NoSPJ']]['TIKETUX'];
    $sum_lpoc       += $data_op [$row['NoSPJ']]['OP'];

    $nospj = $row['NoSPJ'];
    $list_no_spj .="'$nospj',";

    $idx_row=$idx+8;

    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoSPJ']." / ".$row['KodeJurusan']." ".date_format(date_create($row['JamBerangkat']),'H:i')." ".$row['Mobil']." / ".$row['Driver']." (".$data_spj_qty." Pax)");
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $data_tiket_tunai[$row['NoSPJ']]['TUNAI']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $data_tiket_edc[$row['NoSPJ']]['EDC']);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $data_tiket_voucher[$row['NoSPJ']]['VOUCHER']);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $data_tiket_elevania[$row['NoSPJ']]['ELEVANIA']);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $data_tiket_member[$row['NoSPJ']]['MEMBER']);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, $data_tiket_transafer[$row['NoSPJ']]['TRANSFER']);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $data_tiket_tiketux[$row['NoSPJ']]['TIKETUX']);
    $objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $data_op [$row['NoSPJ']]['OP']);


    $idx++;

}

$list_no_spj = $list_no_spj != "" ? $list_no_spj : "'KOSONG'";

//===========================================================================================================
// tiket yang dibayar hari ini dan berangkat hari ini tapi counter berangkatnya beda dengan counter bayarnya
// jadi pembayaranya di akui di counter bayarnya.

$sql_tiket_ex = "SELECT tbl_reservasi.NoTiket, NoSPJ, tbl_reservasi.KodeJadwal, JamBerangkat, TglBerangkat, WaktuCetakTiket, Nama, JenisPembayaran,
                            IF(JenisPembayaran = 0,Total,0) AS tunai,
                            IF(JenisPembayaran = 5,Total,0) AS edc,
                            IF(JenisPembayaran = 3 AND LEFT(KodeVoucher,5) != 'ELDAY',Total,0) AS voucher,
                            IF(JenisPembayaran = 4,Total,0) AS member,
                            IF(JenisPembayaran = 6,Total,0) AS transfer,
                            IF(JenisPembayaran = 99,Total,0) AS tiketux,
                            IF(JenisPembayaran = 3 AND LEFT(KodeVoucher,5) = 'ELDAY',Total,0) AS elevania
                    FROM tbl_reservasi
                    LEFT JOIN tbl_voucher 
                    ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
                      WHERE Total > 0 AND FlagBatal !=1 AND CetakTiket =1
                      AND JenisPembayaran IN ( 0,3,4,5,6,99) 
                      AND DATE( WaktuCetakTiket ) =  '$awal'
                      AND DATE( WaktuCetakTiket ) = DATE( TglBerangkat )
                      AND tbl_reservasi.KodeCabang = '$cabang'
                      AND tbl_reservasi.NoSPJ NOT IN (".rtrim($list_no_spj,',').")
                    ORDER BY KodeJadwal;";

if(!$data = $db->sql_query($sql_tiket_ex)){
    echo("Err:".__LINE__.mysql_error());
    exit();
}

while($row = $db->sql_fetchrow($data)) {

    $sum_tunai += $row['tunai'];
    $sum_edc += $row['edc'];
    $sum_voucher += $row['voucher'];
    $sum_member += $row['member'];
    $sum_transfer += $row['transfer'];
    $sum_tiketux += $row['tiketux'];
    $sum_elevania += $row['elevania'];


    $idx_row=$idx+8;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoTiket']." / ".$row['KodeJadwal']." ".date_format(date_create($row['JamBerangkat']),'H:i')." ".date_format(date_create($row['TglBerangkat']),'d-m-Y')." / ".$row['Nama']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, ($row['tunai'] == 0)?'':$row['tunai']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, ($row['edc'] == 0)?'':$row['edc']);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, ($row['voucher'] == 0)?'':$row['voucher']);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, ($row['elevania'] == 0)?'':$row['elevania']);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, ($row['member'] == 0)?'':$row['member']);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, ($row['transfer'] == 0)?'':$row['transfer']);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, ($row['tiketux'] == 0)?'':$row['tiketux']);

    $objPHPExcel->getActiveSheet()->getStyle('B'.$idx_row)->applyFromArray($font);

    $idx++;


}

//====================================================================================================
//tiket yang dibayar sebelum hari keberangkatan di list satu2
//ambil tiket percabang



$sql_tiket = "SELECT tbl_reservasi.NoTiket, NoSPJ, tbl_reservasi.KodeJadwal, JamBerangkat, TglBerangkat, WaktuCetakTiket, Nama, JenisPembayaran,
                            IF(JenisPembayaran = 0,Total,0) AS tunai,
                            IF(JenisPembayaran = 5,Total,0) AS edc
                    FROM tbl_reservasi
                    LEFT JOIN tbl_voucher 
                    ON tbl_reservasi.NoTiket = tbl_voucher.NoTiket
                      WHERE Total > 0 AND FlagBatal !=1 AND CetakTiket =1
                      AND JenisPembayaran IN ( 0, 5) 
                      AND DATE( WaktuCetakTiket ) =  '$awal'
                      AND DATE( WaktuCetakTiket ) < DATE( TglBerangkat )
                      AND tbl_reservasi.KodeCabang = '$cabang'
                    ORDER BY KodeJadwal;";

if(!$data = $db->sql_query($sql_tiket)){
    die("Err:".__LINE__.mysql_error());
}
while($row = $db->sql_fetchrow($data)){


    $sum_tunai      += $row['tunai'];
    $sum_edc        += $row['edc'];
    $sum_voucher    += $row['voucher'];
    $sum_member     += $row['member'];
    $sum_transfer   += $row['transfer'];
    $sum_tiketux    += $row['tiketux'];
    $sum_elevania   += $row['elevania'];

    $idx_row=$idx+8;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoTiket']." / ".$row['KodeJadwal']." ".date_format(date_create($row['JamBerangkat']),'H:i')." ".date_format(date_create($row['TglBerangkat']),'d-m-Y')." / ".$row['Nama']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, ($row['tunai'] == 0)?'':$row['tunai']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, ($row['edc'] == 0)?'':$row['edc']);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, ($row['voucher'] == 0)?'':$row['voucher']);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, ($row['elevania'] == 0)?'':$row['elevania']);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, ($row['member'] == 0)?'':$row['member']);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, ($row['transfer'] == 0)?'':$row['transfer']);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, ($row['tiketux'] == 0)?'':$row['tiketux']);

    $objPHPExcel->getActiveSheet()->getStyle('B'.$idx_row)->applyFromArray($font);

    $idx++;
}


//===================================================================================================================
//TOP UP DEPOSIT MEMBER PERCABANG

$sql_topup = "SELECT * FROM tbl_member_deposit_topup_log
                  JOIN tbl_md_member ON tbl_md_member.IdMember = tbl_member_deposit_topup_log.IdMember
                  WHERE KodeCabang = '$cabang' AND DATE(WaktuTransaksi) = '$awal' AND IsVerified = 1";
if(!$topup = $db->sql_query($sql_topup)){
    die("error : ".__LINE__);
}

while ($row = $db->sql_fetchrow($topup)){

    $sum_member += $row['JumlahRupiah'];


    $idx_row=$idx+8;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoSeriKartu']." / ".$row['Nama']);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $row['JumlahRupiah']);

    $objPHPExcel->getActiveSheet()->getStyle('B'.$idx_row)->applyFromArray($font);

    $idx++;

}
//=====================================================================================================================


//===========================================================================================================
//AMBIL DATA PAKET TUNAI BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql_paket_tunai = "SELECT sum(HargaPaket) AS paket_tunai, COUNT(HargaPaket) AS jml_paket_tunai, NoSPJ
                        FROM tbl_paket
                        WHERE TglBerangkat = '$awal' AND JenisPembayaran = 0 AND KodeCabang = '$cabang'
                        AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
                        GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql_paket_tunai)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_paket_tunai [$row['NoSPJ']]['TUNAI'] = $row['paket_tunai'];
    $data_paket_tunai [$row['NoSPJ']]['JML_TUNAI'] = $row['jml_paket_tunai'];
}

//AMBIL DATA PAKET TUNAI BERDASARKAN TANGGAL, CABANG GROUP BY NO SPJ
$sql_paket_edc = "SELECT sum(HargaPaket) AS paket_edc, COUNT(HargaPaket) AS jml_paket_edc, NoSPJ
                        FROM tbl_paket
                        WHERE TglBerangkat = '$awal' AND JenisPembayaran = 5 AND KodeCabang = '$cabang'
                        AND FlagBatal != 1 AND CetakTiket = 1 AND DATE(TglBerangkat) = DATE(WaktuCetakTiket)
                        GROUP BY NoSPJ";
if(!$tiket = $db->sql_query($sql_paket_edc)){
    die("Err:".__LINE__."<br>".mysql_error());
}
while($row = $db->sql_fetchrow($tiket)){
    $data_paket_edc [$row['NoSPJ']]['EDC'] = $row['paket_edc'];
    $data_paket_edc [$row['NoSPJ']]['JML_EDC'] = $row['jml_paket_edc'];
}

$objPHPExcel->getActiveSheet()->setCellValue('B'.($idx+8), "PAKET");
$objPHPExcel->getActiveSheet()->mergeCells('B'.($idx+8).':G'.($idx+8));

if(!$spj = $db->sql_query($paket_sql_spj)){
    die("Err:".mysql_error());
}

while($row = $db->sql_fetchrow($spj)){

    $sum_tunai += $data_paket_tunai [$row['NoSPJ']]['TUNAI'];
    $sum_edc += $data_paket_edc[$row['NoSPJ']]['EDC'];

    $data_paket_jml = $data_paket_tunai[$row['NoSPJ']]['JML_TUNAI']+$data_paket_edc[$row['NoSPJ']]['JML_EDC'];

    $idx_row=$idx+9;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoSPJ']." / ".$row['KodeJurusan']." ".date_format(date_create($row['JamBerangkat']),'H:i')." ".$row['Mobil']." / ".$row['Driver']." (".$data_paket_jml." Paket)");
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $data_paket_tunai [$row['NoSPJ']]['TUNAI']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $data_paket_edc[$row['NoSPJ']]['EDC']);

    $idx++;
}

//paket yang dibayar hari ini dan keberangkatan untuk esoknya di list satu2
//ambil paket percabang
$sql_paket = "SELECT *, IF(JenisPembayaran = 0,HargaPaket,0) AS tunai, IF(JenisPembayaran = 5,HargaPaket,0) AS edc 
                FROM tbl_paket
                JOIN tbl_md_jurusan ON tbl_paket.IdJurusan = tbl_md_jurusan.IdJurusan
                WHERE FlagBatal != 1 AND CetakTiket = 1 
                AND JenisPembayaran IN (0,5) 
                AND tbl_paket.KodeCabang = '$cabang'
                AND DATE(WaktuCetakTiket) = '$awal' 
                AND DATE(WaktuCetakTiket) < DATE(TglBerangkat);";

if(!$data = $db->sql_query($sql_paket)){
    die("Err:".__LINE__);
}

while($row = $db->sql_fetchrow($data)){

    $sum_tunai += $row['tunai'];
    $sum_edc += $row['edc'];

    $idx_row=$idx+9;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NoTiket']." / ".$row['KodeJadwal']." ".date_format(date_create($row['JamBerangkat']),'H:i')." ".date_format(date_create($row['TglBerangkat']),'d-m-Y')." / Pengirim ".$row['NamaPengirim']." / Penerima ".$row['NamaPenerima']);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, ($row['tunai'] == 0)?'':$row['tunai']);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, ($row['edc'] == 0)?'':$row['edc']);

    $objPHPExcel->getActiveSheet()->getStyle('B'.$idx_row)->applyFromArray($font);

    $idx++;
}

//QUERY BIAYA TAMBAHAN
$sql = "SELECT KodeCabang,SUM(Jumlah) AS Jumlah,KodeJurusan, Keterangan, NamaPenerima, JenisBiaya, f_kendaraan_ambil_nopol_by_kode(KodeKendaraan) as mobil
            FROM tbl_biaya_tambahan
            WHERE DATE(TglCetak)='$awal' AND KodeCabang = '$cabang'
            GROUP BY NamaPenerima, TglCetak ";
if (!$other = $db->sql_query($sql)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}

if (!$other = $db->sql_query($sql)){
    echo("Err:".__LINE__."<br>");
    die(mysql_error());
}


$objPHPExcel->getActiveSheet()->setCellValue('B'.($idx+9), "BIAYA TAMBAHAN");
$objPHPExcel->getActiveSheet()->mergeCells('B'.($idx+9).':G'.($idx+9));

while($dataother = $db->sql_fetchrow($other)){

    $idx_row=$idx+10;
    $objPHPExcel->getActiveSheet()->mergeCells('B'.$idx_row.':G'.$idx_row);
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $dataother['NamaPenerima']." / ".$dataother['KodeJurusan']." ".$dataother['mobil']." / ".$dataother['Keterangan']);
    $objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $dataother['Jumlah']);

    $idx++;
}

// jika pendapatan lebih besar dari pengeluaran maka modal jangan dipake
if(($sum_tunai+$sum_member) >= ($sum_lpoc+$sum_biaya_tambahan)){
    $saldo_akhir = $modalcabang;
}else{
    $saldo_akhir = $modalcabang + ($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan);
}

//jika modal terakhir kurang dari MODALFIX maka harus digenapkan lagi jadi MODALFIX
if($modalcabang < $MODALFIX){
    if((($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan)) > ($MODALFIX- $modalcabang)){
        $saldo_akhir = $MODALFIX;
    }else{
        $saldo_akhir = $modalcabang + ($sum_tunai+$sum_member) - ($sum_lpoc+$sum_biaya_tambahan);
    }
}

$setoran = ($sum_tunai) - ($sum_spj+$sum_other);

$idx_row=$idx+10;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':G'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, 'Total');
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, '=SUM(H8:H'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, '=SUM(I8:I'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, '=SUM(J8:J'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, '=SUM(K8:K'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, '=SUM(L8:L'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('M'.$idx_row, '=SUM(M8:M'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, '=SUM(N8:N'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, '=SUM(O8:O'.($idx_row-1).')');
$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, '=SUM(P8:P'.($idx_row-1).')');


$objPHPExcel->getActiveSheet()->getStyle("A".$idx_row.":P".$idx_row)->applyFromArray($style);
$objPHPExcel->getActiveSheet()->getStyle("A6:P".$idx_row)->applyFromArray($border);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Laporan Penerimaan Pengeluaran Counter '.$detail_cabang['Nama']." Tanggal".date_format(date_create($awal),'d-m-Y'));
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');

?>