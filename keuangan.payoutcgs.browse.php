<?php

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassPayoutCGS.php');

// SESSION
$id_page = 408.2;
$userdata = session_pagestart($user_ip,$id_page);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

//METHODES

// PARAMETER
$siklus         = isset($HTTP_POST_VARS['siklus'])?$HTTP_POST_VARS['siklus'] : $HTTP_GET_VARS['siklus'];
$fil_bln        = isset($HTTP_POST_VARS['bulan'])?$HTTP_POST_VARS['bulan']:$HTTP_GET_VARS['bulan'];
$fill_thn       = isset($HTTP_POST_VARS['tahun'])?$HTTP_POST_VARS['tahun']:$HTTP_GET_VARS['tahun'];
$kode_sopir     = isset($HTTP_POST_VARS['kodesopir'])?$HTTP_POST_VARS['kodesopir']:$HTTP_GET_VARS['kodesopir'];
$nama           = isset($HTTP_POST_VARS['nama'])?$HTTP_POST_VARS['nama']:$HTTP_GET_VARS['nama'];

$PayoutCGS  = new PayoutCGS();

$result = $PayoutCGS->browseManifestCGS($siklus,$fil_bln,$fill_thn,$kode_sopir);

$idx = 0;

//PLOT DATA
while($data=$db->sql_fetchrow($result)){

  $idx++;

  $odd =($idx%2)==0?"even":"odd";

  $template->assign_block_vars(
    'ROW',array(
      'odd'         => $odd,
      'no'          => $idx,
      'tglberangkat'=> dateparse(FormatMySQLDateToTgl($data['TglBerangkat'])),
      'jamberangkat'=> $data['JamBerangkat'],
      'cabangasal'  => $data['KodeCabangAsal'],
      'cabangtujuan'=> $data['KodeCabangTujuan'],
      'kodejadwal'  => $data['KodeJadwal'],
      'nopol'       => $data['NoPolisi'],
      'waktucetak'  => dateparseWithTime(FormatMySQLDateToTglWithTime($data['TglSPJ']))
    )
  );

}

$template->assign_vars(array(
    "DRIVER"  => substr($nama." (".$kode_sopir.")",0,50)
  )
);

$template->set_filenames(array('body' =>'keuangan.payoutcgs/browse.tpl'));

$template->pparse('body');
?>