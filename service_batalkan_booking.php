<?php
define('FRAMEWORK', true);

$adp_root_path = './';
include($adp_root_path . 'common.php');
$ip			= $_SERVER['REMOTE_ADDR'];
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
//if($ip!=""){
//    echo("ACCESS DENIED");
//    exit;
//}
//#############################################################################

// mencari tiket yang belum cetak tiket, belum dibatalkan, tanggal berangkatnya lebih besar atau sama dengan hari ini dan waktu pesan sudah lebih dari 6 jam
$sql = "SELECT NoTiket, KodeBooking, NomorKursi, Nama, TglBerangkat, WaktuPesan, NOW() AS SEKARANG,
        TIMEDIFF(NOW(),WaktuPesan) AS SELISIH,
        CetakTiket, WaktuCetakTiket
        FROM tbl_reservasi
        WHERE TglBerangkat >= CURDATE() AND CetakTiket = 0 AND FlagBatal = 0
        HAVING SELISIH > '06:00:00'";
if (!$result = $db->sql_query($sql)){
    echo("Err:".__LINE__);exit;
}

$jml=0;
if($db->sql_affectedrows($result) > 0){
    while($row = $db->sql_fetchrow($result)){
        //  BATALKAN SEMUA TIKET YANG DI TEMUKAN DARI QUERY DI ATAS
        $sql = "UPDATE tbl_reservasi SET FlagBatal = 1, PetugasPembatalan = 0, WaktuPembatalan = NOW()
                WHERE NoTiket ='".$row['NoTiket']."'";

        if (!$batal = $db->sql_query($sql)){
            echo("Err:".__LINE__);exit;
        }

        //BATALKAN TIKET DI RESERVSAI OLAP
        if($db->sql_affectedrows($batal) == 0){
            $sql = "UPDATE tbl_reservasi_olap SET FlagBatal = 1, PetugasPembatalan = 0, WaktuPembatalan = NOW()
            WHERE NoTiket = '".$row['NoTiket']."'";
            if (!$result = $db->sql_query($sql)){
                echo("Err:".__LINE__);exit;
            }else{
                $jml++;
            }
        }else{
            $jml++;
        }

        //MENGUPDATE TBL POSISI, MEMBEBASKAN STATUS KURSI YANG DIBATALKAN
        $sql = "UPDATE tbl_posisi_detail
			    SET StatusKursi = 0, Nama=NULL, NoTiket=NULL,
				KodeBooking=NULL,Session=NULL,StatusBayar=0
			    WHERE NoTiket='".$row['NoTiket']."'";
        if (!$db->sql_query($sql)){
            echo("Err :".__LINE__);exit;
        }

        if($db->sql_affectedrows()==0){
            //MENGUPDATE TBL POSISI,
            $sql =
                "UPDATE tbl_posisi_detail_backup
				SET StatusKursi = 0, Nama=NULL, NoTiket=NULL,
					KodeBooking=NULL,Session=NULL,StatusBayar=0
				WHERE NoTiket='".$row['NoTiket']."';";

            if (!$db->sql_query($sql)){
                echo("Err =".__LINE__);
            }
        }


    }
}

echo  "Jumlah Tiket Yang Dibatalkan = ".$jml;

?>