<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$id_page  = 200;
$userdata = session_pagestart($user_ip,$id_page);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

include($adp_root_path . 'ClassCabang.php');

// PARAMETER
$perpage 		= $config['perpage'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$cabang  		= isset($HTTP_GET_VARS['cabang'])? $HTTP_GET_VARS['cabang'] : $HTTP_POST_VARS['cabang'];

// LIST
$template->set_filenames(array('body' => 'bookingops/index.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$cabang		= $cabang==""?"CHP":$cabang;

//GET LIST CABANG
$Cabang	= new Cabang();
$data_cabang	= $Cabang->ambilDataDetail($cabang);

//SET JURUSAN BERANGKAT DARI CABANG DIPILIH
$sql 	= 
	"SELECT IdJurusan,KodeCabangTujuan 
	FROM tbl_md_jurusan 
	WHERE f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='".$data_cabang['KodeCabang']."' AND FlagAktif=1 ORDER BY KodeCabangTujuan";

if(!$result=$db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$col=1;

while($row=$db->sql_fetchrow($result)){
	$template->assign_block_vars("LISTDATA",array("idjurusan"	=>$row['IdJurusan'],"col"=>$col%4));
	$col++;
}

$page_title = "Booking";


$template->assign_vars(array(
	'BCRUMP'    		=>setBcrump($id_page),
	'ACTION_CARI'		=> append_sid('bookingops.'.$phpEx),
	'TGL_AWAL'			=> $tanggal_mulai,
	'OPT_CABANG'		=> $Cabang->setInterfaceComboCabang($cabang),
	'CABANG_BERANGKAT'=> $data_cabang['Nama']
	)
);
	      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>